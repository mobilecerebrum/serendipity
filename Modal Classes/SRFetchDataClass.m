#import "SRFetchDataClass.h"

@implementation SRFetchDataClass

#pragma mark
#pragma mark - Init
#pragma mark

// ----------------------------------------------------------------------------------------------------------------
// init

- (id)init {
	// Call super
	self = [super init];
	if (self) {
		// Initialization
		citiesList = [[NSMutableArray alloc]init];
	}

	// Return
	return self;
}

#pragma mark
#pragma mark - Get CountryList Method
#pragma mark

// ----------------------------------------------------------------------------------------------------------------
// getCountryList:

- (void)getCountryList {
	NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"countries_codes" ofType:@"xml"]];
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:[NSData dataWithContentsOfURL:baseURL]];

	// Tell NSXMLParser that this class is its delegate
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];

	// Kick off file parsing
	BOOL result = [parser parse];
	if (result)
		NSLog(@"Succeed parsing xml");
}

#pragma mark
#pragma mark - NSXMLParser Delegate Method
#pragma mark

// ----------------------------------------------------------------------------------------------------------------
// didStartElement:

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
}

// ----------------------------------------------------------------------------------------------------------------
// foundCharacters:

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
}

// ----------------------------------------------------------------------------------------------------------------
// didEndElement:

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
}

// ----------------------------------------------------------------------------------------------------------------
// parserDidEndDocument:

- (void)parserDidEndDocument:(NSXMLParser *)parser {
}

@end
