//
//  TrackingLocationClass.h
//  Serendipity
//
//  Created by Abbas Mulani on 09/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackingLocationClass : NSObject

@property (nonatomic,strong) NSString *locationName;
@property (nonatomic,strong) NSString *locationId;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSNumber *notify;
@property (nonatomic,strong) NSNumber *notify_on;
@property (nonatomic,strong) NSString *radius;
@property BOOL isSelected;
 
@end
