//
//  SRLabel.h
//  Serendipity
//
//  Created by Leo on 18/10/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRLabel : UILabel

@property (nonatomic,strong) NSIndexPath *indexPath;

@end
