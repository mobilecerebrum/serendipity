#import <Foundation/Foundation.h>

@protocol SRFetchDataClassDelegate <NSObject>
- (void)fetchedCitiesListComplete:(NSArray *)inArr;
@end

@interface SRFetchDataClass : NSObject <NSXMLParserDelegate>

{
	// Instance variable
	NSMutableArray *citiesList;
}

// Property
@property (weak) id delegate;

#pragma mark
#pragma mark - Get CountryList Method
#pragma mark

- (void)getCountryList;

@end
