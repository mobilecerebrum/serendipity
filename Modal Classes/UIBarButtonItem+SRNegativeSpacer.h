#import <UIKit/UIKit.h>

@interface UIBarButtonItem (SRNegativeSpacer)

+ (UIBarButtonItem*)negativeSpacerWithWidth:(NSInteger)width;

@end
