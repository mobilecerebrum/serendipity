#import "UIBarButtonItem+SRNegativeSpacer.h"
#import "SRModalClass.h"
#import "SRDistanceClass.h"
#import <MapKit/MKGeometry.h>


#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

@implementation SRModalClass

// ----------------------------------------------------------------------------------------------------------------
// showTabBar:

+ (void)showTabBar:(UITabBarController *)tabbarcontroller {
	tabbarcontroller.tabBar.hidden = NO;
	CGFloat height = 49;
	if ((APP_DELEGATE).isZero) {
		height = 0;
	}
	[UIView animateWithDuration:0 animations: ^{
	    for (UIView *view in tabbarcontroller.view.subviews) {
	        if ([view isKindOfClass:[UITabBar class]]) {
	            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y - height, view.frame.size.width, view.frame.size.height)];
			}
	        else {
	            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height - height)];
			}
		}
	} completion: ^(BOOL finished) {
	}];
}

// ----------------------------------------------------------------------------------------------------------------
// hideTabBar:

+ (void)hideTabBar:(UITabBarController *)tabbarcontroller {
	[UIView animateWithDuration:0 animations: ^{
	    for (UIView *view in tabbarcontroller.view.subviews) {
	        if ([view isKindOfClass:[UITabBar class]]) {
	            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y + 49.f, view.frame.size.width, view.frame.size.height)];
			}
	        else {
	            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height + 49.f)];
			}
		}
	} completion: ^(BOOL finished) {
	    //do smth after animation finishes
	    tabbarcontroller.tabBar.hidden = YES;
	}];
}

#pragma mark
#pragma mark - Navigation Customize Method
#pragma mark

// ----------------------------------------------------------------------------------------------------------------
// setNavTitle: forViewNavCon:

+ (void)setNavTitle:(NSString *)inTitleStr forViewNavCon:(UINavigationItem *)inNavItem forFontSize:(CGFloat)inSize {
	UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH/2, 67)];
	lblTitle.text = inTitleStr;
	lblTitle.backgroundColor = [UIColor clearColor];
	lblTitle.textColor = [UIColor whiteColor];
	lblTitle.font = [UIFont fontWithName:kFontHelveticaMedium size:inSize];
	lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.numberOfLines = 0;
	[lblTitle sizeToFit];
	inNavItem.titleView = lblTitle;
}

// ----------------------------------------------------------------------------------------------------------------
// setLeftNavBarItem:

+ (UIButton *)setLeftNavBarItem:(NSString *)inTitleStr
                       barImage:(UIImage *)inImage
                  forViewNavCon:(UIViewController *)inViewCon
                         offset:(CGFloat)inOffset {
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

	if (inTitleStr == nil) {
		UIImage *buttonImage = inImage;
		[button setImage:buttonImage forState:UIControlStateNormal];
		button.frame = CGRectMake(0, 0, 30, kKeyNavBarLeftBtnHeight);
	}
	else {
		inOffset = 24;
		[button setTitle:inTitleStr forState:UIControlStateNormal];
		[button setTitleColor:Rgb2UIColor(255, 149, 0) forState:UIControlStateNormal];
		[button.titleLabel setFont:[UIFont fontWithName:kFontHelveticaMedium size:14.0]];
		button.frame = CGRectMake(0, 0, 66, kKeyNavBarLeftBtnHeight);
	}

	UIBarButtonItem *spacerItem = [UIBarButtonItem negativeSpacerWithWidth:inOffset];
	UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
	imgView.backgroundColor = [UIColor clearColor];
	UIBarButtonItem *nonActiveBtn = [[UIBarButtonItem alloc] initWithCustomView:imgView];
	UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	NSArray *items = @[spacerItem, nonActiveBtn, customBarItem];
	inViewCon.navigationItem.leftBarButtonItems = items;

	// Return
	return button;
}

// ----------------------------------------------------------------------------------------------------------------
// setRightNavBarItem:

+ (UIButton *)setRightNavBarItem:(NSString *)inTitleStr barImage:(UIImage *)inImage forViewNavCon:(UIViewController *)inViewCon offset:(CGFloat)inOffset {
	UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];

	if (inImage == nil) {
		inOffset = 18;
		[rightButton setTitle:inTitleStr forState:UIControlStateNormal];
		[rightButton setTitleColor:Rgb2UIColor(255, 149, 0) forState:UIControlStateNormal];
		[rightButton.titleLabel setFont:[UIFont fontWithName:kFontHelveticaMedium size:14.0]];
		rightButton.frame = CGRectMake(0, 0, 44, kKeyNavBarLeftBtnHeight);
	}
	else {
		inOffset = 23;
		UIImage *buttonImage = inImage;
		[rightButton setImage:buttonImage forState:UIControlStateNormal];
		rightButton.frame = CGRectMake(0, 0, kKeyNavBarLeftBtnWidth, kKeyNavBarLeftBtnHeight);
	}

	UIBarButtonItem *spacerItem = [UIBarButtonItem negativeSpacerWithWidth:inOffset];
	UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
	NSArray *items = @[spacerItem, customBarItem];

	inViewCon.navigationItem.rightBarButtonItems = items;

	// Return
	return rightButton;
}

#pragma mark
#pragma mark - Gredient Method
#pragma mark

// ----------------------------------------------------------------------------------------------------------------
// applyGredientColor:

+ (void)applyGredientColor:(UIColor *)topColor
               centerColor:(UIColor *)inCenterColor
               bottomColor:(UIColor *)inBottomColor
                   forView:(UIImageView *)inImageView {
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = inImageView.frame;

	// Add colors to layer
	gradient.colors = [NSArray arrayWithObjects:
	                   (id)[topColor CGColor],
	                   (id)[inCenterColor CGColor],
	                   (id)[inBottomColor CGColor],
	                   nil];

	[inImageView.layer insertSublayer:gradient atIndex:0];
}

// ----------------------------------------------------------------------------------------------------------------
// createImageWith:

+ (UIImage *)createImageWith:(UIColor *)color {
	CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();

	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);

	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	// Return
	return image;
}

#pragma mark - Custom Method

/// Set your border with multi color, will return layer with multi colors
/// @param colors pass an arr which contains cgcolorref of color
/// @param view pass view here for its frame and bounds.

+ (UIView *)layerWithMultiColorBorder:(NSArray *)colors forView:(UIView*)view {
    NSCountedSet *countedColors = [[NSCountedSet alloc] initWithArray:colors];
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:colors];
    NSSet *uniqueColors = [orderedSet set];
//    NSMutableArray<NSNumber*>* arrLocations = [NSMutableArray.alloc init];
    NSArray *arrColors = [uniqueColors allObjects];
    NSMutableArray<ColorData*> *arrColorData = [NSMutableArray new];
    
    for (int i = 0; i<uniqueColors.count; i++) {
        int count = (int)[countedColors countForObject:arrColors[i]];
        float percentage = (float)count / (float)colors.count;
        ColorData *colorData = [ColorData new];
        colorData.color = arrColors[i];
        colorData.percentage = percentage;
        [arrColorData addObject:colorData];
    }
    
    MultiBorderColorView *multiBorderColorview = [[MultiBorderColorView alloc] initWithFrame:view.frame];
    [multiBorderColorview setColorList:arrColorData];
    
//    CAGradientLayer *objgradientLayer =  [[CAGradientLayer alloc] init];
//    objgradientLayer.frame = view.frame;
//    objgradientLayer.startPoint = CGPointMake(0.0, 0.5);
//    objgradientLayer.type = kCAGradientLayerAxial;
//    objgradientLayer.endPoint = CGPointMake(1.0, 0.5);
//    
//    objgradientLayer.colors = arrColors;
//
//    CAShapeLayer *objShapeLayer =[[CAShapeLayer alloc] init];
//    objShapeLayer.lineWidth = 10.0;
//    objShapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:view.bounds cornerRadius:view.layer.cornerRadius].CGPath;
//    objShapeLayer.fillColor = nil;
//    objShapeLayer.strokeColor = [UIColor whiteColor].CGColor;
//    objShapeLayer.cornerRadius = 6;
//    objgradientLayer.mask = objShapeLayer;
//    objgradientLayer.cornerRadius = 6;

    return multiBorderColorview;
}

// --------------------------------------------------------------------------------
// imageFromUIView:

+ (UIImage *)imageFromUIView:(UIView *)view {
	CGRect rect = CGRectMake(150, 100, view.bounds.size.width, view.bounds.size.height);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	[view.layer renderInContext:context];
	UIImage *imageCaptureRect;

	imageCaptureRect = UIGraphicsGetImageFromCurrentImageContext();

	UIGraphicsEndImageContext();
	return imageCaptureRect;
}

// --------------------------------------------------------------------------------
// removeNullValuesFromDict:

+ (NSDictionary *)removeNullValuesFromDict:(id)inDict {
	NSMutableDictionary *updateDict = [NSMutableDictionary dictionary];
	if (inDict != (id)[NSNull null]) {
		if ([inDict isKindOfClass:[NSDictionary class]]) {
			for (NSString *inKey in[inDict allKeys]) {
				id value = [inDict objectForKey:inKey];
				if ([value isKindOfClass:[NSNull class]]) {
					// do noting
					[updateDict setObject:@"" forKey:inKey];
				}
				else if ([value isKindOfClass:[NSDictionary class]]) {
					NSMutableDictionary *valueDict = [NSMutableDictionary dictionary];
					for (NSString *valueKey in[value allKeys]) {
						id subValue = [value objectForKey:valueKey];
						if ([subValue isKindOfClass:[NSNull class]]) {
							// do noting
							[valueDict setObject:@"" forKey:valueKey];
						}
						else {
							[valueDict setObject:subValue forKey:valueKey];
						}
					}

					// add to update dict
					[updateDict setObject:valueDict forKey:inKey];
				}
				else if ([value isKindOfClass:[NSArray class]]) {
					NSMutableArray *mutatedArr = [NSMutableArray array];
					for (NSDictionary *inDict in value) {
                        NSMutableDictionary *valueDict = [NSMutableDictionary dictionary];
                        if (![inDict isKindOfClass:[NSDictionary class]]) {
                            continue;
                        }
						for (NSString *valueKey in [inDict allKeys]) {
							id subValue = [inDict objectForKey:valueKey];
							if ([subValue isKindOfClass:[NSNull class]]) {
								// do noting
								[valueDict setObject:@"" forKey:valueKey];
							}
							else {
								[valueDict setObject:subValue forKey:valueKey];
							}
						}

						// Add object to array
						[mutatedArr addObject:valueDict];
					}

					// Add to to upper dict
					[updateDict setObject:mutatedArr forKey:inKey];
				}
				else {
					[updateDict setObject:value forKey:inKey];
				}
			}
		}
	}
	// Return
	return updateDict;
}

// --------------------------------------------------------------------------------
//  showAlert:

+ (void)showAlert:(NSString *)inMessage fromController:(UIViewController*)controller {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"alert.title.text", "")
                                                                   message:inMessage
                                                            preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"alert.button.ok.text", "") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:okButton];
    [controller presentViewController:alert animated:YES completion:nil];
}

+(void)showSuccess:(NSString*)message {
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"alert.title.success", "")
                                        message:message
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", "")
                              otherButtonTitles:nil, nil];

    [alertView show];
}

+ (void)showAlert:(NSString *)inErrorStr {
    
    
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"alert.title.text", "")
                                        message:inErrorStr
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", "")
                              otherButtonTitles:nil, nil];

    [alertView show];
    
    
}

+ (void)showAlertWithTitle:(NSString *)inErrorStr  alertTitle:(NSString*)title{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:title
                                        message:inErrorStr
                                       delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", "")
                              otherButtonTitles:nil, nil];

    [alertView show];
}



// --------------------------------------------------------------------------------
// setUserImage:

+ (void)setUserImage:(NSDictionary *)inDict
              inSize:(NSString *)size
         applyToView:(UIImageView *)inView {
	// Get result
		NSDictionary *profileImgDict = [inDict objectForKey:kKeyProfileImage];
		if ([profileImgDict isKindOfClass:[NSDictionary class]] && [profileImgDict objectForKey:kKeyImageName]) {
		    NSString *imageName = [[inDict objectForKey:kKeyProfileImage] objectForKey:kKeyImageName];
		    if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                
                [inView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            }
            else
                inView.image = [UIImage imageNamed:@"profile_menu.png"];
        }
}

// --------------------------------------------------------------------------------
// geoCodeUsingAddress:

+ (CLLocationCoordinate2D)geoCodeUsingAddress:(NSString *)address {
	double latitude = 0, longitude = 0;
    NSString *esc_addr = [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//	NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
	NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
	if (result) {
		NSScanner *scanner = [NSScanner scannerWithString:result];
		if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
			[scanner scanDouble:&latitude];
			if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
				[scanner scanDouble:&longitude];
			}
		}
	}

	// Co - ordinates
	CLLocationCoordinate2D center;
	center.latitude = latitude;
	center.longitude = longitude;

	// Location center
	return center;
}
#pragma mark
#pragma mark - Get Image URL
#pragma mark
+(NSURL *)getProfileImageURL:(NSString *)fileName
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, fileName]];
}
#pragma mark
#pragma mark - Cache Images Method
#pragma mark

// -------------------------------------------------------------------------------
// pathForCacheFolder:

+ (NSString *)pathForCacheFolder:(NSString *)inFolderName inUserId:(NSString *)userId {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:inFolderName];

	NSError *error;

	if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];

	// Set path for cache folder
	NSString *imagePath = [dirPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", userId]];

	// Return path
	return imagePath;
}

// -------------------------------------------------------------------------------
// removeImage:

+ (void)removeImage:(NSString *)folderName inUser:(NSString *)inId {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *imagePath = [self pathForCacheFolder:folderName inUserId:inId];

	NSError *error;
	BOOL success = [fileManager removeItemAtPath:imagePath error:&error];
	if (success) {
		// Post Notification
		[[NSNotificationCenter defaultCenter]postNotificationName:kKeyNotificationProfileImageCached object:nil];
	}
	else {
	}
}

#pragma mark -
#pragma mark - Scale image in new size
#pragma mark -

// -------------------------------------------------------------------------------
// scaleImage toSize:

+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize {
	float width = newSize.width;
	float height = newSize.height;

	UIGraphicsBeginImageContext(newSize);
	CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);

	float widthRatio = 0.0;
	float heightRatio = 0.0;
	if (image.size.width > width) {
		widthRatio = image.size.width / width;
	}
	else if (image.size.height > height) {
		heightRatio = image.size.height / height;
	}
	float divisor = widthRatio > heightRatio ? widthRatio : heightRatio;

	if (image.size.width > width) {
		width = image.size.width / divisor;
		rect.size.width  = width;
	}
	if (image.size.height > height) {
		height = image.size.height / divisor;
		rect.size.height = height;
	}

	[image drawInRect:rect];

	UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	// Return new image
	return smallImage;
}

#pragma mark
#pragma mark - Bearing Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------
// bearingToLocationFromCoordinate:

+ (NSInteger)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc
                                     toCoordinate:(CLLocationCoordinate2D)toLoc {
	NSInteger directionValue = 0;
	float fLat = degreesToRadians(fromLoc.latitude);
	float fLng = degreesToRadians(fromLoc.longitude);
	float tLat = degreesToRadians(toLoc.latitude);
	float tLng = degreesToRadians(toLoc.longitude);

	float degree = radiandsToDegrees(atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng)));

    NSLog(@"degree before process = %f", degree);
    
    if (degree < 0) {
        degree = degree + 360;
    }
    
//    NSLog(@"fLat = %f", fLat);
//    NSLog(@"fLong = %f", fLng);
//    NSLog(@"tLat = %f", tLat);
//    NSLog(@"tLong = %f", tLng);
//    NSLog(@" tolat = %f & toLong = %f, degree = %f ", toLoc.latitude, toLoc.longitude ,degree);
    
    if ((degree >= 0 && degree < 22.5) || (degree >= 337.5 && degree <= 360)) {
        directionValue = 0;
    }
    else if (degree >= 22.5 && degree < 67.5) {
        directionValue = 1;
    }
    else if (degree >= 67.5 && degree < 112.5) {
        directionValue = 2;
    }
    else if (degree >= 112.5 && degree < 157.5) {
        directionValue = 3;
    }
    else if (degree >= 157.5 && degree < 202.5) {
        directionValue = 4;
    }
    else if (degree >= 202.5 && degree < 245.5) {
        directionValue = 5;
    }
    else if (degree >= 245.5 && degree < 292.5) {
        directionValue = 6;
    }
    else if (degree >= 292.5 && degree < 337.5) {
        directionValue = 7;
    }

    /*
	if ((degree >= 0 && degree < 45) || degree==360) {
		directionValue = 0;
	}
	else if (degree >= 45 && degree < 90) {
		directionValue = 4;
	}
	else if (degree >= 90 && degree < 135) {
		directionValue = 1;
	}
	else if (degree >= 135 && degree < 180) {
		directionValue = 5;
	}
	else if (degree >= 180 && degree < 225) {
		directionValue = 2;
	}
	else if (degree >= 225 && degree < 270) {
		directionValue = 6;
	}
	else if (degree >= 270 && degree < 315) {
		directionValue = 3;
	}
	else if (degree >= 315 && degree < 360) {
		directionValue = 7;
	}*/

	// Return
	return directionValue;
}

#pragma mark
#pragma mark - Add event to calendar Methods
#pragma mark

// --------------------------------------------------------------------------------
// createPlistPath

+ (NSString *)createPlistPath:(NSString *)fileName {
	NSString *plistSubPath = [NSString stringWithFormat:@"%@.%@", fileName, @"plist"];

	NSString *fullPath = [self directoryPath];
	fullPath = [fullPath stringByAppendingPathComponent:plistSubPath];

	// Return
	return fullPath;
}

// --------------------------------------------------------------------------------
// directoryPath

+ (NSString *)directoryPath {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:@"Event"];

	NSError *error;

	if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];

	// Return
	return dirPath;
}

// -----------------------------------------------------------------------------------------------
// Add Event to calender:

+(void)saveEventToCalender:(NSDictionary *)eventdetails
{
    if (![self isEventExist:[eventdetails objectForKey:kKeyId]])
    {
        EKEventStore *store = [EKEventStore new];
        [store requestAccessToEntityType:EKEntityTypeEvent completion: ^(BOOL granted, NSError *error) {
            if (!granted) {
                return;
            }
            //EKEvent *event = [self findEventWithTitle:[eventInfoDict objectForKey:kKeyEventName] inEventStore:store];
            
            NSString *dateString =[NSString stringWithFormat:@"%@ %@",[eventdetails valueForKey:kKeyEvent_Date],[eventdetails valueForKey:kKeyEvent_Time]];
            
            
            NSDate *eventDate =[NSDate  getEventDateFromString:dateString];
            eventDate = [eventDate dateByAddingTimeInterval:-330*60];
            
            EKEvent *event = [EKEvent eventWithEventStore:store];
            event.title = [eventdetails objectForKey:kKeyName];
            event.location = [eventdetails valueForKey:kKeyAddress];
            event.startDate = eventDate;
            event.endDate = [event.startDate dateByAddingTimeInterval:60 * 60];
            event.calendar = [store defaultCalendarForNewEvents];
            EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:-60*60];
            [event addAlarm:alarm];
            
            NSError *err = nil;
            [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
            NSString *savedEventId = event.eventIdentifier;
            
            // Save it to plist so we can access the status
            NSMutableDictionary *eventInfo=[NSMutableDictionary dictionaryWithDictionary:eventdetails];
            [eventInfo setValue:savedEventId forKey:kKeyEventIdentifier];
            [eventInfo setValue:@"60" forKey:kKeyReminder];
            [self saveEvent:eventInfo];
            
        }];
    }else
    {
        
    }

}
// --------------------------------------------------------------------------------
// isEventExist:

+ (BOOL)isEventExist:(NSString *)inEventId {
	BOOL found = NO;

	NSString *userPlistPath = [self createPlistPath:@"RSPV_STATE"];

	NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *eventsIDArr;

	if ([fileManager fileExistsAtPath:userPlistPath]) {
		eventsIDArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
	}
	else {
		// If the file doesn’t exist, create an empty dictionary
		eventsIDArr = [[NSMutableArray alloc] init];
	}
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, inEventId];
    NSArray *dotsFilterArr = [eventsIDArr filteredArrayUsingPredicate:predicate];
    
    if ([dotsFilterArr count] > 0) {
       found = YES;
    }
//	if ([eventsIDArr containsObject:inEventId]) {
//		found = YES;
//	}

	// Get event count
	return found;
}

// --------------------------------------------------------------------------------
// saveEvents:

+ (NSArray *)saveEvent:(NSDictionary *)inEventdetail {
	NSString *userPlistPath = [self createPlistPath:@"RSPV_STATE"];

	NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *eventsIDArr;

	if ([fileManager fileExistsAtPath:userPlistPath]) {
		eventsIDArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
	}
	else {
		// If the file doesn’t exist, create an empty dictionary
		eventsIDArr = [[NSMutableArray alloc] init];
	}
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [inEventdetail valueForKey:kKeyId]];
    NSArray *filterArr = [eventsIDArr filteredArrayUsingPredicate:predicate];
    
    NSMutableDictionary *eventInfo=[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",[inEventdetail valueForKey:kKeyId]],kKeyId,@"60",kKeyReminder,[inEventdetail valueForKey:kKeyEventIdentifier],kKeyEventIdentifier, nil];
    
    if ([filterArr count] > 0) {
        
        [self updateReminderForEvent:eventInfo];
    }else
    {
        // Add object to plist
        [eventsIDArr addObject:eventInfo];
        
        // Write file to path
        [eventsIDArr writeToFile:userPlistPath atomically:YES];
    }
    
	// Get eventID array
	return eventsIDArr;
}

// --------------------------------------------------------------------------------
// updateEventReminder:

+(void)updateReminderForEvent:(NSDictionary *)inEventdetail
{
    NSString *userPlistPath = [SRModalClass createPlistPath:@"RSPV_STATE"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *eventsIDArr;
    
    if ([fileManager fileExistsAtPath:userPlistPath]) {
        eventsIDArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
    }
    else {
        // If the file doesn’t exist, create an empty dictionary
        eventsIDArr = [[NSMutableArray alloc] init];
    }
    
    [eventsIDArr enumerateObjectsUsingBlock:
    ^(id   obj, NSUInteger idx, BOOL *  stop) {
      
        if ([[obj valueForKey:kKeyId] isEqualToString:[inEventdetail valueForKey:kKeyId]])
        {
            EKEventStore *store = [[EKEventStore alloc] init];
            EKEvent *event = [store eventWithIdentifier:[obj valueForKey:kKeyEventIdentifier]];
            if (event)
            {
                EKAlarm *alarm = [EKAlarm alarmWithRelativeOffset:-60*[[inEventdetail valueForKey:kKeyReminder] intValue]];
                [event addAlarm:alarm];
                [store saveEvent:event span:EKSpanThisEvent commit:YES error:nil];
            }
            
            [obj setValue:[NSString stringWithFormat:@"%@",[inEventdetail valueForKey:kKeyReminder]] forKey:kKeyReminder];
        }
        
    }];
    
    // Write file to path
    [eventsIDArr writeToFile:userPlistPath atomically:YES];
    
}

// --------------------------------------------------------------------------------
// getStoredReminder:

+(int)getStoredReminder:(NSString *)inEventId
{
    NSString *userPlistPath = [SRModalClass createPlistPath:@"RSPV_STATE"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *eventsIDArr;
    
    if ([fileManager fileExistsAtPath:userPlistPath]) {
        eventsIDArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
    }
    else {
        // If the file doesn’t exist, create an empty dictionary
        eventsIDArr = [[NSMutableArray alloc] init];
    }
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, inEventId];
        NSArray *filterArr = [eventsIDArr filteredArrayUsingPredicate:predicate];
    
        NSMutableDictionary *eventInfo;
    
        if ([filterArr count] > 0) {
            eventInfo=[NSMutableDictionary dictionaryWithDictionary:[filterArr objectAtIndex:0]];
            
            return [[eventInfo valueForKey:kKeyReminder] intValue];
        }else
        {
            return 0;
        }
}

// --------------------------------------------------------------------------------
// Check email address is valid:
+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; 
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
// --------------------------------------------------------------------------------
//return Date FromDate:
+ (NSString *)returnDateFromDate:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *mainDateDate = [dateFormatter dateFromString:date];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringDate = [dateFormatter stringFromDate:mainDateDate];
    return stringDate;
}

// --------------------------------------------------------------------------------
// return Time FromDate:
+ (NSString *)returnTimeFromDate:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *mainDateDate = [dateFormatter dateFromString:date];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *stringTime = [dateFormatter stringFromDate:mainDateDate];
    return stringTime;
}
// --------------------------------------------------------------------------------
// return string FromDate in MM/DD/YYYY format:

+(NSString *)returnStringInMMDDYYYY:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)returnStringInYYYYMMDD:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:date];
}
// --------------------------------------------------------------------------------
// return date FromString in MM/DD/YYYY format:
+(NSDate *)returnDateInMMDDYYYY:(NSString *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [formatter setTimeZone:utcTimeZone];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    return [formatter dateFromString:date];
}
// --------------------------------------------------------------------------------
// return date FromString in YYYY/MM/DD format:
+(NSDate *)returnDateInYYYYMMDD:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    return [dateFormatter dateFromString:date];
}
// --------------------------------------------------------------------------------
// return date FromString in YYYY/MM/DD HH:mm:ss format:
+(NSDate *)returnFullDate:(NSString *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    return [dateFormatter dateFromString:date];
}

// --------------------------------------------------------------------------------
// RemoveSpecialCharacters:
+(NSString *) RemoveSpecialCharacters:(NSString *)phoneNumber{
    NSMutableString *result = [NSMutableString stringWithCapacity:phoneNumber.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:phoneNumber];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO)
    {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer])
        {
            [result appendString:buffer];
        }
        else
        {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    
    return result;
}

// --------------------------------------------------------------------------------
// Get new distance:
+ (CLLocationCoordinate2D)translateCoord:(CLLocationCoordinate2D)coord MetersLat:(double)metersLat MetersLong:(double)metersLong{
    
    CLLocationCoordinate2D tempCoord;
    
    MKCoordinateRegion tempRegion = MKCoordinateRegionMakeWithDistance(coord, metersLat, metersLong);
    MKCoordinateSpan tempSpan = tempRegion.span;
    
    tempCoord.latitude = coord.latitude + tempSpan.latitudeDelta;
    tempCoord.longitude = coord.longitude + tempSpan.longitudeDelta;
    
    return tempCoord;
    
}

// -----------------------------------------------------------------------------------
// formatPhoneNumber:

+ (NSString *)formatPhoneNumber:(NSString *)simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if (simpleNumber.length == 0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if (simpleNumber.length > 13) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:13];
    }
    
    if (deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if (simpleNumber.length < 7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)" withString:@"($1) $2" options:NSRegularExpressionSearch range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)" withString:@"($1) $2-$3" options:NSRegularExpressionSearch range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}
// --------------------------------------------------------------------------------
// numberValidation:

+ (BOOL)numberValidation:(NSString *)string {
    // Validate string
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890-"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    // Return
    return ([string isEqualToString:filtered]);
}
// --------------------------------------------------------------------------------
// sortMembersArray:
+(NSMutableArray *)sortMembersArray:(NSMutableArray *)array
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:kKeyFirstName ascending:YES];
    [array sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return  array;
}
// --------------------------------------------------------------------------------
// timeLeftSinceDate:
+(NSString*) timeLeftSinceDate: (NSDate *) dateT {
    
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger months;
    NSString *durationString = @"";
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitMonth  fromDate: [NSDate date] toDate: dateT options: 0];
    months = [components month];
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    
    if (months > 0) {
        
        if (months > 1) {
            durationString = [NSString stringWithFormat:@"%ld months", (long)months];
        }
        else {
            durationString = [NSString stringWithFormat:@"%ld month", (long)months];
        }
        return durationString;
    }
    
    if (days >= 7) {
        if (days >= 14) {
            durationString = [NSString stringWithFormat:@"%ld weeks", (long)2];
        }
        else if (days >= 21) {
            durationString = [NSString stringWithFormat:@"%ld weeks", (long)3];
        }
        else if (days >= 28) {
            durationString = [NSString stringWithFormat:@"%ld weeks", (long)4];
        }
        else {
            durationString = [NSString stringWithFormat:@"%ld week", (long)1];
        }
        return durationString;
    }
    
    if (days > 0) {
        
        if (days > 1) {
            durationString = [NSString stringWithFormat:@"%ld days", (long)days];
        }
        else {
            durationString = [NSString stringWithFormat:@"%ld day", (long)days];
        }
        return durationString;
    }
    
    if (hour > 0) {
        
        if (hour > 1) {
            durationString = [NSString stringWithFormat:@"%ld hours", (long)hour];
        }
        else {
            durationString = [NSString stringWithFormat:@"%ld hour", (long)hour];
        }
        return durationString;
    }
    
    if (minutes > 0) {
        
        if (minutes > 1) {
            durationString = [NSString stringWithFormat:@"%ld minutes", (long)minutes];
        }
        else {
            durationString = [NSString stringWithFormat:@"%ld minute", (long)minutes];
        }
        return durationString;
    }
    
    return durationString;;
}
//
//-------------------------------------------------------
//add location offset
+(NSDictionary *)addLocationOffset : (NSDictionary *)dict
{
    // Set distance offset for user location
    float distanceFilter;
    //KiloMeters to miles
    distanceFilter = [[dict objectForKey:kkeyDistanceAccuracy]floatValue];
    distanceFilter = distanceFilter * 0.3048 ;
  
    CLLocationCoordinate2D userCoordinate = CLLocationCoordinate2DMake([[dict objectForKey:kKeyLattitude]floatValue], [[dict objectForKey:kKeyLongitude]floatValue]);
    CLLocationCoordinate2D coordinate = [SRModalClass translateCoord:userCoordinate MetersLat:0.00 MetersLong:distanceFilter];
    
    [dict setValue:[NSString stringWithFormat:@"%f",coordinate.latitude] forKey:kKeyLattitude];
    [dict setValue:[NSString stringWithFormat:@"%f",coordinate.longitude] forKey:kKeyLongitude];
    
    /* double distance;
     if ([[[updatedDict objectForKey:kKeySetting]objectForKey:kkeyMeasurementUnit] boolValue] == TRUE) {
     // Convert distance filter into miles
     distance = [[updatedDict objectForKey:kKeyDistance] doubleValue] + distanceFilter /1609.34;
     }
     else{
     // into kilometers
     distance = [[updatedDict objectForKey:kKeyDistance] doubleValue] + distanceFilter /1000;
     }
     [updatedDict setValue:[NSNumber numberWithDouble:distance] forKey:kKeyDistance];
     NSString *urlStr =[NSString stringWithFormat:@"%@%@=%f&lon=%f",kNominatimServer,kAddressApi,coordinate.latitude,coordinate.longitude];
     NSURL *url = [NSURL URLWithString:urlStr];
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
     [request setHTTPMethod:@"GET"];
     NSError *error = nil;
     NSURLResponse *response = nil;
     NSData *data = [NSURLConnection sendSynchronousRequest:request
     returningResponse:&response
     error:&error];
     if (data)
     {
     if (!error)
     {
     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
     Boolean isAddressPresent=true;
     for (NSString *keyStr in json)
     {
     if ([keyStr isEqualToString:@"error"])
     {
     isAddressPresent=false;
     }
     }
     if (isAddressPresent)
     [[updatedDict objectForKey:kKeyLocation] setValue:[json valueForKey:@"display_name"] forKey:kKeyLiveAddress];
     }
     }*/
    return  dict;
}

// --------------------------------------------------------------------------------
// getDBPath:
NSString* getDBPath()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"trackingDB.sqlite"];
}

void addTextInSignificantTxt(NSString *savedString)
{
   // return;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss a"];
    NSString *dateStr = [dateFormatter stringFromDate:[NSDate date]];
    
    savedString = [NSString stringWithFormat:@"\n\n\nDate:%@\n%@",dateStr,savedString];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:@"Significant.txt"];
    //NSLog(@"Significant.txt Path: %@",documentTXTPath);
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath];
    
    if(!fileExists)
    {
        NSError *error;
        
        BOOL isSuccess = [savedString writeToFile:documentTXTPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
        
        if(isSuccess)
            NSLog(@"Successfully written model to file");
        else
            NSLog(@"Error in writing a file: %@",error);
    }
    else{
        
        NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:documentTXTPath];
        [myHandle seekToEndOfFile];
        [myHandle writeData:[savedString dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    //    NSString *savedString = textview.text;
    
    
}


BOOL isNullObject(id object){
    
    if (((NSNull *) object == [NSNull null]) || (object == nil) ) {
        return YES;
    }
    
    if ([object isKindOfClass:[NSString class]]) {
        
        if ([(NSString *)object isEqualToString:@"<null>"] || [(NSString *)object isEqualToString:@"null"]) {
            
            return YES;
        }
        
        NSString *string = (NSString *)object;
        
        string = [string stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([string isEqualToString:@""]) {
            return YES;
        }
        
    }else if ([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]]) {
        
        if (![object count]) {
            
            return YES;
            
        }
        
    }
    
    return NO;
    
}
NSError *getNSErrorObjectWithDescription(NSString *description,NSInteger code){
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:description forKey:NSLocalizedDescriptionKey];
    
    NSError *error = [NSError errorWithDomain:@"com.leosys.net" code:code userInfo:dict];
    
    return error;
    
}

NSString *stringFromResponseObj(id responseObject){
    
    NSString *str = @"";
    
    NSError *parseError;
    
    str = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&parseError];
    
    return str;
}

@end
