//
//  TrackingUserDetails.h
//  Serendipity
//
//  Created by Abbas Mulani on 09/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrackingLocationClass.h"

@interface TrackingUserDetails : NSObject

@property (nonatomic,strong) NSDictionary *dataDict;
@property (nonatomic,strong) NSString *username;
@property BOOL pushNotificationStatus;
@property (nonatomic,strong) UIImage *userImage;
@property (nonatomic,strong) NSMutableArray <TrackingLocationClass *>*locationObjects;

@end
