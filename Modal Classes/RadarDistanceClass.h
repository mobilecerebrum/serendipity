//
//  RadarDistanceClass.h
//  Serendipity
//
//  Created by Abbas Mulani on 10/02/17.
//  Copyright © 2017 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RadarDistanceClass : NSObject

@property (nonatomic,strong) NSString *lat;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *dataId;
@property (strong, nonatomic) CLLocation *myLocation;
@property (nonatomic,strong) NSDictionary *userDict;

+(RadarDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation;

@end
