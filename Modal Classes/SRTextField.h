//
//  SRTextField.h
//  Serendipity
//
//  Created by Abbas Mulani on 04/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRTextField : UITextField

@property (nonatomic,strong) NSIndexPath *indexPath;

@end
