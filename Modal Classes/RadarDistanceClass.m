//
//  RadarDistanceClass.m
//  Serendipity
//
//  Created by Abbas Mulani on 10/02/17.
//  Copyright © 2017 Pragati Dubey. All rights reserved.
//

#import "RadarDistanceClass.h"

@implementation RadarDistanceClass

+(RadarDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation{
    
    RadarDistanceClass *obj = [[RadarDistanceClass alloc] init];
    obj.dataId = [userDict objectForKey:kKeyId];
    NSDictionary *locationDict = [userDict objectForKey:kKeyLocation];
    obj.lat = [locationDict objectForKey:kKeyLattitude];
    obj.longitude = [locationDict objectForKey:kKeyRadarLong];
    obj.distance = [userDict objectForKey:kKeyDistance];
    obj.myLocation = myLocation;
    obj.userDict = userDict;
    
    return obj;
}

@end
