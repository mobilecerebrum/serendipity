//
//  SRSwitch.h
//  Serendipity
//
//  Created by Abbas Mulani on 09/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRSwitch : UISwitch

@property (nonatomic,strong) NSIndexPath *indexPath;


@end
