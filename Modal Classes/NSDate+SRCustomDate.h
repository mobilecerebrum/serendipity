//
//  NSDate+SRCustomDate.h
//  Serendipity
//
//  Created by Dhanraj Bhandari on 15/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (SRCustomDate)

+(NSString *)getChatTimeStamp:(NSString *)dateStr;
+(NSString *)makeChatTimeStamp:(NSString *)dateStr;
+(NSString *)makeChatTimeStamp;

+(NSDate *)getEventDateFromString:(NSString *)dateStr;
+ (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate;

+(NSString *)dateTimeAgo:(NSString *)dateStr;
+(NSString *)chatDay:(NSString *)dateStr;
+(NSString *)chatTime:(NSString *)dateStr;
//- (NSString *)dateTimeUntilNow;
@end
