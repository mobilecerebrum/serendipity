#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "Constant.h"
#import <EventKit/EventKit.h>
#import "NSDate+SRCustomDate.h"
#import "SRServerConnection.h"
#import "Serendipity-Swift.h"

#define kKeyImageWidth 420.0
#define kKeyImageHeight 800.0

@interface SRModalClass : NSObject

#pragma mark
#pragma mark - Email address validation Method
#pragma mark
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;

// Format phone number
+ (BOOL)numberValidation:(NSString *)string;
+(NSString *) RemoveSpecialCharacters :(NSString *)phoneNumber;
+ (NSString *)formatPhoneNumber:(NSString *)simpleNumber deleteLastChar:(BOOL)deleteLastChar;

#pragma mark
#pragma mark - TabBar Customize Method
#pragma mark

+ (void)showTabBar:(UITabBarController *)tabbarcontroller;
+ (void)hideTabBar:(UITabBarController *)tabbarcontroller;

#pragma mark
#pragma mark - Navigation Customize Method
#pragma mark

+ (void)setNavTitle:(NSString *)inTitleStr
      forViewNavCon:(UINavigationItem *)inNavItem
        forFontSize:(CGFloat)inSize;
+ (UIButton *)setLeftNavBarItem:(NSString *)inTitleStr
                       barImage:(UIImage *)inImage
                  forViewNavCon:(UIViewController *)inViewCon
                         offset:(CGFloat)inOffset;
+ (UIButton *)setRightNavBarItem:(NSString *)inTitleStr
                        barImage:(UIImage *)inImage
                   forViewNavCon:(UIViewController *)inViewCon offset:(CGFloat)inOffset;

#pragma mark
#pragma mark - Gredient Method
#pragma mark

+ (void)applyGredientColor:(UIColor *)topColor
               centerColor:(UIColor *)inCenterColor
               bottomColor:(UIColor *)inBottomColor
                   forView:(UIImageView *)inImageView;
+ (UIImage *)createImageWith:(UIColor *)color;

#pragma mark
#pragma mark - Custom Method
#pragma mark

+ (CLLocationCoordinate2D)geoCodeUsingAddress:(NSString *)address;
+ (CLLocationCoordinate2D)translateCoord:(CLLocationCoordinate2D)coord MetersLat:(double)metersLat MetersLong:(double)metersLong;
+ (NSDictionary *)removeNullValuesFromDict:(NSDictionary *)inDict;
+ (void)showSuccess:(NSString*)message;
+ (void)showAlert:(NSString *)inErrorStr;
+ (void)showAlert:(NSString *)inMessage fromController:(UIViewController*)controller;
+ (void)showAlertWithTitle:(NSString *)inErrorStr  alertTitle:(NSString*)title;
+ (void)setUserImage:(NSDictionary *)inDict
              inSize:(NSString *)size
         applyToView:(UIImageView *)inView;
+ (UIImage *)imageFromUIView:(UIView *)view;
+ (UIView *)layerWithMultiColorBorder:(NSArray *)colors forView:(UIView*)view;

#pragma mark
#pragma mark - Get Image URL
#pragma mark
+(NSURL *)getProfileImageURL:(NSString *)fileName;

#pragma mark
#pragma mark - Cache Images Method
#pragma mark

+ (void)removeImage:(NSString *)folderName inUser:(NSString *)inId;

#pragma mark
#pragma mark - Scales Method
#pragma mark

+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize;

#pragma mark
#pragma mark - Beraing Method
#pragma mark

+ (NSInteger)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc
                                  toCoordinate:(CLLocationCoordinate2D)toLoc;

#pragma mark
#pragma mark - Add event to calendar Methods
#pragma mark

+ (NSString *)createPlistPath:(NSString *)fileName;
+ (NSString *)directoryPath;

+ (void)saveEventToCalender:(NSDictionary *)eventdetails;
+ (BOOL)isEventExist:(NSString *)inEventId;
+ (NSArray *)saveEvent:(NSDictionary *)inEventdetail;
+ (void)updateReminderForEvent:(NSDictionary *)inEventdetail;
+ (int)getStoredReminder:(NSString *)inEventId;

#pragma mark
#pragma mark - Date formats
#pragma mark
+ (NSString *)returnDateFromDate:(NSString *)date;
+ (NSString *)returnTimeFromDate:(NSString *)date;
+ (NSString *)returnStringInMMDDYYYY:(NSDate *)date;
+ (NSDate *)returnDateInMMDDYYYY:(NSString *)date;
+ (NSDate *)returnDateInYYYYMMDD:(NSString *)date;
+(NSString *)returnStringInYYYYMMDD:(NSDate *)date;
+(NSDate *)returnFullDate:(NSString *)date;
+(NSString*) timeLeftSinceDate: (NSDate *) dateT;

//add location offset
+(NSDictionary *)addLocationOffset : (NSDictionary *)dict;

// sorted array
+ (NSMutableArray *)sortMembersArray:(NSMutableArray *)array;


NSString* getDBPath();
BOOL isNullObject(id object);
NSError *getNSErrorObjectWithDescription(NSString *description,NSInteger code);
NSString *stringFromResponseObj(id responseObject);

void addTextInSignificantTxt(NSString *savedString);

@end
