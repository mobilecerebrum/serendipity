#import "UIBarButtonItem+SRNegativeSpacer.h"

@implementation UIBarButtonItem (SRNegativeSpacer)

+ (UIBarButtonItem *)negativeSpacerWithWidth:(NSInteger)width {
	UIBarButtonItem *item = [[UIBarButtonItem alloc]
	                         initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
	                                              target:nil
	                                              action:nil];
	item.width = (width >= 0 ? -width : width);
	
    // Return item
    return item;
}

@end
