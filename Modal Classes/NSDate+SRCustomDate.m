//
//  NSDate+SRCustomDate.m
//  Serendipity
//
//  Created by Dhanraj Bhandari on 15/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "NSDate+SRCustomDate.h"

@implementation NSDate (SRCustomDate)

//-----------------------------------------------------------------------
//Make a chat timestamp

+(NSString *)getChatTimeStamp:(NSString *)dateStr {
    NSString *timeStamp;
   
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    
    NSDate *currentDate = [dateFormatter dateFromString:dateStr];
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormatter.dateFormat = @"MMM d,YYYY h:mm a";
    //dateFormatter.dateFormat = @"hh:mm a";
    timeStamp = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:currentDate]];
    return timeStamp;
}
+(NSString *)makeChatTimeStamp:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([dateStr doubleValue] / 1000.0)];
    NSString *datetime = [dateFormatter stringFromDate:date];
    return datetime;
}
+(NSString *)makeChatTimeStamp
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [NSDate date];
    NSString *datetime = [dateFormatter stringFromDate:date];
    return datetime;
}

+(NSDate *)getEventDateFromString:(NSString *)dateStr
{
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *eventDate =[formatter dateFromString:dateStr];
    return eventDate;
}


+ (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}



// Similar to timeAgo, but only returns "
+ (NSString *)dateTimeAgo:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *chatDate = [NSDate dateWithTimeIntervalSince1970:([dateStr doubleValue] / 1000.0)];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate * now = [NSDate date];
    NSDateComponents *components = [calendar components:
                                    NSCalendarUnitYear|
                                    NSCalendarUnitMonth|
                                    NSCalendarUnitWeekOfYear|
                                    NSCalendarUnitDay|
                                    NSCalendarUnitHour|
                                    NSCalendarUnitMinute|
                                    NSCalendarUnitSecond
                                               fromDate:chatDate
                                                 toDate:now
                                                options:0];
    
    if (components.year >= 1)
    {
        if (components.year == 1)
        {
            return NSLocalizedString(@"1 year ago",nil);
        }
        return [NSString stringWithFormat:@" %ld years ago",(long)components.year];
    }
    else if (components.month >= 1)
    {
        if (components.month == 1)
        {
            return NSLocalizedString(@"1 month ago",nil);
        }
        return [NSString stringWithFormat:@"%ld months ago",(long)components.month];
    }
    else if (components.weekOfYear >= 1)
    {
        if (components.weekOfYear == 1)
        {
            return NSLocalizedString(@"1 week ago",nil);
        }
        return [NSString stringWithFormat:@"%ld weeks ago",(long)components.weekOfYear];
    }
    else if (components.day >= 1)    // up to 6 days ago
    {
        if (components.day == 1)
        {
            return NSLocalizedString(@"Yesterday",nil);
        }
        return [NSString stringWithFormat:@"%ld days ago",(long)components.day];
    }
    else if (components.hour >= 1)   // up to 23 hours ago
    {
        if (components.hour == 1)
        {
            return NSLocalizedString(@"1 hour ago",nil);
        }
        return [NSString stringWithFormat:@"%ld hours ago",(long)components.hour];
    }
    else if (components.minute >= 1) // up to 59 minutes ago
    {
        if (components.minute == 1)
        {
            return NSLocalizedString(@"1 minute ago",nil);
        }
        return [NSString stringWithFormat:@"%ld minutes ago",(long)components.minute];
    }
    else if (components.second < 5)
    {
        return NSLocalizedString(@"Just now",nil);
    }
    
    // between 5 and 59 seconds ago
    return [NSString stringWithFormat:@"%ld secs ago",(long)components.second];
}

+(NSString *)chatDay:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:utcTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *chatDate = [NSDate dateWithTimeIntervalSince1970:([dateStr doubleValue] / 1000.0)];
    NSDate * now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSInteger startDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
                                             inUnit:NSCalendarUnitEra
                                            forDate:chatDate];
    NSInteger endDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
                                           inUnit:NSCalendarUnitEra
                                          forDate:now];
    
    NSInteger diffDays = endDay - startDay;
    if (diffDays == 0) // today!
        return @"Today";
    else
        return @"Not Today";
}

+(NSString *)chatTime:(NSString *)dateStr
{
    NSString *timeStamp;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
   // NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
   // [dateFormatter setTimeZone:utcTimeZone];
    
    NSDate *currentDate = [dateFormatter dateFromString:dateStr];
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    dateFormatter.dateFormat = @"MMM dd, YYYY h:mm a";
    timeStamp = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:currentDate]];
    return timeStamp;
}

//- (NSString *)dateTimeUntilNow
//{
//    NSDate * now = [NSDate date];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    
//    NSDateComponents *components = [calendar components:NSCalendarUnitHour fromDate:self toDate:now options:0];
//    
//    
//    if (components.hour >= 6) // if more than 6 hours ago, change precision
//    {
//        NSInteger startDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
//                                                 inUnit:NSCalendarUnitEra
//                                                forDate:self];
//        NSInteger endDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
//                                               inUnit:NSCalendarUnitEra
//                                              forDate:now];
//        
//        NSInteger diffDays = endDay - startDay;
//        if (diffDays == 0) // today!
//        {
//            NSDateComponents * startHourComponent = [calendar components:NSCalendarUnitHour fromDate:self];
//            NSDateComponents * endHourComponent = [calendar components:NSCalendarUnitHour fromDate:self];
//            if (startHourComponent.hour < 12 &&
//                endHourComponent.hour > 12)
//            {
//                return NSLocalizedString(@"This morning",nil);
//            }
//            else if (startHourComponent.hour >= 12 &&
//                     startHourComponent.hour < 18 &&
//                     endHourComponent.hour >= 18)
//            {
//                return NSLocalizedString(@"This afternoon",nil);
//            }
//            return NSLocalizedString(@"Today",nil);
//        }
//        else if (diffDays == 1)
//        {
//            return NSLocalizedString(@"Yesterday",nil);
//        }
//        else
//        {
//            NSInteger startWeek = [calendar ordinalityOfUnit:NSCalendarUnitWeekOfYear
//                                                      inUnit:NSCalendarUnitEra
//                                                     forDate:self];
//            NSInteger endWeek = [calendar ordinalityOfUnit:NSCalendarUnitWeekOfYear
//                                                    inUnit:NSCalendarUnitEra
//                                                   forDate:now];
//            NSInteger diffWeeks = endWeek - startWeek;
//            if (diffWeeks == 0)
//            {
//                return NSLocalizedString(@"This week",nil);
//            }
//            else if (diffWeeks == 1)
//            {
//                return NSLocalizedString(@"Last week",nil);
//            }
//            else
//            {
//                NSInteger startMonth = [calendar ordinalityOfUnit:NSCalendarUnitMonth
//                                                           inUnit:NSCalendarUnitEra
//                                                          forDate:self];
//                NSInteger endMonth = [calendar ordinalityOfUnit:NSCalendarUnitMonth
//                                                         inUnit:NSCalendarUnitEra
//                                                        forDate:now];
//                NSInteger diffMonths = endMonth - startMonth;
//                if (diffMonths == 0)
//                {
//                    return NSLocalizedString(@"This month",nil);
//                }
//                else if (diffMonths == 1)
//                {
//                    return NSLocalizedString(@"Last month",nil);
//                }
//                else
//                {
//                    NSInteger startYear = [calendar ordinalityOfUnit:NSCalendarUnitYear
//                                                              inUnit:NSCalendarUnitEra
//                                                             forDate:self];
//                    NSInteger endYear = [calendar ordinalityOfUnit:NSCalendarUnitYear
//                                                            inUnit:NSCalendarUnitEra
//                                                           forDate:now];
//                    NSInteger diffYears = endYear - startYear;
//                    if (diffYears == 0)
//                    {
//                        return NSLocalizedString(@"This year",nil);
//                    }
//                    else if (diffYears == 1)
//                    {
//                        return NSLocalizedString(@"Last year",nil);
//                    }
//                }
//            }
//        }
//    }
//   
//    // anything else uses "time ago" precision
//    return [self dateTimeAgo];
//}


@end
