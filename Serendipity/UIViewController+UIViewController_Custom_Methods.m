//
//  UIViewController+UIViewController_Custom_Methods.m
//  Serendipity
//
//  Copyright © 2020 Neha Dubey. All rights reserved.
//

#import "UIViewController+UIViewController_Custom_Methods.h"

@implementation UIViewController (UIViewController_Custom_Methods)

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:(@"Close") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //NSLog(@"ALert Action");
        }]];
        [self presentViewController:alert animated:true completion:^{
            
        }];
    });
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message withCompletionBlok:(void (^)(void))block {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:(@"Close") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            block();
        }]];
        [self presentViewController:alert animated:true completion:NULL];
    });
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonTitle:(NSString *)buttonTitle cancelButtonTitle:(NSString *)cancelButtonTitle withCompletionBlok:(void (^)(BOOL isCancelAction))block {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(YES);
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(NO);
        }]];
        
        [self presentViewController:alert animated:true completion:NULL];
    });
}

- (void)showAlertWithTitleStyleActionSheet:(NSString *)title message:(NSString *)message buttonTitle:(NSString *)firstButtonTitle :(NSString *)secondButtonTitle cancelButtonTitle:(NSString *)cancelButtonTitle withCompletionBlok:(void (^)(BOOL isCancelAction))block {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(YES);
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:firstButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(YES);
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:secondButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(YES);
        }]];
        
        [self presentViewController:alert animated:true completion:NULL];
    });
}

@end
