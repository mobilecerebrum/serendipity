//
//  HSLocalNotificationManger.m
//  Demo
//
//  Created by Hitesh Surani on 13/05/20.
//  Copyright © 2020 Christopher Scott. All rights reserved.
//

#import "HSLocalNotificationManger.h"
#import <UserNotifications/UserNotifications.h>


@implementation HSLocalNotificationManger
+ (id)sharedManager {
    static HSLocalNotificationManger *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void) triggerLocationNotification:(NSString*) title :(NSString*)subtitle{
    [self removeLocationNotification];
    
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    objNotificationContent.title = title;
    objNotificationContent.body = subtitle;
//    objNotificationContent.sound = [UNNotificationSound defaultSound];
    
    NSString* strNotificationID = [NSString stringWithFormat:@"HS_Location%d",arc4random_uniform(100000000)];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:strNotificationID
                                                                          content:objNotificationContent trigger:nil];
    /// 3. schedule localNotification
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSArray* aryNotificationID = [userDefault arrayForKey:@"HS_Location"];

    NSMutableArray* newNotificationArray = [[NSMutableArray alloc] initWithArray:aryNotificationID];
    
    [newNotificationArray addObject:strNotificationID];
    [userDefault setValue:newNotificationArray forKey:@"HS_Location"];
    
    [userDefault synchronize];

    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Local Notification succeeded");
        }
        else {
            NSLog(@"Local Notification failed");
        }
    }];
    
}

-(void) removeLocationNotification{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSArray* aryNotificationID = [userDefault arrayForKey:@"HS_Location"];
    
    if (aryNotificationID.count > 0){
        [[UNUserNotificationCenter currentNotificationCenter] removeDeliveredNotificationsWithIdentifiers:aryNotificationID];
        
        [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:aryNotificationID];

        
        [userDefault setObject:@[] forKey:@"HS_Location"];
    }
}

@end
