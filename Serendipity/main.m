
#import <UIKit/UIKit.h>
#import "SRAppDelegate.h"

#pragma Mark - Init Method

int main(int argc, char *argv[]) {
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, NSStringFromClass([SRAppDelegate class]));
	}
}
