//
//  HSLocalNotificationManger.h
//  Demo
//
//  Created by Hitesh Surani on 13/05/20.
//  Copyright © 2020 Christopher Scott. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HSLocalNotificationManger : NSObject

+ (id)sharedManager;

-(void) triggerLocationNotification:(NSString*) title :(NSString*)subtitle;
-(void) removeLocationNotification;
@end

NS_ASSUME_NONNULL_END
