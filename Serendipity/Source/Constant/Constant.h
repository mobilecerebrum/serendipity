#ifndef Serendipity_Constant_h
#define Serendipity_Constant_h

// Enum
typedef enum : NSUInteger {
	DirectionNorth = 0,
	DirectionEast,
	DirectionSouth,
	DirectionWest
} EnumDirection;

// Directions
#define kKeyDirectionNorth 0
#define kKeyDirectionNorthEast 1
#define kKeyDirectionEast 2
#define kKeyDirectionSouthEast 3
#define kKeyDirectionSouth 4
#define kKeyDirectionSoutnWest 5
#define kKeyDirectionWest 6
#define kKeyDirectionNorthWest 7

#define kDeviceId @"device_id"



// Fonts
#define kFontSharikSansBold @"Sharik Sans"
#define kFontHelveticaRegular @"HelveticaNeue"
#define kFontHelveticaMedium @"HelveticaNeue-Medium"
#define kFontHelveticaBold @"HelveticaNeue-Bold"
#define kFontHelveticaItalic @"HelveticaNeue-Italic"
#define kFontHelveticaItalicMedium @"HelveticaNeue-MediumItalic"

#define kKeyNavBarLeftBtnWidth 44.0
#define kKeyNavBarLeftBtnHeight 50.0
#define kNavBarFontSize 15.0
#define kNavBarSmallFontSize 12.0

// Folders Name
#define kKeyFolderProfileImg @"ProfileImage"

//radar constant
#define kKeyMinimumRadarMile 0.0
#define kKeyMaximumRadarMile 12500.0
#define kKeyMaximumRadarKm 20000.0
#define kKeyTempMiles_Data @"temp_miles_data"
#define kKeyMiles_Data      @"miles_data"
#define kKeyMeters_Data      @"meters_data"

// Type of notifications
#define kKeyConnectionNotification 1
#define kKeyFamilyNotification 2
#define kKeyGroupNotification 3

// Type
#define kKeyConnectionAccept 2
#define kKeyConnectionReject 3
#define kKeyConnectionBlock 4
#define kKeyConnectionBlockForNonConn 5
// Login
#define kKeyDeviceToken @"device_token"
#define kKeyDeviceType @"device_type"
#define kKeyDeviceName @"iOS"

// Sign Up
#define kKeyId @"id"
#define kKeyUser @"user"
#define kKeyCountry @"country"
#define kKeyPhoneCode @"phonecode"
#define kKeyCountryId @"country_id"
#define kKeyGender @"gender"
#define kKeyOldMobileNumber @"old_mobile_number"
#define kKeyOldCountryId @"old_country_id"



#define kKeyCountryMasterId @"country_master_id"
#define kKeyMobileNumber @"mobile_number"
#define kKeyFirstName  @"first_name"
#define kKeyLastName  @"last_name"
#define kKeyProfileImage @"profile_image"
#define kKeyImageName @"file"
#define kkeyCountryName @"nicename"
#define kkeyReview @"review"
#define kKeyIsFromFacebook @"isFromFacebook"
#define kkeyPicture @"picture"
#define kkeyData @"data"
#define kkeyUrl @"url"
#define kkeyLoginType @"login_type"
#define kkeyIsLogin @"is_login"
#define kkeySocialId @"social_id"
#define kkeyFeedback @"feedback"
#define kKeyIsFromSignUp @"isFromSignUp"
#define kKeyIsFromDegreeSeperation @"showDegreeSeperation"
#define kKeyIsFromZeroDegreeSeperation @"zeroDegreeSeperation"
#define kKeyIsFirstTimeLogin @"newUser"


//chat
#define MESSAGE @"message"
#define MESSAGE_ID @"id"
#define MESSAGE_TO_SEND @"messagetosend"
#define MESSAGE_TYPE @"message_type"
#define  DOWNLOAD_STATUS_FLAG @"downloadStatusFlag"
#define IS_OFFLINE_MESSAGE @"offlinemessage"
#define SELF @"self"
#define SENT @"sent"
#define LOCALMESSAGE_ID @"localmessageid"
#define ROOM_ID @"roomid"
#define FROM_ID @"from"
#define PUSH_CHANNEL_KEY @"push_channel"
#define MSG_FROM @"fromname"

// Code verify
#define kKeyVerifyToken @"verify_token"
#define kKeyVerifyCode @"verify_code"
#define kKeydeviceVerifyCode @"dvc_ver_code"
#define smsReferenceID @"sms_reference_id"
#define deviceReferenceID @"dvc_ref_id"

// Complete Sign Up
#define kKeyEmail @"email"
#define kKeyPassword @"password"

// Edit profile
#define kKeyGender @"gender"
#define kKeyUserWebUrl @"user_website"
#define kKeyDescription @"description"
#define kKeyDOB @"dob"
#define kKeyOccupation @"occupation"
#define kKeyAddress @"address"
#define kKeyAddressName @"name"
#define kKeyNotify @"notify"
#define kKeyNotifyTrackingOn @"notify_on"
#define kKeyLiveAddress @"live_address"
#define kKeyDirection @"direction_degree"
#define kKeySpeed @"speed"
#define kKeyoffsetLat @"offset_lat"
#define kKeyBuildVersion @"buildVersion"
#define KkeyDeviceVersion @"device"
#define kKeyoffsetLong @"offset_long"
#define kKeyoffsetAddreass @"offset_address"
#define kKeyMedia @"media"
#define kKeyOpenForDating @"open_for_dating"
#define kKeyShowAge @"show_age"
#define kKeyAboutUser @"info_about_user"

#define kKeyMediaType @"type"
#define kKeyMediaIsDefault @"is_default"
#define kKeyMediaReferenceId @"reference_id"
#define kKeyMediaImage @"image"

// Radar
#define kKeyLattitude @"lat"
#define kKeyLongitude @"long"
#define kKeyRadarLong @"lon"
#define kKeyDistance @"distance"
#define kKeyElevation @"elevation"
#define kKeyCalculatedAddress @"calculatedAddress"
#define kKeyDegree @"degree"
#define kKeyIsTracking @"is_tracking"
#define kKeyUserID @"user_id"
#define kKeyTrackerUserID @"tracker_id"
#define kKeyFavUserID @"favourite_id"
#define kKeyInvisibleUserID @"invisible_id"
#define kKeyType @"type"
#define kKeyContacTyype @"contact_type"
#define kKeyIsContact @"is_contact"
#define kkeyCurrentDate @"date"
#define kKeyIsConnected @"connected"
#define kKeyIsFamilyMember @"family_member"
#define kKeyLocation @"location"
#define kKeyLocations @"locations"
#define kKeyImageObject @"imgObject"
#define kKeyCombination @"combination"
#define kKeyNotCommon @"not_common"
#define kKeyFamily @"family"
#define kKeyIsFavourite @"is_favourite"
#define kKeyToFavourite @"to_favourite"
#define kKeyIsInvisible @"is_invisible"
#define kKeyLocationUpdateTime @"locationUpdateTime"
#define kKeyLastSeenDate @"location_captured_time"
// Add connection
#define kKeyIsConnectionDelete @"is_connection_delete"
#define kKeyConnectionID @"connection_id"
#define kKeyConnection @"connection"
#define kKeyConnectionis_imported @"is_imported"
#define kKeySetting @"setting"
#define kKeyStoredImage @"storedImage"
#define kKeySender @"sender"
#define kKeyReferenceID @"reference_id"
#define kKeyReferenceType @"reference_type_id"
#define kKeyConnectionStatus @"connection_status"
#define kKeyReferenceStatus @"reference_status"
#define kKeyReference @"reference"
// Privacy settings
#define kKeyName @"name"
#define kKeyfullName @"full_name"
#define kKeyValue @"value"
#define kKeyValueEveryone @"Everyone"
#define kKeyValueMyContacts  @"Only My Contacts"
#define kKeyValueMyConnections @"Only My Connections"
#define kKeyValueMyFavConnections  @"Only My Favourite Connections"
#define kKeyValueMyFamily  @"Only My Family"
#define kKeyValueUsersTrackMe  @"Users Tracking Me"
#define kKeyValueNobody @"Nobody"
#define kKeyProfilePhoto @"Profile Photo"
#define kKeyGoInvisible @"Go Invisible"
#define kKeyallowPings @"allow_pings"
#define kKeyPushnotification @"push_notifications"
#define kKeyNotifyOnDegrees @"notify_on_degrees"
#define kKeyNotifyOnConn @"notify_on_connection"
#define kKeyNotifyWithRadius @"notify_within_radius"
#define kKeyNotifyOn @"notify_me_on"
#define kKeyNotifyfor @"notify_me_for"
#define kKeyNotifyMeWithInRadius @"notify_me_within_radius"
#define kKeyRadius @"radius"
#define kKeyRadiusUom @"radius_uom"
#define kKeyOpenForDating @"open_for_dating"
#define kKeyIntelligentAlert @"intelligent_alerts"
#define kKeyFamilyTrack @"family_track"
#define kkeyDistanceAccuracy @"user_dist_accuracy"
#define kkeyBroadcastLocation @"broadcast_location"
#define kkeyBatteryUsage @"battery_usage"
#define kkeytracking_me @"tracking_me"
#define kkeylocation_notification @"location_notification"
#define kkeySmartBatteryUsage @"smart_battery_usage"
#define kkeyHideLastName @"is_lastname"
#define kkeyMeasurementUnit @"measurement_unit"
#define kkeyUnitMiles @"miles"
#define kkeyUnitKilometers @"km"
#define kkeyReal_time @"real_time"
// Connection
#define kKeyIsFromConnection @"isFromConnection"

// User Degree seperation
#define kKeyDegreeCombination @"degree_combination"
#define kKeyDegreeUsers @"degree_users"
#define kKeyIndex       @"index"
// Groups
#define kKeyGroupTag  @"tags"
#define kKeyGroupType @"group_type"
#define kKeyEnableTracking @"enable_tracking"
#define kKeyGroup_is @"group_status"
#define kKeyGroup_Member_Ids @"group_member_ids"
#define kKeyGroup_Admin_Ids @"group_admin_ids"
#define kKeyIsAdmin @"is_admin"
#define kKeyCreatedBy @"created_by"
#define kKeyNotificationNewGroupAdded   @"newGroup_added"

#define kKeyPhoneNumber @"Phone_Number"
#define kKeyGroupID @"group_id"
#define kKeyGroupFile @"file"
#define kKeyMemberImage @"member_image"
#define kKeyGroupMembers @"group_members"
#define kKeyGroupUsers @"users"
#define kKeyGroupList @"groups"

// Family
#define kKeyRelative @"relative"
#define kKeyIs_family @"is_family"
#define kKeyTrackFamily @"family_track"
#define kKeyRelativeId @"relative_id"
#define kKeyRelation @"relation"
#define kKeyAllowPushNotificationTrack @"allow_track"
#define kKeyFamilyLocation @"location"
#define kKeyDefault @"default"
#define kKeyInvitationStatus @"invitation_status_master_id"
#define kKeyFamilyLocationArr @"family_location"
#define kKeyFieldName @"field_name"
#define kKeyFieldValue @"field_value"
#define kKeyAllowTrackStatus @"allow_track_invitation_status"
#define kKeySenderId @"sender_id"
#define kKeyRecieverId @"receiver_id"
#define kKeyStatus @"status"

//Event
#define kKeyEventID @"event_id"
#define kkeyExisting        @"AlreadyExists"
#define kkeyNumbering       @"EventNumber"
#define kKeyEvent           @"Event"
#define kKeyEvents          @"events"
#define kKeyEvent_Data      @"event_data"
#define kKeyEvent_Date      @"event_date"
#define kKeyEvent_Time      @"event_time"
#define kKeyEvent_Type      @"event_type"
#define kKeyIs_Public       @"is_public"
#define kKeyIs_Converted    @"is_converted"
#define kKeyEvent_Status    @"event_status"
#define kKeyAttendees       @"attendees"
#define kKeyEvent_Admin_Ids @"event_admin_ids"
#define kKeyReminder        @"reminder"
#define kKeyEventCreated    @"created_at"
#define kKeyEventUpdated    @"updated_at"
#define kKeyEventIdentifier @"eventIdenfier"
#define kKeyEvent_Attendees @"event_attendees"
#define kKeyNotificationNewEventAdded   @"newEvent_added"
#define kKeySelectedGroupId   @"group_ids"
// External events
#define kKeyExtEventCategory      @"category"
#define kKeyExtEventvenuesKey     @"venues"
#define kKeyExtEventvenue         @"venue"
#define kKeyExtEventPerformerKey  @"performers"
#define kKeyExtEvent              @"event"
#define kKeyExtEventVname         @"venue_name"
#define kKeyExtEventDescription   @"description"
#define kKeyExtEventTitle         @"title"
#define kKeyExtEventID            @"id"
#define kKeyExtEventLatitude      @"latitude"
#define kKeyExtEventLongitude     @"longitude"
#define kKeyExtEventAddress       @"venue_address"
#define kKeyExtEventImage         @"image"
#define kKeyExtEventImageMedium   @"medium"
#define kKeyExtEventImageUrl      @"url"
#define kKeyExtEventCreated       @"created"
#define kKeyExtEventModified      @"modified"
#define kKeyExtEventTime          @"start_time"
#define kKeyExtEventStopTime      @"stop_time"
#define kKeyExternalEvent         @"externalEvent"
#define kKeyExternalEventType     @"externalEventType"
// Chat module
#define kKeyFrom @"me"
#define kKeyTo @"to"
#define kKeyTime @"timestamp"
#define kKeyMessage @"message"
#define kKeyOutgoingFlag @"outgoing"
#define kKeyMessageStatus @"message_status"
#define kKeyMessageType @"message_type"
#define kKeyChatImage @"chat_image"
#define kKeyChatImageName @"chatImageName"
#define kKeyEventChatFlag @"event_chatFlag"
#define kKeyCheckInMessage @"checkin_message"
#define kKeyFriendId @"id"
#define KMediaImage @"image"
#define kMediaType @"mediatype"
#define kKeyCategory @"catogory"
#define kkeyCaption @"caption"
#define KMediaVideo @"video"
#define KKeyMediaPath @"path"
// XMPP
#define kXMPPmyJID @"xmppJID"
#define kXMPPmyPassword @"xmppPassword"

//Social import

#define kKeyProvider        @"provider"
#define kKeyContacts        @"contacts"
#define kKeyFacebook        @"facebook"
#define kKeyGmail           @"gmail"
#define kKeyYahoo           @"yahoo"
#define kKeyHotmail         @"hotmail"
#define kKeyIsImportPhoneContact @"IsImportPhoneContact"

#define kKeyContactValue @0
#define kKeyGmailValue @1
#define kKeyYahooValue @2
#define kKeyOutlookValue @3

#define kKeyoffset @"offset"
#define kKeyIsSelected @"isSelected"
#define kKeyNumRecords @"num_records"
#define kKeyContactType @"contact_type"
#define kKeyContactID @"contact_ids"
#define kKeyTrackingIdValue @"tracking_id"

// tooltip flag
#define kKeyRadarToolTipFlag @"toolTipFlagForRadar"
#define kKeyMapToolTipFlag @"toolTipFlagForMap"
#define kKeyListToolTipFlag @"toolTipFlagForList"

// Tracking
#define kKeyIsFromTracking @"isFromTracking"
#define kKeyTrackLocId @"tracking_location_id"
#define kIslocationTrackOn @"is_location_tracking_on"
#define kKeyTrackNotify @"notify"
#define kKeyAllowTrackNotify @"allow_notify_on"

#define kKeyTrackNotify_On @"notify_on"
#define kKeyTracking @"tracking"
#define kKeyTrackingLocation @"tracking_location"
#define kKeyTrackNotified_on @"notified_on"
#define kKeyAllowTrackingMe @"allow_tracking_me"
#define kKeyTrackingArray @"tracking_me"
#define kkeyTrackingId @"id"
#define kKeyAllowImTracking @"allow_im_tracking"
#define kKeyNotifications @"notifications"


#define kKeyLocationPermission @"Please go to your phone's Settings, then choose Serendipity then Location Services and enable “Always” so that Serendipity can show your accurate location.  Serendipity will not work properly if Location Services is not set to Always"


#endif
