#ifndef Serendipity_NotificationsConstant_h
#define Serendipity_NotificationsConstant_h

#ifndef DISTANCEFILTER
#define DISTANCEFILTER 50
#endif

////Keys For google login
//AIzaSyBc7K0Sv92kigS-S2fTK3lKL7KUjzv8v_Y
//#define kKeyGoogleMap @"AIzaSyBIx4g8Sf_USWaOKd25xaNlPeWRVkRfPko"
#define kKeyGoogleMap @"AIzaSyBlDosAmGJwMLJfIqFAIGkVN7w4TEdYAGg"//@"AIzaSyBc7K0Sv92kigS-S2fTK3lKL7KUjzv8v_Y"//Sunil
#define kKeyServerGoogleMap @"AIzaSyCBaPR9AdPxsNB0ZBVdYV6MqhYCD1CZXQs"
#define kOSMroutingAPi @"route?point"
#define kAddressApi @"reverse?format=json&lat"
//#define kKeyClientID @"429750636944-rgr4l1an0ddtv0a9dmnrilk91ei90tnh.apps.googleusercontent.com" // com.serendipityawaits
#define kKeyClientID @"429750636944-litsrruf29d6i6js93924qsd7bp4ju2u.apps.googleusercontent.com"


//@"364011369130-ga10l2u5e6vdbqu6hj1jv96jqjg1ulme.apps.googleusercontent.com"

// APP Notifications
#define kKeyNotificationGroupRadarRefreshOnSlider @"groupRadarUserRefresh"
#define kKeyNotificationGroupMapRefreshOnSlider @"groupMapUserRefresh"
#define kKeyNotificationEventMapRefreshOnSlider @"eventMapUserRefresh"
#define kKeyNotificationEventRadarRefreshOnSlider @"eventRadarUserRefresh"

#define kKeyNotificationRotateMap @"rotateMap"
#define kKeyNotificationLocationInit @"locationStart"
#define kKeyNotificationMapRefreshOnSlider @"mapUserRefresh"
#define kKeyNotificationProfileImageCached @"profileImgCached"
#define kKeyNotificationRadarUserImageDownloaded @"radaruserimagedownloaded"
#define kKeyNotificationRadarLocationUpdated @"radarLocationUpdated"
#define kKeyNotificationRadarUserChanges @"radarUserChanges"
#define kKeyNotificationRadarUserStartLoading @"radarStartLoading"
#define kKeyNotificationMapUserChanges @"mapUserChanges"
#define kKeyNotificationUpdateRadarList @"updateRadarList"
#define kKeyNotificationUpdateTimer @"updateTimer"
#define kKeyNotificationStopUpdateTimer @"stopTimer"
#define kKeyNotificationFilterApplied @"filterApplied"
#define kKeyNotificationMapFilterApplied @"mapFilterApplied"
#define kKeyNotificationBatteryUsageUpdate @"batteryUsageUpdate"
#define kKeyNotificationUpdateHeadingValue @"updateHeadingValue"
#define kKeyNotificationUpdatedNotificationCount @"updatedNotificationCount"
#define kKeyNotificationGroupRadarChanges @"groupRadarChanges"
#define kKeyNotificationGroupRadarImageDownloaded @"groupradarimagedownloaded"
#define kKeyNotificationGroupUserRadarImageDownloaded @"groupuserradarimagedownloaded"
#define kKeyNotificationDropPinLocation @"kKeyNotificationDropPinLocation"
#define kkeynotificationHideNotificationView @"hideNotificationTable"
#define kKeyNotificationCallCategoryExternalEvent @"getCategoryExternalEvent"
#define kKeyNotificationCallVenueExternalEvent @"getVenueExternalEvent"
#define kKeyNotificationCallExternalEvent @"getExternalEvent"
#define kKeyNotificationEventRadarChanges @"eventRadarChanges"
#define kKeyNotificationEventRadarImageDownloaded @"eventradarimagedownloaded"
#define kKeyNotificationEventUserRadarImageDownloaded @"eventUserradarimagedownloaded"

#define kKeyChatMessageRecieved @"chatmessagerecieved"
#define kKeyNotificationMessageStateUpdated @"chatmessagestateupdated"
#define kKeyNotificationOnlineStatus @"onLineStatunotification"
#define kKeyNotificationBadgeCountUpdated @"getBadgeCount"
#define kKeyChatTypingStateNotify @"chatmessagetypingstatenotify"
#define kKeyNotifyInviteeListFetched @"inviteeListFetched"
#define kKeyNotifyGroupChatOfflineFetched @"groupChatOfflineFetched"

// Images url path
#define kKeyUserProfileImage @"images/"

// Api Urls
#define kKeyClassSendSMS @"users/send-sms"
#define kKeyClassGetCountryList @"country"
#define kKeyClassRegisterUser @"users"
#define kKeyClassVerifyCode @"users/verify"
#define kKeyClassVerifyPhoneCode @"users/verify-phone"

#define kKeyClassVerifydeviceCode @"users/verify-device"

#define kKeyClassfavourites @"favourite"
#define kKeyClassInvisible @"invisible"
#define kKeyClassSendVerifyCode @"users/send-verification-code"
#define kKeyClasschangephonerequestCode @"users/change-phone-request"
#define kKeyClassForgotPassword @"forgot-password"
#define kKeyClassLogin @"users/login"
#define kKeyUserDeviceVerifications @"users/send-device-verification"
#define kKeyClassUpdateLocation @"user-location/"
#define kKeyClassUserAlbum @"media"
#define kKeyClassGetRadarUsers @"radar"
#define kKeyClassSearchUser @"users?q="
#define kKeyClassSearchUsersById @"user-location?user_ids="
#define kKeyClassAddConnection @"connection"
#define kKeyClassBlockdeleteConnectionuser @"block-delete-contact"
#define kKeyClassDeleteBlockConnection @"connection"
#define kKeyClassUpdateConnection @"connection/"
#define kKeyClassApproveRejectContact @"contact/approve-reject-request"
#define kKeyClassGetPendingConnection @"connection?connection_status=1"
#define kKeyClassGetConnection @"connection?connection_status=2"
#define kKeyClassGetBlockedConnection @"get-block-delete-contact?type=block&status=1"
#define kKeyClassGetDeleteConnection @"get-block-delete-contact?type=delete&status=1"
#define kkeyLocationHistory @"user-location/history"
#define kKeyClassGetNotification @"notification?receiver_id="
#define kKeyClassUpdateNotification @"notification/"
#define kKeyClassLogout @"logout"
#define kKeyClassCreatePins @"pins"
#define kKeyClassGetPins @"pins?user_id="
#define kKeyClassUpdatePin @"pins/"
#define kKeyClassDeletePin @"pins/"
#define kKeyClassCreateGroup @"group"
#define kKeyClassDeleteGroupMember @"group-member"
#define kKeyClassFamily @"family"
#define kKeyClassSetting @"setting"
#define kKeyClassEvent @"event?"
#define kKeyClassCreateEvent @"event"
#define kKeyClassEventAttendees @"event-attendees"
#define kKeyClassUpdateUserPhoneNo @"users"
#define kKeyClassUploadContacts @"users/import"
#define kKeyClassUserContactsStore @"user-contacts/store"
#define kKeyClassUserContactsGet @"user-contacts"
#define kKeyClassContactSendConnectionRequest @"contact/send-connection-request"
#define kkeyOnOffSingleTracking @"tracking/togglesinglenotification"

#define kKeyClassContactUs @"users/contact-us"
#define kKeyClassUpdateUserActiveStatus @"users/status"
//#define kKeyClassSendChatNotification   @"index.php/notification"
#define kKeyClassSendChatNotification   @"notification"
#define kKeyClassGetAllRadarMapUsers @"radar/map-users"
#define kKeyClassGetMapUsersDetails @"radar/map-user-bunch"

// tracking (Does not contain in old build)
#define kkeyClassGetImTrackingList @"tracking"
#define kkeyClassAllowCancelTracking @"tracking/"
#define kkeyAllNotificationControl @"tracking/togglenotifications"
#define kkeyDeleteTrackingOnly @"tracking/delete"
#define kkeyClassGetConnTrackingMeList @"tracking?tracking_me=true"
#define kkeyClassAddTrackingLocation @"tracking-location"
#define kkeyClassDeleteTrackingLocation @"tracking-location/"
#define kkeyDeleteTracking @"tracking/delete/"
#define kkeyClassGetTrackLocListOtherUser @"tracking-location?user_id="
#define kkeyClassUpdateTrackLocOtherUser @"tracking-location/"
#define kkeyClassUpdateNotificationPlace @"tracking/notification-place"
#define kkeyClassAllowNotification @"tracking-user/"
#define kKeyClassNotifyIn @"/tracking/notify-in"
#define kKeyClassNotifyOut @"/tracking/notify-out"
#define kKeyClassGetTrackingNotification @"notification/5"
#define kKeyClassEventNotify @"event/notify"
#define kKeyOnOffNotification @"tracking/togglesinglenotification"
// API Notifications
#define kKeyNotificationLocationChanged @"MyLocationChanged"

#define kKeyNotificationAddInvisibleSucceed @"AddInvisibleSucceed"
#define kKeyNotificationAddInvisibleFailed @"AddInvisibleFailed"

#define kKeyNotificationAddFavouritesSucceed @"AddFavouritesSucceed"
#define kKeyNotificationAddFavouritesFailed @"AddFavouritesFailed"

#define kKeyNotificationCountryListSucceed @"CountryListSucceed"
#define kKeyNotificationCountryListFailed @"CountryListFailed"

#define kKeyNotificationRegisterUserSucceed @"registerUserSucceed"
#define kKeyNotificationRegisterUserFailed @"registerUserFailed"

#define kKeyNotificationGetUserProfileSucceed @"GetUserProfileSucceed"
#define kKeyNotificationGetUserProfileFailed @"GetUserProfileFailed"

#define kKeyNotificationVerifyCodeSucceed @"verifyCodeSucceed"
#define kKeyNotificationVerifyCodeFailed @"verifyCodeFailed"

#define kKeyNotificationGetForgotPasswordCodeSucceed @"GetForgotPasswordCodeSucceed"
#define kKeyNotificationGetForgotPasswordCodeFailed @"GetForgotPasswordCodeFailed"

#define kKeyNotificationGetForgotPasswordCodeSucceed @"GetForgotPasswordCodeSucceed"
#define kKeyNotificationGetForgotPasswordCodeFailed @"GetForgotPasswordCodeFailed"

#define kKeyNotificationForgotPasswordSucceed @"ForgotPasswordSucceed"
#define kKeyNotificationForgotPasswordFailed @"ForgotPasswordFailed"


#define kKeyNotificationCompleteSignUPSucceed @"completeSignUpSucceed"
#define kKeyNotificationCompleteSignUPFailed @"completeSignUpFailed"

#define kKeyNotificationEventInOutSucceed @"EventInOutSucceed"
#define kKeyNotificationEventInOutFailed @"EventInOutFailed"

#define kKeyNotificationSignUPSucceed @"signUpSucceed"
#define kKeyNotificationSignUPFailed @"signUpFailed"

#define kKeyNotificationLoginSuccess @"loginsuccesful"
#define kKeyNotificationLoginFailed @"loginfailed"

#define kKeyNotificationSendEmailSuccess @"SendEmailsuccesful"
#define kKeyNotificationSendEmailFail @"SendEmailfailed"

#define kKeyNotificationUserDeviceVerifySuccess @"DeviceVerifysuccesful"
#define kKeyNotificationUserDeviceVerifyFailed @"DeviceVerifyfailed"

#define kKeyNotificationUpdateLocationSuccess @"updatelocationsucccess"
#define kKeyNotificationUpdateLocationFailed @"updatelocationfailed"

#define kKeyNotificationUpdateLocationInBackground @"updateLocationInBackground"
#define kKeyNotificationStopUpdateLocationInBackground @"stopUpdateLocationInBackground"
#define kKeyNotificationrestatrmonitoringLocationInBackground @"restatrmonitoringLocation"

#define kKeyNotificationGetUserDetailsSucceed @"userdetailsucceed"
#define kKeyNotificationGetUserDetailsFailed @"userdetailsfailed"

#define kKeyNotificationUploadUserAlbumSucceed @"useralbumuploadsucceed"
#define kKeyNotificationUploadUserAlbumFailed  @"useralbumuploadfailed"

#define kKeyNotificationGetRadarUsersSucceed @"getradaruserssucceed"
#define kKeyNotificationGetRadarUsersFailed  @"getradarusersfailed"

#define kKeyNotificationSearchUserSucceed @"SearchUserSucceed"
#define kKeyNotificationSearchUserFailed @"SearchUserFailed"

#define kKeyNotificationSearchUsersByIdSucceed @"SearchUsersByIdSucceed"
#define kKeyNotificationSearchUsersByIdFailed @"SearchUsersByIdFailed"
#define kKeyNotificationRefreshList @"RefreshList"
#define kKeyNotificationRefreshMedia @"RefreshMedia"
#define kKeyNotificationBackClicked @"backTabBar"
#define kKeyNotificationAddConnectionSucceed @"AddConnectionSucceed"
#define kKeyNotificationAddConnectionFailed @"AddConnectionFailed"

#define kKeyNotificationUpdateConnectionSucceed @"UpdateConnectionSucceed"
#define kKeyNotificationUpdateConnectionFailed @"UpdateConnectionFailed"

#define kKeyNotificationGetConnectionSucceed @"GetConnectionSucceed"
#define kKeyNotificationGetConnectionFailed @"GetConnectionFailed"


#define kKeyNotificationGetDeleteBlockConnectionSucceed @"GetDeleteBlockConnectionSucceed"
#define kKeyNotificationGettDeleteBlockConnectionFailed @"GetDeleteBlockConnectionFailed"



#define kKeyNotificationGetBlockedUsersSucceed @"getBlockedUsersSucceed"
#define kKeyNotificationGetBlockedUsersFailed @"getBlockedUsersFailed"

#define kKeyNotificationGetDeletedUsersSucceed @"getDeletedUsersSucceed"
#define kKeyNotificationGetDeletedUsersFailed @"getDeletedUsersFailed"


#define kKeyNotificationGetUnDeletedUsersSucceed @"getUnDeletedUsersSucceed"
#define kKeyNotificationGetUnDeletedUsersFailed @"getUnDeletedUsersFailed"


#define kKeyNotificationDeleteBlockConnectionSucceed @"deleteBlockConnectionSucceed"
#define kKeyNotificationDeleteBlockConnectionFailed @"deleteBlockConnectionFailed"

#define kKeyNotificationFetchNotification @"FetchNotification"
#define kKeyNotificationGetNotificationSucceed @"GetNotificationSucceed"
#define kKeyNotificationGetNotificationFailed @"GetNotificationFailed"

#define kKeyNotificationGetTrackingNotificationSucceed @"GetTrackingNotificationSucceed"
#define kKeyNotificationGetTrackingNotificationFailed @"GetTrackingNotificationFailed"
#define kKeyNotificationBackground @"BackgroundNotification"

#define kKeyNotificationNotifyInTrackingNotificationSucceed @"notifyInTrackingNotificationSucceed"
#define kKeyNotificationNotifyInTrackingNotificationFailed @"notifyInTrackingNotificationFailed"
#define kKeyNotificationNotifyOutTrackingNotificationSucceed @"notifyOutTrackingNotificationSucceed"
#define kKeyNotificationNotifyOutTrackingNotificationFailed @"notifyOutTrackingNotificationFailed"

#define kKeyNotificationUpdateNotificationSucceed @"UpdateNotificationSucceed"
#define kKeyNotificationUpdateNotificationFailed @"UpdateNotificationFailed"

#define kKeyContactNotificationUpdateNotificationSucceed @"ContactUpdateNotificationSucceed"
#define kKeyContactNotificationUpdateNotificationFailed @"ContactUpdateNotificationFailed"

#define kKeyturnOnOffNotificationNotifyFailed @"TurnONOffNotificationNotifyFailed"
#define kKeyturnOnOffNotificationNotifySucceed @"TurnONOffNotificationNotifySucceed"

#define kKeylocationHistorySucceed @"locationHistoryApiSucceed"
#define kKeylocationHistoryFailure @"locationHistoryFailure"

#define kKeyNotificationUpdateNotificationPlaceSucceed @"UpdateNotificationPlaceSucceed"
#define kKeyNotificationUpdateNotificationPlaceFailed @"UpdateNotificationPlaceFailed"

#define kKeyNotificationLogoutSucceed @"LogoutNotificationSucceed"
#define kKeyNotificationLogoutFailed @"LogoutNotificationFailed"

#define kKeyNotificationMailSendSuccess @"MailSuccess"
#define KkeyNotificationMailFailed @"MailFailed"

#define kKeyNotificationCreatePinSucceed @"CreatePinSucceed"
#define kKeyNotificationCreatePinFailed @"CreatePinFailed"

#define kKeyNotificationGetPinsSucceed @"GetPinsSucceed"
#define kKeyNotificationGetPinsFailed @"GetPinsFailed"

#define kKeyNotificationUpdatePinsSucceed @"updatePinsSucceed"
#define kKeyNotificationUpdatePinsFailed @"updatePinsFailed"

#define kKeyNotificationDeletePinSucceed @"DeletePinSucceed"
#define kKeyNotificationDeletePinFailed @"DeletePinFailed"

#define kKeyNotificationCreateGroupSucceed @"CreateGroupSucceed"
#define kKeyNotificationCreateGroupFailed @"CreateGroupFailed"

#define kKeyNotificationShowGroupSucceed @"ShowGroupSucceed"
#define kKeyNotificationShowGroupFailed @"ShowGroupFailed"

#define kKeyNotificationDeleteGroupMemberSucceed @"DeleteGroupMemberSucceed"
#define kKeyNotificationDeleteGroupMemberFailed @"DeleteGroupMemberFailed"

#define kKeyNotificationAcceptRejectGroupMemberSucceed @"AcceptRejectGroupMemberSucceed"
#define kKeyNotificationAcceptRejectGroupMemberFailed @"AcceptRejectGroupMemberFailed"

#define kKeyNotificationRequestToJoinEventSucceed @"RequestToJoinEventSucceed"
#define kKeyNotificationRequestToJoinEventFailed @"RequestToJoinEventFailed"

#define kKeyNotificationAcceptRejectEventSucceed @"AcceptRejectEventSucceed"
#define kKeyNotificationAcceptRejectEventFailed @"AcceptRejectEventFailed"


#define  KKeyNotificationEventConverted @"EventConvertedSucceed"

#define kKeyNotificationGetGroupMemberSucceed @"CreateGroupSucceed"
#define kKeyNotificationGetGroupMemberFailed @"CreateGroupFailed"

#define kKeyNotificationFamilyActionSucceed @"FamilyActionSucceed"
#define kKeyNotificationFamilyActionFailed @"FamilyActionFailed"

#define kKeyNotificationGetExternalEventSucceed @"ShowExternalEventSucceed"
#define kKeyNotificationGetExternalEventFailed @"ShowExternalEventFailed"

#define kKeyNotificationGetEventSucceed @"ShowEventSucceed"
#define kKeyNotificationGetEventFailed @"ShowEventFailed"

#define kKeyNotificationCreateEventSucceed @"CreateEventSucceed"
#define kKeyNotificationCreateEventFailed @"CreateEventFailed"

#define kKeyNotificationDeleteEventSucceed @"DeleteEventSucceed"
#define kKeyNotificationDeleteEventFailed @"DeleteEventFailed"

#define kKeyNotificationInviteUserSucceed @"InviteUserSucceed"
#define kKeyNotificationInviteUserFailed @"InviteUserFailed"

#define kKeyNotificationUpdateUserPhoneNoSucceed @"UpdateUserPhoneNoSucceed"
#define kKeyNotificationUpdateUserPhoneNoFailed @"UpdateUserPhoneNoFailed"

#define kKeyNotificationSettingUpdateSucceed @"SettingUpdateSucceed"
#define kKeyNotificationSettingUpdateFailed @"SettingUpdateFailed"

#define kKeyNotificationDeleteAccountSucceed @"DeleteAccountSucceed"
#define kKeyNotificationDeleteAccountFailed @"DeleteAccountFailed"
// TODO: delete them later
#define kKeyNotificationSignUpSuccess @"signupSuccess"

#define kKeyNotificationHideTopDropDown @"HideTopDropDown"

#define kKeyNotificationContactUploadSucceed @"ContactUploadSucceed"
#define kKeyNotificationContactUploadFailed @"ContactUploadFailed"

#define kKeyNotificationUserContactStoreSucceed @"UserContactStoreSucceed"
#define kKeyNotificationUserContactStoreFailed @"UserContactStoreFailed"

#define kKeyNotificationUserSocialContactGetSucceed @"UserSocialContactGetSucceed"
#define kKeyNotificationUserSocialContactGetFailed @"UserSocialContactGetFailed"

#define kKeyNotificationContactSendConnectionRequestSucceed @"ContactSendConnectionRequestSucceed"
#define kKeyNotificationContactSendConnectionRequestFailed @"ContactSendConnectionRequestFailed"

#define kKeyNotificationContactBlockSucceed @"ContactBlockSucceed"
#define kKeyNotificationContactBlockFailed @"ContactBlockFailed"

#define kKeyNotificationContactDeleteSucceed @"ContactDeleteSucceed"
#define kKeyNotificationContactDeleteFailed @"ContactDeleteFailed"

#define kKeyNotificationConnTrackMeSucceed @"ConnTrackMeActionSucceed"
#define kKeyNotificationConnTrackMeFailed @"ConnTrackMeActionFailed"

#define kKeyNotificationConnImTrackSucceed @"ConnImTrackActionSucceed"
#define kKeyNotificationConnImTrackFailed @"ConnImTrackActionFailed"

#define kKeyNotificationGetTrackLocationsSucceed @"GetTrackLocationsActionSucceed"
#define kKeyNotificationGetTrackLocationsFailed @"GetTrackLocationsActionFailed"

#define kKeyNotificationGetMyTrackLocationsSucceed @"GetMyTrackLocationActionsSucceed"
#define kKeyNotificationGetMyTrackLocationsFailed @"GetMyTrackLocationActionsFailed"

#define kKeyNotificationAddMyTrackLocationsSucceed @"AddMyTrackLocationActionSucceed"
#define kKeyNotificationAddMyTrackLocationsFailed @"AddMyTrackLocationActionFailed"

#define kKeyNotificationDeleteMyTrackLocationsSucceed @"DeleteMyTrackLocationActionSucceed"
#define kKeyNotificationDeleteMyTrackLocationsFailed @"DeleteMyTrackLocationActionFailed"

#define kKeyNotificationUpdateTrackLocationSucceed @"UpdateTrackLocationActionSucceed"
#define kKeyNotificationUpdateTrackLocationFailed @"UpdateTrackLocationActionFailed"

#define kKeyNotificationSaveTrackLocationSucceed @"SaveTrackLocationActionSucceed"
#define kKeyNotificationSaveTrackLocationFailed @"SaveTrackLocationActionFailed"

#define kKeyNotificationAllowCancelTrackSucceed @"AllowCancelTrackSucceed"
#define kKeyNotificationAllowCancelTrackFailed @"AllowCancelTrackFailed"

#define kKeyNotificationAllowTrackMeSucceed @"AllowMeTrackSucceed"
#define kKeyNotificationAllowTrackMeFailed @"AllowMeTrackFailed"

#define kKeyNotificationTrackAddedLocationSucceed @"NewTrackLocationAdded"

// User active status
#define kKeyNotificationUpdateActiveStatusSucceed @"UpdateStatusNotificationSucceed"
#define kKeyNotificationUpdateActiveStatusFailed @"UpdateStatusNotificationFailed"

//All serendipity users on map
#define kKeyNotificationGetAllRadarMapUsersSucceed @"GetAllRadarMapUsersSucceed"
#define kKeyNotificationGetAllRadarMapUsersFailed @"GetAllRadarMapUsersFailed"

#define kKeyNotificationGetMapUsersDetailsSucceed @"GetMapUsersDetailsSucceed"
#define kKeyNotificationGetMapUsersDetailsFailed @"GetMapUsersDetailsFailed"


#define kKeyNotificationDeleteTrackingSuccess @"DeleteTrackingSuccess"
#define kKeyNotificationDeleteTrackingFailure @"DeleteTrackingFailure"
#define kKeyToggleAllNotificationSuccess @"NotificationToggleSuccess"
#define kKeyToggleAllNotificationFailure @"NotificationToggleFailure"
#define kKeyToggleSingleNotifications @"NotificationToggleSingleNotificationSuccess"
#endif
