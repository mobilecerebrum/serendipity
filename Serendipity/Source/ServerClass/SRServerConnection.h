#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "NotificationsConstant.h"

// Define api key
#define kKeyApi @""

// Define metods type
#define kPOST @"POST"
#define kGET  @"GET"
#define kPUT @"PUT"
#define kDELETE @"DELETE"

#define kKeyToken @"token"
#define kKeyAuthorization @"Authorization"
#define kKeyStatus @"status"
#define kKeySuccess @"success"
#define kKeyFail @"fail"
#define kKeyResponse @"response"
#define kKeyMessage @"message"
#define kKeyImageUrl @"imageurl"
#define kKeyFlagUserLocation @"userLocation"
#define kKeyObjectUrl @"objectUrl"
#define kKeyResponseData @"response"
#define kKeyRequestedParams @"requestedParams"

#define kKeyActivityIndicatorState @"activity_indicatorState"

// Class type
//NotificationsConstant.h

@interface SRServerConnection : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
	// URL string
	NSString *serverURL;
    NSString *oldUrl;
}

// Property
@property (strong, nonatomic) NSMutableDictionary *loggedInUserInfo,*loggedInFbUserInfo;
@property (strong, nonatomic) NSDictionary *friendProfileInfo;
@property (strong, nonatomic) NSDictionary *groupDetailInfo;
@property (strong, nonatomic) NSArray *groupUsersArr;
    //For event
@property (strong, nonatomic) NSDictionary *eventDetailInfo;
@property (strong, nonatomic) NSArray *eventUsersArr;
@property (strong, nonatomic) UIImage *userImage;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) CLLocation *myLocation;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSArray *notificationArr;
@property (strong, nonatomic) NSArray *radarSourceArray;
@property (strong, nonatomic) NSString *serverURL;
// For contact list
@property(strong,nonatomic) NSMutableArray  *contactList,*importContactArr;
// For country array
@property (strong, nonatomic) NSArray *countryArr;

//For chat online user list
@property(strong,nonatomic) NSMutableArray  *onlineUserList;
#pragma mark
#pragma mark Init
#pragma mark

- (id)initWithURl:(NSString *)inURl;

#pragma mark
#pragma mark Asychronous Request
#pragma mark

- (void)makeAsychronousRequest:(NSString *)inObjectUrl
                      inParams:(NSDictionary *)inParamsDict
           isIndicatorRequired:(BOOL)isVisible
                  inMethodType:(NSString *)inMethodName;

- (void)asychronousRequestWithData:(NSDictionary *)inParamsDict
                         imageData:(NSData *)inImgData
                            forKey:(NSString *)imageKey
                       toClassType:(NSString *)inClassName
                      inMethodType:(NSString *)inMethodName;

#pragma mark
#pragma mark Synchronous Request
#pragma mark

- (void)makeSynchronousRequest:(NSString *)inObjectUrl
                      inParams:(NSDictionary *)inParamsDict
                  inMethodType:(NSString *)inMethodName
           isIndicatorRequired:(BOOL)isVisible;


@end
