#import "Constant.h"
#import "SRAppDelegate.h"
#import "SRModalClass.h"
#import "SRServerConnection.h"
#import "NSMUtableUrlRequest+NSURLRequestWithIgnoreSSL.h"

#define CLCOORDINATES_EQUAL(coord1, coord2)                                    \
(coord1.latitude == coord2.latitude && coord1.longitude == coord2.longitude)

@implementation SRServerConnection

@synthesize onlineUserList;


#pragma mark
#pragma mark Init
#pragma mark

// ------------------------------------------------------------------------------
// initWithURl:

- (id)initWithURl:(NSString *)inURl {
    // Call super
    self = [super init];
    
    if (self) {
        // Initialized connection with url
        serverURL = inURl;
        self.loggedInUserInfo = [NSMutableDictionary dictionary];
        self.onlineUserList = [[NSMutableArray alloc] init];
    }
    
    // Return self
    return self;
}

#pragma mark
#pragma mark Common Methods
#pragma mark

// ------------------------------------------------------------------------------
// prepareRequestWithUrl:

- (NSMutableURLRequest *)prepareRequestWithUrl:(NSString *)inObjectUrl
                                     forParams:(NSDictionary *)inParamsDict
                                    methodType:(NSString *)inMethodName {
    // Url strings
    NSString *strUrl =
    [NSString stringWithFormat:@"%@%@", serverURL, inObjectUrl];
    //console
    if (!([inObjectUrl rangeOfString:@"user-location/"].location == NSNotFound)) {
        addTextInSignificantTxt([NSString stringWithFormat:@"InputURl == %@ and input == %@", strUrl, inParamsDict]);
    }
  //  NSLog(@"prepareRequestWithUrl: \nInputURl == %@ and input == %@", strUrl, inParamsDict);
    // Convert params dict in json string
    NSError *error = nil;
    NSData *jsonData = nil;
    if ([inParamsDict count] > 0)
        jsonData =
        [NSJSONSerialization dataWithJSONObject:inParamsDict
                                        options:NSJSONWritingPrettyPrinted
                                          error:&error];
    // Create mutable request
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    
    [request setHTTPMethod:inMethodName];
    
    [request setValue:@"application/json; charset=utf-8"
   forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json"
   forHTTPHeaderField:@"Accept"];
    
    if (self.token != nil) {
        [request setValue:self.token forHTTPHeaderField:kKeyAuthorization];
    }
    if (jsonData) {
        [request setHTTPBody:jsonData];
    }
    
    // Set request timeout interval time
    request.timeoutInterval = 30;
    
    // Return
    return request;
}

// ------------------------------------------------------------------------------
// processResponseAndData:

- (void)processResponseAndData:(NSURLResponse *)inReponse
                    resultData:(NSData *)inData
               requestHasError:(NSError *)inError
                        forUrl:(NSString *)inObjectUrl
                    forRequest:(NSMutableURLRequest *)inRequest {
    // Call succeed
    id requestedParams = [NSDictionary dictionary];
    
    if (inRequest.HTTPBody != nil) {
        requestedParams = [NSJSONSerialization JSONObjectWithData:inRequest.HTTPBody
                                                          options:NSJSONReadingMutableContainers error:nil];
    }
    
    if (inReponse != nil && inData != nil) {
        NSString *responseStr = [[NSString alloc] initWithData:inData encoding:NSUTF8StringEncoding];
        NSString *outString = [responseStr stringByReplacingOccurrencesOfString:@"\r\n"
                                                                     withString:@""];
        NSData *convertedData = [outString dataUsingEncoding:NSUTF8StringEncoding];
        
        //console
      //  NSLog(@"reqURl == %@ and responsedata == %@", inObjectUrl, outString);
        
        
        // Convert data to string
        id responseData = [NSJSONSerialization JSONObjectWithData:convertedData
                                                          options:NSJSONReadingMutableContainers
                                                            error:nil];
        
        // Check for success and failure
        if ([responseData isKindOfClass:[NSDictionary class]] &&
            [(NSDictionary *) responseData objectForKey:kKeyStatus] != nil && responseData != nil) {
            
            NSString *successOrFailure =
            [(NSDictionary *) responseData objectForKey:kKeyStatus];
            
            NSString *message = @"";
            if ([responseData isKindOfClass:[NSDictionary class]] &&
                [(NSDictionary *) responseData objectForKey:kKeyMessage] != nil && responseData != nil) {
                message = [(NSDictionary *) responseData objectForKey:kKeyMessage];
            }
            
            if ([successOrFailure isEqualToString:kKeySuccess]) {
                id resultObject = [(NSDictionary *) responseData objectForKey:kKeyResponse];
                
                if ([(NSDictionary *) responseData objectForKey:kKeyResponse] == nil) {
                    resultObject = responseData;
                }
                
                NSDictionary *postInfo = [NSDictionary
                                          dictionaryWithObjectsAndKeys:resultObject, kKeyResponseData,
                                          inObjectUrl, kKeyObjectUrl,
                                          requestedParams, kKeyRequestedParams,
                                          message, kKeyMessage,
                                          inRequest.HTTPMethod, @"method",
                                          nil];
                
                
                [self performSelectorOnMainThread:@selector(processSuccessResult:)
                                       withObject:postInfo
                                    waitUntilDone:NO];
            } else {
                id resultObject =  [(NSDictionary *) responseData objectForKey:kKeyResponse];
                
                if ([resultObject isKindOfClass:[NSString class]]) {
                    if ([resultObject isEqualToString:@"Mobile number is not verified."]) {
                        NSDictionary *dict = [(NSDictionary *) responseData objectForKey:@"user_data"];
                        (APP_DELEGATE).dictUserData = [dict objectForKey:@"user"];
                        self.token = [[NSString alloc] initWithFormat:@"Bearer %@", [dict objectForKey:kKeyToken]];
                        
                        // Set Bearer token for API manager
                        [[SRAPIManager sharedInstance] updateToken:self.token];
                    }
                }
                
                NSDictionary *postInfo = [NSDictionary
                                          dictionaryWithObjectsAndKeys:resultObject, kKeyResponseData,
                                          inObjectUrl, kKeyObjectUrl,
                                          requestedParams, kKeyRequestedParams,
                                          nil];
                
                // Perform in main thread
                [self performSelectorOnMainThread:@selector(processFailureResult:)
                                       withObject:postInfo
                                    waitUntilDone:NO];
            }
        } else if (responseData == nil) {
            NSDictionary *postInfo = [NSDictionary
                                      dictionaryWithObjectsAndKeys:responseStr, kKeyResponseData,
                                      inObjectUrl, kKeyObjectUrl,
                                      requestedParams, kKeyRequestedParams, nil];
            
            
            // Perform in main thread
            [self performSelectorOnMainThread:@selector(processFailureResult:)
                                   withObject:postInfo
                                waitUntilDone:NO];
        } else {
//            for (NSString *inKey in [responseData allKeys]) {
                id message = [responseData objectForKey:[responseData allKeys][0]];
                NSDictionary *postInfo = [NSDictionary
                                          dictionaryWithObjectsAndKeys:message,
                                          kKeyResponseData,
                                          inObjectUrl, kKeyObjectUrl,
                                          requestedParams,
                                          kKeyRequestedParams, nil];
                
                // Perform in main thread
                [self performSelectorOnMainThread:@selector(processFailureResult:)
                                       withObject:postInfo
                                    waitUntilDone:NO];
//            }
        }
    }
    // Call failed
    else if (inError != nil) {
        NSString *responseStr =
        [[NSString alloc] initWithData:inData encoding:NSUTF8StringEncoding];
        NSString *outString =
        [responseStr stringByReplacingOccurrencesOfString:@"\r\n"
                                               withString:@""];
        NSData *convertedData = [outString dataUsingEncoding:NSUTF8StringEncoding];
        
        if (convertedData != nil) {
            // Convert data to string
            id responseData =
            [NSJSONSerialization JSONObjectWithData:convertedData
                                            options:NSJSONReadingMutableContainers
                                              error:nil];
            NSDictionary *postInfo = [NSDictionary
                                      dictionaryWithObjectsAndKeys:[responseData objectForKey:kKeyResponse], kKeyResponseData,
                                      inObjectUrl, kKeyObjectUrl,
                                      requestedParams, kKeyRequestedParams, nil];
            // Perform in main thread
            [self performSelectorOnMainThread:@selector(processFailureResult:)
                                   withObject:postInfo
                                waitUntilDone:NO];
        } else {
            id resultObject = [inError localizedDescription];
            NSDictionary *postInfo = [NSDictionary
                                      dictionaryWithObjectsAndKeys:resultObject, kKeyResponseData,
                                      inObjectUrl, kKeyObjectUrl,
                                      requestedParams, kKeyRequestedParams, nil];
            
            // Perform in main thread
            [self performSelectorOnMainThread:@selector(processFailureResult:)
                                   withObject:postInfo
                                waitUntilDone:NO];
        }
    }
}

#pragma mark
#pragma mark Sychronous Request
#pragma mark

// ------------------------------------------------------------------------------
// makeSynchronousRequest

- (void)makeSynchronousRequest:(NSString *)inObjectUrl
                      inParams:(NSDictionary *)inParamsDict
                  inMethodType:(NSString *)inMethodName
           isIndicatorRequired:(BOOL)isVisible {
    
    NSMutableDictionary *paramsMutatedDict = [NSMutableDictionary dictionaryWithDictionary:inParamsDict];
    
    if (isVisible) {
        // Show the indicator
        [APP_DELEGATE showActivityIndicator];
        
        [paramsMutatedDict setObject:[NSNumber numberWithBool:isVisible] forKey:kKeyActivityIndicatorState];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NetworkStatus remoteHostStatus = [(APP_DELEGATE).reach currentReachabilityStatus];
        if (remoteHostStatus == NotReachable) {
            if (![oldUrl isEqualToString:inObjectUrl]) {
                oldUrl = inObjectUrl;
                [APP_DELEGATE hideActivityIndicator];
            }
        }
    });
    
    
    // Prepare request
    NSMutableURLRequest *request = [self prepareRequestWithUrl:inObjectUrl
                                                     forParams:inParamsDict
                                                    methodType:inMethodName];
    request.timeoutInterval = 60;
    
    // Capturing server response
    NSURLResponse *response = nil;
    NSError *requestError = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&requestError];
    // Process response for request
    [self processResponseAndData:response
                      resultData:data
                 requestHasError:requestError
                          forUrl:inObjectUrl
                      forRequest:request];
    
    NSString *responseStr =
    [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //console
    // NSLog(@"makeSynchronousRequest:\n reqURL=%@, response data %@",request.URL.absoluteString, responseStr);
}

#pragma mark
#pragma mark Asychronous Request
#pragma mark

// ------------------------------------------------------------------------------
// makeAsychronousRequest

- (void)makeAsychronousRequest:(NSString *)inObjectUrl
                      inParams:(NSDictionary *)inParamsDict
           isIndicatorRequired:(BOOL)isVisible
                  inMethodType:(NSString *)inMethodName {
    if (!([inObjectUrl rangeOfString:@"user-location/"].location == NSNotFound)) {
        addTextInSignificantTxt(@"makeAsychronousRequest called");
    }
    
    NSMutableDictionary *paramsMutatedDict = [NSMutableDictionary dictionaryWithDictionary:inParamsDict];
    
    if (isVisible) {
        // Show the indicator
        [APP_DELEGATE showActivityIndicator];
        [paramsMutatedDict setObject:[NSNumber numberWithBool:isVisible] forKey:kKeyActivityIndicatorState];
    }
    
    NetworkStatus remoteHostStatus = [(APP_DELEGATE).reach currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        if (![oldUrl isEqualToString:inObjectUrl]) {
            oldUrl = inObjectUrl;
            [APP_DELEGATE hideActivityIndicator];
        }
        [SRModalClass showAlert:NSLocalizedString(@"lbl.noConnectionFnd.txt", @"")];
        
        return;
    }
    
    // Prepare request
    NSMutableURLRequest *request = [self prepareRequestWithUrl:inObjectUrl
                                                     forParams:paramsMutatedDict
                                                    methodType:inMethodName];
    request.timeoutInterval = 60;
    
    NSLog(@"API %@",request.URL);
    
    // Create Operation queue we dont want to block main queue
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:operationQueue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *error) {
        // Process response for request
        [self processResponseAndData:response
                          resultData:data
                     requestHasError:error
                              forUrl:inObjectUrl
                          forRequest:request];
    }];
}

// ------------------------------------------------------------------------------
// asychronousRequestWithData: imageData: forKey: toClassType: forMethod:

- (void)asychronousRequestWithData:(NSDictionary *)inParamsDict
                         imageData:(NSData *)inImgData
                            forKey:(NSString *)imageKey
                       toClassType:(NSString *)inClassName
                      inMethodType:(NSString *)inMethodName {
    NSMutableDictionary *requestedParams = [NSMutableDictionary dictionaryWithDictionary:inParamsDict];
    
   // NSLog(@"Request Param:\n%@",inParamsDict);
    [APP_DELEGATE showActivityIndicator];
    [requestedParams setObject:[NSNumber numberWithBool:YES] forKey:kKeyActivityIndicatorState];
    
    // Url strings
    NSString *strUrl =
    [NSString stringWithFormat:@"%@%@", serverURL, inClassName];
    NSMutableURLRequest *request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    //Pass Token with request
    NSString *header = [NSString stringWithFormat:@"%@", self.token]; //format as needed
    [request setValue:header forHTTPHeaderField:kKeyAuthorization];
    
    [request setHTTPMethod:inMethodName];
    
    request.timeoutInterval = 90;
    
    NSString *boundary = @"";
    
    // set Content-Type in HTTP header
    NSString *contentType =
    [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in inParamsDict) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:
         [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [inParamsDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        
        //   NSString *str = [[NSString alloc]initWithData:body
        //   encoding:NSUTF8StringEncoding];
    }
    
    // add image data
    if (inImgData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"testimage.jpeg\"\r\n", imageKey] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n"
                          dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:inImgData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"]
                          dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength =
    [NSString stringWithFormat:@"%lu", (unsigned long) [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // Create Operation queue we dont want to block main queue
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:operationQueue
     completionHandler:^(NSURLResponse *response, NSData *data,
                         NSError *error) {
        // Call succeed
        if (response != nil && data != nil) {
            NSString *responseStr = [[NSString alloc] initWithData:data
                                                          encoding:NSUTF8StringEncoding];
            //console
            //NSLog(@"response data %@", responseStr);
            
            // Convert data to string
            id responseData = [NSJSONSerialization
                               JSONObjectWithData:data
                               options:NSJSONReadingMutableContainers
                               error:nil];
            
            // Check for success and failure
            if ([responseData isKindOfClass:[NSDictionary class]] &&
                [(NSDictionary *) responseData objectForKey:kKeyStatus] !=
                nil) {
                NSString *successOrFailure =
                [(NSDictionary *) responseData objectForKey:kKeyStatus];
                
                if ([successOrFailure isEqualToString:kKeySuccess]) {
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
                        NSDictionary *dictionary = [httpResponse allHeaderFields];
                        if ([dictionary objectForKey:kKeyAuthorization] != nil) {
                            self.token = [dictionary objectForKey:kKeyAuthorization];
                        }
                    }
                    
                    NSDictionary *postInfo = [NSDictionary
                                              dictionaryWithObjectsAndKeys:[responseData objectForKey:kKeyResponse],
                                              kKeyResponseData,
                                              inClassName, kKeyObjectUrl,
                                              requestedParams,
                                              kKeyRequestedParams,
                                              inMethodName, @"method",nil];
                    
                    // Perform in main thread
                    [self performSelectorOnMainThread:@selector(processSuccessResult:)
                                           withObject:postInfo
                                        waitUntilDone:NO];
                } else {
                    NSDictionary *dict = [(NSDictionary *) responseData objectForKey:kKeyResponse];
                    
                    NSDictionary *postInfo;
                    if ([(NSDictionary *) responseData objectForKey:kKeyMessage]) {
                        id resultObject = [dict objectForKey:kKeyMessage];
                        postInfo = [NSDictionary dictionaryWithObjectsAndKeys:resultObject,
                                    kKeyResponseData,
                                    inClassName, kKeyObjectUrl,
                                    requestedParams,
                                    kKeyRequestedParams, nil];
                    } else if ([[(NSDictionary *) responseData objectForKey:@"status"] isEqualToString:@"error"]) {
                        postInfo = @{response: [(NSDictionary *) responseData objectForKey:kKeyResponse],
                                     kKeyObjectUrl: inClassName,
                                     kKeyRequestedParams: requestedParams,
                                     kKeyResponseData: [(NSDictionary *) responseData objectForKey:kKeyResponse]
                        };
                    }
                    // Perform in main thread
                    [self performSelectorOnMainThread:@selector(processFailureResult:)
                                           withObject:postInfo
                                        waitUntilDone:NO];
                }
            } else if (responseData == nil) {
                NSDictionary *postInfo = [NSDictionary
                                          dictionaryWithObjectsAndKeys:responseStr, kKeyResponseData,
                                          inClassName, kKeyObjectUrl,
                                          requestedParams, kKeyRequestedParams, nil];
                
                // Perform in main thread
                [self performSelectorOnMainThread:@selector(processFailureResult:)
                                       withObject:postInfo
                                    waitUntilDone:NO];
            } else {
//                for (NSString *inKey in[responseData allKeys]) {
                    id message = [responseData objectForKey:[responseData allKeys][0]];
                    NSDictionary *postInfo = [NSDictionary
                                              dictionaryWithObjectsAndKeys:message,
                                              kKeyResponseData,
                                              inClassName, kKeyObjectUrl,
                                              requestedParams,
                                              kKeyRequestedParams, nil];
                    // Perform in main thread
                    [self performSelectorOnMainThread:@selector(processFailureResult:)
                                           withObject:postInfo
                                        waitUntilDone:NO];
//                }
            }
        } else if (error != nil) {
            id resultObject = [error localizedDescription];
            NSDictionary *postInfo = [NSDictionary
                                      dictionaryWithObjectsAndKeys:resultObject, kKeyResponseData,
                                      inClassName, kKeyObjectUrl,
                                      nil];
            
            [self performSelectorOnMainThread:@selector(processFailureResult:)
                                   withObject:postInfo
                                waitUntilDone:NO];
        }
    }];
}

#pragma mark - Process Data

- (void)processSuccessResult:(NSDictionary *)inPostInfo {
    [APP_DELEGATE hideActivityIndicator];
    NSString *successNotification = nil;
  //  NSLog(@"API Response:%@",inPostInfo);
    id resultObject = [inPostInfo objectForKey:kKeyResponseData];
    NSString *inObjectUrl = [inPostInfo objectForKey:kKeyObjectUrl];
    NSString *message = [inPostInfo objectForKey:kKeyMessage];
    NSString *method = [inPostInfo objectForKey:@"method"];
    if (inObjectUrl != nil) {
        
//        if ([inObjectUrl isEqualToString:kKeyClassdeleteConnectionuser]) {
//            successNotification = kKeyNotificationGetUnDeletedUsersSucceed;
//        }
        
        //
        if ([inObjectUrl containsString:kKeyOnOffNotification]){
            successNotification = kKeyturnOnOffNotificationNotifySucceed;
            //jonish history
        }else if([inObjectUrl isEqualToString:kkeyLocationHistory]){
            successNotification = kKeylocationHistorySucceed;
        }//jonish history
        else if ([inObjectUrl isEqualToString:kKeyClassApproveRejectContact]) {
            successNotification = kKeyContactNotificationUpdateNotificationSucceed;
        }else if([inObjectUrl containsString:kkeyOnOffSingleTracking]){
            successNotification = kKeyToggleSingleNotifications;
        }else if ([inObjectUrl isEqualToString:kKeyClassBlockdeleteConnectionuser]) {
            successNotification = kKeyNotificationGetDeleteBlockConnectionSucceed;
        }else if ([inObjectUrl isEqualToString:kKeyClassContactSendConnectionRequest]) {
            successNotification = kKeyNotificationContactSendConnectionRequestSucceed;
        }else if ([inObjectUrl isEqualToString:kKeyClassUserContactsGet]) {
             successNotification = kKeyNotificationUserSocialContactGetSucceed;
         }else if ([inObjectUrl isEqualToString:kKeyClassGetDeleteConnection]) {
            successNotification = kKeyNotificationGetDeletedUsersSucceed;
        }else if ([inObjectUrl isEqualToString:kKeyClassVerifyPhoneCode]) {
            successNotification = kKeyNotificationUserDeviceVerifySuccess;
        }else if ([inObjectUrl isEqualToString:kKeyClassVerifydeviceCode]) {
            successNotification = kKeyNotificationUserDeviceVerifySuccess;
        }else if ([inObjectUrl isEqualToString:kKeyUserDeviceVerifications]) {
            successNotification = kKeyNotificationSendEmailSuccess;
        }else if ([inObjectUrl isEqualToString:kKeyClassContactUs]) {
            successNotification = kKeyNotificationMailSendSuccess;
        }else if ([inObjectUrl isEqualToString:kKeyClassGetCountryList]) {
            successNotification = kKeyNotificationCountryListSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassSendSMS]) {
            successNotification = kKeyNotificationInviteUserSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassRegisterUser]) {
            successNotification = kKeyNotificationRegisterUserSucceed;
            NSString *strToken = [[NSString alloc] initWithFormat:@"Bearer %@", [resultObject objectForKey:kKeyToken]];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:strToken forKey:@"token"];
            [userDefaults synchronize];
            self.token = strToken;
            [self userObjectProcessed:[resultObject objectForKey:@"user"]];
            
        } else if ([inObjectUrl rangeOfString:kKeyClassRegisterUser].location != NSNotFound && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"verify"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"login"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"import"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"map-users"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"status"] && [inObjectUrl rangeOfString:@"?q="].location == NSNotFound) {
            
            if ([resultObject isKindOfClass:[NSString class]] && [resultObject isEqualToString:NSLocalizedString(@"txt.success.user_delete", @"")]) {
                successNotification = kKeyNotificationDeleteAccountSucceed;
            } else if ([inObjectUrl containsString:kKeyClassGetAllRadarMapUsers]) {
                successNotification = kKeyNotificationGetAllRadarMapUsersSucceed;
            } else if ([inObjectUrl containsString:kKeyClassSendVerifyCode] == YES) {
                [self userObjectProcessed:[resultObject objectForKey:@"user"]];
                successNotification = kKeyNotificationSignUPSucceed;
            } else if ([inObjectUrl containsString:kKeyClasschangephonerequestCode] == YES) {
                successNotification = kKeyNotificationSignUPSucceed;
                [self userObjectProcessed:[resultObject objectForKey:@"user"]];
            }else if ([resultObject isKindOfClass:[NSDictionary class]]) {
                if ([[self.loggedInUserInfo objectForKey:kKeyId] isEqualToString:[resultObject objectForKey:kKeyId]]) {
                    [self userObjectProcessed:resultObject];
                }
                successNotification = kKeyNotificationCompleteSignUPSucceed;
            } else  {
                
                successNotification = kKeyNotificationGetUserProfileSucceed;
            }
        } else if ([inObjectUrl isEqualToString:kKeyClassForgotPassword] || [inObjectUrl containsString:kKeyClassForgotPassword]) {
            if ([resultObject isKindOfClass:[NSDictionary class]] && [[[resultObject valueForKey:kKeyUser] allKeys] containsObject:kKeyId]) {
                successNotification = kKeyNotificationGetForgotPasswordCodeSucceed;
            } else
                successNotification = kKeyNotificationForgotPasswordSucceed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassVerifyCode].location != NSNotFound) {
            
            successNotification = kKeyNotificationVerifyCodeSucceed;
            
        } else if ([inObjectUrl isEqualToString:kKeyClassLogin]) {
            NSString *strToken = [[NSString alloc] initWithFormat:@"Bearer %@", [resultObject objectForKey:kKeyToken]];
            self.token = strToken;
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:strToken forKey:@"token"];
            [userDefaults synchronize];
            [self userObjectProcessed:[resultObject objectForKey:@"user"]];
            successNotification = kKeyNotificationLoginSuccess;
        } else if ([inObjectUrl isEqualToString:kKeyClassLogout]) {
            successNotification = kKeyNotificationLogoutSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassEventNotify]) {
            successNotification = kKeyNotificationEventInOutSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassUserAlbum] || [inObjectUrl rangeOfString:kKeyClassUserAlbum].location != NSNotFound) {
            successNotification = kKeyNotificationUploadUserAlbumSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassfavourites]) {
            successNotification = kKeyNotificationAddFavouritesSucceed;
            
        } else if ([inObjectUrl isEqualToString:kKeyClassInvisible]) {
            successNotification = kKeyNotificationAddInvisibleSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassUpdateLocation].location != NSNotFound) {
            successNotification = kKeyNotificationUpdateLocationSuccess;
        } else if ([inObjectUrl isEqualToString:kKeyClassGetMapUsersDetails]) {
            successNotification = kKeyNotificationGetMapUsersDetailsSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetRadarUsers].location != NSNotFound) {
            successNotification = kKeyNotificationGetRadarUsersSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassSearchUser].location != NSNotFound) {
            successNotification = kKeyNotificationSearchUserSucceed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassSearchUsersById].location != NSNotFound) {
            successNotification = kKeyNotificationSearchUsersByIdSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassAddConnection]) {
            successNotification = kKeyNotificationAddConnectionSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassContactUs]) {
            successNotification = kKeyNotificationMailSendSuccess;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassUpdateConnection].location != NSNotFound) {
            successNotification = kKeyNotificationUpdateConnectionSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassGetConnection]) {
            successNotification = kKeyNotificationGetConnectionSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassGetBlockedConnection]) {
            successNotification = kKeyNotificationGetBlockedUsersSucceed;
        }
        else if ([inObjectUrl rangeOfString:kKeyClassGetNotification].location != NSNotFound) {
            
            NSMutableArray *notifArr = [NSMutableArray new];
            [notifArr addObjectsFromArray:resultObject];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K == %@)", kKeyStatus, [NSString stringWithFormat:@"%d", 0]];
            //            NSArray *filterArr = [notifArr filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            //                if ([evaluatedObject[kKeyStatus] intValue] == 0) {
            //                    return YES;
            //                }
            //                return NO;
            //            }]];
            NSArray *filterArr = [notifArr filteredArrayUsingPredicate:predicate];
            notifArr = [[NSMutableArray alloc] initWithArray:filterArr];
            
            //            for (NSDictionary *dicObject in notifArr) {
            //
            //                if (![[dicObject objectForKey:kKeyStatus] boolValue]) {
            //
            //                    NSUInteger indexOfDicObject = [notifArr indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            //                        return ([obj[@"created_at"] isEqualToString:dicObject[@"created_at"]]);
            //                    }];
            //
            //                    if (indexOfDicObject == NSNotFound) {
            //                        [notifArr addObject:dicObject];
            //                    }
            //                }
            //
            //            }
            
            self.notificationArr = notifArr;
            successNotification = kKeyNotificationGetNotificationSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassGetTrackingNotification]) {
            successNotification = kKeyNotificationGetTrackingNotificationSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassUpdateNotification].location != NSNotFound) {
            successNotification = kKeyNotificationUpdateNotificationSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassCreatePins].location != NSNotFound) {
            successNotification = kKeyNotificationCreatePinSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetPins].location != NSNotFound) {
            successNotification = kKeyNotificationGetPinsSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassNotifyIn]) {
            successNotification = kKeyNotificationNotifyInTrackingNotificationSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassNotifyOut]) {
            successNotification = kKeyNotificationNotifyOutTrackingNotificationSucceed;
        } else if ([inObjectUrl isEqualToString:kKeyClassCreateGroup]) {
            successNotification = kKeyNotificationCreateGroupSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassDeleteGroupMember].location != NSNotFound && [resultObject isKindOfClass:[NSDictionary class]]) {
            successNotification = kKeyNotificationAcceptRejectGroupMemberSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassDeleteGroupMember].location != NSNotFound && [resultObject isKindOfClass:[NSString class]]) {
            successNotification = kKeyNotificationAcceptRejectGroupMemberSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassDeleteGroupMember].location != NSNotFound && [resultObject isEqualToString:NSLocalizedString(@"txt.success.record_delete", @"")]) {
            successNotification = kKeyNotificationCreateGroupSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassCreateGroup].location != NSNotFound) {
            successNotification = kKeyNotificationCreateGroupSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassFamily].location != NSNotFound) {
            successNotification = kKeyNotificationFamilyActionSucceed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassEvent].location != NSNotFound) {
            successNotification = kKeyNotificationGetEventSucceed;
            
        } else if ([inObjectUrl isEqualToString:kKeyClassEventAttendees]) {
            successNotification = kKeyNotificationRequestToJoinEventSucceed;
        } else if ([inObjectUrl rangeOfString:kKeyClassEventAttendees].location != NSNotFound) {
            successNotification = kKeyNotificationAcceptRejectEventSucceed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassCreateEvent].location != NSNotFound) {
            if ([resultObject isKindOfClass:[NSString class]] && [resultObject isEqualToString:@"Record deleted successfully"]) {
                successNotification = kKeyNotificationDeleteEventSucceed;
            } else {
                if ([resultObject isKindOfClass:[NSDictionary class]]) {
                    NSString *strconverted = [resultObject objectForKey:kKeyIs_Converted];
                    if ([strconverted isEqualToString:@"0"]) {
                        successNotification = kKeyNotificationCreateEventSucceed;
                    } else {
                        successNotification = KKeyNotificationEventConverted;
                    }
                }
            }
        } else if ([inObjectUrl rangeOfString:kKeyClassSetting].location != NSNotFound) {
            successNotification = kKeyNotificationSettingUpdateSucceed;
            
        } else if ([inObjectUrl isEqualToString:kKeyClassUploadContacts]) {
            successNotification = kKeyNotificationContactUploadSucceed;
        }else if ([inObjectUrl isEqualToString:kKeyClassUserContactsStore]) {
            successNotification = kKeyNotificationUserContactStoreSucceed;
        }else if ([inObjectUrl isEqualToString:kkeyClassGetConnTrackingMeList]) {
            successNotification = kKeyNotificationConnTrackMeSucceed;
        } else if ([inObjectUrl containsString:kkeyClassUpdateNotificationPlace]) {
            successNotification = kKeyNotificationUpdateNotificationPlaceSucceed;
        }else if([inObjectUrl containsString:kkeyAllNotificationControl]){
            successNotification = kKeyToggleAllNotificationSuccess;
        }else if ([inObjectUrl isEqualToString:@"tracking/0"]) {
            successNotification = kKeyNotificationConnImTrackSucceed;
        }else if ([inObjectUrl containsString:kkeyClassAllowCancelTracking]) {
            if([inObjectUrl containsString:kkeyDeleteTrackingOnly]){
                successNotification = kKeyNotificationDeleteTrackingSuccess;
            }else{
                successNotification = kKeyNotificationAllowCancelTrackSucceed;
            }
        } else if ([inObjectUrl isEqualToString:kkeyClassGetImTrackingList]) {
            if ([resultObject isKindOfClass:[NSString class]] && [resultObject isEqualToString:@"Tracking saved successfully."]) {
                successNotification = kKeyNotificationSaveTrackLocationSucceed;
            } else
                successNotification = kKeyNotificationConnImTrackSucceed;
        }else if ([inObjectUrl isEqualToString:kkeyClassAddTrackingLocation]) {
            if ([resultObject isKindOfClass:[NSDictionary class]] && [message isEqualToString:@"added successfully"]) {
                successNotification = kKeyNotificationAddMyTrackLocationsSucceed;
            } else if ([resultObject isKindOfClass:[NSDictionary class]]) {
                successNotification = kKeyNotificationGetMyTrackLocationsSucceed;
            }
        } else if ([inObjectUrl rangeOfString:kkeyClassGetTrackLocListOtherUser].location != NSNotFound) {
            successNotification = kKeyNotificationGetTrackLocationsSucceed;
        } else if ([inObjectUrl rangeOfString:kkeyClassUpdateTrackLocOtherUser].location != NSNotFound) {
            if ([method.lowercaseString isEqualToString:@"delete"]) {
                successNotification = kKeyNotificationDeleteMyTrackLocationsSucceed;
            } else
                successNotification = kKeyNotificationUpdateTrackLocationSucceed;
        } else if ([inObjectUrl containsString:kkeyClassAllowNotification]) {
            successNotification = kKeyNotificationAllowTrackMeSucceed;
        }
    }
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:successNotification object:resultObject userInfo:inPostInfo];
    
    if ([[[inPostInfo objectForKey:kKeyRequestedParams] objectForKey:kKeyActivityIndicatorState] boolValue] == 1) {
        //[APP_DELEGATE hideActivityIndicator];
    }
}

// ------------------------------------------------------------------------------------------------------------
// processFailureResult:

- (void)processFailureResult:(NSDictionary *)inPostInfo {
    [APP_DELEGATE hideActivityIndicator];
    NSString *failureNotification = nil;
    id resultObject = [inPostInfo objectForKey:kKeyResponseData];

    if ([[[NSString stringWithFormat:@"%@", resultObject] lowercaseString] isEqualToString:@"unauthenticated"]){
        if ([APP_DELEGATE isLoggedIn]){
            (APP_DELEGATE).isLogoutCall = YES;
            // Call api for logout
            [APP_DELEGATE logoutIfNotVerified];
            [SRModalClass showAlertWithTitle:@"We have terminated you're session for Account Security." alertTitle:@"Account Verification"];
        }
       
        return;
    }
    

    
    if ([resultObject isKindOfClass:[NSArray class]] && [(NSArray *) resultObject count] > 0) {
        resultObject = [resultObject objectAtIndex:0];
    }
    NSString *inObjectUrl = [inPostInfo objectForKey:kKeyObjectUrl];
    NSString *errorMessage;
    if ([inObjectUrl isEqualToString:kKeyClassSendSMS]) {
        errorMessage = kKeyNotificationInviteUserFailed;
    }//jonish history
    else if ([inObjectUrl isEqualToString:kkeyLocationHistory]){
        errorMessage = kKeylocationHistoryFailure;
    }//jonish history
    else if ([resultObject isKindOfClass:[NSString class]]) {
        errorMessage = resultObject;
    } else if ([resultObject isKindOfClass:[NSDictionary class]]) {
        errorMessage = [resultObject objectForKey:kKeyResponseData];
    }
    
    if (errorMessage == nil && [resultObject isKindOfClass:[NSDictionary class]]) {
        errorMessage = [resultObject objectForKey:kKeyMessage];
    }
    if (inObjectUrl != nil) {
        
        
        if ([inObjectUrl isEqualToString:kKeyClassApproveRejectContact]) {
            failureNotification = kKeyContactNotificationUpdateNotificationFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassBlockdeleteConnectionuser]) {
            failureNotification = kKeyNotificationGettDeleteBlockConnectionFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassContactSendConnectionRequest]) {
            failureNotification = kKeyNotificationContactSendConnectionRequestFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassUserContactsGet]) {
            failureNotification = kKeyNotificationUserSocialContactGetFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassVerifydeviceCode]) {
            failureNotification = kKeyNotificationUserDeviceVerifyFailed;
        }else if ([inObjectUrl isEqualToString:kKeyUserDeviceVerifications]) {
            failureNotification = kKeyNotificationSendEmailFail;
        }else if ([inObjectUrl isEqualToString:kKeyClassContactUs]) {
            failureNotification = KkeyNotificationMailFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassGetCountryList]) {
            failureNotification = kKeyNotificationCountryListFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassRegisterUser]) {
            failureNotification = kKeyNotificationRegisterUserFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassRegisterUser].location != NSNotFound && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"verify"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"login"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"import"] && ![[inObjectUrl componentsSeparatedByString:@"/"] containsObject:@"map-users"]) {
            
            if ([resultObject isKindOfClass:[NSString class]] && ([resultObject isEqualToString:NSLocalizedString(@"txt.error.user_delete", @"")] || [resultObject isEqualToString:NSLocalizedString(@"txt.error.delete_account", @"")])) {
                failureNotification = kKeyNotificationDeleteAccountFailed;
            } else if ([inObjectUrl containsString:kKeyClassGetAllRadarMapUsers]) {
                failureNotification = kKeyNotificationGetAllRadarMapUsersFailed;
            } else if ([resultObject isKindOfClass:[NSDictionary class]]) {
                if ([[self.loggedInUserInfo objectForKey:kKeyId] isEqualToString:[resultObject objectForKey:kKeyId]]) {
                    
                    [self userObjectProcessed:resultObject];
                    failureNotification = kKeyNotificationSignUPFailed;
                }
            } else if ([inObjectUrl containsString:kKeyClassSendVerifyCode]) {
                //[self userObjectProcessed:resultObject];
                failureNotification = kKeyNotificationSignUPFailed;
            }else if ([inObjectUrl rangeOfString:kKeyClassRegisterUser].location != NSNotFound && [[inObjectUrl componentsSeparatedByString:@"/"] count] > 0){
                failureNotification = kKeyNotificationCompleteSignUPFailed;
            }else {
                failureNotification = kKeyNotificationGetUserProfileFailed;
            }
        } else if ([inObjectUrl isEqualToString:kKeyClassForgotPassword] || [inObjectUrl containsString:kKeyClassForgotPassword]) {
            if ([[NSString stringWithFormat:@"%@", resultObject] isEqualToString:NSLocalizedString(@"txt.error.forgot_password", @"")] || [[NSString stringWithFormat:@"%@", resultObject] isEqualToString:NSLocalizedString(@"txt.error.countryCode_Match", @"")]) {
                failureNotification = kKeyNotificationGetForgotPasswordCodeFailed;
            } else
                failureNotification = kKeyNotificationForgotPasswordFailed;
            
        } else if ([inObjectUrl containsString:kKeyClassVerifyCode]) {
            failureNotification = kKeyNotificationVerifyCodeFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassLogin]) {
            failureNotification = kKeyNotificationLoginFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassLogout]) {
            failureNotification = kKeyNotificationLogoutFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassContactUs]) {
            failureNotification = KkeyNotificationMailFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassUserAlbum]) {
            failureNotification = kKeyNotificationUploadUserAlbumFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassfavourites]) {
            failureNotification = kKeyNotificationAddFavouritesFailed;
            
        } else if ([inObjectUrl isEqualToString:kKeyClassInvisible]) {
            failureNotification = kKeyNotificationAddInvisibleFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassUpdateLocation].location != NSNotFound) {
            failureNotification = kKeyNotificationUpdateLocationFailed;
        } else if ([inObjectUrl containsString:kKeyClassGetMapUsersDetails]) {
            failureNotification = kKeyNotificationGetMapUsersDetailsFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassGetRadarUsers]) {
            failureNotification = kKeyNotificationGetRadarUsersFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassSearchUser].location != NSNotFound) {
            failureNotification = kKeyNotificationSearchUserFailed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassSearchUsersById].location != NSNotFound) {
            failureNotification = kKeyNotificationSearchUsersByIdFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassAddConnection]) {
            failureNotification = kKeyNotificationAddConnectionFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassUpdateConnection].location != NSNotFound) {
            failureNotification = kKeyNotificationUpdateConnectionFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetConnection].location != NSNotFound) {
            failureNotification = kKeyNotificationUpdateConnectionFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetBlockedConnection].location != NSNotFound) {
            failureNotification = kKeyNotificationGetBlockedUsersFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassGetDeleteConnection]) {
            failureNotification = kKeyNotificationGetDeletedUsersFailed;
        }
        else if ([inObjectUrl rangeOfString:kKeyClassDeleteBlockConnection].location != NSNotFound) {
            failureNotification = kKeyNotificationDeleteBlockConnectionFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetNotification].location != NSNotFound) {
            failureNotification = kKeyNotificationGetNotificationFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetTrackingNotification].location != NSNotFound) {
            failureNotification = kKeyNotificationGetTrackingNotificationFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassUpdateNotification].location != NSNotFound) {
            failureNotification = kKeyNotificationUpdateNotificationFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassCreatePins]) {
            failureNotification = kKeyNotificationCreatePinFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassGetPins].location != NSNotFound) {
            failureNotification = kKeyNotificationGetPinsFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassDeletePin].location != NSNotFound) {
            failureNotification = kKeyNotificationDeletePinFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassCreateGroup]) {
            failureNotification = kKeyNotificationCreateGroupFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassDeleteGroupMember].location != NSNotFound) {
            failureNotification = kKeyNotificationAcceptRejectGroupMemberFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassDeleteGroupMember].location != NSNotFound && [resultObject isEqualToString:NSLocalizedString(@"txt.error.not_found_member", @"")]) {
            failureNotification = kKeyNotificationDeleteGroupMemberFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassCreateGroup].location != NSNotFound) {
            failureNotification = kKeyNotificationCreateGroupFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassFamily].location != NSNotFound) {
            failureNotification = kKeyNotificationFamilyActionFailed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassEvent].location != NSNotFound) {
            failureNotification = kKeyNotificationGetEventFailed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassEventAttendees].location != NSNotFound) {
            failureNotification = kKeyNotificationAcceptRejectEventFailed;
            
        } else if ([inObjectUrl rangeOfString:kKeyClassCreateEvent].location != NSNotFound) {
            failureNotification = kKeyNotificationCreateEventFailed;
        } else if ([inObjectUrl rangeOfString:kKeyClassSetting].location != NSNotFound) {
            failureNotification = kKeyNotificationSettingUpdateFailed;
            
        } else if ([inObjectUrl isEqualToString:kKeyClassNotifyIn]) {
            failureNotification = kKeyNotificationNotifyInTrackingNotificationFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassEventNotify]) {
            failureNotification = kKeyNotificationEventInOutFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassNotifyOut]) {
            failureNotification = kKeyNotificationNotifyOutTrackingNotificationFailed;
        } else if ([inObjectUrl isEqualToString:kKeyClassUploadContacts]) {
            failureNotification = kKeyNotificationContactUploadFailed;
        }else if ([inObjectUrl isEqualToString:kKeyClassUserContactsStore]) {
            failureNotification = kKeyNotificationUserContactStoreFailed;
        }else if ([inObjectUrl containsString:kkeyClassUpdateNotificationPlace]) {
            failureNotification = kKeyNotificationUpdateNotificationPlaceFailed;
        } else if ([inObjectUrl containsString:kkeyClassAllowCancelTracking]) {
            failureNotification = kKeyNotificationAllowCancelTrackFailed;
        } else if ([inObjectUrl isEqualToString:kkeyClassGetConnTrackingMeList]) {
            failureNotification = kKeyNotificationConnTrackMeFailed;
        } else if ([inObjectUrl isEqualToString:kkeyClassGetImTrackingList]) {
            failureNotification = kKeyNotificationConnImTrackFailed;
        } else if ([inObjectUrl isEqualToString:kkeyClassAddTrackingLocation]) {
            failureNotification = kKeyNotificationSaveTrackLocationFailed;
        } else if ([inObjectUrl rangeOfString:kkeyClassGetTrackLocListOtherUser].location != NSNotFound) {
            failureNotification = kKeyNotificationGetTrackLocationsFailed;
        } else if ([inObjectUrl rangeOfString:kkeyClassUpdateTrackLocOtherUser].location != NSNotFound) {
            failureNotification = kKeyNotificationUpdateTrackLocationFailed;
        } else if ([inObjectUrl containsString:kkeyClassAllowNotification]) {
            failureNotification = kKeyNotificationAllowTrackMeFailed;
        }else if([inObjectUrl containsString:kkeyDeleteTrackingOnly]){
            failureNotification = kKeyNotificationDeleteTrackingFailure;
        }else if([inObjectUrl containsString:kkeyAllNotificationControl]){
            failureNotification = kKeyToggleAllNotificationFailure;
        }else if ([inObjectUrl containsString:kKeyOnOffNotification]){
            failureNotification = kKeyturnOnOffNotificationNotifyFailed;
            }
    }
    // Post notification
    if ([inObjectUrl rangeOfString:kKeyClassGetNotification].location == NSNotFound && [inObjectUrl rangeOfString:kKeyClassGetTrackingNotification].location == NSNotFound) {
        [[NSNotificationCenter defaultCenter] postNotificationName:failureNotification object:errorMessage userInfo:inPostInfo];
    }
    //    if ([[[inPostInfo objectForKey:kKeyRequestedParams]objectForKey:kKeyActivityIndicatorState]boolValue] == 1) {
    //
    //        [APP_DELEGATE hideActivityIndicator];
    //    }
}

// ------------------------------------------------------------------------------------------------------------
// userObjectProcessed:

- (void)userObjectProcessed:(NSDictionary *)inResultObject {
    [self.loggedInUserInfo removeAllObjects];
    [self.loggedInUserInfo addEntriesFromDictionary:[SRModalClass removeNullValuesFromDict:inResultObject]];
    
    if (self.loggedInUserInfo.count > 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:self.loggedInUserInfo forKey:@"loggedInUesrInfo"];
        [userDefaults synchronize];
    }
    
    //Check user age is between 13 to 18
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateOfBirth = [dateFormatter dateFromString:[self.loggedInUserInfo objectForKey:@"dob"]];
    NSDate *now = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:dateOfBirth
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    
    if (age >= 13 && age < 18) {
        
        [APP_DELEGATE setIsUserBelow18:YES];
    } else {
        
        [APP_DELEGATE setIsUserBelow18:NO];
    }
    
    
    // Download the image and cache it
    NSDictionary *profileImgDict = [inResultObject objectForKey:kKeyProfileImage];
    if ([profileImgDict isKindOfClass:[NSDictionary class]]) {
        if ([profileImgDict objectForKey:kKeyImageName]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                UIImage *img = nil;
                NSString *imageName = [[inResultObject objectForKey:kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                    img = [UIImage imageWithData:
                           [NSData dataWithContentsOfURL:
                            [NSURL URLWithString:imageUrl]]];
                    NSData *imgData = UIImageJPEGRepresentation(img, 1.0);
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationProfileImageCached object:imgData];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (img) {
                    }
                });
            });
        }
    }
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
    return YES;
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    //    NSURLProtectionSpace *protectionSpace = [challenge protectionSpace];
    //    id<NSURLAuthenticationChallengeSender> sender = [challenge sender];
    //    if ([[protectionSpace authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust]) {
    //        SecTrustRef trust = [[challenge protectionSpace] serverTrust];
    //        NSURLCredential *credential = [[NSURLCredential alloc] initWithTrust:trust];
    //        [sender useCredential:credential forAuthenticationChallenge:challenge];
    //    } else {
    //        [sender performDefaultHandlingForAuthenticationChallenge:challenge];
    //    }
    if ([[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodServerTrust) {
        [[challenge sender] useCredential:[NSURLCredential credentialForTrust:[[challenge protectionSpace] serverTrust]] forAuthenticationChallenge:challenge];
    }
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
//        //        if ([trustedHosts containsObject:challenge.protectionSpace.host])
//        
//        OSStatus                err;
//        NSURLProtectionSpace *  protectionSpace;
//        SecTrustRef             trust;
//        SecTrustResultType      trustResult;
//        BOOL                    trusted;
//        
//        protectionSpace = [challenge protectionSpace];
//        assert(protectionSpace != nil);
//        
//        trust = [protectionSpace serverTrust];
//        assert(trust != NULL);
//        err = SecTrustEvaluate(trust, &trustResult);
//        trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
//        
//        // If that fails, apply our certificates as anchors and see if that helps.
//        //
//        // It's perfectly acceptable to apply all of our certificates to the SecTrust
//        // object, and let the SecTrust object sort out the mess.  Of course, this assumes
//        // that the user trusts all certificates equally in all situations, which is implicit
//        // in our user interface; you could provide a more sophisticated user interface
//        // to allow the user to trust certain certificates for certain sites and so on).
//        
//        if ( ! trusted ) {
//            err = SecTrustSetAnchorCertificates(trust, (CFArrayRef) [Credentials sharedCredentials].certificates);
//            if (err == noErr) {
//                err = SecTrustEvaluate(trust, &trustResult);
//            }
//            trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
//        }
//        if(trusted)
//            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
//    }
//}

@end
