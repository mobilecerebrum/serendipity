#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CalloutAnnotationView.h"
#import "MKMapView+ZoomLevel.h"
#import "SRProfileImageView.h"
#import "SRMapWindowView.h"
#import "SRChatViewController.h"
#import "ASValueTrackingSlider.h"
#import "REVClusterMapView.h"
#import "REVClusterPin.h"
#import "SRPanoramaViewController.h"

@interface SRGroupMapViewController : ParentViewController <CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate, CalloutAnnotationViewDelegate,ASValueTrackingSliderDataSource,UIAlertViewDelegate>
{ // Instance Variables
	NSArray *sourceArr;
    NSArray *searchArray;

	NSMutableArray *nearByUsersList;
	NSMutableArray *userProfileViewArr;
	NSArray *selectedCombinatonArr;
	NSArray *dotsUserPicsArr;

	CLLocation *myLocation;
    CLPlacemark *placemark;

	//PinAnnotation *myPinAnnotation;
    REVClusterPin *pin;

	// Flags
	BOOL isZoomIn;
	BOOL isSelectAnnotation;
    BOOL isSlide, isPanoramaLoaded;
	BOOL isCheckZoomLvl;
	BOOL isCountAdd;

	// Server
	SRServerConnection *server;
    NSString *distanceUnit;
    NSInteger mapZoomLevel;
    float latestDistance;
    IBOutlet UIButton *btnSatelite;
    IBOutlet UIButton *btnStreet;
    IBOutlet UIButton *zoomInBtn,*zoomOutBtn,*streetViewBtn;
    CLLocationCoordinate2D panoramaLastLoc;
}

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (strong, nonatomic) IBOutlet UIToolbar *locationToolBar;
@property (weak, nonatomic) IBOutlet REVClusterMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *layerView;
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchField;
@property (strong, nonatomic) SRProfileImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnSatelite,*btnStreet;
// Slider
@property (nonatomic, strong) IBOutlet UIView *progressContainerView;
@property (nonatomic, strong) IBOutlet UILabel *lblInPpl;
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider1;
// Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
