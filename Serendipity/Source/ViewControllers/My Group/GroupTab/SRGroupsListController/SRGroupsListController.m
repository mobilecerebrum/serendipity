#import "SRModalClass.h"
#import "SRGroupsViewCell.h"
#import "SRConnectionNotificationCell.h"
#import "SRGroupDetailTabViewController.h"
#import "SRGroupsListController.h"
#import "SRMapViewController.h"
#import "CommonFunction.h"

@implementation SRGroupsListController


#pragma mark
#pragma mark Init Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SRGroupsListController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupSucceed:)
                              name:kKeyNotificationGroupRadarChanges object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupSucceed:)
                              name:kKeyNotificationAcceptRejectGroupMemberSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupFailed:)
                              name:kKeyNotificationAcceptRejectGroupMemberFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(DeleteGroupMemberSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(DeleteGroupMemberFailed:)
                              name:kKeyNotificationCreateGroupFailed object:nil];
    }

    //return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call Super
    [super viewDidLoad];

    // Overlay image view
    overlayImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.objTableView.frame.origin.y, self.view.frame.size.width, self.objTableView.frame.size.height)];
    overlayImgView.alpha = 0.4;
    overlayImgView.userInteractionEnabled = YES;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = overlayImgView.bounds;
        gradient.colors = @[(id) [[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0] CGColor], (id) [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.8] CGColor]];
        [overlayImgView.layer insertSublayer:gradient atIndex:0];
    });

    // Set profile photo
    [SRModalClass setUserImage:server.loggedInUserInfo inSize:nil applyToView:self.imgBGPhoto];

    self.imgTxtSearchBck.layer.cornerRadius = 5.0;

    // Table view
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.objTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Set search icon to textfield
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    container.backgroundColor = [UIColor clearColor];

    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(25, 4, 20, 20)];
    searchIcon.image = [UIImage imageNamed:@"ic_search"];
    [container addSubview:searchIcon];

    self.txtSearchField.leftView = container;
    self.txtSearchField.leftViewMode = UITextFieldViewModeAlways;

    self.txtSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.searchText.txt", "") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtSearchField.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    (APP_DELEGATE).topBarView.hidden = YES;
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;
}
// --------------------------------------------------------------------------------
// searchFriendForSearchText:

- (void)searchFriendForSearchText:(NSString *)searchText {
    NSString *searchTextStr = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if ([searchTextStr length] > 0) {
        NSString *filter = @"%K CONTAINS [cd] %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyName, searchText];
        youInvitedGrpArr = [sourceInvitedGrpArr filteredArrayUsingPredicate:predicate];
        yourGroupListArr = [sourceYourGrpArr filteredArrayUsingPredicate:predicate];
        popularGroupListArr = [sourcePopularGrpArr filteredArrayUsingPredicate:predicate];

        [self.objTableView reloadData];
    } else {
        youInvitedGrpArr = sourceInvitedGrpArr;
        yourGroupListArr = sourceYourGrpArr;
        popularGroupListArr = sourcePopularGrpArr;
        [self.objTableView reloadData];
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {

    [self.navigationController popToViewController:self.delegate animated:NO];
}

// ---------------------------------------------------------------------------------------
// plusBtnAction:

- (void)plusBtnAction:(id)sender {
    SRNewGroupViewController *newGroupCon = [[SRNewGroupViewController alloc] initWithNibName:nil bundle:nil dict:nil server:nil];
    newGroupCon.delegate = self;
    [self.navigationController pushViewController:newGroupCon animated:YES];
}

//
// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTappedCaptured:(UIButton *)sender {
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.objTableView]; // maintable --> replace your tableview name
    NSIndexPath *clickedButtonIndexPath = [self.objTableView indexPathForRowAtPoint:touchPoint];

    NSMutableDictionary *userDict;
    if (clickedButtonIndexPath.section == 0) {
        userDict = youInvitedGrpArr[clickedButtonIndexPath.row];
    } else if (clickedButtonIndexPath.section == 1) {
        userDict = yourGroupListArr[clickedButtonIndexPath.row];
    } else
        userDict = popularGroupListArr[clickedButtonIndexPath.row];

    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:@"FromGroupListView" bundle:nil inDict:userDict server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

#pragma mark
#pragma mark TextField Delegate method
#pragma mark

// --------------------------------------------------------------------------------
//  textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    // Search
    [self searchFriendForSearchText:textField.text];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self searchFriendForSearchText:str];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldShouldBeginEditing:

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Add overlay view
    //[self.view addSubview:overlayImgView];
    self.objTableView.scrollEnabled = NO;

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Add overlay view
    //[self.view addSubview:overlayImgView];
    self.objTableView.scrollEnabled = NO;
}

// --------------------------------------------------------------------------------
// textFieldShouldEndEditing:

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // Remove  overlay view from view
    //[overlayImgView removeFromSuperview];
    self.objTableView.scrollEnabled = YES;

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidEndEditing

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // Search
    [self searchFriendForSearchText:nil];

   // [overlayImgView removeFromSuperview];
    [textField resignFirstResponder];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txtSearchField resignFirstResponder];
   // [overlayImgView removeFromSuperview];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

// ---------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    if (section == 0 && ![APP_DELEGATE isUserBelow18]) {
        count = [youInvitedGrpArr count];
    } else if (section == 1) {
        count = [yourGroupListArr count];
    } else if (![APP_DELEGATE isUserBelow18]){
        count = [popularGroupListArr count];
    }
    return count;
}

// ---------------------------------------------------------------------------------------
// cellForRowAtIndexPath:


- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SRGroupsViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRGroupsViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            cell = (SRGroupsViewCell *) nibObjects[0];
        }
    }

    // Your groups
    NSDictionary *groupDict;
    if (indexPath.section == 0) {
        groupDict = youInvitedGrpArr[indexPath.row];
    } else if (indexPath.section == 1) {
        groupDict = yourGroupListArr[indexPath.row];
    } else {
        groupDict = popularGroupListArr[indexPath.row];
    }
    groupDict = [SRModalClass removeNullValuesFromDict:groupDict];

    // Name
    cell.lblProfileName.text = [groupDict valueForKey:kKeyName];

    // Address
    cell.lblAddress.text = [groupDict valueForKey:kKeyAddress];

    // Show Count of Group Members
    cell.lblMembers.text = [NSString stringWithFormat:@"%lu members", (unsigned long) [[groupDict valueForKey:kKeyGroupMembers] count]];

    //Call Google API for correct distance and address from two locations
    if (server.myLocation != nil && [groupDict isKindOfClass:[NSDictionary class]]) {
        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[groupDict valueForKey:kKeyLattitude] floatValue] longitude:[[groupDict valueForKey:kKeyRadarLong] floatValue]];
        NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.timeoutInterval = 60;
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if (!error) {
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                       if ([self IsValidResponse:data]) {
                                           NSDictionary *dictOSM = [json valueForKey:@"paths"];
                                           NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                                           NSString *distance = arrdistance[0];
                                           double convertDist = [distance doubleValue];
                                           if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                               //meters to killometer
                                               convertDist = convertDist / 1000;
                                           } else {
                                               //meters to miles
                                               convertDist = (convertDist * 0.000621371192);
                                           }
                                           //Correct distance
                                           cell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit];
                                       }
                                   } else {
                                       cell.lblDistanceLabel.text = [NSString stringWithFormat:@"%@ %@", [groupDict valueForKey:kKeyDistance], distanceUnit];
                                   }
                               }];
    } else {
        cell.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        cell.lblDistanceLabel.text = [NSString stringWithFormat:@"0.0 %@", distanceUnit];
    }

    if ([cell.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {
        cell.lblDistanceLabel.text = [NSString stringWithFormat:@"0.0 %@", distanceUnit];
    }

    // Private or public
    if ([[groupDict valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
        cell.lblAccess.text = NSLocalizedString(@"lbl.public.txt", "");
    } else {
        cell.lblAccess.text = NSLocalizedString(@"lbl.private.txt", "");
    }

    // Adjust green dot label appearence
    cell.imgGroups.layer.cornerRadius = cell.imgGroups.frame.size.width / 2;
    cell.imgGroups.layer.masksToBounds = YES;
    cell.imgGroups.clipsToBounds = YES;

    // For groupMember image using sd image cache
    id mediaDict = groupDict[kKeyMediaImage];
    if ([mediaDict isKindOfClass:[NSDictionary class]] && [mediaDict objectForKey:kKeyImageName] != nil) {
        NSString *imageName = [mediaDict objectForKey:kKeyImageName];
        if ([imageName length] > 0) {

            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

            [cell.imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        } else {
            cell.imgProfilePhoto.backgroundColor = [UIColor orangeColor];
            cell.imgProfilePhoto.image = [UIImage imageNamed:@"group-white-50px.png"];
        }
    } else {
        cell.imgProfilePhoto.backgroundColor = [UIColor orangeColor];
        cell.imgProfilePhoto.image = [UIImage imageNamed:@"group-white-50px.png"];
    }

    //TODO:
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude, server.myLocation.coordinate.longitude};

    CLLocationCoordinate2D userLoc = {[[groupDict valueForKey:kKeyLattitude] floatValue], [[groupDict valueForKey:kKeyRadarLong] floatValue]};

    [cell.btnDirection addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDirection.tag = indexPath.row;

    NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
    if (directionValue == kKeyDirectionNorth) {
        cell.lblCompassDirection.text = @"N";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionEast) {
        cell.lblCompassDirection.text = @"E";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionSouth) {
        cell.lblCompassDirection.text = @"S";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionWest) {
        cell.lblCompassDirection.text = @"W";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionNorthEast) {
        cell.lblCompassDirection.text = @"NE";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionNorthWest) {
        cell.lblCompassDirection.text = @"NW";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionSouthEast) {
        cell.lblCompassDirection.text = @"SE";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionSoutnWest) {
        cell.lblCompassDirection.text = @"SW";
        [cell.btnDirection setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
    }

    cell.btnDecline.layer.cornerRadius = 5;
    cell.btnDecline.frame = CGRectMake(cell.btnDecline.frame.origin.x, cell.btnDecline.frame.origin.y, 50, 18);
    cell.btnDecline.layer.borderWidth = 0.5;
    cell.btnDecline.layer.borderColor = [[UIColor redColor] CGColor];
    cell.btnDecline.layer.masksToBounds = YES;

    // Show your are member or not
    cell.declineGroupShadow.hidden = YES;
    cell.declineGroupShadow.userInteractionEnabled = NO;
    NSString *loggedInUser = server.loggedInUserInfo[kKeyId];
    for (int i = 0; i < [groupDict[kKeyGroupMembers] count]; i++) {
        if ([[[groupDict[kKeyGroupMembers] objectAtIndex:i] valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
            if ([[[groupDict[kKeyGroupMembers] objectAtIndex:i] valueForKey:kKeyInvitationStatus] isEqualToString:@"1"]) {
                [cell.btnDecline setTitle:NSLocalizedString(@"lbl.group_member.txt", @"") forState:UIControlStateNormal];
                [cell.btnDecline setTitleColor:[UIColor colorWithRed:1 green:0.647 blue:0 alpha:1] forState:UIControlStateNormal];
                cell.btnDecline.layer.cornerRadius = 0;
                cell.btnDecline.layer.borderColor = [[UIColor clearColor] CGColor];
                cell.btnDecline.frame = CGRectMake(cell.btnDecline.frame.origin.x, cell.btnDecline.frame.origin.y, 120, 20);
                if ([[groupDict valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
                    cell.imgGroups.backgroundColor = [UIColor blackColor];
                    cell.imgGroups.image = [UIImage imageNamed:@"group-white-50px.png"];
                } else {
                    cell.imgGroups.image = [UIImage imageNamed:@"group-white-50px.png"];
                    cell.imgGroups.backgroundColor = [UIColor orangeColor];
                }

            } else if ([[[groupDict[kKeyGroupMembers] objectAtIndex:i] valueForKey:kKeyInvitationStatus] isEqualToString:@"2"]) {
                [cell.btnDecline setTitle:NSLocalizedString(@"You declined", @"") forState:UIControlStateNormal];
                [cell.btnDecline setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                cell.btnDecline.layer.cornerRadius = 0;
                cell.btnDecline.layer.borderColor = [[UIColor clearColor] CGColor];
                cell.btnDecline.frame = CGRectMake(cell.btnDecline.frame.origin.x, cell.btnDecline.frame.origin.y, 120, 20);
                cell.imgGroups.image = [UIImage imageNamed:@"group-add-Nav.png"];
                cell.imgGroups.backgroundColor = [UIColor grayColor];
                [cell.btnDirection setTintColor:[UIColor whiteColor]];
                cell.declineGroupShadow.hidden = NO;
                cell.userInteractionEnabled = NO;
            } else {
                [cell.btnDecline setTitle:NSLocalizedString(@"  Decline", @"") forState:UIControlStateNormal];
                [cell.btnDecline setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                cell.btnDecline.layer.cornerRadius = 5;
                cell.btnDecline.layer.borderColor = [[UIColor redColor] CGColor];
                cell.btnDecline.frame = CGRectMake(cell.btnDecline.frame.origin.x, cell.btnDecline.frame.origin.y, 50, 18);
                if ([[groupDict valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
                    cell.imgGroups.backgroundColor = [UIColor blackColor];
                    cell.imgGroups.image = [UIImage imageNamed:@"group-add-Nav.png"];
                } else {
                    cell.imgGroups.image = [UIImage imageNamed:@"group-add-Nav.png"];
                    cell.imgGroups.backgroundColor = [UIColor orangeColor];
                }
            }
        }
    }
    // Selection style
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];

    // Return
    return cell;
}

#pragma mark
#pragma mark TableView Swipable button Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// canEditRowAtIndexPath:
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    return YES;
}

// ----------------------------------------------------------------------------------------------------------------------
// commitEditingStyle:
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //Nothing gets called here if you invoke `tableView:editActionsForRowAtIndexPath:` according to Apple docs so just leave this method blank
}

// ----------------------------------------------------------------------------------------------------------------------
// editActionsForRowAtIndexPath:
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *buttonArr;

    currentIndex = indexPath.row;
    if (indexPath.section == 0) {
        groupData = youInvitedGrpArr[indexPath.row];
        NSString *title;
        if ([[groupData valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
            title = @"Join";
        } else
            title = @"Request to Join";

        UITableViewRowAction *joinGroup = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:title handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            BOOL alreadySent = NO;
            NSArray *grpMembersArray = groupData[kKeyGroupMembers];
            for (int i = 0; i < [grpMembersArray count]; i++) {
                NSDictionary *userDict = grpMembersArray[i];
                if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && [[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"3"]) {
                    alreadySent = YES;
                }
            }
            if (alreadySent) {
                [SRModalClass showAlert:@"Your have already requested to join this group and your initial request is still pending acceptance."];
            } else {
                NSString *imageName = @"";
                if ([groupData[@"image"] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *dictImage = groupData[@"image"];
                    imageName = dictImage[@"file"];

                }
                dictPram_CometChat = @{kKeyGroupID: groupData[@"chat_room_id"], @"groupName": groupData[@"name"], @"groupPwd": @"", @
                                                                                                                                    "grouppic": imageName};
                NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassDeleteGroupMember];
                NSDictionary *paramDict = @{kKeyGroupID: groupData[kKeyId], kKeyUserID: [server.loggedInUserInfo valueForKey:kKeyId]};
                [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPOST];
            }

        }];
        joinGroup.backgroundColor = [UIColor orangeColor];
        buttonArr = @[joinGroup];
    } else {
        groupData = yourGroupListArr[indexPath.row];
        NSString *loggedInUser = server.loggedInUserInfo[kKeyId];

        if ([[groupData valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
            UITableViewRowAction *deleteGroup = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                // Delete group
                [[[UIAlertView alloc] initWithTitle:@"Group" message:@"Are you sure you want to delete this group?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
            }];
            deleteGroup.backgroundColor = [UIColor redColor];
            buttonArr = @[deleteGroup];
        } else {
            NSString *userGroupId = @"";
            NSArray *grpMembersArray = groupData[kKeyGroupMembers];
            BOOL canLeaveGroup = NO;
            for (int i = 0; i < [grpMembersArray count]; i++) {
                NSDictionary *userDict = grpMembersArray[i];
                if ([[userDict valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
                    userGroupId = userDict[kKeyId];
                    if ([[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"]) {
                        if ([[userDict valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
                            canLeaveGroup = NO;
                        } else
                            canLeaveGroup = YES;
                    }

                    if (canLeaveGroup) {
                        UITableViewRowAction *leaveGroup = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Leave" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                            NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteGroupMember, userGroupId];
                            NSDictionary *paramDict = @{kKeyGroupID: groupData[kKeyId], kKeyUserID: loggedInUser, kKeyStatus: @"2"};
                            [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];

                        }];
                        leaveGroup.backgroundColor = [UIColor redColor];
                        buttonArr = @[leaveGroup];
                    } else {
                        if ([[userDict valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
                            UITableViewRowAction *deleteGroup = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                // Delete group
                                [[[UIAlertView alloc] initWithTitle:@"Group" message:@"Are you sure you want to delete this group?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
                            }];
                            deleteGroup.backgroundColor = [UIColor redColor];
                            buttonArr = @[deleteGroup];

                        } else {
                            (APP_DELEGATE).selectedGroupId = [[NSString alloc] init];
                            (APP_DELEGATE).selectedGroupId = groupData[kKeyId];
                            UITableViewRowAction *acceptGroup = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Accept" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                isJoined = YES;
                                NSString *imageName = @"";
                                if ([groupData[@"image"] isKindOfClass:[NSDictionary class]]) {
                                    NSDictionary *dictImage = groupData[@"image"];
                                    imageName = dictImage[@"file"];

                                }
                                dictPram_CometChat = @{kKeyGroupID: groupData[@"chat_room_id"], @"groupName": groupData[@"name"], @"groupPwd": @"", @"grouppic": imageName};
                                NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteGroupMember, userGroupId];
                                NSDictionary *paramDict = @{kKeyGroupID: groupData[kKeyId], kKeyUserID: loggedInUser, kKeyStatus: @"1"};
                                [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                            }];
                            acceptGroup.backgroundColor = [UIColor orangeColor];

                            UITableViewRowAction *rejectGroup = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Reject" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteGroupMember, userGroupId];
                                NSDictionary *paramDict = @{kKeyGroupID: groupData[kKeyId], kKeyUserID: loggedInUser, kKeyStatus: @"2"};
                                [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                            }];
                            rejectGroup.backgroundColor = [UIColor orangeColor];
                            buttonArr = @[rejectGroup, acceptGroup];
                        }
                    }
                }
            }
        }
    }
    return buttonArr;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *groupDict;
    if (indexPath.section == 0) {
        groupDict = youInvitedGrpArr[indexPath.row];
    } else if (indexPath.section == 1)
        groupDict = yourGroupListArr[indexPath.row];
    else
        groupDict = popularGroupListArr[indexPath.row];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:groupDict];
    if ([groupDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [groupDict[kKeyMediaImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsGroupPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    [dictionary setObject:[NSString stringWithFormat:@"1"] forKey:@"isGroup"];
    server.groupDetailInfo = [SRModalClass removeNullValuesFromDict:dictionary];
    SRGroupDetailTabViewController *objGroupTab = [[SRGroupDetailTabViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:objGroupTab animated:YES];

    // Search settings
    [self.view endEditing:YES];
    self.objTableView.hidden = NO;
    self.btnSearchIcon.hidden = NO;
}

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 97;
}

// ----------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    headerView.backgroundColor = [UIColor clearColor];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    imgView.alpha = 0.8;
    [headerView addSubview:imgView];

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, tableView.bounds.size.width, 18)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:kFontHelveticaMedium size:15];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];

    if (section == 0) {
        //Groups going in this area (Invited Groups)
        [imgView setBackgroundColor:[UIColor colorWithRed:102.0 / 255.0f green:77 / 255.0f blue:66 / 255.0f alpha:0.8]];
        label.text = NSLocalizedString(@"header.title.group_going_in_area.txt", "");
    } else if (section == 1) {
        //Your groups
        [imgView setBackgroundColor:[UIColor colorWithRed:102.0 / 255.0f green:77 / 255.0f blue:66 / 255.0f alpha:0.8]];
        label.text = NSLocalizedString(@"lbl.your_groups.txt", "");
    } else {
        //Popular groups
        [imgView setBackgroundColor:[UIColor colorWithRed:97 / 255.0f green:63 / 255.0f blue:74 / 255.0f alpha:0.8]];
        label.text = NSLocalizedString(@"lbl.popular_groups.txt", "");
    }

    // Return
    return headerView;
}

// ----------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([APP_DELEGATE isUserBelow18] && section != 1){
        return 0;
    }
    return 30;
}

#pragma mark
#pragma mark AlertView Delegates
#pragma mark

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [APP_DELEGATE showActivityIndicator];;
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassCreateGroup, groupData[kKeyId]];
        [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
    }

}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// ShowGroupSucceed:

- (void)ShowGroupSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSArray *groupList = [inNotify object];
    NSMutableArray *popularMutatedArr = [NSMutableArray array];
    NSMutableArray *yourMutatedArr = [NSMutableArray array];
    NSMutableArray *invitedMutatedArr = [NSMutableArray array];

    for (NSDictionary *dict in groupList) {
        if ([dict[kKeyGroupMembers] count] >= 15) {
            NSArray *grpMembersArray = [dict valueForKey:kKeyGroupMembers];
            BOOL isMember = NO;
            for (NSDictionary *memberDict in grpMembersArray) {
                if ([[memberDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && ![[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"3"]) {
                    isMember = YES;

                }
            }
            if (isMember) {
                [popularMutatedArr addObject:dict];
            } else {
                if (![invitedMutatedArr containsObject:dict]) {
                    [invitedMutatedArr addObject:dict];
                }
            }
        } else if ([dict[kKeyUserID] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
            [yourMutatedArr addObject:dict];
        } else {
            NSArray *grpMembersArray = [dict valueForKey:kKeyGroupMembers];
            BOOL isMember = NO;
            for (NSDictionary *memberDict in grpMembersArray) {
                if ([[memberDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && ![[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"3"]) {
                    isMember = YES;
                }
            }
            if (isMember) {
                [yourMutatedArr addObject:dict];
            } else {
                if (![invitedMutatedArr containsObject:dict]) {
                    [invitedMutatedArr addObject:dict];
                }
            }
        }
    }
    sourceYourGrpArr = yourMutatedArr;
    sourcePopularGrpArr = popularMutatedArr;
    sourceInvitedGrpArr = invitedMutatedArr;

    yourGroupListArr = sourceYourGrpArr;
    popularGroupListArr = sourcePopularGrpArr;
    youInvitedGrpArr = sourceInvitedGrpArr;
    
    // Search
    [self searchFriendForSearchText:_txtSearchField.text];

    // Reload table
    [self.objTableView reloadData];
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// AcceptRejectGroupSucceed:
- (void)AcceptRejectGroupSucceed:(NSNotification *)inNotify {
    // Post notification for call new events from server
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewGroupAdded object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFetchNotification object:nil];
    NSDictionary *dict = [[NSDictionary alloc] init];
    dict = [inNotify userInfo];
}

// --------------------------------------------------------------------------------
// AcceptRejectGroupFailed:
- (void)AcceptRejectGroupFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:[inNotify object]];
}

// --------------------------------------------------------------------------------
// DeleteGroupMemberSucceed:

- (void)DeleteGroupMemberSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [self.objTableView reloadData];
}

// --------------------------------------------------------------------------------
// DeleteGroupMemberFailed:

- (void)DeleteGroupMemberFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:[inNotify object]];
}

@end
