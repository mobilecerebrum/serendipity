#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SRProfileImageView.h"
#import "SRNewGroupViewController.h"

@interface SRGroupsListController : ParentViewController <CLLocationManagerDelegate>
{
	// Instance variable
    NSArray *sourceYourGrpArr;
    NSArray *sourcePopularGrpArr;
    NSArray *sourceInvitedGrpArr;
    
	NSArray *yourGroupListArr;
	NSArray *popularGroupListArr;
    NSArray *youInvitedGrpArr;

    NSArray *dotsGroupPicsArr;

	// Server
	SRServerConnection *server;
    
    // Overlay
    UIImageView *overlayImgView;
    
    NSMutableDictionary *groupData;
    NSInteger currentIndex;
    BOOL isJoined;
    NSString *distanceUnit;
    NSDictionary *dictPram_CometChat;
}

// Properties
@property (weak, nonatomic) IBOutlet SRProfileImageView *imgBGPhoto;
@property (weak, nonatomic) IBOutlet UITableView *objTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgSearchBGColor;
@property (weak, nonatomic) IBOutlet UIImageView *imgTxtSearchBck;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchField;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchIcon;

// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
