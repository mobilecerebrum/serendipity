#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#import "SRProfileImageView.h"
#import "SRExtraRadarCircle.h"
#import "SRRadarCircleView.h"
#import "SRRadarDotView.h"
#import "SRRadarFocusView.h"
#import "SRChatViewController.h"
#import "ASValueTrackingSlider.h"
#import "UISegmentedControl+Common.h"

@interface SRGroupRadarViewController : ParentViewController <CLLocationManagerDelegate>

{
	// Instance variable
	float currentDeviceBearing;
	float latestDistance;
	float settingDistance;

	float headingAngle;
    int sliderX;

	NSTimer *detectCollisionTimer;
    NSString *distanceUnit;
	NSArray *nearbyUsers;
	NSArray *sourceArr;
    NSArray *groupUsers;
    NSArray *filteredArray;

	NSMutableArray *dotsArr;
	NSMutableArray *dotsUserPicsArr;

	NSMutableDictionary *userDataDict;

	// View
	SRRadarCircleView *radarCircleView;
	SRRadarFocusView *radarFocusView;

	UIImageView *swipeView;
	UIView *swipeContainerView;
	UILabel *lblDistance;
	UILabel *lblNoPPl;

	CGPoint touchLocation;

	// Flag
	BOOL isOnce;
	BOOL isSwipedOnce;
    BOOL isPrivateGroup;
    BOOL isGroupFilterApplied;
	// Server
	SRServerConnection *server;
    
    CLLocationManager *compassManager;
}

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet SRProfileImageView *bckProfileImg;
@property (nonatomic, strong) IBOutlet UIView *radarViewContainer;
@property (nonatomic, strong) IBOutlet UIView *northLineView;
@property (nonatomic, strong) IBOutlet UIButton *filterBtn;
@property (nonatomic, strong) IBOutlet UISegmentedControl *filterSegment;
@property (nonatomic, strong) IBOutlet UIView *searchView;
@property (nonatomic, strong) IBOutlet SRProfileImageView *radarProfileImgView;
@property (nonatomic, strong) IBOutlet SRExtraRadarCircle *extraCircleView;
@property (nonatomic, strong) IBOutlet UIView *progressContainerView;
//@property (nonatomic, strong) IBOutlet UILabel *lblMiles;
@property (nonatomic, strong) IBOutlet UILabel *lblInPpl;

@property (nonatomic, strong) IBOutlet UIButton *btnReduceMiles;
@property (nonatomic, strong) IBOutlet UIButton *btnIncreasedMiles;
@property (weak, nonatomic) IBOutlet UIImageView *milesSetPlusImg;
@property (weak, nonatomic) IBOutlet UIImageView *milesSetminusImg;


@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider1;

@property (weak, nonatomic) IBOutlet UITextField *txtSearchField;
@property (nonatomic, strong) UIView *radarRefreshOverlay;

// Properties
@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnButtonClick:(id)sender;
- (IBAction)actionOnSegmentedControl:(id)sender;
@end
