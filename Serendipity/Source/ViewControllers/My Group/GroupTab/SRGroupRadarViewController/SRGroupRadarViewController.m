#import "SRModalClass.h"
#import "SRUserTabViewController.h"
#import "SRGroupDetailTabViewController.h"
#import "SRGroupRadarViewController.h"
#import "KxMenu.h"
#import "CalloutAnnotationView.h"
#import "SRMapViewController.h"
#import "UISegmentedControl+Common.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

#define kKeyModifiedFlag @"flag"

@implementation SRGroupRadarViewController

#pragma mark
#pragma mark - Other Methods
#pragma mark

// -------------------------------------------------------------------------------
// makeRoundedImage:

- (UIImage *)makeRoundedImage:(UIImage *)image
                       radius:(float)radius; {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = 100 / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Return
    return roundedImage;
}


// -------------------------------------------------------------------------------------------------
// makeServerCallToGetUsersList

- (void)makeServerCallToGetGroupList:(CLLocation *)inLocation {
    if (sourceArr.count == 0 || sourceArr == nil){
      [APP_DELEGATE showActivityIndicator];
    }
    
    NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
    NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
    NSInteger distance = kKeyMaximumRadarMile;

    NSString *urlStr = [NSString stringWithFormat:@"%@?distance=%@&lat=%@&lon=%@", kKeyClassCreateGroup, [NSString stringWithFormat:@"%ldmi", (long) distance], latitude, longitude];
    [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];

    for (UIView *overlay in self.tabBarController.view.subviews) {
        if (overlay.tag == 100000)
            [overlay setHidden:YES];
    }

}

#pragma mark
#pragma mark - Radar Methods
#pragma mark

// -----------------------------------------------------------------------------------------------
// getHeadingForDirectionFromCoordinate:

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc {
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);

    float degree = radiandsToDegrees(atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng)));

    if (degree >= 0) {
        return -degree;
    } else {
        return -(360 + degree);
    }

    return degree;
}

//                                   -----------------------------------------------------------------------------------------------
// rotateArcsToHeading:

- (void)rotateArcsToHeading:(CGFloat)angle {
    // rotate the circle to heading degree
    radarCircleView.transform = CGAffineTransformMakeRotation(angle);

    // rotate all dots to opposite angle to keep the profile image straight up
    for (SRRadarDotView *dot in dotsArr) {
        dot.transform = CGAffineTransformMakeRotation(-angle);
    }
}

// ---------------------------------------------------------------------------------------
// setDisplayForRadar

- (void)setDisplayForRadar {
    // Spin the view of focus
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spin.duration = 1.5;
    spin.toValue = [NSNumber numberWithFloat:M_PI];
    spin.cumulative = YES;
    spin.removedOnCompletion = NO;
    spin.repeatCount = MAXFLOAT;
    [radarFocusView.layer addAnimation:spin forKey:@"spinRadarView"];
}

// -----------------------------------------------------------------------------------------------
// displayUsersDotsOnRadar

- (void)displayUsersDotsOnRadar {
    // empty the existing dots from radar
    for (SRRadarDotView *dot in dotsArr) {
        [dot removeFromSuperview];
    }
    [dotsArr removeAllObjects];

    // This method should be called after successful return of JSON array from your server-side service
    [self renderUsersOnRadar:nearbyUsers];
}

// -----------------------------------------------------------------------------------------------
// renderUsersOnRadar:

- (void)renderUsersOnRadar:(NSArray *)users {
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
            server.myLocation.coordinate.longitude};

    // The last user in the nearbyUsers list is the farthest
    float maxDistance = [[[users lastObject] valueForKey:kKeyDistance] floatValue];

    // Remove all objects from array
    [dotsUserPicsArr removeAllObjects];

    // Add users dots
    for (NSDictionary *user in users) {
        SRRadarDotView *dot = [[SRRadarDotView alloc] initWithFrame:CGRectMake(0, 0, 12.0, 12.0)];
        dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
        dot.userProfile = user;


        if ([user[kKeyGroupType] isEqualToString:@"0"] && [server.loggedInUserInfo[kKeyId] isEqualToString:user[kKeyUserID]]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-white-dot-20px.png"];
        } else if ([user[kKeyGroupType] isEqualToString:@"0"] && ![server.loggedInUserInfo[kKeyId] isEqualToString:user[kKeyUserID]]) {
            NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[user valueForKey:kKeyGroupMembers]];
            BOOL isExistId = NO;
            for (NSDictionary *dict in groupMemberArray) {
                if ([[dict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                    isExistId = YES;
                }

            }
            if (isExistId) {
                dot.imgView.image = [UIImage imageNamed:@"radar-pin-dot-30px.png"];
            }


        } else if ([user[kKeyGroupType] isEqualToString:@"1"] && [server.loggedInUserInfo[kKeyId] isEqualToString:user[kKeyUserID]]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-white-20px.png"];
        } else if ([user[kKeyGroupType] isEqualToString:@"1"] && ![server.loggedInUserInfo[kKeyId] isEqualToString:user[kKeyUserID]]) {
            NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[user valueForKey:kKeyGroupMembers]];
            BOOL isExistId = NO;
            for (NSDictionary *dict in groupMemberArray) {
                if ([[dict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                    isExistId = YES;
                }
            }
            if (isExistId) {
                dot.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];
            }
        }

        CLLocationCoordinate2D userLoc = {[[user valueForKey:kKeyLattitude] floatValue], [[user valueForKey:kKeyRadarLong] floatValue]};
        float bearing = [self getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        dot.bearing = @(bearing);

        float d = [[user valueForKey:kKeyDistance] floatValue];

        float distance;

        if (maxDistance != 0) {
            if (37 + d >= 130) {
                distance = 130;
            } else {
                distance = (37 + d);
            }
        } else {
            distance = (double) 37;
        }
        dot.distance = @(distance);
        dot.initialDistance = @(distance); // relative distance
        dot.userDistance = @(d);
        dot.zoomEnabled = NO;
        dot.userInteractionEnabled = NO;

        float left = (float) (148 + distance * sin(degreesToRadians(-bearing)));
        float top = (float) (148 - distance * cos(degreesToRadians(-bearing)));

        [self rotateDot:dot fromBearing:currentDeviceBearing toBearing:bearing atDistance:distance];

        dot.frame = CGRectMake(left, top, 12.0, 12.0);
        dot.initialFrame = dot.frame;
        [radarCircleView addSubview:dot];
        [dotsArr addObject:dot];


        dot.transform = CGAffineTransformMakeRotation(-headingAngle);
    }
    // Start timer to detect collision with radar line and blink
    if ([detectCollisionTimer isValid]) {
        [detectCollisionTimer invalidate];
    }

    detectCollisionTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                            target:self selector:@selector(detectCollisions:) userInfo:nil repeats:YES];
}

#pragma mark
#pragma mark - Radar&Dot Collisions Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// detectCollisions:

- (void)detectCollisions:(NSTimer *)theTimer {
    float radarLineRotation = radiandsToDegrees([[radarFocusView.layer.presentationLayer valueForKeyPath:@"transform.rotation.z"] floatValue]);

    if (radarLineRotation >= 0) {
        radarLineRotation += 90;
    } else {
        if (radarLineRotation > -90) {
            radarLineRotation = 90 + radarLineRotation;
        } else
            radarLineRotation = 270 + (radarLineRotation + 180);
    }

    for (int i = 0; i < [dotsArr count]; i++) {
        SRRadarDotView *dot = dotsArr[i];
        float dotBearing = [dot.bearing floatValue];
        if (dotBearing < 0) {
            dotBearing = -dotBearing;
        }
        dotBearing = dotBearing - currentDeviceBearing;
        if (dotBearing < 0) {
            dotBearing = dotBearing + 360;
        }

        // Now get diffrence between dotbearing and radarline
        float diffrence;
        if (dotBearing > radarLineRotation) {
            diffrence = dotBearing - radarLineRotation;
        } else
            diffrence = radarLineRotation - dotBearing;

        if (diffrence <= 5) {
            [self pulse:dot];
        }
    }
}

// -----------------------------------------------------------------------------------------------
// pulse:

- (void)pulse:(SRRadarDotView *)dot {
    /*if ([dot.layer.animationKeys containsObject:@"pulse"] || dot.zoomEnabled) {
        // view is already animating. so return
        return;
       }*/
    // Dot images
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.duration = 1.0;
    pulse.toValue = @1.0F;
    pulse.autoreverses = NO;
    dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    [dot.layer addAnimation:pulse forKey:@"pulse"];


    if (dot.userProfile[kKeyMediaImage] != nil && [dot.userProfile[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        CGRect frame = dot.initialFrame;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.frame = frame;

        frame = dot.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.imgView.frame = frame;

        dot.imgView.contentMode = UIViewContentModeScaleToFill;
        dot.imgView.layer.cornerRadius = dot.imgView.frame.size.width / 2.0;
        dot.imgView.clipsToBounds = YES;
        dot.isImageApplied = YES;

        NSDictionary *profileDict = dot.userProfile[kKeyMediaImage];
        NSString *imageName = profileDict[kKeyImageName];
        if ([imageName length] > 0) {
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

            [dot.imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        } else
            dot.imgView.image = [UIImage imageNamed:@"group-white-50px.png"];
    }
    [self performSelector:@selector(removeImageSetDefault:) withObject:dot afterDelay:0.5];
}

// -----------------------------------------------------------------------------------------------
// removeImageSetDefault:

- (void)removeImageSetDefault:(SRRadarDotView *)inDotView {
    if (inDotView.isImageApplied) {
        // Remove the animation
        [inDotView.layer removeAllAnimations];

        CGRect frame = inDotView.initialFrame;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.frame = frame;

        frame = inDotView.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.imgView.frame = frame;

        inDotView.imgView.contentMode = UIViewContentModeScaleAspectFill;
        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:inDotView.userProfile];


        if ([updatedDict[kKeyGroupType] isEqualToString:@"1"] && [server.loggedInUserInfo[kKeyId] isEqualToString:updatedDict[kKeyUserID]])
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-white-20px.png"];
        else if ([updatedDict[kKeyGroupType] isEqualToString:@"0"] && [server.loggedInUserInfo[kKeyId] isEqualToString:updatedDict[kKeyUserID]]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-white-dot-20px.png"];
        } else if ([updatedDict[kKeyGroupType] isEqualToString:@"0"]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-dot-30px.png.png"];
        } else
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];
        inDotView.isImageApplied = NO;
        inDotView.imgView.clipsToBounds = NO;
    }
}

#pragma mark
#pragma mark - Radar Dot Translate Methods
#pragma mark

// -----------------------------------------------------------------------------------------------
// rotateDot: fromBearing: atDistance:

- (void)rotateDot:(SRRadarDotView *)dot fromBearing:(CGFloat)fromDegrees toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CGMutablePathRef path = CGPathCreateMutable();

    CGPathAddArc(path, nil, 150, 150, distance, degreesToRadians(fromDegrees), degreesToRadians(degrees), YES);

    CAKeyframeAnimation *theAnimation;

    // Animation object for the key path
    theAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    theAnimation.path = path;
    CGPathRelease(path);

    // Set the animation properties
    theAnimation.duration = 3;
    theAnimation.removedOnCompletion = NO;
    theAnimation.repeatCount = 0;
    theAnimation.autoreverses = NO;
    theAnimation.fillMode = kCAFillModeBackwards;
    theAnimation.cumulative = YES;

    CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);
    dot.layer.position = newPosition;
    [dot.layer addAnimation:theAnimation forKey:@"rotateDot"];
}

// -----------------------------------------------------------------------------------------------
// translateDot:

- (void)translateDot:(SRRadarDotView *)dot toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];

    [animation setFromValue:[NSValue valueWithCGPoint:[[dot.layer.presentationLayer valueForKey:@"position"] CGPointValue]]];

    //CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);

    float left = (float) (148 + distance * sin(degreesToRadians(-degrees)));
    float top = (float) (148 - distance * cos(degreesToRadians(-degrees)));
    CGPoint newPosition = CGPointMake(left, top);
    [animation setToValue:[NSValue valueWithCGPoint:newPosition]];

    [animation setDuration:0.3f];
    animation.fillMode = kCAFillModeBackwards;
    animation.autoreverses = NO;
    animation.repeatCount = 0;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];

    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [alphaAnimation setDuration:0.5f];
    alphaAnimation.fillMode = kCAFillModeBackwards;
    alphaAnimation.autoreverses = NO;
    alphaAnimation.repeatCount = 0;
    alphaAnimation.removedOnCompletion = NO;
    alphaAnimation.cumulative = YES;
    alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];

    [dot.layer addAnimation:animation forKey:@"translateDot"];
}

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRGroupRadarViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
        dotsArr = [NSMutableArray array];
        dotsUserPicsArr = [[NSMutableArray alloc] init];

        userDataDict = [[NSMutableDictionary alloc] init];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getUpdateLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateHeadingAngle:)
                              name:kKeyNotificationUpdateHeadingValue
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupFailed:)
                              name:kKeyNotificationCreateGroupFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(filterGroupUserListOnMap:)
                              name:kKeyNotificationGroupMapRefreshOnSlider
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupSucceed:)
                              name:kKeyNotificationAcceptRejectGroupMemberSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupFailed:)
                              name:kKeyNotificationAcceptRejectGroupMemberFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(makeServerCallToGetGroupList:)
                              name:kKeyNotificationNewGroupAdded object:nil];
    }
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];
    [self.filterSegment ensureiOS12Style];

    
#warning Comment because there is no setting for set miles of user
    // [self setmileslableData];
    // Prepare radar view
    radarCircleView = [[SRRadarCircleView alloc] initWithFrame:CGRectMake(0, 0, self.radarViewContainer.bounds.size.width, self.radarViewContainer.bounds.size.height)];
    radarCircleView.points = CGPointMake(self.radarProfileImgView.frame.origin.x, self.radarProfileImgView.frame.origin.y);
    radarCircleView.layer.contentsScale = [UIScreen mainScreen].scale;
    self.radarViewContainer.layer.contentsScale = [UIScreen mainScreen].scale;
//    [self.northLineView removeFromSuperview];
//    [radarCircleView addSubview:self.northLineView];
    compassManager = [[CLLocationManager alloc] init];
    compassManager.delegate = self;
    
    [self.radarViewContainer addSubview:radarCircleView];

    radarFocusView = [[SRRadarFocusView alloc] initWithFrame:CGRectMake(18, 18, 265, 265)];
    radarFocusView.layer.contentsScale = [UIScreen mainScreen].scale;
    radarFocusView.alpha = 0.68;
    [self.radarViewContainer addSubview:radarFocusView];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDotTapped:)];
    [radarCircleView addGestureRecognizer:tapGestureRecognizer];

    // Bring view to front
    [self.view bringSubviewToFront:self.radarProfileImgView];
    [self.view bringSubviewToFront:self.northLineView];

    // Set profile pic
    self.radarProfileImgView.layer.cornerRadius = self.radarProfileImgView.frame.size.width / 2;
    self.radarProfileImgView.layer.masksToBounds = YES;

    // Add View
    swipeContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];

    swipeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, self.view.frame.size.height)];
    swipeView.alpha = 0.9;
    swipeView.backgroundColor = [UIColor colorWithRed:115 / 255.0 green:70 / 255.0 blue:14 / 255.0 alpha:0.6];

    lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 120, 40)];
    lblDistance.font = [UIFont fontWithName:kFontHelveticaBold size:26.0];
    lblDistance.textColor = [UIColor whiteColor];

    lblNoPPl = [[UILabel alloc] initWithFrame:CGRectMake(0, lblDistance.frame.origin.y + 70, 140, 22)];
    lblNoPPl.textColor = [UIColor whiteColor];
    lblNoPPl.font = [UIFont fontWithName:kFontHelveticaMedium size:18.0];

    // Add the views to container view
    [swipeContainerView addSubview:swipeView];
    [swipeContainerView addSubview:lblDistance];
    [swipeContainerView addSubview:lblNoPPl];

    swipeContainerView.alpha = 0.7;
    swipeContainerView.hidden = YES;

    [self.view addSubview:swipeContainerView];

    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:panGesture];

    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDistance:)];
    [self.view addGestureRecognizer:pinch];

    //Add Tap Gesture on dotView
    UITapGestureRecognizer *viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapAction:)];
    self.view.userInteractionEnabled = YES;
    viewTapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:viewTapGesture];

    // Add long press guesture on + button
    UILongPressGestureRecognizer *longPressPlus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressTap:)];
    longPressPlus.minimumPressDuration = 1.0f;
    self.btnIncreasedMiles.tag = 1;
    [self.btnIncreasedMiles addGestureRecognizer:longPressPlus];

    // Add long press guesture on - button
    UILongPressGestureRecognizer *longPressMinus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressTap:)];
    longPressMinus.minimumPressDuration = 1.0f;
    self.btnReduceMiles.tag = 2;
    [self.btnReduceMiles addGestureRecognizer:longPressMinus];

    // Set search icon to textfield
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    container.backgroundColor = [UIColor clearColor];

    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(12, 4, 20, 20)];
    searchIcon.image = [UIImage imageNamed:@"ic_search"];
    [container addSubview:searchIcon];

    self.txtSearchField.leftView = container;
    self.txtSearchField.leftViewMode = UITextFieldViewModeAlways;

    self.txtSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.searchText.txt", "") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txtSearchField.layer.cornerRadius = 5;

    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }
    latestDistance = distancevalue;
    settingDistance = distancevalue;

    // Customize Slider
    self.slider1.maximumValue = latestDistance;
    self.slider1.minimumValue = kKeyMinimumRadarMile;
    self.slider1.value = latestDistance;
    self.slider1.popUpViewColor = [UIColor colorWithRed:1 green:0.588 blue:0 alpha:1];

    // ScrollView
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;

    [_radarRefreshOverlay setHidden:YES];
    // Add long press guesture to refresh radar centered ProfileImgView
    UILongPressGestureRecognizer *radarLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressRadar:)];
    radarLongPress.minimumPressDuration = 1.0f;
    self.radarProfileImgView.userInteractionEnabled = YES;
    //    [self.extraCircleView addGestureRecognizer:radarLongPress];
    [self.radarProfileImgView addGestureRecognizer:radarLongPress];
    [self.extraCircleView bringSubviewToFront:self.radarViewContainer];
    [self.radarViewContainer bringSubviewToFront:self.radarProfileImgView];
    [self.radarProfileImgView bringSubviewToFront:self.radarProfileImgView.superview];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(entersBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];

    [[NSNotificationCenter defaultCenter]       addObserver:self selector:@selector(updateSettings) name:
            UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
}

- (void)entersBackground {

    if ([detectCollisionTimer isValid]) {
        [detectCollisionTimer invalidate];
    }


    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];

    [[NSNotificationCenter defaultCenter]       addObserver:self selector:@selector(updateSettings) name:
            UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];

}


- (void)updateSettings {
    if ([detectCollisionTimer isValid]) {
        [detectCollisionTimer invalidate];
    }
    detectCollisionTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                            target:self
                                                          selector:@selector(detectCollisions:)
                                                          userInfo:nil
                                                           repeats:YES];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(entersBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];


}

//-----------------------------------------------------------------------------------------------
// viewWillAppear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    (APP_DELEGATE).topBarView.hidden = YES;
    //Get and Set distance measurement unit
//    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
//        distanceUnit = kkeyUnitMiles;
//    } else
//        distanceUnit = kkeyUnitKilometers;
    distanceUnit = kkeyUnitMiles;
    [compassManager startUpdatingHeading];
}


//-----------------------------------------------------------------------------------------------
// viewDidAppear

- (void)viewDidAppear:(BOOL)animated {
    // Call super
    [super viewDidAppear:NO];

    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;

#warning Comment because there is no setting for set miles of user

    //[self setmileslableData];

    // Call the radar set up methods after some delay
//    if (!isOnce) {
//        isOnce = YES;
    [self setDisplayForRadar];
    [self makeServerCallToGetGroupList:server.myLocation];
//    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.scrollView setHidden:YES];
    [self.toolTipScrollViewImg setHidden:YES];
    [compassManager stopUpdatingHeading];
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationGroupRadarRefreshOnSlider object:[NSString stringWithFormat:@"%f", latestDistance]];
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnClick:(id)sender {
    //BOOL isIncreased = NO;
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    NSString *strMilesTempData = [userdef valueForKey:kKeyTempMiles_Data];

    if (sender == self.btnIncreasedMiles) {
        if (strMilesData == nil) {
            strMilesData = [NSString stringWithFormat:@"%f", kKeyMaximumRadarMile];
        }
        if (latestDistance >= [strMilesData floatValue] && [strMilesTempData integerValue] >= [strMilesData integerValue]) {
            [SRModalClass showAlert:NSLocalizedString(@"alert.max_Distance.text", @"")];
        } else {
            latestDistance += 30;
            if (latestDistance >= kKeyMaximumRadarMile) {
                latestDistance = kKeyMaximumRadarMile;
            }
            sliderX = latestDistance;
            [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
            [userdef synchronize];
            [self calculateAndGetNewUsers:nearbyUsers];
            swipeContainerView.hidden = NO;
        }
    } else {
        if (latestDistance <= kKeyMinimumRadarMile) {
            [SRModalClass showAlert:NSLocalizedString(@"alert.min_Distance.text", @"")];
            // swipeContainerView.hidden = YES;
        } else {
            latestDistance -= 30;
            if (latestDistance <= 30) {
                latestDistance = kKeyMinimumRadarMile;
                [self.slider1 setValue:0.0 animated:YES];
            }
            sliderX = latestDistance;
            //             [self.slider1 setValue:latestDistance animated:YES];
            [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
            [userdef synchronize];
            // Call method
            [self calculateAndGetNewUsers:nearbyUsers];
            swipeContainerView.hidden = NO;
        }
    }

    [UIView animateWithDuration:0.0 delay:0.9 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self->swipeContainerView.hidden = NO;
        self->sliderX = ((self.slider1.value * self.slider1.frame.size.width) / self.slider1.maximumValue) + self.slider1.frame.origin.x;
        if (self->sliderX > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                    self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                } else
                    self->swipeView.frame = CGRectMake(0, 0, self->sliderX, self.view.frame.size.height);

        if (self->sliderX >= (SCREEN_WIDTH / 2)) {
            self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 140, self->lblDistance.frame.origin.y, 120, 40);
            self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 160, self->lblDistance.frame.origin.y + 40, 140, 22);
                } else {
                    self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y, 140, 40);
                    self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y + 40, 140, 22);
                }
                [self.view bringSubviewToFront:self.progressContainerView];
            }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(hideSwipeView) withObject:self afterDelay:0.4];

                     }];
}

// -----------------------------------------------------------------------------------------------
// hideSwipeView:
- (void)hideSwipeView {
    swipeContainerView.hidden = YES;
}
// -----------------------------------------------------------------------------------------------
// setmileslableData

- (void)setmileslableData {
    // Set the minimum range
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];

    latestDistance = [strMilesData floatValue];
    settingDistance = [strMilesData floatValue];

    NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
    NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

    NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
    NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:@" miles" attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
    [muAtrStr appendAttributedString:atrStr1];
    [muAtrStr appendAttributedString:atrStr2];


}

// -----------------------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnButtonClick:(UIButton *)sender {
    if (sender == self.filterBtn) {
        if (self.filterSegment.isHidden) {
            self.filterSegment.hidden = NO;

        } else {
            isGroupFilterApplied = FALSE;
            self.filterSegment.hidden = YES;
            if (_txtSearchField.text.length > 0) {
                [self searchEventForSearchText:_txtSearchField.text];
            } else {
                nearbyUsers = sourceArr;
                [self displayUsersDotsOnRadar];
                [self calculateAndGetNewUsers:sourceArr];
            }
        }
    }
}

// -----------------------------------------------------------------------------------------------
// actionOnSegmentedControl:
- (IBAction)actionOnSegmentedControl:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 1) {

        //Set color to selected segment index
        
        
        for (int i=0; i<[sender.subviews count]; i++)
        {
            if ([[sender.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && [[sender.subviews objectAtIndex:i]isSelected])
            {
                [[sender.subviews objectAtIndex:i] setTintColor:[UIColor orangeColor]];
            }
            if ([[sender.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && ![[sender.subviews objectAtIndex:i] isSelected])
            {
                [[sender.subviews objectAtIndex:i] setTintColor:[sender tintColor]];
            }
        }
//        for (int i = 0; i < [sender.subviews count]; i++) {
//            if ([sender.subviews[i] isSelected]) {
//                UIColor *tintcolor = [UIColor orangeColor];
//                [sender.subviews[i] setTintColor:tintcolor];
//            } else {
//                [sender.subviews[i] setTintColor:nil];
//            }
//        }
        sender.selectedSegmentIndex = UISegmentedControlNoSegment;
        isPrivateGroup = YES;
        NSArray *menuItems =
                @[
                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_invite_join.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_your_groups.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_pop_private_Groups.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_pop_private_closeProximity.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                ];

        [KxMenu showMenuInView:self.view fromRect:sender.frame menuItems:menuItems];
    } else {
        isPrivateGroup = NO;

        //Set color to selected segment index
//        for (int i = 0; i < [sender.subviews count]; i++) {
//            if ([sender.subviews[i] isSelected]) {
//                UIColor *tintcolor = [UIColor orangeColor];
//                [sender.subviews[i] setTintColor:tintcolor];
//            } else {
//                [sender.subviews[i] setTintColor:nil];
//            }
//        }
        sender.selectedSegmentIndex = UISegmentedControlNoSegment;
        NSArray *menuItems =
                @[
                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_invite_join.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_your_groups.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_pop_publicGroups.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.group_filter_pop_public_closeProximity.txt", @"") image:nil target:self action:@selector(groupFilterApplied:)],

                ];

        [KxMenu showMenuInView:self.view fromRect:sender.frame menuItems:menuItems];
    }
}


- (void)groupFilterApplied:(id)sender {

    KxMenuItem *clickedFilter = sender;

    NSMutableArray *filterArray = [NSMutableArray array];
    NSMutableArray *groupsArray = [NSMutableArray array];

    if (isPrivateGroup) {
        if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_invite_join.txt", @"")]) {
            // Groups you have been invited array
            [groupsArray removeAllObjects];
            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"0"]) {
                    if (![[sourceArr[i] objectForKey:kKeyUserID] isEqual:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                        NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[sourceArr[i] valueForKey:kKeyGroupMembers]];
                        BOOL isExistId = NO;
                        for (NSDictionary *dict in groupMemberArray) {
                            if ([[dict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                                if ([[dict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"] && [[dict valueForKey:kKeyIsAdmin] isEqualToString:@"0"]) {
                                    isExistId = YES;
                                }

                            }

                        }
                        if (isExistId) {
                            [groupsArray addObject:sourceArr[i]];
                        }

                    }

                }
            }
            filterArray = groupsArray;
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_your_groups.txt", @"")]) {
            [groupsArray removeAllObjects];
            // Your groups array
            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"0"]) {
                    NSArray *grpMembers = [sourceArr[i] valueForKey:kKeyGroupMembers];
                    if ([[sourceArr[i] valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {

                        [groupsArray addObject:sourceArr[i]];
                    } else {
                        if ([grpMembers isKindOfClass:[NSArray class]]) {
                            for (NSDictionary *userDict in grpMembers) {
                                if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && ([[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"] || [[userDict valueForKey:kKeyIsAdmin] isEqualToString:@"1"])) {
                                    [groupsArray addObject:sourceArr[i]];
                                }
                            }
                        }

                    }
                }
            }
            filterArray = groupsArray;

        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_pop_private_Groups.txt", @"")]) {
            // Popular groups array
            [groupsArray removeAllObjects];
            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"0"]) {
                    NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[sourceArr[i] valueForKey:kKeyGroupMembers]];
                    if (groupMemberArray.count > 15) {
                        [groupsArray addObject:sourceArr[i]];
                    }
                }
            }
            filterArray = groupsArray;

        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_pop_private_closeProximity.txt", @"")]) {
            //Groups public close proximity
            [groupsArray removeAllObjects];

            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"0"]) {
                    NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[sourceArr[i] valueForKey:kKeyGroupMembers]];

                    if ([[sourceArr[i] valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                        [groupsArray addObject:sourceArr[i]];
                    } else {
                        if (groupMemberArray.count <= 15) {
                            [groupsArray addObject:sourceArr[i]];
                        }
                    }

                }
            }


            filterArray = groupsArray;
        }

    } else {
        if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_invite_join.txt", @"")]) {
            // Groups you have been invited array
            [groupsArray removeAllObjects];
            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
                    if (![[sourceArr[i] objectForKey:kKeyUserID] isEqual:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                        NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[sourceArr[i] valueForKey:kKeyGroupMembers]];
                        BOOL isExistId = NO;
                        for (NSDictionary *dict in groupMemberArray) {
                            if ([[dict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                                if ([[dict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"] && [[dict valueForKey:kKeyIsAdmin] isEqualToString:@"0"]) {
                                    isExistId = YES;
                                }

                            }

                        }
                        if (isExistId) {
                            [groupsArray addObject:sourceArr[i]];
                        }

                    }
                }
            }

            filterArray = groupsArray;
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_your_groups.txt", @"")]) {
            [groupsArray removeAllObjects];
            // Your groups array
            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
                    NSArray *grpMembers = [sourceArr[i] valueForKey:kKeyGroupMembers];
                    if ([[sourceArr[i] objectForKey:kKeyUserID] isEqual:[server.loggedInUserInfo valueForKey:kKeyId]]) {

                        [groupsArray addObject:sourceArr[i]];
                    } else {
                        if ([grpMembers isKindOfClass:[NSArray class]]) {
                            for (NSDictionary *userDict in grpMembers) {
                                if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && ([[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"] || [[userDict valueForKey:kKeyIsAdmin] isEqualToString:@"1"])) {
                                    [groupsArray addObject:sourceArr[i]];
                                }
                            }
                        }

                    }
                }
            }


            filterArray = groupsArray;

        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_pop_publicGroups.txt", @"")]) {
            // Popular groups array
            [groupsArray removeAllObjects];
            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
                    NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[sourceArr[i] valueForKey:kKeyGroupMembers]];
                    if (groupMemberArray.count > 15) {
                        [groupsArray addObject:sourceArr[i]];
                    }
                }
            }
            filterArray = groupsArray;

        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.group_filter_pop_public_closeProximity.txt", @"")]) {
            //Groups public close proximity
            [groupsArray removeAllObjects];

            for (int i = 0; i < sourceArr.count; i++) {
                if ([[sourceArr[i] valueForKey:kKeyGroupMembers] isKindOfClass:[NSArray class]] && [[sourceArr[i] valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
                    NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[sourceArr[i] valueForKey:kKeyGroupMembers]];
                    if ([[sourceArr[i] objectForKey:kKeyUserID] isEqual:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                        [groupsArray addObject:sourceArr[i]];
                    } else {
                        if (groupMemberArray.count <= 15) {
                            [groupsArray addObject:sourceArr[i]];
                        }
                    }
                }
            }

            filterArray = groupsArray;
        }

    }
    nearbyUsers = filterArray;
    filteredArray = filterArray;
    isGroupFilterApplied = TRUE;
    if (_txtSearchField.text.length > 0) {
        [self searchEventForSearchText:_txtSearchField.text];
    } else {
        [self displayUsersDotsOnRadar];
        [self calculateAndGetNewUsers:nearbyUsers];
    }

    // Show text for no people
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld group in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
}
//
// -------------------------------------------------------------------------------------------------
// addViewsOnScrollView:

- (void)addViewsOnScrollView:(NSArray *)inArr {

    CGRect frame;
    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (NSUInteger i = 0; i < [inArr count]; i++) {
        // Get profile pic of users
        NSDictionary *groupsDataDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i == 0) {
            frame = CGRectMake(0, 11, 130, 200);
        } else {
            frame = CGRectMake(frame.origin.x + 126, 11, 130, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;

        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds
                                         byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(8.0, 8.0)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = ((CalloutAnnotationView *) annotationView).userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        ((CalloutAnnotationView *) annotationView).userProfileImage.layer.mask = maskLayer;

        if (groupsDataDict[kKeyMediaImage] != nil && [groupsDataDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {

            NSDictionary *profileDict = groupsDataDict[kKeyMediaImage];
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else {
                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"group-white-50px.png"];
                ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
            }
        } else {
            ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"group-white-50px.png"];
            ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
        }


        //Favourite
        ((CalloutAnnotationView *) annotationView).favUserImage.hidden = TRUE;

        //Invisible
        ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = TRUE;

        //Distance
        ((CalloutAnnotationView *) annotationView).lblDistance.hidden = TRUE;

        //Degree
        ((CalloutAnnotationView *) annotationView).lblDegree.hidden = TRUE;

        ((CalloutAnnotationView *) annotationView).lblProfileName.text = [groupsDataDict valueForKey:kKeyName];

        if ([groupsDataDict[kKeyGroupMembers] isKindOfClass:[NSArray class]]) {
            NSInteger count = [groupsDataDict[kKeyGroupMembers] count];
            ((CalloutAnnotationView *) annotationView).lblOccupation.text = [NSString stringWithFormat:@"%ld members", (long) count];
        }

        // Image for group type
        ((CalloutAnnotationView *) annotationView).imgViewGrp.layer.cornerRadius = ((CalloutAnnotationView *) annotationView).imgViewGrp.frame.size.width / 2.0;
        ((CalloutAnnotationView *) annotationView).btnGrpJoin.layer.cornerRadius = ((CalloutAnnotationView *) annotationView).imgViewGrp.frame.size.width / 2.0;
        ((CalloutAnnotationView *) annotationView).btnGrpJoin.clipsToBounds = YES;
        ((CalloutAnnotationView *) annotationView).btnGrpJoin.hidden = YES;


        NSArray *grpMembersArray = [groupsDataDict valueForKey:kKeyGroupMembers];
        for (NSDictionary *memberDict in grpMembersArray) {
            if ([memberDict[kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && [memberDict[kKeyInvitationStatus] isEqualToString:@"0"]) {
                if ([groupsDataDict[kKeyGroupType] isEqualToString:@"1"]) {
                    [((CalloutAnnotationView *) annotationView).btnGrpJoin setBackgroundColor:[UIColor blackColor]];
                } else {
                    [((CalloutAnnotationView *) annotationView).btnGrpJoin setBackgroundColor:[UIColor orangeColor]];
                }

                ((CalloutAnnotationView *) annotationView).btnGrpJoin.hidden = YES;
                ((CalloutAnnotationView *) annotationView).btnGrpJoin.frame = ((CalloutAnnotationView *) annotationView).imgViewGrp.frame;

                [((CalloutAnnotationView *) annotationView).btnGrpJoin setTitle:@"Join" forState:UIControlStateNormal];
                ((CalloutAnnotationView *) annotationView).userDict = groupsDataDict;
                ((CalloutAnnotationView *) annotationView).btnGrpJoin.tag = [[memberDict valueForKey:kKeyUserID] intValue];

            } else if ([groupsDataDict[kKeyGroupType] isEqualToString:@"1"]) {
                ((CalloutAnnotationView *) annotationView).imgViewGrp.hidden = NO;
                ((CalloutAnnotationView *) annotationView).imgViewGrp.backgroundColor = [UIColor blackColor];
                ((CalloutAnnotationView *) annotationView).imgViewGrp.image = [UIImage imageNamed:@"group-white-50px.png"];
            } else {
                ((CalloutAnnotationView *) annotationView).imgViewGrp.hidden = NO;
                ((CalloutAnnotationView *) annotationView).imgViewGrp.backgroundColor = [UIColor orangeColor];
                ((CalloutAnnotationView *) annotationView).imgViewGrp.image = [UIImage imageNamed:@"group-white-50px.png"];
            }

        }


        // Add Shadow to btnPing
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOpacity:8.5];
        [((CalloutAnnotationView *) annotationView).pingButton addTarget:self action:@selector(actionOnPingBtnClick:) forControlEvents:UIControlEventTouchUpInside];


        //Add Compass Button
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowOpacity:8.5];

        CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                server.myLocation.coordinate.longitude};

        NSDictionary *userLocDict = groupsDataDict[kKeyLocation];
        CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};

        [((CalloutAnnotationView *) annotationView).compassButton addTarget:self action:@selector(compassButtonAction:) forControlEvents:UIControlEventTouchUpInside];


        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            annotationView.lblCompassDirection.text = @"N";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            annotationView.lblCompassDirection.text = @"E";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            annotationView.lblCompassDirection.text = @"S";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            annotationView.lblCompassDirection.text = @"W";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            annotationView.lblCompassDirection.text = @"NE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            annotationView.lblCompassDirection.text = @"NW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            annotationView.lblCompassDirection.text = @"SE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            annotationView.lblCompassDirection.text = @"SW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }



        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(windowTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [((CalloutAnnotationView *) annotationView).userProfileImage addGestureRecognizer:annotationTapGesture];
        ((CalloutAnnotationView *) annotationView).userDict = groupsDataDict;


        //if users count is one
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }
        //        // Add subview to scroll view
        //        [self.scrollView addSubview:annotationView];


        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else
            self.scrollView.scrollEnabled = NO;

        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
        //        self.toolTipScrollViewImg.hidden = NO;
    }
}

// --------------------------------------------------------------------------------
// calculateAndGetNewUsers

- (void)calculateAndGetNewUsers:(NSArray *)array {
    if (latestDistance > -1) {
        // Set value
        NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
        NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

        NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];
        if (str) {
            NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
            NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
            NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:distanceUnit attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
            [muAtrStr appendAttributedString:atrStr1];
            [muAtrStr appendAttributedString:atrStr2];

            [lblDistance setAttributedText:muAtrStr];
        }


        NSPredicate *predicate1 = [NSPredicate predicateWithBlock:^BOOL(id _Nullable evaluatedObject, NSDictionary<NSString *, id> *_Nullable bindings) {

            BOOL status = NO;

            if ([[evaluatedObject objectForKey:kKeyDistance] floatValue] <= self->latestDistance) {
                status = YES;
            }

            return status;
        }];

        if (!isGroupFilterApplied) {
            nearbyUsers = [sourceArr filteredArrayUsingPredicate:predicate1];
        } else
            nearbyUsers = [array filteredArrayUsingPredicate:predicate1];

        if ([self.txtSearchField.text length] > 0){
            NSString *filter = @"%K CONTAINS[cd] %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyName, self.txtSearchField.text];
            nearbyUsers = [nearbyUsers filteredArrayUsingPredicate:predicate];
        }
       
        
        // Show text for no people
        self.lblInPpl.text = [NSString stringWithFormat:@"%ld group in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
        lblNoPPl.text = [NSString stringWithFormat:@"%ld group in", (unsigned long) nearbyUsers.count];
        self.slider1.value = latestDistance;
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationGroupRadarChanges object:nearbyUsers];

        // Filter users
        for (id d in dotsArr) {
            SRRadarDotView *dot = (SRRadarDotView *) d;
            NSDictionary *userProfile = dot.userProfile;

            if ([nearbyUsers containsObject:userProfile]) {
                if (settingDistance != latestDistance) {
                    float maxDistance = settingDistance;
                    float dotDistance = [dot.initialDistance floatValue];

                    if (maxDistance != latestDistance) {
                        float modifiedUserDistance = (maxDistance * dotDistance) / latestDistance;
                        if (modifiedUserDistance > 130) {
                            modifiedUserDistance = 130;
                        } else if (modifiedUserDistance < 37) {
                            modifiedUserDistance = 37;
                        }
                        dot.distance = @(modifiedUserDistance);
                    }
                } else {
                    // The last user in the nearbyUsers list is the farthest
                    float maxDistance = [[[nearbyUsers lastObject] valueForKey:kKeyDistance] floatValue];

                    float d = [[userProfile valueForKey:kKeyDistance] floatValue];

                    float distance;

                    if (maxDistance != 0) {
                        if (37 + d >= 130) {
                            distance = 130;
                        } else {
                            distance = (37 + d);
                        }
                    } else {
                        distance = (double) 37;
                    }

                    // Distance
                    dot.distance = @(distance);
                }

                float degrees = [dot.bearing floatValue];
                float left = (float) (148 + [dot.distance floatValue] * sin(degreesToRadians(-degrees)));
                float top = (float) (148 - [dot.distance floatValue] * cos(degreesToRadians(-degrees)));

                dot.hidden = NO;
                [self translateDot:dot toBearing:[dot.bearing floatValue] atDistance:[dot.distance floatValue]];
                dot.frame = CGRectMake(left, top, 14, 14);
                dot.initialFrame = dot.frame;
                dot.imgView.frame = CGRectMake(0, 0, 14, 14);
            } else {
                dot.hidden = YES;
            }
        }
    }
}

// --------------------------------------------------------------------------------
// pinchDistance:

- (void)pinchDistance:(UIPinchGestureRecognizer *)gesture {
}

// --------------------------------------------------------------------------------
// handlePan:

- (void)handlePan:(UIPanGestureRecognizer *)gesture {
    // Transform the view by the amount of the x translation
    // Calculate how far the user has dragged across the view
    CGFloat progress = [gesture locationInView:self.slider1].x / (self.slider1.bounds.size.width - 5 * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
  //  NSLog(@"%f", [gesture locationInView:self.slider1].x);
    CGPoint translate = [gesture locationInView:gesture.view]; //translationInView:self.view];
    CGPoint translationInView = [gesture translationInView:self.slider1];
    swipeContainerView.hidden = NO;

    NSInteger translateX = [gesture locationInView:self.slider1].x;
    if (translateX < 0) {
        translateX = 0;
    }

    if (gesture.state == UIGestureRecognizerStateCancelled ||
            gesture.state == UIGestureRecognizerStateFailed ||
            gesture.state == UIGestureRecognizerStateEnded) {
        // When Pan ends
        swipeContainerView.hidden = YES;
        self.progressContainerView.hidden = NO;
        isSwipedOnce = NO;
        swipeView.frame = CGRectMake(0, 0, 0, self.view.frame.size.height);
        lblDistance.frame = CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 100, 30);
        lblNoPPl.frame = CGRectMake(0, lblDistance.frame.origin.y + 18, 100, 30);

        if (gesture.state == UIGestureRecognizerStateEnded) {
            if (progress < 1) {
                latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;
                if (latestDistance <= kKeyMinimumRadarMile) {
                    latestDistance = kKeyMinimumRadarMile;
                } else {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                    }
                    // Show people
                }
                [self calculateAndGetNewUsers:nearbyUsers];
                //                self.slider1.value= (translate.x * self.slider1.maximumValue)/SCREEN_WIDTH;
            } else {
                latestDistance = settingDistance;
                // Show people
                [self calculateAndGetNewUsers:nearbyUsers];
            }


            sliderX = translate.x;
            if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
            } else
                swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);

            if (translate.x >= (SCREEN_WIDTH / 2)) {
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
            } else {
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
            }

        }
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        self.progressContainerView.hidden = YES;
        if (translationInView.x > 0) {
            // While changing state of slider to increasing value of slider
            [UIView animateWithDuration:3.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                             }
                             completion:^(BOOL finished) {
                                 if (translate.x <= self.slider1.frame.origin.x) {
                                     self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);

                                     self->lblDistance.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y, 120, 40);
                                     self->lblNoPPl.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y + 40, 140, 22);

                                 } else if (translate.x <= self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                                     if (progress < 1) {

                                         self->latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;

                                         if (self->latestDistance <= kKeyMinimumRadarMile) {
                                             self->latestDistance = kKeyMinimumRadarMile;
                                         } else {
                                             if (self->latestDistance >= kKeyMaximumRadarMile) {
                                                 self->latestDistance = kKeyMaximumRadarMile;
                                             }
                                         }
                                         // Show people
                                         [self calculateAndGetNewUsers:self->nearbyUsers];
                                     } else {
                                         self->latestDistance = self->settingDistance;
                                         // Show people
                                         [self calculateAndGetNewUsers:self->nearbyUsers];
                                     }

                                     self->sliderX = translate.x;
                                     if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                                         self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                                     } else
                                         self->swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);
                                     if (translate.x >= (SCREEN_WIDTH / 2)) {
                                         self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 140, self->lblDistance.frame.origin.y, 120, 40);
                                         self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 160, self->lblDistance.frame.origin.y + 40, 140, 22);
                                     } else {
                                         self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y, 140, 40);
                                         self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y + 40, 140, 22);
                                     }
                                 }
                             }];
        } else {
            // While changing state of slider to decreasing value of slider
            if (progress < 1) {
                latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;
                if (latestDistance <= kKeyMinimumRadarMile) {
                    latestDistance = kKeyMinimumRadarMile;
                } else {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                    }
                }
                // Show people
                [self calculateAndGetNewUsers:nearbyUsers];
            } else {
                latestDistance = settingDistance;
                // Show people
                [self calculateAndGetNewUsers:nearbyUsers];
            }
            // Set swiping view
            if (translate.x < self.slider1.frame.origin.x) {
                swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
            } else {
                if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                    swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                } else
                    swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);
                if (translate.x >= SCREEN_WIDTH / 2) {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
                } else {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
                }
            }
        }
    }
}


//
//--------------------------------------------------------------------------
// refreshRadarBtnClick
- (void)refreshRadarBtnClick {
    [APP_DELEGATE showActivityIndicator];
    [self makeServerCallToGetGroupList:server.myLocation];
}

#pragma mark
#pragma mark - tapGesture Method
#pragma mark

//
//----------------------------------------------------------------
//longPressTap:
- (void)longPressTap:(UILongPressGestureRecognizer *)recognizer {

    if (recognizer.state == UIGestureRecognizerStateEnded) {
        swipeContainerView.hidden = YES;
        return;
    }
    if (recognizer.view.tag == 1) {
        // Long press detected, start the timer
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
        CGFloat distancevalue = 0.0;
        if (strMilesData == nil) {
            distancevalue = kKeyMaximumRadarMile;
        } else {
            NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
            distancevalue = [strArray[0] floatValue];
        }
        self.slider1.maximumValue = distancevalue;
        latestDistance = distancevalue;
        [self.slider1 setValue:distancevalue animated:YES];

        [userdef setValue:[NSString stringWithFormat:@"%f", distancevalue] forKey:kKeyTempMiles_Data];
        [userdef synchronize];

        swipeContainerView.hidden = NO;
        [UIView animateWithDuration:0.0 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width + self.slider1.frame.origin.x, self.view.frame.size.height);
            self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 150, self->lblDistance.frame.origin.y, 120, 40);
            self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 150, self->lblDistance.frame.origin.y + 40, 140, 22);
                    [self.view bringSubviewToFront:self.progressContainerView];
                }
                         completion:^(BOOL finished) {
                         }];
    } else if (recognizer.view.tag == 2) {
        // Long press detected, start the timer
        [self.slider1 setValue:0.0 animated:YES];
        latestDistance = kKeyMinimumRadarMile;
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
        [userdef synchronize];

        swipeContainerView.hidden = NO;
        [UIView animateWithDuration:0.0
                              delay:0.2
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
            self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);

            self->lblDistance.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y, 120, 40);
            self->lblNoPPl.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y + 40, 140, 22);
                             [self.view bringSubviewToFront:self.progressContainerView];
                         }
                         completion:^(BOOL finished) {
                         }];
    }
    [self calculateAndGetNewUsers:nearbyUsers];
}

//
//----------------------------------------------------------------
//onDotTapped:
- (void)onDotTapped:(UITapGestureRecognizer *)recognizer {
    UIView *circleView = recognizer.view;
    CGPoint point = [recognizer locationInView:circleView];
    // The for loop is to find out multiple dots in vicinity
    // you may define a NSMutableArray before the for loop and
    // get the group of dots together
    NSMutableArray *tappedUsers = [NSMutableArray array];
    for (SRRadarDotView *d in dotsArr) {

        if ([d.layer.presentationLayer hitTest:point] != nil) {
            // you can get the list of tapped user(s if more than one users are close enough)
            // use this variable outside of for loop to get list of users
            [tappedUsers addObject:d.userProfile];

        }
    }
    // use tappedUsers variable according to your app logic
    if (tappedUsers.count > 0) {
        [self addViewsOnScrollView:tappedUsers];
    } else {
        [self.scrollView setHidden:YES];
        [self.toolTipScrollViewImg setHidden:YES];
    }
}

// --------------------------------------------------------------------------------
// windowTapGesture:

- (void)windowTapAction:(UITapGestureRecognizer *)sender {

    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }
    [self.scrollView setHidden:YES];
    [self.toolTipScrollViewImg setHidden:YES];


    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];
    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    [dictionary setObject:[NSString stringWithFormat:@"1"] forKey:@"isGroup"];
    if ([userDataDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDataDict[kKeyMediaImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    server.groupDetailInfo = [SRModalClass removeNullValuesFromDict:dictionary];
    SRGroupDetailTabViewController *objGroupTab = [[SRGroupDetailTabViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:objGroupTab animated:YES];

}

// --------------------------------------------------------------------------------
// actionOnPingBtnClick
- (void)actionOnPingBtnClick:(UIButton *)sender {

    //badgeBtn.hidden = YES;
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    // Get profile
    if ([dictionary[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [dictionary[kKeyProfileImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    [dictionary setObject:@"1" forKey:@"isGroup"];
    // Get profile
    server.groupDetailInfo = [SRModalClass removeNullValuesFromDict:dictionary];

    // Set chat view
    SRChatViewController *objSRChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:server.groupDetailInfo];

    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:objSRChatView animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

// --------------------------------------------------------------------------------
// compassButtonAction

- (void)compassButtonAction:(UIButton *)sender {

    // Get profile
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    NSDictionary *dataDict = [SRModalClass removeNullValuesFromDict:calloutView.userDict];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dataDict];
    SRMapViewController *compassView = [[SRMapViewController alloc] initWithNibName:@"FromGroupListView" bundle:nil inDict:dictionary server:server];
//    (APP_DELEGATE).topBarView.hidden = YES;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:compassView animated:YES];
    self.hidesBottomBarWhenPushed = NO;

}


// --------------------------------------------------------------------------------
// viewTapAction:

- (void)viewTapAction:(UITapGestureRecognizer *)sender {
    [self.scrollView setHidden:YES];
    [self.toolTipScrollViewImg setHidden:YES];
}

//
//----------------------------------------------------------------
//tapOnRefreshOverlay:
- (void)tapOnRefreshOverlay:(UITapGestureRecognizer *)recognizer {
    //Hide radar refresh overlay
    [_radarRefreshOverlay setHidden:YES];
}

//
//---------------------------------------------------------------
//longPressRadar:
- (void)longPressRadar:(UILongPressGestureRecognizer *)recognizer {

    [UIView animateWithDuration:0.0 delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{

                     } completion:^(BOOL finished) {

                         [self->_radarRefreshOverlay setHidden:NO];
                         if (!self->_radarRefreshOverlay) {
                    // Add view to refresh radar
                    self->_radarRefreshOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                             self->_radarRefreshOverlay.backgroundColor = [UIColor blackColor];
                    self->_radarRefreshOverlay.alpha = 0.7;
                    self->_radarRefreshOverlay.userInteractionEnabled = TRUE;
                    self->_radarRefreshOverlay.tag = 100000;

                    // Add update lbl
                    UILabel *updateLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 70, SCREEN_HEIGHT - 235, 140, 20)];
                    [updateLbl setText:@"Update radar now"];
                    [updateLbl setTextColor:[UIColor whiteColor]];
                    [updateLbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:14]];
                    [updateLbl setTextAlignment:NSTextAlignmentCenter];
                    [self->_radarRefreshOverlay addSubview:updateLbl];

                    // add refresh btn
                    UIButton *refreshBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, SCREEN_HEIGHT - 195, 60, 60)];
                    refreshBtn.layer.cornerRadius = refreshBtn.frame.size.width / 2;
                    [refreshBtn setImage:[UIImage imageNamed:@"update-radar-white.png"] forState:UIControlStateNormal];
                    [refreshBtn setBackgroundColor:[UIColor whiteColor]];
                    [refreshBtn addTarget:self action:@selector(refreshRadarBtnClick) forControlEvents:UIControlEventTouchUpInside];
                    [refreshBtn setUserInteractionEnabled:TRUE];

                    [self->_radarRefreshOverlay addSubview:refreshBtn];

                    UITapGestureRecognizer *tapGestOnRefreshOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRefreshOverlay:)];
                             [self->_radarRefreshOverlay addGestureRecognizer:tapGestOnRefreshOverlay];

                             [self.tabBarController.view addSubview:self->_radarRefreshOverlay];
                    [self.view bringSubviewToFront:refreshBtn];
                }

                //[self.view bringSubviewToFront:_radarRefreshOverlay];
            }];
}

#pragma mark
#pragma mark Notification Method
#pragma mark
// --------------------------------------------------------------------------------
// filterGroupUserListOnMap:

- (void)filterGroupUserListOnMap:(NSNotification *)inNotify {
    latestDistance = [[inNotify object] floatValue];
    self.slider1.value = latestDistance;
    [self calculateAndGetNewUsers:nearbyUsers];
}

// --------------------------------------------------------------------------------
// getUpdateLocation:

- (void)getUpdateLocation:(NSNotification *)inNotify {
    // Update list
    [self makeServerCallToGetGroupList:server.myLocation];
}

// --------------------------------------------------------------------------------
// updateHeadingAngle:

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    currentDeviceBearing = heading;
    [self rotateArcsToHeading:headingAngle];
}

// --------------------------------------------------------------------------------
// ShowGroupSucceed:

- (void)ShowGroupSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    //[_radarRefreshOverlay setHidden:YES];
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;

    // Display the dots
    NSDictionary *dictionary = [inNotify object];
    if ([dictionary isKindOfClass:[NSDictionary class]] && dictionary[kKeyGroupList] != nil) {
        NSArray *groupListArr = dictionary[kKeyGroupList];
        
        if ([APP_DELEGATE isUserBelow18]){
            NSMutableArray *newGroupListArr = [NSMutableArray new];
            
            for (NSDictionary* dict in groupListArr){
                BOOL isExistId = NO;
                
                NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[dict valueForKey:kKeyGroupMembers]];
                for (NSDictionary *dict in groupMemberArray) {
                    if ([[dict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                        isExistId = YES;
                    }
                }
//                if (isExistId && [[NSString stringWithFormat:@"%@",[dict valueForKey:@"group_type"]] isEqualToString:@"0"]){
                if (isExistId){
                    [newGroupListArr addObject:dict];
                }
            }
            groupListArr = [[NSArray alloc] initWithArray:newGroupListArr];
        }

        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        groupListArr = [groupListArr sortedArrayUsingDescriptors:sortDescriptors];

        groupUsers = dictionary[kKeyGroupUsers];
        server.groupUsersArr = groupUsers;
        if (![sourceArr isEqual:groupListArr]) {
            sourceArr = groupListArr;
            if (settingDistance == latestDistance) {
                nearbyUsers = sourceArr;
                [self displayUsersDotsOnRadar];
            } else {
                [self displayUsersDotsOnRadar];
                [self calculateAndGetNewUsers:sourceArr];
            }

            // Show text for no people
            self.lblInPpl.text = [NSString stringWithFormat:@"%ld group in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];


            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationGroupRadarChanges object:nearbyUsers];
        }
    } else if ([dictionary isKindOfClass:[NSDictionary class]]) {
        // Check whether edit or create
        NSDictionary *postInfo = [inNotify userInfo];
        NSDictionary *requestedParam = postInfo[kKeyRequestedParams];

        NSMutableArray *mutatedArr = [NSMutableArray array];
        [mutatedArr addObjectsFromArray:sourceArr];

        if (requestedParam[@"_method"] != nil) {
            // Find the dict and replace it
            BOOL found = NO;
            for (NSUInteger i = 0; i < [mutatedArr count] && !found; i++) {
                NSDictionary *dict = mutatedArr[i];
                if ([dictionary[kKeyId] isEqualToString:dict[kKeyId]]) {
                    found = YES;
                    mutatedArr[i] = dictionary;
                }
            }
        } else {

            [mutatedArr addObject:dictionary];

        }

        // Sorting
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        sourceArr = [mutatedArr sortedArrayUsingDescriptors:sortDescriptors];

        if (settingDistance == latestDistance) {
            nearbyUsers = sourceArr;
            [self displayUsersDotsOnRadar];
        } else {
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers:sourceArr];
        }

        // Show text for no people
        self.lblInPpl.text = [NSString stringWithFormat:@"%ld group in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationGroupRadarChanges object:nearbyUsers];
    } else {
        // Else deleted
        NSDictionary *postInfo = [inNotify userInfo];
        NSString *objectUrl = postInfo[kKeyObjectUrl];
        NSString *responceStr = postInfo[kKeyResponse];


        //Array
        NSMutableArray *mutatedArr = [NSMutableArray array];
        NSString *groupId;

        if ([responceStr isEqualToString:@"group deleted successfully"]) {
            NSArray *componentsArr = [objectUrl componentsSeparatedByString:@"/"];
            if ([componentsArr count] > 1) {
                groupId = componentsArr[1];
            } else {
                //kKeyRequestedParams
                groupId = [postInfo[kKeyRequestedParams] objectForKey:kKeyGroupID];
            }

            [mutatedArr addObjectsFromArray:sourceArr];
            BOOL found = NO;
            for (NSUInteger i = 0; i < [mutatedArr count] && !found; i++) {
                NSDictionary *dict = mutatedArr[i];
                if ([groupId isEqualToString:dict[kKeyId]]) {
                    found = YES;
                    [mutatedArr removeObject:dict];
                }
            }


            // Sorting
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
            NSArray *sortDescriptors = @[sortDescriptor];
            sourceArr = [mutatedArr sortedArrayUsingDescriptors:sortDescriptors];

            if (settingDistance == latestDistance) {
                nearbyUsers = sourceArr;
                [self displayUsersDotsOnRadar];
            } else {
                [self displayUsersDotsOnRadar];
                [self calculateAndGetNewUsers:sourceArr];
            }
            // Show text for no people
            self.lblInPpl.text = [NSString stringWithFormat:@"%ld group in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];

            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationGroupRadarChanges object:nearbyUsers];
        }
    }
}

// --------------------------------------------------------------------------------
// ShowGroupFailed:

- (void)ShowGroupFailed:(NSNotification *)inNotify {
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;

    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// AcceptRejectGroupSucceed:
- (void)AcceptRejectGroupSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// AcceptRejectGroupFailed:
- (void)AcceptRejectGroupFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}


#pragma mark
#pragma mark TextField Delegate method
#pragma mark

// --------------------------------------------------------------------------------
//  textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    // Search
    [self searchEventForSearchText:textField.text];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self searchEventForSearchText:str];

    // Return
    return YES;
}



// --------------------------------------------------------------------------------
// textFieldDidEndEditing

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // Search
    [self searchEventForSearchText:nil];
    [textField resignFirstResponder];

    // Return
    return YES;
}
// --------------------------------------------------------------------------------
// searchEventsForSearchText:

- (void)searchEventForSearchText:(NSString *)searchText {
    if ([searchText length] > 0) {
        NSString *filter = @"%K CONTAINS[cd] %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyName, searchText];
        nearbyUsers = [nearbyUsers filteredArrayUsingPredicate:predicate];
        [self displayUsersDotsOnRadar];
        
    } else {
        if (isGroupFilterApplied) {
            nearbyUsers = filteredArray;
        } else
            nearbyUsers = sourceArr;
        if (settingDistance == latestDistance) {
            [self displayUsersDotsOnRadar];
        } else {
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers:nearbyUsers];
        }
    }
    // Show text for no people
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld group in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
    [self.scrollView setHidden:YES];
}

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txtSearchField resignFirstResponder];
}

#pragma MARK CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    // rotate north view
    self.northLineView.transform = CGAffineTransformMakeRotation(-degreesToRadians(newHeading.trueHeading));
}

@end
