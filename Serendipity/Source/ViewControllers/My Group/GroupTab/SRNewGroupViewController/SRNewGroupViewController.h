#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SRCountryPickerViewController.h"

@interface SRNewGroupViewController : ParentViewController <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,MFMessageComposeViewControllerDelegate,CountryPickerDelegate>
{
	// Server Instance
	NSMutableArray *membersArr;
	NSMutableArray *adminArr;
	NSMutableArray *cacheImgArr;

	NSDictionary *groupInfoDict;

	SRServerConnection *server;
	CLLocationCoordinate2D locationCoordinate;

	NSData *imgData;
    BOOL isDeleted;
    BOOL isOnce;
    BOOL canEdit;
    NSString *countryMasterId;
}

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *imgBackgroundPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddPic;
@property (weak, nonatomic) IBOutlet UILabel *lblAddPic;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPic;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewGrpDetails;
@property (weak, nonatomic) IBOutlet UITextField *txtGroupName;
@property (weak, nonatomic) IBOutlet UITextField *txtAddTags;
@property (weak, nonatomic) IBOutlet UITextField *txtSetLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTxtDesc;
@property (weak, nonatomic) IBOutlet UITextView *txtViewGrpDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;

@property (weak, nonatomic) IBOutlet UILabel *lblGroupType;
@property (weak, nonatomic) IBOutlet UILabel *lblPublic;
//@property (weak, nonatomic) IBOutlet UISwitch *switchGroupType;
@property (weak, nonatomic) IBOutlet UIButton *publicRadioBtn,*privteRadioBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivate;
@property (weak, nonatomic) IBOutlet UILabel *lblEnableTracking;
@property (weak, nonatomic) IBOutlet UISwitch *switchEnableTracking;
@property (weak, nonatomic) IBOutlet UILabel *lblGroup_is;
@property (weak, nonatomic) IBOutlet UILabel *lblOff;
//@property (weak, nonatomic) IBOutlet UISwitch *switchGroup_is;
@property (weak, nonatomic) IBOutlet UIButton *onRadioBtn,*offRadioBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblOn;
@property (weak, nonatomic) IBOutlet UILabel *lblAddMembers;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollAddMember;

// For future usage
@property (strong, nonatomic) IBOutlet UIView *viewAddMembers;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAddPicBG;
@property (weak, nonatomic) IBOutlet UILabel *lblAddMember;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFromConn;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryName;
@property (weak, nonatomic) IBOutlet UIButton *countryPicBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (strong, nonatomic)UILabel *lblPhoneCode;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewContact;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewAddPic;
// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
                 dict:(NSDictionary *)dataDict
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark Action Method
#pragma mark

- (IBAction)actionOnButtonClick:(id)sender;
- (IBAction)actionOnSwitchClick:(id)sender;

@end
