#import "SRModalClass.h"
#import "SRNewGroupViewController.h"
#import "SRLocationSetViewController.h"
#import "SRMyConnectionsViewController.h"
#import "SRContactsViewController.h"
#import "SRMemberViewController.h"
#import "CommonFunction.h"

#define kLeftBtnTag 600
#define kRightBtnTag 601

#define kKeySelectedObj @"selecetdObj"

@implementation SRNewGroupViewController

#pragma mark
#pragma mark Tap Gesture
#pragma mark

// ---------------------------------------------------------------------------------------
// hideKeyboard:

- (void)hideKeyboard:(UITapGestureRecognizer *)gesture {
    self.scrollViewGrpDetails.contentOffset = CGPointMake(0, 0);
    self.scrollViewAddPic.contentOffset = CGPointMake(0, 0);
    [self.view endEditing:YES];
}

#pragma mark
#pragma mark Scroll Customization Method
#pragma mark

// --------------------------------------------------------------------------------
// addDashedBorderWithColor:

- (CAShapeLayer *)addDashedBorderWithColor:(CGFloat)lineNo {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    CGSize frameSize = CGSizeMake(65, 65);
    CGRect shapeRect = CGRectMake(0.0f, 0.0f, frameSize.width, frameSize.height);
    [shapeLayer setBounds:shapeRect];
    [shapeLayer setPosition:CGPointMake(frameSize.width / 2, frameSize.height / 2)];

    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[[UIColor whiteColor] CGColor]];
    [shapeLayer setLineWidth:lineNo];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineDashPattern:
            @[@10,
                    @2]];

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:shapeRect cornerRadius:frameSize.width / 2];
    [shapeLayer setPath:path.CGPath];

    // Return
    return shapeLayer;
}

// ------------------------------------------------------------------------------------------------------
// addMembersInScrollView:

- (void)addImagesInScrollView {
    CGRect latestFrame = CGRectZero;
    NSArray *groupMemberArr = [[NSArray alloc] initWithArray:groupInfoDict[kKeyGroupMembers]];
    NSMutableArray *adminArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < groupMemberArr.count; i++) {
        if ([[groupMemberArr[i] valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
            [adminArray addObject:[groupMemberArr[i] valueForKey:kKeyUserID]];
        }
    }

    // Add images to scroll view accordingly
    for (NSUInteger i = 0; i < [membersArr count]; i++) {
        NSDictionary *dict = membersArr[i];
        UIView *subView = [[UIView alloc] init];
        if (i == 0) {
            subView.frame = CGRectMake(5, 0, 70, self.scrollAddMember.frame.size.height);
        } else
            subView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 4, 0, 70, self.scrollAddMember.frame.size.height);
        latestFrame = subView.frame;
        subView.backgroundColor = [UIColor clearColor];

        // Add image view
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.layer.cornerRadius = imgView.frame.size.width / 2.0;
        imgView.layer.masksToBounds = YES;
        imgView.image = [UIImage imageNamed:@"profile_menu.png"];

        UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 46, -2, 20, 20)];


        if ([dict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *profileDict = dict[kKeyProfileImage];

            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {

                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                imgView.image = [UIImage imageNamed:@"profile_menu.png"];
        } else
            imgView.image = [UIImage imageNamed:@"profile_menu.png"];


        [btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        btnDelete.tag = i;
        [btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];

        [subView addSubview:imgView];
        [subView addSubview:btnDelete];

        // Name
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 67, 70, 18)];
        lblName.textAlignment = NSTextAlignmentCenter;
        lblName.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
        lblName.textColor = [UIColor whiteColor];
        NSString *lastName = [dict[kKeyLastName] substringToIndex:1];
        lblName.text = [NSString stringWithFormat:@"%@ %@.", dict[kKeyFirstName], lastName];
        [subView addSubview:lblName];

        // Button made admin
        UIButton *btnAddAdmin = [[UIButton alloc] initWithFrame:CGRectMake(0, 67 + 15, 70, 30)];
        if ([adminArray containsObject:[dict valueForKey:kKeyId]]) {
            // Already admin of group
            [btnAddAdmin setTitle:@"Admin" forState:UIControlStateNormal];
            [btnAddAdmin setTitleColor:[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
            [adminArr addObject:dict];
        } else {
            [btnAddAdmin setTitle:@"Make Admin" forState:UIControlStateNormal];
            [btnAddAdmin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        [btnAddAdmin addTarget:self action:@selector(actionOnMakeAdmin:) forControlEvents:UIControlEventTouchUpInside];
        btnAddAdmin.titleLabel.textAlignment = NSTextAlignmentCenter;
        btnAddAdmin.titleLabel.font = [UIFont fontWithName:kFontHelveticaBold size:10.0];
        btnAddAdmin.tag = i;
        [subView addSubview:btnAddAdmin];


        // Add to the scroll view
        [self.scrollAddMember addSubview:subView];
        if (!canEdit) {
            subView.userInteractionEnabled = NO;
        }
    }
    // Check if lastest frame is not empty
    // Add to scroll view check if user can add group members
    if ([groupInfoDict[kKeyCreatedBy] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] || canEdit) {
        if (CGRectEqualToRect(latestFrame, CGRectZero)) {
            latestFrame = CGRectMake(5, 0, 70, self.scrollAddMember.frame.size.height);
        } else
            latestFrame = CGRectMake(latestFrame.origin.x + 70, 0, 70, self.scrollAddMember.frame.size.height);

        UIView *btnSuperView = [[UIView alloc] init];
        btnSuperView.frame = latestFrame;

        UIButton *btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        [btnAdd addTarget:self action:@selector(actionOnAddMember:) forControlEvents:UIControlEventTouchUpInside];
        [btnSuperView addSubview:btnAdd];

        [btnAdd.layer addSublayer:[self addDashedBorderWithColor:1]];
        [btnAdd setImage:[UIImage imageNamed:@"miles-set-plus.png"] forState:UIControlStateNormal];
        [self.scrollAddMember addSubview:btnSuperView];
    }

    if ((latestFrame.origin.x + 30) > SCREEN_WIDTH) {
        self.scrollAddMember.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
        self.scrollAddMember.scrollEnabled = YES;
        self.scrollAddMember.backgroundColor = [UIColor clearColor];
    }
}

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
                 dict:(NSDictionary *)dataDict
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SRNewGroupViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        groupInfoDict = dataDict;

        membersArr = [[NSMutableArray alloc] init];
        adminArr = [[NSMutableArray alloc] init];
        cacheImgArr = [[NSMutableArray alloc] init];

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(CreateGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(CreateGroupFailed:)
                              name:kKeyNotificationCreateGroupFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(inviteUserSucceed:)
                              name:kKeyNotificationInviteUserSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(inviteUserFailed:)
                              name:kKeyNotificationInviteUserFailed object:nil];
    }

    //return
    return self;
}

// --------------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call Super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    if (groupInfoDict == nil)
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.createNewGroup.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    else
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.editgroup.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    leftButton.tag = kLeftBtnTag;
    [leftButton addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"edit_profile_white.png"] forViewNavCon:self offset:-23];
    rightButton.tag = kRightBtnTag;
    [rightButton addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    // AddPic Background View Rounded Appearance.
    self.imgAddPic.layer.cornerRadius = self.imgAddPic.frame.size.width / 2.0;
    self.imgAddPic.layer.masksToBounds = YES;

    self.switchEnableTracking.transform = CGAffineTransformMakeScale(0.95, 0.95);
    self.switchEnableTracking.layer.cornerRadius = 16.0f;
    [self.switchEnableTracking setOn:NO animated:YES];

    self.lblTxtDesc.frame = CGRectMake(0, 45, self.txtViewGrpDesc.frame.size.width, 18);
    self.lblTxtDesc.textAlignment = NSTextAlignmentCenter;
    [self.txtViewGrpDesc addSubview:self.lblTxtDesc];

    // Add tap gesture on scrollview
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [self.scrollViewGrpDetails addGestureRecognizer:singleTap];

    // Add memebers
    if (groupInfoDict == nil) {
        canEdit = YES;
    }
    [self addImagesInScrollView];

    // Add subview for add new connection
    self.viewAddMembers.layer.cornerRadius = 5;
    self.viewAddMembers.layer.masksToBounds = YES;
    [self.view addSubview:self.viewAddMembers];
    self.viewAddMembers.hidden = YES;

    // Set radio buttons
    [self.onRadioBtn setSelected:TRUE];
    [self.publicRadioBtn setSelected:TRUE];
    
    self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtName.placeholder attributes:@{NSForegroundColorAttributeName:UIColor.blackColor}];
    self.txtPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtPhoneNumber.placeholder attributes:@{NSForegroundColorAttributeName:UIColor.blackColor}];
}

//---------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;

    if ((groupInfoDict != nil) && !isOnce) {
        isOnce = YES;
        groupInfoDict = [SRModalClass removeNullValuesFromDict:groupInfoDict];
        self.txtGroupName.text = [groupInfoDict valueForKey:kKeyName];
        self.txtSetLocation.text = [groupInfoDict valueForKey:kKeyAddress];
        self.txtAddTags.text = [groupInfoDict valueForKey:kKeyGroupTag];
        if ([[groupInfoDict valueForKey:kKeyDescription] isKindOfClass:[NSString class]]) {
            self.txtViewGrpDesc.text = [groupInfoDict valueForKey:kKeyDescription];
            self.lblTxtDesc.hidden = YES;
        }

        if ([[groupInfoDict valueForKey:kKeyGroupType] isEqualToString:@"1"]) {
            //[self.switchGroupType setOn:YES];
            [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.publicRadioBtn setSelected:TRUE];
            [self.privteRadioBtn setSelected:FALSE];
        } else {
            //[self.switchGroupType setOn:NO];
            [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.privteRadioBtn setSelected:TRUE];
            [self.publicRadioBtn setSelected:FALSE];
        }

        if ([[groupInfoDict valueForKey:kKeyEnableTracking] isEqualToString:@"1"]) {
            [self.switchEnableTracking setOn:YES];
        } else {
            [self.switchEnableTracking setOn:NO];
        }
        if ([[groupInfoDict valueForKey:kKeyGroup_is] isEqualToString:@"1"]) {
            //[self.switchGroupType setOn:YES];
            [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.onRadioBtn setSelected:TRUE];
            [self.offRadioBtn setSelected:FALSE];
        } else {
            //[self.switchGroupType setOn:NO];
            [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.offRadioBtn setSelected:TRUE];
            [self.onRadioBtn setSelected:FALSE];
        }

        //For group Profile image
        //default Image
        self.imgAddPic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        self.imgBackgroundPic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        // Group image
        if (groupInfoDict[kKeyImageObject] != nil) {
            self.imgAddPic.image = groupInfoDict[kKeyImageObject];
            self.imgBackgroundPic.image = groupInfoDict[kKeyImageObject];
        } else {
            if ([groupInfoDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
                // Get result
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    UIImage *img = nil;

                    if ([groupInfoDict[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                        NSString *imageName = [groupInfoDict[kKeyMediaImage] objectForKey:kKeyImageName];
                        if ([imageName length] > 0) {
                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                            img = [UIImage imageWithData:
                                    [NSData dataWithContentsOfURL:
                                            [NSURL URLWithString:imageUrl]]];
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (img) {
                                self.imgAddPic.image = img;
                                self.imgBackgroundPic.image = img;
                            }
                        });
                    }
                });
            } else {
                self.imgAddPic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                self.imgBackgroundPic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            }
        }
        // Filter the users and alloc the views
        for (NSDictionary *userDict in server.groupDetailInfo[kKeyGroupMembers]) {
            NSMutableDictionary *userDetails = [NSMutableDictionary dictionary];
            if (userDict[kKeyFirstName] != nil) {
                [userDetails addEntriesFromDictionary:userDict];
            } else if (userDict[kKeyUser] != nil) {
                userDetails = userDict[kKeyUser];
            } else {
                NSString *userId = userDict[kKeyUserID];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
                NSArray *filterArr = [server.groupUsersArr filteredArrayUsingPredicate:predicate];
                if ([filterArr count] > 0) {
                    userDetails = filterArr[0];
                }
            }

            // Add dict to mutable array
            userDetails[kKeySelectedObj] = @"selected";
            [membersArr addObject:userDetails];
        }

        NSDictionary *grpDict = (APP_DELEGATE).server.groupDetailInfo;
        NSArray *members = grpDict[kKeyGroupMembers];
        canEdit = NO;
        for (NSUInteger i = 0; i < [members count] && !canEdit; i++) {
            NSDictionary *dict = members[i];
            if ([dict[kKeyIsAdmin] isEqualToString:@"1"] && [dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
                canEdit = YES;
            }
        }


        if ([adminArr containsObject:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
            if ([[grpDict valueForKey:kKeyGroupType] boolValue]) {
                canEdit = YES;
            } else {
                self.txtGroupName.enabled = NO;
                self.btnLocation.userInteractionEnabled = NO;
                self.txtSetLocation.userInteractionEnabled = NO;
                self.txtAddTags.enabled = NO;
                self.txtViewGrpDesc.userInteractionEnabled = NO;
                self.switchEnableTracking.enabled = NO;
                self.publicRadioBtn.enabled = NO;
                self.privteRadioBtn.enabled = NO;
                self.onRadioBtn.enabled = NO;
                self.offRadioBtn.enabled = NO;
                self.btnAddPic.userInteractionEnabled = NO;
            }

        }

        // Add memebers
        [self addImagesInScrollView];
    }
}

#pragma mark
#pragma mark Country Picker Delegates Methods
#pragma mark


-(void) prefillCountryCode{
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSArray *countryArray = server.countryArr;

    for (NSDictionary *dict in countryArray) {
        if ([[dict valueForKey:@"iso"] isEqualToString:countryCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]];

            [self.txtCountryName setText:[dict valueForKey:kkeyCountryName]];

            UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 74, 30)];

            container.backgroundColor = [UIColor clearColor];

            self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 74, 30)];
            self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
            self.lblPhoneCode.textColor = [UIColor blackColor];
            _lblPhoneCode.textAlignment = NSTextAlignmentCenter;
            if (dict && [dict valueForKey:kKeyPhoneCode]) {
                [self.lblPhoneCode setText:[@"+" stringByAppendingString:[NSString stringWithFormat:@"%@", [dict valueForKey:kKeyPhoneCode]]]];
            }

            [container addSubview:self.lblPhoneCode];
            self.txtPhoneNumber.textAlignment = NSTextAlignmentLeft;

            self.txtPhoneNumber.leftView = container;
            self.txtPhoneNumber.leftViewMode = UITextFieldViewModeAlways;

        }
    }
}

// ---------------------------------------------------------------------------------------
// pickCountry:
- (void)pickCountry:(NSDictionary *)countryDict {
    if (countryDict != nil) {

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyPhoneCode]];

        }

        [self.txtCountryName setText:[countryDict valueForKey:kkeyCountryName]];

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];

        }
        [self.txtPhoneNumber becomeFirstResponder];
    } else {
        [self.lblPhoneCode setText:nil];
        [self.txtCountryName setText:nil];
        [self.txtPhoneNumber resignFirstResponder];
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark
// ----------------------------------------------------------------------------------------------
// actionOnCountryBtn

- (IBAction)actionOnCountryBtn {
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}

- (IBAction)btnAddFromConnAction:(id)sender {
    SRMemberViewController *membersCon = [[SRMemberViewController alloc] initWithNibName:nil bundle:nil server:server addedMembers:membersArr];
    membersCon.delegate = self;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:membersCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;

    self.viewAddMembers.hidden = YES;
}

- (IBAction)btnAddNewContactAction:(id)sender {
    if (self.txtName.text.length == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.yourName.text", @"")];
    } else if (countryMasterId == nil) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.country.alert", @"")];
    } else if (self.txtPhoneNumber.text.length == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
    } else {
        // Call login api
        NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtPhoneNumber.text];
        if (mobile_Number.length == 10) {

            [self.txtPhoneNumber resignFirstResponder];
            mobile_Number = [countryMasterId stringByAppendingString:mobile_Number];
            // Call server api to get verify code for password
            [APP_DELEGATE showActivityIndicator];
            // Call api
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            params[kKeyMobileNumber] = mobile_Number;
            params[kKeyMessage] = [NSString stringWithFormat:@"Hello %@, %@ %@ has invited you to join the group they made on the Serendipity app.  Please download the app at http://www.serendipity.app to join the group!", self.txtName.text, [server.loggedInUserInfo valueForKey:kKeyFirstName],[server.loggedInUserInfo valueForKey:kKeyLastName]];

//            "http://serendipity.app"
            
            
            NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassSendSMS];
            [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        } else {
            [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
        }
    }
}
// ----------------------------------------------------------------------------------------------------------
// actionOnBtnDelete:

- (void)actionOnBtnDelete:(UIButton *)inSender {
    isDeleted = YES;
    NSInteger tag = inSender.tag;

    // Remove the member
    NSDictionary *dict = membersArr[tag];
    NSString *idStr = [dict valueForKey:kKeyId];
    NSMutableArray *groupMemberIdArray = [groupInfoDict valueForKey:kKeyGroupMembers];
    for (NSDictionary *dict in groupMemberIdArray) {
        if ([idStr isEqualToString:[dict valueForKey:kKeyUserID]]) {
            idStr = [dict valueForKey:kKeyId];
        }
    }

    // Remove object at index
    [membersArr removeObjectAtIndex:tag];

    // Update scroll view
    for (UIView *subview in[self.scrollAddMember subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    // Add objects to scroll view
    [self addImagesInScrollView];
}

// ----------------------------------------------------------------------------------------------------------
// actionOnAddMember:

- (void)actionOnAddMember:(UIButton *)inSender {
    [self prefillCountryCode];
    self.viewAddMembers.hidden = NO;
    [self.viewAddMembers setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2)];
}

// ----------------------------------------------------------------------------------------------------------
// actionOnMakeAdmin:

- (void)actionOnMakeAdmin:(UIButton *)sender {
    NSInteger tag = sender.tag;
    NSDictionary *userDict = membersArr[tag];

    if ([[sender titleForState:UIControlStateNormal] isEqualToString:@"Admin"]) {
        [sender setTitle:@"Make Admin" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        // Remove from admin array
        [adminArr removeObject:userDict];
    } else {
        [sender setTitle:@"Admin" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];

        // Add to admin array
        [adminArr addObject:userDict];
    }

}

// ----------------------------------------------------------------------------------------------------------
// actionOnRadioButton (Group type):

- (IBAction)actionOnGroupTypeBtn:(UIButton *)inSender {
    if (inSender == self.publicRadioBtn) {
        [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.publicRadioBtn setSelected:TRUE];
        [self.privteRadioBtn setSelected:FALSE];
    } else {
        [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.publicRadioBtn setSelected:FALSE];
        [self.privteRadioBtn setSelected:TRUE];
    }
}
// ----------------------------------------------------------------------------------------------------------
// actionOnRadioButton (Group is):

- (IBAction)actionOnGroupIsBtn:(UIButton *)inSender {
    if (inSender == self.onRadioBtn) {
        [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.onRadioBtn setSelected:TRUE];
        [self.offRadioBtn setSelected:FALSE];
    } else {
        [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.offRadioBtn setSelected:TRUE];
        [self.onRadioBtn setSelected:FALSE];
    }
}

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (IBAction)actionOnButtonClick:(id)sender {
    // End editing
    [self.view endEditing:YES];

    if (((UIButton *) sender).tag == kLeftBtnTag) {
        [self.navigationController popViewControllerAnimated:NO];
    } else if (((UIButton *) sender).tag == kRightBtnTag) {
        NSString *errMsg = nil;
        if ([self.txtGroupName.text length] == 0) {
            errMsg = NSLocalizedString(@"empty.groupname.text", @"");
        }
        if ([self.txtSetLocation.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.grouplocation.text", @"");
        }
        // TODO: Group member notify
        if ([membersArr count] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.grouplmember.text", @"");
        }
        if (errMsg) {
            [SRModalClass showAlert:errMsg];
        } else {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            params[kKeyName] = self.txtGroupName.text;
            if ([self.txtViewGrpDesc.text length] > 0)
                params[kKeyDescription] = self.txtViewGrpDesc.text;
            params[kKeyAddress] = self.txtSetLocation.text;
            if ([self.txtAddTags.text length] > 0)
                params[kKeyGroupTag] = self.txtAddTags.text;

            if (locationCoordinate.latitude < 1 && locationCoordinate.longitude <= 1) {
                locationCoordinate = [self getLocationFromAddressString:self.txtSetLocation.text];
                params[kKeyLattitude] = [NSString stringWithFormat:@"%f", locationCoordinate.latitude];
                params[kKeyRadarLong] = [NSString stringWithFormat:@"%f", locationCoordinate.longitude];
            } else {
                params[kKeyLattitude] = [NSString stringWithFormat:@"%f", locationCoordinate.latitude];
                params[kKeyRadarLong] = [NSString stringWithFormat:@"%f", locationCoordinate.longitude];
            }

            if ([self.publicRadioBtn isSelected]) {
                params[kKeyGroupType] = @"1";
            } else {
                params[kKeyGroupType] = @"0";
            }

            if ([self.switchEnableTracking isOn]) {
                params[kKeyEnableTracking] = @"1";
            } else {
                params[kKeyEnableTracking] = @"0";
            }

            if ([self.onRadioBtn isSelected]) {
                params[kKeyGroup_is] = @"1";
            } else {
                params[kKeyGroup_is] = @"0";
            }

            if (imgData == nil) {
                //imgData = UIImagePNGRepresentation(self.imgAddPic.image);
                imgData = UIImageJPEGRepresentation(self.imgAddPic.image, 1.0);
            }
            // Add Group Member Records
            NSArray *sortedArr = ([membersArr valueForKeyPath:@"@distinctUnionOfObjects.id"]);
            NSString *joinedMembersID = [sortedArr componentsJoinedByString:@","];
            params[kKeyGroup_Member_Ids] = joinedMembersID;

            NSArray *sortedAdminArr = ([adminArr valueForKeyPath:@"@distinctUnionOfObjects.id"]);
            NSString *joinedAdminUserId = [sortedAdminArr componentsJoinedByString:@","];
            params[kKeyGroup_Admin_Ids] = joinedAdminUserId;

            // Check for New Groups or Existing Group
            if ([groupInfoDict count] > 0) {
                //For Update Existing Group
                params[@"_method"] = kPUT;

                NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassCreateGroup, groupInfoDict[kKeyId]];
                [server asychronousRequestWithData:params imageData:imgData forKey:kKeyMediaImage toClassType:urlStr inMethodType:kPOST];
            } else {
                // Create group for xmpp chat
                [server asychronousRequestWithData:params imageData:imgData forKey:kKeyMediaImage toClassType:kKeyClassCreateGroup inMethodType:kPOST];
            }
        }
    } else if (sender == self.btnAddPic) {
        UIActionSheet *actionSheet = nil;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

            // Present action sheet
            actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                      delegate:self cancelButtonTitle:NSLocalizedString (@"cancel.button.title.text", "") destructiveButtonTitle:nil
                                             otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), NSLocalizedString(@"camera.text", ""), nil];
        } else {
            // Present action sheet
            actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                      delegate:self cancelButtonTitle:NSLocalizedString (@"cancel.button.title.text", "") destructiveButtonTitle:nil
                                             otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), nil];
        }

        // Show sheet
        [actionSheet showFromRect:[sender frame] inView:self.view animated:YES];
    } else if (sender == self.btnAddNewContact) {
        NSString *errMsg = nil;
        if ([self.txtName.text length] == 0) {
            errMsg = NSLocalizedString(@"empty.groupname.text", @"");
            [SRModalClass showAlert:errMsg];
        } else if ([self.txtCountryName.text length] == 0) {
            errMsg = NSLocalizedString(@"empty.grouptag.text", @"");
            [SRModalClass showAlert:errMsg];
        } else if ([self.txtPhoneNumber.text length] < 10) {
            errMsg = NSLocalizedString(@"empty.grouplocation.text", @"");
            [SRModalClass showAlert:errMsg];
        } else {
            NSMutableDictionary *contactDict = [NSMutableDictionary dictionary];
            contactDict[kKeyFirstName] = self.txtName.text;
            contactDict[kKeyLastName] = self.txtCountryName.text;
            contactDict[kKeyPhoneNumber] = self.txtName.text;
        }
    }
}

- (IBAction)btnLocationAction {
    // Location from map
    SRLocationSetViewController *objLocationView = [[SRLocationSetViewController alloc] initWithNibName:nil bundle:nil server:server];
    objLocationView.delegate = self;
    objLocationView.getLocation = YES;
    objLocationView.address = self.txtSetLocation.text;
    self.navigationController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:objLocationView animated:YES];
    self.navigationController.hidesBottomBarWhenPushed = YES;
}

#pragma mark
#pragma mark - Message Composer delegate
#pragma mark

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
}
// -----------------------------------------------------------------------------------------------------
// getLocationFromAddressString:

- (CLLocationCoordinate2D)getLocationFromAddressString:(NSString *)addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSString *esc_addr = [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"%@?format=json&addressdetails=1&q=%@", kNominatimServer, esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
 //   NSLog(@"%@", result);
    if (result) {
        if ([result containsString:@"\"lat\":"]) {
            NSRange firstRange = [result rangeOfString:@"\"lat\":"];
            NSRange finalRange = NSMakeRange(firstRange.location + firstRange.length + 1, 6);
            NSString *lat = [result substringWithRange:finalRange];
            latitude = [lat doubleValue];
         //   NSLog(@"contain %@", lat);
        }
        if ([result containsString:@"\"lon\":"]) {
            NSRange firstRange = [result rangeOfString:@"\"lon\":"];
            NSRange finalRange = NSMakeRange(firstRange.location + firstRange.length + 1, 6);
            NSString *lon = [result substringWithRange:finalRange];
            longitude = [lon doubleValue];
          //  NSLog(@"contain %@", lon);
        }
//		NSScanner *scanner = [NSScanner scannerWithString:result];
//		if ([scanner scanUpToString:@"\"lat\" :" intoString:nil])
//        {
//            // && [scanner scanString:@"\"lon\" :" intoString:nil]
//			[scanner scanDouble:&latitude];
//			if ([scanner scanUpToString:@"\"lon\" :" intoString:nil])
//            {
//				[scanner scanDouble:&longitude];
//			}
//		}
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

// -----------------------------------------------------------------------------------------------------
// actionOnSwitchClick:

- (IBAction)actionOnSwitchClick:(id)sender {
}

#pragma mark
#pragma mark TextField delegatss
#pragma mark
// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (![textField isEqual:self.txtPhoneNumber]) {
        return YES;
    }
    if ([textField isEqual:self.txtPhoneNumber]) {
        BOOL isValid = YES;
        NSString *errorMsg = nil;
        NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];

        if ([textStrToCheck length] > 14) {
            errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
        } else if (errorMsg == nil) {
            BOOL flag = [SRModalClass numberValidation:string];

            textField.text = [SRModalClass formatPhoneNumber:self.txtPhoneNumber.text deleteLastChar:NO];

            if (!flag) {
                errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
            }
        }
        // Set flag and show alert
        if (errorMsg) {
            isValid = NO;
            [SRModalClass showAlert:errorMsg];
        }

        // Return
        return isValid;
    }
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtGroupName) {
        [self.txtGroupName resignFirstResponder];
    } else if (textField == self.txtAddTags) {
        [self.txtAddTags resignFirstResponder];
        [self.txtViewGrpDesc becomeFirstResponder];
    } else if (textField == self.txtName) {
        [self.txtName resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.txtSetLocation) {
        [textField resignFirstResponder];
        [self btnLocationAction];
    }
    if (textField == self.txtName || textField == self.txtCountryName || textField == self.txtPhoneNumber) {
        CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 40);
        [self.scrollViewAddPic setContentOffset:scrollPoint animated:YES];
    } else {
        CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 5);
        [self.scrollViewGrpDetails setContentOffset:scrollPoint animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollViewGrpDetails setContentOffset:CGPointZero animated:YES];
    if (textField == self.txtName || textField == self.txtCountryName || textField == self.txtPhoneNumber) {
        [textField resignFirstResponder];
        [self.scrollViewAddPic setContentOffset:CGPointZero animated:YES];
    }
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    BOOL flag;

    // If return key pressed
    if ([text isEqualToString:@"\n"]) {
        if (textView == self.txtViewGrpDesc) {
            [self.txtViewGrpDesc resignFirstResponder];
            [self.scrollViewGrpDetails setContentOffset:CGPointMake(0, 0)];
        }
        flag = NO;
    } else {
        flag = YES;
    }
    return flag;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (textView == self.txtViewGrpDesc) {
        self.lblTxtDesc.hidden = YES;
        [self.scrollViewGrpDetails setContentOffset:CGPointMake(0, self.scrollViewGrpDetails.frame.origin.y)];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (![textView hasText]) {
        self.lblTxtDesc.hidden = NO;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    if (![textView hasText]) {
        self.lblTxtDesc.hidden = NO;
    } else {
        self.lblTxtDesc.hidden = YES;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.viewAddMembers.hidden = YES;
    UITouch *touch = [[event allTouches] anyObject];
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - Image Picker Delegate Method

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.imgAddPic.image = nil;
    self.imgBackgroundPic.image = nil;
    imgData = nil;
    // Selection of image done set it to button
    UIImage *selectedImage = [info valueForKey:UIImagePickerControllerEditedImage];
    //imgData = UIImagePNGRepresentation(selectedImage);
    imgData = UIImageJPEGRepresentation(selectedImage, 1.0);
    self.imgAddPic.image = selectedImage;
    self.imgBackgroundPic.image = selectedImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - ActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Instantiate image picker and set delegate
    UIImagePickerController *imagePickerCon = [[UIImagePickerController alloc] init];
    imagePickerCon.delegate = self;
    imagePickerCon.allowsEditing = YES;
    [imagePickerCon setModalPresentationStyle:UIModalPresentationFullScreen];
    
    if (buttonIndex == 0) {
        imagePickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

        CATransition *transition = [CATransition animation];
        transition.duration = 1;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self presentViewController:imagePickerCon animated:NO completion:nil];
    } else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePickerCon.sourceType = UIImagePickerControllerSourceTypeCamera;

        CATransition *transition = [CATransition animation];
        transition.duration = 1;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];

        [self presentViewController:imagePickerCon animated:NO completion:nil];
    }
}

#pragma mark - Location Controller Deelegate

- (void)locationAddressWithLatLong:(NSString *)data centerCoordinate:(CLLocationCoordinate2D)inCenter {
    locationCoordinate = inCenter;
    self.txtSetLocation.text = data;
}

#pragma mark - Member Controller Delegate

- (void)addedMembersList:(NSArray *)inArr {
    // Remove old objects add new
    if (groupInfoDict != nil) {
        NSMutableArray *filteredArr = [NSMutableArray array];
        for (NSDictionary *inDict in membersArr) {
            if ([inDict[kKeyDegree] isKindOfClass:[NSNumber class]] && [inDict[kKeyDegree] integerValue] != 1) {
                [filteredArr addObject:inDict];
            } else if ([inDict[kKeyDegree] isKindOfClass:[NSNull class]]) {
                [filteredArr addObject:inDict];
            }
        }

        //
        [membersArr removeAllObjects];
        [membersArr addObjectsFromArray:inArr];
        [membersArr addObjectsFromArray:filteredArr];

    } else {
        [membersArr removeAllObjects];
        [membersArr addObjectsFromArray:inArr];
    }

    // Update scroll view
    for (UIView *subview in[self.scrollAddMember subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }

    // Add objects to scroll view
    [self addImagesInScrollView];
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// createGroupSucceed:

- (void)CreateGroupSucceed:(NSNotification *)inNotify {
    // Group created successfully
    NSDictionary *dictionary = [inNotify object];
    if (dictionary[kKeyGroupList] == nil) {
        (APP_DELEGATE).server.groupDetailInfo = [inNotify object];
        [self.navigationController popViewControllerAnimated:YES];

        // Create group for xmpp chat
        NSDictionary *groupDict = [inNotify object];
        NSMutableDictionary *groupMutatedDict = [NSMutableDictionary dictionaryWithDictionary:groupDict];

        NSMutableArray *groupList = [NSMutableArray array];
        for (NSDictionary *userdict in groupDict[kKeyGroupMembers]) {
            NSDictionary *userDetails = userdict[kKeyUser];

            NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionary];
            [mutatedDict setValue:userDetails[kKeyId] forKey:kKeyId];
            [mutatedDict setValue:userDetails[kKeyFirstName] forKey:kKeyFirstName];
            [mutatedDict setValue:userDetails[kKeyLastName] forKey:kKeyLastName];
            if ([userDetails[kKeyProfileImage] isKindOfClass:[NSDictionary class]] && [userDetails[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                [mutatedDict setValue:[userDetails[kKeyProfileImage] objectForKey:kKeyImageName] forKey:kKeyProfileImage];
            }
            [groupList addObject:mutatedDict];
        }
        [groupMutatedDict removeObjectForKey:kKeyGroupMembers];
        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:groupMutatedDict];
        NSString *imageName = @"";
        if ([dictionary[@"image"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dictImage = dictionary[@"image"];
            imageName = dictImage[@"file"];
        }
        NSDictionary *dictPram_CometChat = @{kKeyGroupID: dictionary[@"chat_room_id"], @"groupName": dictionary[@"name"], @"groupPwd": @"", @"grouppic": imageName};
    }
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
}

// --------------------------------------------------------------------------------
// createGroupFailed:

- (void)CreateGroupFailed:(NSNotification *)inNotify {
    [(APP_DELEGATE) hideActivityIndicator];
    [SRModalClass showAlert:[NSString stringWithFormat:@"%@", [inNotify object]]];
}
// --------------------------------------------------------------------------------
// inviteUserSucceed:

- (void)inviteUserSucceed:(NSNotification *)inNotify {
    self.txtName.text = nil;
    self.txtPhoneNumber.text = nil;
    self.txtCountryName.text = nil;
    self.lblPhoneCode.text = nil;
    [self.lblPhoneCode removeFromSuperview];
    [self.viewAddMembers setHidden:YES];
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.invition_group.txt", @"")];

}
// --------------------------------------------------------------------------------
// CreateEventFailed:

- (void)inviteUserFailed:(NSNotification *)inNotify {
    [self.viewAddMembers setHidden:YES];
    [APP_DELEGATE hideActivityIndicator];
}


@end
