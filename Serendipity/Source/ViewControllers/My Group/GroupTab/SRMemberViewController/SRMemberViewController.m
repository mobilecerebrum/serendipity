#import "SRModalClass.h"
#import "SRMemberViewCell.h"
#import "SRMemberViewController.h"

#define kKeySelectedObj @"selecetdObj"

@implementation SRMemberViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
         addedMembers:(NSArray *)inArr {
    // Call Super
    if ([nibNameOrNil isEqualToString:@"AddFromGroups"]) {
        addFrom = nibNameOrNil;
    }
    self = [super initWithNibName:@"SRMemberViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        addedMembers = inArr;

        membersArray = [[NSMutableArray alloc] init];
        cacheImgArr = [[NSMutableArray alloc] init];

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getConnectionSucceed:)
                              name:kKeyNotificationGetConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getConnectionFailed:)
                              name:kKeyNotificationGetConnectionFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupFailed:)
                              name:kKeyNotificationCreateGroupFailed object:nil];
    }

    //return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    if ([addFrom isEqualToString:@"AddFromGroups"]) {
        [SRModalClass setNavTitle:NSLocalizedString(@"lbl.Groups.txt", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    } else
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.MyConnections.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"group-add-Nav.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(addGroupBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    if ([addFrom isEqualToString:@"AddFromGroups"]) {
        // Server call to get group list
        [APP_DELEGATE showActivityIndicator];
        NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
        NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
        NSInteger distance = kKeyMaximumRadarMile;

        NSString *urlStr = [NSString stringWithFormat:@"%@?distance=%@&lat=%@&lon=%@", kKeyClassCreateGroup, [NSString stringWithFormat:@"%ldmi", (long) distance], latitude, longitude];
        [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    } else {
        // Server call Get Connection
        [server makeAsychronousRequest:kKeyClassGetConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    }
}

#pragma mark
#pragma mark NavBar Action Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

// ----------------------------------------------------------------------------------------------------------
// addGroupBtnAction:

- (void)addGroupBtnAction:(id)sender {
    NSMutableArray *groupMemberArr = [NSMutableArray array];
    if ([addFrom isEqualToString:@"AddFromGroups"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeySelectedObj, @"selected"];
        NSArray *filterArr = [membersArray filteredArrayUsingPredicate:predicate];
        if ([filterArr count] > 0) {
            for (int i = 0; i < filterArr.count; i++) {
                [groupMemberArr addObjectsFromArray:[filterArr[i] valueForKey:kKeyGroupMembers]];
            }
        }
        [self.delegate addedMembersFromGroups:groupMemberArr];
        [self.navigationController popToViewController:self.delegate animated:NO];
    } else {
        if ([self.delegate respondsToSelector:@selector(addedMembersList:)]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeySelectedObj, @"selected"];
            NSArray *filterArr = [membersArray filteredArrayUsingPredicate:predicate];
            if ([filterArr count] > 0) {
                [self.delegate addedMembersList:filterArr];
                [self.navigationController popToViewController:self.delegate animated:NO];
            }
        }
    }
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return
    return membersArray.count;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// --------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRMemberViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRMemberViewCell *customCell = (SRMemberViewCell *) nibObjects[0];
            cell = customCell;
        }
    }

    cell.backgroundColor = [UIColor clearColor];

    // Assign values
    NSDictionary *userDict = membersArray[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];
    // Assign values
    if ([addFrom isEqualToString:@"AddFromGroups"]) {
        // Name
        ((SRMemberViewCell *) cell).lblName.text = [NSString stringWithFormat:@"%@", userDict[kKeyName]];

        // Member Count
        ((SRMemberViewCell *) cell).lblProfession.text = [NSString stringWithFormat:@"%lu Members", [userDict[kKeyGroupMembers] count]];

        //Image
        id mediaDict = userDict[kKeyMediaImage];
        if ([mediaDict isKindOfClass:[NSDictionary class]] && [mediaDict objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [mediaDict objectForKey:kKeyImageName];
            if ([imageName length] > 0) {

                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [((SRMemberViewCell *) cell).imageProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else {
                ((SRMemberViewCell *) cell).imageProfile.backgroundColor = [UIColor orangeColor];
                ((SRMemberViewCell *) cell).imageProfile.image = [UIImage imageNamed:@"group-white-50px.png"];
            }
        } else {
            ((SRMemberViewCell *) cell).imageProfile.backgroundColor = [UIColor orangeColor];
            ((SRMemberViewCell *) cell).imageProfile.image = [UIImage imageNamed:@"group-white-50px.png"];
        }

    } else {
        // Name
        ((SRMemberViewCell *) cell).lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];

        // Occupation
        if ([userDict[kKeyOccupation] length] > 0)
            ((SRMemberViewCell *) cell).lblProfession.text = userDict[kKeyOccupation];
        else
            ((SRMemberViewCell *) cell).lblProfession.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");
        if ([((SRMemberViewCell *) cell).lblProfession.text isEqualToString:@""]) {
            ((SRMemberViewCell *) cell).lblName.frame = CGRectMake(((SRMemberViewCell *) cell).lblName.frame.origin.x, ((SRMemberViewCell *) cell).lblName.frame.origin.y + 15, ((SRMemberViewCell *) cell).lblName.frame.size.width, ((SRMemberViewCell *) cell).lblName.frame.size.height);
        }
        //Image
        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {

            NSDictionary *profileDict = userDict[kKeyProfileImage];

            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [((SRMemberViewCell *) cell).imageProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                ((SRMemberViewCell *) cell).imageProfile.image = [UIImage imageNamed:@"profile_menu.png"];
        } else {
            ((SRMemberViewCell *) cell).imageProfile.image = [UIImage imageNamed:@"profile_menu.png"];
        }
    }



    // Location
    if ([userDict[kKeyAddress] length] > 0)
        ((SRMemberViewCell *) cell).lblAddress.text = [NSString stringWithFormat:@"in %@", userDict[kKeyAddress]];
    else
        ((SRMemberViewCell *) cell).lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");

    //Live Address
    if ([userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
        if ([[userDict[kKeyLocation] valueForKey:kKeyLiveAddress] length] > 0) {
            ((SRMemberViewCell *) cell).lblAddress.text = [NSString stringWithFormat:@"in %@", [userDict[kKeyLocation] objectForKey:kKeyLiveAddress]];
        } else
            ((SRMemberViewCell *) cell).lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
    }


    // Dating image
    if ([userDict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *settingDict = userDict[kKeySetting];
        if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
            ((SRMemberViewCell *) cell).lblDating.hidden = NO;
            ((SRMemberViewCell *) cell).imgDating.hidden = NO;
            ((SRMemberViewCell *) cell).imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
        } else {
            ((SRMemberViewCell *) cell).imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
            ((SRMemberViewCell *) cell).lblDating.hidden = YES;
            ((SRMemberViewCell *) cell).imgDating.hidden = YES;
        }
    } else {
        ((SRMemberViewCell *) cell).imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
        ((SRMemberViewCell *) cell).lblDating.hidden = YES;
        ((SRMemberViewCell *) cell).imgDating.hidden = YES;
    }

    // Adjust green dot label appearence
    ((SRMemberViewCell *) cell).lblOnline.layer.cornerRadius = ((SRMemberViewCell *) cell).lblOnline.frame.size.width / 2;
    ((SRMemberViewCell *) cell).lblOnline.layer.masksToBounds = YES;
    ((SRMemberViewCell *) cell).lblOnline.clipsToBounds = YES;


    if ([userDict[kKeyStatus] isEqualToString:@"1"]) {

        ((SRMemberViewCell *) cell).lblOnline.hidden = NO;
    } else {
        ((SRMemberViewCell *) cell).lblOnline.hidden = YES;
    }


    if (userDict[kKeySelectedObj] != nil) {
        ((SRMemberViewCell *) cell).imgCheck.image = [UIImage imageNamed:@"cheked_yellow.png"];
    } else
        ((SRMemberViewCell *) cell).imgCheck.image = [UIImage imageNamed:@"uncheked_yellow.png"];


    // Cell Style
    cell.selectionStyle = UITableViewCellEditingStyleNone;

    // Return
    return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSMutableDictionary *userDict = membersArray[indexPath.section];
    SRMemberViewCell *cell = (SRMemberViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];

    if (userDict[kKeySelectedObj] != nil) {
        int index = 0;
        Boolean found = NO;
        NSArray *arrAllId = [(APP_DELEGATE).selectedGroupId componentsSeparatedByString:@","];
        for (int i = 0; i < [arrAllId count]; i++) {
            if ([arrAllId[i] isEqualToString:userDict[kKeyId]]) {
                found = YES;
                index = i;
                break;
            }
        }
        if (found) {
            NSMutableArray *arrSelectedID = [arrAllId mutableCopy];
            [arrSelectedID removeObjectAtIndex:index];

            (APP_DELEGATE).selectedGroupId = [arrSelectedID componentsJoinedByString:@","];
         //   NSLog(@"%@", (APP_DELEGATE).selectedGroupId);
        }

        [userDict removeObjectForKey:kKeySelectedObj];
        cell.imgCheck.image = [UIImage imageNamed:@"uncheked_yellow.png"];
    } else {
        NSString *str = [[NSString alloc] init];
        str = [userDict[kKeyId] stringByAppendingString:@","];
        if ([(APP_DELEGATE).selectedGroupId isEqualToString:@""]) {
            (APP_DELEGATE).selectedGroupId = str;
        } else {
            (APP_DELEGATE).selectedGroupId = [(APP_DELEGATE).selectedGroupId stringByAppendingString:str];
        }
        NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionaryWithDictionary:userDict];
        mutatedDict[kKeySelectedObj] = @"selected";
        cell.imgCheck.image = [UIImage imageNamed:@"cheked_yellow.png"];
        // Update array
        membersArray[indexPath.section] = mutatedDict;
    }
}


// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat headerHeight = 1.0;
    if (section == 0) {
        headerHeight = 0.0;
    }

    // Return
    return headerHeight;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }

    // Return
    return headerView;
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getConnectionSucceed:

- (void)getConnectionSucceed:(NSNotification *)inNotify {
    NSArray *connectionList = [inNotify object];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    connectionList = [connectionList sortedArrayUsingDescriptors:sortDescriptors];

    [membersArray addObjectsFromArray:connectionList];

    // Prepare the array
    for (NSDictionary *addedDict in addedMembers) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, addedDict[kKeyId]];
        NSArray *filteredObj = [membersArray filteredArrayUsingPredicate:predicate];

        if ([filteredObj count] > 0) {
            NSDictionary *dict = filteredObj[0];
            NSInteger index = [membersArray indexOfObject:dict];

            // Relace the object with this object
            membersArray[index] = addedDict;
        }
    }

    //Check user age is between 13 to 18
    for (int i = 0; i < membersArray.count; i++) {
        NSDictionary *dictionary = membersArray[i];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateOfBirth;
        if (![dictionary[@"dob"] isKindOfClass:[NSNull class]]) {
            dateOfBirth = [dateFormatter dateFromString:dictionary[@"dob"]];
        } else
            dateOfBirth = [dateFormatter dateFromString:@"1970-01-01"];

        NSDate *now = [NSDate date];
        NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                components:NSCalendarUnitYear
                  fromDate:dateOfBirth
                    toDate:now
                   options:0];
        NSInteger age = [ageComponents year];

//        if (age >= 13 && age <= 18) {
//
//            [membersArray removeObjectAtIndex:i];
//        }

    }



    // Reload table
    [self.tableView reloadData];
}

// --------------------------------------------------------------------------------
// getConnectionFailed:

- (void)getConnectionFailed:(NSNotification *)inNotify {
}
// --------------------------------------------------------------------------------
// ShowGroupSucceed:

- (void)ShowGroupSucceed:(NSNotification *)inNotify {

    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;


    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *dictionary = [inNotify object];
    if ([dictionary isKindOfClass:[NSDictionary class]] && dictionary[kKeyGroupList] != nil) {
        NSArray *groupListArr = dictionary[kKeyGroupList];

        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        groupListArr = [groupListArr sortedArrayUsingDescriptors:sortDescriptors];

        groupUsers = dictionary[kKeyGroupUsers];
        server.groupUsersArr = groupUsers;
        if (![membersArray isEqual:groupListArr]) {
            [membersArray addObjectsFromArray:groupListArr];
        }


        // Reload table
        [self.tableView reloadData];
    }
}

// --------------------------------------------------------------------------------
// ShowGroupFailed:

- (void)ShowGroupFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

@end
