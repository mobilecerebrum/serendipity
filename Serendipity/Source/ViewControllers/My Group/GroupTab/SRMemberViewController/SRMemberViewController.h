#import "SRProfileImageView.h"
#import <UIKit/UIKit.h>

#pragma mark
#pragma mark Protocol
#pragma mark

// Custom Delegate to pass Location Data to SREditViewController
@protocol SRMembersViewControllerDelegate <NSObject>
- (void)addedMembersList:(NSArray *)inArr;
- (void)addedMembersFromGroups:(NSMutableArray *)inArr;
@end

@interface SRMemberViewController : ParentViewController
{
	// Instance variable
	NSArray *addedMembers;
	NSMutableArray *cacheImgArr;
	NSMutableArray *membersArray,*groupUsers;
    NSString *addFrom;
	// Server
	SRServerConnection *server;
}

// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *profileImg;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
         addedMembers:(NSArray *)inArr;

@end
