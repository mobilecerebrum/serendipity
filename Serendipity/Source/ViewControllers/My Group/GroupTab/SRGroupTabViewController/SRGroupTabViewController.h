#import <UIKit/UIKit.h>
#import "SRGroupsListController.h"
#import "SRNotificationView.h"

@interface SRGroupTabViewController : UITabBarController
{
	// Instance Variable
	SRNotificationView *notificationView;
	NSArray *notificationArr;
    
    // Controller
    SRGroupsListController *objGroupList;
}

@property (weak, nonatomic) IBOutlet UITextField *txtSearchField;
@property (strong, nonatomic) IBOutlet UITabBarController *tabBarController;
#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil;

@end
