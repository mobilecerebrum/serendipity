#import "SRMemberViewController.h"
#import "SRNewGroupViewController.h"
#import "SRGroupDetailViewController.h"
#import "SRModalClass.h"
#import "CommonFunction.h"

@implementation SRGroupDetailViewController

#pragma mark Private Method

//------------------------------------------------------------------------------------------------------------------------
// displayGroupInfo

- (void)displayGroupInfo {
    // Group image
    if (groupDataDict[kKeyImageObject] != nil) {
        self.imgBackProfileImg.image = groupDataDict[kKeyImageObject];
        self.imgProfilePic.image = groupDataDict[kKeyImageObject];
    } else {
        if ([groupDataDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            // Get result

            if ([groupDataDict[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [groupDataDict[kKeyMediaImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imgBGUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];

                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [self.imgBackProfileImg sd_setImageWithURL:[NSURL URLWithString:imgBGUrl]];

                    [self.imgProfilePic sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else {
                    self.imgBackProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    self.imgProfilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
            }

        } else {
            self.imgBackProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            self.imgProfilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        }
    }

    // Group name
    self.lblGrpName.text = groupDataDict[kKeyName];

    // Group members count
    self.lblGrpMembers.text = [NSString stringWithFormat:@"%lu members", [groupDataDict[kKeyGroupMembers] count]];

    // Group address
    self.lblGrpAddress.text = groupDataDict[kKeyAddress];

    // Group descriptions
    if ([groupDataDict[kKeyDescription] isKindOfClass:[NSString class]] && [groupDataDict[kKeyDescription] length] == 0) {
        self.lblGroupDesc.text = @"No group description";
    } else if ([groupDataDict[kKeyDescription] isKindOfClass:[NSNull class]])
        self.lblGroupDesc.text = @"No group description";
    else {
        self.lblGroupDesc.text = groupDataDict[kKeyDescription];
        [self.lblGroupDesc sizeToFit];

        // Adjust the frame of other view
        CGRect frame = self.containerView.frame;
        frame.origin.y = self.lblGroupDesc.frame.origin.y + self.lblGroupDesc.frame.size.height + 4;
        self.containerView.frame = frame;
    }

    // Group Members
    // Add members to scroll view
//	CGRect latestFrame = CGRectZero;
    NSMutableArray *groupMemberArr = [[NSMutableArray alloc] initWithArray:groupDataDict[kKeyGroupMembers]];

    // Make admins array to show further
    NSMutableArray *adminArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < groupMemberArr.count; i++) {
        if ([[groupMemberArr[i] valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
            [adminArray addObject:[groupMemberArr[i] valueForKey:kKeyUserID]];
        }
    }


    // Get group members information in array (Array created here because we wenat show alphabetically sorted users)
    NSMutableArray *membersArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < groupMemberArr.count; i++) {
        NSDictionary *userDetails;
        if ([groupMemberArr[i] objectForKey:kKeyFirstName] != nil) {
            userDetails = groupMemberArr[i];
        }
//        else if ([[groupMemberArr objectAtIndex:i] objectForKey:kKeyUser] != nil) {
//            userDetails = [[groupMemberArr objectAtIndex:i] objectForKey:kKeyUser];
//        }
        else {
            NSString *userId = [groupMemberArr[i] objectForKey:kKeyUserID];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
            NSArray *filterArr = [server.groupUsersArr filteredArrayUsingPredicate:predicate];
            if ([filterArr count] > 0) {
                userDetails = filterArr[0];
                [userDetails setValue:[groupMemberArr[i] objectForKey:kKeyInvitationStatus] forKey:kKeyInvitationStatus];
                [userDetails setValue:[groupMemberArr[i] objectForKey:kKeyIsAdmin] forKey:kKeyIsAdmin];
            }
        }

        if ([userDetails isKindOfClass:[NSDictionary class]] && userDetails != nil) {
            [membersArr addObject:userDetails];
        }
    }

    // Get sorted data in member array
    membersArr = [SRModalClass sortMembersArray:membersArr];
    CGRect latestFrame = CGRectZero;
    CGRect latestFrame1 = CGRectZero;
    CGRect latestFrame2 = CGRectZero;
    UIView *subView;

    // Show users in scrollview
    for (NSDictionary *userDict in membersArr) {
        // Add view and
        if ([userDict[kKeyInvitationStatus] isEqualToString:@"0"]) {
            // Add view and
            if (CGRectIsEmpty(latestFrame1)) {
                latestFrame1 = CGRectMake(0, 0, 70, self.scrollviewNotConfirmed.frame.size.height);
            } else
                latestFrame1 = CGRectMake(latestFrame1.origin.x + 74, 0, 70, self.scrollviewNotConfirmed.frame.size.height);
            subView = [[UIView alloc] initWithFrame:latestFrame1];
        } else if ([userDict[kKeyInvitationStatus] isEqualToString:@"1"]) {
            // Add to scroll view
            if (CGRectIsEmpty(latestFrame)) {
                latestFrame = CGRectMake(0, 0, 70, self.scrollviewGoingWith.frame.size.height);
            } else
                latestFrame = CGRectMake(latestFrame.origin.x + 74, 0, 70, self.scrollviewGoingWith.frame.size.height);
            subView = [[UIView alloc] initWithFrame:latestFrame];
        } else {
            // Add view and
            if (CGRectIsEmpty(latestFrame2)) {
                latestFrame2 = CGRectMake(0, 0, 70, self.scrollviewUnableToAttend.frame.size.height);
            } else
                latestFrame2 = CGRectMake(latestFrame2.origin.x + 74, 0, 70, self.scrollviewUnableToAttend.frame.size.height);
            subView = [[UIView alloc] initWithFrame:latestFrame2];
        }
        // Add views

        subView.backgroundColor = [UIColor clearColor];

        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 2, 50, 50)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.layer.cornerRadius = imgView.frame.size.width / 2.0;
        imgView.layer.masksToBounds = YES;
        imgView.image = [UIImage imageNamed:@"profile_menu.png"];

        // Apply image

        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {

            NSDictionary *profileDict = userDict[kKeyProfileImage];
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                imgView.image = [UIImage imageNamed:@"profile_menu.png"];
        } else
            imgView.image = [UIImage imageNamed:@"profile_menu.png"];

        // Name
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 52, 70, 18)];
        if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]] && [userDict[kKeyDegree] integerValue] == 1) {
            lblName.textColor = [UIColor whiteColor];
        } else
            lblName.textColor = [UIColor orangeColor];

        lblName.backgroundColor = [UIColor clearColor];
        lblName.textAlignment = NSTextAlignmentCenter;
        lblName.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
        if ([userDict[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
            lblName.text = @"Me";
        } else {

            lblName.text = [NSString stringWithFormat:@"%@ %@.", userDict[kKeyFirstName], [userDict[kKeyLastName] length] > 0 ? [userDict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)] : @""];
        }

        // Label if admin
        UILabel *lblAdmin = [[UILabel alloc] initWithFrame:CGRectMake(0, 51 + 18, 70, 18)];
        if ([userDict[kKeyIsAdmin] isEqualToString:@"1"]) {
            lblAdmin.textColor = [UIColor whiteColor];
            lblAdmin.backgroundColor = [UIColor clearColor];
            lblAdmin.textAlignment = NSTextAlignmentCenter;
            lblAdmin.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            lblAdmin.text = @"(Admin)";

        }
        // Add views
        [subView addSubview:imgView];
        [subView addSubview:lblName];
        [subView addSubview:lblAdmin];


        if ([userDict[kKeyInvitationStatus] isEqualToString:@"0"]) {
            // Add to scroll view
            [self.scrollviewNotConfirmed addSubview:subView];
        } else if ([userDict[kKeyInvitationStatus] isEqualToString:@"1"]) {
            // Add to scroll view
            [self.scrollviewGoingWith addSubview:subView];
        } else {
            // Add to scroll view
            [self.scrollviewUnableToAttend addSubview:subView];
        }

        if ((latestFrame.origin.x + 100) > SCREEN_WIDTH) {
            self.scrollviewGoingWith.contentSize = CGSizeMake(latestFrame.origin.x + 100, 0);
            self.scrollviewGoingWith.scrollEnabled = YES;
            self.scrollviewGoingWith.backgroundColor = [UIColor clearColor];
        }

        if ((latestFrame1.origin.x + 100) > SCREEN_WIDTH) {
            self.scrollviewNotConfirmed.contentSize = CGSizeMake(latestFrame1.origin.x + 100, 0);
            self.scrollviewNotConfirmed.scrollEnabled = YES;
            self.scrollviewNotConfirmed.backgroundColor = [UIColor clearColor];
        }

        if ((latestFrame2.origin.x + 100) > SCREEN_WIDTH) {
            self.scrollviewUnableToAttend.contentSize = CGSizeMake(latestFrame2.origin.x + 100, 0);
            self.scrollviewUnableToAttend.scrollEnabled = YES;
            self.scrollviewUnableToAttend.backgroundColor = [UIColor clearColor];
        }
    }

    // Update Empty scrollView height
    NSUInteger subViewCount = self.scrollviewGoingWith.subviews.count;
    CGRect rect;
    if (subViewCount < 1) {
        subViewCount = 0;
        rect = self.scrollviewGoingWith.frame;
        rect.size.height = 10;
        self.scrollviewGoingWith.frame = rect;
        self.lblGoingWith.text = [NSString stringWithFormat:@"%@ (%lu members)", @"You are going with:", (unsigned long) subViewCount];
    } else {
        rect = self.scrollviewGoingWith.frame;
        rect.size.height = 90;
        self.scrollviewGoingWith.frame = rect;
        self.lblGoingWith.text = [NSString stringWithFormat:@"%@ (%lu members)", @"You are going with:", (unsigned long) subViewCount];
    }
    
    NSArray *memberArr = [[NSArray alloc] initWithArray:groupDataDict[kKeyGroupMembers]];
    NSDictionary *memberDict;
    for (int i = 0; i < memberArr.count; i++) {
        if ([[memberArr[i] objectForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
            memberDict = memberArr[i];
        }
    }
    if ([[memberDict valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
    } else if ([[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"]) {
    } else if ([[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
    } else {
        self.lblGoingWith.text = [NSString stringWithFormat:@"%@", @"Members of group are:"];
    }
    
    subViewCount = self.scrollviewNotConfirmed.subviews.count;
    if (subViewCount < 1) {
        subViewCount = 0;

        rect = self.lblNotConfirmed.frame;
        rect.origin.y = self.scrollviewGoingWith.frame.origin.y + self.scrollviewGoingWith.frame.size.height + 10;
        self.lblNotConfirmed.frame = rect;

        rect = self.scrollviewNotConfirmed.frame;
        rect.size.height = 10;
        rect.origin.y = self.lblNotConfirmed.frame.origin.y + 20;
        self.scrollviewNotConfirmed.frame = rect;
        if (SCREEN_WIDTH <= 320) {
            [self.lblNotConfirmed setFont:[UIFont systemFontOfSize:11.0]];
        }
        self.lblNotConfirmed.text = [NSString stringWithFormat:@"%@ (%lu members)", @"Invited members who have not confirmed:", (unsigned long) subViewCount];
    } else {
        rect = self.lblNotConfirmed.frame;
        rect.origin.y = self.scrollviewGoingWith.frame.origin.y + self.scrollviewGoingWith.frame.size.height + 10;
        self.lblNotConfirmed.frame = rect;

        rect = self.scrollviewNotConfirmed.frame;
        rect.size.height = 90;
        rect.origin.y = self.lblNotConfirmed.frame.origin.y + 20;
        self.scrollviewNotConfirmed.frame = rect;
        if (SCREEN_WIDTH <= 320) {
            [self.lblNotConfirmed setFont:[UIFont systemFontOfSize:11.0]];
        }
        self.lblNotConfirmed.text = [NSString stringWithFormat:@"%@ (%lu members)", @"Invited members who have not confirmed:", (unsigned long) subViewCount];
    }
    subViewCount = self.scrollviewUnableToAttend.subviews.count;
    if (subViewCount < 1) {
        subViewCount = 0;

        rect = self.lblUnableToAttend.frame;
        rect.origin.y = self.scrollviewNotConfirmed.frame.origin.y + self.scrollviewNotConfirmed.frame.size.height + 10;
        self.lblUnableToAttend.frame = rect;

        rect = self.scrollviewUnableToAttend.frame;
        rect.size.height = 10;
        rect.origin.y = self.lblUnableToAttend.frame.origin.y + 20;
        self.scrollviewUnableToAttend.frame = rect;
        self.lblUnableToAttend.text = [NSString stringWithFormat:@"%@ (%lu members)", @"Members unable to attend:", (unsigned long) subViewCount];
    } else {
        rect = self.lblUnableToAttend.frame;
        rect.origin.y = self.scrollviewNotConfirmed.frame.origin.y + self.scrollviewNotConfirmed.frame.size.height + 10;
        self.lblUnableToAttend.frame = rect;

        rect = self.scrollviewNotConfirmed.frame;
        rect.size.height = 90;
        rect.origin.y = self.lblUnableToAttend.frame.origin.y + 20;
        self.scrollviewUnableToAttend.frame = rect;
        self.lblUnableToAttend.text = [NSString stringWithFormat:@"%@ (%lu members)", @"Members unable to attend:", (unsigned long) subViewCount];
    }


    rect = self.lblGrp_type.frame;
    rect.origin.y = self.scrollviewUnableToAttend.frame.origin.y + self.scrollviewUnableToAttend.frame.size.height + 15;
    self.lblGrp_type.frame = rect;
    rect = self.lblGrp_type_Details.frame;
    rect.origin.y = self.scrollviewUnableToAttend.frame.origin.y + self.scrollviewUnableToAttend.frame.size.height + 15;
    self.lblGrp_type_Details.frame = rect;

    rect = self.lblGrp_is.frame;
    rect.origin.y = self.lblGrp_type_Details.frame.origin.y + self.lblGrp_type_Details.frame.size.height + 15;
    self.lblGrp_is.frame = rect;
    rect = self.lblGrp_is_Details.frame;
    rect.origin.y = self.lblGrp_type_Details.frame.origin.y + self.lblGrp_type_Details.frame.size.height + 15;
    self.lblGrp_is_Details.frame = rect;


    // Define the group type
    if ([groupDataDict[kKeyGroupType] isEqualToString:@"1"])
        self.lblGrp_type_Details.text = @"Public(anyone can add)";
    else
        self.lblGrp_type_Details.text = @"Private";

    // Define group on/off
    if ([groupDataDict[kKeyGroup_is] isEqualToString:@"1"])
        self.lblGrp_is_Details.text = @"On";




    // Set scroll view content size
//    if (!(SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736)) {

    self.scrollViewProfileDetails.contentSize = CGSizeMake(0, self.containerView.frame.origin.y + self.containerView.frame.size.height + 10);
    self.scrollViewProfileDetails.scrollEnabled = YES;
    self.scrollViewProfileDetails.backgroundColor = [UIColor clearColor];
//	}
}

#pragma mark Init Method

//------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
            groupInfo:(NSDictionary *)groupDict
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SRGroupDetailViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        groupDataDict = groupDict;
        cacheImgArr = [[NSMutableArray alloc] init];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupSucceed:)
                              name:kKeyNotificationDeleteGroupMemberSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupFailed:)
                              name:kKeyNotificationDeleteGroupMemberFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupSucceed:)
                              name:kKeyNotificationAcceptRejectGroupMemberSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectGroupFailed:)
                              name:kKeyNotificationAcceptRejectGroupMemberFailed object:nil];

    }

    //return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Init Method

//------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Do some customization
//	self.scrollviewGrpMembers.scrollEnabled = NO;

    // Adjust the width for buttons
    CGFloat width = SCREEN_WIDTH / 2 - 1;
        
    NSInteger constValue = 150;
    
    if ([self getBottomSafeArea] > 0.0) {
        constValue = 173;
    }
    CGFloat y = SCREEN_HEIGHT - (constValue + [self getBottomSafeArea]);
    btnNewGroup = [[UIButton alloc] initWithFrame:CGRectMake(0, y, width, 35)];
    [btnNewGroup setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
    [btnNewGroup setTitle:@"New group" forState:UIControlStateNormal];
    [btnNewGroup setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [btnNewGroup addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    btnNewGroup.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:15.0];
    btnNewGroup.titleLabel.textAlignment = NSTextAlignmentCenter;

    btnLeaveGroup = [[UIButton alloc] initWithFrame:CGRectMake(width + 1, y, width, 35)];
    [btnLeaveGroup setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
    if ([groupDataDict[kKeyGroupMembers] isKindOfClass:[NSArray class]] && groupDataDict[kKeyGroupMembers] != nil) {
        NSArray *membersArr = [[NSArray alloc] initWithArray:groupDataDict[kKeyGroupMembers]];
        NSDictionary *memberDict;
        for (int i = 0; i < membersArr.count; i++) {
            if ([[membersArr[i] objectForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                memberDict = membersArr[i];

            }

        }
        if ([[memberDict valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
            if ([[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
                [btnLeaveGroup setTitle:@"Accept Invitation" forState:UIControlStateNormal];
            } else
                [btnLeaveGroup setTitle:@"Delete group" forState:UIControlStateNormal];
        } else if ([[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"]) {
            [btnLeaveGroup setTitle:@"Leave group" forState:UIControlStateNormal];
        } else if ([[memberDict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
            [btnLeaveGroup setTitle:@"Accept Invitation" forState:UIControlStateNormal];
        } else {
            [btnLeaveGroup setTitle:@"Join group" forState:UIControlStateNormal];
        }
    }
    [btnLeaveGroup setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [btnLeaveGroup addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    btnLeaveGroup.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:15.0];
    btnLeaveGroup.titleLabel.textAlignment = NSTextAlignmentCenter;

    // Add to scroll view
    [self.view addSubview:btnNewGroup];
    [self.view addSubview:btnLeaveGroup];

    // Display the group info
    [self displayGroupInfo];
}

//------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

#pragma mark IBAction Method

// --------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnButtonClick:(id)sender {
    if (sender == btnNewGroup) {
        // Create group
        SRNewGroupViewController *objNewGroupView = [[SRNewGroupViewController alloc] initWithNibName:nil bundle:nil dict:nil server:(APP_DELEGATE).server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objNewGroupView animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else {
        NSArray *membersArr = [[NSArray alloc] initWithArray:groupDataDict[kKeyGroupMembers]];

        UIButton *resultButton = (UIButton *) sender;
        if ([resultButton.currentTitle isEqualToString:@"Delete group"]) {
            // Delete group
            NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassCreateGroup, groupDataDict[kKeyId]];
            [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
        } else if ([resultButton.currentTitle isEqualToString:@"Leave group"]) {
            // Leave the group
            for (int i = 0; i < membersArr.count; i++) {
                if ([[membersArr[i] objectForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteGroupMember, [membersArr[i] objectForKey:kKeyId]];
                    NSDictionary *paramDict = @{kKeyGroupID: groupDataDict[kKeyId], kKeyUserID: [server.loggedInUserInfo valueForKey:kKeyId], kKeyStatus: @"2"};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kDELETE];
                }
            }
        } else if ([resultButton.currentTitle isEqualToString:@"Accept Invitation"]) {
            // Accept the group

            for (int i = 0; i < membersArr.count; i++) {
                if ([[membersArr[i] objectForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteGroupMember, [membersArr[i] objectForKey:kKeyId]];
                    NSString *imageName = @"";
                    if ([groupDataDict[@"image"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dictImage = groupDataDict[@"image"];
                        imageName = dictImage[@"file"];
                    }
                    dictPram_CometChat = @{kKeyGroupID: groupDataDict[@"chat_room_id"], @"groupName": groupDataDict[@"name"], @"groupPwd": @"", @"grouppic": imageName};
                    NSDictionary *paramDict = @{kKeyGroupID: groupDataDict[kKeyId], kKeyUserID: [server.loggedInUserInfo valueForKey:kKeyId], kKeyStatus: @"1"};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                }
            }
        } else if ([resultButton.currentTitle isEqualToString:@"Join group"]) {
            // Join the group
            if ([groupDataDict[kKeyGroupMembers] isKindOfClass:[NSArray class]] && groupDataDict[kKeyGroupMembers] != nil) {
                NSMutableArray *memberIdsArr = [[NSMutableArray alloc] init];
                for (int i = 0; i < membersArr.count; i++) {
                    [memberIdsArr addObject:[membersArr[i] objectForKey:kKeyUserID]];
                    if ([[membersArr[i] objectForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                        // If you have already sent request to join group
                        if ([[membersArr[i] objectForKey:kKeyInvitationStatus] isEqualToString:@"3"]) {
                            [SRModalClass showAlert:@"Your have already requested to join this group and your initial request is still pending acceptance."];

                        }
                    }
                }

                // If member array not contains login user then call API to join group
                if (![memberIdsArr containsObject:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                    [btnLeaveGroup setEnabled:FALSE];
                    [APP_DELEGATE showActivityIndicator];
                    NSString *imageName = @"";
                    if ([groupDataDict[@"image"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dictImage = groupDataDict[@"image"];
                        imageName = dictImage[@"file"];
                    }
                    dictPram_CometChat = @{kKeyGroupID: groupDataDict[@"chat_room_id"], @"groupName": groupDataDict[@"name"], @"groupPwd": @"", @"grouppic": imageName};
                    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassDeleteGroupMember];
                    NSDictionary *paramDict = @{kKeyGroupID: groupDataDict[kKeyId], kKeyUserID: server.loggedInUserInfo[kKeyId]};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPOST];
                }
            }
        }
    }
}

#pragma mark Notification Method

// --------------------------------------------------------------------------------
// ShowGroupSucceed:

- (void)ShowGroupSucceed:(NSNotification *)inNotify {
    // Display the dots
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *dictionary = [inNotify object];
    if ([dictionary isKindOfClass:[NSDictionary class]] && dictionary[kKeyGroupList] == nil) {
        server.groupDetailInfo = dictionary;
        groupDataDict = dictionary;

        // Update scroll view
        for (UIView *subview in[self.scrollviewGoingWith subviews]) {
            if ([subview isKindOfClass:[UIView class]]) {
                [subview removeFromSuperview];
            }
        }
        for (UIView *subview in[self.scrollviewNotConfirmed subviews]) {
            if ([subview isKindOfClass:[UIView class]]) {
                [subview removeFromSuperview];
            }
        }
        for (UIView *subview in[self.scrollviewUnableToAttend subviews]) {
            if ([subview isKindOfClass:[UIView class]]) {
                [subview removeFromSuperview];
            }
        }

        [self displayGroupInfo];
    }
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;

}

// --------------------------------------------------------------------------------
// AcceptRejectGroupSucceed:
- (void)AcceptRejectGroupSucceed:(NSNotification *)inNotify {
    // Post notification for call new events from server
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewGroupAdded object:nil];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"You have successfully joined the group" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:true completion:nil];
}

// --------------------------------------------------------------------------------
// AcceptRejectGroupFailed:
- (void)AcceptRejectGroupFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:[inNotify object]];
}

@end
