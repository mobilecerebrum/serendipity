#import <UIKit/UIKit.h>

@interface SRGroupDetailViewController : ParentViewController
{
	// Server Connection
	SRServerConnection *server;

	// Instance Variables
	NSDictionary *groupDataDict;
	NSMutableArray *cacheImgArr;
    NSDictionary *dictPram_CometChat;

    UIButton *btnNewGroup;
    UIButton *btnLeaveGroup;
}

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *imgBackProfileImg;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackColor;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewProfileDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblGrpName;
@property (weak, nonatomic) IBOutlet UILabel *lblGrpMembers;
@property (weak, nonatomic) IBOutlet UILabel *lblGrpAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupDesc;

@property (weak, nonatomic) IBOutlet UIView *containerView;

//@property (weak, nonatomic) IBOutlet UILabel *lblGrpMembersCount;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewGrpMembers;

@property (weak, nonatomic) IBOutlet UILabel *lblGoingWith,*lblNotConfirmed,*lblUnableToAttend;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewGoingWith,*scrollviewNotConfirmed,*scrollviewUnableToAttend;

@property (weak, nonatomic) IBOutlet UILabel *lblGrp_is;
@property (weak, nonatomic) IBOutlet UILabel *lblGrp_is_Details;
@property (weak, nonatomic) IBOutlet UILabel *lblGrp_type;
@property (weak, nonatomic) IBOutlet UILabel *lblGrp_type_Details;
//@property (weak, nonatomic) IBOutlet UIButton *btnCreateEvent;
//@property (weak, nonatomic) IBOutlet UIButton *btnLeaveGroup;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
            groupInfo:(NSDictionary *)groupDict
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)actionOnButtonClick:(id)sender;

@end
