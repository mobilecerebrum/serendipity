
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GHContextMenuActionType){
    // Default
    GHContextMenuActionTypePan,
    // Allows tap action in order to trigger an action
    GHContextMenuActionTypeTap
};

@protocol GHContextOverlayViewDataSource;
@protocol GHContextOverlayViewDelegate;

@interface GHContextMenuView : UIView<CAAnimationDelegate>

@property (nonatomic, assign) id<GHContextOverlayViewDataSource> dataSource;
@property (nonatomic, assign) id<GHContextOverlayViewDelegate> delegate;
@property (nonatomic, assign) NSInteger selectedIndex;

//For userDegree view
@property (nonatomic)   BOOL        isDegreeView;
@property (nonatomic)   NSUInteger  userDrgree;

@property (nonatomic, assign) GHContextMenuActionType menuActionType;

//- (void) longPressDetected:(UIGestureRecognizer*) gestureRecognizer;
- (void) tapBtnDetected:(UIGestureRecognizer*) gestureRecognizer;

-(void)setUpDegreeView;     //For DegreeSetup  View
-(void)setUpViewRadius:(NSUInteger)viewRadius;  //For setup view radius to show big circle
@end

@protocol GHContextOverlayViewDataSource <NSObject>

@required
- (NSInteger) numberOfMenuItems;
- (UIImage*) imageForItemAtIndex:(NSInteger) index;


@optional
-(BOOL) shouldShowMenuAtPoint:(CGPoint) point;
- (UILabel*) labelForItemAtIndex:(NSInteger) index ;//For user degree
@end

@protocol GHContextOverlayViewDelegate <NSObject>

- (void) didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint) point;

@end
