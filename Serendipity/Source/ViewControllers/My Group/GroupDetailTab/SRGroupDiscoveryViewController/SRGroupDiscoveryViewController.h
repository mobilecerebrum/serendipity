#import <UIKit/UIKit.h>
#import "GHContextMenuView.h"
#import "SRServerConnection.h"
#import "SRGroupUserRadarListViewController.h"
#import "SRGroupUserListView.h"
#import "SRGroupUserMapView.h"

@interface SRGroupDiscoveryViewController : ParentViewController <CLLocationManagerDelegate,GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>
{
	// Instance Variable
    NSMutableArray *groupUsers;
    
    SRGroupUserRadarListViewController *radarCon;
    SRGroupUserListView *listView;
    SRGroupUserMapView *mapView;
    
    // Flag
    BOOL isOnce;
    
    // Server
    SRServerConnection *server;
}

// Properties
@property (nonatomic, strong) IBOutlet UIButton *btnAnimate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)btnAnimateClick:(id)sender;

@end
