#import "SRGroupTabViewController.h"
#import "SRModalClass.h"
#import "SRGroupDiscoveryViewController.h"

@implementation SRGroupDiscoveryViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
	//Call Super
	self = [super initWithNibName:@"SRGroupDiscoveryViewController" bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		server = inServer;
		groupUsers = [[NSMutableArray alloc]init];
	}

	//return
	return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
	// Dealloc all register notifications
	[[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark
#pragma mark Standard Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
	// Call Super
	[super viewDidLoad];

	// Set Up For Animation Button on RadarDiscovery
	GHContextMenuView *overlay = [[GHContextMenuView alloc] init];
	overlay.dataSource = self;
	overlay.delegate = self;
	overlay.selectedIndex = 0;

	UITapGestureRecognizer *_longPressRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:overlay action:@selector(tapBtnDetected:)];
	[self.btnAnimate addGestureRecognizer:_longPressRecognizer];

	//
}

// -----------------------------------------------------------------------------------------------
// viewDidAppear

- (void)viewDidAppear:(BOOL)animated {
	// Call super
	[super viewDidAppear:NO];

	if (!isOnce) {
		isOnce = YES;

		// Views
		radarCon = [[SRGroupUserRadarListViewController alloc]initWithNibName:nil bundle:nil server:server inUserArr:groupUsers];
		radarCon.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
		radarCon.delegate = self;
		[self.view addSubview:radarCon.view];
		[self.view bringSubviewToFront:self.btnAnimate];

		// Filter the users and alloc the views
		for (NSDictionary *userDict in[server.groupDetailInfo objectForKey:kKeyGroupMembers]) {
			NSDictionary *userDetails;
			if ([userDict objectForKey:kKeyFirstName] != nil) {
				userDetails = userDict;
			}
			else if ([userDict objectForKey:kKeyUser] != nil) {
				userDetails = [userDict objectForKey:kKeyUser];
			}
			else {
				NSString *userId = [userDict objectForKey:kKeyUserID];
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
				NSArray *filterArr = [server.groupUsersArr filteredArrayUsingPredicate:predicate];
				if ([filterArr count] > 0) {
					userDetails = [filterArr objectAtIndex:0];
                }
			}

			// Add dict to mutable array
           if ([userDetails isKindOfClass:[NSDictionary class]] && userDetails != nil) {
                [groupUsers addObject: userDetails];
            }
        }

		// Sorting
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
		NSArray *sortedArr = [groupUsers sortedArrayUsingDescriptors:sortDescriptors];

		[groupUsers removeAllObjects];
		[groupUsers addObjectsFromArray:sortedArr];

		// Load radar users
		[radarCon loadUsersInRadar:groupUsers];

		listView = [[SRGroupUserListView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        listView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
		[self.view addSubview:listView];
		listView.server = server;
		listView.delegate = self;
		[listView loadUsers:groupUsers];
		listView.hidden = YES;

		mapView = [[SRGroupUserMapView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
		[self.view addSubview:mapView];
		mapView.server = server;
		mapView.delegate = self;
		[mapView loadUsers:groupUsers];
		mapView.hidden = YES;
	}
}

#pragma mark
#pragma mark IBAction Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// btnAnimateClick:

- (IBAction)btnAnimateClick:(id)sender {
	[self.btnAnimate setBackgroundColor:[UIColor clearColor]];
	[self.btnAnimate setImage:[UIImage imageNamed:@"close-black.png"] forState:UIControlStateNormal];
}

#pragma mark
#pragma mark GHContextMenu Delegate Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// numberOfMenuItems

- (NSInteger)numberOfMenuItems {
	return 3;
}

//------------------------------------------------------------------------------------------------------------------------
// imageForItemAtIndex:

- (UIImage *)imageForItemAtIndex:(NSInteger)index {
	NSString *imageName = nil;
	switch (index) {
		case 0:
			imageName = @"toogle-radar-white-50px.png";
			break;

		case 1:
			imageName = @"toogle-map-white.png";
			break;

		case 2:
			imageName = @"toogle-list-white-50px.png";
			break;

		default:
			break;
	}

	// Image name
	return [UIImage imageNamed:imageName];
}

//------------------------------------------------------------------------------------------------------------------------
// didSelectItemAtIndex:

- (void)didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point {
	switch (selectedIndex) {
		case 0:
		{
			[self.btnAnimate setImage:[UIImage imageNamed:@"toogle-radar-black-50px.png"] forState:UIControlStateNormal];
			listView.hidden = YES;
			mapView.hidden = YES;
			radarCon.view.hidden = NO;
			[self.view bringSubviewToFront:self.btnAnimate];

			break;
		}

		case 1:
		{
			listView.hidden = YES;
			mapView.hidden = NO;
			radarCon.view.hidden = YES;

			[self.btnAnimate.layer setShadowOffset:CGSizeMake(5, 5)];
			[self.btnAnimate.layer setShadowColor:[[UIColor blackColor] CGColor]];
			[self.btnAnimate.layer setShadowOpacity:8.5];
			[self.btnAnimate setImage:[UIImage imageNamed:@"toogle-map-black.png"] forState:UIControlStateNormal];
			[self.view bringSubviewToFront:self.btnAnimate];
			break;
		}

		case 2:
		{
			// Remove views
			listView.hidden = NO;
			mapView.hidden = YES;
			radarCon.view.hidden = YES;
			[self.btnAnimate setImage:[UIImage imageNamed:@"toogle-list-black-50px.png"] forState:UIControlStateNormal];
			[self.view bringSubviewToFront:self.btnAnimate];
			break;
		}

		default:
			break;
	}
}

@end
