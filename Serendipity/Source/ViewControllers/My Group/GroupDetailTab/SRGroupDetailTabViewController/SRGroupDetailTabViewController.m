#import "SRModalClass.h"
#import "SRChatViewController.h"
#import "SRGroupDetailViewController.h"
#import "SRGroupDiscoveryViewController.h"
#import "SRNewGroupViewController.h"
#import "SRGroupDetailTabViewController.h"

@implementation SRGroupDetailTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    // Call super
    self = [super initWithNibName:@"SRGroupDetailTabViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Set delegate
    [self setDelegate:self];


    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    // Check if user has privillage to edit group
    NSDictionary *grpDict = (APP_DELEGATE).server.groupDetailInfo;
    NSArray *members = grpDict[kKeyGroupMembers];

    BOOL isAdmin = NO;
    for (NSUInteger i = 0; i < [members count] && !isAdmin; i++) {
        NSDictionary *dict = members[i];
        if ([dict[kKeyIsAdmin] isEqualToString:@"1"] && [dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
            isAdmin = YES;
        }
    }

    rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"edit_profile_white.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(editGroupBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    NSMutableArray *memberIdsArr = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < [members count]; i++) {
        NSDictionary *dict = members[i];
        [memberIdsArr addObject:dict[kKeyUserID]];
        if ([memberIdsArr containsObject:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
            if ([dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] && [dict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                if ([[grpDict valueForKey:kKeyGroupType] boolValue]) {
                    rightButton.hidden = NO;
                } else if ([dict[kKeyIsAdmin] isEqualToString:@"1"]) {
                    rightButton.hidden = NO;
                } else
                    rightButton.hidden = YES;
            }
        } else
            rightButton.hidden = YES;
    }

    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.unselectedItemTintColor = appTintColor;
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];

    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;

    [[UITabBarItem appearance]                      setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f],
            NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance]              setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f],
            NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];


    // Set SRGroupUser tab
    SRGroupDetailViewController *groupTab = [[SRGroupDetailViewController alloc] initWithNibName:nil bundle:nil groupInfo:(APP_DELEGATE).server.groupDetailInfo server:(APP_DELEGATE).server];
    UIImage *tabImage = [UIImage imageNamed:@"group-orange-50px.png"];
    UITabBarItem *objUserTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userGroup.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"group-white-50px.png"]];
    [objUserTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    groupTab.tabBarItem = objUserTabItem;

    //  Set SRGroupDiscovery tab
    SRGroupDiscoveryViewController *discoveryTab = [[SRGroupDiscoveryViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-discovery.png"];
    UITabBarItem *objSRSOPTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userDiscovery.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-discovery-active.png"]];
    [objSRSOPTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    discoveryTab.tabBarItem = objSRSOPTabItem;

    // Set Pings tab
   // NSLog(@"%@", (APP_DELEGATE).server.groupDetailInfo);
    SRChatViewController *objSRPingsView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:(APP_DELEGATE).server.groupDetailInfo];
    tabImage = [UIImage imageNamed:@"tabbar-ping.png"];
    UITabBarItem *SRPingsViewTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userPing.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-ping-active.png"]];
    [SRPingsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRPingsView.tabBarItem = SRPingsViewTabItem;

    self.viewControllers = @[groupTab, discoveryTab, objSRPingsView];
    self.selectedIndex = 0;

    //Set Enable or Disable Chat option
    NSArray *groupMembersDict = [(APP_DELEGATE).server.groupDetailInfo valueForKey:kKeyGroupMembers];
    BOOL isMember = NO;
    if ([[(APP_DELEGATE).server.groupDetailInfo valueForKey:kKeyUserID] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:kKeyId]]) {
        isMember = YES;
    } else {
        for (NSDictionary *dict in groupMembersDict) {

            if ([[dict valueForKey:kKeyUserID] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:kKeyId]]) {
                if ([dict[@"invitation_status_master_id"] isEqualToString:@"1"]) {
                    isMember = YES;

                }
            }
        }
    }
    if (!isMember) {
        rightButton.hidden = YES;
        [[[self tabBar] items][2] setEnabled:FALSE];
    }

}
// -----------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    // Title
    if ((APP_DELEGATE).server.groupDetailInfo != nil) {
        NSString *fullName = [NSString stringWithFormat:@"%@", [(APP_DELEGATE).server.groupDetailInfo valueForKey:kKeyName]];
        [SRModalClass setNavTitle:fullName forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }
}


#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    if ((APP_DELEGATE).isOnChat && (APP_DELEGATE).isPreviewOpen) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationBackClicked
                                                            object:nil];
    } else
        [self.navigationController popViewControllerAnimated:NO];
}

// ---------------------------------------------------------------------------------------
// plusBtnAction:

- (void)editGroupBtnAction:(id)sender {

    NSArray *membersArr = [[NSArray alloc] initWithArray:(APP_DELEGATE).server.groupDetailInfo[kKeyGroupMembers]];
    BOOL isAdmin = NO;
    for (int i = 0; i < membersArr.count; i++) {
        if ([[membersArr[i] objectForKey:kKeyUserID] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:kKeyId]]) {
            isAdmin = YES;
        }
    }

    if ([[(APP_DELEGATE).server.groupDetailInfo valueForKey:kKeyUserID] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:kKeyId]] || isAdmin || [[(APP_DELEGATE).server.groupDetailInfo valueForKey:kKeyGroupType] boolValue]) {

        SRNewGroupViewController *objEditGroup = [[SRNewGroupViewController alloc] initWithNibName:nil bundle:nil dict:(APP_DELEGATE).server.groupDetailInfo server:(APP_DELEGATE).server];
        [self.navigationController pushViewController:objEditGroup animated:NO];
    } else {
        [SRModalClass showAlert:NSLocalizedString(@"alert.group_edit.text", @"")];
    }

}

#pragma mark -
#pragma mark TabBarController Delegate
#pragma mark -

// ----------------------------------------------------------------------------
// didSelectViewController:

- (void)tabBarController:(UITabBarController *)inTabBarController didSelectViewController:(UIViewController *)viewController {
    if (inTabBarController.selectedIndex == 1 || inTabBarController.selectedIndex == 2) {
        if (rightButton == nil) {
            rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"edit_profile_white.png"] forViewNavCon:self offset:-23];
        }
        [rightButton setImage:[UIImage imageNamed:@"group-white-50px.png"] forState:UIControlStateNormal];
        rightButton.userInteractionEnabled = NO;
    } else {
        NSDictionary *grpDict = (APP_DELEGATE).server.groupDetailInfo;
        NSArray *members = grpDict[kKeyGroupMembers];

        for (NSUInteger i = 0; i < [members count]; i++) {
            NSDictionary *dict = members[i];
            if ([dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] && [dict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                if ([[grpDict valueForKey:kKeyGroupType] boolValue]) {
                    rightButton.hidden = NO;
                } else if ([dict[kKeyIsAdmin] isEqualToString:@"1"]) {
                    rightButton.hidden = NO;
                } else
                    rightButton.hidden = YES;
            } else if ([dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] && [dict[kKeyInvitationStatus] isEqualToString:@"0"]) {
                rightButton.hidden = YES;
            }
        }
    }
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// ShowGroupSucceed:

- (void)ShowGroupSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSString class]]) {
        [self.navigationController popViewControllerAnimated:NO];
    }

    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
}

@end
