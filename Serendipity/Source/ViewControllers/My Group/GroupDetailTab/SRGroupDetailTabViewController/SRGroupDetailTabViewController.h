#import <UIKit/UIKit.h>

@interface SRGroupDetailTabViewController : UITabBarController<UITabBarControllerDelegate>
{
    // Instance
    UIButton *rightButton;
}

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil;
@end
