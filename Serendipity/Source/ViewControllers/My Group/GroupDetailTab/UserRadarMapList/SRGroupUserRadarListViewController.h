#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "SRExtraRadarCircle.h"
#import "SRRadarCircleView.h"
#import "SRRadarDotView.h"
#import "SRRadarFocusView.h"
#import "SRServerConnection.h"
#import "SRChatViewController.h"

@interface SRGroupUserRadarListViewController : ParentViewController{
    // Instance variable
    float currentDeviceBearing;
    float latestDistance;
    float settingDistance;
    
    float headingAngle;
    
    NSTimer *detectCollisionTimer;
    
    NSArray *nearbyUsers;
    NSArray *sourceArr;
    
    NSMutableArray *dotsArr;
    NSMutableArray *dotsUserPicsArr;
    
    NSMutableDictionary *userDataDict;
    
    // View
    SRRadarCircleView *radarCircleView;
    SRRadarFocusView *radarFocusView;
    
    UIImageView *swipeView;
    UIView *swipeContainerView;
    UILabel *lblDistance;
    UILabel *lblNoPPl;
    NSString *distanceUnit;
    CGPoint touchLocation;
    
    // Flag
    BOOL isOnce;
    BOOL isSwipedOnce;
    
    // Server
    SRServerConnection *server;
}

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *bckProfileImg;
@property (nonatomic, strong) IBOutlet UIView *radarViewContainer;
@property (nonatomic, strong) IBOutlet UIView *northLineView;
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet SRProfileImageView *radarProfileImgView;
@property (nonatomic, strong) IBOutlet SRExtraRadarCircle *extraCircleView;

@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
            inUserArr:(NSArray *)userArr;

#pragma mark
#pragma mark - Load Method
#pragma mark

- (void)loadUsersInRadar:(NSArray *)inArr;

@end
