#import "SRModalClass.h"
#import "SwipeableTableViewCell.h"
#import "SRConnectionCellView.h"
#import "SRUserTabViewController.h"
#import "SRGroupUserListView.h"
#import "SRMapViewController.h"
#import "SRDistanceClass.h"
#import "SRProfileTabViewController.h"

@interface GroupMemberDistanceClass : NSObject

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *distance;
@property(nonatomic, strong) NSString *dataId;
@property(strong, nonatomic) CLLocation *myLocation;

@end

@implementation GroupMemberDistanceClass


@end

@interface SRGroupUserListView ()

@property(nonatomic, strong) NSOperationQueue *distanceQueue;

@end

@implementation SRGroupUserListView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRGroupUserListView" owner:self options:nil];
        self = nibArray[0];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(imagesGotDownloaded:)
                              name:kKeyNotificationGroupUserRadarImageDownloaded
                            object:nil];
    }


    // Return
    return self;
}

//-----------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Load Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// loadUsers:

- (void)loadUsers:(NSArray *)inArr {
    tempDataArray = [[NSMutableArray alloc] init];
    _distanceQueue = [[NSOperationQueue alloc] init];
    //Get and Set distance measurement unit
    if ([[[_server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    usersListArr = [[NSMutableArray alloc] initWithArray:inArr];

    // Group image
    if (self.server.groupDetailInfo[kKeyImageObject] != nil) {
        self.profileImg.image = self.server.groupDetailInfo[kKeyImageObject];
    } else {
        if ([self.server.groupDetailInfo[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            // Get result

            if ([self.server.groupDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [self.server.groupDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [self.profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
            } else
                self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
        } else {
            self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
        }
    }

    // reload data
    // [self getDistance];
    [self.objTblView reloadData];

}

- (void)getDistance {


    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (!tempDataArray.count) {

        [arr addObjectsFromArray:usersListArr];

    } else {

        for (NSDictionary *userDict in usersListArr) {

            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

            NSArray *tempArr = [tempDataArray filteredArrayUsingPredicate:predicateForDistanceObj];

            if (tempArr && tempArr.count) {

                GroupMemberDistanceClass *distanceObj = [tempArr firstObject];

                NSDictionary *locationDict = userDict[kKeyLocation];


                BOOL isBLocationChanged = NO;
                BOOL isALocationChanged = NO;

                if (![distanceObj.lat isEqualToString:locationDict[kKeyLattitude]] || ![distanceObj.longitude isEqualToString:locationDict[kKeyRadarLong]]) {

                    isBLocationChanged = YES;
                }

                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];

                if (distance >= 160) {

                    isALocationChanged = YES;

                }

                if (isBLocationChanged || isALocationChanged) {
                    [arr addObject:userDict];
                }
            } else {

                [arr addObject:userDict];

            }
        }
    }


    for (int i = 0; i < arr.count; i++) {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];


        if (_server.myLocation != nil && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            NSBlockOperation *op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled) {

                    [self callAddressAPI:userDict];
                } else {

                  //  NSLog(@"Operation Not canceled");
                }
            }];
            [_distanceQueue addOperation:op];

        }

    }

}

- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }
}

- (void)callAddressAPI:(NSDictionary *)userDict {
 //   NSLog(@"Uesrdict::%@", userDict);
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:_server.myLocation.coordinate.latitude longitude:_server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];

    NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f&lon=%f", kNominatimServer, kAddressApi, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
  //  NSLog(@"Nominatim URl=%@", urlStr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   Boolean isAddressPresent = true;
                                   GroupMemberDistanceClass *obj = [[GroupMemberDistanceClass alloc] init];

                                   for (NSString *keyStr in json) {
                                       if ([keyStr isEqualToString:@"error"]) {
                                           isAddressPresent = false;
                                       }
                                   }
                                   if (isAddressPresent) {
                                       obj.address = [json valueForKey:@"display_name"];
                                   }
                                   NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];

                                   NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
                                   [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                                          completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                                              if (!error) {
                                                                  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                                                  if ([self IsValidResponse:data]) {
                                                                      NSDictionary *dictOSM = [json valueForKey:@"paths"];
                                                                      NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                                                                      NSString *distance = arrdistance[0];
                                                                      double convertDist = [distance doubleValue];
                                                                      if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                                                          //meters to killometer
                                                                          convertDist = convertDist / 1000;
                                                                          obj.distance = [NSString stringWithFormat:@"%.1f%@", convertDist, distanceUnit];
                                                                      } else {
                                                                          //meters to miles
                                                                          convertDist = (convertDist * 0.000621371192);
                                                                          obj.distance = [NSString stringWithFormat:@"%.1f%@", convertDist, distanceUnit];
                                                                      }
                                                                  }
                                                              } else {
                                                                  obj.distance = [NSString stringWithFormat:@"%@%@", userDict[kKeyDistance], distanceUnit];
                                                              }
                                                              NSUInteger indexOfUserObject = [usersListArr indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                                                                  if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                                                      return YES;
                                                                  } else {

                                                                      return NO;
                                                                  }
                                                              }];
                                                              NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                                              NSArray *tempArr = [tempDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                                              if (tempArr && tempArr.count) {

                                                                  NSUInteger indexOfObject = [tempDataArray indexOfObjectPassingTest:^BOOL(GroupMemberDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                                                                      if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                                                          return YES;
                                                                      } else {

                                                                          return NO;
                                                                      }

                                                                  }];
                                                                  tempDataArray[indexOfObject] = obj;
                                                              } else {
                                                                  [tempDataArray addObject:obj];
                                                              }
                                                              dispatch_async(dispatch_get_main_queue(), ^{

                                                                  if (indexOfUserObject != NSNotFound && indexOfUserObject < usersListArr.count) {
                                                                      NSDictionary *dict = usersListArr[indexOfUserObject];
                                                                      NSDictionary *dictLocation = [dict valueForKey:kKeyLocation];
                                                                      [dictLocation setValue:obj.address forKey:kKeyLiveAddress];
                                                                      [dict setValue:dictLocation forKey:kKeyLocation];
                                                                      usersListArr[indexOfUserObject] = dict;
                                                                      [_objTblView reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                                                  }
                                                              });
                                                          }];
                                   obj.dataId = userDict[kKeyId];
                                   NSDictionary *locationDict = userDict[kKeyLocation];
                                   obj.lat = locationDict[kKeyLattitude];
                                   obj.longitude = locationDict[kKeyRadarLong];
                                   obj.myLocation = _server.myLocation;
                                   NSUInteger indexOfUserObject = [usersListArr indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                                       if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                           return YES;
                                       } else {

                                           return NO;
                                       }
                                   }];
                                   NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                   NSArray *tempArr = [tempDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                   if (tempArr && tempArr.count) {

                                       NSUInteger indexOfObject = [tempDataArray indexOfObjectPassingTest:^BOOL(GroupMemberDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                                           if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                               return YES;
                                           } else {

                                               return NO;
                                           }

                                       }];
                                       tempDataArray[indexOfObject] = obj;
                                   } else {
                                       [tempDataArray addObject:obj];
                                   }
                                   dispatch_async(dispatch_get_main_queue(), ^{

                                       if (indexOfUserObject != NSNotFound && indexOfUserObject < usersListArr.count) {
                                           NSDictionary *dict = usersListArr[indexOfUserObject];
                                           NSDictionary *dictLocation = [dict valueForKey:kKeyLocation];
                                           [dictLocation setValue:obj.address forKey:kKeyLiveAddress];
                                           [dict setValue:dictLocation forKey:kKeyLocation];
                                           usersListArr[indexOfUserObject] = dict;
                                           [_objTblView reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                       }
                                   });
                               }

                           }];
}

#pragma mark
#pragma mark Action Methods
#pragma mark
//
// ---------------------------------------------------------------------------------------
// actionOnProfileCellBtn:

- (void)actionOnProfileCellBtn:(UIButton *)sender {
    // Get profile
    NSDictionary *userDict = usersListArr[sender.tag];
    self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:userDict];

    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
    [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
}

//
// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTappedCaptured:(UIButton *)sender {
    NSMutableDictionary *userDict = usersListArr[sender.tag];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:userDict server:self.server];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
    [((UIViewController *) self.delegate).navigationController pushViewController:dropPin animated:YES];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
}

// ---------------------------------------------------------------------------------------
// degreeBtnSelected:
- (void)degreeBtnSelected:(UIButton *)sender {
    // Get profile
    NSDictionary *userDict = usersListArr[sender.tag];
    self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:userDict];

    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:@"SRUserTabViewController" bundle:nil profileData:nil server:self.server];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
    [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
}

#pragma mark
#pragma mark Tap Gesture
#pragma mark

// ---------------------------------------------------------------------------------------
// gestureRecognizer:
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]]) {      //change it to your condition
        return NO;
    }
    return YES;
}

// ---------------------------------------------------------------------------------------
// singleTapGestureCaptured:

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)sender {
    SwipeableTableViewCell *cell = (SwipeableTableViewCell *) sender.view;
    if (CGPointEqualToPoint(cell.scrollView.contentOffset, CGPointZero)) {
        // Get profile
        NSDictionary *userDict = usersListArr[cell.tag];
        // Get profile
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:userDict];
        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];

        // Show user public profile
        if ([[_server.loggedInUserInfo valueForKey:kKeyId] isEqualToString:[dictionary valueForKey:kKeyId]]) {
            SRProfileTabViewController *profileTabCon = [[SRProfileTabViewController alloc] initWithNibName:nil bundle:nil];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
            [((UIViewController *) self.delegate).navigationController pushViewController:profileTabCon animated:YES];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
        } else {
            SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
            [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
        }
    } else
        [cell.scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [usersListArr count];
}

// ----------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// ----------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"connectionCell";
    NSDictionary *userDict = usersListArr[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];

    // For the purposes of this demo, just return a random cell.
    SwipeableTableViewCell *cell;
    if (!cell) {
        cell = [[SwipeableTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.backgroundColor = [UIColor clearColor];

        // Tap Gesture
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        singleTap.delegate = self;
        [cell addGestureRecognizer:singleTap];
        cell.tag = indexPath.section;
        singleTap.cancelsTouchesInView = NO;

        // Assign values
        SRConnectionCellView *contentView = (SRConnectionCellView *) cell.scrollViewContentView;
        contentView.lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];

        // User image
        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {

            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        // Occupation
        if ([userDict[kKeyOccupation] length] > 0)
            contentView.lblProfession.text = userDict[kKeyOccupation];
        else
            contentView.lblProfession.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");
        contentView.lblProfession.lineBreakMode = NSLineBreakByTruncatingTail;
        if ([contentView.lblProfession.text isEqualToString:@""]) {
            contentView.lblName.frame = CGRectMake(contentView.lblName.frame.origin.x, contentView.lblName.frame.origin.y + 15, contentView.lblName.frame.size.width, contentView.lblName.frame.size.height);
        }
        //Favourite
        contentView.favImg.hidden = TRUE;
        //Invisible
        contentView.btnInvisible.hidden = TRUE;
        contentView.blackOverlay.hidden = TRUE;
        // Location

        NSDictionary *userLocDict = [[NSDictionary alloc] init];
        if ([userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            userLocDict = userDict[kKeyLocation];
        }

        if (![[NSString stringWithFormat:@"%@", [userLocDict valueForKey:kKeyDistance]] isEqualToString:@""]) {
            contentView.lblDistance.text = [userLocDict valueForKey:kKeyDistance];

        } else
            contentView.lblDistance.text = [NSString stringWithFormat:@"0.0 %@", distanceUnit];

        if (![[NSString stringWithFormat:@"%@", [userLocDict valueForKey:kKeyLiveAddress]] isEqualToString:@""]) {
            contentView.lblAddress.text = [userLocDict valueForKey:kKeyLiveAddress];
            [contentView.lblAddress sizeToFit];
            [contentView.btnCompass setUserInteractionEnabled:YES];
        } else {
            contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        }



//        else{
//
//            contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
//        }
//        
//        if ([contentView.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")])
//        {
//            contentView.lblDistance.text = [NSString stringWithFormat:@"0.0 %@",distanceUnit];
//        }
//        if ([contentView.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")])
//        {
//            contentView.lblDistance.text = [NSString stringWithFormat:@"0.0 %@",distanceUnit];
//        }

        //For Family show (You can t date Family  member so hide date icon)

//        if ([[userDict objectForKey:kKeyFamily]isKindOfClass:[NSDictionary class]]) {
//            // NSDictionary *familyDict = [userDict objectForKey:kKeyFamily];
//            contentView.lblDating.hidden = YES;
//            contentView.imgDating.hidden = YES;
//            contentView.lblFamily.hidden = YES;
//        }else
        // Dating
        if ([userDict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *settingDict = userDict[kKeySetting];
            contentView.lblFamily.hidden = YES;

            if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
                contentView.lblDating.hidden = NO;
                contentView.imgDating.hidden = NO;
                contentView.imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
                // contentView.lblDating.textColor = [UIColor orangeColor];
            } else {
                contentView.imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
                contentView.lblDating.hidden = YES;
                contentView.imgDating.hidden = YES;
            }
        } else {
            contentView.lblDating.hidden = YES;
            contentView.imgDating.hidden = YES;
            contentView.lblFamily.hidden = YES;
            contentView.imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
        }

        // Degree
        [contentView.btnDegree addTarget:self action:@selector(degreeBtnSelected:) forControlEvents:UIControlEventTouchUpInside];
        contentView.btnDegree.tag = indexPath.section;

        contentView.btnDegree.layer.cornerRadius = contentView.btnDegree.frame.size.width / 2.0;
        contentView.btnDegree.layer.masksToBounds = YES;
        if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            [contentView.btnDegree setTitle:[NSString stringWithFormat:@" %ld°", (long) degree] forState:UIControlStateNormal];
        } else {
            [contentView.btnDegree setTitle:@" 6°" forState:UIControlStateNormal];
        }
        
        [contentView.btnDegree setHidden:FALSE];
        if ([userDict[kKeyId] isEqual:self.server.loggedInUserInfo[kKeyId]]) {
            [contentView.btnDegree setHidden:TRUE];
        }

        //TODO:
        CLLocationCoordinate2D myLoc = {self.server.myLocation.coordinate.latitude,
                self.server.myLocation.coordinate.longitude};

        CLLocationCoordinate2D userLoc;
        float lat = 0.0;
        if ([userLocDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            if ([userLocDict valueForKey:kKeyLattitude] != nil)
                lat = [[userLocDict valueForKey:kKeyLattitude] floatValue];
        }


        float lon = 0.0;
        if ([userLocDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            if ([userLocDict valueForKey:kKeyRadarLong] != nil)
                lon = [[userLocDict valueForKey:kKeyRadarLong] floatValue];
        }

        userLoc.latitude = lat;
        userLoc.longitude = lon;


        [contentView.btnCompass addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
        contentView.btnCompass.tag = indexPath.section;

        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            contentView.lblCompassDirection.text = @"N";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            contentView.lblCompassDirection.text = @"E";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            contentView.lblCompassDirection.text = @"S";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            contentView.lblCompassDirection.text = @"W";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            contentView.lblCompassDirection.text = @"NE";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            contentView.lblCompassDirection.text = @"NW";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            contentView.lblCompassDirection.text = @"SE";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            contentView.lblCompassDirection.text = @"SW";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }


        contentView.btnProfileView.tag = indexPath.section;
        [contentView.btnChat setHidden:YES];
        // Adjust green dot label appearence
        contentView.lblOnline.hidden = YES;
        [contentView.btnChat setHidden:true];
    }

    // Return
    return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }

    // Return
    return height;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }

    // Return
    return headerView;
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// imagesGotDownloaded:

- (void)imagesGotDownloaded:(NSNotification *)inNotify {
    // Remove all objects
    dotsUserPicsArr = [inNotify object];

    // Reload table view
    [self.objTblView reloadData];
}

@end
