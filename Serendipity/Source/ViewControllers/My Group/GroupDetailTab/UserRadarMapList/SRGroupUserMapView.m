#import "SRModalClass.h"
#import "CalloutAnnotation.h"
#import "SRUserTabViewController.h"
#import "SRMapProfileView.h"
#import "SRGroupUserMapView.h"
#import "SRProfileTabViewController.h"
#import "SRPanoramaViewController.h"

const double groupUserZoomLevel = 0.007812;

@implementation SRGroupUserMapView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRGroupUserMapView" owner:self options:nil];
        self = nibArray[0];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(imagesGotDownloaded:)
                              name:kKeyNotificationGroupUserRadarImageDownloaded
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updatedLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        [defaultCenter addObserver:self selector:@selector(backFromPanoramaView) name:@"BackToGroupMapFromStreetView" object:nil];
    }
    // Return
    return self;
}

//-----------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Load Methods
#pragma mark

- (void)willMoveToSuperview:(UIView *)newSuperview {

}
// ---------------------------------------------------------------------------------------
// loadUsers:

- (void)loadUsers:(NSArray *)inArr {
    zoomInBtn.layer.borderWidth = 1.0;
    zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    zoomOutBtn.layer.borderWidth = 1.0;
    zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    [self.locationToolBar setBackgroundImage:[UIImage new]
                          forToolbarPosition:UIToolbarPositionAny
                                  barMetrics:UIBarMetricsDefault];

    MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];

    if (SCREEN_WIDTH <= 375) {
        spacer.width = -16;
    } else
        spacer.width = -22; // for example shift right bar button to the right

//    self.locationToolBar.frame = CGRectMake(self.locationToolBar.frame.origin.x, self.locationToolBar.frame.origin.y, 32, 30);
    [self.locationToolBar.layer setCornerRadius:5.0];
    [self.locationToolBar.layer setMasksToBounds:YES];
    self.locationToolBar.items = @[buttonItem];


    //Get and Set distance measurement unit
    if ([[[_server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    nearByUsersList = [[NSMutableArray alloc] init];
    userProfileViewArr = [[NSMutableArray alloc] init];
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    // ScrollView
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideScrollView:)];
    [self.scrollView addGestureRecognizer:tapGesture];
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;

    myLocation = self.server.myLocation;

    // Plot users on map
    [self.mapView setCenterCoordinate:myLocation.coordinate animated:YES];
    MKCoordinateSpan span = MKCoordinateSpanMake(0.5, 0.0);
    MKCoordinateRegion region = MKCoordinateRegionMake(MKCoordinateForMapPoint(MKMapPointForCoordinate(myLocation.coordinate)), span);
    [self.mapView setRegion:region animated:YES];

    // Add my annotation
    if (myPinAnnotation == nil) {
        myPinAnnotation = [[PinAnnotation alloc] init];
        myPinAnnotation.coordinate = myLocation.coordinate;
        [self.mapView addAnnotation:myPinAnnotation];
    } else
        myPinAnnotation.coordinate = myLocation.coordinate;

    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
        } else {
            [self.mapView removeAnnotation:annotation];
        }
    }
    for (UIView *view in userProfileViewArr) {
        [view removeFromSuperview];
    }
    // Check if users are on same distance
    NSMutableArray *usersArr = [NSMutableArray arrayWithArray:inArr];
    for (NSUInteger i = 0; i < [usersArr count];) {
        NSDictionary *dict = usersArr[i];
        double distance = 0.0;
        if ([dict isKindOfClass:[NSDictionary class]] && dict[kKeyDistance] != [NSNull null]) {
            distance = [dict[kKeyDistance] doubleValue];
        }
        NSMutableArray *mutatedArr = [NSMutableArray array];
        NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionary];
        if ([dict isKindOfClass:[NSDictionary class]]) {
            if (dict.allKeys.count > 0 && dict[kKeyLocation] != [NSNull null]) {
                mutatedDict[kKeyLocation] = dict[kKeyLocation];
            }
        }

        [mutatedArr addObject:dict];

        for (NSUInteger j = 1; j < [usersArr count]; j++) {
            NSDictionary *innerDict = usersArr[j];
            double innerDistance = 0.0;
            if ([innerDict isKindOfClass:[NSDictionary class]] && innerDict[kKeyDistance] != [NSNull null]) {

                innerDistance = [innerDict[kKeyDistance] doubleValue];
            }
            double difference;
            if (innerDistance > distance) {
                difference = innerDistance - distance;
            } else
                difference = distance - innerDistance;

            if (difference == 0) {
                [mutatedArr addObject:innerDict];
            }
        }

        // Add to near by users
        mutatedDict[kKeyCombination] = mutatedArr;
        [nearByUsersList addObject:mutatedDict];

        // Remove objects from array
        [usersArr removeObjectsInArray:mutatedArr];
    }
    // Now plot users
    [self plotUsersOnMapView];

    // Group image
    if (self.server.groupDetailInfo[kKeyImageObject] != nil) {
        self.profileImg.image = self.server.groupDetailInfo[kKeyImageObject];
    } else {
        if ([self.server.groupDetailInfo[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            // Get result
            if ([self.server.groupDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [self.server.groupDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [self.profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
            } else
                self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
        } else {
            self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
        }
    }
}

- (void)backFromPanoramaView {
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        [_mapView setCenterCoordinate:panoramaLastLoc zoomLevel:5 animated:YES];
    }
}

- (IBAction)actionOnBtnClick:(UIButton *)inSender {
    UIButton *btn = inSender;
    if (btn.tag == 1) {
        self.mapView.mapType = MKMapTypeSatellite;
        [btnStreet setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    } else {
        self.mapView.mapType = MKMapTypeStandard;
        [btnStreet setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// zoomInMap
- (IBAction)zoomInMap {
    zoomInBtn.userInteractionEnabled = NO;
    zoomInBtn.layer.borderColor = [[UIColor orangeColor] CGColor];
    zoomOutBtn.userInteractionEnabled = YES;
    zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];

    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta = MIN(region.span.latitudeDelta * 2.0, 180.0);
    region.span.longitudeDelta = MIN(region.span.longitudeDelta * 2.0, 180.0);
    [self.mapView setRegion:region animated:YES];
}

// --------------------------------------------------------------------------------
// zoomOutMap
- (IBAction)zoomOutMap {
    zoomInBtn.userInteractionEnabled = YES;
    zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    zoomOutBtn.userInteractionEnabled = NO;
    zoomOutBtn.layer.borderColor = [[UIColor orangeColor] CGColor];

    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta /= 2.0;
    region.span.longitudeDelta /= 2.0;
    [self.mapView setRegion:region animated:YES];
}
// -------------------------------------------------------------------------------------------------
// hideScrollView:

- (void)hideScrollView:(UITapGestureRecognizer *)gesture {
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;
}

// -------------------------------------------------------------------------------------------------
// addViewsOnScrollView:


- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }

}

- (void)addViewsOnScrollView:(NSArray *)inArr {

    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (NSUInteger i = 0; i < [inArr count]; i++) {
        CGRect frame = CGRectMake(0, 11, 130, 200);


        NSDictionary *userDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i > 0) {
            frame = CGRectMake(frame.origin.x + 126, 11, 130, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;

        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds
                                         byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(8.0, 8.0)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = ((CalloutAnnotationView *) annotationView).userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        ((CalloutAnnotationView *) annotationView).userProfileImage.layer.mask = maskLayer;

        if (userDict[kKeyProfileImage] != nil && [userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else {
                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
            }
        } else {
            ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
        }

        ((CalloutAnnotationView *) annotationView).lblProfileName.text = [userDict valueForKey:kKeyFirstName];
        if ([[userDict valueForKey:kKeyOccupation] length] <= 0) {
            ((CalloutAnnotationView *) annotationView).lblOccupation.text = @"None";
        } else {
            ((CalloutAnnotationView *) annotationView).lblOccupation.text = userDict[kKeyOccupation];
        }
//        double distance = 0.0;
//        if ([userDict isKindOfClass:[NSDictionary class]] && [userDict valueForKey:kKeyDistance] !=[NSNull null]) {
//            distance = [[userDict valueForKey:kKeyDistance]doubleValue];
//        }
//        
//		((CalloutAnnotationView *)annotationView).lblDistance.text = [NSString stringWithFormat:@"%.1f mi", distance];

        //Favourite
        if ([userDict[kKeyIsFavourite] boolValue] == true) {
            ((CalloutAnnotationView *) annotationView).favUserImage.hidden = FALSE;
        } else
            ((CalloutAnnotationView *) annotationView).favUserImage.hidden = TRUE;

        //Invisible
        if ([userDict[kKeyIsInvisible] boolValue] == true) {
            ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = FALSE;
        } else
            ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = TRUE;

        //Call Google API for correct distance and address from two locations
        if (_server.myLocation != nil && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            ((CalloutAnnotationView *) annotationView).lblDistance.text = [NSString stringWithFormat:@"0 miles"];
            CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:_server.myLocation.coordinate.latitude longitude:_server.myLocation.coordinate.longitude];
            CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                        if (!error) {
                            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                            if ([self IsValidResponse:data]) {
                                NSDictionary *dictOSM = [json valueForKey:@"paths"];
                                NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                                NSString *distance = arrdistance[0];
                                double convertDist = [distance doubleValue];
                                if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                    //meters to killometer
                                    convertDist = convertDist / 1000;
                                } else {
                                    //meters to miles
                                    convertDist = (convertDist * 0.000621371192);
                                }
                                //Correct distance
                                ((CalloutAnnotationView *) annotationView).lblDistance.text = [NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit];
                            }
                        } else {
                            ((CalloutAnnotationView *) annotationView).lblDistance.text = [NSString stringWithFormat:@"%@ %@", [userDict valueForKey:kKeyDistance], distanceUnit];
                        }
                    }];
        } else {
            ((CalloutAnnotationView *) annotationView).lblDistance.text = [NSString stringWithFormat:@"0 miles"];
        }
        //Degree
        if ([[userDict valueForKey:kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            ((CalloutAnnotationView *) annotationView).lblDegree.text = [NSString stringWithFormat:@" %ld°", (long) degree];
        } else {
            ((CalloutAnnotationView *) annotationView).lblDegree.text = @" 6°";
        }

        // Add Shadow to btnPing
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOpacity:8.5];
        [((CalloutAnnotationView *) annotationView).pingButton addTarget:self action:@selector(pingButtonAction:) forControlEvents:UIControlEventTouchUpInside];

        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [((CalloutAnnotationView *) annotationView).userProfileImage addGestureRecognizer:annotationTapGesture];
        ((CalloutAnnotationView *) annotationView).userDict = userDict;
        
        if (userDict[kKeyLocation] && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
                    //Compass location
            CLLocationCoordinate2D myLoc = {self.server.myLocation.coordinate.latitude,
                self.server.myLocation.coordinate.longitude};
                    
                    NSDictionary *userLocDict = userDict[kKeyLocation];
                    CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};
                    
        //            [contentView.btnCompass addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
        //            contentView.btnCompass.tag = indexPath.section;
                    
                    NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
                    if (directionValue == kKeyDirectionNorth) {
                        annotationView.lblCompassDirection.text = @"N";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionEast) {
                        annotationView.lblCompassDirection.text = @"E";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionSouth) {
                        annotationView.lblCompassDirection.text = @"S";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionWest) {
                        annotationView.lblCompassDirection.text = @"W";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionNorthEast) {
                        annotationView.lblCompassDirection.text = @"NE";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionNorthWest) {
                        annotationView.lblCompassDirection.text = @"NW";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionSouthEast) {
                        annotationView.lblCompassDirection.text = @"SE";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionSoutnWest) {
                        annotationView.lblCompassDirection.text = @"SW";
                        [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
                    }
                }

        // Add subview to scroll view
        //if users count is one
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }

        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else
            self.scrollView.scrollEnabled = NO;

        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
    }
}

// -------------------------------------------------------------------------------
// makeRoundedImage:

- (UIImage *)makeRoundedImage:(UIImage *)image
                       radius:(float)radius; {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = radius / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Return
    return roundedImage;
}

// -------------------------------------------------------------------------------
// plotUsersOnMapView:

- (void)plotUsersOnMapView {
    [userProfileViewArr removeAllObjects];

    PinAnnotation *pinAnnotation;
    CLLocationCoordinate2D coordinate;

    for (NSDictionary *dict in nearByUsersList) {
        if (![[dict[kKeyLocation] objectForKey:kKeyLattitude] isKindOfClass:[NSNull class]]) {
            float lat = [[dict[kKeyLocation] objectForKey:kKeyLattitude] floatValue];
            float lon = [[dict[kKeyLocation] objectForKey:kKeyRadarLong] floatValue];
            coordinate.latitude = lat;
            coordinate.longitude = lon;
            pinAnnotation = [[PinAnnotation alloc] init];
            pinAnnotation.coordinate = coordinate;
            pinAnnotation.title = @"default";
            [self.mapView addAnnotation:pinAnnotation];
        }
    }
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

//
// -----------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
}

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
   // NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self];
  //  NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:touchPoint toCoordinateFromView:self];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        self->streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    self->panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_mapView camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [self->_mapView setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        self->isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
                        [((UIViewController *) self.delegate).navigationController pushViewController:panoramaView animated:YES];
                        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"BackToGroupMapFromStreetView" object:nil];
                    });
                } else {

                    if (!(CGRectContainsPoint(self->streetViewBtn.frame, touchPoint))) {
                        if (!self->isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
//        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapView addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_mapView camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [_mapView setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
                        [((UIViewController *) self.delegate).navigationController pushViewController:panoramaView animated:YES];
                        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"BackToGroupMapFromStreetView" object:nil];
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}

#pragma mark
#pragma mark
#pragma mark Action methods

// --------------------------------------------------------------------------------
// actionOnImageTap:

- (void)actionOnImageTap:(UITapGestureRecognizer *)inTap {
}

// --------------------------------------------------------------------------------
//

- (void)pingButtonAction:(UIButton *)sender {

    //badgeBtn.hidden = YES;
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];

    BOOL flag = YES;//Check first if you allow to ping or not

    if ([[dictionary valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[dictionary valueForKey:kKeySetting]];

        if (![[setting valueForKey:kKeyallowPings] boolValue])
            flag = NO;
    }

    if (flag) {
        if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];

        // Set chat view
        SRChatViewController *objSRChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:self.server.friendProfileInfo];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
        [((UIViewController *) self.delegate).navigationController pushViewController:objSRChatView animated:YES];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
    }

}

// --------------------------------------------------------------------------------
// annotationTapAction:

- (void)annotationTapAction:(UITapGestureRecognizer *)sender {
    //badgeBtn.hidden = YES;
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];

    // Show user public profile
    if ([[_server.loggedInUserInfo valueForKey:kKeyId] isEqualToString:[dictionary valueForKey:kKeyId]]) {
        SRProfileTabViewController *profileTabCon = [[SRProfileTabViewController alloc] initWithNibName:nil bundle:nil];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
        [((UIViewController *) self.delegate).navigationController pushViewController:profileTabCon animated:YES];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
    } else {
        SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
        [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
    }

}

#pragma mark
#pragma mark MapView Delegates
#pragma mark

//-------------------------------------------------------------------------------------------------------------
// Check Zoom Level:

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {

    if (!(zoomInBtn.userInteractionEnabled)) {
        zoomInBtn.userInteractionEnabled = YES;
        zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    }
    if (!(zoomOutBtn.userInteractionEnabled)) {
        zoomOutBtn.userInteractionEnabled = YES;
        zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    }

    MKZoomScale currentZoomScale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
    if (currentZoomScale > groupUserZoomLevel) {
        if (!isCheckZoomLvl) {
            isCheckZoomLvl = YES;
            //NSMutableArray *annotationsToRemove = [mapView.annotations mutableCopy];
            //				[annotationsToRemove removeObject:mapView.userLocation];
            //				[self.mapView removeAnnotations:annotationsToRemove];

            for (id <MKAnnotation> annotation in self.mapView.annotations) {
                if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                } else {
                    [self.mapView removeAnnotation:annotation];
                }
            }
            for (UIView *view in userProfileViewArr) {
                [view removeFromSuperview];
            }
            [self plotUsersOnMapView];
            //myPinAnnotation.coordinate = myLocation.coordinate;

            isZoomIn = YES;
        }
    } else {
        if (isCheckZoomLvl) {
            isCheckZoomLvl = NO;
            //				NSMutableArray *annotationsToRemove = [mapView.annotations mutableCopy];
            //				[annotationsToRemove removeObject:mapView.userLocation];
            //				[self.mapView removeAnnotations:annotationsToRemove];
            for (id <MKAnnotation> annotation in self.mapView.annotations) {
                if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                } else {
                    [self.mapView removeAnnotation:annotation];
                }
            }
            isZoomIn = NO;
            for (UIView *view in userProfileViewArr) {
                [view removeFromSuperview];
            }

            // Add my annotation
            //myPinAnnotation.coordinate = myLocation.coordinate;

            [self plotUsersOnMapView];
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------
// viewForAnnotation:

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *annotationView;

    // check annotation is not user location
    if ([annotation isEqual:[mapView userLocation]]) {
        // Return for user pin
        return nil;
    }
    if ([annotation isKindOfClass:[PinAnnotation class]]) {
        // Pin annotation.
        annotationView = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:@"Pin"];

        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Pin"];
        } else {
            annotationView.annotation = annotation;
        }

        // My pin
        if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
            // Set profile pic
//			UIImage *profileImage;
            self.imgUserProfile.layer.masksToBounds = YES;
//			if (self.imgUserProfile == nil) {
//				profileImage = [UIImage imageNamed:@"profile_menu.png"];
//			}
            SRMapProfileView *myProfileView = [[SRMapProfileView alloc] init];

            UIImage *img = [SRModalClass imageFromUIView:myProfileView];
            annotationView.image = img;

            //[annotationView addSubview:myProfileView];
        } else {
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
            SRMapProfileView *userProfileView = nibArray[1];

            if (!isZoomIn) {
                userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y, 17, 17);
                userProfileView.bgImg.hidden = YES;

                for (NSDictionary *user in nearByUsersList) {
                    if (annotation.coordinate.latitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyLattitude] floatValue] && annotation.coordinate.longitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyRadarLong] floatValue]) {
                        if ([user[kKeyCombination] count] > 1) {
                            UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake(7, 6, 10, 10)];
                            lblCount.textColor = [UIColor blackColor];
                            lblCount.textAlignment = NSTextAlignmentCenter;
                            lblCount.backgroundColor = [UIColor clearColor];
                            lblCount.font = [UIFont fontWithName:kFontHelveticaRegular size:8.0];
                            lblCount.text = [NSString stringWithFormat:@"%lu", [user[kKeyCombination] count]];

                            // Add to view
                            [userProfileView addSubview:lblCount];
                            userProfileView.userProfileImg.image = [UIImage imageNamed:@"radar-pin-orange-15px.png"];
                        } else {
                            NSDictionary *updatedDict = [user[kKeyCombination] objectAtIndex:0];
                            updatedDict = [SRModalClass removeNullValuesFromDict:updatedDict];
                            if ([updatedDict[kKeyDegree] integerValue] != 1) {
                                userProfileView.userProfileImg.image = [UIImage imageNamed:@"radar-pin-orange-15px.png"];
                            } else {
                                userProfileView.userProfileImg.image = [UIImage imageNamed:@"radar-pin-dot-17px.png"];
                            }
                        }
                    }
                }
            } else {
                // Provide round rect
                userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y - 2, 37, 37);

                for (NSDictionary *user in nearByUsersList) {
                    if (annotation.coordinate.latitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyLattitude] floatValue] && annotation.coordinate.longitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyRadarLong] floatValue]) {
                        NSArray *combinationArr = user[kKeyCombination];
                        NSDictionary *userDict = combinationArr[0];

                        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                if ([imageName length] > 0) {
                                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                                    [userProfileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                } else
                                    userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                            } else
                                userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                        } else
                            userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];

                        userProfileView.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                        userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2.0;
                        userProfileView.userProfileImg.clipsToBounds = YES;
                        userProfileView.userProfileImg.layer.masksToBounds = YES;


                        if ([user[kKeyCombination] count] > 1) {
                            userProfileView.userProfileImg.layer.borderWidth = 2.0;
                            userProfileView.userProfileImg.layer.borderColor = [UIColor whiteColor].CGColor;
                            userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2;
                        }
                    }
                }
            }

            // Add to view
            [userProfileViewArr addObject:userProfileView];
            UIImage *img = [SRModalClass imageFromUIView:userProfileView];
            annotationView.image = img;
        }
    } else if ([annotation isKindOfClass:[CalloutAnnotation class]]) {
        annotationView.image = [UIImage imageNamed:@"transparent.png"];

        // Set Details in annotattionview
        for (NSDictionary *user in nearByUsersList) {
            if (annotation.coordinate.latitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyLattitude] floatValue] && annotation.coordinate.longitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyRadarLong] floatValue]) {
                NSArray *combinationArr = user[kKeyCombination];
                // Callout annotation.
                annotationView = [[NSBundle mainBundle] loadNibNamed:@"CalloutAnnotationView" owner:self options:nil][1];

                annotationView.hidden = YES;
                annotationView.image = [UIImage imageNamed:@"transparent.png"];

                // Add ProfileViews in scrollview
                selectedCombinatonArr = combinationArr;
                [self addViewsOnScrollView:combinationArr];

                //Set MapView Center at annotation coordinates
                CalloutAnnotation *calloutAnnotation = (CalloutAnnotation *) annotation;
                // Move the display position of MapView.
                [UIView animateWithDuration:0.5f
                                 animations:^(void) {
                                     [mapView setCenterCoordinate:calloutAnnotation.coordinate animated:YES];
                                 }];
            }
        }
    }
    // No callout
    [annotationView setCanShowCallout:NO];
    [annotationView setEnabled:YES];

    // Return
    return annotationView;
}

//-----------------------------------------------------------------------------------------------------------------
// didSelectAnnotationView:

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    MKZoomScale currentZoomScale = mapView.bounds.size.width / mapView.visibleMapRect.size.width;
    isSelectAnnotation = YES;

//    if (currentZoomScale > groupUserZoomLevel) {
        if ([view.annotation isKindOfClass:[PinAnnotation class]]) {
            // Selected the pin annotation.
            CalloutAnnotation *calloutAnnotation = [[CalloutAnnotation alloc] init];
            PinAnnotation *pinAnnotation = ((PinAnnotation *) view.annotation);
            calloutAnnotation.title = pinAnnotation.title;
            calloutAnnotation.coordinate = pinAnnotation.coordinate;
            pinAnnotation.calloutAnnotation = calloutAnnotation;
            [mapView addAnnotation:calloutAnnotation];
        }
//    }
}

//-----------------------------------------------------------------------------------------------------------------
// didDeselectAnnotationView:

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    //Hide ProfilesScrollview if unhide
    isSelectAnnotation = NO;

    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;
    selectedCombinatonArr = nil;

    if ([view.annotation isKindOfClass:[PinAnnotation class]]) {
        // Deselected the pin annotation.
        PinAnnotation *pinAnnotation = ((PinAnnotation *) view.annotation);
        [mapView removeAnnotation:pinAnnotation.calloutAnnotation];
        pinAnnotation.calloutAnnotation = nil;
    }
}

//-----------------------------------------------------------------------------------------------------------------
// didUpdateUserLocation:

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // Zoom to region containing the user location
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

#pragma mark
#pragma mark Delegate Callout Method
#pragma mark

// --------------------------------------------------------------------------------
// calloutButtonClicked:

- (void)calloutButtonClicked:(NSString *)title {
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// updatedLocation:

- (void)updatedLocation:(NSNotification *)inNotify {
    myLocation = self.server.myLocation;

    // Set Pin
    if (self.mapView != nil) {
        [self.mapView setCenterCoordinate:myLocation.coordinate animated:YES];
        MKCoordinateSpan span = MKCoordinateSpanMake(0.5, 0.0);
        MKCoordinateRegion region = MKCoordinateRegionMake(MKCoordinateForMapPoint(MKMapPointForCoordinate(myLocation.coordinate)), span);
        [self.mapView setRegion:region animated:YES];

        // Add my annotation
        if (myPinAnnotation == nil) {
            myPinAnnotation = [[PinAnnotation alloc] init];
            myPinAnnotation.coordinate = myLocation.coordinate;
            [self.mapView addAnnotation:myPinAnnotation];
        } else
            myPinAnnotation.coordinate = myLocation.coordinate;
    }
}

// --------------------------------------------------------------------------------
// imagesGotDownloaded:

- (void)imagesGotDownloaded:(NSNotification *)inNotify {
    // Remove all objects
    dotsUserPicsArr = [inNotify object];

    if (isZoomIn) {
        for (SRMapProfileView *profileView in userProfileViewArr) {
            NSArray *combinationArr = profileView.combinationsArr;
            NSDictionary *userDict = combinationArr[0];

            if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                    NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                    if ([imageName length] > 0) {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                        [profileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                    } else
                        profileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                } else
                    profileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
            } else
                profileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
            // If selected annotation
            if (selectedCombinatonArr) {
                [self addViewsOnScrollView:selectedCombinatonArr];
            }
        }
    }
}

@end
