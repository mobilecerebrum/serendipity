#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CalloutAnnotationView.h"
#import "PinAnnotation.h"
#import "SRProfileImageView.h"
#import "SRMapWindowView.h"
#import "SRMapProfileView.h"
#import "SRChatViewController.h"
#import "MKMapView+ZoomLevel.h"

@interface SRGroupUserMapView : UIView <CLLocationManagerDelegate, MKMapViewDelegate,UIGestureRecognizerDelegate,CalloutAnnotationViewDelegate,UIAlertViewDelegate>
{
    // Instance Variables
    NSMutableArray *nearByUsersList;
    NSMutableArray *userProfileViewArr;
    NSArray *selectedCombinatonArr;
    NSArray *dotsUserPicsArr;
    NSString *distanceUnit;
    CLLocation *myLocation;
    
    PinAnnotation *myPinAnnotation;
    
    // Flags
    BOOL isZoomIn;
    BOOL isSelectAnnotation;
    
    BOOL isCheckZoomLvl;
    BOOL isCountAdd;
    BOOL isPanoramaLoaded;
    IBOutlet UIButton *streetViewBtn,*zoomInBtn,*zoomOutBtn;
    CLLocationCoordinate2D panoramaLastLoc;
    IBOutlet UIButton *btnSatelite;
    IBOutlet UIButton *btnStreet;
  }

// Properties
@property (strong, nonatomic) IBOutlet UIToolbar *locationToolBar;
@property (nonatomic, strong) IBOutlet UIButton *btnSatelite,*btnStreet;
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIImageView *profileImg;

@property (strong, nonatomic) SRProfileImageView *imgUserProfile;
@property (nonatomic, strong) SRServerConnection *server;

// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (void)loadUsers:(NSArray *)inArr;

@end
