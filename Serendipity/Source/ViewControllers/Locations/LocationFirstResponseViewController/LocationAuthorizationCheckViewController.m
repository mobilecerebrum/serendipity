//
//  LocationAuthorizationCheckViewController.m
//  Serendipity
//
//  Created by Ivan Boldyrev on 12/21/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import "LocationAuthorizationCheckViewController.h"

@interface LocationAuthorizationCheckViewController ()

@end

@implementation LocationAuthorizationCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self getBottomSafeArea] > 0){
        self.imgAppBg.image = [UIImage imageNamed:@"AppBackground_Safe"];
    }else{
        self.imgAppBg.image = [UIImage imageNamed:@"AppBackground"];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeScreen)
                                                 name:@"locationServicesClose"
                                               object:nil];
}

- (void)closeScreen {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)openAppSettings {
    NSURL * url = [NSURL URLWithString:@"App-prefs:root=LOCATION_SERVICES"];
    
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url
                                           options:[NSDictionary new]
                                 completionHandler:nil];
    }
}

- (IBAction)actionOpenSystemSettings:(UIButton *)sender {
    [self openAppSettings];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"locationServicesClose" object:nil];
}

@end
