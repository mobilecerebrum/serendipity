//
//  LocationAuthorizationCheckViewController.h
//  Serendipity
//
//  Created by Ivan Boldyrev on 12/21/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationAuthorizationCheckViewController : ParentViewController

@property (strong,nonatomic) IBOutlet UIImageView *imgAppBg;


@end

NS_ASSUME_NONNULL_END
