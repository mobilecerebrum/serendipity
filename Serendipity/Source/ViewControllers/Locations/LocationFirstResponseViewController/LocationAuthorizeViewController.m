//
//  LocationAuthorizeViewController.m
//  Serendipity
//
//  Created by Ivan Boldyrev on 12/21/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "LocationAuthorizeViewController.h"
#import "LGAlertView.h"

@interface LocationAuthorizeViewController () <CLLocationManagerDelegate,LGAlertViewDelegate>
{
    BOOL actionButtonClicked;
    UITapGestureRecognizer *tapRecognizer;
}

@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation LocationAuthorizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self getBottomSafeArea] > 0){
        self.imgAppBg.image = [UIImage imageNamed:@"AppBackground_Safe"];
    }else{
        self.imgAppBg.image = [UIImage imageNamed:@"AppBackground"];
    }
    
    actionButtonClicked = NO;
    self.btnOpenSettings.layer.cornerRadius = 10;
    self.btnOpenSettings.clipsToBounds = true;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    //    if (@available(iOS 13.0, *)) {
    //        _lblInfo.text = @"Tap \"Only While Using App\"";
    //    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self showPopoverAlertAnimated:YES];
}

- (void)showPopoverAlertAnimated:(BOOL)animated {
    
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    
    
    if (@available(iOS 13.0, *)) {
        
        LGAlertView *alertView = [LGAlertView alertViewWithTitle:@"Allow Serendipity to acces your location?"
                                                         message:@"Your location will be shared with other users on Serendipity. You can stop it at anytime and customize with whom you would like to share your location with in Privacy Settings."
                                                           style:LGAlertViewStyleAlert
                                                    buttonTitles:@[@"Allow While Using App", @"Allow Once", @"Don't Allow"]
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:nil
                                                        delegate:self];
        
        
        [alertView setButtonPropertiesAtIndex:0 handler:^(LGAlertViewButtonProperties * _Nonnull properties) {
            [properties setBackgroundColorHighlighted:appTintColor];
            [properties setTitleColorHighlighted:[UIColor blackColor]];
            properties.backgroundColor = appTintColor;
            properties.titleColor = [UIColor blackColor];
        }];
        
        [alertView setButtonPropertiesAtIndex:1 handler:^(LGAlertViewButtonProperties * _Nonnull properties) {
            [properties setBackgroundColorHighlighted:[UIColor whiteColor]];
            [properties setTitleColorHighlighted:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
        }];
        
        
        [alertView setButtonPropertiesAtIndex:2 handler:^(LGAlertViewButtonProperties * _Nonnull properties) {
            [properties setBackgroundColorHighlighted:[UIColor whiteColor]];
            [properties setTitleColorHighlighted:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
        }];
                
        [alertView show];
    }else{
        
        LGAlertView *alertView = [LGAlertView alertViewWithTitle:@"Allow Serendipity to acces your location?"
                                                         message:@"Your location will be shared with other users on Serendipity. You can stop it at anytime and customize with whom you would like to share your location with in Privacy Settings."
                                                           style:LGAlertViewStyleAlert
                                                    buttonTitles:@[@"Allow While Using App", @"Always Allow", @"Don't Allow"]
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:nil
                                                        delegate:self];
        
        
        [alertView setButtonPropertiesAtIndex:1 handler:^(LGAlertViewButtonProperties * _Nonnull properties) {
            properties.backgroundColor = appTintColor;
            properties.titleColor = [UIColor blackColor];
        }];
        
        [alertView setButtonPropertiesAtIndex:0 handler:^(LGAlertViewButtonProperties * _Nonnull properties) {
            [properties setBackgroundColorHighlighted:[UIColor whiteColor]];
            [properties setTitleColorHighlighted:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
        }];
        
        
        [alertView setButtonPropertiesAtIndex:2 handler:^(LGAlertViewButtonProperties * _Nonnull properties) {
            [properties setBackgroundColorHighlighted:[UIColor whiteColor]];
            [properties setTitleColorHighlighted:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
        }];
        
        
        [alertView show];
    }
}
-(void) handleTapGesture{
    [self handleAfterConfirmation];
}

-(void) handleAfterConfirmation{
    actionButtonClicked = YES;
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted){
        [self showScreen];
    }else if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways) {
        [self completeTutorial];
    } else {
        [_lblInfo setHidden:YES];
        [self.locationManager requestAlwaysAuthorization];
        [self.locationManager startUpdatingLocation];
    }
}

- (void)showPopoverAlertError:(NSString *) title mesaageText:(NSString *) message actionText:(NSString *) action {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:action
                                                        style:UIAlertActionStyleCancel
                                                      handler:^(UIAlertAction * _Nonnull action) {
        [self showPopoverAlertAnimated:NO];
    }]];
    
    [self presentViewController:alertController animated:NO completion:nil];
}

-(void)completeTutorial {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"locationTutorialDone"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"appIsFirstResponse"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"locationTutorialDone" object:nil];
    [APP_DELEGATE showTutorial:self];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (actionButtonClicked){
        switch (status) {
            case kCLAuthorizationStatusDenied:
            case kCLAuthorizationStatusRestricted:
            case kCLAuthorizationStatusNotDetermined:{
            }break;
            case kCLAuthorizationStatusAuthorizedWhenInUse: {
                [self showScreen];
            } break;
                
            case kCLAuthorizationStatusAuthorizedAlways: {
                if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
                    [self showScreen];
                }else{
                    if (@available(iOS 13.0, *)) {
                      //  NSLog(@"No need to dissmiss for iOS 13. It will automatically dismiss from appdelegate");
                        [self showScreen];
                    }else{
                        [self completeTutorial];
                    }
                }
            } break;
                
            default:
                break;
        }
        
    }
}

-(void) showScreen{
    if ([self getBottomSafeArea] > 0){
        self.imgAppBg.image = [UIImage imageNamed:@"Image_Second"];
    }else{
        self.imgAppBg.image = [UIImage imageNamed:@"6P_Image_Second"];
    }
    [self.btnOpenSettings setHidden:NO];
    [self.lblInfo setHidden:YES];
}


- (IBAction)btnOpenLocationSettingsTap:(id)sender {
    actionButtonClicked = YES;
    NSURL* url = [[NSURL alloc]initWithString:UIApplicationOpenSettingsURLString];
    
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}


- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(NSString *)title {
    
    if (@available(iOS 13.0, *)) {
        if (index == 0){
            [self handleAfterConfirmation];
        }else{
            [self showPopoverAlertError:@"Choose \"Always Allow\" to use Serendipity"
                            mesaageText:@"Serendipity only works if it can \"Only While Using App\" access your location. Please tap on \"Only While Using App.\""
                             actionText:@"OK"];
        }
    }else{
        if (index == 1){
            [self handleAfterConfirmation];
        }else{
            [self showPopoverAlertError:@"Choose \"Always Allow\" to use Serendipity"
                            mesaageText:@"Serendipity only works if it can \"Only While Using App\" access your location. Please tap on \"Only While Using App.\""
                             actionText:@"OK"];
        }
    }
}

@end
