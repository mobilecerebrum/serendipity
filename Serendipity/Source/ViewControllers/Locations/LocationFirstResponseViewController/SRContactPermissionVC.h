//
//  SRContactPermissionVC.h
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "ParentViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRContactPermissionVC : ParentViewController
{
    SRServerConnection *server;
}


@property (weak, nonatomic) IBOutlet UIButton *btnConnectContact;

@property (weak) id delegate;
@property (weak) NSString* isForMenu;
@end


NS_ASSUME_NONNULL_END
