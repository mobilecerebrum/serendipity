//
//  LocationFirstResponseViewController.m
//  Serendipity
//
//  Created by Ivan Boldyrev on 12/21/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import "LocationFirstResponseViewController.h"

@interface LocationFirstResponseViewController ()

@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation LocationFirstResponseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self getBottomSafeArea] > 0){
        self.imgAppBg.image = [UIImage imageNamed:@"AppBackground_Safe"];
    }else{
        self.imgAppBg.image = [UIImage imageNamed:@"AppBackground"];
    }
    
    self.button.layer.cornerRadius = 10;
    self.button.clipsToBounds = YES;
}

@end
