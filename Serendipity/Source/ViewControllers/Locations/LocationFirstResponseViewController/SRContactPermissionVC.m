//
//  SRContactPermissionVC.m
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "SRContactPermissionVC.h"
#import "SRModalClass.h"
#import "PhoneConatactInviteStep1VC.h"
@interface SRContactPermissionVC ()
{
    BOOL accessGranted;
}
@end

@implementation SRContactPermissionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    server = (APP_DELEGATE).server;
     
    if ([_isForMenu isEqualToString:@"yes"]){
        UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:5];
        [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    }else{
    
    }
//
//
       UIButton *rightDoneButton = [SRModalClass setRightNavBarItem:@"Skip" barImage:nil forViewNavCon:self offset:10];
       
       [rightDoneButton addTarget:self action:@selector(actionOnSkip:) forControlEvents:UIControlEventTouchUpInside];
      self.btnConnectContact.layer.cornerRadius = 5.0;
    [self.btnConnectContact addTarget:self action:@selector(actionOnConnectContact:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidAppear:(BOOL)animated{
    [self checkContactPermission];
}

- (void)actionOnSkip:(UIButton *)sender {
    [self showAlertController:@"Are you sure you want to skip this step?" alertTitle:@"Alert"];
    
    
}
- (void)actionOnConnectContact:(UIButton *)sender {
    if (accessGranted) {
        PhoneConatactInviteStep1VC *objPhoneConatactInviteStep1View = [[PhoneConatactInviteStep1VC alloc] initWithNibName:nil bundle:nil inDict:nil server:server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objPhoneConatactInviteStep1View animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    }else{
        [self showForceContactAlert];
    }
}

-(void) checkContactPermission{
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
       // ABAddressBookRef addressBook = ABAddressBookCreate();


       if (&ABAddressBookRequestAccessWithCompletion != NULL) {
           // We are on iOS 6
           dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

           ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
               accessGranted = granted;
               dispatch_semaphore_signal(semaphore);
           });

           dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
       } else {
           // We are on iOS 5 or Older
           accessGranted = YES;
       }
       if (!accessGranted) {
           [self showForceContactAlert];

       }
}

#pragma mark Action Methods
- (void)actionOnBack:(UIButton *)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

-(void)showForceContactAlert{
    UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Permissions Required"
                                     message:@"You have forcefully denied some of the required permissions for this action. Please open settings, go to permissions and allow them."
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* skipButton = [UIAlertAction
                                 actionWithTitle:@"SKIP"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action) {
        [self actionOnSkip:[UIButton new]];
    }];
    
    UIAlertAction* settingButton = [UIAlertAction
    actionWithTitle:@"SETTINGS"
    style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {
        NSURL* url = [[NSURL alloc]initWithString:UIApplicationOpenSettingsURLString];
        
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        [self showForceContactAlert];
    }];
    
    [alert addAction:skipButton];
    [alert addAction:settingButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)showAlertController:(NSString *)message alertTitle :(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes I'm Sure"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
            [self showPopUpContactRefresh];
                                    }];
    UIAlertAction* noBtn = [UIAlertAction
    actionWithTitle:@"No"
    style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {
        
    }];
    [alert addAction:yesButton];
    [alert addAction:noBtn];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showPopUpContactRefresh{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"If you wish to import your phone contacts later, just go to the Main Menu and click on     next to the word Contacts.\n\n"];

    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = [UIImage imageNamed:@"icons-8-available-updates-filled-100"];
    textAttachment.bounds = CGRectMake(0.0,-8,textAttachment.image.size.width,textAttachment.image.size.height);
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    [attributedString replaceCharactersInRange:NSMakeRange(88, 1) withAttributedString:attrStringWithImage];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0, SCREEN_WIDTH-40, 300)];
    [label setAttributedText:attributedString];
    [label setNumberOfLines:0];
    [label setTextAlignment: NSTextAlignmentCenter];
    [label sizeToFit];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    alert.tag = 1;
    [alert setValue:label forKey:@"accessoryView"];
    [alert show];
}
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setValue:@"yes" forKey:kKeyIsImportPhoneContact];
        [userDefault synchronize];
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
            [APP_DELEGATE setInitialController];
        }];
    }
}

@end
