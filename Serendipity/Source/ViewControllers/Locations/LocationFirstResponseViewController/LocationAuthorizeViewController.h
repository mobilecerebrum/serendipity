//
//  LocationAuthorizeViewController.h
//  Serendipity
//
//  Created by Ivan Boldyrev on 12/21/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationAuthorizeViewController : ParentViewController

@property (strong,nonatomic) IBOutlet UILabel *lblInfo;
@property (strong,nonatomic) IBOutlet UIImageView *imgAppBg;
@property (strong,nonatomic) IBOutlet UIButton *btnOpenSettings;

@end

NS_ASSUME_NONNULL_END
