//
//  LocationEnableViewController.m
//  Serendipity
//
//  Created by Ivan Boldyrev on 12/21/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import "LocationEnableViewController.h"

@interface LocationEnableViewController ()

@end

@implementation LocationEnableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(close)
                                                 name:@"locationServicesClose"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self close];
}

- (IBAction)actionButton:(id)sender {
    [self openAppSettings];
}

- (void)openAppSettings {
    NSURL * url = [NSURL URLWithString:@"App-prefs:root=LOCATION_SERVICES"];
    
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url
                                           options:[NSDictionary new]
                                 completionHandler:nil];
    }
}

- (void)close {
    if ([CLLocationManager locationServicesEnabled]) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"locationServicesClose" object:nil];
}

@end
