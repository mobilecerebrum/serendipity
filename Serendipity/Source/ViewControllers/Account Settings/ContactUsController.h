//
//  ContactUsController.h
//  Serendipity
//
//  Created by Madhura on 14/12/17.
//  Copyright © 2017 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface ContactUsController : ParentViewController<UITextViewDelegate>
{
    SRServerConnection *server;

}
@property (strong, nonatomic) IBOutlet SRProfileImageView *BackGroundPhotoImage;
@property (weak, nonatomic) IBOutlet UIView *viewSubject;
@property (strong, nonatomic) IBOutlet UIImageView *BackGroundColorImage;
@property (strong, nonatomic) IBOutlet UIImageView *appLogoImage;
@property (strong, nonatomic) IBOutlet UITextView *txtSubject, *txtMessage;
@property(strong,nonatomic)IBOutlet UIButton *btnSubmit;
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

-(IBAction)btnSubmitClicked:(id)sender;
@end
