#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface SRAccountSettingsViewController : ParentViewController<UIActionSheetDelegate,UIAlertViewDelegate>
{
	//instance Vaiables
	NSMutableArray *menuItemArray;
    UIActionSheet *milesSheet,*metersSheet;
    NSString *milesData ,*metersData;
    //Server
    SRServerConnection *server;
    NSString *index;
    NSDictionary *payload;
}

// Properties
@property (strong, nonatomic) IBOutlet UITableView *objtableView;
@property (strong, nonatomic) IBOutlet UIButton *btnDeleteAccount;
@property (strong, nonatomic) IBOutlet SRProfileImageView *BackGroundPhotoImage;
@property (strong, nonatomic) IBOutlet UIImageView *BackGroundColorImage;
@property (strong, nonatomic) IBOutlet UIImageView *appLogoImage;

// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark Action Method
#pragma mark

- (IBAction)btnDeleteAccountClick:(id)sender;

@end
