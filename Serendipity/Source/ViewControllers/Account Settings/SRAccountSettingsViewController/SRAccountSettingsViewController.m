#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRAlertSettingViewController.h"
#import "SRTerms&ConditionsViewController.h"
#import "SRManagePersistentConnectionsViewController.h"
#import "SRChangePhoneNumberViewController.h"
#import "SRLoginViewController.h"
#import "SRAccountSettingsViewController.h"
#import "SRDeleteAccountViewController.h"
#import "SRAllowPingsViewController.h"
#import "SRDiscoveryRadarViewController.h"
#import "ContactUsController.h"

@implementation SRAccountSettingsViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRAccountSettingsViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(logoutFailed:)
                              name:kKeyNotificationLogoutFailed
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateSucceed:)
                              name:kKeyNotificationSettingUpdateSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateFailed:)
                              name:kKeyNotificationSettingUpdateFailed object:nil];
    }
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//-------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // call Super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.AccountSettings", " ") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    //left button on nav bar
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    // Initializing item array for table

    menuItemArray = [@[NSLocalizedString(@"lbl.Allow Pings.txt", @""), NSLocalizedString(@"lbl.Alert_setting.txt", @""), NSLocalizedString(@"lbl.intelligent_alert.txt", @""), NSLocalizedString(@"lbl.Open To Dating.txt", @""), NSLocalizedString(@"lbl.Manage Connections.txt", @""), NSLocalizedString(@"lbl.Measurement_sysytem.txt", @""), NSLocalizedString(@"lbl.System Status.txt", @""), NSLocalizedString(@"lbl.Change Phone Number.txt", @""), NSLocalizedString(@"lbl.contact_us.txt", @""), NSLocalizedString(@"lbl.Logout.txt", @"")] mutableCopy];


    // Set Footer to Tableview
    self.objtableView.tableFooterView = [[UIView alloc] init];
    if (SCREEN_HEIGHT == 480) {
        self.objtableView.scrollEnabled = YES;
    } else
        self.objtableView.scrollEnabled = NO;

    //Set Default Miles distance
    milesData = @"3000 miles";
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMetersData = [userdef valueForKey:kKeyMeters_Data];
    if (strMetersData.length > 0) {
        metersData = strMetersData;
    } else
        metersData = @"0 M";
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// -----------------------------------------------------------------------------------
// setRadarMilesData:

- (void)setRadarMilesData:(NSString *)radiusMilesData {

    NSString *value = radiusMilesData;

    NSDictionary *payload = @{kKeyFieldName: kKeyRadius,
            kKeyFieldValue: value,
    };

    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
}
// -----------------------------------------------------------------------------------
// Update user distance from current location:

- (void)updateDistanceData:(NSString *)meterData {

    NSArray *strArray = [meterData componentsSeparatedByString:@" "];
    NSString *distanceFilter = strArray[0];
    NSDictionary *payload = @{
            kKeyFieldName: kkeyDistanceAccuracy,
            kKeyFieldValue: distanceFilter
    };

    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];

}

- (void)actionOnBack:(id)sender {

    if (self.delegate == nil) {
        (APP_DELEGATE).accountSettingVCFlag = YES;
        [self.navigationController popViewControllerAnimated:NO];
    } else {
        [self.navigationController popToViewController:self.delegate animated:NO];
    }
}

- (IBAction)btnDeleteAccountClick:(id)sender {
    SRDeleteAccountViewController *deleteAcc = [[SRDeleteAccountViewController alloc] initWithNibName:@"SRDeleteAccountViewController" bundle:nil server:server];
    [self.navigationController pushViewController:deleteAcc animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 2) {
        if (buttonIndex == 1) {
            (APP_DELEGATE).isLogoutCall = YES;
            // Call api for logout
            [APP_DELEGATE showActivityIndicator];
            [server makeAsychronousRequest:kKeyClassLogout inParams:nil isIndicatorRequired:NO inMethodType:kGET];
        }
    }
}

- (void)changeSwitch:(UISwitch *)sender {
    NSString *value = @"";
    NSString *name = @"";

    if ([sender isOn]) {

        value = @"1";
        if (sender.tag == 2) {
            name = @"intelligent_alerts";
        } else if (sender.tag == 3) {
            name = @"open_for_dating";
        } else if (sender.tag == 5) {
            value = @"0";
            name = @"measurement_unit";
        }

    } else {

        value = @"0";
        if (sender.tag == 2) {
            name = @"intelligent_alerts";
        } else if (sender.tag == 3) {
            name = @"open_for_dating";
        } else if (sender.tag == 5) {
            value = @"1";
            name = @"measurement_unit";
        }
    }

    payload = @{@"field_name": name,
            @"field_value": value
    };
    [APP_DELEGATE showActivityIndicator];
    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:[NSString stringWithFormat:@"%i",!sender.on] forKey:kkeyMeasurementUnit];
    [self.objtableView reloadData];
}

#pragma mark TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Scetions
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Rows counts
    return [menuItemArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *) nibObjects[0];
            cell = customCell;
        }
    }

    //Set Selection Style None
    ((SRHomeMenuCustomCell *) cell).selectionStyle = UITableViewCellSelectionStyleNone;

    // Manage subviews hide and unhide
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.hidden = NO;
    //((CustomCell *)cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuIconImage.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;

    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.cornerRadius = ((SRHomeMenuCustomCell *) cell).MenuIconImage.frame.size.width / 3;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.masksToBounds = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.clipsToBounds = YES;

    if (indexPath.row == 2 || indexPath.row == 3) {

        UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 54, 4, 60, 25)];
        objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
        objSwitch.tintColor = [UIColor whiteColor];
        [objSwitch setOnTintColor:[UIColor orangeColor]];
        objSwitch.backgroundColor = [UIColor lightGrayColor];

        NSString *openForDate = [[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kKeyOpenForDating];
        NSString *intelligentAlert = [[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kKeyIntelligentAlert];

        if (indexPath.row == 2) {
            UILabel *alertLbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, SCREEN_WIDTH - 20, 40)];
            alertLbl.numberOfLines = 3;
            alertLbl.lineBreakMode = NSLineBreakByWordWrapping;
            alertLbl.textColor = [UIColor whiteColor];
            alertLbl.font = [UIFont fontWithName:kFontHelveticaBold size:10];
            alertLbl.text = NSLocalizedString(@"lbl.intelligent_Alert.txt", @"");
            [((SRHomeMenuCustomCell *) cell).contentView addSubview:alertLbl];

            if (intelligentAlert != nil) {
                if ([intelligentAlert isEqualToString:@"1"]) {
                    [objSwitch setOn:YES];
                } else
                    [objSwitch setOn:NO];
            } else
                [objSwitch setOn:NO];
            objSwitch.layer.cornerRadius = 16.0f;
            objSwitch.tag = indexPath.row;
            [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
            [((SRHomeMenuCustomCell *) cell) addSubview:objSwitch];

        }

        if (indexPath.row == 3) {
            if (openForDate != nil) {
                if ([openForDate isEqualToString:@"1"]) {
                    [objSwitch setOn:YES];
                } else
                    [objSwitch setOn:NO];
            } else
                [objSwitch setOn:NO];

            objSwitch.layer.cornerRadius = 16.0f;
            objSwitch.tag = indexPath.row;
            [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
            [((SRHomeMenuCustomCell *) cell) addSubview:objSwitch];

            //Manage open dating accordingly
            if ([APP_DELEGATE isUserBelow18]) {
                ((SRHomeMenuCustomCell *) cell).MenuItemLabel.hidden = YES;
                objSwitch.hidden = YES;
            }
        }
    } else if (indexPath.row == 5) {
        // lbl imperial
        UILabel *imperialLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 150, 10, 50, 15)];
        imperialLbl.textColor = [UIColor blackColor];
        imperialLbl.font = [UIFont fontWithName:kFontHelveticaBold size:12];
        imperialLbl.text = @"Imperial";
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:imperialLbl];
        
        
        //Metric lbl
        UILabel *metricLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 45, 10, 40, 15)];
        metricLbl.textColor = [UIColor blackColor];
        metricLbl.font = [UIFont fontWithName:kFontHelveticaBold size:12];
        metricLbl.text = @"Metric";
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:metricLbl];
        
        

        // Swich
        NSString *measurement_unit = [[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit];
        UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 100, 4, 60, 25)];
        if (measurement_unit != nil) {
            if ([measurement_unit isEqualToString:@"1"]) {
                [objSwitch setOn:NO];
                imperialLbl.textColor = [UIColor whiteColor];
            } else{
                [objSwitch setOn:YES];
                metricLbl.textColor = [UIColor whiteColor];
            }
        } else {
            [objSwitch setOn:NO];
            imperialLbl.textColor = [UIColor whiteColor];
        }
            
        objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
        objSwitch.tintColor = [UIColor orangeColor];
        [objSwitch setOnTintColor:[UIColor orangeColor]];
        objSwitch.backgroundColor = [UIColor orangeColor];
        objSwitch.layer.cornerRadius = 16.0f;
        objSwitch.tag = indexPath.row;
        [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        [((SRHomeMenuCustomCell *) cell) addSubview:objSwitch];
    }

    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame = CGRectMake(20, 10, 300, 15);
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.font = [UIFont fontWithName:kFontHelveticaRegular size:13.0];
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.text = menuItemArray[indexPath.row];
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.translatesAutoresizingMaskIntoConstraints = true;
    ((SRHomeMenuCustomCell *) cell).backgroundColor = [UIColor clearColor];

    return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        NSArray *titleArray = @[NSLocalizedString(@"lbl.Everyone.txt", @""), NSLocalizedString(@"lbl.MyContacts.txt", @""), NSLocalizedString(@"lbl.MyConnections.txt", @""), NSLocalizedString(@"lbl.MyFavConnections.txt", @""), NSLocalizedString(@"lbl.Nobody.txt", @"")];

        SRAllowPingsViewController *objSRAllowPingsVC = [[SRAllowPingsViewController alloc] initWithNibName:NSLocalizedString(@"lbl.Allow Pings.txt", @"") bundle:nil menuArray:titleArray server:server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRAllowPingsVC animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 1) {
        SRAlertSettingViewController *objSRAlertSettingVC = [[SRAlertSettingViewController alloc] initWithNibName:nil bundle:nil menuArray:nil server:server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRAlertSettingVC animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 4) {
        SRManagePersistentConnectionsViewController *objSRManagePersistentConnections = [[SRManagePersistentConnectionsViewController alloc] initWithNibName:nil bundle:nil server:server];
        objSRManagePersistentConnections.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRManagePersistentConnections animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 6) {
        NSURL *twitterURL = [NSURL URLWithString:@"https://twitter.com/serendipity1on1"];
        if ([[UIApplication sharedApplication] canOpenURL:twitterURL])
            [[UIApplication sharedApplication] openURL:twitterURL options:@{} completionHandler:nil];
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"] options:@{} completionHandler:nil];
    } else if (indexPath.row == 7) {
        SRChangePhoneNumberViewController *objSRChangePhoneNumber = [[SRChangePhoneNumberViewController alloc] initWithNibName:nil bundle:nil server:server];
        objSRChangePhoneNumber.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRChangePhoneNumber animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 8) {
        ContactUsController *objContactUS = [[ContactUsController alloc] initWithNibName:nil bundle:nil server:server];

        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objContactUS animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 9) {
        UIAlertView *logoutAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:NSLocalizedString(@"alert.logout.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.no.text", @"") otherButtonTitles:NSLocalizedString(@"alert.button.yes.text", @""), nil];
        logoutAlert.tag = 2;

        [logoutAlert show];
    }
}

// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return
    if (indexPath.row == 2) {
        return 70;
    } else if (indexPath.row == 3 && [APP_DELEGATE isUserBelow18]) {
        return 0;
    }
    return 38;
}

#pragma mark
#pragma mark ActionSheet Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet == milesSheet) {
        if (!([[milesSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"])) {
            milesData = [milesSheet buttonTitleAtIndex:buttonIndex];
            [self setLocationInMiles:milesData];
        }
    } else {
        if (!([[metersSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"])) {
            metersData = [metersSheet buttonTitleAtIndex:buttonIndex];
            [self setLocationInMeters:metersData];
        }
    }
}

// ----------------------------------------------------------------------------------------------------------------------
// setLocationInMiles:
- (void)setLocationInMiles:(NSString *)miles {
    milesData = miles;
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setValue:milesData forKey:kKeyMiles_Data];

    [userdef synchronize];

    //Reload tableview cell
    NSIndexPath *rowToReload = [NSIndexPath indexPathForRow:4 inSection:0];
    NSArray *rowsToReload = @[rowToReload];
    [self.objtableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationAutomatic];

    [self setRadarMilesData:milesData];
}

// ----------------------------------------------------------------------------------------------------------------------
// setLocationInMeters:
- (void)setLocationInMeters:(NSString *)meters {
    metersData = meters;
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    [userdef setValue:metersData forKey:kKeyMeters_Data];

    [userdef synchronize];

    //Reload tableview cell
    NSIndexPath *rowToReload = [NSIndexPath indexPathForRow:9 inSection:0];
    NSArray *rowsToReload = @[rowToReload];
    [self.objtableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationAutomatic];

    [self updateDistanceData:metersData];
}

#pragma mark
#pragma mark Notifications Methods
#pragma mark
// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:


- (void)settingUpdateSucceed:(NSNotification *)inNotify {
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSString *allowPings = [response valueForKey:kKeyallowPings];
    NSString *pushNotifications = [response valueForKey:kKeyPushnotification];
    NSString *openForDating = [response valueForKey:kKeyOpenForDating];
    NSString *distance = [response valueForKey:kkeyDistanceAccuracy];
    NSString *measurement_unit = [response valueForKey:kkeyMeasurementUnit];
    NSString *intelligentAlerts = [response valueForKey:kKeyIntelligentAlert];

    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:allowPings forKey:kKeyallowPings];
    [userDef setValue:pushNotifications forKey:kKeyPushnotification];
    [userDef setValue:openForDating forKey:kKeyOpenForDating];
    [userDef setValue:distance forKey:kkeyDistanceAccuracy];
    [userDef setValue:measurement_unit forKey:kkeyMeasurementUnit];
    [userDef setValue:intelligentAlerts forKey:kKeyIntelligentAlert];
    [userDef synchronize];

    // Update setting in logged in info
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:allowPings forKey:kKeyallowPings];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:openForDating forKey:kKeyOpenForDating];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:measurement_unit forKey:kkeyMeasurementUnit];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:intelligentAlerts forKey:kKeyIntelligentAlert];
    SRDiscoveryRadarViewController *objRadar = [[SRDiscoveryRadarViewController alloc] init];
    if ([[payload valueForKey:@"field_name"] isEqualToString:@"measurement_unit"]) {
        (APP_DELEGATE).isMeasurementChangesForRadar = true;
        (APP_DELEGATE).isMeasurementChangesForMap = true;
        if ([[response valueForKey:kkeyMeasurementUnit] boolValue]) {//Miles
            if ((APP_DELEGATE).oldDataArray.count != 0) {
                for (int i = 0; i < (APP_DELEGATE).oldDataArray.count; i++) {
                    NSDictionary *userDict = (APP_DELEGATE).oldDataArray[i];
                    if ([[userDict valueForKey:@"dataId"] isEqualToString:@"68"]) {
                        NSLog(@"%f", [[userDict valueForKey:kKeyDistance] floatValue]);
                    }
                    float distance = [[userDict valueForKey:kKeyDistance] floatValue];
                    distance = distance * 0.621371;
                    float speed = [[[[userDict valueForKey:@"_userDict"] valueForKey:kKeyLocation] valueForKey:kKeySpeed] floatValue];
                    speed = speed * 0.621371;
                    if ([[userDict valueForKey:@"dataId"] isEqualToString:@"68"]) {
                        NSLog(@"%f", distance);
                    }
                    [userDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
                    NSDictionary *dict = [userDict valueForKey:@"_userDict"];
                    NSDictionary *dictLoc = [dict valueForKey:kKeyLocation];
                    [dictLoc setValue:[NSString stringWithFormat:@"%.2f", speed] forKey:kKeySpeed];
                    [dict setValue:dictLoc forKey:kKeyLocation];
                    [userDict setValue:dict forKey:@"_userDict"];
                    (APP_DELEGATE).oldDataArray[i] = userDict;
                }
                for (int i = 0; i < (APP_DELEGATE).oldSourceArray.count; i++) {
                    NSDictionary *userDict = (APP_DELEGATE).oldSourceArray[i];
                    float distance = [[userDict valueForKey:kKeyDistance] floatValue];
                    distance = distance * 0.621371;
                    float speed = [[[userDict valueForKey:kKeyLocation] valueForKey:kKeySpeed] floatValue];
                    speed = speed * 0.621371;
                    NSDictionary *dictLoc = [userDict valueForKey:kKeyLocation];
                    [dictLoc setValue:[NSString stringWithFormat:@"%.2f", speed] forKey:kKeySpeed];
                    [userDict setValue:dictLoc forKey:kKeyLocation];
                    [userDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
                    (APP_DELEGATE).oldSourceArray[i] = userDict;
                }
                [objRadar PlotUsers];
            }
        } else {//Km
            if ((APP_DELEGATE).oldDataArray.count != 0) {
                for (int i = 0; i < (APP_DELEGATE).oldDataArray.count; i++) {
                    NSDictionary *userDict = (APP_DELEGATE).oldDataArray[i];
                    if ([[userDict valueForKey:@"dataId"] isEqualToString:@"68"]) {
                        NSLog(@"%f", [[userDict valueForKey:kKeyDistance] floatValue]);
                    }
                    float distance = [[userDict valueForKey:kKeyDistance] floatValue];
                    distance = distance * 1.60934;
                    float speed = [[[[userDict valueForKey:@"_userDict"] valueForKey:kKeyLocation] valueForKey:kKeySpeed] floatValue];
                    speed = speed * 1.60934;
                    if ([[userDict valueForKey:@"dataId"] isEqualToString:@"157"]) {
                        NSLog(@"%f", distance);
                    }
                    [userDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
                    NSDictionary *dict = [userDict valueForKey:@"_userDict"];
                    NSDictionary *dictLoc = [dict valueForKey:kKeyLocation];
                    [dictLoc setValue:[NSString stringWithFormat:@"%.2f", speed] forKey:kKeySpeed];
                    [dict setValue:dictLoc forKey:kKeyLocation];
                    [userDict setValue:dict forKey:@"_userDict"];
                    (APP_DELEGATE).oldDataArray[i] = userDict;
                }
                for (int i = 0; i < (APP_DELEGATE).oldSourceArray.count; i++) {
                    NSDictionary *userDict = (APP_DELEGATE).oldSourceArray[i];
                    float distance = [[userDict valueForKey:kKeyDistance] floatValue];
                    distance = distance * 1.60934;
                    float speed = [[[userDict valueForKey:kKeyLocation] valueForKey:kKeySpeed] floatValue];
                    speed = speed * 1.60934;
                    [userDict setValue:[NSString stringWithFormat:@"%.2f", speed] forKey:[[userDict valueForKey:kKeyLocation] valueForKey:kKeySpeed]];
                    [userDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
                    (APP_DELEGATE).oldSourceArray[i] = userDict;
                }
                [objRadar PlotUsers];
            }
        }
    }
    [self.objtableView reloadData];
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {

}

// ----------------------------------------------------------------------------------------------------------------------
// logoutFailed:


- (void)logoutFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:@"Your attempt to log out has failed. Please try again."];
}


@end
