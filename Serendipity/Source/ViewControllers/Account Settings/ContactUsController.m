//
//  ContactUsController.m
//  Serendipity
//
//  Created by Madhura on 14/12/17.
//  Copyright © 2017 Pragati Dubey. All rights reserved.
//

#import "ContactUsController.h"
#import "SRModalClass.h"

@interface ContactUsController ()

@end

@implementation ContactUsController


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"ContactUsController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(mailSentSucceded:)
                              name:kKeyNotificationMailSendSuccess
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(mailSentFailure:)
                              name:KkeyNotificationMailFailed object:nil];
        
    }
    
    // Return
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [super viewDidLoad];
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    self.txtMessage.layer.borderWidth = 1.0f;
    self.txtMessage.layer.borderColor = [[UIColor blackColor] CGColor];
    self.viewSubject.layer.borderWidth = 1.0f;
    self.viewSubject.layer.borderColor = [[UIColor blackColor] CGColor];
    //self.txtSubject.delegate = self;
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.ContactUs", " ") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    //left button on nav bar
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view from its nib.
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if ([self.txtMessage.text isEqualToString:@"Message"]){
        self.txtMessage.text = @"";
        self.txtMessage.textColor = [UIColor lightGrayColor];
        return YES;
    }
    else{
        self.txtMessage.textColor = [UIColor blackColor];
        return YES;
    }
}

-(void) textViewDidChange:(UITextView *)textView
{
    [self checkEnableDissableSubmit];
    if(self.txtMessage.text.length == 0){
        self.txtMessage.textColor = [UIColor lightGrayColor];
     //   self.txtMessage.text = @"Message";
    }
    else{
        self.txtMessage.textColor = [UIColor blackColor];
    }
}

-(BOOL) textViewShouldEndEditing:(UITextView *)textView
{
    [self checkEnableDissableSubmit];
    if(self.txtMessage.text.length == 0)
    {
        self.txtMessage.textColor = [UIColor lightGrayColor];
        self.txtMessage.text = @"Message";
        [self.txtMessage resignFirstResponder];
        return YES;
    }
    else
    {
        self.txtMessage.textColor = [UIColor blackColor];
        return YES;
    }
    
}

-(void)checkEnableDissableSubmit {
    if ((self.txtMessage.text.length == 0) || (self.txtSubject.text.length == 0) || [self.txtMessage.text isEqualToString:@"Message"]){
        [self.btnSubmit setEnabled:NO];
        self.btnSubmit.backgroundColor=[UIColor lightGrayColor];
    }else{
        [self.btnSubmit setEnabled:YES];
        self.btnSubmit.backgroundColor=[UIColor colorWithRed:255/255.0 green:171/255.0 blue:64/255.0 alpha:1];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(IBAction)btnSubmitClicked:(id)sender
{
    if ([_txtMessage.text isEqualToString:@""] && [_txtSubject.text isEqualToString:@""])
    {
        [SRModalClass showAlert:NSLocalizedString(@"alert.empty_mail.txt", @"")];
    }
    else if([_txtMessage.text isEqualToString:@""])
    {
        [SRModalClass showAlert:NSLocalizedString(@"alert.message_content_notavailable.txt", @"")];
    }
    else if([_txtSubject.text isEqualToString:@""])
    {
        [SRModalClass showAlert:NSLocalizedString(@"alert.mail_subject_missing.txt", @"")];
    }
    else
    {
        NSDictionary *payload = @{@"subject":_txtSubject.text,
                                  @"message":_txtMessage.text
        };
        NSString *url = [NSString stringWithFormat:@"%@",kKeyClassContactUs];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:YES inMethodType:kPOST];
    }
}

-(void)mailSentSucceded:(NSNotification *)inNotify
{
    _txtSubject.text = @"Subject:";
    _txtMessage.text = @"Message";
    _txtMessage.textColor = [UIColor lightGrayColor];
    _txtSubject.textColor = [UIColor lightGrayColor];
    [(APP_DELEGATE) hideActivityIndicator];
    //[SRModalClass showAlert:NSLocalizedString(@"alert.mail_sent.txt", @"")];
    [SRModalClass showAlertWithTitle:@"Your message was sent successfully" alertTitle:@"Thank you"];

}

-(void)mailSentFailure:(NSNotification *)inNotify
{
    [(APP_DELEGATE) hideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.mail_Fail.txt", @"")];
}

@end
