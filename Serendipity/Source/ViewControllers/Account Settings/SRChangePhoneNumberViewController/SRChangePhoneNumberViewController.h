#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRChangePhoneNumberViewController2.h"
#import "SRProfileImageView.h"

@interface SRChangePhoneNumberViewController : ParentViewController
{
    //Instance Varialbel
    
    //Server
    SRServerConnection *server;
    
}
//Properties
@property (weak, nonatomic) IBOutlet SRProfileImageView *screenBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *screenBackgroundColorImage;
@property (weak, nonatomic) IBOutlet UIImageView *appLogo;
@property (weak, nonatomic) IBOutlet UITextView *notificationTextView;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImage;
@property (strong, nonatomic) IBOutlet UILabel *info1;
@property (strong, nonatomic) IBOutlet UILabel *info2;
//Other Properties
@property(weak)id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
