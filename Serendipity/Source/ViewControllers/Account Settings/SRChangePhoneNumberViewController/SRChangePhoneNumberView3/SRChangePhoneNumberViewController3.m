
#import "SRModalClass.h"
#import "SRServerConnection.h"
#import "SRMenuViewController.h"

#import "SRChangePhoneNumberViewController3.h"

@interface SRChangePhoneNumberViewController3 ()

@end

@implementation SRChangePhoneNumberViewController3
@synthesize phoneNumber;


#pragma mark
#pragma mark Init Method
#pragma mark
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
                 data:(NSDictionary *)inData {
    // Call super
    self = [super initWithNibName:@"SRChangePhoneNumberViewController3" bundle:nil];
    
    if (self) {
        // Initialization
        server = inServer;
        signUpData = inData;
        
        // Notification register
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getUserDetailsSucceed:)
                              name:kKeyNotificationCompleteSignUPSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getUserDetailsFailed:)
                              name:kKeyNotificationCompleteSignUPFailed object:nil];
        
    }
    
    
    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    
    // Title
    [SRModalClass setNavTitle:phoneNumber forViewNavCon:self.navigationItem forFontSize:15.0];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:@"Cancel" barImage:nil forViewNavCon:self offset:15];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    // Set gesture to dismiss keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];
    
    [self postUserPhoneNoDetails];
    
    
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// -----------------------------------------------------------------------------------
// postUserPhoneNoDetails:

-(void)postUserPhoneNoDetails
{
    
    NSDictionary *params = @{kKeyMobileNumber:[NSString stringWithFormat:@"%@",phoneNumber],
                        @"_method":kPUT
                            };
   NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, [server.loggedInUserInfo objectForKey:kKeyId]];
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:NO inMethodType:kPOST];
    
}

// -----------------------------------------------------------------------------------
// nextBtnAction:

- (void)actionOnBack:(id)sender {
  //[self dismissViewControllerAnimated:YES completion:nil];
    
    //Pop to main menu controller screen

    SRMenuViewController *objMenu = [[SRMenuViewController alloc] init];
    [self.navigationController pushViewController:objMenu animated:NO];
    
    
}



#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFldCode) {
        [self.txtFldCode resignFirstResponder];
        [self.scrollView setContentOffset:CGPointMake(0, 0)];
    }
    
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    if (sender == self.txtFldCode) {
        [self.scrollView setContentOffset:CGPointMake(0, 0)];
    }
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)    textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
    replacementString:(NSString *)string {
    BOOL isValid = YES;
    
    NSString *errorMsg = nil;
    NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];
    if (textField == self.txtFldCode) {
        if (textField == self.txtFldCode && [textStrToCheck length] > 6) {
            errorMsg = NSLocalizedString(@"alert.code.txt", @"");
        }
    }
    // Set flag and show alert
    if (errorMsg) {
        isValid = NO;
        [SRModalClass showAlert:errorMsg];
    }
    
    // Return
    return isValid;
}

#pragma mark
#pragma mark Private Method
#pragma mark
// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
    // End editing
    [self.view endEditing:YES];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}


#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:
- (void)getUserDetailsSucceed:(NSNotification *)inNotify {
    // Display Data
    
}

// --------------------------------------------------------------------------------
// getUserDetailsFailed:
- (void)getUserDetailsFailed:(NSNotification *)inNotify {
    // Display Data
    
}
@end
