
#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface SRChangePhoneNumberViewController3 : ParentViewController
{
    // Instance variable
    NSDictionary *signUpData;
    
    // Server
    SRServerConnection *server;
}
@property(strong,nonatomic)NSString *phoneNumber;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextView *txtFirstMsg;
@property (strong, nonatomic) IBOutlet UITextView *txtSecondMsg;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode;
@property (strong, nonatomic) IBOutlet UILabel *lblFldCode;
@property (weak, nonatomic) IBOutlet UIImageView *imgAppLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imgBGColorImage;
@property (weak, nonatomic) IBOutlet SRProfileImageView *imgBGScreenImage;

@property(weak)id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
                 data:(NSDictionary *)inData ;
@end
