
#import "SRChangePhoneNumberViewController.h"

@implementation SRChangePhoneNumberViewController

#pragma mark
#pragma mark Init Method
#pragma mark
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
{
    self = [super initWithNibName:@"SRChangePhoneNumberViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
    }
    return self;
}


#pragma mark
#pragma mark Standard Methods
#pragma mark

// -----------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.ChangePhoneNumber", " ") forViewNavCon:self.navigationItem forFontSize:15.0];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:@"Cancel" barImage:nil forViewNavCon:self offset:15];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:@"Next" barImage:nil forViewNavCon:self offset:-10];
    [rightButton addTarget:self action:@selector(nextBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    self.info1.text = NSLocalizedString(@"lbl.changeNumber_info", @"");
    self.info2.text = NSLocalizedString(@"lbl.changeNumber_information", @"");
    
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// -----------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
    
}

// -----------------------------------------------------------------------------------
// nextBtnAction:

- (void)nextBtnAction:(id)sender {
    SRChangePhoneNumberViewController2 *objSRChangePhoneNumberView2=[[SRChangePhoneNumberViewController2 alloc] initWithNibName:nil bundle:nil server:server];
    
   [self.navigationController pushViewController:objSRChangePhoneNumberView2 animated:YES];
}




@end
