#import "SRChangePhoneNumberViewController2.h"
#import "LNNumberpad.h"
#import "SRCountryPickerViewController.h"

@implementation SRChangePhoneNumberViewController2

#pragma mark
#pragma mark Init Method
#pragma mark

//-----------------------------------------------------------------------------------
//initWithNibName

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
	self = [super initWithNibName:@"SRChangePhoneNumberViewController2" bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
        server = inServer;
	}
    
    // Register Notifications
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(signUPUserSucceed:)
                          name:kKeyNotificationSignUPSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(signUPUserFailed:)
                          name:kKeyNotificationCompleteSignUPFailed
                        object:nil];

	return self;
}

#pragma mark
#pragma mark Standard Method
#pragma mark

//-----------------------------------------------------------------------------------
//viewDidLoad

- (void)viewDidLoad {
	[super viewDidLoad];
	// Set navigation bar
	self.navigationController.navigationBar.hidden = NO;
	self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
	self.navigationController.navigationBar.translucent = NO;

	// Title
	[SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.ChangePhoneNumber", " ") forViewNavCon:self.navigationItem forFontSize:15.0];

	UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:15];
	[leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
	UIButton *rightButton = [SRModalClass setRightNavBarItem:@"Done" barImage:nil forViewNavCon:self offset:-10];
	[rightButton addTarget:self action:@selector(actionOnDoneBack:) forControlEvents:UIControlEventTouchUpInside];

	self.imgVerificationBG.hidden = YES;
	self.activityIndicator.hidden = YES;
	self.lblVerifying.hidden = YES;
    

    self.txtNewPhoneNumber.inputView = self.keyPad;
    NSMutableDictionary *userData = server.loggedInUserInfo;
    if (userData != nil) {
        (APP_DELEGATE).userPhoneNoDetails = userData;
    }
    
    NSMutableDictionary *updatedDict  = (APP_DELEGATE).userPhoneNoDetails;
    
    self.txtOldPhoneNumber.text =[SRModalClass formatPhoneNumber:[updatedDict valueForKey:kKeyMobileNumber] deleteLastChar:NO];
    countryMasterId= [server.loggedInUserInfo valueForKey:kKeyCountryMasterId];
    oldCountryMasterId = [server.loggedInUserInfo valueForKey:kKeyCountryMasterId];
    NSString *countryCode = [server.loggedInUserInfo valueForKey:kKeyCountryMasterId];
    for (int i = 0; i < server.countryArr.count; i++) {
        if ([countryCode isEqualToString:[[server.countryArr objectAtIndex:i]valueForKey:kKeyId]]) {
            
            if(server.countryArr && server.countryArr.count && i<server.countryArr.count && [[server.countryArr objectAtIndex:i]valueForKey:kKeyPhoneCode])
            {
                varCountryCode=[@"+" stringByAppendingString:[[server.countryArr objectAtIndex:i]valueForKey:kKeyPhoneCode]];
                self.txtOldCOuntryCode.text = [@"+" stringByAppendingString:[[server.countryArr objectAtIndex:i]valueForKey:kKeyPhoneCode]];
                self.txtNewCountryCode.text = [@"+" stringByAppendingString:[[server.countryArr objectAtIndex:i]valueForKey:kKeyPhoneCode]];
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ((APP_DELEGATE).accountSettingVCFlag) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        (APP_DELEGATE).accountSettingVCFlag = NO;
    }
}

 -(IBAction)actionOnCountryBtn:(id)sender{
     UIButton *btnCountryCode=(UIButton *)sender;
     self.isOldCountrySelected=btnCountryCode.tag;
    [APP_DELEGATE hideActivityIndicator];
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc]initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}


- (void) pickCountry :(NSDictionary *)countryDict
{
    self.isCountrySelected = YES;
    if (countryDict && [countryDict count] != 0)
    {
        
        // Set search icon to textfield
        if([countryDict valueForKey:kKeyPhoneCode])
        {
            if (self.isOldCountrySelected==0)
            {
                oldCountryMasterId = [NSString stringWithFormat:@"%@",[countryDict valueForKey:kKeyId]];
                [self.txtOldCOuntryCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];
            }
            else
            {
                countryMasterId = [NSString stringWithFormat:@"%@",[countryDict valueForKey:kKeyId]];
                 [self.txtNewCountryCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];
            }
            self.isOldCountrySelected=2;
        }
    }
}

#pragma mark
#pragma mark Textfield Delegates
#pragma mark

// -----------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}



// -----------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
	NSString *totalString = [NSString stringWithFormat:@"%@%@", textField.text, string];
	NSString *errorMsg = nil;
    BOOL isValid = YES;
	// if it's the phone number textfield format it.
	if (textField == self.txtOldPhoneNumber || textField == self.txtNewPhoneNumber)
    {
		if (range.length == 1) {
			// Delete button was hit.. so tell the method to delete the last char.
			textField.text = [SRModalClass formatPhoneNumber:totalString deleteLastChar:YES];
		}
		else {
			textField.text = [SRModalClass formatPhoneNumber:totalString deleteLastChar:NO];
		}
        if ([textField.text length] >= 17)
            {
                errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
            }
            else if (errorMsg == nil) {
                BOOL flag = [SRModalClass numberValidation:string];
                if (!flag) {
                    errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
                }
            }
        // Set flag and show alert
        if (errorMsg) {
            isValid = NO;
            [SRModalClass showAlert:errorMsg];
        }
	}
    
	return isValid;
}


// --------------------------------------------------------------------------------
// validateFields

- (NSString *)validateFields {
    
    NSString *errMsg = nil;
    NSMutableDictionary *updatedDict  = (APP_DELEGATE).userPhoneNoDetails;
    NSString *oldNumber=[SRModalClass formatPhoneNumber:[updatedDict valueForKey:kKeyMobileNumber] deleteLastChar:NO];
    // Phone Number validation
    if ([[self.txtOldPhoneNumber text]length] == 0) {
        errMsg = NSLocalizedString(@"empty.contact.alert", "");
        [self.txtOldPhoneNumber becomeFirstResponder];
    }
    else if ([[self.txtNewPhoneNumber text]length] == 0) {
        errMsg = NSLocalizedString(@"empty.contact.alert", "");
        [self.txtNewPhoneNumber becomeFirstResponder];
    }
    else if (![self.txtOldCOuntryCode.text isEqualToString:varCountryCode])
    {
        errMsg = NSLocalizedString(@"Wrong.CountryCode.alert", "");
    }
    else if ([self.txtNewPhoneNumber.text isEqualToString:self.txtOldPhoneNumber.text])
    {
        errMsg = NSLocalizedString(@"same.number.alert", "");
    }
    else if(![self.txtOldPhoneNumber.text isEqualToString:oldNumber])
    {
        errMsg = NSLocalizedString(@"number.notmatched.alert", "");
    }
    return errMsg;
}


// -----------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	// hides keyboard when another part of layout was touched
    [self.txtOldPhoneNumber resignFirstResponder];
	[self.view endEditing:YES];
	[super touchesBegan:touches withEvent:event];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// -----------------------------------------------------------------------------------
// nextBtnAction:

- (void)actionOnBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}

// -----------------------------------------------------------------------------------
// actionOnDoneBack:

- (IBAction)actionOnDoneBack:(id)sender {
    
	[self.view endEditing:YES];
   	NSString *errMsg = [self validateFields];
    if (errMsg) {
        [SRModalClass showAlert:errMsg];
        }
    else {
	self.imgVerificationBG.hidden = NO;
	self.lblNewPhoneNumber.hidden = NO;
	self.confirmationView.hidden = NO;
    [self.confirmationView setCenter:CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)];
    self.lblNewPhoneNumber.text = [self.txtNewCountryCode.text stringByAppendingString:self.txtNewPhoneNumber.text];
	self.confirmationView.layer.cornerRadius = 5;
	self.confirmationView.layer.masksToBounds = YES;
	[self.view addSubview:self.confirmationView];
    }
    
}

- (IBAction)editBtnAction:(id)sender {
    
	[self.confirmationView removeFromSuperview];
	[self.txtOldPhoneNumber becomeFirstResponder];
	self.imgVerificationBG.hidden = YES;
    
}

- (IBAction)yesBtnAction:(id)sender {
	self.activityIndicator.hidden = NO;
	self.lblVerifying.hidden = NO;
	[self.activityIndicator startAnimating];
	self.lblVerifying.text = [NSString stringWithFormat:@"Verifying %@%@", self.txtNewCountryCode.text, self.txtNewPhoneNumber.text];

	self.confirmationView.hidden = YES;
    
    //Call webservice to get OTP
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    //[server.loggedInUserInfo valueForKey:kKeyCountryMasterId]
    [params setObject:countryMasterId forKey:kKeyCountryId];
    [params setObject:oldCountryMasterId forKey:kKeyOldCountryId];
    [params setObject:[SRModalClass RemoveSpecialCharacters:self.txtNewPhoneNumber.text] forKey:kKeyMobileNumber];
    [params setObject:[SRModalClass RemoveSpecialCharacters:self.txtOldPhoneNumber.text] forKey:kKeyOldMobileNumber];
    
    
//    NSString *deviceToken = server.deviceToken ? server.deviceToken: @"";
//    [params setObject:deviceToken forKey:kKeyDeviceToken];
//    [params setObject:kKeyDeviceName forKey:kKeyDeviceType];

//    country_id=99&mobile_number=9904827390&old_mobile_number=7573004377&old_country_id=99

    // Call api
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClasschangephonerequestCode];
    [server makeSynchronousRequest:urlStr inParams:params inMethodType:kPOST isIndicatorRequired:YES];
   // [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];

}
// --------------------------------------------------------------------------------
// signUPUserSucceed:

- (void)signUPUserSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSMutableDictionary *responceDict = [inNotify object];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:(APP_DELEGATE).userPhoneNoDetails];
    
    // Show Alert
    [SRModalClass showAlert:NSLocalizedString(@"alert.code.hassent.txt", @"")];
    self.imgVerificationBG.hidden = YES;
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    self.lblVerifying.hidden = YES;

    SRCodeViewController *objSRCodeView = [[SRCodeViewController alloc] initWithNibName:nil bundle:nil server:server data:params];
    objSRCodeView.codeFlag = YES;
    
    NSString *dataToken = [NSString stringWithFormat:@"%@",self.txtNewPhoneNumber.text];
    
    if (self.txtNewPhoneNumber.text.length > 10) {
        NSString *str = [dataToken stringByReplacingOccurrencesOfString:@"(" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@")" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
        objSRCodeView.phoneNumber = [NSString stringWithFormat:@"%@",str];
    }
    else
        objSRCodeView.phoneNumber = dataToken;
    
    NSString *dataToken2 = [NSString stringWithFormat:@"%@",self.txtNewCountryCode.text];
    NSString *str2 = [dataToken2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
    objSRCodeView.countryCode = [NSString stringWithFormat:@"%@",str2];
    objSRCodeView.delegate = self;
    objSRCodeView.backNavigationFlag = YES;
    objSRCodeView.countryMasterId = countryMasterId;
    
    objSRCodeView.sms_reference_code = [[responceDict valueForKey:kKeyUser] valueForKey:smsReferenceID];
    [self.navigationController pushViewController:objSRCodeView animated:NO];
}
// --------------------------------------------------------------------------------
// signUPUserFailed:

- (void)signUPUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    self.imgVerificationBG.hidden = YES;
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:[inNotify object]];
}

@end
