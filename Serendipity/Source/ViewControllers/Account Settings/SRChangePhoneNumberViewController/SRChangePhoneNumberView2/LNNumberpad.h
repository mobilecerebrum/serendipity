#import <UIKit/UIKit.h>

@interface LNNumberpad : UIView

// The one and only LNNumberpad instance you should ever need:
+ (LNNumberpad *)defaultLNNumberpad;

@end
