#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "LNNumberpad.h"
#import "SRChangePhoneNumberViewController3.h"
#import "SRProfileImageView.h"
#import "SRCodeViewController.h"
#import "SRCountryPickerViewController.h"

@interface SRChangePhoneNumberViewController2 : ParentViewController<CountryPickerDelegate>
{
    NSString *countryMasterId,*varCountryCode,*oldCountryMasterId;

    // Server
    SRServerConnection *server;
}
@property (weak, nonatomic) IBOutlet SRProfileImageView *screenBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *screenBackgroundColorImage;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPhoneBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblOldCountryCodeBackground;
@property (nonatomic, assign) BOOL isCountrySelected, isOldCountrySelected;

@property (weak, nonatomic) IBOutlet UITextField *txtOldCOuntryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblNewCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblOldPhoneBackground;
@property (weak, nonatomic) IBOutlet UITextField *txtNewCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblOldNumberWarning;
@property (weak, nonatomic) IBOutlet UILabel *lblNewNumberWarning;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPhoneNumber;
@property (weak, nonatomic) IBOutlet UIImageView *imgVerificationBG;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet  UILabel *lblVerifying;

@property (strong, nonatomic) IBOutlet UIView *confirmationView;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblisCorrectNum;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnYes,*btnOldCountCode,*btnNewCountryCOde;

@property (strong, nonatomic) IBOutlet LNNumberpad *keyPad;

//@property (weak)id delegate;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
-(IBAction)actionOnCountryBtn:(id)sender;
@end
