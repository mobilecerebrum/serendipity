//
//  SRAllowPingsViewController.m
//  Serendipity
//
//  Created by Leo on 16/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRAllowPingsViewController.h"
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"

@interface SRAllowPingsViewController ()

@end

@implementation SRAllowPingsViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
            menuArray:(NSArray *)array
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRAllowPingsViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        menuArray = array;
        navTitle = nibNameOrNil;
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
       
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateSucceed:)
                              name:kKeyNotificationSettingUpdateSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateFailed:)
                              name:kKeyNotificationSettingUpdateFailed object:nil];
    }
    
    // return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


// ---------------------------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.settingTable.backgroundColor = [UIColor clearColor];
    self.settingTable.tableFooterView = [UIView new];
    // Set title
    [SRModalClass setNavTitle:navTitle forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    allowPingsSwitchOn = [[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyallowPings] integerValue]-1;
}
// ---------------------------------------------------------------------------------------
// BackBtnAction:

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Scetions
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Rows counts
    return [menuArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
    if ([nibObjects count] > 0) {
        SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *) nibObjects[0];
        cell = customCell;
    }

    //Set Selection Style None
    ((SRHomeMenuCustomCell *)cell).selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Manage subviews hide and unhide
    //((CustomCell *)cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemTwitterBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuIconImage.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = YES;
    

    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = CGRectMake(20, 10, 300, 15);
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:14];
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.text = menuArray[indexPath.row];
    if ([navTitle isEqualToString:NSLocalizedString(@"lbl.Alert_setting.txt", @"")]) {
        if (indexPath.row != 0) {
            // Swich
            UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-60, 4, 60, 25)];
            objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
            objSwitch.tintColor = [UIColor whiteColor];
            [objSwitch setOnTintColor:[UIColor orangeColor]];
            objSwitch.backgroundColor = [UIColor lightGrayColor];
            objSwitch.layer.cornerRadius = 16.0f;
            objSwitch.tag = indexPath.row;
            [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
            
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:objSwitch];

        }
        
        if (indexPath.row==4) {
            UILabel *alertLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 25, SCREEN_WIDTH-20, 50)];
            alertLbl.textColor = [UIColor blackColor];
            alertLbl.numberOfLines = 3;
            alertLbl.lineBreakMode = NSLineBreakByWordWrapping;
            alertLbl.font = [UIFont fontWithName:kFontHelveticaBold size:10];
            alertLbl.text= NSLocalizedString(@"lbl.intelligent_Alert.txt", @"");
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:alertLbl];
        }
    }
    else{
        // Swich
        UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-60, 4, 60, 25)];
        objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
        objSwitch.tintColor = [UIColor whiteColor];
        [objSwitch setOnTintColor:[UIColor orangeColor]];
        objSwitch.backgroundColor = [UIColor lightGrayColor];
        objSwitch.layer.cornerRadius = 16.0f;
        objSwitch.tag = indexPath.row;
        if (allowPingsSwitchOn == indexPath.row) {
            [objSwitch setOn:YES];
        }
        else
            [objSwitch setOn:NO];
        [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        [((SRHomeMenuCustomCell *)cell).contentView addSubview:objSwitch];

    }
    ((SRHomeMenuCustomCell *)cell).backgroundColor = [UIColor clearColor];
    
    return cell;
}
// ---------------------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([navTitle isEqualToString:NSLocalizedString(@"lbl.Alert_setting.txt", @"")]) {
        if (indexPath.row == 4) {
            return 100;
        }
    }
    return 44;
}
#pragma mark
#pragma mark UISwitch value change action
#pragma mark

- (void)changeSwitch:(UISwitch *)sender {
    allowPingsSwitchOn = sender.tag;
    [self.settingTable reloadData];
    NSDictionary *payload;
    
    if (allowPingsSwitchOn == 4) {
        allowPingsSwitchOn = 5;
    }
    
    payload = @{@"field_name":kKeyallowPings,
                              @"field_value":[NSString stringWithFormat:@"%ld",allowPingsSwitchOn+1]
                              };
    
    //Here we have changed allowping, because
    allowPingsSwitchOn = sender.tag;
    
    NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
}


#pragma mark
#pragma mark Notifications Methods
#pragma mark
// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:


- (void)settingUpdateSucceed:(NSNotification *)inNotify {
        [APP_DELEGATE hideActivityIndicator];
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSString *allowPings = [response valueForKey:kKeyallowPings];
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:allowPings forKey:kKeyallowPings];
    [userDef synchronize];
    NSMutableDictionary *dictSetting=[[server.loggedInUserInfo valueForKey:kKeySetting] mutableCopy];
    // Update setting in logged in info
    [dictSetting setValue:allowPings forKey:kKeyallowPings];
    server.loggedInUserInfo[kKeySetting] = dictSetting;
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {
        [APP_DELEGATE hideActivityIndicator];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
