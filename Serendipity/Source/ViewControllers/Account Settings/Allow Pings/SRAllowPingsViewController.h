//
//  SRAllowPingsViewController.h
//  Serendipity
//
//  Created by Leo on 16/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRAllowPingsViewController : ParentViewController
{
    NSArray *menuArray;
    NSString *navTitle;
    NSInteger allowPingsSwitchOn;
    //Server
    SRServerConnection *server;
}
@property (strong ,nonatomic) IBOutlet UITableView *settingTable;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          menuArray:(NSArray *)array
               server:(SRServerConnection *)inServer;

@end
