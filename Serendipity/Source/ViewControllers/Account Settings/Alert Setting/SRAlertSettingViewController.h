//
//  SRAlertSettingViewController.h
//  Serendipity
//
//  Created by Leo on 16/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASValueTrackingSlider.h"

@interface SRAlertSettingViewController : ParentViewController
{
    NSArray *menuArray;
    //Server
    SRServerConnection *server;
    NSString *strNotifymeValues,*strNotifyConnectionIndex;
    //slider
    ASValueTrackingSlider *connectionSlider, *degreeSlider,*nonConnectionSlider;
    
    UILabel *connDistOneLbl,*connDistTwoLbl,*connDistThreeLbl,*connDistFourLbl,*connDistFiveLbl,*connDistSixLbl,*connDistSevenLbl,*connDistEightLbl,*connDistNineLbl;
    
    UILabel *distOneLbl,*distTwoLbl,*distThreeLbl,*distFourLbl,*distFiveLbl,*distSixLbl,*distSevenLbl,*distEightLbl,*distNineLbl;
    
    UILabel *degreeOneLbl,*degreeTwoLbl,*degreeThreeLbl,*degreeFourLbl,*degreeFiveLbl,*degreeSixLbl;
    NSInteger connectionSliderValue,degreeSliderValue,distanceSliderValue,notifyMeOfValue;
    BOOL connFlag,nonConnFlag,degreeFlag;
}
@property (strong ,nonatomic) IBOutlet UITableView *settingTable;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          menuArray:(NSArray *)array
               server:(SRServerConnection *)inServer;

@end
