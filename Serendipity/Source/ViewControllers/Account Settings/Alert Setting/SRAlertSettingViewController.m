//
//  SRAlertSettingViewController.m
//  Serendipity
//
//  Created by Leo on 16/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRAlertSettingViewController.h"
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"

#define kSliderMinValue 0
#define kSliderMaxValue 120
#define kSliderInterval 15
#define kDegreeSliderInterval 24
@interface SRAlertSettingViewController ()

@end

@implementation SRAlertSettingViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
            menuArray:(NSArray *)array
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRAlertSettingViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
    
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateSucceed:)
                              name:kKeyNotificationSettingUpdateSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateFailed:)
                              name:kKeyNotificationSettingUpdateFailed object:nil];
    }
    
    // return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


// ---------------------------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    connFlag = TRUE;
    nonConnFlag = TRUE;
    degreeFlag = TRUE;
    strNotifymeValues=[[NSString alloc]init];
    strNotifyConnectionIndex=[[NSString alloc]init];
    self.settingTable.backgroundColor = [UIColor clearColor];
    // Set title
    [SRModalClass setNavTitle:NSLocalizedString(@"lbl.Alert_setting.txt", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];

     menuArray =[[NSArray alloc]initWithObjects:NSLocalizedString(@"lbl.notify_me_conn.txt", @""),NSLocalizedString(@"lbl.notify_with.txt", @""),NSLocalizedString(@"lbl.notify_within.txt", @""),NSLocalizedString(@"lbl.notify_me.txt", @""),NSLocalizedString(@"lbl.notify_connections.txt", @""),NSLocalizedString(@"lbl.notify_connectionOfConn.txt", @""),NSLocalizedString(@"lbl.notify_favConnection.txt", @""),NSLocalizedString(@"lbl.notify_me_within.txt", @""),nil];
    
    //Set slider values
    connectionSliderValue = [[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey: kKeyNotifyWithRadius]integerValue];
    degreeSliderValue = [[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey: kKeyNotifyOnDegrees]integerValue];
    distanceSliderValue = [[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey: kKeyNotifyMeWithInRadius]integerValue];
    strNotifymeValues = [[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey: kKeyNotifyfor];
    if (![strNotifymeValues isEqualToString:@""])
    {
        NSArray *items = [strNotifymeValues componentsSeparatedByString:@","];
        for (int i = 0; i < [items count]; i++) {
            if ([[items objectAtIndex:i] intValue] == 1) {
                notifyMeOfValue = 4;
                if ([strNotifyConnectionIndex isEqualToString:@""])
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@"4"];
                }
                else
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@",4"];
                }
            }
            else if ([[items objectAtIndex:i] intValue] == 2)
            {
                notifyMeOfValue = 5;
                if ([strNotifyConnectionIndex isEqualToString:@""])
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@"5"];
                }
                else
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@",5"];
                }
            }
            else if ([[items objectAtIndex:i] intValue] == 3)
            {
                notifyMeOfValue = 6;
                if ([strNotifyConnectionIndex isEqualToString:@""])
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@"6"];
                }
                else
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@",6"];
                }
            }
            else if ([[items objectAtIndex:i] intValue] == 4)
            {
                notifyMeOfValue = 7;
                if ([strNotifyConnectionIndex isEqualToString:@""])
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@"7"];
                }
                else
                {
                    strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:@",7"];
                }
            }
        }
    }
}
// ---------------------------------------------------------------------------------------
// BackBtnAction:

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Scetions
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Rows counts
    return [menuArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *)[nibObjects objectAtIndex:0];
            cell = customCell;
        }
    }
    
    //Set Selection Style None
    ((SRHomeMenuCustomCell *)cell).selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Manage subviews hide and unhide
    //((CustomCell *)cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemTwitterBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuIconImage.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = CGRectMake(20, 10, 150, 15);
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.numberOfLines = 0;
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:14];
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.text = [menuArray objectAtIndex:indexPath.row];

    if (indexPath.row == 0 || indexPath.row == 3)
    {
        CGRect frame;
        frame = ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame;
        frame.origin.y = 10;
        frame.origin.x = 10;
        frame.size.width = 200;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = frame;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.translatesAutoresizingMaskIntoConstraints = true;
        // Swich
            UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-60, 3, 60, 3)];
            objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
            objSwitch.tintColor = [UIColor whiteColor];
            [objSwitch setOnTintColor:[UIColor orangeColor]];
            objSwitch.backgroundColor = [UIColor lightGrayColor];
            objSwitch.layer.cornerRadius = 16.0f;
            objSwitch.tag = indexPath.row;
            [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
            if (indexPath.row == 0) {
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOnConn]boolValue] == YES) {
                    [objSwitch setOn:YES animated:NO];
                }
                else
                    [objSwitch setOn:NO animated:NO];
            }
            else
            {
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOn]boolValue] == YES) {
                    [objSwitch setOn:YES animated:NO];
                }
                else
                    [objSwitch setOn:NO animated:NO];
            }
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:objSwitch];
    }
    else if (indexPath.row == 1 )
    {
        CGRect frame;
        frame = ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame;
        frame.origin.y = 20;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.translatesAutoresizingMaskIntoConstraints = true;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = frame;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
        
        UIView *cellView;
//        if (SCREEN_WIDTH == 320 ) {
//            cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 265, 15, 155, 40)];
//        }
        if (SCREEN_WIDTH >= 414 ) {
            cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 337, 10, 220, 40)];
        }
        else
            cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 295, 10, 220, 40)];
        cellView.backgroundColor = [UIColor clearColor];
        
        if (SCREEN_WIDTH == 320 ) {
            degreeSlider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,10,140, 6)];
        }
        else
            degreeSlider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,10,185, 8)];
        degreeSlider.maximumValue = kSliderMaxValue;
        degreeSlider.minimumValue = kSliderMinValue;
        degreeSlider.minimumTrackTintColor = [UIColor blackColor];
        degreeSlider.maximumTrackTintColor = [UIColor blackColor];
        degreeSlider.popUpViewColor = [UIColor clearColor];
        degreeSlider.textColor = [UIColor clearColor];
        [degreeSlider setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        degreeSlider.backgroundColor = [UIColor blackColor];
        degreeSlider.continuous = NO;
        [degreeSlider addTarget:self action:@selector(setDegreeOffset) forControlEvents:UIControlEventValueChanged];
       
        [cellView addSubview:degreeSlider];
        
        // Set Connection slider value
        if (degreeSliderValue > 1) {
            degreeSlider.value = (degreeSliderValue-1) * kDegreeSliderInterval;
        }
        else
            degreeSlider.value = kSliderMinValue;
        int x = 15;
        degreeOneLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (degreeSliderValue > 1) {
            degreeOneLbl.textColor = [UIColor blackColor];
        }
        else
            degreeOneLbl.textColor = [UIColor whiteColor];
        degreeOneLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        degreeOneLbl.text=@"1";
        degreeOneLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:degreeOneLbl];
        
        if (SCREEN_WIDTH == 320 ) {
            x = x + degreeOneLbl.frame.size.width + 16;
        }
        else
            x = x + degreeOneLbl.frame.size.width + 25;
        degreeTwoLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (degreeSliderValue == 2) {
            degreeTwoLbl.textColor = [UIColor whiteColor];
        }
        else
            degreeTwoLbl.textColor = [UIColor blackColor];
        degreeTwoLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        degreeTwoLbl.text=@"2";
        degreeTwoLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:degreeTwoLbl];
        
        if (SCREEN_WIDTH == 320 ) {
            x = x + degreeTwoLbl.frame.size.width + 16;
        }
        else
            x = x + degreeTwoLbl.frame.size.width + 25;
        degreeThreeLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (degreeSliderValue == 3) {
            degreeThreeLbl.textColor = [UIColor whiteColor];
        }
        else
            degreeThreeLbl.textColor = [UIColor blackColor];
        degreeThreeLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        degreeThreeLbl.text=@"3";
        degreeThreeLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:degreeThreeLbl];
        
        if (SCREEN_WIDTH == 320 ) {
            x = x + degreeThreeLbl.frame.size.width + 16;
        }
        else
            x = x + degreeThreeLbl.frame.size.width + 25;
        degreeFourLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (degreeSliderValue == 4) {
            degreeFourLbl.textColor = [UIColor whiteColor];
        }
        else
            degreeFourLbl.textColor = [UIColor blackColor];
        degreeFourLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        degreeFourLbl.text=@"4";
        [cellView addSubview:degreeFourLbl];
        
        if (SCREEN_WIDTH == 320 ) {
            x = x + degreeFourLbl.frame.size.width + 16;
        }
        else
            x = x + degreeFourLbl.frame.size.width + 25;
        degreeFiveLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (degreeSliderValue == 5) {
            degreeFiveLbl.textColor = [UIColor whiteColor];
        }
        else
            degreeFiveLbl.textColor = [UIColor blackColor];
        degreeFiveLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        degreeFiveLbl.text=@"5";
        [cellView addSubview:degreeFiveLbl];
        [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
        
        if (SCREEN_WIDTH == 320 ) {
            x = x + degreeFiveLbl.frame.size.width + 15;
        }
        else
            x = x + degreeFiveLbl.frame.size.width + 25;
        degreeSixLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (degreeSliderValue == 6) {
            degreeSixLbl.textColor = [UIColor whiteColor];
        }
        else
            degreeSixLbl.textColor = [UIColor blackColor];
        degreeSixLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        degreeSixLbl.text=@"6";
        [cellView addSubview:degreeSixLbl];
        [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
        
//        UILabel *degreeTextLbl = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-100, 20, 100, 15)];
//        degreeTextLbl.text = @"degrees or less";
//        degreeTextLbl.textColor = [UIColor blackColor];
//        degreeTextLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
//        [((SRHomeMenuCustomCell *)cell).contentView addSubview:degreeTextLbl];
        
        UILabel *degreeTextLbl;
        if (SCREEN_WIDTH >= 414 ) {
            degreeTextLbl = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-130, 15, 100, 15)];
            degreeTextLbl.text = @"degrees or less";
            degreeTextLbl.textColor = [UIColor blackColor];
            degreeTextLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];

        }
        else
            degreeTextLbl = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-90, 15, 100, 15)];
            degreeTextLbl.text = @"degrees or less";
            degreeTextLbl.textColor = [UIColor blackColor];
            degreeTextLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];

        [((SRHomeMenuCustomCell *)cell).contentView addSubview:degreeTextLbl];
    }
    else if (indexPath.row ==2 )
    {
        NSLog(@"%ld",(long)indexPath.row);
        CGRect frame;
        frame = ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame;
        frame.origin.y = 25;
        frame.size.width = 100;
        frame.size.height = 40;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.translatesAutoresizingMaskIntoConstraints = true;
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = frame;
         
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.numberOfLines = 3;
            
//        UIView *cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 210, 20, 200, 40)];
//        cellView.backgroundColor = [UIColor clearColor];
        
        UIView *cellView;
        if (SCREEN_WIDTH >= 414 ) {
            cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 220, 25, 200, 40)];
            cellView.backgroundColor = [UIColor clearColor];
        }
        else
            cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 220, 25, 200, 40)];
            cellView.backgroundColor = [UIColor clearColor];
                
        if (SCREEN_WIDTH == 320 ) {
            connectionSlider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,10,185, 6)];
        }
        else
            connectionSlider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,10,180, 8)];
        connectionSlider.maximumValue = kSliderMaxValue;
        connectionSlider.minimumValue = kSliderMinValue;
        connectionSlider.value = kSliderMinValue;
        
        connectionSlider.minimumTrackTintColor = [UIColor blackColor];
        connectionSlider.maximumTrackTintColor = [UIColor blackColor];
        connectionSlider.popUpViewColor = [UIColor clearColor];
        connectionSlider.textColor = [UIColor clearColor];
        [connectionSlider setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        connectionSlider.backgroundColor = [UIColor blackColor];
        connectionSlider.continuous = NO;
        [connectionSlider addTarget:self action:@selector(setDistanceOffsetOfConnection) forControlEvents:UIControlEventValueChanged];
        [cellView addSubview:connectionSlider];
                
        int x = 10;
        connDistOneLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        connDistOneLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        if (connectionSliderValue > 1) {
            connDistOneLbl.textColor = [UIColor blackColor];
        }
        else
            connDistOneLbl.textColor = [UIColor whiteColor];
        connDistOneLbl.text=@"1";
        connDistOneLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:connDistOneLbl];
            

        x = x + connDistOneLbl.frame.size.width + 9;
        connDistTwoLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (connectionSliderValue == 3) {
            connDistTwoLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 1 * kSliderInterval;
        }
        else
            connDistTwoLbl.textColor = [UIColor blackColor];
        connDistTwoLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistTwoLbl.text=@"3";
        connDistTwoLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:connDistTwoLbl];
            
        x = x + connDistTwoLbl.frame.size.width + 9;
        connDistThreeLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
        if (connectionSliderValue == 5) {
            connDistThreeLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 2 * kSliderInterval;
        }
        else
            connDistThreeLbl.textColor = [UIColor blackColor];
        connDistThreeLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistThreeLbl.text=@"5";
        connDistThreeLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:connDistThreeLbl];
            
        x = x + connDistThreeLbl.frame.size.width + 9;
        connDistFourLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
        if (connectionSliderValue == 10) {
            connDistFourLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 3 * kSliderInterval;
        }
        else
            connDistFourLbl.textColor = [UIColor blackColor];
        connDistFourLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistFourLbl.text=@"10";
        [cellView addSubview:connDistFourLbl];
            

        x = x + connDistFourLbl.frame.size.width + 9;
        connDistFiveLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
        if (connectionSliderValue == 25) {
            connDistFiveLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 4 * kSliderInterval;
        }
        else
            connDistFiveLbl.textColor = [UIColor blackColor];
        connDistFiveLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistFiveLbl.text=@"25";
        [cellView addSubview:connDistFiveLbl];
        [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
            
        x = x + connDistFiveLbl.frame.size.width + 9;
        connDistSixLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
        if (connectionSliderValue == 50) {
            connDistSixLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 5 * kSliderInterval;
        }
        else
            connDistSixLbl.textColor = [UIColor blackColor];
        connDistSixLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistSixLbl.text=@"50";
        [cellView addSubview:connDistSixLbl];
            
        x = x + connDistSixLbl.frame.size.width + 9;
        connDistSevenLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
        if (connectionSliderValue == 75) {
            connDistSevenLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 6 * kSliderInterval;
        }
        else
            connDistSevenLbl.textColor = [UIColor blackColor];
        connDistSevenLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistSevenLbl.text=@"75";
        [cellView addSubview:connDistSevenLbl];
            
        x = x + connDistSevenLbl.frame.size.width + 9;
        connDistEightLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 19, 15)];
        if (connectionSliderValue == 100) {
            connDistEightLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 7 * kSliderInterval;
        }
        else
            connDistEightLbl.textColor = [UIColor blackColor];
        connDistEightLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistEightLbl.text=@"100";
        [cellView addSubview:connDistEightLbl];
            

        x = x + connDistEightLbl.frame.size.width + 9;
        connDistNineLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 19, 15)];
        if (connectionSliderValue == 200) {
            connDistNineLbl.textColor = [UIColor whiteColor];
            connectionSlider.value = 8 * kSliderInterval;
        }
        else
            connDistNineLbl.textColor = [UIColor blackColor];
        connDistNineLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
        connDistNineLbl.text=@"200";
        [cellView addSubview:connDistNineLbl];
            
        [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
            
        UILabel *lineLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 69, SCREEN_WIDTH-10, 1)];
            lineLbl.backgroundColor = [UIColor blackColor];
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:lineLbl];
        
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
    }
    else
    {
        CGRect frame;
        frame = ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame;
        
        if (indexPath.row == 7) {
            frame.origin.x = 30;
            frame.size.width = 100;
            frame.size.height = 60;
           
            ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = frame;
            ((SRHomeMenuCustomCell *)cell).MenuItemLabel.translatesAutoresizingMaskIntoConstraints = true;
            ((SRHomeMenuCustomCell *)cell).MenuItemLabel.numberOfLines = 3;
            ((SRHomeMenuCustomCell *)cell).MenuItemLabel.lineBreakMode =NSLineBreakByWordWrapping;
            
//            UIView *cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 210, 20, 200, 40)];
//            cellView.backgroundColor = [UIColor clearColor];

            
            UIView *cellView;
            if (SCREEN_WIDTH >= 414 ) {
                cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 220, 25, 200, 40)];
                cellView.backgroundColor = [UIColor clearColor];
            }
            else
                cellView = [[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 220, 25, 200, 40)];
                cellView.backgroundColor = [UIColor clearColor];
            
            if (SCREEN_WIDTH == 320 ) {
                nonConnectionSlider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,10,185, 6)];
            }
            else
                nonConnectionSlider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,10,180, 8)];
            nonConnectionSlider.maximumValue = kSliderMaxValue;
            nonConnectionSlider.minimumValue = kSliderMinValue;
            nonConnectionSlider.value = kSliderMinValue;
            nonConnectionSlider.minimumTrackTintColor = [UIColor blackColor];
            nonConnectionSlider.maximumTrackTintColor = [UIColor blackColor];
            nonConnectionSlider.popUpViewColor = [UIColor clearColor];
            nonConnectionSlider.textColor = [UIColor clearColor];
            [nonConnectionSlider setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
            nonConnectionSlider.backgroundColor = [UIColor blackColor];
            nonConnectionSlider.continuous = NO;
            
            [nonConnectionSlider addTarget:self action:@selector(setDistanceOffset) forControlEvents:UIControlEventValueChanged];
            [cellView addSubview:nonConnectionSlider];
            
            int x = 10;
            distOneLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
            if (distanceSliderValue > 1) {
                distOneLbl.textColor = [UIColor blackColor];
            }
            else
                distOneLbl.textColor = [UIColor whiteColor];
            distOneLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distOneLbl.text=@"1";
            distOneLbl.textAlignment = NSTextAlignmentCenter;
            [cellView addSubview:distOneLbl];
            
            
            x = x + distOneLbl.frame.size.width + 9;
            distTwoLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
            if (distanceSliderValue == 3) {
                distTwoLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 1 * kSliderInterval;
            }
            else
                distTwoLbl.textColor = [UIColor blackColor];
            distTwoLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distTwoLbl.text=@"3";
            distTwoLbl.textAlignment = NSTextAlignmentCenter;
            [cellView addSubview:distTwoLbl];
            
            x = x + distTwoLbl.frame.size.width + 9;
            distThreeLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 10, 15)];
            if (distanceSliderValue == 5) {
                distThreeLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 2 * kSliderInterval;
            }
            else
                distThreeLbl.textColor = [UIColor blackColor];
            distThreeLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distThreeLbl.text=@"5";
            distThreeLbl.textAlignment = NSTextAlignmentCenter;
            [cellView addSubview:distThreeLbl];
            
            
            x = x + distThreeLbl.frame.size.width + 9;
            distFourLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
            if (distanceSliderValue == 10) {
                distFourLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 3 * kSliderInterval;
            }
            else
                distFourLbl.textColor = [UIColor blackColor];
            distFourLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distFourLbl.text=@"10";
            [cellView addSubview:distFourLbl];
            
            
            x = x + distFourLbl.frame.size.width + 9;
            distFiveLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
            if (distanceSliderValue == 25) {
                distFiveLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 4 * kSliderInterval;
            }
            else
                distFiveLbl.textColor = [UIColor blackColor];
            distFiveLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distFiveLbl.text=@"25";
            [cellView addSubview:distFiveLbl];
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
            
            
            x = x + distFiveLbl.frame.size.width + 9;
            distSixLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
            if (distanceSliderValue == 50) {
                distSixLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 5 * kSliderInterval;
            }
            else
                distSixLbl.textColor = [UIColor blackColor];
            distSixLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distSixLbl.text=@"50";
            [cellView addSubview:distSixLbl];
            
            
            x = x + distSixLbl.frame.size.width + 9;
            distSevenLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 13, 15)];
            if (distanceSliderValue == 75) {
                distSevenLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 6 * kSliderInterval;
            }
            else
                distSevenLbl.textColor = [UIColor blackColor];
            distSevenLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distSevenLbl.text=@"75";
            [cellView addSubview:distSevenLbl];
            
            x = x + distSevenLbl.frame.size.width + 9;
            distEightLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 19, 15)];
            if (distanceSliderValue == 100) {
                distEightLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 7 * kSliderInterval;
            }
            else
                distEightLbl.textColor = [UIColor blackColor];
            distEightLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distEightLbl.text=@"100";
            [cellView addSubview:distEightLbl];
            
            
            x = x + distEightLbl.frame.size.width + 9;
            distNineLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, 25, 19, 15)];
            if (distanceSliderValue == 200) {
                distNineLbl.textColor = [UIColor whiteColor];
                nonConnectionSlider.value = 8 * kSliderInterval;
            }
            else
                distNineLbl.textColor = [UIColor blackColor];
            distNineLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:11];
            distNineLbl.text=@"200";
            [cellView addSubview:distNineLbl];
            
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
            UILabel *lineLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 69, SCREEN_WIDTH-10, 1)];
            lineLbl.backgroundColor = [UIColor blackColor];
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:lineLbl];
        }
        else{
            frame.origin.x = 40;
            ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = frame;
            NSArray *arrIndex=[strNotifyConnectionIndex componentsSeparatedByString:@","];
            
            UIButton *btnView = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 48, 7, 25, 25)];
            btnView.backgroundColor = [UIColor clearColor];
            // Add target
            btnView.userInteractionEnabled = YES;
            btnView.tag = indexPath.row;
            [btnView addTarget:self action:@selector(notifyMeofConnection:) forControlEvents:UIControlEventTouchUpInside];
            [((SRHomeMenuCustomCell *)cell).contentView addSubview:btnView];
            // Set text color for options
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOn]boolValue] == NO) {
                    [((SRHomeMenuCustomCell *)cell).MenuItemLabel setTextColor:[UIColor blackColor]];
                [btnView setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            }
            else
            {
                BOOL found=NO;
                if ([arrIndex count]>0)
                {
                    for (int i=0;i<[arrIndex count];i++)
                    {
                        if ([[arrIndex objectAtIndex:i] intValue]==indexPath.row)
                        {
                            found=YES;
                            [((SRHomeMenuCustomCell *)cell).MenuItemLabel setTextColor:[UIColor whiteColor]];
                            [btnView setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                            break;
                        }
                    }
                    if (found==NO)
                    {
                        [((SRHomeMenuCustomCell *)cell).MenuItemLabel setTextColor:[UIColor blackColor]];
                        [btnView setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                    }
                }
                else
                {
                    [((SRHomeMenuCustomCell *)cell).MenuItemLabel setTextColor:[UIColor blackColor]];
                    [btnView setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                }
            }
        }
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
    }
    
        degreeSlider.enabled = ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOnConn]boolValue] == YES);
        connectionSlider.enabled = ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOnConn]boolValue] == YES);
        nonConnectionSlider.enabled = ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOn]boolValue] == YES);
    
    ((SRHomeMenuCustomCell *)cell).backgroundColor = [UIColor clearColor];
    return cell;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 ) {
        return 30;
    }
    else if (indexPath.row == 2 || indexPath.row == 8||indexPath.row ==7)
    {
        return 70;
    }
    return 50;
}
#pragma mark
#pragma mark notify about users
#pragma mark
-(void)notifyMeofConnection:(UIButton *)recognizer
{
    notifyMeOfValue = recognizer.tag;
    if ([strNotifyConnectionIndex isEqualToString:@""])
    {
        strNotifyConnectionIndex=[NSString stringWithFormat:@"%ld",(long)notifyMeOfValue];
    }
    else
    {
        if ([strNotifyConnectionIndex rangeOfString:[NSString stringWithFormat:@"%ld",(long)notifyMeOfValue]].location==NSNotFound)
        {
             strNotifyConnectionIndex=[strNotifyConnectionIndex stringByAppendingString:[NSString stringWithFormat:@",%ld",(long)notifyMeOfValue]];
        }
        else
        {
            strNotifyConnectionIndex=[strNotifyConnectionIndex stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%ld",(long)notifyMeOfValue] withString:@""];
        }
    }
    //[self.settingTable reloadData];
    
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOn]boolValue] == NO) {
        [SRModalClass showAlert:@"Please configure the notifications you want to receive for nearby users."];
    }
    else{
        NSString *notifyValue = @"";
        NSArray *arr=[strNotifyConnectionIndex componentsSeparatedByString:@","];
        for (int i=0;i<[arr count];i++)
        {
            if ([[arr objectAtIndex:i] intValue] == 4) {
                if (![notifyValue isEqualToString:@""])
                {
                    notifyValue = [notifyValue stringByAppendingString:@",1"];
                }
                else
                {
                    notifyValue = [notifyValue stringByAppendingString:@"1"];
                }
            }
            else if ([[arr objectAtIndex:i] intValue] == 5) {
                if (![notifyValue isEqualToString:@""])
                {
                    notifyValue = [notifyValue stringByAppendingString:@",2"];
                }
                else
                {
                    notifyValue = [notifyValue stringByAppendingString:@"2"];
                }
            }
            else if ([[arr objectAtIndex:i] intValue] == 6)
            {
                if (![notifyValue isEqualToString:@""])
                {
                    notifyValue = [notifyValue stringByAppendingString:@",3"];
                }
                else
                {
                    notifyValue = [notifyValue stringByAppendingString:@"3"];
                }
            }
            else if ([[arr objectAtIndex:i] intValue] == 7) {
                if (![notifyValue isEqualToString:@""])
                {
                    notifyValue = [notifyValue stringByAppendingString:@",4"];
                }
                else
                {
                    notifyValue = [notifyValue stringByAppendingString:@"4"];
                }
            }
        }
        NSLog(@"%@", notifyValue);
        NSDictionary *payload = @{@"field_name":kKeyNotifyfor,
                                  @"field_value":notifyValue
                                  };
        NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        
        if ([notifyValue isEqualToString:@""]) {
            payload = @{@"field_name":kKeyNotifyOn,
                        @"field_value":[NSString stringWithFormat:@"%ld",(long)0]
                        };
            notifyMeOfValue = 0;
            strNotifymeValues=@"";
            NSMutableDictionary *dictSetting = [[server.loggedInUserInfo valueForKey:kKeySetting]mutableCopy];
            [dictSetting setValue:@"NO" forKey:kKeyNotifyOn];
            [server.loggedInUserInfo setObject:dictSetting forKey:kKeySetting];
            NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        }
    }
    [self.settingTable reloadData];
}
#pragma mark
#pragma mark UISwitch value change action
#pragma mark

- (void)changeSwitch:(UISwitch *)sender {
    NSDictionary *payload;
    [APP_DELEGATE showActivityIndicator];
    if ([sender isOn]) {
        if (sender.tag == 0)
        {
            [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:@"1" forKey:kKeyNotifyOnConn];
            payload = @{@"field_name":kKeyNotifyOnConn,
                                      @"field_value":[NSString stringWithFormat:@"%ld",(long)1]
                                      };
            NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        }
        else if (sender.tag == 3)
        {
            payload = @{@"field_name":kKeyNotifyOn,
                        @"field_value":[NSString stringWithFormat:@"%ld",(long)1]
                        };
            notifyMeOfValue = 4;
            strNotifymeValues = @"1";
            strNotifyConnectionIndex = @"4";
            [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:@"1" forKey:kKeyNotifyOn];
            NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
            payload = @{@"field_name":kKeyNotifyfor,
                        @"field_value":strNotifymeValues
                        };
            NSString *url1 = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url1 inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        }
    }
    else {
        if (sender.tag == 0)
        {
            [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:@"0" forKey:kKeyNotifyOnConn];
            payload = @{@"field_name":kKeyNotifyOnConn,
                        @"field_value":[NSString stringWithFormat:@"%ld",(long)0]
                        };
            NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        }
        else if (sender.tag == 3)
        {
            payload = @{@"field_name":kKeyNotifyOn,
                        @"field_value":[NSString stringWithFormat:@"%ld",(long)0]
                        };
            notifyMeOfValue = 0;
            strNotifymeValues = @"";
            [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:@"0" forKey:kKeyNotifyOn];
            NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
            payload = @{@"field_name":kKeyNotifyfor,
                        @"field_value":strNotifymeValues
                        };
            NSString *url1 = [NSString stringWithFormat:@"%@",kKeyClassSetting];
            [server makeAsychronousRequest:url1 inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        }
    }
  [self.settingTable reloadData];
}
#pragma mark
#pragma mark Location offset value change action
#pragma mark
// ---------------------------------------------------------------------------------------------------------------------------------
// setDistanceOffset:

-(void)setDistanceOffset
{
    NSString *nonConnLocOffset = @"1";
    // get the nearest interval value
    for(int i = kSliderMinValue; i <= kSliderMaxValue; i=i+kSliderInterval){
            
        if(nonConnectionSlider.value >= i && nonConnectionSlider.value < i + kSliderInterval){
            if(nonConnectionSlider.value - i < i + kSliderInterval - nonConnectionSlider.value){
                    nonConnectionSlider.value = i;
                }else{
                    nonConnectionSlider.value = i + kSliderInterval;
                }
                if (nonConnectionSlider.value == 0) {
                    nonConnLocOffset = @"1";
                    distOneLbl.textColor = [UIColor whiteColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 15)
                {
                    nonConnLocOffset = @"3";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor whiteColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 30)
                {
                    nonConnLocOffset = @"5";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor whiteColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 45)
                {
                    nonConnLocOffset = @"10";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor whiteColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 60)
                {
                    nonConnLocOffset = @"25";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor whiteColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 75)
                {
                    nonConnLocOffset = @"50";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor whiteColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 90)
                {
                    nonConnLocOffset = @"75";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor whiteColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 105)
                {
                    nonConnLocOffset = @"100";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor whiteColor];
                    distNineLbl.textColor = [UIColor blackColor];
                }
                else if (nonConnectionSlider.value == 120)
                {
                    nonConnLocOffset = @"200";
                    distOneLbl.textColor = [UIColor blackColor];
                    distTwoLbl.textColor = [UIColor blackColor];
                    distThreeLbl.textColor = [UIColor blackColor];
                    distFourLbl.textColor = [UIColor blackColor];
                    distFiveLbl.textColor = [UIColor blackColor];
                    distSixLbl.textColor = [UIColor blackColor];
                    distSevenLbl.textColor = [UIColor blackColor];
                    distEightLbl.textColor = [UIColor blackColor];
                    distNineLbl.textColor = [UIColor whiteColor];
                }
            
            // Call webservice if switch is on
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOn]boolValue] == NO) {
                if (connFlag) {
                    connFlag = FALSE;
                    [SRModalClass showAlert:NSLocalizedString(@"alert.enable_notify_switch.txt", @"")];
                }
            }
            else{
                NSDictionary *payload = @{@"field_name":kKeyNotifyMeWithInRadius,
                                          @"field_value":nonConnLocOffset
                                          };
                NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
                [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
                break;
            }
        }
    }

}

// ---------------------------------------------------------------------------------------------------------------------------------
// setDistanceOffsetOfConnection:

-(void)setDistanceOffsetOfConnection
{
    NSString *locOffset = @"1";
    // get the nearest interval value
    for(int i = kSliderMinValue; i <= kSliderMaxValue; i=i+kSliderInterval){
        
        if(connectionSlider.value >= i && connectionSlider.value < i + kSliderInterval){
            if(connectionSlider.value - i < i + kSliderInterval - connectionSlider.value){
                connectionSlider.value = i;
            }else{
                connectionSlider.value = i + kSliderInterval;
            }
            if (connectionSlider.value == 0) {
                locOffset = @"1";
                connDistOneLbl.textColor = [UIColor whiteColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 15)
            {
                locOffset = @"3";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor whiteColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 30)
            {
                locOffset = @"5";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor whiteColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 45)
            {
                locOffset = @"10";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor whiteColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 60)
            {
                locOffset = @"25";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor whiteColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 75)
            {
                locOffset = @"50";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor whiteColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 90)
            {
                locOffset = @"75";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor whiteColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 105)
            {
                locOffset = @"100";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor whiteColor];
                connDistNineLbl.textColor = [UIColor blackColor];
            }
            else if (connectionSlider.value == 120)
            {
                locOffset = @"200";
                connDistOneLbl.textColor = [UIColor blackColor];
                connDistTwoLbl.textColor = [UIColor blackColor];
                connDistThreeLbl.textColor = [UIColor blackColor];
                connDistFourLbl.textColor = [UIColor blackColor];
                connDistFiveLbl.textColor = [UIColor blackColor];
                connDistSixLbl.textColor = [UIColor blackColor];
                connDistSevenLbl.textColor = [UIColor blackColor];
                connDistEightLbl.textColor = [UIColor blackColor];
                connDistNineLbl.textColor = [UIColor whiteColor];
            }
            
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOnConn]boolValue] == YES) {
                NSDictionary *payload = @{@"field_name":kKeyNotifyWithRadius,
                                          @"field_value":locOffset
                                          };
                NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
                [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
                break;
            }
            else
            {
                if (nonConnFlag) {
                    nonConnFlag = FALSE;
                    [SRModalClass showAlert:NSLocalizedString(@"alert.enable_notifyConnection_switch.txt", @"")];
                }
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// setDegreeOffset:

-(void)setDegreeOffset
{
    NSString *degreeOffset = @"1";
    // get the nearest interval value
    for(int i = kSliderMinValue; i <= kSliderMaxValue; i=i+kDegreeSliderInterval){
        
        if(degreeSlider.value >= i && degreeSlider.value < i + kDegreeSliderInterval){
            if(degreeSlider.value - i < i + kDegreeSliderInterval - degreeSlider.value){
                degreeSlider.value = i;
            }else{
                degreeSlider.value = i + kDegreeSliderInterval;
            }
            if (degreeSlider.value == 0) {
                degreeOffset = @"1";
                degreeOneLbl.textColor = [UIColor whiteColor];
                degreeTwoLbl.textColor = [UIColor blackColor];
                degreeThreeLbl.textColor = [UIColor blackColor];
                degreeFourLbl.textColor = [UIColor blackColor];
                degreeFiveLbl.textColor = [UIColor blackColor];
                degreeSixLbl.textColor = [UIColor blackColor];
            }
            else if (degreeSlider.value == 24)
            {
                degreeOffset = @"2";
                degreeOneLbl.textColor = [UIColor blackColor];
                degreeTwoLbl.textColor = [UIColor whiteColor];
                degreeThreeLbl.textColor = [UIColor blackColor];
                degreeFourLbl.textColor = [UIColor blackColor];
                degreeFiveLbl.textColor = [UIColor blackColor];
                degreeSixLbl.textColor = [UIColor blackColor];
            }
            else if (degreeSlider.value == 48)
            {
                degreeOffset = @"3";
                degreeOneLbl.textColor = [UIColor blackColor];
                degreeTwoLbl.textColor = [UIColor blackColor];
                degreeThreeLbl.textColor = [UIColor whiteColor];
                degreeFourLbl.textColor = [UIColor blackColor];
                degreeFiveLbl.textColor = [UIColor blackColor];
                degreeSixLbl.textColor = [UIColor blackColor];
            }
            else if (degreeSlider.value == 72)
            {
                degreeOffset = @"4";
                degreeOneLbl.textColor = [UIColor blackColor];
                degreeTwoLbl.textColor = [UIColor blackColor];
                degreeThreeLbl.textColor = [UIColor blackColor];
                degreeFourLbl.textColor = [UIColor whiteColor];
                degreeFiveLbl.textColor = [UIColor blackColor];
                degreeSixLbl.textColor = [UIColor blackColor];
            }
            else if (degreeSlider.value == 96)
            {
                degreeOffset = @"5";
                degreeOneLbl.textColor = [UIColor blackColor];
                degreeTwoLbl.textColor = [UIColor blackColor];
                degreeThreeLbl.textColor = [UIColor blackColor];
                degreeFourLbl.textColor = [UIColor blackColor];
                degreeFiveLbl.textColor = [UIColor whiteColor];
                degreeSixLbl.textColor = [UIColor blackColor];
            }
            else if (degreeSlider.value == 120)
            {
                degreeOffset = @"6";
                degreeOneLbl.textColor = [UIColor blackColor];
                degreeTwoLbl.textColor = [UIColor blackColor];
                degreeThreeLbl.textColor = [UIColor blackColor];
                degreeFourLbl.textColor = [UIColor blackColor];
                degreeFiveLbl.textColor = [UIColor blackColor];
                degreeSixLbl.textColor = [UIColor whiteColor];
            }
            
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kKeyNotifyOnConn]boolValue] == YES) {
                NSDictionary *payload = @{@"field_name":kKeyNotifyOnDegrees,
                                          @"field_value":degreeOffset
                                          };
                NSString *url = [NSString stringWithFormat:@"%@",kKeyClassSetting];
                [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
                break;
            }
            else
            {
                if (degreeFlag) {
                    degreeFlag = FALSE;
                    [SRModalClass showAlert:NSLocalizedString(@"alert.enable_notifyConnection_switch.txt", @"")];
                }
            }
        }
    }
}
#pragma mark
#pragma mark Notifications Methods
#pragma mark
// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:


- (void)settingUpdateSucceed:(NSNotification *)inNotify {
        [APP_DELEGATE hideActivityIndicator];
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSString *notify_me_within_radius = [response valueForKey:kKeyNotifyMeWithInRadius];
    NSString *notify_within_radius = [response valueForKey:kKeyNotifyWithRadius];
    NSString *notify_me_on = [response valueForKey:kKeyNotifyOn];
    NSString *notify_on_degrees = [response valueForKey:kKeyNotifyOnDegrees];
    NSString *notify_on_connection = [response valueForKey:kKeyNotifyOnConn];
    NSString *notify_me_for=[response valueForKey:kKeyNotifyfor];
    
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:notify_me_within_radius forKey:kKeyNotifyMeWithInRadius];
    [userDef setValue:notify_within_radius forKey:kKeyNotifyWithRadius];
    [userDef setValue:notify_me_on forKey:kKeyNotifyOn];
    [userDef setValue:notify_on_degrees forKey:kKeyNotifyOnDegrees];
    [userDef setValue:notify_on_connection forKey:kKeyNotifyOnConn];
    [userDef setValue:notify_me_for forKey:kKeyNotifyfor];
    [userDef synchronize];
    
    // Update setting in logged in info
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:notify_me_within_radius forKey:kKeyNotifyMeWithInRadius];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:notify_within_radius forKey:kKeyNotifyWithRadius];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:notify_me_on forKey:kKeyNotifyOn];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:notify_on_degrees forKey:kKeyNotifyOnDegrees];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:notify_on_connection forKey:kKeyNotifyOnConn];
     [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:notify_me_for forKey:kKeyNotifyfor];
    //Save slider values
    connectionSliderValue = [notify_within_radius integerValue];
    degreeSliderValue = [notify_on_degrees integerValue];
    distanceSliderValue = [notify_me_within_radius integerValue];
    //[self.settingTable reloadData];
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {
        [APP_DELEGATE hideActivityIndicator];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
