#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "LiveConnectClient.h"

#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRProfileImageView.h"
#import "Serendipity-Bridging-Header.h"
#import <Serendipity-Swift.h>


#define YAHOO_AUTHORIZATION_CODE @"YAHOO_AUTHORIZATION_CODE"
#define YahooConsumerkey @"dj0yJmk9TlNuVzVwbUxNQVdUJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWJk"
#define YahooConsumerSecret @"4935672cd4752ba2e45edf8206dc31f6ab3e15fc"
#define YahooAppId @"rdR8LD6q"
#define YahooCallBackUrl @"http://dev.serendipity.app/callback"


#define LiveClientId  @"6a8d57df-8439-4124-aae9-f9b19f0d19e4" //@"0000000040188722"    //@"000000004813E69A"

//#define YahooConsumerkey @"dj0yJmk9a0MxN3p5OXhrdmNPJmQ9WVdrOVZ6RnBZa1JDTjJzbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1iNg--"
//#define YahooConsumerSecret @"f939c9656990e54ebc1d5db7f68fc421e2091415"
//
//#define YahooAppId @"W1ibDB7k"
//#define YahooCallBackUrl @"http://www.wikireviews.com"
//#define YahooClientId @"000000004813E69A"

@protocol YahooSessionGetNotifyDelegate <NSObject>

-(void)didReceiveAuthorization;

@end
@interface SRManagePersistentConnectionsViewController : ParentViewController<GIDSignInDelegate,LiveAuthDelegate,LiveOperationDelegate>
{
    //Instance Variable
    NSMutableArray *menuItemArray;
    NSMutableArray *menuIconArray;
    
    //Server
    SRServerConnection *server;
    YahooSwift *yahoo;
}
//Properties
@property (strong, nonatomic) UISwitch *gmailSwitch, *yahooSwitch, *hotmailSwitch;

@property (strong, nonatomic) IBOutlet SRProfileImageView *imgBackgroundPhotoImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackgroundColorImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppLogoImage;
@property (strong, nonatomic) IBOutlet UILabel *lblAddNetworks;
@property (strong, nonatomic) IBOutlet UITableView *objTableView;

@property (strong, nonatomic) NSString *contactType;

//Yahoo Session

@property (weak) id<YahooSessionGetNotifyDelegate> delegate1;
@property (weak) id delegate;
//Live
@property (strong, nonatomic) LiveConnectClient *liveClient;

//Other Properties

#pragma mark
#pragma mark Init Method
#pragma mark
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
