
#import "SRManagePersistentConnectionsViewController.h"

@implementation SRManagePersistentConnectionsViewController



@synthesize liveClient;

#pragma mark Init Method



- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRManagePersistentConnectionsViewController" bundle:nibBundleOrNil];
    if (self) {
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(contactUploadSuccess:)
                              name:kKeyNotificationContactUploadSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(contactUploadFailed:)
                              name:kKeyNotificationContactUploadFailed object:nil];
        [defaultCenter addObserver:self
            selector:@selector(contactUploadFailed:)
                name:kKeyNotificationUserContactStoreFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(yahooContactsImported:) name:@"YAHOO_CONTACTS_LOADED" object:nil];


        // Custom initialization
        server = inServer;
    }
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma marks
#pragma marks Standard Methods
#pragma marks

//-------------------------------------------------------------------------------------
//ViewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.managePersistentConnections", " ") forViewNavCon:self.navigationItem forFontSize:15.0];



    //left nav bar button
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    //initialize item names and images
//    menuItemArray=[[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"lbl.facebook.txt", @""),NSLocalizedString(@"lbl.linkedin.txt", @""),NSLocalizedString(@"lbl.googleplus.txt", @""),NSLocalizedString(@"lbl.whatsapp.txt", @""),NSLocalizedString(@"lbl.viber.txt", @""),NSLocalizedString(@"lbl.yahoo.txt", @""),NSLocalizedString(@"lbl.outlook.txt", @""),NSLocalizedString(@"lbl.aol.txt", @""), nil];

//    menuIconArray = [[NSMutableArray alloc] initWithObjects:@"social-facebook.png", @"social-linkedin.png", @"social-googleplus.png", @"social-whatsapp.png", @"social-viber.png", @"social-yahoo.png", @"social-outlook.png", @"social-aol.png", nil];

    menuItemArray = [@[NSLocalizedString(@"lbl.googleplus.txt", @""), NSLocalizedString(@"lbl.yahoo.txt", @""), NSLocalizedString(@"lbl.outlook.txt", @"")] mutableCopy];

    menuIconArray = [@[@"social-googleplus.png", @"social-yahoo.png", @"social-outlook.png"] mutableCopy];


    //set footerview to uitableview
    self.objTableView.tableFooterView = [[UIView alloc] init];
    self.hidesBottomBarWhenPushed = YES;

    //Get google sign in to get gmail contact
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    //[signIn signInSilently];
    signIn.delegate = self;
    signIn.presentingViewController = self;
    
    //For google add contact scope
    NSString *contactsScope = @"https://www.googleapis.com/auth/contacts.readonly";
    NSArray *currentScopes = [GIDSignIn sharedInstance].scopes;
    [GIDSignIn sharedInstance].scopes = [currentScopes arrayByAddingObject:contactsScope];
    signIn.scopes = [currentScopes arrayByAddingObject:contactsScope];
    [[SRAPIManager sharedInstance] updateToken:server.token];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                      selector:@selector(settingUpdateSucceed:)
                          name:kKeyNotificationSettingUpdateSucceed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                      selector:@selector(settingUpdateFailed:)
                          name:kKeyNotificationSettingUpdateFailed object:nil];

}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSettingUpdateSucceed object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSettingUpdateFailed object:nil];
}

#pragma mark
#pragma mark UIViewcontroller delegate
#pragma mark

// ------------------------------------------------------------------------------------------------------------------
- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] pushViewController:viewController animated:YES];
}

#pragma mark Actions Methods

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

#pragma mark TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItemArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *) nibObjects[0];
            cell = customCell;
        }
    }

    // Manage subviews hide and unhide

    ((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN.hidden = YES;

    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.cornerRadius = ((SRHomeMenuCustomCell *) cell).MenuIconImage.frame.size.width / 3;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.masksToBounds = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.clipsToBounds = YES;

    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.text = menuItemArray[indexPath.row];
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame = CGRectMake(55, 5, 250, 34);
    ((SRHomeMenuCustomCell *) cell).MenuIconImage.frame = CGRectMake(15, 5, 34, 34);
    ((SRHomeMenuCustomCell *) cell).MenuIconImage.image = [UIImage imageNamed:menuIconArray[indexPath.row]];

    
    cell.clipsToBounds = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectionStyle = UITableViewCellSeparatorStyleSingleLine;
    
    if (indexPath.row == 0) {
        self.gmailSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 54, 5, 51, 21)];
        self.gmailSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
        self.gmailSwitch.tintColor = [UIColor whiteColor];
        [self.gmailSwitch setOnTintColor:[UIColor orangeColor]];
        self.gmailSwitch.backgroundColor = [UIColor lightGrayColor];
        self.gmailSwitch.layer.cornerRadius = 16.0f;
        self.gmailSwitch.tag = indexPath.row;
        [self.gmailSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        [((SRHomeMenuCustomCell *) cell) addSubview:self.gmailSwitch];
        ((SRHomeMenuCustomCell *) cell).backgroundColor = [UIColor clearColor];
        [self.gmailSwitch setOn:[[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kKeyGmail] boolValue]];
    } else if (indexPath.row == 1) {
        self.yahooSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 54, 5, 51, 21)];
        self.yahooSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
        self.yahooSwitch.tintColor = [UIColor whiteColor];
        [self.yahooSwitch setOnTintColor:[UIColor orangeColor]];
        self.yahooSwitch.backgroundColor = [UIColor lightGrayColor];
        self.yahooSwitch.layer.cornerRadius = 16.0f;
        self.yahooSwitch.tag = indexPath.row;
        [self.yahooSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        [((SRHomeMenuCustomCell *) cell) addSubview:self.yahooSwitch];
        ((SRHomeMenuCustomCell *) cell).backgroundColor = [UIColor clearColor];
        [self.yahooSwitch setOn:[[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kKeyYahoo] boolValue]];
    } else {
        self.hotmailSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 54, 5, 51, 21)];
        self.hotmailSwitch.transform = CGAffineTransformMakeScale(0.75, 0.75);
        self.hotmailSwitch.tintColor = [UIColor whiteColor];
        [self.hotmailSwitch setOnTintColor:[UIColor orangeColor]];
        self.hotmailSwitch.backgroundColor = [UIColor lightGrayColor];
        self.hotmailSwitch.layer.cornerRadius = 16.0f;
        self.hotmailSwitch.tag = indexPath.row;
        [self.hotmailSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        [((SRHomeMenuCustomCell *) cell) addSubview:self.hotmailSwitch];
        ((SRHomeMenuCustomCell *) cell).backgroundColor = [UIColor clearColor];
        [self.hotmailSwitch setOn:[[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kKeyHotmail] boolValue]];
    }

    //tableView.separatorStyle

    // Return
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark
#pragma mark UISwitch value change action
#pragma mark


- (void)changeSwitch:(UISwitch *)sender {
//    if ([sender isOn]) {
        if (sender.tag == 0) {
//            [[GIDSignIn sharedInstance] setAllowsSignInWithWebView:NO];//HS
//            [[GIDSignIn sharedInstance] signIn];
            NSDictionary *payload = @{kKeyFieldName: kKeyGmail,
                                      kKeyFieldValue: @(sender.isOn),
            };

            NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        } else if (sender.tag == 1) {
//            [self createYahooSession];
            NSDictionary *payload = @{kKeyFieldName: kKeyYahoo,
                                      kKeyFieldValue: @(sender.isOn),
            };

            NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        } else if (sender.tag == 2) {
//            [self getHotmailContacts];
            NSDictionary *payload = @{kKeyFieldName: kKeyHotmail,
                                      kKeyFieldValue: @(sender.isOn),
            };

            NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
        }
//    }
}

#pragma mark
#pragma mark Google sign in Delegate
#pragma mark

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.

    if (!error) {
        [APP_DELEGATE showActivityIndicator];
        // get user contact
        [self getGmailcontact:user];
    }
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {

}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:^{
        [APP_DELEGATE showActivityIndicator];
    }];

}

- (void)getGmailcontact:(GIDGoogleUser *)user {
    NSString *urlStr;

    // Google Contacts feed
    //
    //    https://www.googleapis.com/oauth2/v2/userinfo
    urlStr = [NSString stringWithFormat:@"https://www.google.com/m8/feeds/contacts/default/full?access_token=%@&max-results=999&alt=json&v=3.0", user.authentication.accessToken];

    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
//    [request setValue:@"3.0" forHTTPHeaderField:@"GData-Version"];


//    NSString *output = nil;
    NSError *error = nil;

    NSURLResponse *response = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];

    if (data) {
        // API fetch succeeded
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];

        if ([dict[@"feed"][@"entry"] count]) {
            //parse data

            NSMutableArray *contactArray = [[NSMutableArray alloc] init];

            for (id obj in dict[@"feed"][@"entry"]) {
                NSString *name = obj[@"title"][@"$t"] != nil ? obj[@"title"][@"$t"] : @"";;

//                NSString *phonenum = obj[@"gd$phoneNumber"][0][@"$t"] != nil ? obj[@"gd$phoneNumber"][0][@"$t"] : @"";
                NSArray *arrMobile = [obj valueForKey:@"gd$phoneNumber"];
                NSString* strMobile;
                if ([arrMobile count] > 0) {
                    NSMutableDictionary* dictMobile = arrMobile[0];
                    strMobile = [dictMobile valueForKey:@"uri"];
                    NSString *stringWithoutSpaces = [strMobile
                    stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    strMobile = [stringWithoutSpaces
                    stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
                }
                NSString *email = obj[@"gd$email"][0][@"address"] != nil ? obj[@"gd$email"][0][@"address"] : @"";

                if (strMobile.length > 0) {
                    NSDictionary *dict = @{kKeyFirstName: name, kKeyLastName: name, kKeyEmail: email, kKeyMobileNumber: strMobile};
                    [contactArray addObject:dict];
                }
            }

            if (contactArray.count > 0) {
                NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
                [dataDict setValue:contactArray forKey:kKeyContacts];
                [dataDict setValue:kKeyGmailValue forKey:kKeyContacTyype];
                [server makeSynchronousRequest:kKeyClassUserContactsStore inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
            }
        }
    } else {
        // fetch failed
//        output = [error description];

    }

}

#pragma mark
#pragma mark Yahoo Delegate Method
#pragma mark

-(void)yahooContactsImported:(NSNotification*)notification {
    NSMutableArray *contacts = [notification object];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    if (contacts != nil) {
        if (contacts.count > 0) {
            [dataDict setValue:contacts forKey:kKeyContacts];
            [dataDict setValue:kKeyYahooValue forKey:kKeyContacTyype];
            [server makeSynchronousRequest:kKeyClassUserContactsStore inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
        } else {
            [(APP_DELEGATE) hideActivityIndicator];
        }
    }
}

// -------------------------------------------------------------------------------------------------------------------------------------
// createYahooSession

- (void)createYahooSession {
    
    yahoo = [YahooSwift new];
    [yahoo createSessionWithConsumerKey:YahooConsumerkey secretKey:YahooConsumerSecret callbackUrl:YahooCallBackUrl authorizeUrl:@"https://api.login.yahoo.com/oauth2/request_auth" viewController:self];
    
}


#pragma mark- get notification from yahoo

- (void)didReceiveAuthorization {
    [self createYahooSession];
}

// -------------------------------------------------------------------------------------------------------------------------------------

#pragma mark- Hotmail live Delegate Method

- (void)getHotmailContacts {
    NSString *scopeText = @"wl.signin wl.basic wl.contacts_emails";

    NSArray *scopes = [scopeText componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.liveClient = [[LiveConnectClient alloc] initWithClientId:LiveClientId
                                                           scopes:scopes
                                                         delegate:self
                                                        userState:@"init"];

    [self loginWithScopes:scopes];
}

// ----------------------------------------------------------------------------------------------------
// loginWithScopes:

- (void)loginWithScopes:(NSArray *)scopes {
    @try {
        [self.liveClient login:self
                        scopes:scopes
                      delegate:self
                     userState:@"login"];
    }
    @catch (id ex) {

    }
}

// ------------------------------------------------------------------------------------------------------
// getLivemailContacts

- (void)getLivemailContacts {
    if (self.liveClient.session) {
        [self.liveClient getWithPath:@"me/contacts"
                            delegate:self
                           userState:@"get"];
    } else {
        [APP_DELEGATE hideActivityIndicator];
    }

}

#pragma mark LiveAuthDelegate

- (void)authCompleted:(LiveConnectSessionStatus)status session:(LiveConnectSession *)session
            userState:(id)userState {

    [APP_DELEGATE showActivityIndicator];
    [self getLivemailContacts];
}

// ------------------------------------------------------------------------------------------------------
// authFailed:

- (void)authFailed:(NSError *)error
         userState:(id)userState {
    [APP_DELEGATE hideActivityIndicator];
}

#pragma mark LiveOperationDelegate

// -------------------------------------------------------------------------------------------------------
// liveOperationSucceeded

- (void)liveOperationSucceeded:(LiveOperation *)operation {

    [APP_DELEGATE hideActivityIndicator];

    _contactType = @"hotmail";

    if ([operation.result[@"data"] count]) {

        //parse data
        NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];

        NSMutableArray *contactArray = [[NSMutableArray alloc] init];

        for (id obj in operation.result[@"data"]) {
            NSString *email = obj[@"emails"][@"preferred"];

            if (email.length > 0) {
                NSDictionary *dict = @{kKeyFirstName: obj[kKeyFirstName], kKeyLastName: obj[kKeyLastName], kKeyEmail: obj[@"emails"][@"preferred"], kKeyMobileNumber: @""};
                [contactArray addObject:dict];
            }
        }

        if (contactArray.count > 0) {
            [dataDict setValue:contactArray forKey:kKeyGroupUsers];
            [dataDict setValue:kKeyHotmail forKey:kKeyType];
            [server makeAsychronousRequest:kKeyClassUploadContacts inParams:dataDict isIndicatorRequired:YES inMethodType:kPOST];
        } else {
            [SRModalClass showAlert:@"None of your contacts are currently on Serendipity.  We will let you know when your contacts join.  (put Okay button below the text)"];
        }

    } else {
        [SRModalClass showAlert:@"None of your contacts are currently on Serendipity.  We will let you know when your contacts join.  (put Okay button below the text)"];
    }
}

// -------------------------------------------------------------------------------------------------------------------------------------
// liveOperationFailed

- (void)liveOperationFailed:(NSError *)error operation:(LiveOperation *)operation {

    [APP_DELEGATE hideActivityIndicator];

    // [SRModalClass showAlert:@"Error in connection"];
}


#pragma mark
#pragma mark - Upload contact Notificiation

- (void)settingUpdateSucceed:(NSNotification *)inNotify  {
    NSMutableDictionary *dict = [inNotify object];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:[dict valueForKey:kKeyGmail] forKey:kKeyGmail];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:[dict valueForKey:kKeyYahoo] forKey:kKeyYahoo];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:[dict valueForKey:kKeyHotmail] forKey:kKeyHotmail];
    [self.objTableView reloadData];
}

- (void)settingUpdateFailed:(NSNotification *)inNotify  {
    [self.objTableView reloadData];
}

- (void)contactUploadSuccess:(NSNotification *)notification {
    [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import.text", @"")];
}

- (void)contactUploadFailed:(NSNotification *)notification {
    [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import_fail.txt", @"")];
}
@end
