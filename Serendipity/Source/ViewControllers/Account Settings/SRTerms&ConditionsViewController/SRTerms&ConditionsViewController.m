#import "SRTerms&ConditionsViewController.h"

@implementation SRTerms_ConditionsViewController

#pragma mark Init Method

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    titleString = nibNameOrNil;
    self = [super initWithNibName:@"SRTerms&ConditionsViewController" bundle:nibBundleOrNil];
    if (self) {
        server = inServer;
    }
    return self;
}

#pragma mark Standard Methods

- (void)viewDidLoad {
    //Super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    if ([titleString isEqualToString:@"Terms & Policy1"]) {
        [SRModalClass setNavTitle:@"Terms of Service" forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        titleText = @"Terms_of_Service";
    } else if ([titleString isEqualToString:@"Terms & Policy2"]) {
        [SRModalClass setNavTitle:@"Mobile App End User License Agreement" forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        titleText = @"Terms & Policy";

    } else if ([titleString isEqualToString:@"Terms & Policy3"]) {
        [SRModalClass setNavTitle:@"Privacy Notice" forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        titleText = @"Privacy Notice";


    } else {
        [SRModalClass setNavTitle:titleString forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }



    //left bar button
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

//    self.webViewTerm_Conditions.scalesPageToFit = YES;
    self.webViewTerm_Conditions.scrollView.bounces = NO;
    [self.webViewTerm_Conditions setBackgroundColor:[UIColor clearColor]];
    [self.webViewTerm_Conditions setOpaque:NO];
    self.webViewTerm_Conditions.scrollView.scrollEnabled = YES;
    self.webViewTerm_Conditions.navigationDelegate = self;

    if ([titleText containsString:@"Terms & Policy"]) {

        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"privacy_notice" ofType:@"html"];
        NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [self.webViewTerm_Conditions loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
//[self.webViewTerm_Conditions loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:@"https://serendipity.app/legal"]]];

    } else if ([titleText isEqualToString:@"Privacy Notice"]) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"policy" ofType:@"html"];
        NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [self.webViewTerm_Conditions loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];

    } else if ([titleText isEqualToString:@"Terms_of_Service"]) {
        [SRModalClass setNavTitle:@"Terms of Service" forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
//        [self.webViewTerm_Conditions loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:@"https://serendipity.app/termsofservice"]]];//https://serendipity.app/legal
        
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"terms_services" ofType:@"html"];
        NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [self.webViewTerm_Conditions loadHTMLString:htmlString baseURL:[[NSBundle mainBundle] bundleURL]];
    }
}

#pragma mark - Private Methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollViewBis {
    return self.webViewTerm_Conditions;
}

#pragma mark - Delegate Methods
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    if ([titleString isEqualToString:@"Terms & Policy2"]) {
        
        [webView evaluateJavaScript:@"window.location.href = '#section2';" completionHandler:^(id _Nullable content, NSError * _Nullable error) {
            
        }];
    } else if ([titleString isEqualToString:@"Terms & Policy3"]) {
        [webView evaluateJavaScript:@"window.location.href = '#section3';" completionHandler:^(id _Nullable content, NSError * _Nullable error) {
            
        }];
    }
}


#pragma mark - Action Methods

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
    _btnPrivacyPolicy = nil;
    _btnLicAgreement = nil;
}

@end
