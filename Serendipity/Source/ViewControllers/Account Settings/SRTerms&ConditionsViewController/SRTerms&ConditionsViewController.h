#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRProfileImageView.h"

@interface SRTerms_ConditionsViewController : ParentViewController<WKNavigationDelegate>
{
    //Instance Variables
    NSString *titleString;
    NSString *titleText;
    //Server
    SRServerConnection *server;
}

// properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *imgBackGroundPhotoImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackGroundColorImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppLogoImage;
@property (strong, nonatomic) IBOutlet WKWebView *webViewTerm_Conditions;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UIButton *btnTermAndCondition;
@property (strong, nonatomic) IBOutlet UIButton *btnLicAgreement;
@property (strong, nonatomic) IBOutlet UIButton *btnPrivacyPolicy;
// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
