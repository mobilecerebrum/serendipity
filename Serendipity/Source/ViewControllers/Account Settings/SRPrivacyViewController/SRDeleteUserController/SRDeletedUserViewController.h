//
//  SRDeletedUserViewController.h
//  Serendipity
//
//  Created by Hitesh Surani on 04/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "ParentViewController.h"
#import "SRProfileImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRDeletedUserViewController : ParentViewController
{
    SRServerConnection *server;
    NSInteger currentIndex;
    //Instance Variables
    NSMutableArray *DeletUserArray;
    NSMutableArray *cacheImgArr;
    BOOL isMyConnection;
}


@property (weak, nonatomic) IBOutlet SRProfileImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *bckClrImg;
@property (strong, nonatomic) IBOutlet UIImageView *logoImg;
@property (strong, nonatomic) UILabel *lblNoData;

//Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *) dict server:(SRServerConnection *)inServer;


@end

NS_ASSUME_NONNULL_END
