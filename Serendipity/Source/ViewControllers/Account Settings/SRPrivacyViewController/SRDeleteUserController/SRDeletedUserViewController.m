//
//  SRDeletedUserViewController.m
//  Serendipity
//
//  Created by Hitesh Surani on 04/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "SRDeletedUserViewController.h"
#import "SRModalClass.h"
#import "SRAddNewConnectionCell.h"

@interface SRDeletedUserViewController ()

@end

@implementation SRDeletedUserViewController


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SRDeletedUserViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        DeletUserArray = [[NSMutableArray alloc] init];


//        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getDeletedUsersSucceed:)
                              name:kKeyNotificationGetDeletedUsersSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getDeleteUsersFailed:)
                              name:kKeyNotificationGetDeletedUsersFailed object:nil];
//        [defaultCenter addObserver:self
//                          selector:@selector(deleteBlockConnectionSucceed:)
//                              name:kKeyNotificationGetUnDeletedUsersSucceed object:nil];
//        [defaultCenter addObserver:self
//                          selector:@selector(deleteBlockConnectionFailed:)
//                              name:kKeyNotificationGetUnDeletedUsersFailed object:nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Title
      [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.deletedUser.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
      UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
      [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
      self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.lblNoData = [[UILabel alloc] init];
    self.lblNoData.textAlignment = NSTextAlignmentCenter;
    self.lblNoData.textColor = [UIColor blackColor];
    self.lblNoData.text = NSLocalizedString(@"lbl.nodeleteConnectionFnd.txt", @"");
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [APP_DELEGATE showActivityIndicator];
    [server makeAsychronousRequest:kKeyClassGetDeleteConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

#pragma mark Action Methods
- (void)actionOnBack:(UIButton *)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)inBlockBtnAction:(UIButton *)sender {
    currentIndex = sender.tag;
    NSDictionary *userDict = DeletUserArray[currentIndex];

//    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]] && ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] intValue] == 4)) {
        //isMyConnection = YES;
        // Unblock Connection alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Undelete Connections" message:@"Are you sure you want to undelete this user?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = currentIndex;
        [alert show];
//    } else {
//        isMyConnection = NO;
//        // Delete Connection alert
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Connections" message:@"Are you sure you want to delete this user?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
//        alert.tag = currentIndex;
//        [alert show];
//    }
}
#pragma mark
#pragma mark AlertView Delegates
#pragma mark

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        
        NSDictionary *userDict = DeletUserArray[currentIndex];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[kKeyUserID] = userDict[kKeyId];//connectionParams[kKeyConnectionID];
        params[kKeyType] = @"delete";
        params[kKeyIsContact] = @0;
        params[kKeyStatus] = @0;
        NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
    
         [APP_DELEGATE showActivityIndicator];
         [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];

        //NSDictionary *userDict = DeletUserArray[currentIndex];
//        NSMutableDictionary *params = [NSMutableDictionary dictionary];
//        params[kKeyUserID] = server.loggedInUserInfo[kKeyId];
//        params[kKeyConnectionID] = userDict[kKeyId];
//        params[kKeyConnectionStatus] = @2;
//        NSString *strUrl;
//        if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]] && ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] intValue] == 4)) {
//            NSDictionary *connectionDict = userDict[kKeyConnection];
//            strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, connectionDict[kKeyId]];
//
//           /// API CALL
//            ///      [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPUT];
//
//
//        } else {
//            strUrl = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteBlockConnection, [[userDict valueForKey:kKeyConnection] valueForKey:kKeyId]];
//
//            /// API CALL
//           /// [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
//        }
        [DeletUserArray removeObjectAtIndex:alertView.tag];
        [self.tableView reloadData];
    }

}
//------------------------------numberOfSectionsInTableView:-------------------------------------//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (DeletUserArray.count == 0) {
        self.lblNoData.frame = tableView.frame;
        tableView.backgroundView = self.lblNoData;
    } else {
        tableView.backgroundView = nil;
    }
     return [DeletUserArray count];
}
//--------------------------------------- numberOfRowsInSection:---------------------------------------//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
//------------------------------------ cellForRowAtIndexPath: --------------------------------------//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        static NSString *cellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRAddNewConnectionCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRAddNewConnectionCell *customCell = (SRAddNewConnectionCell *) nibObjects[0];
            cell = customCell;
        }
    }

    //Set Selection Style None
    ((SRAddNewConnectionCell *) cell).selectionStyle = UITableViewCellSelectionStyleNone;

    // Get details and display
    NSDictionary *userDict = DeletUserArray[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];

    // Name
    ((SRAddNewConnectionCell *) cell).lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];

    // Occupation
    if ([userDict[kKeyOccupation] length] > 0)
        ((SRAddNewConnectionCell *) cell).lblOccupation.text = userDict[kKeyOccupation];
    else
        ((SRAddNewConnectionCell *) cell).lblOccupation.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");

    if ([((SRAddNewConnectionCell *) cell).lblOccupation.text isEqualToString:@""]) {
        ((SRAddNewConnectionCell *) cell).lblName.frame = CGRectMake(((SRAddNewConnectionCell *) cell).lblName.frame.origin.x, ((SRAddNewConnectionCell *) cell).lblName.frame.origin.y + 15, ((SRAddNewConnectionCell *) cell).lblName.frame.size.width, ((SRAddNewConnectionCell *) cell).lblName.frame.size.height);
    }
    // Location
    if ([userDict[kKeyAddress] length] > 0)
        ((SRAddNewConnectionCell *) cell).lblLocation.text = userDict[kKeyAddress];
    else
        ((SRAddNewConnectionCell *) cell).lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");


    [((SRAddNewConnectionCell *) cell).lblFamily setHidden:TRUE];
    [((SRAddNewConnectionCell *) cell).datingImage setHidden:TRUE];
    [((SRAddNewConnectionCell *) cell).datingLbl setHidden:TRUE];

    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
        if ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"1"] || [[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"2"]) {
            ((SRAddNewConnectionCell *) cell).addBtn.hidden = YES;
        } else
            ((SRAddNewConnectionCell *) cell).addBtn.hidden = NO;
    } else
        ((SRAddNewConnectionCell *) cell).addBtn.hidden = NO;

    ((SRAddNewConnectionCell *) cell).addBtn.tag = indexPath.row;
    // Profile image
    ((SRAddNewConnectionCell *) cell).profileImg.contentMode = UIViewContentModeScaleAspectFill;
    ((SRAddNewConnectionCell *) cell).profileImg.clipsToBounds = YES;


    // User profile image
    if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [((SRAddNewConnectionCell *) cell).profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    } else
        ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];

    // Profile image
    ((SRAddNewConnectionCell *) cell).profileImg.contentMode = UIViewContentModeScaleAspectFill;
    ((SRAddNewConnectionCell *) cell).profileImg.clipsToBounds = YES;

    // Add button target and action
    ((SRAddNewConnectionCell *) cell).addBtn.tag = indexPath.section;
    [((SRAddNewConnectionCell *) cell).addBtn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [((SRAddNewConnectionCell *) cell).addBtn addTarget:self action:@selector(inBlockBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [((SRAddNewConnectionCell *) cell).addBtn.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    ((SRAddNewConnectionCell *) cell).addBtn.layer.cornerRadius = 20;

    // Return
    return cell;
}
//------------------------------- heightForRowAtIndexPath -------------------------------------//

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

// -----------------------------  heightForHeaderInSection: -------------------------------------//

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }
    return height;
}

#pragma mark
#pragma mark - Notification Methods
#pragma mark
// --------------------------------------------------------------------------------
// getDeletedUsersSucceed:

- (void)getDeletedUsersSucceed:(NSNotification *)inNotify {
    DeletUserArray = [inNotify object];

    // Reload table
    [self.tableView reloadData];
}

//// --------------------------------------------------------------------------------
// getDeleteUsersFailed:

- (void)getDeleteUsersFailed:(NSNotification *)inNotify {


}

// --------------------------------------------------------------------------------
// deleteBlockConnectionSucceed:

- (void)deleteBlockConnectionSucceed:(NSNotification *)inNotify {
//    [SRModalClass showAlert:errMsg];
//    if (isMyConnection) {
//        isMyConnection = NO;
        [SRModalClass showAlert:@"The connection has been un-deleted successfully."];
//    } else {
//        [SRModalClass showAlert:@"Delete blocked connection successfully."];
//    }
    // Reload table
    [self.tableView reloadData];
}

// --------------------------------------------------------------------------------
// deleteBlockConnectionFailed:

- (void)deleteBlockConnectionFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:@"Sorry, Request failed"];
    [APP_DELEGATE hideActivityIndicator];
}



@end
