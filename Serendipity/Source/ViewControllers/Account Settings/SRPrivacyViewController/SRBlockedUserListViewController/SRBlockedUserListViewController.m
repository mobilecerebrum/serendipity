#import "SRModalClass.h"
#import "SRAddNewConnectionCell.h"
#import "SRBlockedUserListViewController.h"


@implementation SRBlockedUserListViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SRBlockedUserListViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        blockedUsersArray = [[NSMutableArray alloc] init];


        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getBlockedUsersSucceed:)
                              name:kKeyNotificationGetBlockedUsersSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getBlockedUsersFailed:)
                              name:kKeyNotificationGetBlockedUsersFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(deleteBlockConnectionSucceed:)
                              name:kKeyNotificationUpdateConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(deleteBlockConnectionFailed:)
                              name:kKeyNotificationUpdateConnectionFailed object:nil];
    }
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark Standard Overrides

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.BlockedUser.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];


    //Hide empty rows
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Server call Get Connection
    [server makeAsychronousRequest:kKeyClassGetBlockedConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

#pragma mark Action Methods

- (void)actionOnBack:(UIButton *)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

- (void)inBlockBtnAction:(UIButton *)sender {
    currentIndex = sender.tag;
    NSDictionary *userDict = blockedUsersArray[currentIndex];

    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]] && ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] intValue] == 4)) {
        isMyConnection = YES;
        // Unblock Connection alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unblock Connections" message:@"Are you sure you want to unblock this user?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = currentIndex;
        [alert show];
    } else {
        isMyConnection = NO;
        // Delete Connection alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Blocked Connections" message:@"Are you sure you want to delete this user from list?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = currentIndex;
        [alert show];
    }
}

#pragma mark
#pragma mark AlertView Delegates
#pragma mark

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        // Call Server API
        NSDictionary *userDict = blockedUsersArray[currentIndex];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[kKeyUserID] = userDict[kKeyId];//connectionParams[kKeyConnectionID];
        params[kKeyType] = @"block";
        params[kKeyIsContact] = @0;
        params[kKeyStatus] = @0;
        NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
        
        [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        
        [blockedUsersArray removeObjectAtIndex:currentIndex];
        [self.tableView reloadData];
        
        
////        params[kKeyUserID] = server.loggedInUserInfo[kKeyId];
////        params[kKeyConnectionID] = userDict[kKeyId];
////        params[kKeyConnectionStatus] = @2;
//        NSString *strUrl;
//        if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]] && ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] intValue] == 4)) {
//            NSDictionary *connectionDict = userDict[kKeyConnection];
//            strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, connectionDict[kKeyId]];
//            [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPUT];
//
//
//        } else {
//            strUrl = [NSString stringWithFormat:@"%@/%@", kKeyClassDeleteBlockConnection, [[userDict valueForKey:kKeyConnection] valueForKey:kKeyId]];
//            [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
//        }
//        [blockedUsersArray removeObjectAtIndex:alertView.tag];


//        NSMutableDictionary *params = [NSMutableDictionary dictionary];
//        [params setObject:[server.loggedInUserInfo objectForKey:kKeyId] forKey:kKeyUserID];
//        [params setObject:[userDict objectForKey:kKeyId] forKey:kKeyConnectionID];
//        [params setObject:[NSNumber numberWithInt:2] forKey:kKeyConnectionStatus];
//        NSString *strUrl = kKeyClassAddConnection;
//        if ([[userDict objectForKey:kKeyConnection]isKindOfClass:[NSDictionary class]]) {
//            NSDictionary *connectionDict = [userDict objectForKey:kKeyConnection];
//            strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, [connectionDict objectForKey:kKeyId]];
//            [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPUT];
//        }
    }

}


#pragma mark
#pragma mark TableView DataSource Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [blockedUsersArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// ----------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        static NSString *cellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRAddNewConnectionCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRAddNewConnectionCell *customCell = (SRAddNewConnectionCell *) nibObjects[0];
            cell = customCell;
        }
    }

    //Set Selection Style None
    ((SRAddNewConnectionCell *) cell).selectionStyle = UITableViewCellSelectionStyleNone;

    // Get details and display
    NSDictionary *userDict = blockedUsersArray[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];

    // Name
    ((SRAddNewConnectionCell *) cell).lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];

    // Occupation
    if ([userDict[kKeyOccupation] length] > 0)
        ((SRAddNewConnectionCell *) cell).lblOccupation.text = userDict[kKeyOccupation];
    else
        ((SRAddNewConnectionCell *) cell).lblOccupation.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");

    if ([((SRAddNewConnectionCell *) cell).lblOccupation.text isEqualToString:@""]) {
        ((SRAddNewConnectionCell *) cell).lblName.frame = CGRectMake(((SRAddNewConnectionCell *) cell).lblName.frame.origin.x, ((SRAddNewConnectionCell *) cell).lblName.frame.origin.y + 15, ((SRAddNewConnectionCell *) cell).lblName.frame.size.width, ((SRAddNewConnectionCell *) cell).lblName.frame.size.height);
    }
    // Location
    if ([userDict[kKeyAddress] length] > 0)
        ((SRAddNewConnectionCell *) cell).lblLocation.text = userDict[kKeyAddress];
    else
        ((SRAddNewConnectionCell *) cell).lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");


    [((SRAddNewConnectionCell *) cell).lblFamily setHidden:TRUE];
    [((SRAddNewConnectionCell *) cell).datingImage setHidden:TRUE];
    [((SRAddNewConnectionCell *) cell).datingLbl setHidden:TRUE];

    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
        if ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"1"] || [[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"2"]) {
            ((SRAddNewConnectionCell *) cell).addBtn.hidden = YES;
        } else
            ((SRAddNewConnectionCell *) cell).addBtn.hidden = NO;
    } else
        ((SRAddNewConnectionCell *) cell).addBtn.hidden = NO;

    ((SRAddNewConnectionCell *) cell).addBtn.tag = indexPath.row;
    // Profile image
    ((SRAddNewConnectionCell *) cell).profileImg.contentMode = UIViewContentModeScaleAspectFill;
    ((SRAddNewConnectionCell *) cell).profileImg.clipsToBounds = YES;


    // User profile image
    if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [((SRAddNewConnectionCell *) cell).profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    } else
        ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];

    // Profile image
    ((SRAddNewConnectionCell *) cell).profileImg.contentMode = UIViewContentModeScaleAspectFill;
    ((SRAddNewConnectionCell *) cell).profileImg.clipsToBounds = YES;

    // Add button target and action
    ((SRAddNewConnectionCell *) cell).addBtn.tag = indexPath.section;
    [((SRAddNewConnectionCell *) cell).addBtn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [((SRAddNewConnectionCell *) cell).addBtn addTarget:self action:@selector(inBlockBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [((SRAddNewConnectionCell *) cell).addBtn.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    ((SRAddNewConnectionCell *) cell).addBtn.layer.cornerRadius = 20;

    // Return
    return cell;
}

#pragma mark
#pragma mark Tableview Delegates
#pragma mark
// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }

    // Return
    return height;
}

#pragma mark
#pragma mark - Notification Methods
#pragma mark
// --------------------------------------------------------------------------------
// getBlockedUsersSucceed:

- (void)getBlockedUsersSucceed:(NSNotification *)inNotify {
    blockedUsersArray = [inNotify object];

    // Reload table
    [self.tableView reloadData];
}

// --------------------------------------------------------------------------------
// getBlockedUsersFailed:

- (void)getBlockedUsersFailed:(NSNotification *)inNotify {


}

// --------------------------------------------------------------------------------
// deleteBlockConnectionSucceed:

- (void)deleteBlockConnectionSucceed:(NSNotification *)inNotify {

//    NSString *errMsg = [inNotify userInfo][kKeyResponse];

//    [SRModalClass showAlert:errMsg];
    if (isMyConnection) {
        isMyConnection = NO;
        [SRModalClass showAlertWithTitle:@"Connection unblocked successfully" alertTitle:@"Unblock Connections"];
        
    } else {
          [SRModalClass showAlertWithTitle:@"You successfully unblocked your connection!" alertTitle:@"Unblock Connections"];
    }
    // Reload table

    [self.tableView reloadData];
//    [SRModalClass showAlert:@"Delete blocked connection successfully."];
//    // Reload table
    [blockedUsersArray removeObjectAtIndex:currentIndex];
//    [self.tableView reloadData];
}

// --------------------------------------------------------------------------------
// deleteBlockConnectionFailed:

- (void)deleteBlockConnectionFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:@"Sorry, Request failed"];
    [APP_DELEGATE hideActivityIndicator];
}


@end
