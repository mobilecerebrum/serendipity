
#import "SRProfileImageView.h"

#import <UIKit/UIKit.h>

@interface SRBlockedUserListViewController : ParentViewController
{ // Server
    SRServerConnection *server;
    NSInteger currentIndex;
    //Instance Variables
    NSMutableArray *blockedUsersArray;
    NSMutableArray *cacheImgArr;
    BOOL isMyConnection;

}

@property (weak, nonatomic) IBOutlet SRProfileImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *bckClrImg;
@property (strong, nonatomic) IBOutlet UIImageView *logoImg;

//Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *) dict server:(SRServerConnection *)inServer;

@end
