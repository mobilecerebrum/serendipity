#import "SRPrivacySettingTableViewCell.h"
#import "SRProfilePhotoViewController.h"
#import "SRBlockedUserListViewController.h"
#import "SRPrivacyViewController.h"
#import "SRGoInvisibleViewController.h"
#import "SRDeletedUserViewController.h"

#define kSliderMinValue 0
#define kSliderMaxValue 100
#define kSliderInterval 25

@implementation SRPrivacyViewController

#pragma mark Init Method

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRPrivacyViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        menuItemArray = [[NSMutableArray alloc] init];
        passDict = [NSMutableDictionary dictionary];
        deletPassDict = [NSMutableDictionary dictionary];
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getBlockedConnectionSucceed:)
                              name:kKeyNotificationGetBlockedUsersSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getBlockedConnectionFailed:)
                              name:kKeyNotificationGetBlockedUsersFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateSucceed:)
                              name:kKeyNotificationSettingUpdateSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateFailed:)
                              name:kKeyNotificationSettingUpdateFailed object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(getDeletedUsersSucceed:)
                              name:kKeyNotificationGetDeletedUsersSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getDeleteUsersFailed:)
                              name:kKeyNotificationGetDeletedUsersFailed object:nil];


    }

    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Standard Overrides

- (void)viewDidLoad {
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"lbl.privacy_setting.txt", " ") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    NSString *url = [NSString stringWithFormat:@"%@/%@", kKeyClassSetting, [server.loggedInUserInfo valueForKey:kKeyId]];
    [server makeAsychronousRequest:url inParams:nil isIndicatorRequired:NO inMethodType:kGET];

    // Table Data Array initialization
    NSUserDefaults *userDeff = [NSUserDefaults standardUserDefaults];
    NSString *strValue = [userDeff valueForKey:@"profile_photo"];
    NSString *go_invisibleStr = [[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:@"go_invisible"];

    NSString *profilePhotoStatus = @"";
    NSString *goInvisbleStatus = @"";

    // Set option for profile photo
    if (strValue != nil) {
        if ([strValue isEqualToString:@"1"]) {
            profilePhotoStatus = kKeyValueEveryone;
        } else if ([strValue isEqualToString:@"2"]) {
            profilePhotoStatus = kKeyValueMyContacts;

        } else if ([strValue isEqualToString:@"3"]) {
            profilePhotoStatus = kKeyValueMyConnections;
        } else if ([strValue isEqualToString:@"4"]) {
            profilePhotoStatus = kKeyValueMyFavConnections;
        } else if ([strValue isEqualToString:@"5"]) {
            profilePhotoStatus = kKeyValueNobody;
        }
    } else {
        profilePhotoStatus = kKeyValueEveryone;
    }

    if (go_invisibleStr != nil) {
        NSArray *go_invisibleArr = [go_invisibleStr componentsSeparatedByString:@","];
        if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:@"everyone_except"] isEqualToString:@"1"]) {
            goInvisbleStatus = @"Everyone Except";
        } else if (go_invisibleArr.count) {
            if (go_invisibleArr.count > 1) {
                goInvisbleStatus = @"Multiple";
            } else if (go_invisibleArr.count == 1) {
                if ([go_invisibleArr[0] isEqualToString:@"1"]) {
                    goInvisbleStatus = kKeyValueEveryone;
                } else {
                    goInvisbleStatus = kKeyValueNobody;
                }
            } else {
                goInvisbleStatus = kKeyValueNobody;
            }
        } else {
            goInvisbleStatus = kKeyValueNobody;
        }
    }
    NSDictionary *params = @{kKeyName: NSLocalizedString(@"lbl.Profile-Photo.txt", @""), kKeyValue: profilePhotoStatus};
    [menuItemArray addObject:params];

    params = @{kKeyName: NSLocalizedString(@"lbl.Go-Invisible.txt", @""), kKeyValue: goInvisbleStatus};
    [menuItemArray addObject:params];

    params = @{kKeyName: NSLocalizedString(@"lbl.Blocked.txt", @"")};
    [menuItemArray addObject:params];
    
    params = @{kKeyName: NSLocalizedString(@"lbl.deleted.txt", @"")};
    [menuItemArray addObject:params];


    params = @{kKeyName: NSLocalizedString(@"lbl.loc_offset.txt", @"")};
    [menuItemArray addObject:params];

    params = @{kKeyName: NSLocalizedString(@"lbl.hideLast_name.txt", @"")};
    [menuItemArray addObject:params];

    // Set footer view for hiding empty rows of table
    self.tblView.tableFooterView = [[UIView alloc] init];

}

- (void)viewWillAppear:(BOOL)animated {

    // Call super
    [super viewWillAppear:animated];

    [server makeAsychronousRequest:kKeyClassGetBlockedConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    
    [server makeAsychronousRequest:kKeyClassGetDeleteConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];

    // [self postSelectedData];

}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItemArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRPrivacySettingTableViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRPrivacySettingTableViewCell *customCell = (SRPrivacySettingTableViewCell *) nibObjects[0];
            cell = customCell;
        }
    }

    //Set Selection Style None
    ((SRPrivacySettingTableViewCell *) cell).selectionStyle = UITableViewCellSelectionStyleNone;

    // Set values
    NSDictionary *dict = menuItemArray[indexPath.row];

    ((SRPrivacySettingTableViewCell *) cell).lblName.text = dict[kKeyName];
    ((SRPrivacySettingTableViewCell *) cell).imgView.hidden = YES;

    if (indexPath.row < 2) {

        // Set selected values to the index row
        ((SRPrivacySettingTableViewCell *) cell).lblOption.text = dict[kKeyValue];
        ((SRPrivacySettingTableViewCell *) cell).imgView.hidden = NO;

        [((SRPrivacySettingTableViewCell *) cell).lblOption setAdjustsFontSizeToFitWidth:YES];
    } else if (indexPath.row == 2) {

        CGRect frame = ((SRPrivacySettingTableViewCell *) cell).lblOption.frame;
        frame.origin.y = frame.origin.y - 2;
//        frame.origin.x = cell.frame.size.width - 33;
        frame.origin.x = cell.contentView.frame.size.width - 33;
        frame.size.width = 25;
        frame.size.height = 25;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.frame = frame;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.backgroundColor = [UIColor blackColor];
        ((SRPrivacySettingTableViewCell *) cell).lblOption.layer.cornerRadius = frame.size.width / 2;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.layer.masksToBounds = YES;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.hidden = NO;
        if ([blockedConnectionList count] > 0) {
            ((SRPrivacySettingTableViewCell *) cell).lblOption.text = [NSString stringWithFormat:@"%lu", (unsigned long) [blockedConnectionList count]];
        } else {
            ((SRPrivacySettingTableViewCell *) cell).lblOption.text = @"0";
        }
        ((SRPrivacySettingTableViewCell *) cell).lblOption.textAlignment = NSTextAlignmentCenter;
    }
    else if (indexPath.row == 3) {

        CGRect frame = ((SRPrivacySettingTableViewCell *) cell).lblOption.frame;
        frame.origin.y = frame.origin.y - 2;
        //        frame.origin.x = cell.frame.size.width - 33;
        frame.origin.x = cell.contentView.frame.size.width - 33;
        frame.size.width = 25;
        frame.size.height = 25;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.frame = frame;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.backgroundColor = [UIColor blackColor];
        ((SRPrivacySettingTableViewCell *) cell).lblOption.layer.cornerRadius = frame.size.width / 2;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.layer.masksToBounds = YES;
        ((SRPrivacySettingTableViewCell *) cell).lblOption.hidden = NO;
        if ([DeletUserArray count] > 0) {
            ((SRPrivacySettingTableViewCell *) cell).lblOption.text = [NSString stringWithFormat:@"%lu", (unsigned long) [DeletUserArray count]];
        } else {
            ((SRPrivacySettingTableViewCell *) cell).lblOption.text = @"0";
        }
        ((SRPrivacySettingTableViewCell *) cell).lblOption.textAlignment = NSTextAlignmentCenter;
    }
    else if (indexPath.row == 4) {
        UIView *cellView;
        if (SCREEN_WIDTH == 320) {
            cellView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 210, 5, 205, 40)];
        } else
            cellView = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 265, 10, 260, 40)];
        cellView.backgroundColor = [UIColor clearColor];

        if (SCREEN_WIDTH == 320) {
            slider = [[ASValueTrackingSlider alloc] initWithFrame:CGRectMake(10, 5, 190, 8)];
        } else
            slider = [[ASValueTrackingSlider alloc] initWithFrame:CGRectMake(10, 5, 240, 8)];
        slider.maximumValue = kSliderMaxValue;
        slider.minimumValue = kSliderMinValue;
        if (sliderValue > 0) {
            slider.value = (sliderValue / 1000) * kSliderInterval;
        } else {
            slider.value = kSliderMinValue;
        }
        slider.minimumTrackTintColor = [UIColor blackColor];
        slider.maximumTrackTintColor = [UIColor blackColor];
        slider.popUpViewColor = [UIColor clearColor];
        slider.textColor = [UIColor clearColor];
        [slider setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        slider.backgroundColor = [UIColor blackColor];
        [slider setContinuous:NO];
        [slider addTarget:self action:@selector(setSliderRoundValue) forControlEvents:UIControlEventValueChanged];
        [cellView addSubview:slider];

        int x = 5;
        distOneLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, 20, 25, 15)];
        if (sliderValue > 0) {
            distOneLbl.textColor = [UIColor blackColor];
        } else
            distOneLbl.textColor = [UIColor whiteColor];
        distOneLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
        distOneLbl.text = @"0ft";
        distOneLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:distOneLbl];

        if (SCREEN_WIDTH == 320) {
            x = distOneLbl.frame.size.width + 10;
        } else
            x = distOneLbl.frame.size.width + 25;
        distTwoLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, 20, 35, 15)];
        distTwoLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
        if (sliderValue == 1000) {
            distTwoLbl.textColor = [UIColor whiteColor];
        } else
            distTwoLbl.textColor = [UIColor blackColor];

        distTwoLbl.text = @"1000ft";
        distTwoLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:distTwoLbl];

        if (SCREEN_WIDTH == 320) {
            x = x + distTwoLbl.frame.size.width + 10;
        } else
            x = x + distTwoLbl.frame.size.width + 25;
        distThreeLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, 20, 35, 15)];
        if (sliderValue == 2000) {
            distThreeLbl.textColor = [UIColor whiteColor];
        } else
            distThreeLbl.textColor = [UIColor blackColor];
        distThreeLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
        distThreeLbl.text = @"2000ft";
        distThreeLbl.textAlignment = NSTextAlignmentCenter;
        [cellView addSubview:distThreeLbl];

        if (SCREEN_WIDTH == 320) {
            x = x + distThreeLbl.frame.size.width + 10;
        } else
            x = x + distThreeLbl.frame.size.width + 25;
        distFourLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, 20, 35, 15)];
        if (sliderValue == 3000) {
            distFourLbl.textColor = [UIColor whiteColor];
        } else
            distFourLbl.textColor = [UIColor blackColor];
        distFourLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
        distFourLbl.text = @"3000ft";
        [cellView addSubview:distFourLbl];

        if (SCREEN_WIDTH == 320) {
            x = x + distFourLbl.frame.size.width + 10;
        } else
            x = x + distFourLbl.frame.size.width + 25;
        distFiveLbl = [[UILabel alloc] initWithFrame:CGRectMake(x, 20, 35, 15)];
        if (sliderValue == 4000) {
            distFiveLbl.textColor = [UIColor whiteColor];
        } else
            distFiveLbl.textColor = [UIColor blackColor];
        distFiveLbl.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
        distFiveLbl.text = @"4000ft";
        [cellView addSubview:distFiveLbl];
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:cellView];

        sliderLbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 50, SCREEN_WIDTH - 20, 80)];
        sliderLbl.textColor = [UIColor whiteColor];
        sliderLbl.numberOfLines = 5;
        sliderLbl.lineBreakMode = NSLineBreakByWordWrapping;
        sliderLbl.font = [UIFont fontWithName:kFontHelveticaBold size:12];
        sliderLbl.text = NSLocalizedString(@"lbl.distance_privacy.txt", @"");
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:sliderLbl];

    } else if (indexPath.row == 5) {
        UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 54, 4, 60, 25)];
//        objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
        objSwitch.transform = CGAffineTransformMakeScale(1.0, 1.0);
        objSwitch.tintColor = [UIColor whiteColor];
        [objSwitch setOnTintColor:[UIColor orangeColor]];
        objSwitch.backgroundColor = [UIColor lightGrayColor];
        objSwitch.layer.cornerRadius = 16.0f;
        objSwitch.tag = indexPath.row;
        [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyHideLastName] boolValue] == YES) {
            [objSwitch setOn:YES animated:NO];
        } else
            [objSwitch setOn:NO animated:NO];
        [((SRPrivacySettingTableViewCell *) cell) addSubview:objSwitch];
    }
//    ((SRPrivacySettingTableViewCell *)cell).imgView.hidden = YES;
    // Cell customization
    cell.tintColor = [UIColor whiteColor];

    // Return
    return cell;
}

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == 0) {
        NSDictionary *dict = menuItemArray[indexPath.row];

        SRProfilePhotoViewController *profilePhotoCon = [[SRProfilePhotoViewController alloc] initWithNibName:NSLocalizedString(@"lbl.Profile-Photo.txt", @"") bundle:nil server:server inData:dict];
        profilePhotoCon.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:profilePhotoCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 1) {
        NSDictionary *dict = menuItemArray[indexPath.row];
        SRGoInvisibleViewController *goInvisibleCon = [[SRGoInvisibleViewController alloc] initWithNibName:NSLocalizedString(@"lbl.Go-Invisible.txt", @"") bundle:nil server:server inData:dict];
        goInvisibleCon.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:goInvisibleCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 2) {
        if ([blockedConnectionList count] > 0) {
            SRBlockedUserListViewController *objBlockUsersView = [[SRBlockedUserListViewController alloc] initWithNibName:nil bundle:nil inDict:passDict server:server];
            objBlockUsersView.delegate = self;
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objBlockUsersView animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else {

        }
    }
    else if (indexPath.row == 3) {
        if ([DeletUserArray count] > 0) {
            SRDeletedUserViewController *objDeletUsersView =  [[SRDeletedUserViewController alloc] initWithNibName:nil bundle:nil inDict:deletPassDict server:server];
            objDeletUsersView.delegate = self;
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objDeletUsersView animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else {

        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 4) {
        return 130;
    }
    return 44;
}

#pragma mark
#pragma mark SRProfilePhotoViewController Delegate
#pragma mark

// -----------------------------------------------------------------------------------------------------
// dataFromSRProfilePhotoViewController

- (void)dataFromSRProfilePhotoViewController:(NSDictionary *)data {
    // Get the index of array which dictionary need to replace

    for (NSUInteger i = 0; i < 2; i++) {
        NSDictionary *dict = menuItemArray[i];
        if ([dict[kKeyName] isEqualToString:data[kKeyName]]) {
            index = i;
        }
    }

    menuItemArray[index] = data;

    [self postSelectedData];

}

#pragma mark
#pragma mark SRGoInvisibleViewController Delegate
#pragma mark

// -----------------------------------------------------------------------------------------------------
// dataFromSRGoInvisibleViewController

- (void)dataFromSRGoInvisibleViewController:(NSDictionary *)data {
    [APP_DELEGATE showActivityIndicator];
    NSString *everyExcept = data[kKeyValue];
    NSArray *go_invisibleArr = [everyExcept componentsSeparatedByString:@","];
    if ([go_invisibleArr containsObject:@"7"]) {
        NSLog(@"Everyone except");
        // update everyone except value in setting
        NSDictionary *payload = @{@"field_name": @"everyone_except",
                @"field_value": @"1"
        };
        NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];

        // Update everyone except values to list
        // Add saved setting to final array
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        NSMutableArray *arr1 = [[NSMutableArray alloc] init];
        for (int i = 0; i < go_invisibleArr.count; i++) {
            NSInteger numkey = [go_invisibleArr[i] integerValue];
            [arr addObject:[NSString stringWithFormat:@"%ld", numkey]];
        }

        if ([arr containsObject:[NSString stringWithFormat:@"%d", 8]]) {
            [arr1 addObject:[NSString stringWithFormat:@"%d", 2]];
        }
        if ([arr containsObject:[NSString stringWithFormat:@"%d", 9]]) {
            [arr1 addObject:[NSString stringWithFormat:@"%d", 3]];
        }
        if ([arr containsObject:[NSString stringWithFormat:@"%d", 10]]) {
            [arr1 addObject:[NSString stringWithFormat:@"%d", 4]];
        }
        if ([arr containsObject:[NSString stringWithFormat:@"%d", 12]]) {
            [arr1 addObject:[NSString stringWithFormat:@"%d", 7]];
        }
//        if ([arr containsObject:[NSString stringWithFormat:@"%d",12]])
//        {
//            [arr1 addObject:[NSString stringWithFormat:@"%d",7]];
//        }

        // Make key of selected values
        NSString *key = @"";
        for (int i = 0; i < arr1.count; i++) {
            //As indexpath starts from 1 is added to every indexpath.row
            NSInteger numkey = [arr1[i] integerValue];
            key = [[key stringByAppendingString:[NSString stringWithFormat:@"%ld", (long) numkey]] stringByAppendingString:@","];
        }
        if ([key length] > 0) {
            key = [key substringToIndex:[key length] - 1];
        }
        [data setValue:key forKey:kKeyValue];

        // UPDATE EVERYONE EXCEPT SETTING
        NSDictionary *payload1 = @{@"field_name": @"go_invisible",
                @"field_value": data[kKeyValue]
        };
        NSString *url1 = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url1 inParams:payload1 isIndicatorRequired:NO inMethodType:kPOST];
    } else {
        // Set everyone except to "0"
        // update everyone except value in setting
        NSDictionary *payload = @{@"field_name": @"everyone_except",
                @"field_value": @"0"
        };
        NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];

        NSDictionary *payload1 = @{@"field_name": @"go_invisible",
                @"field_value": data[kKeyValue]
        };
        NSString *url1 = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url1 inParams:payload1 isIndicatorRequired:NO inMethodType:kPOST];
    }
}

- (void)postSelectedData {

    NSDictionary *infoDict = menuItemArray[index];
    NSString *value = @"";
    NSString *name = @"";


    if ([[infoDict valueForKey:kKeyName] isEqualToString:kKeyProfilePhoto]) {

        name = @"profile_photo";

        if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.Everyone.txt", @"")]) {
            value = @"1";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyContacts.txt", @"")]) {
            value = @"2";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyConnections.txt", @"")]) {
            value = @"3";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyFavConnections.txt", @"")]) {
            value = @"4";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.Nobody.txt", @"")]) {
            value = @"5";
        }

    } else if ([[infoDict valueForKey:kKeyName] isEqualToString:kKeyGoInvisible]) {
        name = @"go_invisible";

        if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.Everyone.txt", @"")]) {
            value = @"1";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyContacts.txt", @"")]) {
            value = @"2";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyConnections.txt", @"")]) {
            value = @"3";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyFavConnections.txt", @"")]) {
            value = @"4";
        } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.Nobody.txt", @"")]) {
            value = @"5";
        }
    }
    // Call api to update settings value
    NSDictionary *payload = @{@"field_name": name,
            @"field_value": value
    };
    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];

}

- (void)postDataOfGoInvisible {
    NSString *goInvisbleStatus = @"";
    NSString *go_invisibleStr = [[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:@"go_invisible"];
    if (go_invisibleStr != nil) {
        NSArray *go_invisibleArr = [go_invisibleStr componentsSeparatedByString:@","];
        if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:@"everyone_except"] isEqualToString:@"1"]) {
            goInvisbleStatus = @"Everyone Except";
        } else if (go_invisibleArr.count) {
            if (go_invisibleArr.count > 1) {
                goInvisbleStatus = @"Multiple";
            } else if (go_invisibleArr.count == 1) {
                if ([go_invisibleArr[0] isEqualToString:@"1"]) {

                    goInvisbleStatus = kKeyValueEveryone;
                } else if ([go_invisibleArr[0] isEqualToString:@"2"]) {
                    goInvisbleStatus = kKeyValueMyContacts;
                } else if ([go_invisibleArr[0] isEqualToString:@"3"]) {
                    goInvisbleStatus = kKeyValueMyConnections;
                } else if ([go_invisibleArr[0] isEqualToString:@"4"]) {
                    goInvisbleStatus = kKeyValueMyFavConnections;
                } else if ([go_invisibleArr[0] isEqualToString:@"6"]) {
                    goInvisbleStatus = kKeyValueNobody;
                } else if ([go_invisibleArr[0] isEqualToString:@"7"]) {
                    goInvisbleStatus = kKeyValueUsersTrackMe;
                } else {
                    goInvisbleStatus = kKeyValueNobody;
                }
            } else {
                goInvisbleStatus = kKeyValueNobody;
            }
        } else {
            goInvisbleStatus = kKeyValueNobody;
        }
    }
    NSDictionary *params = @{kKeyName: NSLocalizedString(@"lbl.Go-Invisible.txt", @""), kKeyValue: goInvisbleStatus};
    menuItemArray[1] = params;
}

#pragma mark
#pragma mark UISwitch value change action
#pragma mark

- (void)changeSwitch:(UISwitch *)sender {

    NSString *hideLName;
    if ([sender isOn]) {
        hideLName = @"1";
    } else
        hideLName = @"0";

    NSDictionary *payload = @{
            kKeyFieldName: kkeyHideLastName,
            kKeyFieldValue: hideLName
    };

    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
}

#pragma mark
#pragma mark Location offset value change action
#pragma mark
// ---------------------------------------------------------------------------------------------------------------------------------
// setSliderRoundValue:

- (void)setSliderRoundValue {
    NSString *locOffset;
    // get the nearest interval value
    for (int i = kSliderMinValue; i <= kSliderMaxValue; i = i + kSliderInterval) {

        if (slider.value >= i && slider.value < i + kSliderInterval) {
            if (slider.value - i < i + kSliderInterval - slider.value) {
                slider.value = i;
            } else {
                slider.value = i + kSliderInterval;
            }
            if (slider.value == 0) {
                locOffset = @"0";
                distOneLbl.textColor = [UIColor whiteColor];
                distTwoLbl.textColor = [UIColor blackColor];
                distThreeLbl.textColor = [UIColor blackColor];
                distFourLbl.textColor = [UIColor blackColor];
                distFiveLbl.textColor = [UIColor blackColor];
            } else if (slider.value == 25) {
                locOffset = @"1000";
                distOneLbl.textColor = [UIColor blackColor];
                distTwoLbl.textColor = [UIColor whiteColor];
                distThreeLbl.textColor = [UIColor blackColor];
                distFourLbl.textColor = [UIColor blackColor];
                distFiveLbl.textColor = [UIColor blackColor];
            } else if (slider.value == 50) {
                locOffset = @"2000";
                distOneLbl.textColor = [UIColor blackColor];
                distTwoLbl.textColor = [UIColor blackColor];
                distThreeLbl.textColor = [UIColor whiteColor];
                distFourLbl.textColor = [UIColor blackColor];
                distFiveLbl.textColor = [UIColor blackColor];
            } else if (slider.value == 75) {
                locOffset = @"3000";
                distOneLbl.textColor = [UIColor blackColor];
                distTwoLbl.textColor = [UIColor blackColor];
                distThreeLbl.textColor = [UIColor blackColor];
                distFourLbl.textColor = [UIColor whiteColor];
                distFiveLbl.textColor = [UIColor blackColor];
            } else {
                locOffset = @"4000";
                distOneLbl.textColor = [UIColor blackColor];
                distTwoLbl.textColor = [UIColor blackColor];
                distThreeLbl.textColor = [UIColor blackColor];
                distFourLbl.textColor = [UIColor blackColor];
                distFiveLbl.textColor = [UIColor whiteColor];
            }
            break;
        }
    }
    NSDictionary *payload = @{
            kKeyFieldName: kkeyDistanceAccuracy,
            kKeyFieldValue: locOffset
    };

    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
    NSString *offset_lat = @"", *offset_long = @"", *offset_address = @"", *updatedLocation = @"";
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateLocation, server.loggedInUserInfo[kKeyId]];
    NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
    NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
    if (![locOffset isEqualToString:@"0"]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:latitude forKey:kKeyLattitude];
        [dict setValue:longitude forKey:kKeyLongitude];
        [dict setValue:locOffset forKey:kkeyDistanceAccuracy];
        dict = [[SRModalClass addLocationOffset:[dict mutableCopy]] mutableCopy];
        offset_lat = [dict valueForKey:kKeyLattitude];
        offset_long = [dict valueForKey:kKeyLongitude];
    } else {
        offset_lat = @"0";
        offset_long = @"0";
    }
    double sped = server.myLocation.speed * 2.23694;
    NSString *speed = [NSString stringWithFormat:@"%f", sped];
    NSString *direction = [NSString stringWithFormat:@"%f", server.myLocation.course];
    NSDictionary *params = @{kKeyLattitude: latitude, kKeyLongitude: longitude, kKeyLiveAddress: updatedLocation, kKeySpeed: speed, kKeyDirection: direction, kKeyoffsetAddreass: offset_address, kKeyoffsetLong: offset_long, kKeyoffsetLat: offset_lat};
    [server makeAsychronousRequest:urlString inParams:params isIndicatorRequired:NO inMethodType:kPUT];
}

#pragma mark
#pragma mark Notofication methods action
#pragma mark
// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:


- (void)settingUpdateSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSDictionary *requestedParams = [[inNotify userInfo] valueForKey:kKeyRequestedParams];
    NSString *proPhoto = [response valueForKey:@"profile_photo"];
    NSString *goInvisible;
    if ([[requestedParams valueForKey:@"field_name"] isEqualToString:@"everyone_except"]) {
        NSString *everyoneExcept = [response valueForKey:@"everyone_except"];
        [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:everyoneExcept forKey:@"everyone_except"];
    } else if ([[requestedParams valueForKey:@"field_name"] isEqualToString:@"go_invisible"]) {
        goInvisible = [response valueForKey:@"go_invisible"];
        [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:goInvisible forKey:@"go_invisible"];
    }
    NSString *distance = [response valueForKey:kkeyDistanceAccuracy];
    NSString *hideLastN = [response valueForKey:kkeyHideLastName];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:hideLastN forKey:kkeyHideLastName];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:proPhoto forKey:@"profile_photo"];

    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:proPhoto forKey:@"profile_photo"];
    [userDef setValue:goInvisible forKey:@"go_invisible"];
    [userDef setValue:distance forKey:kkeyDistanceAccuracy];
    [userDef synchronize];

    [self postDataOfGoInvisible];
    sliderValue = [distance integerValue];

    [self.tblView reloadData];
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:[inNotify object]];
}

// --------------------------------------------------------------------------------
// getBlockedConnectionSucceed:

- (void)getBlockedConnectionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    blockedConnectionList = [inNotify object];
    [self.tblView reloadData];
    passDict = [inNotify object];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName
                                                                   ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    blockedConnectionList = [blockedConnectionList sortedArrayUsingDescriptors:sortDescriptors];

    // Reload table
    [self.tblView reloadData];
}

// --------------------------------------------------------------------------------
// getBlockedConnectionFailed:

- (void)getBlockedConnectionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

- (void)getDeletedUsersSucceed:(NSNotification *)inNotify {
   [APP_DELEGATE hideActivityIndicator];
    DeletUserArray = [inNotify object];
    [self.tblView reloadData];
    
    deletPassDict = [inNotify object];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName
                                                                   ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    DeletUserArray = [DeletUserArray sortedArrayUsingDescriptors:sortDescriptors];
    [self.tblView reloadData];
    // Reload table
    
}

//// --------------------------------------------------------------------------------
// getDeleteUsersFailed:

- (void)getDeleteUsersFailed:(NSNotification *)inNotify {


}

@end
