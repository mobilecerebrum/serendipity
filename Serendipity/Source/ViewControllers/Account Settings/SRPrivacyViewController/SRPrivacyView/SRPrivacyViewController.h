#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRProfileImageView.h"
#import "ASValueTrackingSlider.h"

@interface SRPrivacyViewController : ParentViewController

{
	//Instance Variables
	NSMutableArray *menuItemArray;
    NSArray *blockedConnectionList;
    NSArray *DeletUserArray;
    NSMutableDictionary *passDict;
    NSMutableDictionary *deletPassDict;
    NSMutableDictionary *globalDict;
    BOOL isOnce;
    NSInteger index;
    NSInteger sliderValue;
   
    
	//Server
	SRServerConnection *server;
    
    //slider
    ASValueTrackingSlider *slider;
    UILabel *distOneLbl,*distTwoLbl,*distThreeLbl,*distFourLbl,*distFiveLbl,*sliderLbl;
}

// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *bckProfileImg;
@property (strong, nonatomic) IBOutlet UIImageView *bckClrImg;
@property (strong, nonatomic) IBOutlet UITableView *tblView;

@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
