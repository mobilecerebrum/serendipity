#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRProfileImageView.h"

#pragma mark
#pragma mark Protocol
#pragma mark

// Custom Delegate to pass Data to SRPrivacyViewController
@protocol SRProfilePhotoViewControllerDelegate <NSObject>
- (void)dataFromSRProfilePhotoViewController:(NSDictionary *)data;
@end

@interface SRProfilePhotoViewController : ParentViewController
{
    //instance variables
    NSMutableDictionary *infoDict;
	NSMutableArray *menuItemArray;
    
    NSString *navTitle;
    //Server
    SRServerConnection *server;
    NSInteger profilePhotoSwitchOn;
}

// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *bckProfileImg;
@property (strong, nonatomic) IBOutlet UIImageView *bckClrImg;
@property (strong, nonatomic) IBOutlet UITableView *tblView;

// Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
               inData:(NSDictionary *)inDetailDict;
@end
