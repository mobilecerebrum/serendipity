#import "SRProfilePhotoViewController.h"

@implementation SRProfilePhotoViewController

#pragma mark - Init Method

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
               inData:(NSDictionary *)inDetailDict {
    self = [super initWithNibName:@"SRProfilePhotoViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        navTitle = nibNameOrNil;
        infoDict = [[NSMutableDictionary alloc] initWithDictionary:inDetailDict];


    }
    return self;
}

#pragma mark - Standard Overrides

- (void)viewDidLoad {
    // call super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:navTitle forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    // left navbar button
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    // initializing menuitemarray
    menuItemArray = [@[NSLocalizedString(@"lbl.Everyone.txt", @""), NSLocalizedString(@"lbl.MyContacts.txt", @""), NSLocalizedString(@"lbl.MyConnections.txt", @""), NSLocalizedString(@"lbl.MyFavConnections.txt", @""), NSLocalizedString(@"lbl.Nobody.txt", @"")] mutableCopy];

    // Set footerview to uitableview
    self.tblView.tableFooterView = [[UIView alloc] init];

    // Save switch state for Profile Photo OR Go Invisible
    if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.Everyone.txt", @"")]) {
        profilePhotoSwitchOn = 0;
    } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyContacts.txt", @"")]) {
        profilePhotoSwitchOn = 1;
    } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyConnections.txt", @"")]) {
        profilePhotoSwitchOn = 2;
    } else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyFavConnections.txt", @"")]) {
        profilePhotoSwitchOn = 3;
    }
//    else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.MyFamily.txt", @"")])
//    {
//        profilePhotoSwitchOn = 4;
//    }
    else if ([[infoDict valueForKey:kKeyValue] isEqualToString:NSLocalizedString(@"lbl.Nobody.txt", @"")]) {
        profilePhotoSwitchOn = 5;
    }
}

#pragma mark - Action Methods

- (void)actionOnBack:(id)sender {

    if ([_delegate respondsToSelector:@selector(dataFromSRProfilePhotoViewController:)]) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:infoDict];
        [_delegate dataFromSRProfilePhotoViewController:dict];
    }

    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItemArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *) nibObjects[0];
            cell = customCell;
        }
    }

    // Manage subviews hide and unhide
    ((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuIconImage.hidden = YES;

    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;

    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame = CGRectMake(20, 10, 300, 15);
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.text = menuItemArray[indexPath.row];
    ((SRHomeMenuCustomCell *) cell).backgroundColor = [UIColor clearColor];

    // Swich
    UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 60, 4, 60, 25)];
    objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
    objSwitch.tintColor = [UIColor whiteColor];
    [objSwitch setOnTintColor:[UIColor orangeColor]];
    objSwitch.backgroundColor = [UIColor lightGrayColor];
    objSwitch.layer.cornerRadius = 16.0f;
    objSwitch.tag = indexPath.row;

    if (profilePhotoSwitchOn == indexPath.row) {
        [objSwitch setOn:YES];
    }
    [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [((SRHomeMenuCustomCell *) cell).contentView addSubview:objSwitch];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark - UISwitch value change action

- (void)changeSwitch:(UISwitch *)sender {
    // Profile Photo switch
    profilePhotoSwitchOn = sender.tag;
    [self.tblView reloadData];
    [infoDict setValue:menuItemArray[sender.tag] forKey:kKeyValue];
    [infoDict setValue:kKeyProfilePhoto forKey:kKeyName];
}


@end
