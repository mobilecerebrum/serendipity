//
//  SRTrackOnMapViewController.h
//  Serendipity
//
//  Created by xamrian on 01/11/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "SRCompassWindow.h"
@interface SRTrackOnMapViewController : ParentViewController<GMSMapViewDelegate,UIAlertViewDelegate>
{
    SRServerConnection *server;
    NSDictionary *userDict;
    GMSMarker *dropPin;
    
    GMSMarker *selectedMarker;
    NSMutableDictionary *userSettingDictionary;
    NSDictionary *userDataDict;
    SRCompassWindow *compassWindow;
    NSString *adressStr;
    BOOL isAdded;
    BOOL fromEventOrGroup;
    NSMutableAttributedString * wholeString;
    
    NSInteger delayTime;
    BOOL isDropped;
    NSNumber *zoomLevel;
    
    BOOL fromTrackScreen;
    BOOL isPanoramaLoaded;
    IBOutlet UIButton *streetViewBtn;
    CLLocationCoordinate2D panoramaLastLoc;
   
}
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong) NSTimer *updateTimer;


#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
