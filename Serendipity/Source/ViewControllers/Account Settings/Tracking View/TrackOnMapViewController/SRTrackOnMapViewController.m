//
//  SRTrackOnMapViewController.m
//  Serendipity
//
//  Created by xamrian on 01/11/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRTrackOnMapViewController.h"
#import "SRModalClass.h"
#import "SRPanoramaViewController.h"

@interface SRTrackOnMapViewController ()

@end

@implementation SRTrackOnMapViewController

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil profileData:(NSDictionary *)inProfileDict server:(SRServerConnection *)inServer {
    // Call super
    if ([nibNameOrNil isEqualToString:@"fromTrackScreen"]) {
        fromTrackScreen = YES;
    }

    self = [super initWithNibName:@"SRTrackOnMapViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        userDict = inProfileDict;
    }
    // Register notification
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(calltoGetUserLocationSucceed:)
                          name:kKeyNotificationSearchUsersByIdSucceed
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(calltoGetUserLocationFailed:)
                          name:kKeyNotificationSearchUsersByIdFailed
                        object:nil];

    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark Standard Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    // Set title
    [SRModalClass setNavTitle:NSLocalizedString(@"Tracking", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];

    _mapView.myLocationEnabled = YES;
    _mapView.settings.myLocationButton = YES;
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    //calltoGetUserLocation
    NSDictionary *locDict = [[userDict valueForKey:kKeyReference] valueForKey:kKeyLocation];
    CLLocationCoordinate2D currentloc = {[[locDict valueForKey:kKeyLattitude] floatValue], [[locDict valueForKey:kKeyRadarLong] floatValue]};
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentloc.latitude longitude:currentloc.longitude zoom:18];
    [self.mapView animateToCameraPosition:camera];

    [self calltoGetUserLocation];
//    [self calculateTimerDelay];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:panoramaLastLoc.latitude longitude:panoramaLastLoc.longitude zoom:5];
        [self.mapView animateToCameraPosition:camera];
    }
}

#pragma mark
#pragma mark Action Method
#pragma mark

// ----------------------------------------------------------------------------------------------
// stopTimer
- (void)backBtnAction {
    if (fromTrackScreen) {
        [self.navigationController popViewControllerAnimated:NO];
    } else {
        [_updateTimer invalidate];
        [self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:NO];
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
    }
}

// ----------------------------------------------------------------------------------------------
// stopTimer
- (void)stopTimer {
    [_updateTimer invalidate];
}

#pragma mark
#pragma mark Stret view Action Method
#pragma mark

//
// -----------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
}

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
    NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [_mapView.projection coordinateForPoint:touchPoint];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    // Zoom in one zoom level
                    [_mapView animateToZoom:12];
                    [_mapView animateToBearing:90];
                    [_mapView animateToViewingAngle:45];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {

                    if (!(CGRectContainsPoint(streetViewBtn.frame, touchPoint))) {
                        if (!isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapView addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [_mapView.projection coordinateForPoint:point];
    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    // Zoom in one zoom level
                    [_mapView animateToZoom:12];
                    [_mapView animateToBearing:90];
                    [_mapView animateToViewingAngle:45];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}


#pragma mark
#pragma mark Private Method
#pragma mark

// ----------------------------------------------------------------------------------------------
// calltoGetUserLocation
- (void)calltoGetUserLocation {
    NSString *urlStr = [NSString stringWithFormat:@"%@%@", kKeyClassSearchUsersById, userDict[kKeySenderId]];
    [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

- (void)displayMarker:(NSDictionary *)Dict {
    NSDictionary *locDict = [Dict valueForKey:kKeyLocation];
    if ([locDict isKindOfClass:[NSDictionary class]]) {
        if (locDict.count > 0) {
            if ([locDict valueForKey:kKeyLattitude] != nil && [locDict valueForKey:kKeyRadarLong] != nil) {
                CLLocationCoordinate2D currentloc = {[[locDict valueForKey:kKeyLattitude] floatValue], [[locDict valueForKey:kKeyRadarLong] floatValue]};
                if (!isDropped) {
                    isDropped = YES;

                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentloc.latitude longitude:currentloc.longitude zoom:18];
                    [self.mapView animateToCameraPosition:camera];

                    // Create current selected marker
                    dropPin.map = nil;
                    dropPin = [[GMSMarker alloc] init];
                    dropPin.position = currentloc;
                    dropPin.title = [[Dict[kKeyFirstName] stringByAppendingString:@" "] stringByAppendingString:Dict[kKeyLastName]];
                    dropPin.snippet = [Dict[kKeyLocation] objectForKey:kKeyLiveAddress];
                    //dropMarker.icon = [UIImage imageNamed:@"radar-pin-black-30px"];
                    dropPin.userData = Dict;
                    dropPin.icon = [UIImage imageNamed:@"radar-pin-orange-30px"];
                    dropPin.appearAnimation = kGMSMarkerAnimationPop;
                    dropPin.map = _mapView;
                } else {
                    [CATransaction begin];
                    [CATransaction setAnimationDuration:4.0];
                    NSLog(@"ZoomLevel:%f", self.mapView.camera.zoom);
//                        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentloc.latitude  longitude:currentloc.longitude zoom:self.mapView.camera.zoom];
//                        [self.mapView animateToCameraPosition:camera];
                    [self.mapView animateToLocation:currentloc];
                    dropPin.icon = [UIImage imageNamed:@"radar-pin-orange-30px"];
                    dropPin.position = currentloc;
                    dropPin.appearAnimation = kGMSMarkerAnimationPop;
                    [CATransaction commit];
                }

            }
        }
    }

}

- (void)calculateTimerDelay {
    if (_updateTimer && [_updateTimer isValid]) {
        // Don't Do anything

        NSLog(@"");
    } else {

        // Set the Timer with loop

        NSString *userBatterySetting = [[userSettingDictionary valueForKey:kKeySetting] valueForKey:kkeyBatteryUsage];
        int updateDuration = 0;

        if ([userBatterySetting isEqualToString:NSLocalizedString(@"lbl.minimal.txt", @"")]) {
            updateDuration = 0;
        } else if ([userBatterySetting isEqualToString:NSLocalizedString(@"lbl.very_low.txt", @"")]) {
            updateDuration = 34;
        } else if ([userBatterySetting isEqualToString:NSLocalizedString(@"lbl.low.txt", @"")]) {
            updateDuration = 68;
        } else if ([userBatterySetting isEqualToString:NSLocalizedString(@"lbl.medium.txt", @"")]) {
            updateDuration = 102;
        } else if ([userBatterySetting isEqualToString:NSLocalizedString(@"lbl.high.txt", @"")]) {
            updateDuration = 136;
        } else if ([userBatterySetting isEqualToString:NSLocalizedString(@"lbl.real_time.txt", @"")]) {
            updateDuration = 170;
        } else {

            updateDuration = 136;
        }

        delayTime = 0;
        if (updateDuration == 0) {
            delayTime = 0;
        } else if (updateDuration == 34) {
            delayTime = (60 * 60);
        } else if (updateDuration == 68) {
            delayTime = (30 * 60);
        } else if (updateDuration == 102) {
            delayTime = (5 * 60);
        } else if (updateDuration == 136) {
            delayTime = 60;
        } else if (updateDuration == 170) {
            delayTime = 10;
        }

        if (delayTime > 0) {
            // Get updated location after given time
            [_updateTimer invalidate];

            if ([self respondsToSelector:@selector(calltoGetUserLocation)]) {
                _updateTimer = [NSTimer scheduledTimerWithTimeInterval:delayTime
                                                                target:self selector:@selector(calltoGetUserLocation)
                                                              userInfo:nil repeats:YES];
            }
        }
    }
}

// --------------------------------------------------------------------------------
// displayDayTime:
- (NSString *)displayDayTime {
    NSDate *currentDate;
    currentDate = [SRModalClass returnFullDate:[[userDataDict valueForKey:kKeyLocation] valueForKey:kKeyLastSeenDate]];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components

    NSInteger monthNumber = [components month]; //gives you month
    NSInteger day = [components day]; //gives you day
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [df monthSymbols][monthNumber - 1];

    NSDateFormatter *time = [[NSDateFormatter alloc] init];

    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    //    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    //    [time setTimeZone:utcTimeZone];
    //    [time setTimeZone:[NSTimeZone localTimeZone]];//change time zone

    [time setDateFormat:@"hh:mm a"];
    [time setAMSymbol:@"am"];

    [time setPMSymbol:@"pm"];
    NSString *timeString = [time stringFromDate:currentDate];


    NSString *dayTime = [NSString stringWithFormat:@"%@ %ld at %@ %@ near", monthName, (long) day, timeString, localAbbreviation];

    return dayTime;
    //    compassWindow.title.text = dayTime;
}

// --------------------------------------------------------------------------------
// getAddressFromLatLon:
- (void)getAddressFromLatLon:(CLLocation *)bestLocation {
    adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");;

    //Call Google API for correct distance and address from two locations
    if (server.myLocation != nil) {

        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:bestLocation.coordinate.latitude longitude:bestLocation.coordinate.longitude];


        NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    if (!error) {
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                        if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                            //parse data
                            NSArray *rows = json[@"rows"];
                            NSDictionary *rowsDict = rows[0];
                            NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                            NSDictionary *dict = dataArray[0];
                            NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                            NSString *distance = [distanceDict valueForKey:@"text"];


                            //Live Address
                            NSArray *destinationArray = json[@"destination_addresses"];
                            adressStr = [NSString stringWithFormat:@"in %@", destinationArray[0]];
                            if ([distance floatValue] <= 0) {
                                adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                            }
                        }
                    }
                }];
    }
}

#pragma mark
#pragma mark GMSMapView Delegate and Datasource
#pragma mark

// --------------------------------------------------------------------------------
// didTapMarker:
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {

    //change pin location from center to left side
    CGPoint point = [_mapView.projection pointForCoordinate:marker.position];
    point.x = point.x + SCREEN_WIDTH / 2 - 20;
    GMSCameraUpdate *camera =
            [GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
    [_mapView animateWithCameraUpdate:camera];

    // Check for marker in the list if same marker got tapped then remove the select object
    if (marker.userData != nil) {
        selectedMarker = marker;
        marker.icon = [UIImage imageNamed:@"radar-pin-black-30px"];
        selectedMarker.userData = userDataDict;

        CGPoint markerPoint = [self.mapView.projection pointForCoordinate:marker.position];
        CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
        CGRect frame = CGRectMake(pointInViewCoords.x - 20, pointInViewCoords.y - 55, 300, 48
        );


        if (compassWindow == nil) {

            compassWindow = [[SRCompassWindow alloc] initWithFrame:frame];
        } else {
            compassWindow.userDict = userDataDict;
            compassWindow.frame = frame;
        }


        if ([userDataDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {

            if ([userDataDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDataDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [compassWindow.profilePic sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];


        //Set DayTime
        [self displayDayTime];

        //Set Location Address


        //Set Location Address
        if (userDataDict[kKeyLocation] != nil && ![userDataDict[kKeyLocation] isKindOfClass:[NSNull class]]) {
            if ([userDataDict[kKeyLocation] objectForKey:kKeyLiveAddress] != nil && [[userDataDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 0 && ![[userDataDict[kKeyLocation] objectForKey:kKeyLiveAddress] isKindOfClass:[NSNull class]]) {
                adressStr = [NSString stringWithFormat:@"%@.", [userDataDict[kKeyLocation] objectForKey:kKeyLiveAddress]];
            } else {
                adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");
            }
        }


        NSString *windowText = [NSString stringWithFormat:@"%@ \n %@", [self displayDayTime], adressStr];
        wholeString = [[NSMutableAttributedString alloc] initWithString:windowText];
        NSRange range1 = [windowText rangeOfString:[self displayDayTime]];
        NSDictionary *attrDict1 = @{
                NSFontAttributeName: [UIFont boldSystemFontOfSize:13],
                NSForegroundColorAttributeName: [UIColor whiteColor]
        };
        [wholeString addAttributes:attrDict1 range:range1];
        NSRange range2 = [windowText rangeOfString:adressStr];
        NSDictionary *attrDict2 = @{
                NSFontAttributeName: [UIFont boldSystemFontOfSize:13],
                NSForegroundColorAttributeName: [UIColor orangeColor]
        };
        [wholeString addAttributes:attrDict2 range:range2];
        compassWindow.txtViewAddr.attributedText = wholeString;

        if (adressStr.length > 0) {
            //                        [compassWindow.txtViewAddr sizeToFit];
            CGRect txtViewFrame = compassWindow.txtViewAddr.frame;
            CGRect compassWindowFrame = compassWindow.frame;
            compassWindowFrame.size.width = SCREEN_WIDTH - 10;
            //                        compassWindowFrame.size.height = txtViewFrame.size.height + 10;
            //                        compassWindowFrame.size.height = 65;
            compassWindow.txtViewAddr.scrollEnabled = YES;
            compassWindow.txtViewAddr.contentSize = [compassWindow.txtViewAddr sizeThatFits:compassWindow.txtViewAddr.frame.size];
            compassWindowFrame.size.height = txtViewFrame.size.height + 10;
            compassWindow.frame = compassWindowFrame;
        }

        if (!isAdded) {
            isAdded = YES;
            compassWindow.hidden = NO;
            [self.mapView addSubview:compassWindow];
        } else {
            compassWindow.hidden = NO;
        }
    }
    [compassWindow.txtViewAddr scrollRangeToVisible:NSMakeRange(0, 0)];

    return YES;
}

// --------------------------------------------------------------------------------
// didChangeCameraPosition:

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    CGRect frame = CGRectMake(pointInViewCoords.x - 20, pointInViewCoords.y - 55, compassWindow.frame.size.width, compassWindow.frame.size.height
    );

    if (compassWindow == nil) {
        compassWindow = [[SRCompassWindow alloc] initWithFrame:frame];
    } else
        compassWindow.frame = frame;
}

// --------------------------------------------------------------------------------
// idleAtCameraPosition:

// This method gets called whenever the map was moving but has now stopped

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    CGRect frame = CGRectMake(pointInViewCoords.x - 20, pointInViewCoords.y - 55, compassWindow.frame.size.width, compassWindow.frame.size.height
    );

    if (compassWindow == nil) {
        compassWindow = [[SRCompassWindow alloc] initWithFrame:frame];
    } else
        compassWindow.frame = frame;
}

// --------------------------------------------------------------------------------
// didTapAtCoordinate:

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    if (selectedMarker) {
        //selectedMarker = nil;
        compassWindow.hidden = YES;
        selectedMarker.icon = [UIImage imageNamed:@"radar-pin-orange-30px"];
    }
}


#pragma mark Notification Method

// --------------------------------------------------------------------------------
// calltoGetUserLocationSucceed:
- (void)calltoGetUserLocationSucceed:(NSNotification *)inNotify {
    NSArray *responseArray = [inNotify object];
    if (responseArray.count > 0) {
        NSDictionary *dict = responseArray[0];
        if (dict) {
            userDataDict = dict;
            // Set marker to map
            if (fromTrackScreen) {
                if ([userDict[kKeyReference] isKindOfClass:[NSDictionary class]] && ([[[userDict[kKeyReference] objectForKey:kKeyLocation] objectForKey:kKeyLattitude] isEqualToString:dict[kKeyLattitude]] || [[[userDict[kKeyReference] objectForKey:kKeyLocation] objectForKey:kKeyRadarLong] isEqualToString:dict[kKeyRadarLong]])) {
                    [self displayMarker:dict];
                } else {

                    [self displayMarker:dict];
                }
            } else {
                [self displayMarker:dict];
            }
        }
    }
}


// --------------------------------------------------------------------------------
// calltoGetUserLocationFailed:
- (void)calltoGetUserLocationFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];

}


@end
