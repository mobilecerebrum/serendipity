//
//  SRConnectionAmTrackingVC.m
//  Serendipity
//
//  Created by Leo on 31/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRConnectionAmTrackingVC.h"
#import "SRModalClass.h"
#import "SRConnectionTrackingCell.h"
#import "SRAddConnectionTabViewController.h"
#import "SRNotifyMeViewController.h"
#import "SRDistanceClass.h"
#import "NotifyMeVC.h"
#import "SRTrackBtnCellTableViewCell.h"
#import "SRlocationHistoryVC.h"
#import "UIViewController+UIViewController_Custom_Methods.h"


@interface SRConnectionAmTrackingVC ()

@end

@implementation SRConnectionAmTrackingVC

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRConnectionAmTrackingVC" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        server = inServer;
        reloadSection = [[NSNumber alloc] init];

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(connectionActionSucceed:)
                              name:kKeyNotificationConnImTrackSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(connectionActionFailed:)
                              name:kKeyNotificationConnImTrackFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(updateConnectionSucceed:)
                              name:kKeyNotificationAllowCancelTrackSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateConnectionFailed:)
                              name:kKeyNotificationAllowCancelTrackFailed object:nil];
    }

    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    _amTrackingDistanceQueue = [[NSOperationQueue alloc] init];

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.connectionMTrack.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    //Creating and Adding a blurring overlay view
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.imgScreenBackGroundImage.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark
        ];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.tintColor = [UIColor clearColor];
        blurEffectView.frame = self.imgScreenBackGroundImage.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self.imgScreenBackGroundImage addSubview:blurEffectView];
    }


    self.lblNoDataFound = [[UILabel alloc] init];
    self.lblNoDataFound.textAlignment = NSTextAlignmentCenter;
    self.lblNoDataFound.textColor = [UIColor orangeColor];
    [self.lblNoDataFound setFont:[UIFont fontWithName:kFontHelveticaMedium size:15]];
    self.lblNoDataFound.text = NSLocalizedString(@"lbl.noConnectionImTrack.txt", @"");
    self.lblNoDataFound.numberOfLines = 5;
    [self.lblNoDataFound sizeToFit];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    // Call api
    [APP_DELEGATE showActivityIndicator];
    connectionListArr = [[NSMutableArray alloc] init];
    [server makeAsychronousRequest:kkeyClassGetImTrackingList inParams:nil isIndicatorRequired:NO inMethodType:kGET];

}

#pragma mark - Action Method

- (void)actionOnBack:(id)sender {
    [APP_DELEGATE hideActivityIndicator];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)plusBtnAction:(id)sender {
    SRAddConnectionTabViewController *newConn = [[SRAddConnectionTabViewController alloc] initWithNibName:kKeyIsFromTracking bundle:nil];
    self.navigationController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:newConn animated:NO];
    self.navigationController.hidesBottomBarWhenPushed = YES;
}

- (void)actionOnBtnCloseRequest:(UIButton *)sender {
    reloadSection = @(sender.tag);
    NSDictionary *cellDataDict = connectionListArr[[sender tag]];

    // Call API here to update close pending request
    if ([cellDataDict[kKeyTracking] isKindOfClass:[NSArray class]]) {
        NSMutableArray *trackingIdsArray = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in cellDataDict[kKeyTracking]) {
            NSDictionary *params = @{kKeyId: [dict valueForKey:kKeyId]};
            [trackingIdsArray addObject:params];
        }

        //

        NSData *json = [NSJSONSerialization dataWithJSONObject:trackingIdsArray options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSMutableDictionary *payload = [@{@"deleteids": jsonString} mutableCopy];
        //
//         NSDictionary *payload = [[cellDataDict objectForKey:kKeyTracking] objectAtIndex:0];
//         [payload setValue:@"2" forKey:kKeyStatus];

        NSString *url = [NSString stringWithFormat:@"%@/%@", kkeyClassGetImTrackingList, [cellDataDict valueForKey:kKeyId]];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:YES inMethodType:kDELETE];
    }

}
//jonish history
- (void)openTrackingHistoryScreen:(UIButton *)sender {
    NSMutableDictionary *obj =  connectionListArr[sender.tag];
    self.hidesBottomBarWhenPushed = YES;
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"LocationHistory" bundle:nil];
        SRlocationHistoryVC *controller = [s instantiateViewControllerWithIdentifier:@"SRlocationHistoryVC"];
    (APP_DELEGATE).selectedConnection = obj;
        [self.navigationController pushViewController:controller animated:true];
}
//jonish history
#pragma mark - Switch Method

- (void)actionOnSwitch:(UISwitch *)sender {
    reloadSection = @(sender.tag);
    NSDictionary *cellDataDict = connectionListArr[sender.tag];
    if ([sender isOn]) {
        // Allow notification YES
        NSDictionary *payload = cellDataDict[kKeyAllowImTracking];
        NSDictionary *param = @{kKeyAllowImTracking: @1};
        NSString *url = [NSString stringWithFormat:@"%@%@", kkeyClassAllowNotification, [cellDataDict valueForKey:kKeyId]];
        [server makeAsychronousRequest:url inParams:param isIndicatorRequired:YES inMethodType:kPUT];
    } else {
        // Allow notification NO
        NSDictionary *payload = cellDataDict[kKeyAllowImTracking];
        NSDictionary *param = @{kKeyAllowImTracking: @0};
        NSString *url = [NSString stringWithFormat:@"%@%@", kkeyClassAllowNotification, [cellDataDict valueForKey:kKeyId]];
        [server makeAsychronousRequest:url inParams:param isIndicatorRequired:YES inMethodType:kPUT];
    }

}

#pragma mark - TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (connectionListArr.count == 0) {

        self.lblNoDataFound.frame = tableView.frame;
        tableView.backgroundView = self.lblNoDataFound;
        return 0;

    }else {
        tableView.backgroundView = nil;
        return connectionListArr.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"Cell1";
    SRConnectionTrackingCell *customCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];

    NSDictionary *connDict = connectionListArr[indexPath.section];
    connDict = [SRModalClass removeNullValuesFromDict:connDict];
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionTrackingCell" owner:self options:nil];

    if ([nibObjects count] > 0) {
        customCell = (SRConnectionTrackingCell *) nibObjects[0];
        
        NSMutableArray *leftUtilityButtons = [NSMutableArray new];
               [leftUtilityButtons sw_addUtilityButtonWithColor: [UIColor redColor] icon:[UIImage imageNamed:@"close-icon.png"]];
                        
                  customCell.leftUtilityButtons = leftUtilityButtons;
                  customCell.delegate = self;
        
        //jonish history
        SRTrackBtnCellTableViewCell *locViewSecond = [[[NSBundle mainBundle] loadNibNamed:@"SRTrackBtnCellTableViewCell" owner:self options:nil] objectAtIndex:0];
        locViewSecond.trackBtnOutlet.tag = indexPath.row;
        locViewSecond.trackBtnOutlet.frame = CGRectMake(0, customCell.locationView.frame.origin.y, self.view.frame.size.width, 45);
        [locViewSecond.trackBtnOutlet addTarget:self action:@selector(openTrackingHistoryScreen:) forControlEvents:UIControlEventTouchUpInside];
        locViewSecond.frame = customCell.locationView.bounds;
        //jonish history
        [customCell.locationView addSubview:locViewSecond];
        NSString *occupation = connDict[kKeyOccupation];
        if (occupation.length > 0) {
            customCell.lblDetailsLabel.text = connDict[kKeyOccupation];
        } else
            customCell.lblDetailsLabel.text = @"Occupation not available";

        customCell.lblProfileNameLabel.text = [NSString stringWithFormat:@"%@ %@", connDict[kKeyFirstName], connDict[kKeyLastName]];

        //set Address here
        if (!isNullObject(connDict[kKeyLocation])) {
            if ([[connDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 1) {
                customCell.lblAddressLabel.text = [connDict[kKeyLocation] objectForKey:kKeyLiveAddress];
            } else {
                customCell.lblAddressLabel.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
            }

        } else {
            customCell.lblAddressLabel.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        }
        // If not family
        customCell.lblfamily.hidden = YES;
        CGRect frame;
        frame = customCell.imgProfileImage.frame;
        frame.origin.x = 0;
        customCell.imgProfileImage.frame = frame;
        //}

        // User image
        if ([connDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([connDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [connDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [customCell.imgProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];


        // distance
        [customCell.lblNotificationLabel setText:@"Notifications"];

        // Switch action
        customCell.notificationSwitch.layer.cornerRadius = 16.0;
        customCell.notificationSwitch.tag = indexPath.section;
        [customCell.notificationSwitch addTarget:self action:@selector(actionOnSwitch:) forControlEvents:UIControlEventValueChanged];

        if ([connDict[kKeyAllowImTracking] isKindOfClass:[NSDictionary class]]) {
            if ([[NSString stringWithFormat:@"%@", [connDict[kKeyAllowImTracking] objectForKey:kKeyAllowImTracking]] isEqualToString:@"1"]) {
                [customCell.notificationSwitch setOn:YES];
            } else {
                [customCell.notificationSwitch setOn:NO];
            }
        } else {
            [customCell.notificationSwitch setOn:NO];
        }

        //Display Pending Request Cell
        if ([connDict[kKeyTracking] isKindOfClass:[NSArray class]]) {
            NSArray *trackLocArr = [[NSArray alloc] initWithArray:[connDict valueForKey:kKeyTracking]];
            BOOL isPending = NO;
            for (NSDictionary *trackDataDict in trackLocArr) {
                if ([trackDataDict[kKeyStatus] isEqualToString:@"0"]) {
                    isPending = YES;
                }
            }
            if (isPending) {
                customCell.btnClose.tag = indexPath.section;
                [customCell.btnClose addTarget:self action:@selector(actionOnBtnCloseRequest:) forControlEvents:UIControlEventTouchUpInside];
                customCell.lblPendingRequest.hidden = NO;
                customCell.btnClose.hidden = NO;
                customCell.imgBlackOverlay.hidden = NO;
                customCell.lblProfileNameLabel.textColor = [UIColor whiteColor];

                customCell.lblAddressLabel.hidden = YES;
                customCell.lblDetailsLabel.hidden = YES;
                customCell.lblfamily.hidden = YES;
                customCell.lblLocation.hidden = YES;
                customCell.notificationSwitch.hidden = YES;
                customCell.lblNotification.hidden = YES;
                customCell.lblNotificationLabel.hidden = YES;
            } else {
                customCell.lblPendingRequest.hidden = YES;
                customCell.btnClose.hidden = YES;
                customCell.imgBlackOverlay.hidden = YES;
            }


            // Adjust green dot label appearence
            customCell.lblOnline.hidden = YES;
            CGRect frame = customCell.lblOnline.frame;


            if ([connDict[kKeyStatus] isEqualToString:@"1"]) {
                customCell.lblOnline.hidden = NO;
                // Adjust green dot label appearence
                customCell.lblOnline.layer.cornerRadius = customCell.lblOnline.frame.size.width / 2;
                customCell.lblOnline.layer.masksToBounds = YES;
                customCell.lblOnline.clipsToBounds = YES;

                customCell.lblProfileNameLabel.frame = CGRectMake(frame.origin.x + 15, customCell.lblProfileNameLabel.frame.origin.y, customCell.lblProfileNameLabel.frame.size.width + 10, customCell.lblProfileNameLabel.frame.size.height);

            } else {
                customCell.lblOnline.hidden = YES;
                customCell.lblProfileNameLabel.frame = CGRectMake(frame.origin.x, customCell.lblProfileNameLabel.frame.origin.y, customCell.lblProfileNameLabel.frame.size.width + 10, customCell.lblProfileNameLabel.frame.size.height);
            }

        }
    }
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}



// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 145;
}

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section <= connectionListArr.count) {
        if (connectionListArr.count == 0) {
            return;
        }
        NSDictionary *userDict = connectionListArr[indexPath.section];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];
        
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"TrackingRequest" bundle:nil];
                              NotifyMeVC *controller = [s instantiateViewControllerWithIdentifier:@"NotifyMeVC"];
                               controller.server = server;
                               controller.profileDict = userDict;
                          
                              [self.navigationController pushViewController:controller animated:true];
        
        
//        SRNotifyMeViewController *notifyMe = [[SRNotifyMeViewController alloc] initWithNibName:@"SRNotifyMeViewController" bundle:nil profileData:userDict server:server];
//        [self.navigationController pushViewController:notifyMe animated:YES];
    }

}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *cellIndexPath = [self.connectionTable indexPathForCell:cell];
                                 NSInteger section = cellIndexPath.section;
                                 NSDictionary *userDict = connectionListArr[section];
                                 userDict = [SRModalClass removeNullValuesFromDict:userDict];
                                 id userId = userDict[kKeyId];
                                 int uId = [userId intValue];
     NSString *name = userDict[kKeyFirstName];
    [self showAlertWithTitle:@"Serendipity" message:[NSString stringWithFormat:@"Are you sure you want to remove your ability to track %@?", name] buttonTitle:@"Yes, Remove Tracking" cancelButtonTitle:@"No, Keep Tracking" withCompletionBlok:^(BOOL isCancelAction) {
                         
                          if (isCancelAction){
                              return;
                          }else{

                              [self deleteTrackUserById:uId];
                              [connectionListArr removeObjectAtIndex:section];
                              NSIndexSet *indexes = [[NSIndexSet alloc] initWithIndex:section];
                              [self.connectionTable deleteSections:indexes withRowAnimation:UITableViewRowAnimationLeft];
                    
                          }
           }];
}

-(void)deleteTrackUserById:(int)userId{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"receiver_id"] = [NSString stringWithFormat:@"%d", userId];
    [APP_DELEGATE showActivityIndicator];
        
    [server makeAsychronousRequest:@"tracking-user/deletebysender" inParams:params isIndicatorRequired:YES inMethodType:kPOST];
   
}

#pragma mark
#pragma mark Notification Method
#pragma mark


// --------------------------------------------------------------------------------
// connectionActionSucceed:

- (void)connectionActionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSArray class]]) {
        // Remove all objects add new
        [connectionListArr removeAllObjects];
        [connectionListArr addObjectsFromArray:object];
    }
    [self.connectionTable reloadData];

    [_amTrackingDistanceQueue cancelAllOperations];
//        [self getDistance];

}


// --------------------------------------------------------------------------------
// connectionActionFailed:

- (void)connectionActionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}


// --------------------------------------------------------------------------------
// updateConnectionSucceed:

- (void)updateConnectionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSString class]]) {
        if (reloadSection && [reloadSection integerValue] <= connectionListArr.count) {
            [self.connectionTable beginUpdates];
            [connectionListArr removeObjectAtIndex:[reloadSection integerValue]];
            [self.connectionTable deleteSections:[NSIndexSet indexSetWithIndex:[reloadSection integerValue]] withRowAnimation:UITableViewRowAnimationRight];
            [self.connectionTable endUpdates];
            reloadSection = nil;
        }
    }

}

// --------------------------------------------------------------------------------
// updateConnectionFailed:

- (void)updateConnectionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:@"Sorry. We are not able to load your connections. Please close and reopen the app and try again."];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Private Methods

- (void)callDistanceAPI:(NSDictionary *)userDict {


    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];


    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = rows[0];
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];

                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       double convertDist = [distanceWithoutCommas doubleValue];
                                       if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                           if ([distance containsString:@"km"]) {

                                           } else {
                                               //meters to killometer
                                               convertDist = convertDist / 1000;
                                           }
                                       } else {
                                           if ([distance containsString:@"km"]) {
                                               //killometer to miles
                                               convertDist = convertDist * 0.621371192;
                                           } else {
                                               //meters to miles
                                               convertDist = (convertDist * 0.000621371192);
                                           }
                                       }


                                       NSLog(@"Distance: %@", [NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit]);

                                       SRDistanceClass *obj = [[SRDistanceClass alloc] init];
                                       obj.dataId = userDict[kKeyId];
                                       NSDictionary *locationDict = userDict[kKeyLocation];
                                       obj.lat = locationDict[kKeyLattitude];
                                       obj.longitude = locationDict[kKeyRadarLong];
                                       obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit];
                                       obj.myLocation = server.myLocation;

                                       //Live Address
                                       NSArray *destinationArray = json[@"destination_addresses"];

                                       NSLog(@"Address : %@", [NSString stringWithFormat:@"in %@", destinationArray[0]]);

                                       NSUInteger indexOfUserObject = [connectionListArr indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {


                                           if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                               return YES;
                                           } else {

                                               return NO;
                                           }

                                       }];

                                       NSLog(@"%lu", (unsigned long) indexOfUserObject);


                                       NSLog(@"IndexPath : %lu", (unsigned long) indexOfUserObject);


                                       NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                       NSArray *tempArr = [(APP_DELEGATE).oldConnTrackAmArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                       obj.address = [NSString stringWithFormat:@"in %@", destinationArray[0]];
                                       if (convertDist <= 0) {
                                           obj.address = @"";
                                       }

                                       if (tempArr && tempArr.count) {

                                           NSUInteger indexOfObject = [(APP_DELEGATE).oldConnTrackAmArray indexOfObjectPassingTest:^BOOL(SRDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                                               if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                                   return YES;
                                               } else {

                                                   return NO;
                                               }

                                           }];


                                           (APP_DELEGATE).oldConnTrackAmArray[indexOfObject] = obj;

                                       } else {

                                           [(APP_DELEGATE).oldConnTrackAmArray addObject:obj];

                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^{

                                           if (indexOfUserObject != NSNotFound && indexOfUserObject < connectionListArr.count) {
                                               [self.connectionTable reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                           }
                                       });
                                   }
                               }
                           }];

}


- (void)getDistance {


    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (!(APP_DELEGATE).oldConnTrackAmArray.count) {

        [arr addObjectsFromArray:connectionListArr];

    } else {

        for (NSDictionary *userDict in connectionListArr) {

            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

            NSArray *tempArr = [(APP_DELEGATE).oldConnTrackAmArray filteredArrayUsingPredicate:predicateForDistanceObj];

            if (tempArr && tempArr.count) {

                SRDistanceClass *distanceObj = [tempArr firstObject];

                NSDictionary *locationDict = userDict[kKeyLocation];

                BOOL isBLocationChanged = NO;
                BOOL isALocationChanged = NO;

                if (![distanceObj.lat isEqualToString:locationDict[kKeyLattitude]] || ![distanceObj.longitude isEqualToString:locationDict[kKeyRadarLong]]) {

                    isBLocationChanged = YES;
                }

                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];

                if (distance >= 160) {

                    isALocationChanged = YES;

                }

                if (isBLocationChanged || isALocationChanged) {
                    [arr addObject:userDict];
                }


            } else {

                [arr addObject:userDict];

            }
        }
    }


    for (int i = 0; i < arr.count; i++) {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];


        if (server.myLocation != nil && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            NSBlockOperation *op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled) {

                    [self callDistanceAPI:userDict];
                } else {

                    NSLog(@"Operation Not canceled");
                }
            }];
            [_amTrackingDistanceQueue addOperation:op];

        }

    }

}


@end
