//
//  SRConnectionAmTrackingVC.h
//  Serendipity
//
//  Created by Leo on 31/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRServerConnection.h"

@interface SRConnectionAmTrackingVC : ParentViewController<UITableViewDelegate,UITableViewDataSource>
{
    SRServerConnection *server;
    NSMutableArray *connectionListArr;
    NSNumber *reloadSection;
    NSString *distanceUnit;
}
@property (strong,nonatomic) IBOutlet UITableView *connectionTable;
@property (strong,nonatomic) IBOutlet UIImageView *imgScreenBackGroundImage;
@property (nonatomic,strong) NSOperationQueue *amTrackingDistanceQueue;
@property (strong, nonatomic) UILabel *lblNoDataFound;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

@end
