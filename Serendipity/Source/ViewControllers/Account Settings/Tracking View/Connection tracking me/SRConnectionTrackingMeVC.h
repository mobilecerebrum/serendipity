//
//  SRConnectionTrackingMeVC.h
//  Serendipity
//
//  Created by Leo on 01/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRSwitch.h"


@interface SRConnectionTrackingMeVC : ParentViewController<UITableViewDataSource,UITableViewDelegate>
{
    SRServerConnection *server;
    NSMutableArray *connectionListArr,*expandedTrackList;
    NSInteger currentIndex;
    NSString *distanceUnit;
    NSMutableDictionary *selectedLocDict;
    CGPoint buttonPosition;
    UIButton* selectedRadioBtn;
    //jonish CR
    NSInteger selectedTrackingLocation;
    NSInteger selectedIndex;
    BOOL isExpandable;
    NSString *notificationType;
    CGFloat heightLbl;
    //jonish CR
}
@property(strong,nonatomic)IBOutlet UIImageView *imgScreenBackGroundImage;
@property (strong,nonatomic) IBOutlet UITableView *connectionTable;
@property (nonatomic,strong) NSOperationQueue *trackMeDistanceQueue;
@property (strong, nonatomic) UILabel *lblNoDataFound;
@property (weak, nonatomic) IBOutlet SRSwitch *allNotificationToggleBtn;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

@end
