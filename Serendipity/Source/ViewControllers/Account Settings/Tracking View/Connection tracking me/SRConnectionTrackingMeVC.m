//
//  SRConnectionTrackingMeVC.m
//  Serendipity
//
//  Created by Leo on 01/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRConnectionTrackingMeVC.h"
#import "SRModalClass.h"
#import "SRConnectionTrackingCell.h"
#import "SRNotifyMeViewController.h"
#import "SRDistanceClass.h"
#import "SRMyConnectionSubCell.h"
#import "SRMyConnectionSubCellTwo.h"
#import <QuartzCore/QuartzCore.h>


@interface SRConnectionTrackingMeVC ()

@end

@implementation SRConnectionTrackingMeVC

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {

    self = [super initWithNibName:@"SRConnectionTrackingMeVC" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        server = inServer;
        expandedTrackList = [[NSMutableArray alloc] init];

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(connectionActionSucceed:)
                              name:kKeyNotificationConnTrackMeSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(connectionActionFailed:)
                              name:kKeyNotificationConnTrackMeFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(connectionTrackMeSucceed:)
                              name:kKeyNotificationAllowTrackMeSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(connectionTrackMeFailed:)
                              name:kKeyNotificationAllowTrackMeFailed object:nil];

        [defaultCenter addObserver:self selector:@selector(allowCancelTrackSucceed:)
                              name:kKeyNotificationAllowCancelTrackSucceed object:nil];

        [defaultCenter addObserver:self selector:@selector(allowCancelTrackFailed:)
                              name:kKeyNotificationAllowCancelTrackFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(UpdateNotificationPlaceSucceed:)
                              name:kKeyNotificationUpdateNotificationPlaceSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(UpdateNotificationPlaceFailed:)
                              name:kKeyNotificationUpdateNotificationPlaceFailed object:nil];
        [defaultCenter addObserver:self selector:@selector(deleteTrackingSuccess:) name:kKeyNotificationDeleteTrackingSuccess object:nil];
        [defaultCenter addObserver:self selector:@selector(deleteTrackingFailure:) name:kKeyNotificationDeleteTrackingFailure object:nil];
        
        [defaultCenter addObserver:self selector: @selector(toggleAllSuccess:) name:kKeyToggleAllNotificationSuccess object:nil];
        [defaultCenter addObserver:self selector:@selector(toggleAllFailure:) name:kKeyToggleAllNotificationFailure object:nil];
        [defaultCenter addObserver:self selector:@selector(toggleSingleNotificationSuccess:) name:kKeyturnOnOffNotificationNotifySucceed object:nil];
    }

    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    heightLbl = 0.0;
    // Do any additional setup after loading the view from its nib.
    selectedLocDict = [[NSMutableDictionary alloc] init];
    _trackMeDistanceQueue = [[NSOperationQueue alloc] init];
    //jonish CR
    selectedTrackingLocation = -1;
    selectedIndex = 0;
    isExpandable = NO;
    
    //jonish CR
    self.lblNoDataFound = [[UILabel alloc] init];
    self.lblNoDataFound.textAlignment = NSTextAlignmentCenter;
    self.lblNoDataFound.textColor = [UIColor orangeColor];
    [self.lblNoDataFound setFont:[UIFont fontWithName:kFontHelveticaMedium size:16]];
    self.lblNoDataFound.text = NSLocalizedString(@"lbl.noConnectionFnd.txt", @"");

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.connectionTrackMe.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    //Creating and Adding a blurring overlay view
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.imgScreenBackGroundImage.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark
        ];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.tintColor = [UIColor clearColor];
        blurEffectView.frame = self.imgScreenBackGroundImage.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self.imgScreenBackGroundImage addSubview:blurEffectView];
    }
    // Call api
    [APP_DELEGATE showActivityIndicator];
    connectionListArr = [[NSMutableArray alloc] init];
    [server makeAsychronousRequest:kkeyClassGetConnTrackingMeList inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    
    _allNotificationToggleBtn.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"AllToggleStatus"];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;
}

#pragma mark
#pragma mark Action Method
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:
- (void)actionOnBack:(id)sender {
    [APP_DELEGATE hideActivityIndicator];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)allNotificationOffBtnAction:(id)sender {
    
    SRSwitch *onOffSwitch = (SRSwitch *) sender;
    NSString *urlStr;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"AllToggleStatus"]){
        urlStr = [NSString stringWithFormat:@"%@/%s", kkeyAllNotificationControl,"0"];
        [[NSUserDefaults standardUserDefaults] setBool:(BOOL)false forKey:@"AllToggleStatus"];
        onOffSwitch.on = false;
    }else{
        urlStr = [NSString stringWithFormat:@"%@/%s", kkeyAllNotificationControl,"1"];
        [[NSUserDefaults standardUserDefaults] setBool:(BOOL)true forKey:@"AllToggleStatus"];
        onOffSwitch.on = true;
    }
    [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}


- (void)actionOnRadioButton:(UIButton *)Sender {
    buttonPosition = [Sender convertPoint:CGPointZero toView:self.connectionTable];
    NSIndexPath *indexPath = [self.connectionTable indexPathForRowAtPoint:buttonPosition];
    currentIndex = indexPath.section;

    NSMutableArray *locArray = [[NSMutableArray alloc] init];
    if (selectedLocDict[[NSString stringWithFormat:@"%ld", (long) indexPath.section]]) {
        locArray = selectedLocDict[[NSString stringWithFormat:@"%ld", (long) indexPath.section]];
    }


    NSMutableDictionary *cellDataDict = connectionListArr[indexPath.section];
    NSArray *locationsArr = cellDataDict[kkeytracking_me];
//    BOOL isAllowed = false;
    if ([[Sender imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"radio_unchecked.png"]]) {
        [Sender setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [locArray addObject:[locationsArr[Sender.tag] valueForKey:kKeyId]];
//        isAllowed = true;
    } else {
        [Sender setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [locArray removeObject:[locationsArr[Sender.tag] valueForKey:kKeyId]];
//        isAllowed = false;
    }
    [selectedLocDict setValue:locArray forKey:[NSString stringWithFormat:@"%ld", (long) indexPath.section]];
    [self callAPIToAllowOrDisallowTracking:cellDataDict andIsAllow:YES isFromRadio:true];

//    }

}
//jonish cr
- (void)actionOnPlusBtn:(UIButton *)sender {
    isExpandable = YES;
    selectedTrackingLocation = sender.tag;
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.connectionTable];
    NSIndexPath *indexPath = [self.connectionTable indexPathForRowAtPoint:rootViewPoint];
    selectedIndex = indexPath.section;
    [self.connectionTable reloadData];
}

- (void)actionOnMinusBtn:(UIButton *)sender {
    isExpandable = NO;
    selectedTrackingLocation = -1;
    selectedIndex =0;
    [self.connectionTable reloadData];
}

- (void)actionOnOffNotificationBtn:(UIButton *)sender {
    
    //self test
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *inStr = [NSString stringWithFormat: @"%ld", (long)sender.tag];
    NSString *title = sender.titleLabel.text;
    params[kKeyTrackingIdValue] = inStr;
    if([title  isEqual: @"Turn ON Notification"]){
        params[kKeyAllowTrackNotify] = @"1";
        params[kKeyNotifications] = @"1";
    }else{
        params[kKeyAllowTrackNotify] = @"1";
        params[kKeyNotifications] = @"0";
    }
    [server makeAsychronousRequest:kkeyOnOffSingleTracking inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

- (void)actionOnDeleteBtn:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Serendipity" message:@"Do you want to delete tracking" preferredStyle:UIAlertControllerStyleAlert];
    
        //Add Buttons
    
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        selectedTrackingLocation = sender.tag;
                                        CGPoint center= sender.center;
                                        CGPoint rootViewPoint = [sender.superview convertPoint:center                           toView:self.connectionTable];
                                        NSIndexPath *indexPath = [self.connectionTable                          indexPathForRowAtPoint:rootViewPoint];
                                        selectedIndex = indexPath.section;
                                        NSDictionary *obj = connectionListArr[selectedIndex];
                                        NSArray *trackingArray = obj[kKeyTrackingArray];
                                        NSDictionary *trackingObj = trackingArray[selectedTrackingLocation];
                            
                                        NSString *urlStr = [NSString stringWithFormat:@"%@/%@",                              kkeyDeleteTrackingOnly,trackingObj[kkeyTrackingId]];
                                        
                                        [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
                                    }];
    
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
    
        //Add your buttons to alert controller
    
        [alert addAction:yesButton];
        [alert addAction:noButton];
    [self presentViewController:alert animated:true completion:NULL];
}

//jonish CR
#pragma mark - Switch Method

- (void)actionOnSwitch:(UISwitch *)sender {
    CGPoint switchPosition = [sender convertPoint:CGPointZero toView:self.connectionTable];
    NSIndexPath *indexPath = [self.connectionTable indexPathForRowAtPoint:switchPosition];
//  SRConnectionTrackingCell *cell = (SRConnectionTrackingCell *)[(UITableView *)_connectionTable cellForRowAtIndexPath:indexPath];
    currentIndex = indexPath.section;
    NSMutableDictionary *userDataDict = [[NSMutableDictionary alloc] initWithDictionary:connectionListArr[indexPath.section]];

    NSDictionary *userDict;

    //NSString *strValue = @"0";
    if ([sender isOn]) {
        //strValue = @"1";
        NSNumber *number = @(indexPath.section);
        [expandedTrackList addObject:number];

        if (userDataDict[kKeyIs_family] != nil && [userDataDict[kKeyIs_family] isKindOfClass:[NSDictionary class]]) {
            userDict = userDataDict[kKeyIs_family];
            [userDataDict[kKeyAllowTrackingMe] setValue:@"1" forKey:kKeyAllowTrackingMe];
            [userDict setValue:@"1" forKey:kKeyAllowPushNotificationTrack];
            userDataDict[kKeyIs_family] = userDict;
        } else {
            [userDataDict[kKeyAllowTrackingMe] setValue:@"1" forKey:kKeyAllowTrackingMe];
            userDict = userDataDict;
            userDataDict = [[NSMutableDictionary alloc] initWithDictionary:userDict];
        }
        // Allow notification YES
        [self callAPIToAllowOrDisallowTracking:userDataDict andIsAllow:YES isFromRadio:false];
    } else {
        NSNumber *number = @(sender.tag);
        [expandedTrackList removeObject:number];

        if (userDataDict[kKeyIs_family] != nil && [userDataDict[kKeyIs_family] isKindOfClass:[NSDictionary class]]) {
            userDict = userDataDict[kKeyIs_family];
            [userDataDict[kKeyAllowTrackingMe] setValue:@"0" forKey:kKeyAllowTrackingMe];
            [userDict setValue:@"0" forKey:kKeyAllowPushNotificationTrack];
            userDataDict[kKeyIs_family] = userDict;
        } else {
            [userDataDict[kKeyAllowTrackingMe] setValue:@"0" forKey:kKeyAllowTrackingMe];
            userDict = userDataDict;
            userDataDict = [[NSMutableDictionary alloc] initWithDictionary:userDict];
        }

        // Disallow notification
        [self callAPIToAllowOrDisallowTracking:userDataDict andIsAllow:NO isFromRadio:false];
    }
    // Reload table view
    connectionListArr[currentIndex] = userDataDict;
    [self.connectionTable reloadSections:[NSIndexSet indexSetWithIndex:currentIndex] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Switch Method

- (void)callAPIToAllowOrDisallowTracking:(NSMutableDictionary *)dict andIsAllow:(BOOL)allow isFromRadio:(BOOL)isFromRadio {
    // Allow notification YES
    NSMutableArray *locArray = [[NSMutableArray alloc] init];
    NSMutableArray *locationsArray = [[NSMutableArray alloc] initWithArray:[dict valueForKey:kkeytracking_me]];
    NSMutableArray *selectedLocationArr = selectedLocDict[[NSString stringWithFormat:@"%ld", (long) currentIndex]];
    
    NSMutableDictionary *trackDict = [[NSMutableDictionary alloc] init];
    
    if (locationsArray.count > 0) {
        // If cell is expandable it already containd seletec location id's which we checking here
        if (!isFromRadio) {
            for (int i = 0; i < locationsArray.count; i++) {
                if ([selectedLocationArr containsObject:[locationsArray[i] valueForKey:kKeyId]]) {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:@"1" forKey:kKeyNotifications];
                    [locDict setValue:@"1" forKey:kKeyStatus];
                    [locDict setValue:[locationsArray[i] valueForKey:kKeyId] forKey:kKeyId];
                    [locArray addObject:locDict];
                } else {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:@"0" forKey:kKeyNotifications];
                    [locDict setValue:@"0" forKey:kKeyStatus];
                    [locDict setValue:[locationsArray[i] valueForKey:kKeyId] forKey:kKeyId];
                    [locArray addObject:locDict];
                }
            }
        } else {
//            for (int i = 0; i < locationsArray.count; i++) {
                if ([selectedLocationArr containsObject:[locationsArray[selectedRadioBtn.tag] valueForKey:kKeyId]]) {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:@"1" forKey:kKeyNotifications];
                    [locDict setValue:@"1" forKey:kKeyStatus];
                    [locDict setValue:[locationsArray[selectedRadioBtn.tag] valueForKey:kKeyId] forKey:kKeyId];
                    [locArray addObject:locDict];
                    [trackDict setValue:@1 forKey:@"is_radio_checked"];
                } else {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:@"0" forKey:kKeyNotifications];
                    [locDict setValue:@"0" forKey:kKeyStatus];
                    [locDict setValue:[locationsArray[selectedRadioBtn.tag] valueForKey:kKeyId] forKey:kKeyId];
                    [locArray addObject:locDict];
                    [trackDict setValue:@0 forKey:@"is_radio_checked"];
                    
                }
//            }
            
        }
        
    }
    // Create json and call webservice to send locations for which you are allowing tracking
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
    [postDict setValue:locArray forKey:kKeyTracking];

    NSData *json = [NSJSONSerialization dataWithJSONObject:locArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];

    NSString *allow_tracking_me;
    if (allow) {
        allow_tracking_me = @"1";
    } else
        allow_tracking_me = @"0";
    
    if (isFromRadio) {
        [trackDict setValue:jsonString forKey:@"trackings"];
    } else {
        [trackDict setValue:@"[]" forKey:@"trackings"];
        [trackDict setValue:dict[kKeyId] forKey:@"tracker_id"];
    }
    
    [trackDict setValue:allow_tracking_me forKey:kKeyAllowTrackingMe];

    //Add list of address for tracking locations
    [server makeAsychronousRequest:kkeyClassUpdateNotificationPlace inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
}

-(NSMutableAttributedString *)getAttributedCustomStringInWhite:(NSString *)first andSecond:(NSString *)second andThird:(NSString *)third{
    NSString *strFirst = first;
    NSString *strSecond = second;
    NSString *strThird = third;
    UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@ %@",strFirst,strSecond, strThird];

    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strFirst]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:appOrangeColor
                  range:[strComplete rangeOfString:strSecond]];
        
    [attributedString addAttribute:NSFontAttributeName
                   value: [UIFont boldSystemFontOfSize:15]
                   range:[strComplete rangeOfString:strSecond]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strThird]];

     
    return attributedString;
}


-(NSMutableAttributedString *)getAttributedCustomString:(NSString *)first andSecond:(NSString *)second andThird:(NSString *)third{
    NSString *strFirst = first;
    NSString *strSecond = second;
    NSString *strThird = third;
   
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@ %@",strFirst,strSecond, strThird];

    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor blackColor]
                  range:[strComplete rangeOfString:strFirst]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor blackColor]
                  range:[strComplete rangeOfString:strSecond]];
        
    [attributedString addAttribute:NSFontAttributeName
                   value: [UIFont boldSystemFontOfSize:16]
                   range:[strComplete rangeOfString:strSecond]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor blackColor]
                  range:[strComplete rangeOfString:strThird]];

     
    return attributedString;
}

-(NSMutableAttributedString *)getAttributedSingleStr:(NSString *)first{
    NSString *strFirst = first;
   
    NSString *strComplete = [NSString stringWithFormat:@"%@",strFirst];

    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor blackColor]
                  range:[strComplete rangeOfString:strFirst]];
     
    return attributedString;
}

//-(NSString *)convertKMtoFeet:(NSString *)valueInKm{
//   
//    float value = [valueInKm floatValue];
//    float val =  value * 1000;
//     int valueInFeet = [[NSString stringWithFormat:@"%f", val] intValue];
//
//      return [NSString stringWithFormat:@"%d meter",valueInFeet];
//   
//}

-(NSString *)convertKMtoFeet:(NSString *)valueInKm{
     id unitt = (APP_DELEGATE).server.loggedInUserInfo[@"setting"][@"measurement_unit"];
     int measurementUnit = [unitt intValue];
    //1 = imperial -> Feet ,//0 = metrics ->meters
    //1km = 3280.84 feet
    //1km = 1000 meters
    //1km = 0.62 mile
    //radius is coming in kms from backend
    
    float value = [valueInKm floatValue];
    
    if (measurementUnit == 0){
        //metrics is selected ->show in meters
        float val =  value * 1000;
        int valueInFeet = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInFeet];
        return [NSString stringWithFormat:@"%d meters",roundOffValue];
        
        
    }else{
        //imperial -> Feet
        float val =  value * 3280.84;
        int valueInMeters = [[NSString stringWithFormat:@"%f", val] intValue];
          int roundOffValue = [self roundOFFvalueBy:valueInMeters];
       return [NSString stringWithFormat:@"%d feet",roundOffValue];
    }
}

-(int)roundOFFvalueBy:(int)value{
    double val = 50.0 * floor((value /50.0)+ 0.5);
    int valueRoundedOff = [[NSString stringWithFormat:@"%f", val] intValue];
    return valueRoundedOff;
}

#pragma mark - TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (connectionListArr.count == 0) {
        self.lblNoDataFound.frame = tableView.frame;
        tableView.backgroundView = self.lblNoDataFound;
        return 0;
    } else {
        tableView.backgroundView = nil;
        return connectionListArr.count;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
//jonish CR
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"Cell1";
    SRConnectionTrackingCell *customCell = (SRConnectionTrackingCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    customCell.lblDetailsLabel.text = nil;
    customCell.lblDetailsLabel.attributedText = nil;
    
    NSDictionary *connDict = connectionListArr[indexPath.section];
    connDict = [SRModalClass removeNullValuesFromDict:connDict];
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionTrackingCell" owner:self options:nil];
    if ([nibObjects count] > 0) {
        customCell = (SRConnectionTrackingCell *) nibObjects[0];
        // if expanded or allow track is enable
        if (connDict[kKeyAllowTrackingMe] != [NSNull null] && ![connDict[kKeyAllowTrackingMe] isEqual:@""]) {
            if ([[NSString stringWithFormat:@"%@", [connDict[kKeyAllowTrackingMe] objectForKey:kKeyAllowTrackingMe]] isEqualToString:@"1"] || [expandedTrackList containsObject:@(indexPath.section)]) {
                //isExpandable = YES;
                
                [customCell.notificationSwitch setOn:YES];
            }else{
                [customCell.notificationSwitch setOn:NO];
            }
            
        } else {
            [customCell.notificationSwitch setOn:NO];
        }
        if (connDict[kKeyIs_family] != nil && [connDict[kKeyIs_family] isKindOfClass:[NSDictionary class]]) {
            if ([[connDict[kKeyIs_family] objectForKey:kKeyAllowPushNotificationTrack] isEqualToString:@"1"] || [expandedTrackList containsObject:@(indexPath.section)]) {
                //isExpandable = YES;
                [customCell.notificationSwitch setOn:YES];
            } else {
                [customCell.notificationSwitch setOn:NO];
            }
        }

        // User dict
        if (connDict[kKeyAllowTrackingMe] != [NSNull null] && ![connDict[kKeyAllowTrackingMe] isEqual:@""] && [[connDict[kKeyAllowTrackingMe] objectForKey:kKeyAllowTrackingMe] isEqualToString:@"1"]) {
            // Relation
           // customCell.lblDetailsLabel.text = @"allows tracking below:";
            customCell.lblfamily.hidden = YES;
            customCell.lblAddressLabel.hidden = YES;
            customCell.imgFamilyTag.hidden = NO;
            // Name
            customCell.lblProfileNameLabel.text = [NSString stringWithFormat:@"%@ %@", connDict[kKeyFirstName], connDict[kKeyLastName]];

            CGRect frame2;
            frame2 = customCell.lblOnline.frame;
            frame2.origin.x = frame2.origin.x + 5;
            frame2.origin.y = frame2.origin.y + 5;
            customCell.lblOnline.frame = frame2;

            CGRect frame;
            frame = customCell.lblProfileNameLabel.frame;
            frame.origin.x = frame.origin.x + 10;
            frame.origin.y = frame.origin.y + 10;
            customCell.lblProfileNameLabel.frame = frame;

            CGRect frame1;
            frame1 = customCell.lblDetailsLabel.frame;
            frame1.origin.x = frame1.origin.x + 10;
            frame1.origin.y = frame1.origin.y + 10;
            customCell.lblDetailsLabel.frame = frame1;
        } else {
            //jonish cr
            CGRect frame1;
            frame1 = customCell.lblDetailsLabel.frame;
            frame1.origin.x = frame1.origin.x + 10;
            frame1.origin.y = frame1.origin.y + 10;
            customCell.lblDetailsLabel.frame = frame1;
            
            NSString *occupation = connDict[kKeyOccupation];
//            if (occupation.length > 0)
 //              customCell.lblDetailsLabel.text = @"allows tracking below:";//connDict[kKeyOccupation];
//            else
//                customCell.lblDetailsLabel.text = @"You allowed Vinodini to track you.";
//
            
            //jonish cr
            // If not family
            customCell.lblfamily.hidden = YES;
            CGRect frame;
            frame = customCell.imgProfileImage.frame;
            frame.origin.x = 0;
            customCell.imgProfileImage.frame = frame;
            //Name
            customCell.lblProfileNameLabel.text = [NSString stringWithFormat:@"%@ %@", connDict[kKeyFirstName], [connDict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)]];
            //set Address
            if (!isNullObject(connDict[kKeyLocation])) {
                if ([[connDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 1) {
                    customCell.lblAddressLabel.text = @"";//[connDict[kKeyLocation] objectForKey:kKeyLiveAddress];
                } else {
                    customCell.lblAddressLabel.text = @"";// NSLocalizedString(@"lbl.location_not_available.txt", @"");
                }

            } else {
                customCell.lblAddressLabel.text = @"";// NSLocalizedString(@"lbl.location_not_available.txt", @"");
            }
        }
        // User image
        if ([connDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([connDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [connDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [customCell.imgProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        // distance
        [customCell.lblNotificationLabel setText:@"Notifications"];
        // Switch action
        customCell.notificationSwitch.layer.cornerRadius = 16.0;
        if (connDict[kKeyAllowTrackingMe] != [NSNull null] && ![connDict[kKeyAllowTrackingMe] isEqual:@""]) {
            if ([[connDict[kKeyAllowTrackingMe] objectForKey:kKeyAllowTrackingMe] isEqualToString:@"1"] || [expandedTrackList containsObject:@(indexPath.section)]) {
                [customCell.notificationSwitch setOn:YES];
            } else {
                [customCell.notificationSwitch setOn:NO];
            }
        } else {
            [customCell.notificationSwitch setOn:NO];
        }
        customCell.notificationSwitch.tag = indexPath.section;
        [customCell.notificationSwitch addTarget:self action:@selector(actionOnSwitch:) forControlEvents:UIControlEventValueChanged];
        NSArray *locationsArr = connDict[kkeytracking_me];
        NSMutableArray *arrLocation = [[NSMutableArray alloc] init];
        for (NSUInteger i = 0; i < [locationsArr count]; i++) {
            NSDictionary *locationDict = locationsArr[i];
            locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
            if ([locationDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
                [arrLocation addObject:locationsArr[i]];
            }
        }
       //Neha CR
        NSMutableArray *arrAcceptedRequests = [NSMutableArray new];
        //location array - array having all the dictionaries.
        //arrLocation - only request(address) with status - 1(i.e. accepted)
        
        for (NSUInteger i = 0; i < [arrLocation count]; i++) {
            NSDictionary *locationDict = arrLocation[i];
           NSString *status = locationDict[@"status"];
            if ([status isEqualToString:@"1"]){
                //accepted
                [arrAcceptedRequests addObject:locationDict];
            }
        }

        arrLocation = arrAcceptedRequests;
        //Neha CR
        
    
       NSString *name = [NSString stringWithFormat:@"%@", connDict[kKeyFirstName]];
    

        if ([arrLocation count] > 0){
            if ([arrLocation count] == 1){
                
                NSMutableAttributedString *str = [self getAttributedCustomStringInWhite:@"You allowed" andSecond:name andThird:@"to track you and receive notifications for the location below:"];
                customCell.lblDetailsLabel.attributedText = str;
                
                
                // customCell.lblDetailsLabel.text = [NSString stringWithFormat:@"You allowed %@ to track you and receive notifications for the location below:", name];
            }else{
                NSMutableAttributedString *str = [self getAttributedCustomStringInWhite:@"You allowed" andSecond:name andThird:@"to track you and receive notifications for the locations below:"];
                               customCell.lblDetailsLabel.attributedText = str;
                
               //  customCell.lblDetailsLabel.text = [NSString stringWithFormat:@"You allowed %@ to track you and receive notifications for the locations below:", name];
            }

        }else{
            //it has tracking without address.
       //      customCell.lblDetailsLabel.text = [NSString stringWithFormat:@"You allowed %@ to track you", name];

            customCell.lblDetailsLabel.attributedText =  [self getAttributedCustomStringInWhite:@"You allowed" andSecond:name andThird:@"to track you."];
        }

        //if(!isExpandable){
        UIView *frameView = [[UIView alloc] init];
            for (NSUInteger i = 0; i < [arrLocation count]; i++) {
                int increment = (i == 0) ? 0 : 10;
                NSDictionary *locationDict = arrLocation[i];
                locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
                NSString *notify_on = [locationDict valueForKey:kKeyTrackNotify_On];
                
                //Last minute change
//                NSString *notify = [[locationDict valueForKey:kKeyLocation] valueForKey:kKeyTrackNotify];
                //Jonish
                NSString *notify =  [locationDict valueForKey:kKeyAllowTrackNotify];
                //jonish
                
                NSString *distance = [[locationDict valueForKey:kKeyLocation] valueForKey:kKeyRadius];
                NSString *distanceInMeter = [self convertKMtoFeet:distance];

                if (selectedTrackingLocation == i && selectedIndex == indexPath.section){
                    SRMyConnectionSubCellTwo *locViewSecond = [[[NSBundle mainBundle] loadNibNamed:@"SRMyConnectionSubCellTwo" owner:self options:nil] objectAtIndex:0];
                    //SRConnectionTrackingCell *locView = (SRConnectionTrackingCell *) nibObjects[1];
                    locViewSecond.name.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddressName]];
                    locViewSecond.address.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddress]];

                    CGFloat heightLbl = [self getLabelHeight:locViewSecond.address];
                    locViewSecond.frame = CGRectMake(0,(frameView.frame.origin.y + frameView.frame.size.height) + increment,(APP_DELEGATE).window.frame.size.width,(290 + heightLbl));
                    frameView.frame = locViewSecond.frame;
                    NSDictionary *locationDict = arrLocation[i];
                    locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
                    
                    // Add views
                    NSString *notify_on = [locationDict valueForKey:kKeyTrackNotify_On];
                    
                                    //Last minute change
                    //                NSString *notify = [[locationDict valueForKey:kKeyLocation] valueForKey:kKeyTrackNotify];
                                    //Jonish
                                    NSString *notify =  [locationDict valueForKey:kKeyAllowTrackNotify];
                                    //jonish
                    
                    UIImage *btnUnFilledImage = [UIImage imageNamed:@"UnselectedN"];

                    [locViewSecond.oneTimeOnlyRadioBtn setImage:btnUnFilledImage forState:UIControlStateNormal];
                    [locViewSecond.departureRadioBtn setImage:btnUnFilledImage forState:UIControlStateNormal];
                    [locViewSecond.arrivalRadioBtn setImage:btnUnFilledImage forState:UIControlStateNormal];
                    [locViewSecond.arrivalDepartureRadioBtn setImage:btnUnFilledImage forState:UIControlStateNormal];

                    
                    UIImage *btnImage = [UIImage imageNamed:@"selectedN"];
                    
                    if ([notify_on isEqualToString:@"3"] && [notify isEqualToString:@"2"]) {
                        [locViewSecond.arrivalDepartureRadioBtn setImage:btnImage forState:UIControlStateNormal];
                    }else if([notify_on isEqualToString:@"1"] && [notify isEqualToString:@"2"]){
                        [locViewSecond.arrivalRadioBtn setImage:btnImage forState:UIControlStateNormal];
                    }else if([notify_on isEqualToString:@"2"] && [notify isEqualToString:@"2"]){
                        
                        [locViewSecond.departureRadioBtn setImage:btnImage forState:UIControlStateNormal];
                    }else if([notify_on isEqualToString:@"1"] && [notify isEqualToString:@"1"]){
                        [locViewSecond.oneTimeOnlyRadioBtn setImage:btnImage forState:UIControlStateNormal];
                    }else if([notify_on isEqualToString:@"2"] && [notify isEqualToString:@"1"]){
                        [locViewSecond.oneTimeOnlyRadioBtn setImage:btnImage forState:UIControlStateNormal];
                    }
                    NSString *notifyId = [locationDict valueForKey:kkeyTrackingId];
                    locViewSecond.turnOnNotificationBtn.tag = [notifyId integerValue];
                    
                    NSString *notificationStatus = [locationDict valueForKey:kKeyNotifications];
                    //self test
                    if([notificationStatus  isEqual: @"0"]){
                        locViewSecond.cellDynmicTitleLbl.attributedText = [self getAttributedSingleStr:@"Notifications"];
                        locViewSecond.offLblHeight.constant = 50;
                        [locViewSecond.turnOnNotificationBtn setTitle:@"Turn ON Notification" forState:UIControlStateNormal];
                    }else{
                        locViewSecond.cellDynmicTitleLbl.text = @"Notifications";
                        locViewSecond.offLblHeight.constant = 0;
                        [locViewSecond.turnOnNotificationBtn setTitle:@"Turn OFF Notification" forState:UIControlStateNormal];
                        if ([notify_on isEqualToString:@"3"] && [notify isEqualToString:@"2"]) {
                            
                            locViewSecond.cellDynmicTitleLbl.attributedText = [self getAttributedCustomString:@"Arrival/Departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                        }else if([notify_on isEqualToString:@"1"] && [notify isEqualToString:@"2"]){
                            locViewSecond.cellDynmicTitleLbl.attributedText = [self getAttributedCustomString:@"Arrival Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                        }else if([notify_on isEqualToString:@"2"] && [notify isEqualToString:@"2"]){
                            locViewSecond.cellDynmicTitleLbl.attributedText = [self getAttributedCustomString:@"Departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                        }else if([notify_on isEqualToString:@"1"] && [notify isEqualToString:@"1"]){
                            locViewSecond.cellDynmicTitleLbl.attributedText = [self getAttributedCustomString:@"One time arrival Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                        }else if([notify_on isEqualToString:@"2"] && [notify isEqualToString:@"1"]){
                            locViewSecond.cellDynmicTitleLbl.attributedText = [self getAttributedCustomString:@"One time departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                        }
                    }
                    //self test
                    [locViewSecond.minusBtn addTarget:self action:@selector(actionOnMinusBtn:) forControlEvents:UIControlEventTouchUpInside];
                    [locViewSecond.turnOnNotificationBtn addTarget:self action:@selector(actionOnOffNotificationBtn:) forControlEvents:UIControlEventTouchUpInside];
                    [customCell.locationView addSubview:locViewSecond];
                }else{
                    SRMyConnectionSubCell *locView = [[[NSBundle mainBundle] loadNibNamed:@"SRMyConnectionSubCell" owner:self options:nil] objectAtIndex:0];
                    locView.locationLbl.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddress]];

                   CGFloat heightLbl = [self getLabelHeight:locView.locationLbl];
                    locView.frame = CGRectMake(0,(frameView.frame.origin.y + frameView.frame.size.height) + increment,(APP_DELEGATE).window.frame.size.width,(95 + heightLbl));
                    frameView.frame = locView.frame;
                    // Add views
                    locView.name.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddressName]];
                    //self test
                    if ([notify_on isEqualToString:@"3"] && [notify isEqualToString:@"2"]) {
                        
                        locView.trackingTypeLbl.attributedText = [self getAttributedCustomString:@"Arrival/Departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                    }else if([notify_on isEqualToString:@"1"] && [notify isEqualToString:@"2"]){
                        locView.trackingTypeLbl.attributedText = [self getAttributedCustomString:@"Arrival Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                    }else if([notify_on isEqualToString:@"2"] && [notify isEqualToString:@"2"]){
                        locView.trackingTypeLbl.attributedText = [self getAttributedCustomString:@"Departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                    }else if([notify_on isEqualToString:@"1"] && [notify isEqualToString:@"1"]){
                        locView.trackingTypeLbl.attributedText = [self getAttributedCustomString:@"One time arrival Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                    }else if([notify_on isEqualToString:@"2"] && [notify isEqualToString:@"1"]){
                        locView.trackingTypeLbl.attributedText = [self getAttributedCustomString:@"One time departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distanceInMeter] andThird:@"of:"];
                    }
                    //self test
                    locView.plusBtn.tag = i;
                    [locView.plusBtn addTarget:self action:@selector(actionOnPlusBtn:) forControlEvents:UIControlEventTouchUpInside];
                    locView.deleteBtn.tag = i;
                    [locView.deleteBtn addTarget:self action:@selector(actionOnDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
                    [customCell.locationView addSubview:locView];
                }
            }
//        }else{
//            for (NSUInteger i = 0; i < [arrLocation count]; i++) {
//                int increment = (i == 0) ? 0 : 2;
//                if (selectedTrackingLocation == i){
//                    SRMyConnectionSubCell *locViewSecond = [[[NSBundle mainBundle] loadNibNamed:@"SRConnectionTrackingCell" owner:self options:nil] objectAtIndex:1];
//                    //SRConnectionTrackingCell *locView = (SRConnectionTrackingCell *) nibObjects[1];
//                    locViewSecond.frame = CGRectMake(0,(i * 236) + increment,(APP_DELEGATE).window.frame.size.width,236);
//                    NSDictionary *locationDict = arrLocation[i];
//                    locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
//                    // Add views
//                    BOOL isArrive = [[locationDict valueForKey:kKeyTrackNotify_On] isEqual:@"1"];
//                    [customCell.locationView addSubview:locViewSecond];
//                }else{
//                    int increment = (i == 0) ? 0 : 2;
//                    SRMyConnectionSubCell *locView = [[[NSBundle mainBundle] loadNibNamed:@"SRMyConnectionSubCell" owner:self options:nil] objectAtIndex:0];
//                    locView.frame = CGRectMake(0,(i * 100) + increment,(APP_DELEGATE).window.frame.size.width,100);
//                    NSDictionary *locationDict = arrLocation[i];
//                    locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
//                    // Add views
//                    BOOL isArrive = [[locationDict valueForKey:kKeyTrackNotify_On] isEqual:@"1"];
//                    locView.name.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddressName]];
//
//                    if (isArrive) {
//                        locView.locationLbl.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddress]];
//                    } else {
//                        locView.locationLbl.text = [NSString stringWithFormat:@"%@", [locationDict[kKeyLocation] objectForKey:kKeyAddress]];
//                    }
//                    locView.plusBtn.tag = i;
//                    locView.indexOfLocPin.tag = indexPath.row;
//                    [locView.plusBtn addTarget:self action:@selector(actionOnPlusBtn:) forControlEvents:UIControlEventTouchUpInside];
//                    [customCell.locationView addSubview:locView];
//                }
//
//            }
//                //customCell = (SRConnectionTrackingCell *) nibObjects[1];
////                NSArray *locationsArr = connDict[kkeytracking_me];
////                NSMutableArray *arrLocation = [[NSMutableArray alloc] init];
////                for (NSUInteger i = 0; i < [locationsArr count]; i++) {
////                    NSDictionary *locationDict = locationsArr[i];
////                    locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
////                    if ([locationDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
////                        [arrLocation addObject:locationsArr[i]];
////                    }
////                }
////                NSMutableArray *selectedArray = [[NSMutableArray alloc] init];
////                if (selectedLocDict[[NSString stringWithFormat:@"%ld", (long) indexPath.section]]) {
////                    selectedArray = selectedLocDict[[NSString stringWithFormat:@"%ld", (long) indexPath.section]];
////                }
////                CGRect latestFrame;
////                for (NSUInteger i = 0; i < [arrLocation count]; i++) {
////                    NSDictionary *locationDict = arrLocation[i];
////                    locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
////                    if (i == 0) {
////                        latestFrame = CGRectMake(0, 0, customCell.frame.size.width - customCell.lblLocation.frame.size.width + 10, customCell.scrollView.frame.size.height);
////                    } else
////                        latestFrame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 2, 0, customCell.frame.size.width - customCell.lblLocation.frame.size.width + 10, customCell.scrollView.frame.size.height);
////                    // Add views
////                    UIScrollView *locatnScrollView = [[UIScrollView alloc] init];
////                    locatnScrollView.tag = indexPath.section;
////                    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10]};
////                    BOOL isArrive = [[locationDict valueForKey:kKeyTrackNotify_On] isEqual:@"1"];
////
////                    CGFloat textWidth = [[locationDict[kKeyLocation] objectForKey:kKeyAddress] sizeWithAttributes:attributes].width;
////                    UILabel *lblType = [[UILabel alloc] initWithFrame:CGRectMake(0, 7, textWidth, 18)];
////                    if (isArrive) {
////                        lblType.text = [NSString stringWithFormat:@"(Arrival) %@", [locationDict[kKeyLocation] objectForKey:kKeyAddress]];
////                    } else {
////                        lblType.text = [NSString stringWithFormat:@"(Departure) %@", [locationDict[kKeyLocation] objectForKey:kKeyAddress]];
////                    }
////
////                    lblType.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
////                    lblType.textColor = [UIColor whiteColor];
////                    lblType.backgroundColor = [UIColor clearColor];
////                    UIButton *btnView = [[UIButton alloc] initWithFrame:CGRectMake(lblType.frame.origin.x + lblType.frame.size.width + 5, 7, 25, 25)];
////                    btnView.backgroundColor = [UIColor clearColor];
////                    latestFrame.size.width = btnView.frame.origin.x + 30;
////
////                    if ([[locationsArr[i] valueForKey:kKeyStatus] isEqualToString:@"1"]) {
////                        [btnView setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
////                        if (![selectedArray containsObject:[locationsArr[i] objectForKey:kKeyId]])
////                            [selectedArray addObject:[locationsArr[i] objectForKey:kKeyId]];
////                    } else
////                        [btnView setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
////                    btnView.tag = i;
////                    // Add target
////                    [btnView addTarget:self action:@selector(actionOnRadioButton:) forControlEvents:UIControlEventTouchUpInside];
////
////                    locatnScrollView.frame = latestFrame;
////                    [locatnScrollView addSubview:lblType];
////                    [locatnScrollView addSubview:btnView];
////
////                    // Scroll content size
////                    [customCell.scrollView addSubview:locatnScrollView];
////                    customCell.scrollView.contentSize = CGSizeMake(latestFrame.origin.x + latestFrame.size.width + 2, 0);
////                    [selectedLocDict setValue:selectedArray forKey:[NSString stringWithFormat:@"%ld", (long) indexPath.section]];
//                //}
//        }
    }
    
    // Adjust green dot label appearence
    customCell.lblOnline.hidden = YES;
    CGRect frame = customCell.lblOnline.frame;

    if ([connDict[kKeyStatus] isEqualToString:@"1"]) {
        customCell.lblOnline.hidden = NO;
        // Adjust green dot label appearence
        customCell.lblOnline.layer.cornerRadius = customCell.lblOnline.frame.size.width / 2;
        customCell.lblOnline.layer.masksToBounds = YES;
        customCell.lblOnline.clipsToBounds = YES;
        customCell.lblOnline.backgroundColor = [UIColor greenColor];

        customCell.lblProfileNameLabel.frame = CGRectMake(frame.origin.x + 15, customCell.lblProfileNameLabel.frame.origin.y, customCell.lblProfileNameLabel.frame.size.width + 10, customCell.lblProfileNameLabel.frame.size.height);
    } else {
        customCell.lblOnline.hidden = NO;
        // Adjust gray dot label appearence
        customCell.lblOnline.layer.cornerRadius = customCell.lblOnline.frame.size.width / 2;
        customCell.lblOnline.layer.masksToBounds = YES;
        customCell.lblOnline.backgroundColor = [UIColor grayColor];
        customCell.lblOnline.clipsToBounds = YES;
    }
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return customCell;
}
#pragma mark - UItableview Delegate


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger count = 100;
    NSLog(@"%ld", (long)indexPath.section);
    NSLog(@"%ld", (long)indexPath.row);
    NSDictionary *connDict = connectionListArr[indexPath.section];
    connDict = [SRModalClass removeNullValuesFromDict:connDict];
    NSArray *locationsArr = connDict[kkeytracking_me];
    NSMutableArray *arrLocation = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < [locationsArr count]; i++) {
        NSDictionary *locationDict = locationsArr[i];
        locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
        if ([locationDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            [arrLocation addObject:locationsArr[i]];
            NSString *cellText = [NSString stringWithFormat:@"%@", [locationsArr[i][kKeyLocation] objectForKey:kKeyAddress]];
            heightLbl = [self heightNeededForText:cellText withFont:[UIFont systemFontOfSize:16] width:(self.view.frame.size.width - self.view.frame.size.width/4) lineBreakMode:NSLineBreakByWordWrapping];
        }
    }
    
    // if expanded or allow track is enable
//    if (connDict[kKeyAllowTrackingMe] != [NSNull null] && ![connDict[kKeyAllowTrackingMe] isEqual:@""]) {
//        if ([expandedTrackList containsObject:@(indexPath.section)] || [[connDict[kKeyAllowTrackingMe] objectForKey:kKeyAllowTrackingMe] isEqualToString:@"1"]) {
//            count = 350;
//        } else if (connDict[kKeyIs_family] != nil && [connDict[kKeyIs_family] isKindOfClass:[NSDictionary class]]) {
//            if ([expandedTrackList containsObject:@(indexPath.section)] || [[connDict[kKeyIs_family] objectForKey:kKeyAllowPushNotificationTrack] isEqualToString:@"1"]) {
//                count = 350;
//            }
//        } else {
            //self test
            if(isExpandable && selectedIndex == indexPath.section){
                count = 105 + (arrLocation.count - 1) * 115;
                count = count + 310 + heightLbl;
            }else{
                count = 105 + (arrLocation.count * 115) + heightLbl;
            }
             return count;
            //self test
        //}
    //}
    return count;
}

- (CGFloat)heightNeededForText:(NSString *)text withFont:(UIFont *)font width:(CGFloat)width lineBreakMode:(NSLineBreakMode)lineBreakMode {
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = lineBreakMode;
    CGSize size = [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                     options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                  attributes:@{ NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle }
                                     context:nil].size;

    return ceilf(size.height);
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}
//jonish CR
// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark
#pragma mark - Notification Method
#pragma mark

- (void)connectionActionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSArray class]]) {
        // Remove all objects add new
        [connectionListArr removeAllObjects];
        for (int i = 0; i < [object count]; i++) {
            NSDictionary *connDict = [object objectAtIndex:i];
            if (connDict[kKeyAllowTrackingMe] != [NSNull null] && ![connDict[kKeyAllowTrackingMe] isEqual:@""]) {
                [connectionListArr addObject:[object objectAtIndex:i]];
            }
        }
        [self.connectionTable reloadData];
        [_trackMeDistanceQueue cancelAllOperations];
//      [self getDistance];
    }
}

// --------------------------------------------------------------------------------
// connectionActionFailed:

- (void)connectionActionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// allowCancelTrackSucceed:
- (void)allowCancelTrackSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// AllowCancelTrackFailed:
- (void)allowCancelTrackFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// UpdateNotificationPlaceSucceed:
- (void)UpdateNotificationPlaceSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSString *jsonString = [[[inNotify userInfo] valueForKey:kKeyRequestedParams] valueForKey:@"trackings"];
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *responseArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    if (connectionListArr.count > 0 && connectionListArr.count <= currentIndex) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:connectionListArr[currentIndex]];
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[dict valueForKey:kkeytracking_me]];
        for (int i = 0; i < array.count; i++) {
            NSMutableDictionary *replaceDict = array[i];
            if ([[replaceDict valueForKey:kKeyId] isEqualToString:[responseArray[i] valueForKey:kKeyId]]) {
                [replaceDict setValue:[responseArray[i] valueForKey:kKeyNotifications] forKey:kKeyNotifications];
                [replaceDict setValue:[responseArray[i] valueForKey:kKeyStatus] forKey:kKeyStatus];
                array[i] = replaceDict;
            }
        }
        [dict setValue:array forKey:kkeytracking_me];
        connectionListArr[currentIndex] = dict;
        NSIndexPath *rowToReload = [NSIndexPath indexPathForRow:0 inSection:currentIndex];
        NSArray *rowsToReload = @[rowToReload];
        [_connectionTable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
}

// --------------------------------------------------------------------------------
// UpdateNotificationPlaceFailed:
- (void)UpdateNotificationPlaceFailed:(NSNotification *)inNotify {

    [APP_DELEGATE hideActivityIndicator];
}

- (void)deleteTrackingSuccess:(NSNotification *)inNotify {
    
    selectedTrackingLocation = -1;
    [APP_DELEGATE showActivityIndicator];
    [server makeAsychronousRequest:kkeyClassGetConnTrackingMeList inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

- (void)deleteTrackingFailure:(NSNotification *)inNotify {
}

- (void)toggleAllSuccess:(NSNotification *)inNotify {
    [APP_DELEGATE showActivityIndicator];
    [server makeAsychronousRequest:kkeyClassGetConnTrackingMeList inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

- (void)toggleAllFailure:(NSNotification *)inNotify {
}

- (void)toggleSingleNotificationSuccess:(NSNotification *)inNotify {
    [APP_DELEGATE showActivityIndicator];
    [server makeAsychronousRequest:kkeyClassGetConnTrackingMeList inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

// --------------------------------------------------------------------------------
// connectionTrackMeSucceed:

- (void)connectionTrackMeSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}
// --------------------------------------------------------------------------------
// connectionTrackMeFailed:

- (void)connectionTrackMeFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}


#pragma mark Private Methods

- (void)callDistanceAPI:(NSDictionary *)userDict {


    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];


    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = rows[0];
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];

                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       double convertDist = [distanceWithoutCommas doubleValue];
                                       if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                           if ([distance containsString:@"km"]) {

                                           } else {
                                               //meters to killometer
                                               convertDist = convertDist / 1000;
                                           }
                                       } else {
                                           if ([distance containsString:@"km"]) {
                                               //killometer to miles
                                               convertDist = convertDist * 0.621371192;
                                           } else {
                                               //meters to miles
                                               convertDist = (convertDist * 0.000621371192);
                                           }
                                       }


                                       NSLog(@"Distance: %@", [NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit]);

                                       SRDistanceClass *obj = [[SRDistanceClass alloc] init];
                                       obj.dataId = userDict[kKeyId];
                                       NSDictionary *locationDict = userDict[kKeyLocation];
                                       obj.lat = locationDict[kKeyLattitude];
                                       obj.longitude = locationDict[kKeyRadarLong];
                                       obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit];
                                       obj.myLocation = server.myLocation;

                                       //Live Address
                                       NSArray *destinationArray = json[@"destination_addresses"];

                                       NSLog(@"Address : %@", [NSString stringWithFormat:@"in %@", destinationArray[0]]);

                                       NSUInteger indexOfUserObject = [connectionListArr indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {


                                           if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                               return YES;
                                           } else {

                                               return NO;
                                           }

                                       }];

                                       NSLog(@"%lu", (unsigned long) indexOfUserObject);


                                       NSLog(@"IndexPath : %lu", (unsigned long) indexOfUserObject);


                                       NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                       NSArray *tempArr = [(APP_DELEGATE).oldConnTrackMeArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                       obj.address = [NSString stringWithFormat:@"in %@", destinationArray[0]];
                                       if (convertDist <= 0) {
                                           obj.address = @"";
                                       }

                                       if (tempArr && tempArr.count) {

                                           NSUInteger indexOfObject = [(APP_DELEGATE).oldConnTrackMeArray indexOfObjectPassingTest:^BOOL(SRDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                                               if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                                   return YES;
                                               } else {

                                                   return NO;
                                               }

                                           }];


                                           (APP_DELEGATE).oldConnTrackMeArray[indexOfObject] = obj;

                                       } else {

                                           [(APP_DELEGATE).oldConnTrackMeArray addObject:obj];

                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^{

                                           if (indexOfUserObject != NSNotFound && indexOfUserObject < connectionListArr.count) {
                                               [self.connectionTable reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                           }
                                       });
                                   }
                               }
                           }];

}


- (void)getDistance {


    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (!(APP_DELEGATE).oldConnTrackMeArray.count) {

        [arr addObjectsFromArray:connectionListArr];

    } else {

        for (NSDictionary *userDict in connectionListArr) {

            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

            NSArray *tempArr = [(APP_DELEGATE).oldConnTrackMeArray filteredArrayUsingPredicate:predicateForDistanceObj];

            if (tempArr && tempArr.count) {

                SRDistanceClass *distanceObj = [tempArr firstObject];

                NSDictionary *locationDict = userDict[kKeyLocation];

                BOOL isBLocationChanged = NO;
                BOOL isALocationChanged = NO;

                if (![distanceObj.lat isEqualToString:locationDict[kKeyLattitude]] || ![distanceObj.longitude isEqualToString:locationDict[kKeyRadarLong]]) {

                    isBLocationChanged = YES;
                }

                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];

                if (distance >= 160) {

                    isALocationChanged = YES;

                }

                if (isBLocationChanged || isALocationChanged) {
                    [arr addObject:userDict];
                }


            } else {

                [arr addObject:userDict];

            }
        }
    }


    for (int i = 0; i < arr.count; i++) {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];


        if (server.myLocation != nil && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            NSBlockOperation *op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled) {

                    [self callDistanceAPI:userDict];
                } else {

                    NSLog(@"Operation Not canceled");
                }
            }];
            [_trackMeDistanceQueue addOperation:op];

        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
