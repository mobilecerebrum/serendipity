//
//  NotifyMeVC.h
//  Serendipity
//
//  Created by Neha Dubey on 08/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotifyMeVC :UIViewController <UITableViewDelegate, UITableViewDataSource>
{
      NSDictionary *userDict;
      BOOL fromAmTrackingView;
}
@property (strong, nonatomic) SRServerConnection *server;
@property (strong, nonatomic) NSDictionary *profileDict;
@property (strong, nonatomic) NSMutableArray *arrAddresses;
@property (strong, nonatomic) NSMutableArray *arrTrackings;
@property (nonatomic) BOOL hasTrackingConnection;
@property (nonatomic) BOOL isSimpletrackingPending;
@end

NS_ASSUME_NONNULL_END
