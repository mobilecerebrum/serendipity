//
//  NotifyMeVC.m
//  Serendipity
//
//  Created by Neha Dubey on 08/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "NotifyMeVC.h"
#import "RequestingTrackLblTblCell.h"
#import "YesNoOptionsTblCell.h"
#import "AddAddressTblCell.h"
#import "WhiteBoxTblCell.h"
#import "WhiteBoxNotificationTblCell.h"
#import "SRNotifyMeMapViewController.h"
#import "UIViewController+UIViewController_Custom_Methods.h"
#import "SRModalClass.h"
#import <QuartzCore/QuartzCore.h>



@interface NotifyMeVC ()
@property (weak, nonatomic) IBOutlet UITableView *tbl;
@property(nonatomic) BOOL yesButtonSelected;
@property(nonatomic) NSUInteger trackingCount;
@property(nonatomic) NSUInteger addressCount;
@property(nonatomic) NSMutableArray *arrSelectedIndexesId;
@property(nonatomic) NSMutableDictionary *dictSelectedAddress;
@property (weak, nonatomic) IBOutlet UIView *viewChooseOne;
@property (weak, nonatomic) IBOutlet UIImageView *ivRadioArrival;
@property (weak, nonatomic) IBOutlet UIImageView *ivRadioDeparture;
@property(nonatomic)  NSString *onetimeKey;
@property (weak, nonatomic) IBOutlet UILabel *lblNavigationTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property(nonatomic) BOOL whiteBoxBlinkingStatus;
@property(nonatomic) BOOL btnBlinkingStatus;

typedef NS_ENUM(NSInteger, NotificationTypo) {
    ArrivalDeparture = 1,
    JustArrival = 2,
    JustDeparture = 3,
    OneTime = 4,
};
@end

@implementation NotifyMeVC



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _arrSelectedIndexesId = [NSMutableArray new];
    _dictSelectedAddress = [NSMutableDictionary new];
//    NSDictionary *dict = @{
//        @"address" : @"South Parade 2 Horley Row Horley RH6 8BH UK",
//        @"id" : @"1590",
//        @"lat" :@"51.180077",
//        @"lon" : @"-0.173150",
//        @"name" : @"home",
//        @"notify" : @"2",
//        @"notify_count" : @"",
//        @"radius" : @"0.15",
//        @"tracker_id" : @"2119",
//        @"user_id" : @"2159"
//    };
//
//
//     self.arrAddresses = [NSMutableArray new];
//     [self.arrAddresses addObject:dict];
//
    userDict = [[NSDictionary alloc] init];
    userDict = _profileDict;
    
 //   _hasTrackingConnection = true;
    _yesButtonSelected = false;
    _trackingCount = 0;
    _addressCount = 0;
    //onetimeKey = @"";
     [self hideView];
    
    self.tbl.rowHeight = UITableViewAutomaticDimension;
    self.tbl.estimatedRowHeight = 44;
   
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
//    kKeyNotificationAddMyTrackLocationsSucceed;
//               } else if ([resultObject isKindOfClass:[NSDictionary class]]) {
//                   successNotification = kKeyNotificationGetMyTrackLocationsSucceed;
//               }


             [defaultCenter addObserver:self
                         selector:@selector(SaveTrackLocationsSucceed:)
                             name:kKeyNotificationSaveTrackLocationSucceed object:nil];
            [defaultCenter addObserver:self
                              selector:@selector(GetTrackLocationsSucceed:)
                                  name:kKeyNotificationGetTrackLocationsSucceed object:nil];
            [defaultCenter addObserver:self
                              selector:@selector(GetTrackLocationsFailed:)
                                  name:kKeyNotificationGetTrackLocationsFailed object:nil];

            [defaultCenter addObserver:self
                              selector:@selector(SaveTrackLocationsFailed:)
                                  name:kKeyNotificationSaveTrackLocationFailed object:nil];
    
            [defaultCenter addObserver:self selector:@selector(getListOfUserLocation) name:kKeyNotificationTrackAddedLocationSucceed object:nil];
    
            [defaultCenter addObserver:self
                                selector:@selector(statusChanged:)
                                    name:kKeyturnOnOffNotificationNotifySucceed object:nil];
            [defaultCenter addObserver:self
                              selector:@selector(statusNotChanged:)
                                  name:kKeyturnOnOffNotificationNotifyFailed object:nil];
    
             [defaultCenter addObserver:self
                                 selector:@selector(connectionMtracking:)
                                     name:kKeyNotificationConnImTrackSucceed object:nil];
            [defaultCenter addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationAddMyTrackLocationsSucceed object:nil];
    
            [defaultCenter addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationGetMyTrackLocationsSucceed object:nil];
    
           [defaultCenter addObserver:self selector:@selector(SaveTrackLocationFailed:) name:kKeyNotificationAddMyTrackLocationsFailed object:nil];
    
           [defaultCenter addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationUpdateTrackLocationSucceed object:nil];
    
           [defaultCenter addObserver:self selector:@selector(SaveTrackLocationFailed:) name:kKeyNotificationUpdateTrackLocationFailed object:nil];
}

-(void)updateTable{
    
    self.tbl.delegate = self;
    self.tbl.dataSource = self;
    
    if (_hasTrackingConnection){
        _lblNavigationTitle.text = @"Tracking Notifications";
    }else{
        _lblNavigationTitle.text = @"Tracking Requests";
    }

      _trackingCount = [self.arrTrackings count];
      _addressCount = [self.arrAddresses count];
       dispatch_async(dispatch_get_main_queue(), ^{
            [self.tbl reloadData];
    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _whiteBoxBlinkingStatus = true;
    _btnBlinkingStatus = false;
    [_arrSelectedIndexesId removeAllObjects];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self getListOfUserLocation];
    [self hideView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

// showNotifyMeMap:
- (void)showNotifyMeMap{
    SRNotifyMeMapViewController *notifyMap = [[SRNotifyMeMapViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:_server];
    notifyMap.trackUserDict = userDict;
    [self.navigationController pushViewController:notifyMap animated:YES];
}

#pragma mark - Action Method

- (void)getListOfUserLocation {
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kkeyClassGetTrackLocListOtherUser, userDict[kKeyId]];
    [_server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

-(void)turnNotificationOnOffStatus:(int)status andtrackingId:(int)trackingId andNotification:(int)notification{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"tracking_id"] = [NSString stringWithFormat:@"%d" , trackingId];
    params[@"allow_notify_on"] = [NSString stringWithFormat:@"%d" , status]; // check frequency to off
    params[@"notifications"] = [NSString stringWithFormat:@"%d" , notification];  //to check notification is on or off
     [APP_DELEGATE showActivityIndicator];
     [self.server makeAsychronousRequest:kKeyOnOffNotification inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

#pragma mark - Notifications Methods
// --------------------------------------------------------------------------------
// GetTrackLocationsSucceed:



- (void)statusChanged:(NSNotification *)inNotify {
   //status changed
     
       [self getListOfUserLocation];
    dispatch_async(dispatch_get_main_queue(), ^{
                     [self.tbl reloadData];
                  });
}

- (void)statusNotChanged:(NSNotification *)inNotify {
     [APP_DELEGATE hideActivityIndicator];
    [self showAlertWithTitle:@"" message:@"Unable to change the status at this moment. Please try again later."];
}

- (void)GetTrackLocationsSucceed:(NSNotification *)inNotify {
  //  [self.arrAddresses removeAllObjects];
    
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        // Get list of saved tracking locations
        NSMutableArray *arrayAddress = [[inNotify object] valueForKey:@"tracking_location"];
       // self.arrAddresses = arrayAddress;
        NSMutableArray *arrayTrackings = [[inNotify object] valueForKey:@"tracking"];
        self.arrTrackings = arrayTrackings;
        id hasTrackingConn = [[inNotify object] valueForKey:@"has_tracking_connection"];
        id simpletrackingPending = [[inNotify object] valueForKey:@"isSimpleTrackingRequestPending"];
        
        if ([hasTrackingConn intValue] == 1){
            self.hasTrackingConnection = YES;
        }else{
             self.hasTrackingConnection = NO;
        }
        
        if ([simpletrackingPending intValue] == 1){
                   self.isSimpletrackingPending = YES;
               }else{
                   self.isSimpletrackingPending = NO;
        }
        
        
        [self updateTable];
    }
}

// SaveTrackLocationsFailed:
- (void)SaveTrackLocationsFailed:(NSNotification *)inNotify {
    //[APP_DELEGATE hideActivityIndicator];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationSucceed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationFailed object:nil];
    
    [SRModalClass showAlert:inNotify.object];

    if (fromAmTrackingView) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
        UIViewController *vc = self.navigationController.viewControllers[numberOfViewControllers - 3];
        [self.navigationController popToViewController:vc animated:NO];
    }
}

// GetTrackLocationsFailed:

- (void)GetTrackLocationsFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}
// --------------------------------------------------------------------------------
// SaveTrackLocationsSucceed:
- (void)SaveTrackLocationsSucceed:(NSNotification *)inNotify {
    [self getListOfUserLocation];
    dispatch_async(dispatch_get_main_queue(), ^{
                     [self.tbl reloadData];
                  });


  //  [APP_DELEGATE hideActivityIndicator];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationSucceed object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationFailed object:nil];
}

- (void)connectionMtracking:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// GetTrackLocationsSucceed:

- (void)SaveTrackLocationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];

//    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationTrackAddedLocationSucceed object:nil];
//    [self.navigationController popViewControllerAnimated:YES];
//
    NSDictionary *dict = inNotify.object;
    self.arrAddresses = [NSMutableArray new];
    [self.arrAddresses addObject:dict];
    [self updateTable];
}

// --------------------------------------------------------------------------------
// GetTrackLocationsFailed:

- (void)SaveTrackLocationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (![self hasTrackingConnection] && !_yesButtonSelected ){
        //(requestingtracklblCell + yes no cell)
         return 2;
    }
    
    if (![self hasTrackingConnection] && _yesButtonSelected ){
        //check for existing user's addresses count = x
        
        if (_addressCount == 0){
              //2(requestingtracklblCell + yes no cell)+x+1(addAddressCell)
             return (2 + _addressCount + 1);
        }else{
            //2(requestingtracklblCell + yes no cell)+x
            return (2 + _addressCount);
        }
    }
    
    if ([self hasTrackingConnection]){
         //check for existing user's tracking count = x
         //check for existing user's address count = y
        //(requestingtracklblCell) + x + y + 1(addAddressCell)
        
        if (_addressCount == 0){
              //(requestingtracklblCell) + x + y + 1(addAddressCell)
               return (1 + _addressCount + _trackingCount + 1);
        }else{
            //(requestingtracklblCell) + x + y
              return (1 + _addressCount + _trackingCount);
        }
        
    }

    return 0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
  if (![self hasTrackingConnection] && !_yesButtonSelected ){
         //(requestingtracklblCell + yes no cell)
          return NO;
     }
     
     if (![self hasTrackingConnection] && _yesButtonSelected ){
         //check for existing user's addresses count = x
         //2(requestingtracklblCell + yes no cell)+x+1(addAddressCell)
         return NO;
     }
     
     if ([self hasTrackingConnection]){
          //check for existing user's tracking count = x
          //check for existing user's address count = y
         //(requestingtracklblCell) + x + y + 1(addAddressCell)
           if ([indexPath row] > 0 && [indexPath row] <= _trackingCount){
                     return YES;
                 }
     }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
       // [self->userLocationsArr removeObjectAtIndex:indexPath.row];
      //  [tableView reloadData]; // tell table to refresh now
    }
}

-(void)deleteTrackingWithId:(NSIndexPath *)indexPath{
    
    [self showAlertWithTitle:@"Serendipity" message:@"Are you sure you want to delete this tracking?" buttonTitle:@"Yes" cancelButtonTitle:@"Cancel" withCompletionBlok:^(BOOL isCancelAction) {
                      
                       if (isCancelAction){
                           return;
                       }else{
                           NSDictionary *tracking = self.arrTrackings[indexPath.row - 1];
                           NSString *strUrl = [NSString stringWithFormat:@"%@%d", kkeyDeleteTracking, [[tracking objectForKey:@"id"]intValue]];
                                     [_server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
                                     [_arrTrackings removeObjectAtIndex:indexPath.row - 1];

                                   [self updateTable];
                           if([_arrTrackings count] == 0){
                               [self getListOfUserLocation];
                           }
                           
                       }
        }];
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    WhiteBoxNotificationTblCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell != nil){
       UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"           "handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
           
                 [self showAlertWithTitle:@"Serendipity" message:@"Are you sure you want to delete this tracking?" buttonTitle:@"Yes" cancelButtonTitle:@"Cancel" withCompletionBlok:^(BOOL isCancelAction) {
                    
                     if (isCancelAction){
                         return;
                     }else{
                         
                          NSDictionary *tracking = self.arrTrackings[indexPath.row - 1];
                         
                         NSString *strUrl = [NSString stringWithFormat:@"%@%d", kkeyDeleteTracking, [[tracking objectForKey:@"id"]intValue]];
                                   [_server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
                                   [_arrTrackings removeObjectAtIndex:indexPath.row - 1];
                         [self updateTable];
                         if([_arrTrackings count] == 0){
                             [self getListOfUserLocation];
                         }
                     }
                 
               }];
           }];
       
           UITableViewCell *commentCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
           CGFloat height = commentCell.frame.size.height;
           UIImage *backgroundImage = [self deleteImageForHeight:height];
           deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
           return @[deleteAction];
    }
    NSArray *arr = [NSArray new];
    return arr;
}

- (UIImage*)deleteImageForHeight:(CGFloat)height{

    CGRect frame = CGRectMake(0, 0, 62, height);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(62, height), NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    CGContextFillRect(context, frame);
    UIImage *image = [UIImage imageNamed:@"trash-white"];
    [image drawInRect:CGRectMake((frame.size.width-10)/2, (frame.size.height - 25)/2, 25, 25)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

-(NSMutableAttributedString *)getTextBolded:(NSString *)first andSecond:(NSString *)second andThird:(NSString *)third{
     NSString *strFirst = first;
      NSString *strSecond = second;
     NSString *strThird = third;
      NSString *strComplete = [NSString stringWithFormat:@"%@ %@ %@",strFirst,strSecond, strThird];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSFontAttributeName
                     value: [UIFont boldSystemFontOfSize:17]
                     range:[strComplete rangeOfString:strSecond]];
    
     return attributedString;
    
}

-(NSMutableAttributedString *)getAttributedString:(NSString *)first andSecond:(NSString *)second andThird:(NSString *)third{
    NSString *strFirst = first;
    NSString *strSecond = second;
    NSString *strThird = third;
    UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];

   
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@ %@",strFirst,strSecond, strThird];

    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strFirst]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:appOrangeColor
                  range:[strComplete rangeOfString:strSecond]];
        
    [attributedString addAttribute:NSFontAttributeName
                   value: [UIFont boldSystemFontOfSize:18]
                   range:[strComplete rangeOfString:strSecond]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strThird]];

     
    return attributedString;
}

-(void)deleteAddress{

        [self showAlertWithTitle:@"" message:@"Are you sure you want to delete this address?" buttonTitle:@"Yes" cancelButtonTitle:@"Cancel" withCompletionBlok:^(BOOL isCancelAction) {

                           if (isCancelAction){
                               return;
                           }else{
                               [self.arrAddresses removeAllObjects];
                                 [self updateTable];
                           }

                     }];
}

//- (void)deleteAddressWithId:(int)addressId{
//    [self showAlertWithTitle:@"" message:@"Are you sure you want to delete this address?" buttonTitle:@"Yes" cancelButtonTitle:@"No" withCompletionBlok:^(BOOL isCancelAction) {
//
//                       if (isCancelAction){
//                           return;
//                       }else{
//
//                           NSString *strUrl = [NSString stringWithFormat:@"%@%d", kkeyClassDeleteTrackingLocation, addressId];
//                           [_server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
//
//                           for (int i = 0 ; i < [_arrAddresses count] ; i++) {
//                             NSDictionary *address = [_arrAddresses objectAtIndex:i];
//                             int currentId = [[address objectForKey:@"id"] intValue];
//                               if (currentId == addressId) {
//                                   [_arrAddresses removeObjectAtIndex:i];
//                                   [self updateTable];
//                                   break;
//
//                               }
//                           }
//                       }
//
//                 }];
//}


//if(hasConnection ==0){
//    // show yes no button
//}else if(isSimpletrackingPending==1 && (array==null || array.size()==0)){
//    // Simple tacking request pending
//}else if(isSimpletrackingPending==0 && (array==null || array.size()==0) && isTrackngConnected==1){
//    //Simply connected but no ongoing tacking
//}else if(array!=null && array.size()>0){
//    // there are ongoing tracking request
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //1st will be same in every case

    //tracking count will be greater than 0 iff hasTrackingConnection parameter will have 1(true) value.
   
    if (![self hasTrackingConnection] && !_yesButtonSelected ){
        if ([indexPath row] == 0 ){
                         RequestingTrackLblTblCell *requestingTrackLblTblCell = [tableView dequeueReusableCellWithIdentifier:@"RequestingTrackLblTblCell"];
               
               NSString *name = [NSString stringWithFormat:@"%@ %@",  [userDict valueForKey:kKeyFirstName], [userDict valueForKey:kKeyLastName]];
               
               if ([_arrTrackings count] == 0){
                   NSMutableAttributedString* str = [self getAttributedString:@"You are requesting to track" andSecond:[NSString stringWithFormat:@"%@'s",name] andThird:@"phone"];
                   
                   requestingTrackLblTblCell.lblRequesting.attributedText = str;
                   return requestingTrackLblTblCell;
               }else{
                   
                   NSMutableAttributedString* str = [self getAttributedString:@"You are receiving the follow notications about" andSecond: name andThird:@""];
                   requestingTrackLblTblCell.lblRequesting.attributedText = str;
                  
                  return requestingTrackLblTblCell;
               }
           }else if ([indexPath row] == 1 ){
                  
        //(requestingtracklblCell + yes no cell)
       
         NSString *name = [NSString stringWithFormat:@"%@",  [userDict valueForKey:kKeyFirstName]];
               
            YesNoOptionsTblCell *yesNoOptionsTblCell = [tableView dequeueReusableCellWithIdentifier:@"YesNoOptionsTblCell"];
               NSMutableAttributedString * str = [self getTextBolded:@"Want to ask" andSecond:name andThird:@"for permission to receive arrival and departure notifications for a specific address?"];
               
               yesNoOptionsTblCell.lblWantToAsk.attributedText = str;
            [yesNoOptionsTblCell setDidTapNoButtonBlock:^(UIButton* sender){
                [self sendTrackingRequestWithoutNotification];
             }];
            
            [yesNoOptionsTblCell setDidTapYesButtonBlock:^(UIButton* sender){
                  _yesButtonSelected = true;
                dispatch_async(dispatch_get_main_queue(), ^{
                   [self.tbl reloadData];
                });
             }];
            return yesNoOptionsTblCell;
        }else{
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
                 return cell;
        }
    }
    
    else if (![self hasTrackingConnection] && _yesButtonSelected ){
        
        if ([indexPath row] == 0 ){
                         RequestingTrackLblTblCell *requestingTrackLblTblCell = [tableView dequeueReusableCellWithIdentifier:@"RequestingTrackLblTblCell"];
               
               NSString *name = [NSString stringWithFormat:@"%@ %@",  [userDict valueForKey:kKeyFirstName], [userDict valueForKey:kKeyLastName]];
               
               if ([_arrTrackings count] == 0){
                   NSMutableAttributedString* str = [self getAttributedString:@"You are requesting to track" andSecond:[NSString stringWithFormat:@"%@'s",name] andThird:@"phone"];
                   
                   requestingTrackLblTblCell.lblRequesting.attributedText = str;
                   return requestingTrackLblTblCell;
               }else{
                   
                   NSMutableAttributedString* str = [self getAttributedString:@"You are receiving the follow notications about" andSecond: name andThird:@""];
                   requestingTrackLblTblCell.lblRequesting.attributedText = str;
                  
                  return requestingTrackLblTblCell;
               }
           }else if ([indexPath row] == 1 ){
                  
        
        //check for existing user's addresses count = x
        //2(requestingtracklblCell + yesno_cell)+x+1(addAddressCell)
    
                   YesNoOptionsTblCell *yesNoOptionsTblCell = [tableView dequeueReusableCellWithIdentifier:@"YesNoOptionsTblCell"];
               NSMutableAttributedString * str = [self getTextBolded:@"Want to ask" andSecond:[NSString stringWithFormat:@"%@", [userDict valueForKey:kKeyFirstName]] andThird:@"for permission to receive arrival and departure notifications for a specific address?"];
                           
               yesNoOptionsTblCell.lblWantToAsk.attributedText = str;
                    [yesNoOptionsTblCell setDidTapNoButtonBlock:^(UIButton* sender){
                        //send request
                        [self sendTrackingRequestWithoutNotification];
                    }];
                    
                     [yesNoOptionsTblCell setDidTapYesButtonBlock:^(UIButton* sender){
                         _yesButtonSelected = true;
                    }];
                         return yesNoOptionsTblCell;
       }else if ([indexPath row] == _addressCount + 2){
           //this is supposed to be the last cell i.e. add address
           AddAddressTblCell *addAddressTblCell = [tableView dequeueReusableCellWithIdentifier:@"AddAddressTblCell"];
           if ([self.arrTrackings count] == 0){
               addAddressTblCell.lblAddressTitle.text = [NSString stringWithFormat:@"What address would you like to receive notifications about for %@?",[userDict valueForKey:kKeyFirstName]];
              
           }else{
                 addAddressTblCell.lblAddressTitle.text = @"Want to receive notifications for another address?";
           }
           
                  [addAddressTblCell setDidTapButtonBlock:^(UIButton* sender){
                      [self showNotifyMeMap];
                  }];
                  return addAddressTblCell;
       }
       else{
           UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];
           UIColor *appGreyeColor = [UIColor colorWithRed:176.0f/255.0f green:176.0f/255.0f blue:176.0f/255.0f alpha:1.0f];
                    
           
           // rest cells having existisg addresses
                     WhiteBoxTblCell *whiteBoxTblCell = [tableView dequeueReusableCellWithIdentifier:@"WhiteBoxTblCell"];
                    __block WhiteBoxTblCell *weakRef = whiteBoxTblCell;
           
           if (_whiteBoxBlinkingStatus){
                                    [self startBlinkingLabel: whiteBoxTblCell.ViewWhiteBox];
                               }else{
                                    [self stopBlinkingLabel:whiteBoxTblCell.ViewWhiteBox];
                                   _btnBlinkingStatus = true;
                               }
                      if (_btnBlinkingStatus){
                           [self startBlinkingButton:whiteBoxTblCell.btnSendRequest];
                      }else{
                          [self stopBlinkingButton:whiteBoxTblCell.btnSendRequest];
                      }
           
                    
                     NSDictionary *address = _arrAddresses[indexPath.row - 2];
                     id addressId = [address objectForKey:@"id"];
                     whiteBoxTblCell.lblAddress.text = [address objectForKey:@"address"];
                     whiteBoxTblCell.lblAddressName.text =  [address objectForKey:@"name"];
           
                      whiteBoxTblCell.iVOneTime.image = [UIImage imageNamed:@"unselectedN.png"];
                      whiteBoxTblCell.iVJustDeparture.image = [UIImage imageNamed:@"unselectedN.png"];
                      whiteBoxTblCell.iVJustArrival.image = [UIImage imageNamed:@"unselectedN.png"];
                      whiteBoxTblCell.iVArrival_Departure.image = [UIImage imageNamed:@"unselectedN.png"];
                      whiteBoxTblCell.btnSendRequest.backgroundColor = appGreyeColor;
                      whiteBoxTblCell.btnSendRequest.backgroundColor = appGreyeColor;
           
        
                id type = _dictSelectedAddress[addressId];
                int ty = [type intValue];
                switch (ty) {
                    case 1:
                        whiteBoxTblCell.iVArrival_Departure.image = [UIImage imageNamed:@"selectedN.png"];
                          whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                        break;

                    case 2:
                        whiteBoxTblCell.iVJustArrival.image = [UIImage imageNamed:@"selectedN.png"];
                         whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                        break;

                    case 3:
                         whiteBoxTblCell.iVJustDeparture.image = [UIImage imageNamed:@"selectedN.png"];
                         whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                        break;
                    case 4:
                        whiteBoxTblCell.iVOneTime.image = [UIImage imageNamed:@"selectedN.png"];
                         whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                       break;
                }
                
                 [whiteBoxTblCell setDidTapAddJustArrivalButtonBlock:^(UIButton* sender){
                            
                             _dictSelectedAddress[addressId] = @2;
                             _whiteBoxBlinkingStatus = false;
                    
                   [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                 }];
                                    
                   [whiteBoxTblCell setDidTapAddJustDepartureButtonBlock:^(UIButton* sender){
                                        _dictSelectedAddress[addressId] = @3;
                       _whiteBoxBlinkingStatus = false;
                       [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                      }];
                   [whiteBoxTblCell setDidTapAddOneTimeButtonBlock:^(UIButton* sender){
                                        _dictSelectedAddress[addressId] = @4;
                       _whiteBoxBlinkingStatus = false;
                       [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                          [self showViewWitheAddressId:addressId];
                                       }];
                   [whiteBoxTblCell setDidTapAddArrivalDepartureButtonBlock:^(UIButton* sender){
                                    _dictSelectedAddress[addressId] = @1;
                       _whiteBoxBlinkingStatus = false;
                       [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                    }];
                
                   [whiteBoxTblCell setDidTapDeleteAddressButtonBlock:^(UIButton *sender) {
                       [self deleteAddress];
                      // [self deleteAddressWithId:[addressId intValue]];
                    }];
                          
                   [whiteBoxTblCell setDidTapsendRequestButtonBlock:^(UIButton *sender) {
                         _btnBlinkingStatus = false;
                       [self sendTrackingRequest:addressId andAddressName: [address objectForKey:@"name"]];
                          }];
                
                          return whiteBoxTblCell;
            }
    }
    
   else if ([self hasTrackingConnection]){
       
       UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];
              UIColor *appGreyeColor = [UIColor colorWithRed:176.0f/255.0f green:176.0f/255.0f blue:176.0f/255.0f alpha:1.0f];
    
       
       if ([indexPath row] == 0 ){
                        RequestingTrackLblTblCell *requestingTrackLblTblCell = [tableView dequeueReusableCellWithIdentifier:@"RequestingTrackLblTblCell"];
              
              NSString *name = [NSString stringWithFormat:@"%@ %@",  [userDict valueForKey:kKeyFirstName], [userDict valueForKey:kKeyLastName]];
              
                 if ([_arrTrackings count] > 0){
                     // there are ongoing tracking request
                     //Raj has allowed tracking and for you to receive notifications for the location(s) below:
                     if ([_arrTrackings count] == 1){
                          NSMutableAttributedString* str = [self getAttributedString:@"" andSecond:[NSString stringWithFormat:@"%@",name] andThird:@"has allowed tracking and for you to receive notifications for the location below:"];
                         requestingTrackLblTblCell.lblRequesting.attributedText = str;
                         return requestingTrackLblTblCell;
                         
                     }else{
                         NSMutableAttributedString* str = [self getAttributedString:@"" andSecond:[NSString stringWithFormat:@"%@",name] andThird:@"has allowed tracking and for you to receive notifications for the locations below:"];
                         requestingTrackLblTblCell.lblRequesting.attributedText = str;
                         return requestingTrackLblTblCell;
                     }
                     
                 }else{
                     //no ongoing tracking without address
                     if (_isSimpletrackingPending){
                         //pending
                         NSMutableAttributedString* str = [self getAttributedString:@"" andSecond:[NSString stringWithFormat:@"%@",name] andThird:@"has pending tracking."];
                        requestingTrackLblTblCell.lblRequesting.attributedText = str;
                        return requestingTrackLblTblCell;
                     }else{
                         //accepted
                         //Raj has allowed tracking.
                         NSMutableAttributedString* str = [self getAttributedString:@"" andSecond:[NSString stringWithFormat:@"%@",name] andThird:@"has allowed tracking."];
                            requestingTrackLblTblCell.lblRequesting.attributedText = str;
                            return requestingTrackLblTblCell;
                     }
                 }
           
//              if ([_arrTrackings count] == 0){
//                  NSMutableAttributedString* str = [self getAttributedString:@"You are requesting to track" andSecond:[NSString stringWithFormat:@"%@'s",name] andThird:@"phone"];
//
//                  requestingTrackLblTblCell.lblRequesting.attributedText = str;
//                  return requestingTrackLblTblCell;
//              }else{
//
//                  NSMutableAttributedString* str = [self getAttributedString:@"You are receiving the follow notications about" andSecond: name andThird:@""];
//                  requestingTrackLblTblCell.lblRequesting.attributedText = str;
//
//                 return requestingTrackLblTblCell;
//              }
          }else if ([indexPath row] > 0 && [indexPath row] <= _trackingCount){
                 
       
         //It means It must have some tracking count
         //check for existing user's tracking count = x
         //check for existing user's address count = y
         //(requestingtracklblCell) + x + y + 1(addAddressCell)

          //Show tracking cells
          WhiteBoxNotificationTblCell *whiteBoxNotificationTblCell = [tableView dequeueReusableCellWithIdentifier:@"WhiteBoxNotificationTblCell"];
            
            whiteBoxNotificationTblCell.iVOneTime.image = [UIImage imageNamed:@"unselectedN.png"];
            whiteBoxNotificationTblCell.iVJustDeparture.image = [UIImage imageNamed:@"unselectedN.png"];
            whiteBoxNotificationTblCell.iVJustArrival.image = [UIImage imageNamed:@"unselectedN.png"];
            whiteBoxNotificationTblCell.iVArrival_Departure.image = [UIImage imageNamed:@"unselectedN.png"];
 
            NSDictionary *tracking = self.arrTrackings[indexPath.row - 1];
            NSDictionary *trackingAddress = [tracking valueForKey:@"location"];
            whiteBoxNotificationTblCell.lblAddressName.text = [trackingAddress objectForKey:@"name"];
            whiteBoxNotificationTblCell.lblAddress.text = [trackingAddress objectForKey:@"address"];
            
            NSString *radius = [trackingAddress objectForKey:@"radius"];
            NSString *radiusFeetMile = [self convertKMtoFeet:radius];
            id statPending = [tracking objectForKey:@"status"]; //check if its approved or pending
            int statusPending = [statPending intValue];
            
            switch (statusPending) {
                case 0:
                      //pending
                    whiteBoxNotificationTblCell.lblPending.hidden = NO;
                     whiteBoxNotificationTblCell.constraintWidthPending.constant = 140;
                    whiteBoxNotificationTblCell.viewNotificationsOff.hidden = YES;
                    whiteBoxNotificationTblCell.containerAddDelete.hidden = YES;
                    whiteBoxNotificationTblCell.backgroundColor = appGreyeColor;
                    
                    break;
                    case 1:
                     //approved
                    whiteBoxNotificationTblCell.lblPending.hidden = YES;
                    whiteBoxNotificationTblCell.constraintWidthPending.constant = 50;
                    whiteBoxNotificationTblCell.viewNotificationsOff.hidden = NO;
                    whiteBoxNotificationTblCell.containerAddDelete.hidden = NO;
                    whiteBoxNotificationTblCell.backgroundColor = appOrangeColor;
                    break;
               
            }
            
            id stat = [tracking objectForKey:@"notifications"]; //check if its on or off
            int statusNotification = [stat intValue];
            
            switch (statusNotification) {
                case 1:
                    //on
                    whiteBoxNotificationTblCell.viewNotificationsOff.hidden = YES;
                    whiteBoxNotificationTblCell.lblNotificationType.hidden = NO;
                    [whiteBoxNotificationTblCell.btnTurnOffNotification setTitle:@"Turn OFF Notification" forState:UIControlStateNormal];
                    break;
                    
                case 0:
                    //off
                     whiteBoxNotificationTblCell.viewNotificationsOff.hidden = NO;
                     whiteBoxNotificationTblCell.lblNotificationType.hidden = YES;
                     [whiteBoxNotificationTblCell.btnTurnOffNotification setTitle:@"Turn ON Notification" forState:UIControlStateNormal];
                    break;
            }

            //allow_notify_on- check notification sending frequency
            if ([[tracking objectForKey:@"allow_notify_on"] intValue] == 1){
                //one time
                 whiteBoxNotificationTblCell.iVOneTime.image = [UIImage imageNamed:@"selectedN.png"];
                id type = [tracking objectForKey:@"notify_on"];
                               int typeNotify = [type intValue];
                               switch (typeNotify) {
                                   case 1:
                                       
//                                       [self getTextBolded:@"One-time Arrival Notifications within" andSecond:radiusFeetMile andThird:@"of:"];
                                     whiteBoxNotificationTblCell.lblNotificationType.attributedText  =  [self getTextBolded:@"One-time Arrival Notifications within" andSecond:radiusFeetMile andThird:@"of:"];
                                       //[NSString stringWithFormat:@"One-time Arrival Notifications within %@ of:",radiusFeetMile];
                                       break;
                                    case 2:
                                      
                                     whiteBoxNotificationTblCell.lblNotificationType.attributedText   =  [self getTextBolded:@"One-time Departure Notifications within" andSecond:radiusFeetMile andThird:@"of:"];
                                       //[NSString stringWithFormat:@"One-time Departure Notifications within %@ of:",radiusFeetMile];
                                       break;
                                   
                               }

                }else{
                //it can be off or always
                id type = [tracking objectForKey:@"notify_on"];
                int typeNotify = [type intValue];
                switch (typeNotify) {
                    case 1:
                          whiteBoxNotificationTblCell.iVJustArrival.image = [UIImage imageNamed:@"selectedN.png"];
                        whiteBoxNotificationTblCell.lblNotificationType.attributedText =  [self getTextBolded:@"Arrival Notifications within" andSecond:radiusFeetMile andThird:@"of:"];
                        break;
                     case 2:
                          whiteBoxNotificationTblCell.iVJustDeparture.image = [UIImage imageNamed:@"selectedN.png"];
                        whiteBoxNotificationTblCell.lblNotificationType.attributedText = [self getTextBolded:@"Departure Notifications within" andSecond:radiusFeetMile andThird:@"of:"];
                        break;
                     case 3:
                          whiteBoxNotificationTblCell.iVArrival_Departure.image = [UIImage imageNamed:@"selectedN.png"];
                        whiteBoxNotificationTblCell.lblNotificationType.attributedText = [self getTextBolded:@"Arrival/Departure Notifications within" andSecond:radiusFeetMile andThird:@"of:"];
                        break;
                    default:
                        break;
                }
                
            }
            
    [whiteBoxNotificationTblCell setDidTapTurnOffNotificationButtonBlock:^(UIButton* sender){
      [self showAlertWithTitle:@"" message:@"Are you sure you want to turn off this notification?" buttonTitle:@"Yes" cancelButtonTitle:@"Cancel" withCompletionBlok:^(BOOL isCancelAction) {
                                             
            if (isCancelAction){
                return;
              }else{
              switch (statusNotification) {
                                              case 1:
                                                  //turn off
                                      if ([[tracking objectForKey:@"allow_notify_on"] intValue] == 1){
                                          [self turnNotificationOnOffStatus: 1 andtrackingId:[[tracking objectForKey:@"id"]intValue] andNotification:0];
                                      }else{
                                              [self turnNotificationOnOffStatus: 0 andtrackingId:[[tracking objectForKey:@"id"]intValue] andNotification:0];
                                      }
                                                  break;
                                                  
                                              case 0:
                                                  // turn on
                                  if ([[tracking objectForKey:@"allow_notify_on"] intValue] == 1){
                                      [self turnNotificationOnOffStatus: 1 andtrackingId:[[tracking objectForKey:@"id"]intValue] andNotification:1];
                                  }else{
                                      [self turnNotificationOnOffStatus: 2 andtrackingId:[[tracking objectForKey:@"id"]intValue] andNotification:1];
                                      }
                                  break;
                                  }
                                }
                                          
                          }];
            
                      }];
              
            id trackingId = [tracking objectForKey:@"id"];
            if ([_arrSelectedIndexesId containsObject:trackingId]){
                //expand
                 whiteBoxNotificationTblCell.btnAdd.selected = YES;
                 whiteBoxNotificationTblCell.constraintHeightView2.constant = 200;
            }else{
                //collapse
                whiteBoxNotificationTblCell.btnAdd.selected = NO;
                 whiteBoxNotificationTblCell.constraintHeightView2.constant = 0;
            }
 
            [whiteBoxNotificationTblCell layoutIfNeeded];
            
            __block WhiteBoxNotificationTblCell *weakRef = whiteBoxNotificationTblCell;
            [whiteBoxNotificationTblCell setDidTapPlusMinusButtonBlock:^(UIButton* sender){
                
                if (weakRef.btnAdd.isSelected) {
                    //make it collapse
                    NSUInteger indexId = [_arrSelectedIndexesId indexOfObject:trackingId];
                    [_arrSelectedIndexesId removeObjectAtIndex:indexId];

                } else {
                    if (![_arrSelectedIndexesId containsObject:trackingId]){
                             [_arrSelectedIndexesId addObject:trackingId];
                               }
                   
                }
                [weakRef layoutIfNeeded];
                
                [_tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }];
                    
            [whiteBoxNotificationTblCell setDidTapDeleteButtonBlock:^(UIButton* sender){
                      //delete
                [self deleteTrackingWithId:indexPath];
                
             }];
            
            return whiteBoxNotificationTblCell;
            
        }else if ([indexPath row] == _trackingCount + _addressCount + 1 ){
            //this is supposed to be the last cell i.e. add address
                      AddAddressTblCell *addAddressTblCell = [tableView dequeueReusableCellWithIdentifier:@"AddAddressTblCell"];
            
            if ([self.arrTrackings count] == 0){
              
                          addAddressTblCell.lblAddressTitle.attributedText =   [self getTextBolded:@"What address would you like to receive notifications about for" andSecond:[userDict valueForKey:kKeyFirstName] andThird:@""];
                 //  [NSString stringWithFormat:@"What address would you like to receive notifications about for %@?",[userDict valueForKey:kKeyFirstName]];
                         
                      }else{
                            addAddressTblCell.lblAddressTitle.text = @"Want to receive notifications for another address?";
                      }
            
                   [addAddressTblCell setDidTapButtonBlock:^(UIButton* sender){
                                     [self showNotifyMeMap];

                              }];
            
                             return addAddressTblCell;
        }else{
           // rest cells having existisg addresses
       WhiteBoxTblCell *whiteBoxTblCell = [tableView dequeueReusableCellWithIdentifier:@"WhiteBoxTblCell"];
           __block WhiteBoxTblCell *weakRef = whiteBoxTblCell;
        whiteBoxTblCell.iVOneTime.image = [UIImage imageNamed:@"unselectedN.png"];
        whiteBoxTblCell.iVJustDeparture.image = [UIImage imageNamed:@"unselectedN.png"];
        whiteBoxTblCell.iVJustArrival.image = [UIImage imageNamed:@"unselectedN.png"];
        whiteBoxTblCell.iVArrival_Departure.image = [UIImage imageNamed:@"unselectedN.png"];
        whiteBoxTblCell.btnSendRequest.backgroundColor = appGreyeColor;
       
             if (_whiteBoxBlinkingStatus){
                          [self startBlinkingLabel: whiteBoxTblCell.ViewWhiteBox];
                     }else{
                          [self stopBlinkingLabel:whiteBoxTblCell.ViewWhiteBox];
                         _btnBlinkingStatus = true;
                     }
            if (_btnBlinkingStatus){
                 [self startBlinkingButton:whiteBoxTblCell.btnSendRequest];
            }else{
                [self stopBlinkingButton:whiteBoxTblCell.btnSendRequest];
            }
           
            if ([_arrAddresses count] > indexPath.row - ([_arrTrackings count] + 1)){
                NSDictionary *address = _arrAddresses[indexPath.row - ([_arrTrackings count] + 1)];
                id addressId = [address objectForKey:@"id"];
                  whiteBoxTblCell.lblAddress.text = [address objectForKey:@"address"];
                  whiteBoxTblCell.lblAddressName.text =  [address objectForKey:@"name"];
                  
                  id type = _dictSelectedAddress[addressId];
                  int ty = [type intValue];
                  switch (ty) {
                      case 1:
                          whiteBoxTblCell.iVArrival_Departure.image = [UIImage imageNamed:@"selectedN.png"];
                            whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                          break;

                      case 2:
                          whiteBoxTblCell.iVJustArrival.image = [UIImage imageNamed:@"selectedN.png"];
                           whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                          break;

                      case 3:
                           whiteBoxTblCell.iVJustDeparture.image = [UIImage imageNamed:@"selectedN.png"];
                           whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                          break;
                      case 4:
                          whiteBoxTblCell.iVOneTime.image = [UIImage imageNamed:@"selectedN.png"];
                           whiteBoxTblCell.btnSendRequest.backgroundColor = appOrangeColor;
                         break;
                  }
                  
                   [whiteBoxTblCell setDidTapAddJustArrivalButtonBlock:^(UIButton* sender){
                               _dictSelectedAddress[addressId] = @2;
                               _whiteBoxBlinkingStatus = false;
                       [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                   }];
                                      
                     [whiteBoxTblCell setDidTapAddJustDepartureButtonBlock:^(UIButton* sender){
                                          _dictSelectedAddress[addressId] = @3;
                                         _whiteBoxBlinkingStatus = false;
                         [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                        }];
                  
                     [whiteBoxTblCell setDidTapAddOneTimeButtonBlock:^(UIButton* sender){
                                          _dictSelectedAddress[addressId] = @4;
                          _whiteBoxBlinkingStatus = false;
                         [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                            [self showViewWitheAddressId:addressId];
                                         }];
                  
                     [whiteBoxTblCell setDidTapAddArrivalDepartureButtonBlock:^(UIButton* sender){
                                      _dictSelectedAddress[addressId] = @1;
                          _whiteBoxBlinkingStatus = false;
                         [self.tbl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                      }];
                  
                     [whiteBoxTblCell setDidTapDeleteAddressButtonBlock:^(UIButton *sender) {
                           [self deleteAddress];
                        // [self deleteAddressWithId:[addressId intValue]];
                      }];
                            
                     [whiteBoxTblCell setDidTapsendRequestButtonBlock:^(UIButton *sender) {
                         _btnBlinkingStatus = false;
                         [self sendTrackingRequest:addressId andAddressName: [address objectForKey:@"name"]];
                            }];
                  
                            return whiteBoxTblCell;
            }
            else{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
                           return cell;
            }
         
        }
   }else{
       
       UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
            return cell;
   }
}


-(void)showViewWitheAddressId: (id)addressId{
    _viewChooseOne.hidden = NO;
    _onetimeKey = [NSString stringWithFormat:@"4_%@", addressId];
    _dictSelectedAddress[_onetimeKey] = 0;
}

-(void)hideView{
     _viewChooseOne.hidden = YES;
}

- (IBAction)didTapArrival:(id)sender {
    _ivRadioArrival.image = [UIImage imageNamed:@"selectedN.png"];
    _ivRadioDeparture.image = [UIImage imageNamed:@"unselectedN.png"];
     _dictSelectedAddress[_onetimeKey] = @1;
     [self hideView];
}

- (IBAction)didTapDeparture:(id)sender {
    _ivRadioArrival.image = [UIImage imageNamed:@"unselectedN.png"];
    _ivRadioDeparture.image = [UIImage imageNamed:@"selectedN.png"];
    _dictSelectedAddress[_onetimeKey] = @2;
     [self hideView];
}
- (IBAction)didTapClosePopUpView:(id)sender {
    [self hideView];
}

-(void) startBlinkingLabel:(UIView *)label
{
    label.alpha = 1.0f;
    label.borderWidth = 3.0;
    label.borderColor = [UIColor orangeColor];
    [UIView animateWithDuration:0.7
                          delay:0.3
                        options: UIViewAnimationOptionAutoreverse |UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         label.alpha = 0.3f;
                     }
                     completion:^(BOOL finished){
                         if (finished) {

                         }
                     }];
}

-(void) stopBlinkingLabel:(UIView *)label
{
    // REMOVE ANIMATION
    label.borderWidth = 0.0;
    label.borderColor = [UIColor whiteColor];
    [label.layer removeAllAnimations];
    [label.layer removeAnimationForKey:@"opacity"];
    label.alpha = 1.0f;
 
}

-(void) startBlinkingButton:(UIButton *)label
{
    label.alpha = 1.0f;
    [UIView animateWithDuration:0.7
                          delay:0.3
                        options: UIViewAnimationOptionAutoreverse |UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         label.alpha = 0.3f;
                     }
                     completion:^(BOOL finished){
                         if (finished) {

                         }
                     }];
}

-(void) stopBlinkingButton:(UIButton *)label
{
    // REMOVE ANIMATION
    [label.layer removeAllAnimations];
    [label.layer removeAnimationForKey:@"opacity"];
    label.alpha = 1.0f;
}


-(void)sendTrackingRequestWithoutNotification{
    NSMutableDictionary *postDict = [NSMutableDictionary new];
    postDict[@"id"] = @"";
    postDict[@"is_location_tracking_on"] = @"1";
    postDict[@"tracking_location_id"] = @"0";
    postDict[@"user_id"] = [userDict valueForKey:kKeyId];
    postDict[@"notify_on"] = @"";
    postDict[@"notify"] = @"";
    NSMutableArray *trackLocationArray = [NSMutableArray new];
       [trackLocationArray addObject:postDict];
       
       NSMutableDictionary *pDict = [[NSMutableDictionary alloc] init];
       [pDict setValue:trackLocationArray forKey:@"tracking"];

       NSData *json = [NSJSONSerialization dataWithJSONObject:trackLocationArray options:0 error:nil];
       NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
       NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);

       NSMutableDictionary *trackDict = [@{kKeyTracking: jsonString} mutableCopy];
       NSLog(@"%@", trackDict);
       
       [_server makeAsychronousRequest:kkeyClassGetImTrackingList inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
    
}

-(void)sendTrackingRequest:(id)addressId andAddressName:(NSString *)name{
    //choose if one time - arrival departure exist
    
//    notify_on -> arr - 1/de - 2 , are/depa - 3 , onetime - Darr or dep.   empty
//    notify -> in case of one time ->  "1" // in othercase - "2"
//    id -> ""
//    user_id -> selected user
//    tracking_location_id - address id --------------- empty
//    is_location_tracking_on - 1 always
   
   NSMutableDictionary *postDict = [NSMutableDictionary new];
    postDict[@"id"] = @"";
    postDict[@"is_location_tracking_on"] = @"1";
    postDict[@"tracking_location_id"] = addressId;
    postDict[@"user_id"] = [userDict valueForKey:kKeyId];
    postDict[@"notify_on"] = @"";
    postDict[@"notify"] = @"";
    
   id key =  [_dictSelectedAddress objectForKey:addressId];
   int type = [key intValue];
    switch (type) {
        case 1:
            //arrival/departure
            postDict[@"notify_on"] = @"3";
            postDict[@"notify"] = @"2";
            break;
         case 2:
            //Just arrival
            postDict[@"notify_on"] = @"1";
            postDict[@"notify"] = @"2";
            break;
         case 3:
            //Just Departure
            postDict[@"notify_on"] = @"2";
            postDict[@"notify"] = @"2";
            break;
         case 4:
            //One-time
            postDict[@"notify"] = @"1"; //2,3
            
            //check if one time is arrival or departure
            id oneTimekey =  [_dictSelectedAddress objectForKey:[NSString stringWithFormat:@"4_%@",addressId]];
            int typeOneTime = [oneTimekey intValue];
            
            switch (typeOneTime) {
                case 1:
                    //arrival
                    postDict[@"notify_on"] = @"1";
                    break;
                    
                case 2:
                    //departure
                    postDict[@"notify_on"] = @"2";
                     break;
            }
            
            break;
        }

    NSString *notifyON = [postDict objectForKey:@"notify_on"];
    NSString *notify = [postDict objectForKey:@"notify"];
    
    if ([notifyON isEqualToString:@""] || [notify isEqualToString:@""]){
        [self showAlertWithTitle:@"" message:[NSString stringWithFormat:@"Please mark what type of notification you would like to receive for the address %@" , name]];
        return;
    }
    
    NSMutableArray *trackLocationArray = [NSMutableArray new];
    [trackLocationArray addObject:postDict];
    
    NSMutableDictionary *pDict = [[NSMutableDictionary alloc] init];
    [pDict setValue:trackLocationArray forKey:@"tracking"];

    NSData *json = [NSJSONSerialization dataWithJSONObject:trackLocationArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);

    NSMutableDictionary *trackDict = [@{kKeyTracking: jsonString} mutableCopy];
    NSLog(@"%@", trackDict);
    
    [_server makeAsychronousRequest:kkeyClassGetImTrackingList inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
    
    [self.arrAddresses removeAllObjects];
    [self updateTable];
}

- (IBAction)backBtnAction:(UIButton *)sender {
    
    [self.arrAddresses removeAllObjects];
  [APP_DELEGATE hideActivityIndicator];
       if (fromAmTrackingView) {
           [self.navigationController popViewControllerAnimated:YES];
       } else {
           NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
           UIViewController *vc = self.navigationController.viewControllers[numberOfViewControllers - 3];
           [self.navigationController popToViewController:vc animated:NO];
       }
}

-(NSString *)convertKMtoFeet:(NSString *)valueInKm{
     id unitt = self.server.loggedInUserInfo[@"setting"][@"measurement_unit"];
     int measurementUnit = [unitt intValue];
    //1 = imperial -> Feet ,//0 = metrics ->meters
    //1km = 3280.84 feet
    //1km = 1000 meters
    //1km = 0.62 mile
    //radius is coming in kms from backend
    
    float value = [valueInKm floatValue];
    
    if (measurementUnit == 0){
        //metrics is selected ->show in meters
        float val =  value * 1000;
        int valueInFeet = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInFeet];
        return [NSString stringWithFormat:@"%d meters",roundOffValue];
        
    }else{
        //imperial -> Feet
        float val =  value * 3280.84;
        int valueInMeters = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInMeters];
       return [NSString stringWithFormat:@"%d feet",roundOffValue];
    }
}

-(int)roundOFFvalueBy:(int)value{
    double val = 50.0 * floor((value /50.0)+ 0.5);
    int valueRoundedOff = [[NSString stringWithFormat:@"%f", val] intValue];
    return valueRoundedOff;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end

