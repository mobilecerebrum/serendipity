//
//  WhiteBoxTblCell.h
//  Serendipity
//
//  Created by Neha Dubey on 10/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WhiteBoxTblCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *ViewWhiteBox;

@property (weak, nonatomic) IBOutlet UILabel *lblAddressName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnArrival_Departure;
@property (weak, nonatomic) IBOutlet UIImageView *iVArrival_Departure;
- (IBAction)btnArrival_DepartureAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnOneTime;
@property (weak, nonatomic) IBOutlet UIImageView *iVOneTime;
- (IBAction)btnOneTimeAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnJustArrival;
@property (weak, nonatomic) IBOutlet UIImageView *iVJustArrival;
- (IBAction)btnJustArrivalAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnJustDeparture;
@property (weak, nonatomic) IBOutlet UIImageView *iVJustDeparture;
- (IBAction)btnJustDepartureAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnDeleteAdress;
@property (weak, nonatomic) IBOutlet UIButton *btnSendRequest;
- (IBAction)btnSendRequest:(UIButton *)sender;
- (IBAction)btnDeleteAddress:(UIButton *)sender;

//btnBlocks

@property (copy, nonatomic) void (^didTapAddArrivalDepartureButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapAddOneTimeButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapAddJustArrivalButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapAddJustDepartureButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapDeleteAddressButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapsendRequestButtonBlock)(UIButton* sender);

- (void)setDidTapAddArrivalDepartureButtonBlock:(void (^)(UIButton* sender))didTapAddArrivalDepartureButtonBlock;
- (void)setDidTapAddOneTimeButtonBlock:(void (^)(UIButton* sender))didTapAddOneTimeButtonBlock;
- (void)setDidTapJustArrivalButtonBlock:(void (^)(UIButton* sender))didTapAddJustArrivalButtonBlock;
- (void)setDidTapJustDepartureButtonBlock:(void (^)(UIButton* sender))didTapAddJustDepartureButtonBlock;
- (void)setDidTapDeleteAddressButtonBlock:(void (^)(UIButton* sender))didTapDeleteAddressButtonBlock;
- (void)setDidTapsendRequestButtonBlock:(void (^)(UIButton* sender))didTapsendRequestButtonBlock;


@end

NS_ASSUME_NONNULL_END
