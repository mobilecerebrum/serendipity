//
//  RequestingTrackLblTblCell.h
//  Serendipity
//
//  Created by Neha Dubey on 09/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestingTrackLblTblCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblRequesting;

@end

NS_ASSUME_NONNULL_END
