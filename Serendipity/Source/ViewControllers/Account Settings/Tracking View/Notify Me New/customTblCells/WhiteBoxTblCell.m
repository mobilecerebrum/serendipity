//
//  WhiteBoxTblCell.m
//  Serendipity
//
//  Created by Neha Dubey on 10/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "WhiteBoxTblCell.h"

@implementation WhiteBoxTblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnDeleteAddress:(UIButton *)sender{
    if (self.didTapDeleteAddressButtonBlock){
                     self.didTapDeleteAddressButtonBlock(sender);
           }
}
- (IBAction)btnSendRequest:(UIButton *)sender{
    if (self.didTapsendRequestButtonBlock){
                     self.didTapsendRequestButtonBlock(sender);
           }
}

- (IBAction)btnArrival_DepartureAction:(UIButton *)sender{
    if (self.didTapAddArrivalDepartureButtonBlock){
                     self.didTapAddArrivalDepartureButtonBlock(sender);
           }
}

- (IBAction)btnOneTimeAction:(UIButton *)sender{
    if (self.didTapAddOneTimeButtonBlock){
                     self.didTapAddOneTimeButtonBlock(sender);
           }
}

- (IBAction)btnJustArrivalAction:(UIButton *)sender{
    if (self.didTapAddJustArrivalButtonBlock){
                     self.didTapAddJustArrivalButtonBlock(sender);
           }
}

- (IBAction)btnJustDepartureAction:(UIButton *)sender{
    if (self.didTapAddJustDepartureButtonBlock){
                     self.didTapAddJustDepartureButtonBlock(sender);
           }
}

@end
