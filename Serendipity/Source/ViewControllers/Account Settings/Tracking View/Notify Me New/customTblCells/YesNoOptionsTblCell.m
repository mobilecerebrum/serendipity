//
//  YesNoOptionsTblCell.m
//  Serendipity
//
//  Created by Neha Dubey on 09/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "YesNoOptionsTblCell.h"

@implementation YesNoOptionsTblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnNoAction:(UIButton *)sender {
   if (self.didTapNoButtonBlock)
         {
             self.didTapNoButtonBlock(sender);
         }
}

- (IBAction)btnYesAction:(UIButton *)sender {
     if (self.didTapYesButtonBlock)
           {
               self.didTapYesButtonBlock(sender);
           }
}
@end
