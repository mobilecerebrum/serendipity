//
//  AddAddressTblCell.h
//  Serendipity
//
//  Created by Neha Dubey on 09/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddAddressTblCell : UITableViewCell
@property (copy, nonatomic) void (^didTapButtonBlock)(UIButton* sender);
@property (weak, nonatomic) IBOutlet UILabel *lblAddressTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnAddAddress;
- (IBAction)btnAddAddressAction:(UIButton *)sender;
- (void)setDidTapButtonBlock:(void (^)(UIButton* sender))didTapButtonBlock;
@end

NS_ASSUME_NONNULL_END
