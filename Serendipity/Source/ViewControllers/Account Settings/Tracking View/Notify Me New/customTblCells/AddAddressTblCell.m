//
//  AddAddressTblCell.m
//  Serendipity
//
//  Created by Neha Dubey on 09/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "AddAddressTblCell.h"

@implementation AddAddressTblCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnAddAddressAction:(UIButton *)sender {
    if (self.didTapButtonBlock)
       {
           self.didTapButtonBlock(sender);
       }
}
@end
