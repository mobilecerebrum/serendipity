//
//  WhiteBoxNotificationTblCell.h
//  Serendipity
//
//  Created by Neha Dubey on 10/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//View 1 outlets
@interface WhiteBoxNotificationTblCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *view1; //120
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationType;
@property (weak, nonatomic) IBOutlet UIView *viewNotificationsOff;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIView *containerAddDelete;
- (IBAction)btnAddAction:(UIButton *)sender;
- (IBAction)btnDeleteAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblPending;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthPending;

//btnBlocks
@property (copy, nonatomic) void (^didTapPlusMinusButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapDeleteButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapTurnOffNotificationButtonBlock)(UIButton* sender);

- (void)setDidTapYesButtonBlock:(void (^)(UIButton* sender))didTapPlusMinusButtonBlock;
- (void)setDidTapNoButtonBlock:(void (^)(UIButton* sender))didTapDeleteButtonBlock;
- (void)setDidTapTurnOffNotificationButtonBlock:(void (^)(UIButton* sender))didTapTurnOffNotificationButtonBlock;

//View 2 outlets

@property (weak, nonatomic) IBOutlet UIView *view2; // 200
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightView2;

@property (weak, nonatomic) IBOutlet UIButton *btnArrival_Departure;
@property (weak, nonatomic) IBOutlet UIImageView *iVArrival_Departure;
- (IBAction)btnArrival_DepartureAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnOneTime;
@property (weak, nonatomic) IBOutlet UIImageView *iVOneTime;
- (IBAction)btnOneTimeAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnJustArrival;
@property (weak, nonatomic) IBOutlet UIImageView *iVJustArrival;
- (IBAction)btnJustArrivalAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnJustDeparture;
@property (weak, nonatomic) IBOutlet UIImageView *iVJustDeparture;
- (IBAction)btnJustDepartureAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTurnOffNotification;
- (IBAction)btnTurnOffNotificatioAction:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
