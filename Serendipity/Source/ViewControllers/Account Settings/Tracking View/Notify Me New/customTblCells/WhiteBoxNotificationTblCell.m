//
//  WhiteBoxNotificationTblCell.m
//  Serendipity
//
//  Created by Neha Dubey on 10/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "WhiteBoxNotificationTblCell.h"

@implementation WhiteBoxNotificationTblCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnAddAction:(UIButton *)sender {
    if (self.didTapPlusMinusButtonBlock)
            {
                self.didTapPlusMinusButtonBlock(sender);
            }
}

- (IBAction)btnDeleteAction:(UIButton *)sender {
    if (self.didTapDeleteButtonBlock)
              {
                  self.didTapDeleteButtonBlock(sender);
              }
}
- (IBAction)btnTurnOffNotificatioAction:(UIButton *)sender {
    if (self.didTapTurnOffNotificationButtonBlock){
                  self.didTapTurnOffNotificationButtonBlock(sender);
        }
}

//- (IBAction)btnArrival_DepartureAction:(UIButton *)sender{
//    if (self.didTapArrivalDepartureButtonBlock){
//                     self.didTapArrivalDepartureButtonBlock(sender);
//           }
//}
//
//- (IBAction)btnOneTimeAction:(UIButton *)sender{
//    if (self.didTapOneTimeButtonBlock){
//                     self.didTapOneTimeButtonBlock(sender);
//           }
//}
//
//- (IBAction)btnJustArrivalAction:(UIButton *)sender{
//    if (self.didTapJustArrivalButtonBlock){
//                     self.didTapJustArrivalButtonBlock(sender);
//           }
//}
//
//- (IBAction)btnJustDepartureAction:(UIButton *)sender{
//    if (self.didTapJustDepartureButtonBlock){
//                     self.didTapJustDepartureButtonBlock(sender);
//           }
//}



@end
