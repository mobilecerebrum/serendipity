//
//  YesNoOptionsTblCell.h
//  Serendipity
//
//  Created by Neha Dubey on 09/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YesNoOptionsTblCell : UITableViewCell
@property (copy, nonatomic) void (^didTapYesButtonBlock)(UIButton* sender);
@property (copy, nonatomic) void (^didTapNoButtonBlock)(UIButton* sender);

@property (weak, nonatomic) IBOutlet UILabel *lblWantToAsk;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
- (IBAction)btnNoAction:(UIButton *)sender;
- (IBAction)btnYesAction:(UIButton *)sender;

- (void)setDidTapYesButtonBlock:(void (^)(UIButton* sender))didTapYesButtonBlock;
- (void)setDidTapNoButtonBlock:(void (^)(UIButton* sender))didTapNoButtonBlock;



@end

NS_ASSUME_NONNULL_END
