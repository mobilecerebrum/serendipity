//
//  Constants.swift
//
//  Created by Vakul Saini on 03/04/19.
//  Copyright © 2019 enAct eServices. All rights reserved.
//

import UIKit
//import SDWebImage


// MARK:- Device Sizes
let DEVICE_WIDTH = UIScreen.main.bounds.width
let DEVICE_HEIGHT = UIScreen.main.bounds.height

typealias CellButtonActionBlock = (_ sender: UIButton) -> Void

// Extension of color for application
extension UIColor {
   
    static let appGreen         = #colorLiteral(red: 0.4862745098, green: 0.7607843137, blue: 0.2666666667, alpha: 1)
    static let appRed           = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
    static let appGrayText      = #colorLiteral(red: 0.6431372549, green: 0.6352941176, blue: 0.6352941176, alpha: 1)
    static let appPink          = #colorLiteral(red: 0.9215686275, green: 0.09019607843, blue: 0.2392156863, alpha: 1)
    static let appOrange        = #colorLiteral(red: 0.9283502698, green: 0.5976424813, blue: 0.1303094923, alpha: 1)
    static let appDarkGrayText  = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    static let appLightPink     = #colorLiteral(red: 0.9341263175, green: 0.2336417139, blue: 0.3079964817, alpha: 1)
    static let appLightOrange   = #colorLiteral(red: 0.8947797418, green: 0.4817588925, blue: 0, alpha: 1)
    static let appBorderGrey    = #colorLiteral(red: 0.8394775391, green: 0.8395989537, blue: 0.8474748731, alpha: 1)
    
}


enum Language {
    case English
    case Hindi
    
    var stringValue: String {
        if self == .English {
            return "en"
        }
        else {
            return "hi"
        }
    }
    
    var intValue: Int {
        if self == .English {
            return 1
        }
        else {
            return 2
        }
    }
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    
   
    
    
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
               return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
         if let t: String = textField.text {
             textField.text = String(t.prefix(maxLength))
         }
        
    }
}

extension UIView {
    @IBInspectable
    public var viewCornerRadius: CGFloat{
        set{
            self.layer.cornerRadius = newValue
        }get{
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.borderColor!)
            return color
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOffset = CGSize(width: 1, height: 4)
            layer.shadowOpacity = 0.4
            layer.shadowRadius = shadowRadius
        }
    }
}

func isValidated(_ password: String) -> Bool {
       var lowerCaseLetter: Bool = false
       var upperCaseLetter: Bool = false
       var digit: Bool = false
       var specialCharacter: Bool = false

           for char in password.unicodeScalars {
               if !lowerCaseLetter {
                   lowerCaseLetter = CharacterSet.lowercaseLetters.contains(char)
               }
               if !upperCaseLetter {
                   upperCaseLetter = CharacterSet.uppercaseLetters.contains(char)
               }
               if !digit {
                   digit = CharacterSet.decimalDigits.contains(char)
               }
               if !specialCharacter {
                   specialCharacter = CharacterSet.punctuationCharacters.contains(char)
               }
           }
           if specialCharacter  && digit && lowerCaseLetter && upperCaseLetter{
               //do what u want
               return true
           }
           else {
               return false
           }
   }


extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}



func getAttributedDictionary(key:String, and value: String)-> Dictionary<String,String>{
       return ["attribute_code": key , "value":value]
   }

class customLabel : UILabel{
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
//        let pointSize = self.font.pointSize
//        var fontSize = 0.0
//        if UIDevice.Type.iph{
//            fontSize = 2.0
//        }else if IS_IPHONE_6Por7Por8P{
//            fontSize = 1.0
//        }
//
//        let fontSi = UIFont(name: self.font.fontName, size: pointSize + CGFloat(fontSize))
//        self.font = fontSi
    }
}


func postNotification( name: String,  object: Any?, _ userInfo: [AnyHashable: Any]?) {
    let notification = Notification.init(name: Notification.Name(rawValue: name), object: object, userInfo: userInfo)
    NotificationCenter.default.post(notification)
}

func observeNotification( observer: Any,  selector: Selector,  name: String,  object: Any?) {
    NotificationCenter.default.addObserver(observer, selector: selector, name: NSNotification.Name(rawValue: name), object: object)
}
typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
  case topRightBottomLeft
  case topLeftBottomRight
  case horizontal
  case vertical

var startPoint: CGPoint {
    return points.startPoint
}

var endPoint: CGPoint {
    return points.endPoint
}

var points: GradientPoints {
    switch self {
    case .topRightBottomLeft:
        return (CGPoint(x: 0.0, y: 1.0), CGPoint(x: 1.0, y: 0.0))
    case .topLeftBottomRight:
        return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 1, y: 1))
    case .horizontal:
        return (CGPoint(x: 0.0, y: 0.5), CGPoint(x: 1.0, y: 0.5))
    case .vertical:
        return (CGPoint(x: 0.0, y: 0.0), CGPoint(x: 0.0, y: 1.0))
    }
  }
}

extension UIView {

func applyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil) {
    let gradient: CAGradientLayer = CAGradientLayer()
    gradient.frame = self.bounds
    gradient.colors = colours.map { $0.cgColor }
    gradient.locations = locations
    self.layer.insertSublayer(gradient, at: 0)
}

func applyGradient(withColours colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
    let gradient: CAGradientLayer = CAGradientLayer()
    gradient.frame = self.bounds
    gradient.colors = colours.map { $0.cgColor }
    gradient.startPoint = orientation.startPoint
    gradient.endPoint = orientation.endPoint
    self.layer.insertSublayer(gradient, at: 0)
  }
}

extension UIColor {
    
    static func colorFromRGB(R: CGFloat, G: CGFloat, B: CGFloat, A: CGFloat) -> UIColor {
        return UIColor(red: R/255.0, green: G/255.0, blue: B/255.0, alpha: A)
    }
    
    var hexString: String {
        let colorRef = cgColor.components!
        let r:CGFloat = colorRef[0]
        let g:CGFloat = colorRef[1]
        let b:CGFloat = colorRef[2]
        
        return String(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
    }
}



extension UIView {
    
    static func fromNib() -> UIView {
        let nibName = String(describing: self)
        let outlets = Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        var obj: UIView?
        for outlet in outlets ?? [] {
            if let out = outlet as? UIView {
                obj = out
                break
            }
        }
        return obj!
    }
    
    func addBorder(withWidth width: CGFloat, borderColor color: UIColor ) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    
    func roundCorners(rectCorner: UIRectCorner, radius: CGFloat, withBorder borderWidth: CGFloat = 0, borderColor color: UIColor = UIColor.clear ) {
        if borderWidth == 0 {
            let bezierPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: rectCorner, cornerRadii: CGSize(width: radius, height: radius))
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = bezierPath.cgPath
            self.layer.mask = shapeLayer
        } else {
            let bezierPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: rectCorner, cornerRadii: CGSize(width: radius, height: radius))
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = bezierPath.cgPath
            shapeLayer.borderWidth = borderWidth
            shapeLayer.borderColor = color.cgColor
            self.layer.mask = shapeLayer
        }
    }
    
   
    func makeRound() {
        let radius = max(self.frame.size.width, self.frame.size.height)/2.0
        makeRound(radius: radius)
    }
    
    func makeRound(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func dropShadow(color: UIColor, offSet: CGSize, opacity: Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offSet
        self.layer.shadowOpacity = opacity
    }
    
    func dropShadow(color: UIColor, offSet: CGSize, opacity: Float, radius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
    }
    
    func makeRoundWithShadow(color: UIColor, opacity: Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.cornerRadius = self.frame.size.width/2.0
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = self.frame.size.width/2.0
        self.layer.shadowOpacity = opacity
    }
    
    func removeShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.0
    }
        
    
    func setSize(_ size: CGSize) {
        var frame = self.frame
        frame.size = size
        self.frame = frame
    }
    
    func setOrigin(_ point: CGPoint) {
        var frame = self.frame
        frame.origin = point
        self.frame = frame
    }
    
    
    func setWidth(_ width: CGFloat) {
        var frame = self.frame
        frame.size.width = width
        self.frame = frame
    }
    
    func setHeight(_ height: CGFloat) {
        var frame = self.frame
        frame.size.height = height
        self.frame = frame
    }
    
    func setX(_ x: CGFloat) {
        var frame = self.frame
        frame.origin.x = x
        self.frame = frame
    }
    
    func setY(_ y: CGFloat) {
        var frame = self.frame
        frame.origin.y = y
        self.frame = frame
    }
    
    func width() -> CGFloat {
        return self.frame.size.width
    }
    
    func height() -> CGFloat {
        return self.frame.size.height
    }
    
    func minX() -> CGFloat {
        return self.frame.origin.x
    }
    
    func minY() -> CGFloat {
        return self.frame.origin.y
    }
    
    func maxX() -> CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    
    func maxY() -> CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
    func center() -> CGPoint {
        let frame = self.frame
        return CGPoint(x: frame.width/2, y: frame.height/2)
    }
    
    func placeAtCenter() {
        guard let view = self.superview else {return}
        var frame = self.frame
        frame.origin.x = (view.width() - frame.width)/2
        frame.origin.y = (view.height() - frame.height)/2
        self.frame = frame
    }
    
    func centerHorizontally() {
        guard let view = self.superview else {return}
        var frame = self.frame
        frame.origin.x = (view.width() - frame.width)/2
        self.frame = frame
    }
    
    func centerVertically() {
        guard let view = self.superview else {return}
        var frame = self.frame
        frame.origin.y = (view.height() - frame.height)/2
        self.frame = frame
    }
    
    func getLabel() -> UILabel? {
        var lbl : UILabel?
        for obj in subviews {
            if obj is UILabel {
                lbl = obj as? UILabel
                break
            }
        }
        return lbl
    }
    
    func getImageView() -> UIImageView? {
        var iv : UIImageView?
        for obj in subviews {
            if obj is UIImageView {
                iv = obj as? UIImageView
                break
            }
        }
        return iv
    }
    
    func getButton() -> UIButton? {
        var btn : UIButton?
        for obj in subviews {
            if obj is UIButton {
                btn = obj as? UIButton
                break
            }
        }
        return btn
    }
    
    func getView() -> UIView? {
        var v : UIView?
        for obj in subviews {
            v = obj
            break
        }
        return v
    }
    
    func getTextField() -> UITextField? {
        var tf : UITextField?
        for obj in subviews {
            if obj is UITextField {
                tf = obj as? UITextField
                break
            }
        }
        return tf
    }
    
    func getTextView() -> UITextView? {
        var tv : UITextView?
        for obj in subviews {
            if obj is UITextView {
                tv = obj as? UITextView
                break
            }
        }
        return tv
    }
    
    func flip() {
        
        
        UIView.transition(with: self, duration: 0.5, options: UIView.AnimationOptions.transitionFlipFromLeft, animations: {
            
        }) { (finished) in
            
        }
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension String {
func localized(_ lang:String) ->String {
    let path = Bundle.main.path(forResource: lang, ofType: "lproj")
    let bundle = Bundle(path: path!)
    return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
}}

extension String {
    func isValidPhoneNumber() -> Bool {
        let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")
        let characterSet = CharacterSet(charactersIn: self)
        return allowedCharacters.isSuperset(of: characterSet)
        
    }

    func isvalidcountPhoneNumber() -> Bool {
        if  self.count == 10 {
            return true
            
        }else
        {
            return false
        }
    }
    
    func isvalidcountString() -> Bool {
           if  self.count < 3 || self.count > 30 {
               return false
           }else
           {
               return true
           }
       }
    
    static let NA = "N/A"
    static let EMPTY = ""
    
    var intValue: Int64 {
        return (Int64)(self) ?? 0
    }
    var floatValue: Float {
        return (Float)(self) ?? 0.0
    }
    var cgFloatValue: CGFloat {
        return (CGFloat)(floatValue)
    }
    var doubleValue: Double {
        return (Double)(self) ?? 0.0
    }
    var parseDoubleValue: Double {
        var stringArray = self.components(separatedBy: CharacterSet.decimalDigits.inverted)
        stringArray.removeAll{$0 == ""}
        let value = stringArray.joined(separator: ".")
        return (Double)(value) ?? 0.0
    }
    
    var boolValue: Bool {
        switch self.lowercased() {
            case "true", "yes", "1":
                return true
            case "false", "no", "0":
                return false
            default:
                return false
        }
    }
    
    var numberOnly: String {
        let set = CharacterSet(charactersIn: "0123456789.")
        return self.components(separatedBy: set.inverted).joined()
        //return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
  
    var utcStringToUnix:TimeInterval {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let utcDate = dateFormatter.date(from: self) {
            return utcDate.timeIntervalSince1970
        }
        return 0
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    /// Checking if the string is empty without containing whitespace or new line
    func isEmpty() -> Bool {
        return (self.trimmed.count == 0)
    }
    
    mutating func trimSpaces() {
        self = self.trimmingCharacters(in: .whitespaces)
    }
    
    mutating func trimNewLines() {
        self = self.trimmingCharacters(in: .newlines)
    }
    
    mutating func trimSpacesAndNewLines() {
        self = self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    mutating func removeSpaces() {
        self = self.replacingOccurrences(of: " ", with: "")
    }
    
    mutating func removeNewLines() {
        self = self.replacingOccurrences(of: "\n", with: "")
    }
    
    mutating func removeSpacesAndNewLines() {
        self.removeSpaces()
        self.removeNewLines()
    }
    
    func containsAlphabetsOnly() -> Bool {
        let alphabets = "abcdefghijklmnopqrstuvwxyz "
        let unwantedCharacters = NSCharacterSet(charactersIn: String(format: "%@%@", alphabets, alphabets.uppercased())).inverted
        let result = (self as NSString).rangeOfCharacter(from: unwantedCharacters).location == NSNotFound
        return result
    }
    
    func decimalPad(separator decimalSeparator: String) -> Bool {
        let alphabets = "0123456789" + decimalSeparator
        let unwantedCharacters = NSCharacterSet(charactersIn: String(format: "%@%@", alphabets, alphabets.uppercased())).inverted
        let result = (self as NSString).rangeOfCharacter(from: unwantedCharacters).location == NSNotFound
        return result
    }
    
    
    
    func containsNumericsOnly() -> Bool {
        let numerics = "0123456789"
        let unwantedCharacters = NSCharacterSet(charactersIn: String(format: "%@%@", numerics, numerics.uppercased())).inverted
        let result = (self as NSString).rangeOfCharacter(from: unwantedCharacters).location == NSNotFound
        return result
    }
    
    var asciiArray: [UInt32] {
        return unicodeScalars.filter{$0.isASCII}.map{$0.value}
    }
    
    /// Will give an array of all words
    var byWords: [String] {
        var byWords:[String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {word, substringRange, enclosingRange,_ in
            guard let word = word else { return }
            byWords.append(word)
        }
        return byWords
    }
    
    /// first max words
    func firstWords(_ max: Int) -> [String] {
        return Array(byWords.prefix(max))
    }
    
    /// First word only
    var firstWord: String {
        return byWords.first ?? ""
    }
    
    /// Last max words
    func lastWords(_ max: Int) -> [String] {
        return Array(byWords.suffix(max))
    }
    
    /// Last word only
    var lastWord: String {
        return byWords.last ?? ""
    }
    
    func rangeOf(_ str: String) -> NSRange {
        return (self as NSString).range(of: str)
    }
    
//    func isValidPhoneNumber() -> Bool {
//        let phoneNumberRegex = "^((\\+)|(00))[0-9]{6,14}$"
//        //let phoneNumberRegex = "^[+]?(?:[0-9]{2})?[0-9]{10}$"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
//        return predicate.evaluate(with: self)
//    }
    
    func isValidEmail() -> Bool {
        let phoneNumberRegex = "\\A[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\z"
        let predicate = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        return predicate.evaluate(with: self.lowercased())
    }
    
    func isValidName() -> Bool {
        let phoneNumberRegex = "^[\\p{L} .'-]+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        return predicate.evaluate(with: self)
    }
    
  
    func isValidPassword() -> Bool {
         let pwdRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{6,}"
        
        /// only for this app regex
        // let pwdRegex = "\\w{6,10}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", pwdRegex)
        return predicate.evaluate(with: self)
    }
    
    func isValidTime() -> Bool {
        let timeRegex = "^[0-9]{2}:[0-9]{2} (AM|PM|am|pm)"
        let predicate = NSPredicate(format: "SELF MATCHES %@", timeRegex)
        return predicate.evaluate(with: self)
    }
    
    func isValidCharacterInput() -> Bool {
        let timeAplhabet = "0123456789: ampm"
        let unwantedCharacters = NSCharacterSet(charactersIn: String(format: "%@%@", timeAplhabet, timeAplhabet.uppercased())).inverted
        let result = (self as NSString).rangeOfCharacter(from: unwantedCharacters).location == NSNotFound
        return result
    }
    
    func isValidUrl() -> Bool {
        let regex = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: self)
    }
    
    static func customRandomId(ofLength length: Int, appendTimeStamp: Bool) -> String {
        let alphabets = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var str = ""
        for _ in 0..<length {
            let randomIndex = Int(arc4random())%alphabets.count
            let alphabet = (alphabets as NSString).character(at: randomIndex)
            str.append(String(format: "%c", alphabet))
        }
        if appendTimeStamp {
            let currentTime = Date().timeIntervalSince1970 * 1000
            str.append(String(format: "%.0f", currentTime))
        }
        return str
    }
    
    static func randomId() -> String {
        return String.customRandomId(ofLength: 8, appendTimeStamp: true)
    }
    
    func size(withConstrainedWidth width: CGFloat, font: UIFont) -> CGSize {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading] , attributes: [.font : font], context: nil)
        return boundingBox.size
    }
    
    func size(heightConstrainedWidth height: CGFloat, font: UIFont) -> CGSize {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading] , attributes: [.font : font], context: nil)
        return boundingBox.size
    }
    
    func defaultIfEmpty(_ _default: String) -> String {
        if self.isEmpty() { return _default}
        else { return self }
    }
    
    func convertStringToDictionary() -> [String: Any]? {
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                return dict
            } catch {
                //                print("Something went wrong")
            }
        }
        return nil
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    
    var phoneNumberFormat: String {
        
        let number = self.numberOnly
        
        // let mask = "+X (XXX) XXX-XXXX"
        // let mask = "(XXX) XXX-XXXX" // US format
        // let mask = "XXX XXX XXXX" // Arab format
        let mask = "XXX-XXX-XXXX"
        
        var areaCode = ""
        var phoneNumber = number
        if number.count > 10 {
            let startIndex = number.index(number.startIndex, offsetBy: 0)
            let endIndex = number.index(number.startIndex, offsetBy: number.count - 10)
            
            // Separate the area code from total
            areaCode = String(number[startIndex..<endIndex])
            
            // Separate the phone number from total
            phoneNumber = String(number[endIndex...])
        }
        
        let cleanPhoneNumber = phoneNumber
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        
        if areaCode.count > 0 {
            result = "+\(areaCode) \(result)"
        }
        
        return result
    }
    
    var distanceFormat: String {
        
        let number = self.numberOnly
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .none
        formatter.locale = Locale.current
        var str = formatter.string(from: NSNumber(value: number.doubleValue)) ?? String.EMPTY
        if str == "0" {
            str = String.EMPTY
        }
        return str
    }
    
    var currencyFormat: String {
        let number = self.numberOnly
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .none
        formatter.locale = Locale.current
        var str = formatter.string(from: NSNumber(value: number.doubleValue)) ?? String.EMPTY
        if str == "0" {
            str = String.EMPTY
        }
        return str
    }
    
    var fullPrice: String {
        return "$\(self.currencyFormat)"
    }
    
    var remove0IfInt: String {
        return self.replacingOccurrences(of: ".0", with: "")
    }
}

extension Character {
    var asciiValue: UInt32? {
        return String(self).unicodeScalars.filter{$0.isASCII}.first?.value
    }
}


extension UILabel {
    
    /*override open func awakeFromNib() {
     super.awakeFromNib()
     
     let pointSize = self.font?.pointSize
     if let font = UIFont(name: kOpenSans, size: pointSize!) {
     self.font = font
     }
     }*/
    
    func setAttributedText(withTallerText: String, tallerHeight: CGFloat, smallerText: String, andSmallerHeight: CGFloat) {
        
        let attributedString = NSMutableAttributedString(string: withTallerText,
                                                         attributes: [NSAttributedString.Key.font : UIFont(name: self.font.fontName, size: tallerHeight) ?? UIFont.systemFont(ofSize: tallerHeight)])
        attributedString.append(NSMutableAttributedString(string: smallerText,
                                                          attributes: [NSAttributedString.Key.font : UIFont(name: self.font.fontName, size: andSmallerHeight) ?? UIFont.systemFont(ofSize: andSmallerHeight),
                                                                       NSAttributedString.Key.foregroundColor: UIColor.colorFromRGB(R: 169, G: 169, B: 169, A: 1.0)]))
        self.attributedText = attributedString
    }
    
    func setAttributedText(firstText first: String, firstFontName: String, secondText second: String, secondFontName: String, color: UIColor = UIColor.colorFromRGB(R: 169, G: 169, B: 169, A: 1.0), withLine underLine: Bool = false) {
        
        if first.count == 0 {
            return
        }
        let pointSize = self.font.pointSize
        let attributedString = NSMutableAttributedString(string: first,
                                                         attributes: [NSAttributedString.Key.font : UIFont(name: firstFontName, size: pointSize) ?? UIFont.systemFont(ofSize: pointSize),
                                                                      NSAttributedString.Key.foregroundColor: self.textColor ?? UIColor.black])
        if second.count == 0 {
            self.attributedText = attributedString
            return
        }
        if (underLine) {
            let attributes = [NSAttributedString.Key.font : UIFont(name: secondFontName, size: pointSize) ?? UIFont.systemFont(ofSize: pointSize),
                              NSAttributedString.Key.foregroundColor: color
                ] as [NSAttributedString.Key : Any]
            let secondAttributedString = NSMutableAttributedString(string: second,
                                                                   attributes: attributes)
            attributedString.append(secondAttributedString)
            
            let range = NSMakeRange(first.count, second.count)
            attributedString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                            NSAttributedString.Key.underlineColor: UIColor.colorFromRGB(R: 0, G: 0, B: 240, A: 1)],
                                           range: range)
            
        } else {
            if (second.isEmpty()) {
                self.attributedText = attributedString
                return
            }
            attributedString.append(NSMutableAttributedString(string: second,
                                                              attributes: [NSAttributedString.Key.font : UIFont(name: secondFontName, size: pointSize) ?? UIFont.systemFont(ofSize: pointSize),
                                                                           NSAttributedString.Key.foregroundColor: color]))
        }
        
        self.attributedText = attributedString
    }
    
    func setAttributedText(firstText first: String, firstFontName: String, color: UIColor = UIColor.colorFromRGB(R: 169, G: 169, B: 169, A: 1.0), withLine underLine: Bool = false) {
        
        if first.count == 0 {
            return
        }
        let pointSize = self.font.pointSize
        
        if (underLine) {
            let attributes = [NSAttributedString.Key.font : UIFont(name: firstFontName, size: pointSize) ?? UIFont.systemFont(ofSize: pointSize),
                              NSAttributedString.Key.foregroundColor: color
                ] as [NSAttributedString.Key : Any]
            let secondAttributedString = NSMutableAttributedString(string: first,
                                                                   attributes: attributes)
            
            let range = NSMakeRange(0, first.count)
            secondAttributedString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                                                  NSAttributedString.Key.underlineColor: UIColor.colorFromRGB(R: 0, G: 0, B: 240, A: 1)],
                                                 range: range)
            self.attributedText = secondAttributedString
        } else {
            
        }
        
    }
    
}


extension Date {
    
    func stringValue(_ format: String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: self)
    }
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }

    var startOfMonth: Date {

        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)

        return  calendar.date(from: components)!
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }

    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)!
    }

    func isMonday() -> Bool {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.weekday], from: self)
        return components.weekday == 2
    }
}


extension UITextField {
    func setPlaceholderColor(_ color: UIColor) {
        let text = self.placeholder ?? ""
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
}


/*extension UIImage {
    func uploadOnFireStorage(completion: @escaping (_ urlStr: String?) -> Void) {
        let fileName = "\((Int64)(Date().timeIntervalSince1970))" + "_" + String.randomId() + ".jpg"
        let data = self.jpegData(compressionQuality: 0.5)!
        _ = FirFile.shared.upload(data: data, withName: fileName, block: { (urlStr, error) in
            
            if let _urlStr = urlStr {
                // Save image in cache
                self.saveInDisk(forKey: _urlStr)
            }
            
            completion(urlStr)
        })
    }
    
    func saveInDisk(forKey key: String) {
        SDImageCache.shared.store(self, forKey: key) { }
    }
    
    func dataWithQuality(_ quality: Quality) -> Data {
        switch quality {
            case .Original:
                return self.jpegData(compressionQuality: 1)!
            case .Highest:
                return self.jpegData(compressionQuality: 0.9)!
            case .High:
                return self.jpegData(compressionQuality: 0.8)!
            case .Medium:
                return self.jpegData(compressionQuality: 0.5)!
            case .Low:
                return self.jpegData(compressionQuality: 0.3)!
            case .Lowest:
                return self.jpegData(compressionQuality: 0.1)!
        }
    }
    
    func withQuality(_ quality: Quality) -> UIImage {
        if quality == .Original {
            return self
        }
        else {
            return UIImage(data: dataWithQuality(quality))!
        }
    }
    
    public enum `Quality` {
        case Original
        case Highest
        case High
        case Medium
        case Low
        case Lowest
    }
}*/


extension Error {
    static func error(domain: String, code: Int, userInfo dict: [String : Any]? = nil) -> Error {
        var d = domain
        if d.isEmpty {
            d = "com.hourful.ios"
        }
        return NSError(domain: d, code: code, userInfo: dict) as Error
    }
    
    static func error(message: String) -> Error {
        return NSError.error(domain: "", code: 9090, userInfo: [NSLocalizedDescriptionKey: message])
    }
    
    static func error(message: String, code: Int) -> Error {
        return NSError.error(domain: "", code: code, userInfo: [NSLocalizedDescriptionKey: message])
    }
}


extension UIView {
    
    func startZRotation(forDuration duration: CFTimeInterval, repeatCount: CGFloat, clockWise: Bool) {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        let direction: CGFloat = clockWise ? 1.0 : -1.0
        animation.toValue = .pi * 2 * direction
        animation.duration = duration
        animation.isCumulative = true
        animation.repeatCount = Float(repeatCount)
        layer.add(animation, forKey: "transform.rotation.z")
    }
    
    func stopZRotation() {
        layer.removeAnimation(forKey: "transform.rotation.z")
    }
    
    func shake() {
        let midX = center.x
        let midY = center.y
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.06
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: midX - 10, y: midY)
        animation.toValue = CGPoint(x: midX + 10, y: midY)
        layer.add(animation, forKey: "position")
    }
    
    func removeShake() {
        layer.removeAnimation(forKey: "position")
    }
    
    
    func startWiggling() {
        
        guard layer.animation(forKey: "wiggle") == nil, layer.animation(forKey: "bounce") == nil else { return }
        
        let angle = 0.04
        
        let wiggle = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        wiggle.values = [-angle, angle]
        wiggle.autoreverses = true
        wiggle.duration = TimeInterval.random(0.1, variance: 0.025)
        wiggle.repeatCount = Float.infinity
        layer.add(wiggle, forKey: "wiggle")
        
        let bounce = CAKeyframeAnimation(keyPath: "transform.translation.y")
        bounce.values = [4.0, 0.0]
        bounce.autoreverses = true
        bounce.duration = TimeInterval.random(0.12, variance: 0.025)
        bounce.repeatCount = Float.infinity
        layer.add(bounce, forKey: "bounce")
    }
    
    func stopWiggling() {
        layer.removeAnimation(forKey: "wiggle")
        layer.removeAnimation(forKey: "bounce")
    }
}

extension TimeInterval {
    static func random(_ interval: TimeInterval, variance: Double) -> TimeInterval {
        return interval + variance * Double((Double(arc4random_uniform(1000)) - 500.0) / 500.0)
    }
}

//extension UIButton {
//    func appEnable(_ enable: Bool) {
//        if enable {
//            self.isUserInteractionEnabled = true
//            self.backgroundColor = UIColor.appDarkRed
//        }
//        else {
//            self.isUserInteractionEnabled = false
//            self.backgroundColor = UIColor.appRedDisabled
//        }
//    }
//}


extension UIDevice {
    
    public enum `Type` {
        case iPad
        case Uknown
        case iPhone_5_5S_SE_5C
        case iPhone_6_6S_7_8
        case iPhone_6p_6Sp_7p_8p
        case iPhone_X_XS
        case iPhoneXSMax
        case iPhoneXR
    }
    
    public var type: Type {
        if userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                return .iPhone_5_5S_SE_5C
            case 1334:
                return .iPhone_6_6S_7_8
            case 1920, 2208:
                return .iPhone_6p_6Sp_7p_8p
            case 2436:
                return .iPhone_X_XS
            case 2688:
                return .iPhoneXSMax
            case 1792:
                return .iPhoneXR
            default:
                return .Uknown
            }
        }
        else {
            return .iPad
        }
    }
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()
    
    static let OSVersion: String = {
        return UIDevice.current.systemVersion
    }()
}




extension UICollectionView {
    func registerNibCell(_ name: String) {
        self.register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
    }
}

extension UITableView {
    func registerNibCell(_ name: String) {
        self.register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
}


extension Bool {
    var stringValueTF: String {
        return self ? "true" : "false"
    }
    var stringValue10: String {
        return self ? "1" : "0"
    }
    var stringValueYN: String {
        return self ? "Yes" : "No"
    }
}

extension Double {
    func stringValue(upTo: Int) -> String {
        return String(format: "%.\(upTo)f", self)
    }
    var intValue: Int {
        return (Int)(self)
    }
    var floatValue: Float {
        return (Float)(self)
    }
    
    var distanceFormat: String {
        if self < 805 {
            return self.stringValue(upTo: 0) + "m"
        }
        else {
            return (self/1609.34).stringValue(upTo: 1).remove0IfInt + "mi"
        }
    }
}

extension Int {
    var floatValue: Float {
        return (Float)(self)
    }
    var doubleValue: Double {
        return (Double)(self)
    }
    var stringValue: String {
        return String(format: "%ld", self)
    }
}

extension Int64 {
    var floatValue: Float {
        return (Float)(self)
    }
    var doubleValue: Double {
        return (Double)(self)
    }
    var stringValue: String {
        return String(format: "%ld", self)
    }
}

extension Float {
    var intValue: Int {
        return (Int)(self)
    }
    var doubleValue: Double {
        return (Double)(self)
    }
    func stringValue(upTo: Int) -> String {
        return String(format: "%.\(upTo)f", self)
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


private var bundleKey: UInt8 = 0
final class BundleExtension: Bundle {
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        return (objc_getAssociatedObject(self, &bundleKey) as? Bundle)?.localizedString(forKey: key, value: value, table: tableName) ?? super.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {
    static let once: Void = { object_setClass(Bundle.main, type(of: BundleExtension())) }()
    static func set(language: Language) {
        Bundle.once
        guard let path = Bundle.main.path(forResource: language.stringValue, ofType: "lproj") else {
            return
        }
        objc_setAssociatedObject(Bundle.main, &bundleKey, Bundle(path: path), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}
