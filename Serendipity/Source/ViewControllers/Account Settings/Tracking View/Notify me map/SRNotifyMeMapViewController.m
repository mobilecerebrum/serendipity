//
//  SRNotifyMeMapViewController.m
//  Serendipity
//
//  Created by Leo on 02/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRNotifyMeMapViewController.h"
#import "SRModalClass.h"
#import "SRPanoramaViewController.h"
#import "MKMapView+ZoomLevel.h"

@interface SRNotifyMeMapViewController ()

@end

@implementation SRNotifyMeMapViewController
- (DBMapSelectorManager *)mapSelectorManager {
    if (nil == _mapSelectorManager) {
        _mapSelectorManager = [[DBMapSelectorManager alloc] initWithMapView:self.notifyMeMap];
        _mapSelectorManager.delegate = self;
    }
    return _mapSelectorManager;
}
// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    if ([nibNameOrNil isEqualToString:@"SRNotifyMeMapViewController"]) {
        callDelegate = TRUE;

    } else {
        callDelegate = FALSE;
    }
    self = [super initWithNibName:@"SRNotifyMeMapViewController" bundle:nibBundleOrNil];
    if (inProfileDict.count > 0) {
        updateLocationDict = inProfileDict;
    }
    if (self) {
        // Custom initialization
        server = inServer;
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationAddMyTrackLocationsSucceed object:nil];
         [defaultCenter addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationGetMyTrackLocationsSucceed object:nil];
        [defaultCenter addObserver:self selector:@selector(SaveTrackLocationFailed:) name:kKeyNotificationAddMyTrackLocationsFailed object:nil];
        [defaultCenter addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationUpdateTrackLocationSucceed object:nil];
        [defaultCenter addObserver:self selector:@selector(SaveTrackLocationFailed:) name:kKeyNotificationUpdateTrackLocationFailed object:nil];
    }

    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Do any additional setup after loading the view from its nib.
    //    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.notifyMe.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    [SRModalClass setNavTitle:@"Tracking Distance" forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:@"Close" barImage:nil forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:@"Done" barImage:nil forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(actionOnDone:) forControlEvents:UIControlEventTouchUpInside];
    // Street view
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

   //Neha
        _searchBar.searchTextField.textColor = [UIColor whiteColor];
        // Set text color of search bar
    //    for (UIView *subView in self.searchBar.subviews) {
    //        for (UIView *secondLevelSubview in subView.subviews) {
    //            if ([secondLevelSubview isKindOfClass:[UITextField class]]) {
    //                UITextField *searchBarTextField = (UITextField *) secondLevelSubview;
    //                //set font color here
    //                 searchBarTextField.textColor = [UIColor whiteColor];
    //                break;
    //            }
    //        }
    //    }
        //Neha
    self.notifyMeMap.showsUserLocation = YES;
    // Set map selector settings
    if (updateLocationDict.count > 0) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[updateLocationDict valueForKey:kKeyLattitude] floatValue], [[updateLocationDict valueForKey:kKeyRadarLong] floatValue]);
        [self.notifyMeMap setCenterCoordinate:coordinate zoomLevel:1 animated:true];
        self.mapSelectorManager.circleCoordinate = coordinate;

//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.notifyMeMap setCenterCoordinate:coordinate zoomLevel:13 animated:true];
//        });
        double radius = [[updateLocationDict valueForKey:kKeyRadius] doubleValue];
        NSLog(@"Radius::%f", radius);
        if (radius < 1000) {
            radius = radius * 1000;
        }
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                      self.mapSelectorManager.circleRadius = 150;  //change -Neha map radius - 100 to 150
                      [self.mapSelectorManager applySelectorSettings];
              });
              
              addressName = [[_trackUserDict valueForKey:kKeyLocation] valueForKey:kKeyLiveAddress];
              radiusStr = @"150"; //change -Neha map radius - 100 to 150
              
    } else if (_trackUserDict != nil) {
        double lat = [[[_trackUserDict valueForKey:kKeyLocation] valueForKey:kKeyLattitude] doubleValue];
        double lon = [[[_trackUserDict valueForKey:kKeyLocation] valueForKey:kKeyRadarLong] doubleValue];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
        
        [self.notifyMeMap setCenterCoordinate:coordinate zoomLevel:1 animated:true];
        self.mapSelectorManager.circleCoordinate = coordinate;

//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.notifyMeMap setCenterCoordinate:coordinate zoomLevel:13 animated:true];
//        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.mapSelectorManager.circleRadius = 150; //Neha radius chnage
                [self.mapSelectorManager applySelectorSettings];
        });
        
        addressName = [[_trackUserDict valueForKey:kKeyLocation] valueForKey:kKeyLiveAddress];
        radiusStr = @"150"; // neha
        
    } else {
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(server.myLocation.coordinate.latitude, server.myLocation.coordinate.longitude);
        
        [self.notifyMeMap setCenterCoordinate:coordinate zoomLevel:1 animated:true];
        self.mapSelectorManager.circleCoordinate = coordinate;

//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self.notifyMeMap setCenterCoordinate:coordinate zoomLevel:13 animated:true];
//        });
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       self.mapSelectorManager.circleRadius = 150; //change -Neha map radius - 100 to 150
                       [self.mapSelectorManager applySelectorSettings];

               });
               radiusStr = @"150"; //change -Neha map radius - 100 to 150
           }
           self.mapSelectorManager.circleRadiusMax = 25000;
//    [self.mapSelectorManager applySelectorSettings];

    // Auto complete
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = _tableDataSource;
    _resultsController.tableView.dataSource = _tableDataSource;

    MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.notifyMeMap];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
//    if (SCREEN_WIDTH <= 320) {
//        spacer.width = 220;
//    } else
        spacer.width = SCREEN_WIDTH - 100;

    UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshLocBtnAction:)];
    [self.locationToolBar.layer setMasksToBounds:YES];
    self.locationToolBar.items = @[buttonItem, spacer, refresh];
}
// ------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {

    //Call Super
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationAddMyTrackLocationsSucceed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationAddMyTrackLocationsFailed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SaveTrackLocationSucceed:) name:kKeyNotificationAddMyTrackLocationsSucceed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SaveTrackLocationFailed:) name:kKeyNotificationAddMyTrackLocationsFailed object:nil];

    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        [self.notifyMeMap setCenterCoordinate:panoramaLastLoc zoomLevel:5 animated:YES];
    }
}


// ------------------------------------------------------------------------------------------------------------------------------
// viewWillDisappear:

- (void)viewWillDisappear:(BOOL)animated {

    //Call Super
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationAddMyTrackLocationsSucceed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationAddMyTrackLocationsFailed object:nil];
}


-(NSString *)convertKMtoFeet:(NSString *)valueInKm{
     id unitt = server.loggedInUserInfo[@"setting"][@"measurement_unit"];
     int measurementUnit = [unitt intValue];
    //1 = imperial -> Feet ,//0 = metrics ->meters
    //1km = 3280.84 feet
    //1km = 1000 meters
    //1km = 0.62 mile
    //radius is coming in kms from backend
    
    float value = [valueInKm floatValue];
    
    if (measurementUnit == 0){
        //metrics is selected ->show in meters
        float val =  value * 1000;
        int valueInFeet = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInFeet];
        return [NSString stringWithFormat:@"%d meters",roundOffValue];
        
    }else{
        //imperial -> Feet
        float val =  value * 3280.84;
        int valueInMeters = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInMeters];
       return [NSString stringWithFormat:@"%d feet",roundOffValue];
    }
}

-(int)roundOFFvalueBy:(int)value{
    double val = 50.0 * floor((value /50.0)+ 0.5);
    int valueRoundedOff = [[NSString stringWithFormat:@"%f", val] intValue];
    return valueRoundedOff;
}


#pragma mark
#pragma mark - IBAction Method
#pragma mark

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
    NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [self.notifyMeMap convertPoint:touchPoint toCoordinateFromView:self.view];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_notifyMeMap camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [_notifyMeMap setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {

                    if (!(CGRectContainsPoint(streetViewBtn.frame, touchPoint))) {
                        if (!isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.notifyMeMap addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.notifyMeMap];
    CLLocationCoordinate2D tapPoint = [self.notifyMeMap convertPoint:point toCoordinateFromView:self.view];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_notifyMeMap camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [_notifyMeMap setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}

#pragma mark
#pragma mark Action Method
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:
- (void)actionOnBack:(id)sender {
    [APP_DELEGATE hideActivityIndicator];
    [self.navigationController popViewControllerAnimated:YES];
}

// ----------------------------------------------------------------------------------------------------------
// actionOnDone:
- (void)actionOnDone:(id)sender {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Address" message:@"Please mention type of address" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    av.alertViewStyle = UIAlertViewStylePlainTextInput;
    if (updateLocationDict.count > 0) {
        [[av textFieldAtIndex:0] setText:[updateLocationDict valueForKey:kKeyName]];
    }
    [av textFieldAtIndex:0].delegate = self;
    [av show];
}

// --------------------------------------------------------------------------------
// getAddressFromLatLon:
- (void)getAddressFromLatLon:(CLLocation *)bestLocation {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (error) {
                           return;
                       }
                       CLPlacemark *placemark = [placemarks lastObject];
                       addressName = @"";
                       if (placemark.subLocality != NULL) {
                           addressName = [addressName stringByAppendingString:placemark.subLocality];
                       }
                       if (placemark.locality != NULL) {
                           addressName = [[addressName stringByAppendingString:@" "] stringByAppendingString:placemark.locality];
                       }
                       if (placemark.country != NULL) {
                           addressName = [[addressName stringByAppendingString:@" "] stringByAppendingString:placemark.country];
                       }
                       if (placemark.postalCode != NULL) {
                           addressName = [[addressName stringByAppendingString:@" "] stringByAppendingString:placemark.postalCode];
                       }
                   }];
}

// --------------------------------------------------------------------------------
// tapOnMapHideTableKeyboard

- (void)tapOnMapHideTableKeyboard {
    // Resign keyboard
    [_searchBar resignFirstResponder];
    // Search
    [_tableDataSource sourceTextHasChanged:nil];
    // Remove  overlay view from view
    [_resultsController willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [_resultsController.view removeFromSuperview];
                         [_resultsController removeFromParentViewController];
                     }];
}

// ----------------------------------------------------------------------------------------------------------
// refreshLocBtnAction:
- (void)refreshLocBtnAction:(id)sender {
    [self.notifyMeMap setCenterCoordinate:self.notifyMeMap.centerCoordinate zoomLevel:1 animated:true];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[updateLocationDict valueForKey:kKeyLattitude] floatValue], [[updateLocationDict valueForKey:kKeyRadarLong] floatValue]);
        self.mapSelectorManager.circleCoordinate = coordinate;
        self.mapSelectorManager.circleRadius = self.mapSelectorManager.circleRadius;
        [self.mapSelectorManager applySelectorSettings];
    });
}

// ----------------------------------------------------------------------------------------------------------
// currentLocBtnAction:
- (void)currentLocBtnAction:(id)sender {
    // Move map to that location
    [self.notifyMeMap setCenterCoordinate:self.notifyMeMap.centerCoordinate zoomLevel:1 animated:true];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.notifyMeMap setCenterCoordinate:self.notifyMeMap.userLocation.coordinate animated:YES];
        MKCoordinateRegion region = self.notifyMeMap.region;
        [_notifyMeMap setRegion:region animated:YES];
    });
    

}

#pragma mark
#pragma mark- Alert View Delegate
#pragma mark

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    } else {
        if (buttonIndex == 1) {
            NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
            
            if ([[alertView textFieldAtIndex:0].text isEqualToString:@""]){
                [SRModalClass showAlert: @"Please enter type of address"];
                return;
            }
            
            if (callDelegate) {
                // Return address to main tracking screen
                
                [locDict setValue:addressName forKey:kKeyAddress];
                [locDict setValue:[NSString stringWithFormat:@"%f", self.mapSelectorManager.circleCoordinate.latitude] forKey:kKeyLattitude];
                [locDict setValue:[NSString stringWithFormat:@"%f", self.mapSelectorManager.circleCoordinate.longitude] forKey:kKeyRadarLong];
                if (radiusStr) {
                    [locDict setValue:radiusStr forKey:kKeyRadius];
                } else
                    [locDict setValue:[updateLocationDict valueForKey:kKeyRadius] forKey:kKeyRadius];
                [locDict setValue:[alertView textFieldAtIndex:0].text forKey:kKeyName];
                [locDict setValue:[updateLocationDict valueForKey:kKeyId] forKey:kKeyId];
                [self.delegate setTrackingAddress:locDict];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            } else {
                if (updateLocationDict.count > 0) {
                    [locDict setValue:[self.trackUserDict valueForKey:kKeyId] forKey:kKeyUserID];
                    [locDict setValue:addressName forKey:kKeyAddress];
                    [locDict setValue:[alertView textFieldAtIndex:0].text forKey:kKeyName];
                    [locDict setValue:[NSString stringWithFormat:@"%f", self.mapSelectorManager.circleCoordinate.latitude] forKey:kKeyLattitude];
                    [locDict setValue:[NSString stringWithFormat:@"%f", self.mapSelectorManager.circleCoordinate.longitude] forKey:kKeyRadarLong];
                    [locDict setValue:radiusStr forKey:kKeyRadius];
                    [locDict setValue:[server.loggedInUserInfo valueForKey:kKeyId] forKey:kKeyTrackerUserID];
                    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kkeyClassUpdateTrackLocOtherUser, updateLocationDict[kKeyId]];
                    // call API
                    [server makeAsychronousRequest:strUrl inParams:locDict isIndicatorRequired:YES inMethodType:kPUT];
                } else {
                    [locDict setValue:[self.trackUserDict valueForKey:kKeyId] forKey:kKeyUserID];
                    [locDict setValue:addressName forKey:kKeyAddress];
                    [locDict setValue:[alertView textFieldAtIndex:0].text forKey:kKeyName];
                    [locDict setValue:[NSString stringWithFormat:@"%f", self.mapSelectorManager.circleCoordinate.latitude] forKey:kKeyLattitude];
                    [locDict setValue:[NSString stringWithFormat:@"%f", self.mapSelectorManager.circleCoordinate.longitude] forKey:kKeyRadarLong];
                    [locDict setValue:radiusStr forKey:kKeyRadius];
                    [locDict setValue:[server.loggedInUserInfo valueForKey:kKeyId] forKey:kKeyTrackerUserID];
                    // Call API
                    [server makeAsychronousRequest:kkeyClassAddTrackingLocation inParams:locDict isIndicatorRequired:NO inMethodType:kPOST];
                }
            }
        }
    }
}

#pragma mark
#pragma mark - MKMapViewDelegate
#pragma mark

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    return [self.mapSelectorManager mapView:mapView viewForAnnotation:annotation];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {

    [self.mapSelectorManager mapView:mapView annotationView:annotationView didChangeDragState:newState fromOldState:oldState];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
    return [self.mapSelectorManager mapView:mapView rendererForOverlay:overlay];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [self.mapSelectorManager mapView:mapView regionDidChangeAnimated:animated];
}

#pragma mark - DBMapSelectorManager Delegate

- (void)mapSelectorManager:(DBMapSelectorManager *)mapSelectorManager didChangeCoordinate:(CLLocationCoordinate2D)coordinate {
    if (self.searchBar.text.length == 0) {
        CLLocation *currentloc = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
        [self getAddressFromLatLon:currentloc];
    }
}

- (void)mapSelectorManager:(DBMapSelectorManager *)mapSelectorManager didChangeRadius:(CLLocationDistance)radius {
//    radiusStr = (radius >= 1000) ? [NSString stringWithFormat:@"%.1f", radius * .001f] : [NSString stringWithFormat:@"%.0f", radius];
    radiusStr = [NSString stringWithFormat:@"%.0f", radius];
}

#pragma mark
#pragma mark GMSAutocompleteTableDataSource Delegate method
#pragma mark
// --------------------------------------------------------------------------------
// didAutocompleteWithPlace:

- (void) tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    [_searchBar resignFirstResponder];
    _searchBar.text = place.name;
    addressName = place.formattedAddress;
    // Remove previous annotation
    [_notifyMeMap removeAnnotations:_notifyMeMap.annotations];

    // Move map to that locatin
    MKCoordinateRegion region = {{0.0, 0.0}, {0.0, 0.0}};
    region.center.latitude = place.coordinate.latitude;
    region.center.longitude = place.coordinate.longitude;
    region.span.longitudeDelta = 0.15f;
    region.span.latitudeDelta = 0.15f;
    [_notifyMeMap setRegion:region animated:YES];

    // Add new annotation
    self.mapSelectorManager.delegate = self;
    [self.notifyMeMap setCenterCoordinate:self.notifyMeMap.centerCoordinate zoomLevel:1 animated:true];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.mapSelectorManager.circleCoordinate = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
        [self.mapSelectorManager applySelectorSettings];
    });
}

// --------------------------------------------------------------------------------
// didFailAutocompleteWithError:

- (void)     tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [_searchBar resignFirstResponder];
    _searchBar.text = @"";
    [SRModalClass showAlert:@"Sorry, we are not able to properly map user locations at the moment. Please close and reopen the app."];
}

// --------------------------------------------------------------------------------
// didUpdateAutocompletePredictionsForTableDataSource:

- (void)didUpdateAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

// --------------------------------------------------------------------------------
// didRequestAutocompletePredictionsForTableDataSource:

- (void)didRequestAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

#pragma mark
#pragma mark Search Bar Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// textDidChange:

- (void)searchBar:(UISearchBar *)inSearchBar textDidChange:(NSString *)searchText {
    if ([searchText length] == 0) {
        [self performSelector:@selector(tapOnMapHideTableKeyboard) withObject:_searchBar afterDelay:0];
    } else {
        [_tableDataSource sourceTextHasChanged:searchText];
    }
}

// --------------------------------------------------------------------------------
// searchBarShouldBeginEditing:

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)inSearchBar {
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidBeginEditing:

- (void)searchBarTextDidBeginEditing:(UISearchBar *)inSearchBar {
    // Add overlay view
    [self addChildViewController:_resultsController];
    _resultsController.view.frame = CGRectMake(0, 44, SCREEN_WIDTH, 150);
    _resultsController.view.alpha = 0.0f;
    [self.view addSubview:_resultsController.view];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 1.0f;
                     }];
    [_resultsController didMoveToParentViewController:self];
}

// --------------------------------------------------------------------------------
// searchBarShouldEndEditing:

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)inSearchBar {
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidEndEditing:

- (void)searchBarTextDidEndEditing:(UISearchBar *)srchBar {
    // Search
    [_tableDataSource sourceTextHasChanged:nil];
    // Remove  overlay view from view
    [_resultsController willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [_resultsController.view removeFromSuperview];
                         [_resultsController removeFromParentViewController];
                     }];
}

// --------------------- -----------------------------------------------------------
// searchBarSearchButtonClicked:

- (void)searchBarSearchButtonClicked:(UISearchBar *)inSearchBar {

    // Search
    [_tableDataSource sourceTextHasChanged:inSearchBar.text];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark Notifications Methods
#pragma mark
// --------------------------------------------------------------------------------
// GetTrackLocationsSucceed:

- (void)SaveTrackLocationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];

    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationTrackAddedLocationSucceed object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

// --------------------------------------------------------------------------------
// GetTrackLocationsFailed:

- (void)SaveTrackLocationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

@end
