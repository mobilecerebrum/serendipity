//
//  SRNotifyMeMapViewController.h
//  Serendipity
//
//  Created by Leo on 02/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DBMapSelectorManager.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

@protocol SRNotifyMeMapViewDelegate <NSObject>
@optional
-(void)setTrackingAddress:(NSMutableDictionary *)locDict;
@end

@interface SRNotifyMeMapViewController : ParentViewController<MKMapViewDelegate,DBMapSelectorManagerDelegate,UISearchBarDelegate,
    GMSMapViewDelegate, GMSAutocompleteTableDataSourceDelegate,UITextFieldDelegate,UIAlertViewDelegate>
{
    // Controllers
    UITableViewController *_resultsController;
    GMSAutocompleteTableDataSource *_tableDataSource;
    // Server
    SRServerConnection *server;
    NSArray *menuArray;
    NSString *radiusStr,*addressName;
    NSDictionary *updateLocationDict;
    BOOL callDelegate;
    
    BOOL isPanoramaLoaded;
    IBOutlet UIButton *streetViewBtn;
    CLLocationCoordinate2D panoramaLastLoc;
}
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (nonatomic, strong) DBMapSelectorManager *mapSelectorManager;
@property (strong,nonatomic) IBOutlet MKMapView *notifyMeMap;
@property (strong,nonatomic) IBOutlet UIView *backView;
@property (strong,nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIToolbar *locationToolBar;
@property (strong, nonatomic) NSDictionary *trackUserDict;
@property (nonatomic, assign) id<SRNotifyMeMapViewDelegate> delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
