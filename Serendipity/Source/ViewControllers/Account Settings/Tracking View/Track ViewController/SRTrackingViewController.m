//
//  SRTrackingViewController.m
//  Serendipity
//
//  Created by Leo on 17/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRTrackingViewController.h"


#define CONNECTIONS_TRACKING_ME @"My Connections Tracking Me"
#define SEND_PUSH_NOTIFICATION_MY_LOCATION @"Sending Push Notifications of my Location"

typedef enum : NSUInteger {
    CELLTYPE_ONOFFSWITCH = 1, // Switch Cell
    CELLTYPE_MY_ADDRESS_LABEL, // To show My Address label in center
    CELLTYPE_ADDRESS_TYPE, // For adding address
    CELLTYPE_COUNT, // For Displaying count cell
    CELLTYPE_PUSH_NOTIFICATION // Push notification cell
} CELLTYPE;

@interface CellData : NSObject

@property(nonatomic, strong) id data;
@property(nonatomic, strong) NSString *titleString;
@property(nonatomic, strong) NSString *addrType;
@property(nonatomic, strong) NSString *distanceString;
@property(nonatomic, strong) NSString *count;
@property(nonatomic) NSInteger tag;
@property CELLTYPE type;

@end

@implementation CellData


@end

@interface SRTrackingViewController () <UITextFieldDelegate, SRNotifyMeMapViewDelegate>

@property BOOL switchState;
@property(nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation SRTrackingViewController

- (void)registerForNotifications {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(getMyTrackLocationsSucceed:)
                          name:kKeyNotificationGetMyTrackLocationsSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getMyTrackLocationsFailed:)
                          name:kKeyNotificationGetMyTrackLocationsFailed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(createMyTrackLocationSucceed:)
                          name:kKeyNotificationAddMyTrackLocationsSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(createMyTrackLocationFailed:)
                          name:kKeyNotificationAddMyTrackLocationsFailed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector( deleteMyTrackLocationSucceed:)
                          name:kKeyNotificationDeleteMyTrackLocationsSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(deleteMyTrackLocationFailed:)
                          name:kKeyNotificationDeleteMyTrackLocationsFailed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(settingUpdateSucceed:)
                          name:kKeyNotificationSettingUpdateSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(settingUpdateFailed:)
                          name:kKeyNotificationSettingUpdateFailed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getTrackingNotificationSucceed:)
                          name:kKeyNotificationGetTrackingNotificationSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getTrackingNotificationFailed:)
                          name:kKeyNotificationGetTrackingNotificationFailed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(UpdateNotificationPlaceSucceed:)
                          name:kKeyNotificationAllowCancelTrackSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(UpdateNotificationPlaceFailed:)
                          name:kKeyNotificationAllowCancelTrackFailed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(UpdateNotificationPlaceSucceed:)
                          name:kKeyNotificationUpdateNotificationPlaceSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(UpdateNotificationPlaceFailed:)
                          name:kKeyNotificationUpdateNotificationPlaceFailed object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(updateNotificationSucceed:)
                          name:kKeyNotificationUpdateNotificationSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(updateNotificationFailed:)
                          name:kKeyNotificationUpdateNotificationFailed object:nil];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRTrackingViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        server = inServer;

    }

    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    addressArray = [[NSMutableArray alloc] init];
    notificationsList = [[NSMutableArray alloc] init];
    notificationDict = [[NSMutableDictionary alloc] init];
    updatedNotiList = [[NSMutableArray alloc] init];
    clearNotificationArr = [[NSMutableArray alloc] init];
    
  

    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    [_objtableView setTableFooterView:[UIView new]];
    _switchState = YES;

    // Set title
//    [SRModalClass setNavTitle:NSLocalizedString(@"lbl.tracking.txt", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    [self setNavViewShowCount:false count:@"0"];
    

    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];

    // Set profile pic
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.masksToBounds = YES;
    // Set name of user
    self.lblName.text = [server.loggedInUserInfo valueForKey:kKeyFirstName];

}

-(void)setNavViewShowCount:(BOOL)showCount count:(NSString*)count {
    NavBarView* navBarView = (NavBarView *)[[[NSBundle mainBundle] loadNibNamed:@"NavBarView" owner:nil options:nil] firstObject];
    navBarView.lblTitle.text = NSLocalizedString(@"lbl.tracking.txt", @"");
    [navBarView.lblCount setHidden:!showCount];
    navBarView.lblCount.text = count;
    navBarView.lblCount.layer.cornerRadius = navBarView.lblCount.frame.size.height / 2.0;
    
    self.navigationItem.titleView = navBarView;
}
// ------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {

    //Call Super
    [super viewWillAppear:YES];
    //Neha balnk issue
      [notificationsList removeAllObjects];
    
    //jonish sprint 2
    menuItemArray = @[NSLocalizedString(@"lbl.connection_track.txt", @""),
                          NSLocalizedString(@"lbl.push_notification.txt", @""),
                          NSLocalizedString(@"lbl.my_addr.txt", @""),
                          NSLocalizedString(@"lbl.connection_track.txt", @""),
                          NSLocalizedString(@"lbl.connection_mtrack.txt", @""),
                          NSLocalizedString(@"lbl.track_req.txt", @""),
                          @"Create Tracking Request"];

        _dataArray = [[NSMutableArray alloc] init];

        CellData *obj1 = [[CellData alloc] init];
        obj1.titleString = [NSString stringWithFormat:CONNECTIONS_TRACKING_ME];
        obj1.type = CELLTYPE_ONOFFSWITCH;

        [_dataArray addObject:obj1];


        CellData *obj2 = [[CellData alloc] init];
        obj2.titleString = [NSString stringWithFormat:SEND_PUSH_NOTIFICATION_MY_LOCATION];
        obj2.type = CELLTYPE_ONOFFSWITCH;

        [_dataArray addObject:obj2];

        CellData *obj3 = [[CellData alloc] init];
        obj3.titleString = [NSString stringWithFormat:@"My Addresses"];
        obj3.type = CELLTYPE_MY_ADDRESS_LABEL;

        [_dataArray addObject:obj3];


        CellData *obj4 = [[CellData alloc] init];
        obj4.data = nil;
        obj4.type = CELLTYPE_ADDRESS_TYPE;

        [_dataArray addObject:obj4];

        CellData *obj5 = [[CellData alloc] init];
        obj5.titleString = [NSString stringWithFormat:@"My Connections Tracking Me"];
        obj5.count = @"0";
        obj5.type = CELLTYPE_COUNT;

        [_dataArray addObject:obj5];

        CellData *obj6 = [[CellData alloc] init];
        obj6.titleString = [NSString stringWithFormat:@"Connections I'm Tracking"];
        obj6.count = @"0";
        obj6.type = CELLTYPE_COUNT;

        [_dataArray addObject:obj6];

        CellData *obj7 = [[CellData alloc] init];
        obj7.titleString = [NSString stringWithFormat:@"Tracking Requests"];
        obj7.count = [NSString stringWithFormat:@"%ld", (unsigned long) notificationsList.count];
        obj7.type = CELLTYPE_COUNT;
        [_dataArray addObject:obj7];
        
        CellData *obj8 = [[CellData alloc] init];
        obj8.titleString = [NSString stringWithFormat:@"Create Tracking Request"];
        obj8.count = [NSString stringWithFormat:@"%ld", (unsigned long) notificationsList.count];
        obj8.type = CELLTYPE_COUNT;
        [_dataArray addObject:obj8];

        //
        trackMeCount = @"0";
        trackingCount = @"0";
        if ((APP_DELEGATE).isFromNotification) {
    //        (APP_DELEGATE).topBarView.hidden = YES;
            (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
            [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
            (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
            (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
            (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
            (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
            (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
            (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
            (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
            (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
        }
    // same code delete from view did load
    //jonish sprint 2
    // Register notification
    [self registerForNotifications];
    isClearNotifications = NO;
    //Get notification list
    [server makeAsychronousRequest:kKeyClassGetTrackingNotification inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    
//    NSOperation* op = [[NSOperation alloc] init];
//    op.qualityOfService = NSQualityOfServiceUserInteractive;
//    op.queuePriority = NSOperationQueuePriorityVeryHigh;
//    [op setCompletionBlock:^{
//        // Get list of user tracking locations
        [server makeAsychronousRequest:kkeyClassAddTrackingLocation inParams:nil isIndicatorRequired:NO inMethodType:kGET];
//    }];
//
//    NSOperationQueue *queue = [NSOperationQueue currentQueue];
//    [queue addOperation:op];
    
}

// ------------------------------------------------------------------------------------------------------------------------------
// viewWillDisappear:

- (void)viewWillDisappear:(BOOL)animated {

    //Call Super
    [super viewWillDisappear:YES];
    if (isSelectNotifications) {
        // Update notification count in menu view
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdatedNotificationCount object:nil userInfo:nil];
        });
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Navigation Bar buttton Methods

// ---------------------------------------------------------------------------------------------------------------------------------
// backBtnAction
- (void)backBtnAction {
    if ((APP_DELEGATE).isFromNotification) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
        (APP_DELEGATE).isFromNotification = NO;
        (APP_DELEGATE).isNotificationFromClosed = FALSE;

    } else {
        if (isSelectNotifications) {
            for (NSDictionary *notifyDict in inOutNotifArr) {
                if ([notifyDict[kKeyReferenceType] intValue] == 6 && ![notifyDict[kKeyStatus] boolValue]) {
                    if (notifyDict[kKeyId] != nil) {
                        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                        [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kPUT];
                        isClearNotifications = YES;
                    }
                }
            }
            //[_dataArray removeObjectsInArray:clearNotificationArr];
        }
        [self.navigationController popViewControllerAnimated:NO];
    }
}


#pragma mark - TableView Data source Methods



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellData *obj = _dataArray[indexPath.row];
    if (obj.type == CELLTYPE_ADDRESS_TYPE) {
        return 60.0;
    }
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellData *obj = _dataArray[indexPath.row];
    if (obj.type == CELLTYPE_ADDRESS_TYPE) {
        return 60.0;
    }
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _dataArray.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//
//        if (indexPath.row == 2) {
//
//            [cell setPreservesSuperviewLayoutMargins:YES];
//        }
//
//    }

    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:normalInsets];
//
//        if (indexPath.row == 2) {
//
//            [cell setLayoutMargins:noSepratorInsets];
//        }
//
//    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell;

    CellData *obj = _dataArray[indexPath.row];

    switch (obj.type) {
        case CELLTYPE_COUNT: {
            TrackingCountDisplayCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"TrackingCountDisplayCell"];

            if (!cell1) {

                NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"TrackingCountDisplayCell" owner:nil options:nil];
                cell1 = [nibs firstObject];
            }
            cell1.backgroundColor = [UIColor clearColor];
            cell1.titleLabel.text = obj.titleString;
            [cell1.countLabel setFont:[UIFont fontWithName:kFontHelveticaMedium size:12]];
            if ([cell1.titleLabel.text isEqualToString:@"Tracking Requests"] || [cell1.titleLabel.text isEqualToString:@"Create Tracking Request"]) {
                [cell1.titleLabel setTextColor:[UIColor blackColor]];
                cell1.clearBtn.hidden = YES;
                cell1.countLabel.hidden = ![cell1.titleLabel.text isEqualToString:@"Tracking Requests"];
                if (notificationsList.count > 0) {
                    cell1.countLabel.text = [NSString stringWithFormat:@"%ld", (unsigned long) notificationsList.count];
                } else
                    cell1.countLabel.text = @"0";
            } else if ([cell1.titleLabel.text isEqualToString:@"Connections I'm Tracking"]) {
                cell1.countLabel.hidden = NO;
                cell1.countLabel.text = [NSString stringWithFormat:@"%@", trackingCount];
                [cell1.titleLabel setTextColor:[UIColor blackColor]];
                cell1.backgroundColor = [UIColor clearColor];
                cell1.clearBtn.hidden = YES;
            } else if ([cell1.titleLabel.text isEqualToString:@"My Connections Tracking Me"]) {
                cell1.countLabel.hidden = NO;
                [cell1.titleLabel setTextColor:[UIColor blackColor]];
                cell1.countLabel.text = [NSString stringWithFormat:@"%@", trackMeCount];
                cell1.backgroundColor = [UIColor clearColor];
                cell1.clearBtn.hidden = YES;
            } else if ([cell1.titleLabel.text isEqualToString:@"Notifications"]) {

                cell1.countLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long) clearNotificationArr.count];
                if (isSelectNotifications) {
                    if (clearNotificationArr.count > 0) {
                        [cell1.titleLabel setTextColor:[UIColor whiteColor]];
                        cell1.clearBtn.hidden = NO;
                        cell1.clearBtn.layer.cornerRadius = 4.0;
                        [cell1.clearBtn addTarget:self action:@selector(clearNotificationsBtnAction) forControlEvents:UIControlEventTouchUpInside];
                        cell1.countLabel.hidden = YES;
                    } else {
                        [cell1.titleLabel setTextColor:[UIColor blackColor]];
                        cell1.countLabel.hidden = NO;
                        cell1.clearBtn.hidden = YES;
                    }
                } else {
                    [cell1.titleLabel setTextColor:[UIColor blackColor]];
                    cell1.countLabel.hidden = NO;
                    cell1.clearBtn.hidden = YES;
                }
            } else {
                cell1.countLabel.hidden = YES;
                // NSDictionary *dataDict = obj.data;
//                cell1.titleLabel.text = [NSString stringWithFormat:@"%@ - %@",[self displayDayTime:dataDict],obj.titleString];
                cell1.titleLabel.text = obj.titleString;
                cell1.backgroundColor = [UIColor colorWithRed:0.17 green:0.09 blue:0.02 alpha:1.0];
                cell1.clearBtn.hidden = YES;

                [cell1.titleLabel setTextColor:[UIColor whiteColor]];
                cell1.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

                cell1.countLabel.lineBreakMode = NSLineBreakByWordWrapping;

                [cell1.countLabel setFont:[UIFont fontWithName:kFontHelveticaMedium size:10]];
                [cell1 layoutIfNeeded];
                [cell1 setNeedsUpdateConstraints];
            }
            cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell1;
        }
            break;

        case CELLTYPE_ADDRESS_TYPE: {
            SRAddressCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"SRAddressCell"];

            if (!cell1) {

                NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SRAddressCell" owner:nil options:nil];
                cell1 = [nibs firstObject];
                cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell1.addrLabel.hidden = YES;
            cell1.addressTextField.indexPath = indexPath;
            cell1.addressTextField.delegate = self;
            if (addressArray.count > 0) {
                for (int i = 0; i < addressArray.count; i++) {
                    cell1.addressTextField.placeholder = @"";
                    cell1.addrLabel.hidden = NO;
                    cell1.addrLabel.text = obj.titleString;
                    cell1.addrLabel.textColor = [UIColor whiteColor];
                    float radius = [obj.distanceString floatValue];
                    cell1.distanceLabel.text = [[NSString stringWithFormat:@"%.1f", radius] stringByAppendingString:@" Km"];
                    cell1.typeLabel.text = obj.addrType;
                    [cell1.addCloseButton setImage:nil forState:UIControlStateNormal];

                    if (obj.titleString == nil) {
                        [cell1.addCloseButton setImage:[UIImage imageNamed:@"add_address"] forState:UIControlStateNormal];
                        [cell1.addCloseButton setUserInteractionEnabled:FALSE];
                        [cell1.typeLabel setHidden:YES];
                        [cell1.distanceLabel setHidden:YES];
                        [cell1.locationImageView setHidden:YES];
                        [cell1.addrLabel setHidden:YES];
                        cell1.addressTextField.placeholder = @" Add new address";
                        [cell1.addressTextField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
                        cell1.separatorInset = UIEdgeInsetsMake(0.0f, 10.0, 0.0f, 10.0);

                    } else {
                        [cell1.addCloseButton setImage:[UIImage imageNamed:@"remove_address"] forState:UIControlStateNormal];
                        [cell1.addCloseButton setUserInteractionEnabled:TRUE];
                        [cell1.addCloseButton addTarget:self action:@selector(removeAddressToTrack:) forControlEvents:UIControlEventTouchUpInside];
                        [cell1.typeLabel setHidden:NO];
                        [cell1.distanceLabel setHidden:NO];
                        [cell1.locationImageView setHidden:NO];
                        [cell1.distanceButton addTarget:self action:@selector(distanceBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                        cell1.distanceButton.tag = indexPath.row - 3;
                        cell1.distanceButton.indexPath = indexPath;
                        [cell1.addressTextField setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
                        [cell1.addressTextField setTextColor:[UIColor whiteColor]];
                        cell1.separatorInset = UIEdgeInsetsMake(0.0f, [UIScreen mainScreen].bounds.size.width, 0.0f, 0);
                    }
                    cell1.addCloseButton.tag = indexPath.row - 3;
                }
            } else {
                cell1.addressTextField.placeholder = @" Add new address";
                [cell1.typeLabel setHidden:YES];
                [cell1.addCloseButton setImage:[UIImage imageNamed:@"add_address"] forState:UIControlStateNormal];
                cell1.addrLabel.hidden = YES;
                [cell1.distanceLabel setHidden:YES];
                [cell1.locationImageView setHidden:YES];
                [cell1.addCloseButton removeTarget:self action:@selector(removeAddressToTrack:) forControlEvents:UIControlEventTouchUpInside];
                [cell1.addressTextField setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
                cell1.separatorInset = UIEdgeInsetsMake(0.0f, 10.0, 0.0f, 10.0);
            }
            return cell1;

        }
            break;

        case CELLTYPE_MY_ADDRESS_LABEL: {
            TextMiddleAlignCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"TextMiddleAlignCell"];

            if (!cell1) {

                NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"TextMiddleAlignCell" owner:nil options:nil];
                cell1 = [nibs firstObject];
                cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell1.separatorInset = UIEdgeInsetsMake(0.0f, [UIScreen mainScreen].bounds.size.width, 0.0f, 0);
            return cell1;

        }
            break;

        case CELLTYPE_ONOFFSWITCH: {
            static NSString *cellIdentifier = @"TrackingSwitchCell";

            TrackingSwitchCell *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

            if (!cell1) {

                NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"TrackingSwitchCell" owner:nil options:nil];
                cell1 = (TrackingSwitchCell *) [nibs firstObject];
                cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            }

            cell1.titleLabel.text = obj.titleString;
            cell1.onOffSwitch.indexPath = indexPath;
            if ([obj.titleString isEqualToString:CONNECTIONS_TRACKING_ME]) {
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeytracking_me] boolValue] == YES) {
                    [cell1.onOffSwitch setOn:YES animated:NO];
                } else
                    [cell1.onOffSwitch setOn:NO animated:NO];
            } else {
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeylocation_notification] boolValue] == YES) {
                    [cell1.onOffSwitch setOn:YES animated:NO];
                } else
                    [cell1.onOffSwitch setOn:NO animated:NO];
            }

            [cell1.onOffSwitch addTarget:self action:@selector(onOffSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];

            return cell1;

        }
            break;

        case CELLTYPE_PUSH_NOTIFICATION: {
            NotificationAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationAddressCell"];

            if (!cell) {

                NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"NotificationAddressCell" owner:nil options:nil];
                cell = [nibs firstObject];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }

            [cell.onOffSwitch addTarget:self action:@selector(oneOffSwitchAction:) forControlEvents:UIControlEventValueChanged];
            [cell.allowButton addTarget:self action:@selector(allowTrackingClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.allowButton.tag = obj.tag;

            [cell.closeButton addTarget:self action:@selector(cancelTrackingClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.closeButton.tag = obj.tag;

            TrackingUserDetails *detailObj = (TrackingUserDetails *) obj.data;

            [cell configureCell:detailObj indexPath:indexPath];
            return cell;

        }
            break;

        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;


}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CellData *obj = _dataArray[indexPath.row];

    switch (obj.type) {
        case CELLTYPE_COUNT: {
            if ([obj.titleString isEqualToString:@"My Connections Tracking Me"]) {
                SRConnectionTrackingMeVC *amTracking = [[SRConnectionTrackingMeVC alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
                [self.navigationController pushViewController:amTracking animated:YES];
            } else if ([obj.titleString isEqualToString:@"Connections I'm Tracking"]) {
                SRConnectionAmTrackingVC *amTracking = [[SRConnectionAmTrackingVC alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
                [self.navigationController pushViewController:amTracking animated:YES];
            } else if ([obj.titleString isEqualToString:@"Notifications"]) {
                //Set Arrival Departure Notifications
                if (!isSelectNotifications) {
                    isSelectNotifications = YES;

                    for (int i = 0; i < clearNotificationArr.count; i++) {
                        NSInteger rowIndex = _dataArray.count;
                        CellData *obj9 = [[CellData alloc] init];
                        obj9 = clearNotificationArr[i];
                        if ([obj.titleString isEqualToString:@"Notifications"]) {
                            rowIndex = indexPath.row + 1;
                        }
                        [_dataArray insertObject:obj9 atIndex:rowIndex];
                        rowIndex++;
                        [self.objtableView reloadData];
                    }
                } else {
                    isSelectNotifications = NO;
                    for (int i = 0; i < clearNotificationArr.count; i++) {
                        //                        NSInteger rowIndex = _dataArray.count;
                        CellData *obj9 = [[CellData alloc] init];
                        obj9 = clearNotificationArr[i];
                        if ([obj9.titleString isEqualToString:@"My Connections Tracking Me"]) {

                        } else if ([obj9.titleString isEqualToString:@"Connections I'm Tracking"]) {

                        } else if ([obj9.titleString isEqualToString:@"Notifications"]) {

                        } else if (![obj9.titleString isEqualToString:@"Tracking Requests"]) {
                            [APP_DELEGATE showActivityIndicator];
                            for (NSDictionary *notifyDict in inOutNotifArr) {
                                if ([notifyDict[kKeyReferenceType] intValue] == 6 && ![notifyDict[kKeyStatus] boolValue]) {
                                    if (notifyDict[kKeyId] != nil) {
                                        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                                        [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kPUT];
                                        isClearNotifications = YES;
                                    }
                                }
                            }
                            //[_dataArray removeObjectsInArray:clearNotificationArr];
                        }
                        [self.objtableView reloadData];
                    }
                }
            } else if ([obj.titleString isEqualToString:@"Create Tracking Request"]) {
                SRAddConnectionTabViewController *newConn = [[SRAddConnectionTabViewController alloc] initWithNibName:kKeyIsFromTracking bundle:nil];
//                (APP_DELEGATE).topBarView.hidden = YES;
                [self.navigationController pushViewController:newConn animated:YES];
                selectedRowindexPath = indexPath;
            } else if ([obj.titleString isEqualToString:@"Tracking Requests"]) {
                SRTrackingRequestsVC *vcTrackingRequests = [[SRTrackingRequestsVC alloc] initWithNibName:@"SRTrackingRequestsVC" bundle:nil];
                vcTrackingRequests.server = server;
                vcTrackingRequests.notificationDict = notificationDict;
                vcTrackingRequests.notificationsList = notificationsList;
                vcTrackingRequests.arrTrackingRequest = self->arrTrackingRequest;
                [self.navigationController pushViewController:vcTrackingRequests animated:YES];
                selectedRowindexPath = indexPath;
//                NSDictionary *notifyDict = obj.data;
//                if (notifyDict[kKeyId] != nil) {
//                    [APP_DELEGATE showActivityIndicator];
//                    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
//                    [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kPUT];
//                    isClearNotifications = NO;
//
//                }
            }
        }
            break;
        default:
            break;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    CellData *obj = _dataArray[indexPath.row];
    switch (obj.type) {
        case CELLTYPE_COUNT: {
            if ([obj.titleString isEqualToString:@"My Connections Tracking Me"]) {
                return NO;
            } else if ([obj.titleString isEqualToString:@"Connections I'm Tracking"]) {
                return NO;
            } else if ([obj.titleString isEqualToString:@"Notifications"]) {
                return NO;
            } else if (![obj.titleString isEqualToString:@"Tracking Requests"]) {
                return YES;
            }
        }
            break;
        default:
            break;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {


    CellData *obj = _dataArray[indexPath.row];
    [clearNotificationArr removeObjectIdenticalTo:obj];
    selectedRowindexPath = indexPath;
    NSDictionary *dataDict = [[NSDictionary alloc] initWithDictionary:obj.data];
    if (dataDict[kKeyId] != nil) {
        [APP_DELEGATE showActivityIndicator];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, dataDict[kKeyId]];
        [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kPUT];
        isClearNotifications = NO;
    }
}


#pragma mark - Allow/Cancel tracking


- (void)allowTrackingClick:(UIButton *)sender {
    NSLog(@"allowTrackingClick");
    currentIndex = sender.tag;

    buttonPosition = [sender convertPoint:CGPointZero toView:self.objtableView];
    NSIndexPath *indexPath = [self.objtableView indexPathForRowAtPoint:buttonPosition];
    NotificationAddressCell *cell = (NotificationAddressCell *) [(UITableView *) _objtableView cellForRowAtIndexPath:indexPath];
    if ([notificationsList count] > 0) {
        if (currentIndex <= notificationsList.count) {
            NSDictionary *locationDict = [notificationDict valueForKey:notificationsList[currentIndex]];
            // Call API to allow tracking
            // Add locations to track on scrollview
            NSMutableArray *locationsArr = [[NSMutableArray alloc] initWithArray:[locationDict valueForKey:kKeyLocations]];

            NSMutableArray *idsArray = [[NSMutableArray alloc] init];
            TrackingUserDetails *detailObj = [[TrackingUserDetails alloc] init];
            detailObj.locationObjects = [[NSMutableArray alloc] initWithArray:cell.selectedLocationsArr];

            for (int i = 0; i < detailObj.locationObjects.count; i++) {
                TrackingLocationClass *locationObj = detailObj.locationObjects[i];
                [idsArray addObject:locationObj.locationId];
            }
            //Create array to store values of locations
            NSMutableArray *addLocationsArray = [[NSMutableArray alloc] init];
            for (int j = 0; j < locationsArr.count; j++) {
                if ([idsArray containsObject:[locationsArr[j] valueForKey:kKeyId]]) {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:[locationsArr[j] valueForKey:kKeyId] forKey:kKeyId];
                    if ([cell.onOffSwitch isOn])
                        [locDict setValue:@"1" forKey:kKeyNotifications];
                    else
                        [locDict setValue:@"0" forKey:kKeyNotifications];
                    [locDict setValue:@"1" forKey:kKeyStatus];
                    [addLocationsArray addObject:locDict];
                } else {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:[locationsArr[j] valueForKey:kKeyId] forKey:kKeyId];
                    [locDict setValue:@"0" forKey:kKeyNotifications];
                    [locDict setValue:@"0" forKey:kKeyStatus];
                    [addLocationsArray addObject:locDict];
                }
            }
            if ([locationsArr count] == 1) {
                NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
                [postDict setValue:addLocationsArray forKey:@"tracking"];
                NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                locDict = locationsArr[0];
                NSData *json = [NSJSONSerialization dataWithJSONObject:addLocationsArray options:0 error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
                NSMutableDictionary *trackDict = [@{@"trackings": jsonString, kKeyAllowTrackingMe: @"1", @"tracker_id": [locDict valueForKey:@"tracker_id"]} mutableCopy];
                NSLog(@"%@", trackDict);
                //Add list of address for tracking locations
                [server makeAsychronousRequest:kkeyClassUpdateNotificationPlace inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
            } else {
                NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
                [postDict setValue:addLocationsArray forKey:@"tracking"];
                NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                locDict = locationsArr[0];
                NSData *json = [NSJSONSerialization dataWithJSONObject:addLocationsArray options:0 error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
                NSMutableDictionary *trackDict = [@{@"trackings": jsonString, kKeyAllowTrackingMe: @"1", @"tracker_id": [locDict valueForKey:@"tracker_id"]} mutableCopy];
                NSLog(@"%@", trackDict);
                //Add list of address for tracking locations
                [server makeAsychronousRequest:kkeyClassUpdateNotificationPlace inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
            }
        }
        [server makeAsychronousRequest:kkeyClassAddTrackingLocation inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    }
}

- (void)cancelTrackingClick:(UIButton *)sender {
    NSLog(@"cancelTrackingClick");
    currentIndex = sender.tag;
    buttonPosition = [sender convertPoint:CGPointZero toView:self.objtableView];
    NSIndexPath *indexPath = [self.objtableView indexPathForRowAtPoint:buttonPosition];
    NotificationAddressCell *cell = (NotificationAddressCell *) [(UITableView *) _objtableView cellForRowAtIndexPath:indexPath];

    if (currentIndex <= notificationsList.count) {
        NSDictionary *locationDict = [notificationDict valueForKey:notificationsList[currentIndex]];
        NSMutableArray *locationsArr = [[NSMutableArray alloc] initWithArray:[locationDict valueForKey:kKeyLocations]];

        NSMutableArray *idsArray = [[NSMutableArray alloc] init];
        TrackingUserDetails *detailObj = [[TrackingUserDetails alloc] init];
        detailObj.locationObjects = [[NSMutableArray alloc] initWithArray:cell.selectedLocationsArr];

        for (int i = 0; i < detailObj.locationObjects.count; i++) {
            TrackingLocationClass *locationObj = detailObj.locationObjects[i];
            [idsArray addObject:locationObj.locationId];
        }

        //Create array to store values of locations
        NSMutableArray *addLocationsArray = [[NSMutableArray alloc] init];
        for (int j = 0; j < locationsArr.count; j++) {
            NSDictionary *params = @{kKeyId: [locationsArr[j] valueForKey:kKeyId]};
            [addLocationsArray addObject:params];
        }
        NSData *json = [NSJSONSerialization dataWithJSONObject:addLocationsArray options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSMutableDictionary *payload = [@{@"deleteids": jsonString} mutableCopy];
        NSString *url = [NSString stringWithFormat:@"%@/0", kkeyClassGetImTrackingList];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:YES inMethodType:kDELETE];
    }

}


#pragma mark - Add/Remove location button action


- (void)removeAddressToTrack:(UIButton *)sender {
    SRButton *selectedButton = (SRButton *) sender;
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kkeyClassUpdateTrackLocOtherUser, [addressArray[selectedButton.tag] valueForKey:kKeyId]];
    // Remove address from tracking locations
    [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:YES inMethodType:kDELETE];
}


#pragma mark - distanceBtnAction


- (void)distanceBtnAction:(UIButton *)sender {
    isEmptyAddressCell = YES;
    selectedIndex = sender.tag;
    self.selectedTfIndex = [NSIndexPath indexPathForRow:selectedIndex inSection:1];
    SRButton *selectedButton = (SRButton *) sender;
    self.selectedTfIndex = selectedButton.indexPath;
    if (addressArray.count > 0) {
        NSDictionary *dataDict = addressArray[selectedIndex];
        SRNotifyMeMapViewController *notifyMap = [[SRNotifyMeMapViewController alloc] initWithNibName:@"SRNotifyMeMapViewController" bundle:nil profileData:dataDict server:server];
        notifyMap.delegate = self;
        notifyMap.trackUserDict = dataDict;
        [self.navigationController pushViewController:notifyMap animated:YES];
    }
}



#pragma mark - ClearNotificationsBtnAction


- (void)clearNotificationsBtnAction {
    [APP_DELEGATE showActivityIndicator];
    for (NSDictionary *notifyDict in inOutNotifArr) {
        if ([notifyDict[kKeyReferenceType] intValue] == 6 && ![notifyDict[kKeyStatus] boolValue]) {
            if (notifyDict[kKeyId] != nil) {
                NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kPUT];


                isClearNotifications = YES;
            }
        }
    }
}


#pragma mark - OnOffSwitchAction


- (void)oneOffSwitchAction:(id)sender {

    SRSwitch *onOffSwitch = (SRSwitch *) sender;

    dispatch_async(dispatch_get_main_queue(), ^{

        _switchState = !_switchState;

        CellData *obj = _dataArray[onOffSwitch.indexPath.row];
        TrackingUserDetails *detailObj = (TrackingUserDetails *) obj.data;
        detailObj.pushNotificationStatus = !detailObj.pushNotificationStatus;
        obj.data = detailObj;
        _dataArray[onOffSwitch.indexPath.row] = obj;
        [_objtableView reloadRowsAtIndexPaths:@[onOffSwitch.indexPath] withRowAnimation:UITableViewRowAnimationNone];

    });


}


#pragma mark - TextField Delegate Method


// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    SRTextField *selectedTextField = (SRTextField *) sender;
    self.selectedTfIndex = selectedTextField.indexPath;
    if (self.selectedTfIndex) {
        if (selectedTextField.text.length > 0) {
            [sender resignFirstResponder];
            return;
        } else {
            isEmptyAddressCell = NO;
            if (addressArray.count == 0) {
                [sender resignFirstResponder];
                SRNotifyMeMapViewController *notifyMap = [[SRNotifyMeMapViewController alloc] initWithNibName:@"SRNotifyMeMapViewController" bundle:nil profileData:nil server:server];
                notifyMap.delegate = self;
                [self.navigationController pushViewController:notifyMap animated:YES];
                [sender resignFirstResponder];
                return;
            }
            NSDictionary *dataDict = addressArray[selectedIndex];
            SRNotifyMeMapViewController *notifyMap = [[SRNotifyMeMapViewController alloc] initWithNibName:@"SRNotifyMeMapViewController" bundle:nil profileData:dataDict server:server];
            notifyMap.delegate = self;
            notifyMap.trackUserDict = dataDict;
            [self.navigationController pushViewController:notifyMap animated:YES];
            [sender resignFirstResponder];
        }
    }
}

// --------------------------------------------------------------------------------
//textFieldDidEndEditing
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

// --------------------------------------------------------------------------------
//textFieldDidEndEditing
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - SRNotifyMeMapDelegate Method


- (void)setTrackingAddress:(NSMutableDictionary *)locDict {
    addressDict = [[NSMutableDictionary alloc] initWithDictionary:locDict];
////    [addressArray addObject:addressDict];
//    CellData *obj4 = [[CellData alloc] init];
//    obj4.data = nil;
//    obj4.titleString = [locDict valueForKey:kKeyAddress];
//    obj4.addrType = [locDict valueForKey:kKeyName];
//    obj4.distanceString = [locDict valueForKey:kKeyRadius];
//    obj4.type = CELLTYPE_ADDRESS_TYPE;
//
//    _dataArray[self.selectedTfIndex.row] = obj4;

//    if (addressArray.count > 0)
//    {
//        if (addressArray.count >= selectedIndex || addressArray.count <= selectedIndex) {
//            [addressArray removeObjectAtIndex:selectedIndex];
//            [addressArray insertObject:addressDict atIndex:selectedIndex];
//        }
//    }

    // call webservice to save location
    if ([locDict valueForKey:kKeyId]) {
        addressDict[kKeyUserID] = [server.loggedInUserInfo valueForKey:kKeyId];
        // Add list of address tracking locations
        [server makeAsychronousRequest:kkeyClassAddTrackingLocation inParams:addressDict isIndicatorRequired:NO inMethodType:kPOST];
    } else {
        addressDict[kKeyUserID] = [server.loggedInUserInfo valueForKey:kKeyId];
        // Add list of address tracking locations
        [server makeAsychronousRequest:kkeyClassAddTrackingLocation inParams:addressDict isIndicatorRequired:NO inMethodType:kPOST];
    }


}

-(void)removeAllAddress {
    for (int i = _dataArray.count-1; i >= 0; i--) {
        if ([[_dataArray objectAtIndex:i] isKindOfClass:[CellData classForCoder]]) {
            if (((CellData*)[_dataArray objectAtIndex:i]).type == CELLTYPE_ADDRESS_TYPE) {
                [_dataArray removeObjectAtIndex:i];
            }
        }
    }
}


#pragma mark - Notification Method


// --------------------------------------------------------------------------------
// getMyTrackLocationsSucceed:
- (void)getMyTrackLocationsSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSDictionary class]]) {
        // Addd addresses in address array
        trackMeCount = [object valueForKey:@"tracking_me_count"];
        trackingCount = [object valueForKey:@"tracking_count"];
        addressArray = [object valueForKey:@"tracking_location"];
        if(addressArray == nil) {
            return;
        }
        NSInteger index = 3;
        
        [self removeAllAddress];
        
        for (int i = 0; i < addressArray.count; i++) {
            CellData *obj4 = [[CellData alloc] init];
            obj4.titleString = [addressArray[i] valueForKey:kKeyAddress];
            obj4.addrType = [addressArray[i] valueForKey:kKeyName];
            obj4.distanceString = [addressArray[i] valueForKey:kKeyRadius];
            obj4.type = CELLTYPE_ADDRESS_TYPE;
            [_dataArray insertObject:obj4 atIndex:index];
            index++;
        }
        //Add Blank cell in last.
        CellData *obj4 = [[CellData alloc] init];
        obj4.data = nil;
        obj4.type = CELLTYPE_ADDRESS_TYPE;
        [_dataArray insertObject:obj4 atIndex:index];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.objtableView reloadData];
        });
    }
}

// --------------------------------------------------------------------------------
// getMyTrackLocationsFailed:
- (void)getMyTrackLocationsFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// createMyTrackLocationSucceed:
- (void)createMyTrackLocationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSDictionary class]]) {
        // add new
        NSInteger index = 3;
        if (addressArray.count > 0) {
            index = index + addressArray.count;
        }
        BOOL isAlreadyexists = NO;
        for (int i = 0; i < addressArray.count; i++) {
            if ([[addressArray[i] valueForKey:kKeyId] isEqualToString:[object valueForKey:kKeyId]]) {
                addressArray[i] = object;
                isAlreadyexists = YES;
            }
        }
        if (!isAlreadyexists) {
            [addressArray addObject:object];
        }

        if (!isEmptyAddressCell) {
            CellData *obj4 = [[CellData alloc] init];
            obj4.data = nil;
            obj4.type = CELLTYPE_ADDRESS_TYPE;
            [_dataArray insertObject:obj4 atIndex:index + 1];
        }

        [self.objtableView reloadData];
    }
}

// --------------------------------------------------------------------------------
// createMyTrackLocationFailed:
- (void)createMyTrackLocationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// deleteMyTrackLocationSucceed:
- (void)deleteMyTrackLocationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *dict = [inNotify object];
    for (int i = 0; i < addressArray.count; i++) {
        NSString *delete_id = [addressArray[i] valueForKey:kKeyId];
        if ([delete_id integerValue] == [[dict valueForKey:kKeyId] integerValue]) {
            [addressArray removeObjectAtIndex:i];
            [_dataArray removeObjectAtIndex:i + 3];
            [self.objtableView reloadData];
        }
    }
}

// --------------------------------------------------------------------------------
// deleteMyTrackLocationFailed:
- (void)deleteMyTrackLocationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// UpdateNotificationPlaceSucceed:
- (void)UpdateNotificationPlaceSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSIndexPath *indexPath = [self.objtableView indexPathForRowAtPoint:buttonPosition];
    NSInteger numberOfRows = 6 + addressArray.count;
    NSIndexPath *path = [NSIndexPath indexPathForRow:numberOfRows inSection:0];
    if (indexPath.row <= _dataArray.count) {
        [self.objtableView beginUpdates];
        [_dataArray removeObjectAtIndex:indexPath.row];
        if (path != indexPath) {
            [self.objtableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
            [self.objtableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
            [self.objtableView endUpdates];
        }
    }
    [(APP_DELEGATE) callNotificationsApi];
}

// --------------------------------------------------------------------------------
// UpdateNotificationPlaceFailed:
- (void)UpdateNotificationPlaceFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:
- (void)settingUpdateSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSString *location_notification = [response valueForKey:kkeylocation_notification];
    NSString *tracking_me = [response valueForKey:kkeytracking_me];

    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:location_notification forKey:kkeylocation_notification];
    [userDef setValue:tracking_me forKey:kkeytracking_me];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:location_notification forKey:kkeylocation_notification];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:tracking_me forKey:kkeytracking_me];
    [userDef synchronize];

}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}
// --------------------------------------------------------------------------------
// getTrackingNotificationSucceed:

- (void)getTrackingNotificationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    arrTrackingRequest = [[NSMutableArray alloc] init];
    notificationDict = [inNotify object];
    if (notificationDict.count > 0) {
        NSArray *arr = [notificationDict allKeys];
        for (int i = 0; i < arr.count; i++) {
            if (![notificationsList containsObject:arr[i]]) {
                [notificationsList addObject:arr[i]];
                NSDictionary *dict = [notificationDict valueForKey:notificationsList[i]];
                
                CellData *obj8 = [[CellData alloc] init];
                               obj8.type = CELLTYPE_PUSH_NOTIFICATION;
                               obj8.tag = i;
                               TrackingUserDetails *detailObj = [[TrackingUserDetails alloc] init];
                               detailObj.dataDict = dict;
                               if (dict[kKeySender] != [NSNull null]) {

                                   detailObj.username = [NSString stringWithFormat:@"%@ %@", [dict[kKeySender] objectForKey:kKeyFirstName], [dict[kKeySender] objectForKey:kKeyLastName]];

                               }
                               detailObj.userImage = [UIImage imageNamed:@"tabbar-user-female.png"];
                               detailObj.pushNotificationStatus = @1;

                               // Create location array of user
                               detailObj.locationObjects = [[NSMutableArray alloc] init];
                               NSMutableArray *locationsArr = [[NSMutableArray alloc] initWithArray:[dict valueForKey:kKeyLocations]];


                for (int i = 0; i < locationsArr.count; i++) {
                    if ([locationsArr[i] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dict = locationsArr[i];
                        TrackingLocationClass *location1 = [[TrackingLocationClass alloc] init];
                         location1.locationName = @"";
                         location1.radius = @"";
                         location1.address = @"";
                         location1.notify_on = @0;
                         location1.notify = @0;
                         location1.locationId = @"";
                         
                        
                        if (dict[kKeyLocation] != [NSNull null]) {
                            id location = dict[kKeyLocation];
                            
                            if ([location valueForKey:kKeyAddressName] != [NSNull null]){
                                 location1.locationName = [location valueForKey:kKeyAddressName];
                            }
                            
                            location1.radius = [location valueForKey:@"radius"];
                            location1.address = [location valueForKey:@"address"];
                            location1.notify = [location valueForKey:@"notify"];
                        }
  
                        location1.locationId = [dict valueForKey:kKeyId];
                        location1.notify_on = [dict valueForKey:@"notify_on"];
      
                        location1.isSelected = YES;
                        [detailObj.locationObjects addObject:location1];
                        
                    }
                }
                //obj8.data = detailObj;
                if (![_dataArray containsObject:obj8]) {
                    [arrTrackingRequest addObject:detailObj];
                }
            }
        }
        
        [self setNavViewShowCount:(notificationsList.count > 0) count:[NSString stringWithFormat:@"%lu", (unsigned long)notificationsList.count]];
        [self.objtableView reloadData];
    } else {
        [self setNavViewShowCount:false count:@"0"];
        [notificationsList removeAllObjects];
    }


    //Set Arrival Departure Notifications
    if (!isAddedOnce) {
        isAddedOnce = YES;

        inOutNotifArr = [[NSMutableArray alloc] initWithArray:server.notificationArr];
        inOutNotifArr = [self sortArrayBasedOndate:inOutNotifArr];
        NSArray *reverseArray = [[inOutNotifArr reverseObjectEnumerator] allObjects];
        inOutNotifArr = [[NSMutableArray alloc] initWithArray:reverseArray];

        if (inOutNotifArr.count > 0) {
            NSInteger rowIndex = _dataArray.count;
            for (int i = 0; i < inOutNotifArr.count; i++) {
                NSDictionary *dict = inOutNotifArr[i];
                if ([dict[kKeyReferenceType] intValue] == 6 && ![dict[kKeyStatus] boolValue]) {
                    CellData *obj9 = [[CellData alloc] init];
                    if ([dict[kKeyReferenceStatus] intValue] == 1 || [dict[kKeyReferenceStatus] intValue] == 2) {
                        if (dict[kKeySender] != [NSNull null]) {
                            NSString *fname = [NSString stringWithFormat:@"%@ %@.", [dict[kKeySender] objectForKey:kKeyFirstName], [[dict[kKeySender] objectForKey:kKeyLastName] substringWithRange:NSMakeRange(0, 1)]];
                            NSString *address;
                            if (dict[kKeyReference] != [NSNull null]) {
                                if ([dict[kKeyReference] objectForKey:kKeyLocation] != [NSNull null]) {
                                    NSDictionary *locDict = [dict[kKeyReference] objectForKey:kKeyLocation];
                                    if (locDict[kKeyAddress] != [NSNull null]) {
                                        address = locDict[kKeyAddress];
                                    }
                                }
                            }

                            if ([dict[kKeyReferenceStatus] intValue] == 1) {
//                            if (![[dict valueForKey:@"notification_date"] isKindOfClass:[NSNull class]])
//                            {
//                                obj9.titleString = [[[dict valueForKey:@"notification_date"] stringByAppendingString:@" - "]stringByAppendingString:[NSString stringWithFormat:@"%@ has arrived at %@", fname, address]];
//
//                            }
//                            else
                                obj9.titleString = [[[self displayDayTime:dict] stringByAppendingString:@" - "] stringByAppendingString:[NSString stringWithFormat:@"%@ has arrived at %@", fname, address]];
                            } else if ([dict[kKeyReferenceStatus] intValue] == 2) {
//                            if (![[dict valueForKey:@"notification_date"] isKindOfClass:[NSNull class]])
//                            {
//                                obj9.titleString = [[[dict valueForKey:@"notification_date"] stringByAppendingString:@" - "]stringByAppendingString:[NSString stringWithFormat:@"%@ has departed from %@", fname, address]];
//
//                            }
//                            else
                                obj9.titleString = [[[self displayDayTime:dict] stringByAppendingString:@" - "] stringByAppendingString:[NSString stringWithFormat:@"%@ has departed from %@", fname, address]];

                            }
                            obj9.type = CELLTYPE_COUNT;
                            obj9.tag = i;
                            obj9.data = dict;


                            if (!isOnce) {
                                isOnce = YES;
                                CellData *obj10 = [[CellData alloc] init];
                                obj10.type = CELLTYPE_COUNT;
                                obj10.tag = i;
                                obj10.data = dict;
                                obj10.titleString = [NSString stringWithFormat:@"Notifications"];
                                [_dataArray insertObject:obj10 atIndex:rowIndex];
                                rowIndex = _dataArray.count;
                            }
                            [clearNotificationArr addObject:obj9];
                            rowIndex++;
                        }
                    }
                }
                [self.objtableView reloadData];

            }
        }

    }

}

// --------------------------------------------------------------------------------
// getTrackingNotificationFailed:

- (void)getTrackingNotificationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// updateNotificationSucceed:

- (void)updateNotificationSucceed:(NSNotification *)inNotify {
    //TODO: - Loop every time when accept request tracking
    [APP_DELEGATE hideActivityIndicator];
    if (!isClearNotifications) {
        if (selectedRowindexPath) {
            if (selectedRowindexPath.row < _dataArray.count) {
                CellData *obj = _dataArray[selectedRowindexPath.row];
                [clearNotificationArr removeObjectIdenticalTo:obj];

                [self.objtableView beginUpdates];
                [_dataArray removeObjectAtIndex:selectedRowindexPath.row];
                [self.objtableView deleteRowsAtIndexPaths:@[selectedRowindexPath] withRowAnimation:UITableViewRowAnimationFade];
                [self.objtableView endUpdates];
                [self.objtableView reloadData];
            }
        }
    } else {
//        isClearNotifications = NO;
        [_dataArray removeObjectsInArray:clearNotificationArr];
        [clearNotificationArr removeAllObjects];
        [self.objtableView reloadData];
    }
    [(APP_DELEGATE) callNotificationsApi];

}

// --------------------------------------------------------------------------------
// updateNotificationFailed:

- (void)updateNotificationFailed:(NSNotification *)inNotify {


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Switch Action Method

- (void)onOffSwitchValueChanged:(id)sender {

    SRSwitch *onOffSwitch = (SRSwitch *) sender;
    CellData *obj = _dataArray[onOffSwitch.indexPath.row];

    if ([obj.titleString isEqualToString:CONNECTIONS_TRACKING_ME]) {
        // Call API to enable / disable Connection Tracking Me
        NSString *tracking_me;
        if (onOffSwitch.isOn) {
            tracking_me = @"1";
        } else
            tracking_me = @"0";

        NSDictionary *payload = @{
                kKeyFieldName: kkeytracking_me,
                kKeyFieldValue: tracking_me
        };

        NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];

    } else {

        // Call API to enable / disable Push Notification of current user
        NSString *location_notification;
        if (onOffSwitch.isOn) {
            location_notification = @"1";
        } else
            location_notification = @"0";

        NSDictionary *payload = @{
                kKeyFieldName: kkeylocation_notification,
                kKeyFieldValue: location_notification
        };

        NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
    }

}

# pragma mark - private methods

// --------------------------------------------------------------------------------
// displayDayTime:
- (NSString *)displayDayTime:(NSDictionary *)dict {
    NSDate *currentDate;
    if (![[dict valueForKey:@"notification_date"] isKindOfClass:[NSNull class]])
        currentDate = [SRModalClass returnFullDate:[dict valueForKey:@"notification_date"]];
    else
        currentDate = [SRModalClass returnFullDate:[dict valueForKey:kKeyEventCreated]];
    NSDateFormatter *time = [[NSDateFormatter alloc] init];

    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    [time setDateFormat:@"MMMM d, YYYY 'at' h:mm a"];
    [time setAMSymbol:@"am"];
    [time setPMSymbol:@"pm"];
    //GMT removed here
    NSString *timeString = [[[time stringFromDate:currentDate] stringByAppendingString:@" "] stringByAppendingString:localAbbreviation];
//    NSString *dayTime = [NSString stringWithFormat:@"%@", [time stringFromDate:currentDate]];

    return timeString;
}

- (NSMutableArray *)sortArrayBasedOndate:(NSMutableArray *)arraytoSort {
    NSDateFormatter *fmtDate = [[NSDateFormatter alloc] init];
    [fmtDate setDateFormat:@"yyyy-MM-dd"];

    NSDateFormatter *fmtTime = [[NSDateFormatter alloc] init];
    [fmtTime setDateFormat:@"hh:mm:ss"];

    NSComparator compareDates = ^(id string1, id string2) {
        NSDate *date1 = [fmtDate dateFromString:string1];
        NSDate *date2 = [fmtDate dateFromString:string2];

        return [date1 compare:date2];
    };

    NSComparator compareTimes = ^(id string1, id string2) {
        NSDate *time1 = [fmtTime dateFromString:string1];
        NSDate *time2 = [fmtTime dateFromString:string2];

        return [time1 compare:time2];
    };

    NSSortDescriptor *sortDesc1 = [[NSSortDescriptor alloc] initWithKey:@"start_date" ascending:YES comparator:compareDates];
    NSSortDescriptor *sortDesc2 = [NSSortDescriptor sortDescriptorWithKey:@"starts" ascending:YES comparator:compareTimes];
    [arraytoSort sortUsingDescriptors:@[sortDesc1, sortDesc2]];

    return arraytoSort;
}

@end
