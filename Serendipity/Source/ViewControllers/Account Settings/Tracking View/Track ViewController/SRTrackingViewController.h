//
//  SRTrackingViewController.h
//  Serendipity
//
//  Created by Leo on 17/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRTrackingViewCell.h"
#import "TrackingSwitchCell.h"
#import "SRAddressCell.h"
#import "TextMiddleAlignCell.h"
#import "TrackingCountDisplayCell.h"
#import "SRTrackingCell.h"
#import "NotificationAddressCell.h"
#import "TrackingUserDetails.h"
#import "TrackingLocationClass.h"
#import "SRSwitch.h"
#import "SRConnectionAmTrackingVC.h"
#import "SRConnectionTrackingMeVC.h"
#import "SRNotifyMeMapViewController.h"
#import "SRTrackOnMapViewController.h"
#import "SRNotifyMeViewController.h"
#import "SRAddConnectionTabViewController.h"
#import "SRTrackingRequestsVC.h"
#import "NavBarView.h"

@interface SRTrackingViewController : ParentViewController
{
    NSArray *menuItemArray;
    NSMutableArray *updatedNotiList,*notificationsList;
    NSMutableArray *addressArray;
    NSInteger currentIndex;
    CGPoint buttonPosition;
    // Server
    SRServerConnection *server;
    NSMutableDictionary *addressDict,*notificationDict;
    NSString *trackingCount,*trackMeCount;
    NSMutableArray *inOutNotifArr;
    NSIndexPath *selectedRowindexPath;
    BOOL isAddedOnce;
    BOOL isClearNotifications;
    BOOL isOnce,isDuplicate;
    BOOL isEmptyAddressCell;
    BOOL isSelectNotifications;
    NSMutableArray *clearNotificationArr;
    NSInteger selectedIndex;
    
    NSMutableArray<TrackingUserDetails *>* arrTrackingRequest;
}
// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *backGroundImage;
@property (strong, nonatomic) IBOutlet SRProfileImageView *profileImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileBackgroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileBackgroundColor;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UITableView *objtableView;
@property (strong, nonatomic) IBOutlet UIImageView *logoAppImage;
@property (nonatomic) NSIndexPath *selectedTfIndex;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
