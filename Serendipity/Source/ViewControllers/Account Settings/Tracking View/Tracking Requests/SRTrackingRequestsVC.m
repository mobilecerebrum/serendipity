//
//  TrackingRequestsVC.m
//  Serendipity
//
//  Created by Rahul Patel on 18/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "SRTrackingRequestsVC.h"

@interface SRTrackingRequestsVC ()

@end

@implementation SRTrackingRequestsVC

- (void)viewDidLoad {
    [super viewDidLoad];
        // Set title
    [SRModalClass setNavTitle:NSLocalizedString(@"lbl.track_req.txt", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.tblViewTrackingRequest.dataSource = self;
    self.tblViewTrackingRequest.delegate = self;
    
    [self.tblViewTrackingRequest setTableFooterView:[UIView new]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(UpdateNotificationPlaceSucceed:)
        name:kKeyNotificationUpdateNotificationPlaceSucceed    object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(UpdateNotificationPlaceSucceed:)
        name:kKeyNotificationConnImTrackSucceed    object:nil];

    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kKeyNotificationUpdateNotificationPlaceSucceed object:nil];
}

- (void)backBtnAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)oneOffSwitchAction:(id)sender {
    SRSwitch *onOffSwitch = (SRSwitch *) sender;
    TrackingUserDetails *detailObj = self.arrTrackingRequest[onOffSwitch.indexPath.row];
    detailObj.pushNotificationStatus = !detailObj.pushNotificationStatus;
    self.arrTrackingRequest[onOffSwitch.indexPath.row] = detailObj;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tblViewTrackingRequest reloadRowsAtIndexPaths:@[onOffSwitch.indexPath] withRowAnimation:UITableViewRowAnimationNone];
    });
}


- (void)allowTrackingClick:(UIButton *)sender {
//    NSLog(@"allowTrackingClick");
    currentIndex = sender.tag;

    buttonPosition = [sender convertPoint:CGPointZero toView:self.tblViewTrackingRequest];
    NSIndexPath *indexPath = [self.tblViewTrackingRequest indexPathForRowAtPoint:buttonPosition];
    NotificationAddressCell *cell = (NotificationAddressCell *) [self.tblViewTrackingRequest cellForRowAtIndexPath:indexPath];
    if ([self.notificationsList count] > 0) {
        if (currentIndex <= self.notificationsList.count) {
            NSDictionary *locationDict = [self.notificationDict valueForKey:self.notificationsList[currentIndex]];
            // Call API to allow tracking
            // Add locations to track on scrollview
            NSMutableArray *locationsArr = [[NSMutableArray alloc] initWithArray:[locationDict valueForKey:kKeyLocations]];

            NSMutableArray *idsArray = [[NSMutableArray alloc] init];
            TrackingUserDetails *detailObj = [[TrackingUserDetails alloc] init];
            detailObj.locationObjects = [[NSMutableArray alloc] initWithArray:cell.selectedLocationsArr];

            for (int i = 0; i < detailObj.locationObjects.count; i++) {
                TrackingLocationClass *locationObj = detailObj.locationObjects[i];
                [idsArray addObject:locationObj.locationId];
            }
            //Create array to store values of locations
            NSMutableArray *addLocationsArray = [[NSMutableArray alloc] init];
            for (int j = 0; j < locationsArr.count; j++) {
                if ([idsArray containsObject:[locationsArr[j] valueForKey:kKeyId]]) {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:[locationsArr[j] valueForKey:kKeyId] forKey:kKeyId];
                    if ([cell.onOffSwitch isOn])
                        [locDict setValue:@"1" forKey:kKeyNotifications];
                    else
                        [locDict setValue:@"1" forKey:kKeyNotifications];
                    [locDict setValue:@"1" forKey:kKeyStatus];
                    [addLocationsArray addObject:locDict];
                } else {
                    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                    [locDict setValue:[locationsArr[j] valueForKey:kKeyId] forKey:kKeyId];
                    [locDict setValue:@"0" forKey:kKeyNotifications];
                    [locDict setValue:@"0" forKey:kKeyStatus];
                    [addLocationsArray addObject:locDict];
                }
            }
            if ([locationsArr count] == 1) {
                NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
                [postDict setValue:addLocationsArray forKey:@"tracking"];
                NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                locDict = locationsArr[0];
                NSData *json = [NSJSONSerialization dataWithJSONObject:addLocationsArray options:0 error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
     //           NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
                NSMutableDictionary *trackDict = [@{@"trackings": jsonString, kKeyAllowTrackingMe: @"1", @"tracker_id": [locDict valueForKey:@"tracker_id"]} mutableCopy];
    //            NSLog(@"%@", trackDict);
                //Add list of address for tracking locations
                [self.server makeAsychronousRequest:kkeyClassUpdateNotificationPlace inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
            } else {
                NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
                [postDict setValue:addLocationsArray forKey:@"tracking"];
                NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
                locDict = locationsArr[0];
                NSData *json = [NSJSONSerialization dataWithJSONObject:addLocationsArray options:0 error:nil];
                NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
                NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);
                NSMutableDictionary *trackDict = [@{@"trackings": jsonString, kKeyAllowTrackingMe: @"1", @"tracker_id": [locDict valueForKey:@"tracker_id"]} mutableCopy];
                NSLog(@"%@", trackDict);
                //Add list of address for tracking locations
                [self.server makeAsychronousRequest:kkeyClassUpdateNotificationPlace inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
            }
        }
        [self.server makeAsychronousRequest:kkeyClassAddTrackingLocation inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    }
}

- (void)cancelTrackingClick:(UIButton *)sender {
    NSLog(@"cancelTrackingClick");
    currentIndex = sender.tag;
    buttonPosition = [sender convertPoint:CGPointZero toView:self.tblViewTrackingRequest];
    NSIndexPath *indexPath = [self.tblViewTrackingRequest indexPathForRowAtPoint:buttonPosition];
    NotificationAddressCell *cell = (NotificationAddressCell *) [self.tblViewTrackingRequest cellForRowAtIndexPath:indexPath];

    if (currentIndex <= self.notificationsList.count) {
        NSDictionary *locationDict = [self.notificationDict valueForKey:self.notificationsList[currentIndex]];
        NSMutableArray *locationsArr = [[NSMutableArray alloc] initWithArray:[locationDict valueForKey:kKeyLocations]];

        NSMutableArray *idsArray = [[NSMutableArray alloc] init];
        TrackingUserDetails *detailObj = [[TrackingUserDetails alloc] init];
        detailObj.locationObjects = [[NSMutableArray alloc] initWithArray:cell.selectedLocationsArr];

        for (int i = 0; i < detailObj.locationObjects.count; i++) {
            TrackingLocationClass *locationObj = detailObj.locationObjects[i];
            [idsArray addObject:locationObj.locationId];
        }

        //Create array to store values of locations
        NSMutableArray *addLocationsArray = [[NSMutableArray alloc] init];
        for (int j = 0; j < locationsArr.count; j++) {
            NSDictionary *params = @{kKeyId: [locationsArr[j] valueForKey:kKeyId]};
            [addLocationsArray addObject:params];
        }
        NSData *json = [NSJSONSerialization dataWithJSONObject:addLocationsArray options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSMutableDictionary *payload = [@{@"deleteids": jsonString} mutableCopy];
        NSString *url = [NSString stringWithFormat:@"%@/0", kkeyClassGetImTrackingList];
        [self.server makeAsychronousRequest:url inParams:payload isIndicatorRequired:YES inMethodType:kDELETE];
    }

}

- (void)UpdateNotificationPlaceSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSIndexPath *indexPath = [self.tblViewTrackingRequest indexPathForRowAtPoint:buttonPosition];
//    NSInteger numberOfRows = 6 + addressArray.count;
//    NSIndexPath *path = [NSIndexPath indexPathForRow:numberOfRows inSection:0];
//    if (indexPath.row <= _dataArray.count) {
        [self.tblViewTrackingRequest beginUpdates];
        [self.arrTrackingRequest removeObjectAtIndex:indexPath.row];
//        if (path != indexPath) {
//            [self.tblViewTrackingRequest reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
            [self.tblViewTrackingRequest deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
            [self.tblViewTrackingRequest endUpdates];
//        }
//    }
    [(APP_DELEGATE) callNotificationsApi];
}


- (void)UpdateNotificationPlaceFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}




#pragma MARK: - UItableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrTrackingRequest.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationAddressCell"];

    if (!cell) {

        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"NotificationAddressCell" owner:nil options:nil];
        cell = [nibs firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //cell.backgroundColor = (__bridge UIColor * _Nullable)(UIColor.blackColor.CGColor);

    [cell.onOffSwitch addTarget:self action:@selector(oneOffSwitchAction:) forControlEvents:UIControlEventValueChanged];
    [cell.allowButton addTarget:self action:@selector(allowTrackingClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.allowButton.tag = indexPath.row;

    [cell.closeButton addTarget:self action:@selector(cancelTrackingClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.closeButton.tag = indexPath.row;

    TrackingUserDetails *detailObj = self.arrTrackingRequest[indexPath.row];

    [cell configureCell:detailObj indexPath:indexPath];
    
//    // User image
    if ([detailObj.dataDict[kKeySender][kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        if ([detailObj.dataDict[kKeySender][kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [detailObj.dataDict[kKeySender][kKeyProfileImage] objectForKey:kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                cell.profileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            cell.profileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    } else
        cell.profileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    
    return cell;
}

@end
