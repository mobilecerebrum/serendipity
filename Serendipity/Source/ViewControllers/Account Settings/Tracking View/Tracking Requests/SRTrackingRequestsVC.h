//
//  TrackingRequestsVC.h
//  Serendipity
//
//  Created by Rahul Patel on 18/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "TrackingUserDetails.h"
#import "NotificationAddressCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRTrackingRequestsVC : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    CGPoint buttonPosition;
    NSInteger currentIndex;
}

@property SRServerConnection *server;
@property NSMutableArray *notificationsList;
@property NSMutableDictionary *notificationDict;
@property (weak, nonatomic) IBOutlet UITableView *tblViewTrackingRequest;
@property NSMutableArray<TrackingUserDetails *>* arrTrackingRequest;

@end

NS_ASSUME_NONNULL_END
