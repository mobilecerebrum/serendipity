//
//  SRNotifyMeViewController.h
//  Serendipity
//
//  Created by Leo on 01/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRNotifyMeTableCell.h"

@interface SRNotifyMeViewController : ParentViewController<UITableViewDelegate,UITableViewDataSource>
{
    // Server
    SRServerConnection *server;
    NSArray *menuArray;
    NSDictionary *userDict;
	NSMutableArray *userLocationsArr,*arriveLocationsArr,*leaveLocationsArr,*trackingLocArray,*expandableArray;

    NSString *notify_on;
    BOOL fromAmTrackingView;
    
    
}
@property (strong,nonatomic) IBOutlet UITableView *notifyTable;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
