//
//  SRNotifyMeViewController.m
//  Serendipity
//
//  Created by Leo on 01/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRNotifyMeViewController.h"
#import "SRModalClass.h"
#import "SRNotifyMeTableCell.h"
#import "SRNotifyMeMapViewController.h"
#import "SwipeableTableViewCell.h"
#import "UIViewController+UIViewController_Custom_Methods.h"

@interface SRNotifyMeViewController ()

@end

@implementation SRNotifyMeViewController
// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    if ([nibNameOrNil isEqualToString:@"SRNotifyMeViewController"]) {
        fromAmTrackingView = YES;
    } else
        fromAmTrackingView = NO;
    self = [super initWithNibName:@"SRNotifyMeViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        server = inServer;
        userDict = [[NSDictionary alloc] initWithDictionary:inProfileDict];
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(GetTrackLocationsSucceed:)
                              name:kKeyNotificationGetTrackLocationsSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(GetTrackLocationsFailed:)
                              name:kKeyNotificationGetTrackLocationsFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(SaveTrackLocationsSucceed:)
                              name:kKeyNotificationSaveTrackLocationSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(SaveTrackLocationsFailed:)
                              name:kKeyNotificationSaveTrackLocationFailed object:nil];
        [defaultCenter addObserver:self selector:@selector(getListOfUserLocation) name:kKeyNotificationTrackAddedLocationSucceed object:nil];
    }

    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];

    [defaultCenter addObserver:self
                      selector:@selector(SaveTrackLocationsSucceed:)
                          name:kKeyNotificationSaveTrackLocationSucceed object:nil];

    arriveLocationsArr = [[NSMutableArray alloc] init];
    leaveLocationsArr = [[NSMutableArray alloc] init];
    expandableArray = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.notifyMe.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:@"Close" barImage:nil forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:@"Done" barImage:nil forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(actionOnDone:) forControlEvents:UIControlEventTouchUpInside];

    menuArray = @[NSLocalizedString(@"The Next Time ", @""), NSLocalizedString(@"Leaves", @""), NSLocalizedString(@"Arrives", @"")];

    // Get user location list


    // set up label
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.navigationController.navigationBar.frame.size.height)];
    footerView.backgroundColor = [UIColor clearColor];
    UIButton *otherBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 15, 120, 25)];
    [otherBtn setTitle:@"Add Address..." forState:UIControlStateNormal];
    [otherBtn.titleLabel setFont:[UIFont fontWithName:kFontHelveticaBold size:14]];
    [otherBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [otherBtn addTarget:self action:@selector(showNotifyMeMap:) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:otherBtn];
    self.notifyTable.tableFooterView = footerView;
    //Set leaves as default selected option
    notify_on = @"2";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     [self getListOfUserLocation];
}

#pragma mark - Action Method

- (void)getListOfUserLocation {
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kkeyClassGetTrackLocListOtherUser, userDict[kKeyId]];
    [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)sender {
//    SwipeableTableViewCell *cell = (SwipeableTableViewCell *) sender.view;
//    if (CGPointEqualToPoint(cell.scrollView.contentOffset, CGPointZero)) {
//        // Get pins detail
//        NSDictionary *pinDict = [myPinsArr objectAtIndex:cell.tag];
//
//        // Animate map to that position
//        CLLocationCoordinate2D coordinate = {[[pinDict valueForKey:kKeyLattitude] floatValue], [[pinDict valueForKey:kKeyRadarLong] floatValue]};
//        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:18];
//        [self.mapView animateToCameraPosition:camera];
//
//        // Create current selected marker
//        GMSMarker *dropMarker = [[GMSMarker alloc] init];
//        dropMarker.position = coordinate;
//        dropMarker.title = [pinDict objectForKey:kKeyName];
//        dropMarker.snippet = [pinDict objectForKey:kKeyAddress];
//        dropMarker.userData = pinDict;
//        dropMarker.icon = [UIImage imageNamed:@"map-pin.png"];
//        selectedMarker = dropMarker;
//        [self showPinWindow:pinDict];
//
//
//    } else
//        [cell.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)deletePinFromList:(UIButton *)inSender {
//    // Delete the pin
//    pinSuperView = inSender.superview;
//
//    UIAlertView *deleteAlert = [[UIAlertView alloc]
//                                initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:NSLocalizedString(@"delete.pin.alert.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.no.text", @"") otherButtonTitles:NSLocalizedString(@"alert.button.yes.text", @""), nil];
//    deleteAlert.tag = 2;
//    [deleteAlert show];
    
}

- (void)actionOnBack:(id)sender {
    [APP_DELEGATE hideActivityIndicator];
    if (fromAmTrackingView) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;

        UIViewController *vc = self.navigationController.viewControllers[numberOfViewControllers - 3];

        [self.navigationController popToViewController:vc animated:NO];
    }
}

- (void)actionOnDone:(id)sender {

    //Neha
    //Case 1: No address added for user
    if ([userLocationsArr count] == 0 ){
        [self showAlertWithTitle:@"" message:@"Please add address to enable the tracking."];
        return;
    }

    //Case 2: No address has been selected by the user.
    NSInteger arrivalLocationArrCount = [arriveLocationsArr count];
    NSInteger departureLocationArrCount = [leaveLocationsArr count];
    
    if (arrivalLocationArrCount == 0 && departureLocationArrCount == 0){
        [self showAlertWithTitle:@"" message:@"Please select address to enable the tracking."];
        return;
    }
    //Neha
    
    NSMutableArray *trackLocationArray = [[NSMutableArray alloc] init];
    if ([userLocationsArr count] > 0) {
        for (int i = 0; i < userLocationsArr.count; i++) {
            NSString *filter = @"%K CONTAINS[cd] %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyTrackLocId, [userLocationsArr[i] valueForKey:kKeyId]];
            NSArray *userArray1 = [trackingLocArray filteredArrayUsingPredicate:predicate];
           
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notify_on == %@" , @"2"];
            NSArray *userArray = [userArray1 filteredArrayUsingPredicate:predicate1];
            


           // if ([notify_on isEqualToString:@"2"]) {
                if ([leaveLocationsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]]) {
                    NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
                    if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
                        if (userArray.count >= 1){
                         [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                        }else{
                           [trackLocDict setValue:@"0" forKey:kKeyId];
                        }

                        [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
                    } else {
                        [trackLocDict setValue:@"" forKey:kKeyId];
                        [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
                    }
                    [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
                    [trackLocDict setValue:@"2" forKey:kKeyTrackNotify_On];
                    [trackLocDict setValue:@"1" forKey:kIslocationTrackOn];
                    
                    if (userArray.count >= 1){
                     [trackLocDict setValue:[userArray[0] valueForKey:kKeyAllowTrackNotify] forKey:kKeyTrackNotify];
                    }else{
                        [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
                    }
                    
                   // [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
                    [trackLocationArray addObject:trackLocDict];
                    //continue;
                }else {
                    NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
                    [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
                    [trackLocDict setValue:@"2" forKey:kKeyTrackNotify_On];
                    [trackLocDict setValue:@"0" forKey:kKeyTrackNotify];
                    [trackLocDict setValue:@"0" forKey:kIslocationTrackOn];
                    if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
                        if (userArray.count >= 1){
                         [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                        }else{
                           [trackLocDict setValue:@"0" forKey:kKeyId];
                        }
                        //[trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                        [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
                    } else {
                        [trackLocDict setValue:@"" forKey:kKeyId];
                        [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
                    }
                    
                    
                    
                    [trackLocationArray addObject:trackLocDict];
                }
            
          //  }
            
//            if ([arriveLocationsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]]) {
//                NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
//                if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
//                    [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
//                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
//                } else {
//                    [trackLocDict setValue:@"" forKey:kKeyId];
//                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
//                }
//                [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
//                [trackLocDict setValue:notify_on forKey:kKeyTrackNotify_On];
//                [trackLocDict setValue:@"1" forKey:kIslocationTrackOn];
//                [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
//                [trackLocationArray addObject:trackLocDict];
//                // continue;
//            } else if ([leaveLocationsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]]) {
//                NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
//                if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
//                    [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
//                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
//                } else {
//                    [trackLocDict setValue:@"" forKey:kKeyId];
//                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
//                }
//                [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
//                [trackLocDict setValue:notify_on forKey:kKeyTrackNotify_On];
//                [trackLocDict setValue:@"1" forKey:kIslocationTrackOn];
//                [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
//                [trackLocationArray addObject:trackLocDict];
//                //continue;
//            } else if ([arriveLocationsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]] && [leaveLocationsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]]) {
//                NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
//                if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
//                    [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
//                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
//                } else {
//                    [trackLocDict setValue:@"" forKey:kKeyId];
//                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
//                }
//                [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
//                [trackLocDict setValue:@"3" forKey:kKeyTrackNotify_On];
//                [trackLocDict setValue:@"1" forKey:kIslocationTrackOn];
//                [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
//                [trackLocationArray addObject:trackLocDict];
//                //continue;
//            }
//            else {
//                NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
//                [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
//                [trackLocDict setValue:notify_on forKey:kKeyTrackNotify_On];
//                [trackLocDict setValue:@"0" forKey:kKeyTrackNotify];
//                [trackLocDict setValue:@"0" forKey:kIslocationTrackOn];
//                [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
//                [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
//                [trackLocationArray addObject:trackLocDict];
//            }
        }
        
        
        for (int i = 0; i < userLocationsArr.count; i++) {
            NSString *filter = @"%K CONTAINS[cd] %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyTrackLocId, [userLocationsArr[i] valueForKey:kKeyId]];

            NSArray *userArray1 = [trackingLocArray filteredArrayUsingPredicate:predicate];
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notify_on == %@" , @"1"];
            NSArray *userArray = [userArray1 filteredArrayUsingPredicate:predicate1];
                      

            
            if ([arriveLocationsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]]) {
                NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
                if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
                    if (userArray.count >= 1){
                     [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                    }else{
                       [trackLocDict setValue:@"0" forKey:kKeyId];
                    }
//                    [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
                } else {
                    [trackLocDict setValue:@"" forKey:kKeyId];
                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
                }
                [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
                [trackLocDict setValue:@"1" forKey:kKeyTrackNotify_On];
                if (userArray.count >= 1){
                    [trackLocDict setValue:[userArray[0] valueForKey:kKeyAllowTrackNotify] forKey:kKeyTrackNotify];
                }else{
                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
                }
                
                
                [trackLocDict setValue:@"1" forKey:kIslocationTrackOn];
                //[trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackNotify] forKey:kKeyTrackNotify];
                [trackLocationArray addObject:trackLocDict];
                // continue;
            }else {
                NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
                [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
                [trackLocDict setValue:@"1" forKey:kKeyTrackNotify_On];
                [trackLocDict setValue:@"0" forKey:kKeyTrackNotify];
                [trackLocDict setValue:@"0" forKey:kIslocationTrackOn];
                if ([[userLocationsArr[i] valueForKey:kkeyExisting] isEqualToString:@"1"]) {
                    if (userArray.count >= 1){
                     [trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                    }else{
                       [trackLocDict setValue:@"0" forKey:kKeyId];
                    }
                    //[trackLocDict setValue:[userArray[0] valueForKey:kKeyId] forKey:kKeyId];
                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
                } else {
                    [trackLocDict setValue:@"" forKey:kKeyId];
                    [trackLocDict setValue:[userLocationsArr[i] valueForKey:kKeyId] forKey:kKeyTrackLocId];
                }
                [trackLocationArray addObject:trackLocDict];
            }
           
        }
        
        
        
        
        
        
        
        
        
    } else {
        NSMutableDictionary *trackLocDict = [[NSMutableDictionary alloc] init];
        [trackLocDict setValue:@"0" forKey:kKeyTrackLocId];
        [trackLocDict setValue:[userDict valueForKey:kKeyId] forKey:kKeyUserID];
        [trackLocDict setValue:@"0" forKey:kKeyTrackNotify_On];
        [trackLocDict setValue:@"0" forKey:kKeyTrackNotify];
        [trackLocDict setValue:@"0" forKey:kIslocationTrackOn];
        [trackLocationArray addObject:trackLocDict];
    }
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
    [postDict setValue:trackLocationArray forKey:@"tracking"];

    NSData *json = [NSJSONSerialization dataWithJSONObject:trackLocationArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSLog(@"%@", [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding]);

    NSMutableDictionary *trackDict = [@{kKeyTracking: jsonString} mutableCopy];
    NSLog(@"%@", trackDict);
    //Add list of address for tracking locations
    [server makeAsychronousRequest:kkeyClassGetImTrackingList inParams:trackDict isIndicatorRequired:YES inMethodType:kPOST];
}

// ----------------------------------------------------------------------------------------------------------
// chechBtnAction:
- (void)checkBtnAction:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SRNotifyMeTableCell *cell = (SRNotifyMeTableCell *) [self.notifyTable cellForRowAtIndexPath:indexPath];
    NSInteger row = indexPath.row - 3;
    [self.notifyTable cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    if ([[cell.checkBtn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"checked.png"]]) {
        // Button has a background image named 'uncheck.png'
        if ([notify_on isEqualToString:@"1"]) {
            [arriveLocationsArr removeObject:[userLocationsArr[row] valueForKey:kKeyId]];
            [cell.checkBtn setImage:[UIImage imageNamed:@"unchecked.png"]
                           forState:UIControlStateNormal];
        } else if ([notify_on isEqualToString:@"2"]) {
            [leaveLocationsArr removeObject:[userLocationsArr[row] valueForKey:kKeyId]];
            [cell.checkBtn setImage:[UIImage imageNamed:@"unchecked.png"]
                           forState:UIControlStateNormal];
        }
        NSDictionary *cellDataDict = userLocationsArr[row];
        [cellDataDict setValue:@"UnExpandable" forKey:@"Expandable"];
        [self.notifyTable reloadData];

        //else
        // [SRModalClass showAlert:@"Please select Arrives/Leaves notification options"];
    } else {
        // Button has not a background image named check.png
        if ([notify_on isEqualToString:@"1"]) {
            [arriveLocationsArr addObject:[userLocationsArr[row] valueForKey:kKeyId]];
            [cell.checkBtn setImage:[UIImage imageNamed:@"checked.png"]
                           forState:UIControlStateNormal];
        } else if ([notify_on isEqualToString:@"2"]) {
            [leaveLocationsArr addObject:[userLocationsArr[row] valueForKey:kKeyId]];
            [cell.checkBtn setImage:[UIImage imageNamed:@"checked.png"]
                           forState:UIControlStateNormal];
        }
        // Set option to notify on
        NSDictionary *cellDataDict = userLocationsArr[row];

        [cellDataDict setValue:@"Expandable" forKey:@"Expandable"];
        [self.notifyTable reloadData];
    }


}

// ----------------------------------------------------------------------------------------------------------
// btnNotifyOffAction:
- (void)btnNotifyOffAction:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    NSInteger row = indexPath.row - 3;
    SRNotifyMeTableCell *cell = (SRNotifyMeTableCell *) [self.notifyTable cellForRowAtIndexPath:indexPath];
    [cell.btnNotifyAlways setSelected:NO];
    [cell.btnNotifyOneTime setSelected:NO];
    if ([sender isSelected]) {

    } else {
        [sender setSelected:YES];
        NSDictionary *dataDict = userLocationsArr[row];
        
        for (int i = 0; i < trackingLocArray.count; i++){
            NSDictionary *tempTrackDataDict = trackingLocArray[i];
            NSString *trackId = [tempTrackDataDict valueForKey:kKeyTrackLocId];
            if ([[dataDict valueForKey:kKeyId] isEqualToString:trackId] && [[tempTrackDataDict valueForKey:kKeyTrackNotify_On] isEqualToString:notify_on]){
                [tempTrackDataDict setValue:@0 forKey:kKeyAllowTrackNotify];
                trackingLocArray[i] = tempTrackDataDict;
            }
        }
        
        [dataDict setValue:@"0" forKey:kKeyTrackNotify];
        [self.notifyTable beginUpdates];
        [self.notifyTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.notifyTable endUpdates];
    }


}

// ----------------------------------------------------------------------------------------------------------
// btnNotifyOneAction:
- (void)btnNotifyOneAction:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    NSInteger row = indexPath.row - 3;
    SRNotifyMeTableCell *cell = (SRNotifyMeTableCell *) [self.notifyTable cellForRowAtIndexPath:indexPath];
    [cell.btnNotifyAlways setSelected:NO];
    [cell.btnNotifyOff setSelected:NO];
    if ([sender isSelected]) {
    } else {
        [sender setSelected:YES];
        NSDictionary *dataDict = userLocationsArr[row];
        
        for (int i = 0; i < trackingLocArray.count; i++){
            NSDictionary *tempTrackDataDict = trackingLocArray[i];
            NSString *trackId = [tempTrackDataDict valueForKey:kKeyTrackLocId];
            if ([[dataDict valueForKey:kKeyId] isEqualToString:trackId] && [[tempTrackDataDict valueForKey:kKeyTrackNotify_On] isEqualToString:notify_on]){
                [tempTrackDataDict setValue:@1 forKey:kKeyAllowTrackNotify];
                trackingLocArray[i] = tempTrackDataDict;
            }
        }

        [dataDict setValue:@"1" forKey:kKeyTrackNotify];
        [self.notifyTable beginUpdates];
        [self.notifyTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.notifyTable endUpdates];
    }
}
// ----------------------------------------------------------------------------------------------------------
// btnNotifyAlwaysAction:
- (void)btnNotifyAlwaysAction:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    NSInteger row = indexPath.row - 3;
    SRNotifyMeTableCell *cell = (SRNotifyMeTableCell *) [self.notifyTable cellForRowAtIndexPath:indexPath];
    [cell.btnNotifyOff setSelected:NO];
    [cell.btnNotifyOneTime setSelected:NO];
    if ([sender isSelected]) {
    } else {
        [sender setSelected:YES];
        NSDictionary *dataDict = userLocationsArr[row];
        
        for (int i = 0; i < trackingLocArray.count; i++){
            NSDictionary *tempTrackDataDict = trackingLocArray[i];
            NSString *trackId = [tempTrackDataDict valueForKey:kKeyTrackLocId];
            if ([[dataDict valueForKey:kKeyId] isEqualToString:trackId] && [[tempTrackDataDict valueForKey:kKeyTrackNotify_On] isEqualToString:notify_on]){
                [tempTrackDataDict setValue:@2 forKey:kKeyAllowTrackNotify];
                trackingLocArray[i] = tempTrackDataDict;
            }
        }
        
        [dataDict setValue:@"2" forKey:kKeyTrackNotify];
        [self.notifyTable beginUpdates];
        [self.notifyTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.notifyTable endUpdates];
    }
}


// ----------------------------------------------------------------------------------------------------------
// showNotifyMeMap:
- (void)showNotifyMeMap:(id)sender {
    SRNotifyMeMapViewController *notifyMap = [[SRNotifyMeMapViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
    notifyMap.trackUserDict = userDict;
    [self.navigationController pushViewController:notifyMap animated:YES];
}

#pragma mark - TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return userLocationsArr.count + 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRNotifyMeTableCell" owner:self options:nil];

    static NSString *CellIdentifier = @"notifyMeCell";
    if (indexPath.row <= 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.font = [UIFont fontWithName:kFontHelveticaRegular size:14];
        cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
        cell.textLabel.textColor = [UIColor whiteColor];

        if (indexPath.row > 0) {
            cell.textLabel.text = menuArray[indexPath.row];
            cell.textLabel.textColor = [UIColor orangeColor];
            cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.85];
            cell.tintColor = [UIColor orangeColor];

            if ([notify_on isEqualToString:@"1"]) {
                if (indexPath.row == 2) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                if (indexPath.row == 1) {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            } else if ([notify_on isEqualToString:@"2"]) {
                if (indexPath.row == 1) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                if (indexPath.row == 2) {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }

            }else if ([notify_on isEqualToString:@"3"]) {
                if (indexPath.row == 1) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                if (indexPath.row == 2) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
        } else
            cell.textLabel.text = [[menuArray[indexPath.row] stringByAppendingString:[userDict valueForKey:kKeyFirstName]] stringByAppendingString:@":"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        NSInteger rowNumber = 0;
        if (userLocationsArr.count > 0) {
            rowNumber = indexPath.row - 3;
        }
        NSDictionary *dict = userLocationsArr[rowNumber];
        SRNotifyMeTableCell *customCell = (SRNotifyMeTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if ([[dict valueForKey:@"Expandable"] isEqualToString:@"Expandable"]) {
            if ([notify_on isEqualToString:@"1"]) {
                if ([arriveLocationsArr containsObject:[userLocationsArr[rowNumber] valueForKey:kKeyId]]) {
                    if (customCell == nil) {
                        customCell = (SRNotifyMeTableCell *) nibObjects[1];
                    }
                } else {
                    if (customCell == nil) {
                        customCell = (SRNotifyMeTableCell *) nibObjects[0];
                    }
                }
            } else if ([notify_on isEqualToString:@"2"]) {
                if ([leaveLocationsArr containsObject:[userLocationsArr[rowNumber] valueForKey:kKeyId]]) {
                    customCell = (SRNotifyMeTableCell *) nibObjects[1];
                } else {
                    customCell = (SRNotifyMeTableCell *) nibObjects[0];
                }
            } else {
                customCell = (SRNotifyMeTableCell *) nibObjects[0];
            }
        } else
            customCell = (SRNotifyMeTableCell *) nibObjects[0];


        customCell.btnNotifyOff.tag = indexPath.row;
        [customCell.btnNotifyOff addTarget:self action:@selector(btnNotifyOffAction:) forControlEvents:UIControlEventTouchUpInside];

        customCell.btnNotifyOneTime.tag = indexPath.row;
        [customCell.btnNotifyOneTime addTarget:self action:@selector(btnNotifyOneAction:) forControlEvents:UIControlEventTouchUpInside];

        customCell.btnNotifyAlways.tag = indexPath.row;
        [customCell.btnNotifyAlways addTarget:self action:@selector(btnNotifyAlwaysAction:) forControlEvents:UIControlEventTouchUpInside];

        [customCell.checkBtn addTarget:self action:@selector(checkBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        customCell.checkBtn.tag = indexPath.row;
        
        NSString *filter = @"%K CONTAINS[cd] %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyTrackLocId, [dict valueForKey:kKeyId]];
        NSArray *userArray1 = [trackingLocArray filteredArrayUsingPredicate:predicate];
                  
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"notify_on == %@" , notify_on];
        NSArray *userArray = [userArray1 filteredArrayUsingPredicate:predicate1];
        
        

        
        
        if ([userArray count] > 0){
        if ([userArray[0] valueForKey:kKeyAllowTrackNotify] != nil) {
            if ([[userArray[0] valueForKey:kKeyAllowTrackNotify] isEqualToNumber:@0]) {
                [customCell.btnNotifyOff setSelected:YES];
                [customCell.btnNotifyOneTime setSelected:NO];
                [customCell.btnNotifyAlways setSelected:NO];
            } else if ([[userArray[0] valueForKey:kKeyAllowTrackNotify]isEqualToNumber:@1]) {
                [customCell.btnNotifyOneTime setSelected:YES];
                [customCell.btnNotifyOff setSelected:NO];
                [customCell.btnNotifyAlways setSelected:NO];
            } else if ([[userArray[0] valueForKey:kKeyAllowTrackNotify] isEqualToNumber:@2]) {
                [customCell.btnNotifyAlways setSelected:YES];
                [customCell.btnNotifyOneTime setSelected:NO];
                [customCell.btnNotifyOff setSelected:NO];
            } else {
                [customCell.btnNotifyAlways setSelected:NO];
                [customCell.btnNotifyOneTime setSelected:NO];
                [customCell.btnNotifyOff setSelected:YES];
            }
        }
        }else{
            if ([dict valueForKey:kKeyTrackNotify] != nil) {
                if ([[dict valueForKey:kKeyTrackNotify] isEqualToString:@"0"]) {
                    [customCell.btnNotifyOff setSelected:YES];
                    [customCell.btnNotifyOneTime setSelected:NO];
                    [customCell.btnNotifyAlways setSelected:NO];
                } else if ([[dict valueForKey:kKeyTrackNotify] isEqualToString:@"1"]) {
                    [customCell.btnNotifyOneTime setSelected:YES];
                    [customCell.btnNotifyOff setSelected:NO];
                    [customCell.btnNotifyAlways setSelected:NO];
                } else if ([[dict valueForKey:kKeyTrackNotify] isEqualToString:@"2"]) {
                    [customCell.btnNotifyAlways setSelected:YES];
                    [customCell.btnNotifyOneTime setSelected:NO];
                    [customCell.btnNotifyOff setSelected:NO];
                } else {
                    [customCell.btnNotifyAlways setSelected:NO];
                    [customCell.btnNotifyOneTime setSelected:NO];
                    [customCell.btnNotifyOff setSelected:YES];
                }
            }
        }

        if (userLocationsArr.count > 0) {
            NSInteger row = indexPath.row - 3;
            customCell.placeAddr.text = [userLocationsArr[row] valueForKey:kKeyAddress];
            customCell.userName.text = [userLocationsArr[row] valueForKey:kKeyName];
            
            if ([notify_on isEqualToString:@"1"]) {
                if ([arriveLocationsArr containsObject:[userLocationsArr[row] valueForKey:kKeyId]]) {
                    [customCell.checkBtn setImage:[UIImage imageNamed:@"checked.png"]
                                         forState:UIControlStateNormal];
                } else {
                    [customCell.checkBtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                }
            } else if ([notify_on isEqualToString:@"2"]) {
                if ([leaveLocationsArr containsObject:[userLocationsArr[row] valueForKey:kKeyId]]) {
                    [customCell.checkBtn setImage:[UIImage imageNamed:@"checked.png"]
                                         forState:UIControlStateNormal];
                } else {
                    [customCell.checkBtn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
                }
            }
        }
        customCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return customCell;
    }


    if (indexPath.row == 1) {
        self.notifyTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    } else
        self.notifyTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    return [UITableViewCell new];
}


#pragma mark - TableView Delegate Methods

// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row <= 2) {
        return 40;
    } else {
        NSInteger rowNumber = 0;
        if (userLocationsArr.count > 0) {
            rowNumber = indexPath.row - 3;
        }
        NSDictionary *dict = userLocationsArr[rowNumber];
        if ([[dict valueForKey:@"Expandable"] isEqualToString:@"Expandable"]) {
            if ([notify_on isEqualToString:@"1"]) {
                if ([arriveLocationsArr containsObject:[userLocationsArr[rowNumber] valueForKey:kKeyId]]) {
                    return 80;
                } else {
                    return 55;
                }
            } else if ([notify_on isEqualToString:@"2"]) {
                if ([leaveLocationsArr containsObject:[userLocationsArr[rowNumber] valueForKey:kKeyId]]) {
                    return 80;
                } else {
                    return 55;
                }
            }
        } else
            return 55;
    }

    return 55;
}

   //Jonish work
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row <= 2) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        [self->userLocationsArr removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
}


-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Neha
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"           "handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
    
          [self showAlertWithTitle:@"Serendipity" message:@"If you delete the address of the user you are tracking, you have to ask again for their permission to track. Are you sure you want to delete?" buttonTitle:@"Ok" cancelButtonTitle:@"Cancel" withCompletionBlok:^(BOOL isCancelAction) {
             
              if (isCancelAction){
                  return;
              }else{
                  NSString *strUrl = [NSString stringWithFormat:@"%@%@", kkeyClassDeleteTrackingLocation, userLocationsArr[indexPath.row - 3][@"id"]];
                            [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
                            [userLocationsArr removeObjectAtIndex:indexPath.row - 3];
                            [tableView reloadData];
              }
          
        }];
    }];
//Neha
    UITableViewCell *commentCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    CGFloat height = commentCell.frame.size.height;
    UIImage *backgroundImage = [self deleteImageForHeight:height];
    deleteAction.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    return @[deleteAction];
}

- (UIImage*)deleteImageForHeight:(CGFloat)height{

    CGRect frame = CGRectMake(0, 0, 62, height);

    UIGraphicsBeginImageContextWithOptions(CGSizeMake(62, height), NO, [UIScreen mainScreen].scale);

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    CGContextFillRect(context, frame);

    UIImage *image = [UIImage imageNamed:@"trash-white"];

    [image drawInRect:CGRectMake((frame.size.width-10)/2, (frame.size.height - 25)/2, 25, 25)];

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return newImage;
}
//Jonish work
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row <= 2 && indexPath.row > 0) {
//        arriveLocationsArr = [[NSMutableArray alloc] init];
//        leaveLocationsArr = [[NSMutableArray alloc] init];
        

        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        if (indexPath.row == 1) {
            notify_on = @"2"; //leaves
        }
        if (indexPath.row == 2) {
            notify_on = @"1";// arrives
        }
        [self.notifyTable reloadData];
    } else if (indexPath.row >= 3) {
        if (userLocationsArr.count > 0) {
            NSInteger row = indexPath.row - 3;
            SRNotifyMeMapViewController *notifyMap = [[SRNotifyMeMapViewController alloc] initWithNibName:nil bundle:nil profileData:userLocationsArr[row] server:server];
            notifyMap.trackUserDict = userDict;
            [self.navigationController pushViewController:notifyMap animated:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row <= 2 && indexPath.row > 0) {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }
}

#pragma mark - Notifications Methods
// --------------------------------------------------------------------------------
// GetTrackLocationsSucceed:

- (void)GetTrackLocationsSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    NSMutableArray *trackLocIdsArr = [[NSMutableArray alloc] init];
    if ([object isKindOfClass:[NSDictionary class]]) {
        // Get list of saved tracking locations
        NSArray *array = [[inNotify object] valueForKey:@"tracking_location"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = 0", @"is_deleted"]; // jonish work
        NSArray *userArray = [array filteredArrayUsingPredicate:predicate];
        userLocationsArr = [[NSMutableArray alloc] initWithArray:userArray];
        // Get saved tracking locations which are already added for tracking
        trackingLocArray = [[NSMutableArray alloc] initWithArray:[[inNotify object] valueForKey:@"tracking"]];
        if ([trackingLocArray isKindOfClass:[NSArray class]]) {
            NSMutableArray *locationsArr = [[NSMutableArray alloc] initWithArray:trackingLocArray];

            for (NSDictionary *dict in locationsArr) {
                [trackLocIdsArr addObject:[dict valueForKey:kKeyTrackLocId]];
                notify_on = [dict valueForKey:kKeyTrackNotify_On];
                if ([[dict valueForKey:kIslocationTrackOn] isEqual:[NSNumber numberWithInt:1]]){
                    
                    if ([[dict valueForKey:kKeyTrackNotify_On] isEqualToString:@"3"]) {
                        [leaveLocationsArr addObject:[dict valueForKey:kKeyTrackLocId]];
                        [arriveLocationsArr addObject:[dict valueForKey:kKeyTrackLocId]];
                    } else if ([[dict valueForKey:kKeyTrackNotify_On] isEqualToString:@"2"]) {
                        [leaveLocationsArr addObject:[dict valueForKey:kKeyTrackLocId]];
                    } else if ([[dict valueForKey:kKeyTrackNotify_On] isEqualToString:@"1"]) {
                        [arriveLocationsArr addObject:[dict valueForKey:kKeyTrackLocId]];
                    }
                }
            }
        }
    }

    for (int i = 0; i < userLocationsArr.count; i++) {
        NSDictionary *dict = userLocationsArr[i];
        if ([trackLocIdsArr containsObject:[userLocationsArr[i] valueForKey:kKeyId]]) {
            NSString *filter = @"%K CONTAINS[cd] %@";
            NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyTrackLocId, [userLocationsArr[i] valueForKey:kKeyId]];
            NSArray *userArray = [trackingLocArray filteredArrayUsingPredicate:predicate];
            [dict setValue:[userArray[0] valueForKey:kKeyTrackLocId] forKey:kKeyTrackLocId];
            [dict setValue:@"1" forKey:kkeyExisting];
            [dict setValue:@"Expandable" forKey:@"Expandable"];
            userLocationsArr[i] = dict;
            
        }
    }

    NSLog(@"trackingLocArray::%@", trackingLocArray);
    NSLog(@"userLocationsArr::%@", userLocationsArr);
    [self.notifyTable reloadData];
}

// --------------------------------------------------------------------------------
// GetTrackLocationsFailed:

- (void)GetTrackLocationsFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// SaveTrackLocationsSucceed:
- (void)SaveTrackLocationsSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationSucceed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationFailed object:nil];
    [SRModalClass showAlert:inNotify.object];

    if (fromAmTrackingView) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;

        UIViewController *vc = self.navigationController.viewControllers[numberOfViewControllers - 3];

        [self.navigationController popToViewController:vc animated:NO];
    }

}

// --------------------------------------------------------------------------------
// SaveTrackLocationsFailed:
- (void)SaveTrackLocationsFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationSucceed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationSaveTrackLocationFailed object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
