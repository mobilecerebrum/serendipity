//
//  SRGoInvisibleViewController.m
//  Serendipity
//
//  Created by Leo on 07/12/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRGoInvisibleViewController.h"

@interface SRGoInvisibleViewController ()

@end

@implementation SRGoInvisibleViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
               inData:(NSDictionary *)inDetailDict {
    self = [super initWithNibName:@"SRGoInvisibleViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        navTitle = nibNameOrNil;
        infoDict = [[NSMutableDictionary alloc]initWithDictionary:inDetailDict];
        
        
    }
    
    // return
    return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//-------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // call super
    [super viewDidLoad];
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Title
    [SRModalClass setNavTitle:navTitle forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    // left navbar button
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    // initializing menuitemarray
    menuItemArray = [@[NSLocalizedString(@"lbl.Everyone.txt", @""), NSLocalizedString(@"lbl.MyContacts.txt", @""), NSLocalizedString(@"lbl.MyConnections.txt", @""), NSLocalizedString(@"lbl.MyFavConnections.txt", @""), NSLocalizedString(@"lbl.Nobody.txt", @""), NSLocalizedString(@"lbl.EveryoneExcept.txt", @"")] mutableCopy];
    
    subMenuItemArray = [@[NSLocalizedString(@"lbl.MyContacts.txt", @""), NSLocalizedString(@"lbl.MyConnections.txt", @""), NSLocalizedString(@"lbl.MyFavConnections.txt", @""), NSLocalizedString(@"Users Tracking Me", @"")] mutableCopy];
    
    tempArray = [@[NSLocalizedString(@"lbl.Everyone.txt", @""), NSLocalizedString(@"lbl.MyContacts.txt", @""), NSLocalizedString(@"lbl.MyConnections.txt", @""), NSLocalizedString(@"lbl.MyFavConnections.txt", @""), NSLocalizedString(@"lbl.Nobody.txt", @""), NSLocalizedString(@"lbl.EveryoneExcept.txt", @""), NSLocalizedString(@"lbl.MyContacts.txt", @""), NSLocalizedString(@"lbl.MyConnections.txt", @""), NSLocalizedString(@"lbl.MyFavConnections.txt", @""), NSLocalizedString(@"Users Tracking Me", @"")] mutableCopy];
    singleSelectedArray = [[NSMutableArray alloc]init];
    multiplSelectedArray = [[NSMutableArray alloc]init];
    finalSelectedOptionsArr  = [[NSMutableArray alloc]init];
    [finalSelectedOptionsArr removeAllObjects];
    subSelectedArray = [[NSMutableArray alloc]init];
    
    // Set footerview to uitableview
    self.tblView.tableFooterView = [[UIView alloc] init];
    
    // Save switch state for Go Invisible
    // Get settings saved in user_settings save it to array
    NSString *go_invisibleStr = [[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:@"go_invisible"];
    NSArray *go_invisibleArr = [go_invisibleStr componentsSeparatedByString:@","];
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:@"everyone_except"] isEqualToString:@"1"])
    {
        goInvisibleSwitchOn = YES;
        // Add saved setting to final array
        for (int i = 0; i < go_invisibleArr.count; i++) {
//            NSInteger numkey = [[go_invisibleArr objectAtIndex:i] integerValue];
//            numkey = numkey - 1;
            if ([go_invisibleArr containsObject:[NSString stringWithFormat:@"%d",2]])
            {
                [subSelectedArray addObject:[NSString stringWithFormat:@"%d",7]];
            }
            if ([go_invisibleArr containsObject:[NSString stringWithFormat:@"%d",3]])
            {
                [subSelectedArray addObject:[NSString stringWithFormat:@"%d",8]];
            }
            if ([go_invisibleArr containsObject:[NSString stringWithFormat:@"%d",4]])
            {
                [subSelectedArray addObject:[NSString stringWithFormat:@"%d",9]];
            }
            if ([go_invisibleArr containsObject:[NSString stringWithFormat:@"%d",7]])
            {
                [subSelectedArray addObject:[NSString stringWithFormat:@"%d",11]];
            }
//            if ([go_invisibleArr containsObject:[NSString stringWithFormat:@"%d",7]])
//            {
//                [subSelectedArray addObject:[NSString stringWithFormat:@"%d",11]];
//            }
            // add subselected options of everyone except to final array
            for (int i = 0; i < subSelectedArray.count; i++) {
                if (![finalSelectedOptionsArr containsObject:subSelectedArray[i]]) {
                    [finalSelectedOptionsArr addObject:subSelectedArray[i]];
                }
            }
            
            if (![finalSelectedOptionsArr containsObject:@"6"])
            {
                [finalSelectedOptionsArr addObject:@"6"];
            }
        }
    }
    else
    {
        goInvisibleSwitchOn = NO;
        // Add saved setting to final array
        for (int i = 0; i < go_invisibleArr.count; i++)
        {
            NSInteger numkey = [go_invisibleArr[i] integerValue];
            numkey = numkey - 1;
            if (numkey > 0 && numkey < 5)
            {
                // If option selected is connection,contacts or favourites then add it in selected array
                [multiplSelectedArray addObject:[NSString stringWithFormat:@"%ld",numkey]];
            }
            [finalSelectedOptionsArr addObject:[NSString stringWithFormat:@"%ld",numkey]];
        }
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnBack:
- (void)actionOnBack:(id)sender
{
    NSLog(@"FINAL ARRAY:%@",finalSelectedOptionsArr);
    if ([_delegate respondsToSelector:@selector(dataFromSRGoInvisibleViewController:)])
    {
         NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
         NSString *key = @"";
         for (int i = 0; i < finalSelectedOptionsArr.count; i++)
         {
             //As indexpath starts from 1 is added to every indexpath.row
             NSInteger numkey = [finalSelectedOptionsArr[(NSUInteger) i] integerValue];
             numkey = numkey + 1;
             key = [[key stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)numkey]]stringByAppendingString:@","];
         }
         if ([key length] > 0)
         {
             key = [key substringToIndex:[key length] - 1];
         }
         [dict setValue:key forKey:kKeyValue];
        // Check if everyone except is selected and no subcategory is selected
        if ([finalSelectedOptionsArr containsObject:@"6"]) {
            if (subSelectedArray.count == 0) {
                [SRModalClass showAlert:@"When going Invisible and selecting ""Everyone except"", please be sure select as many of the four options to which you want to go Invisible."];
            }
            else
            {
                [_delegate dataFromSRGoInvisibleViewController:dict];
                [self.navigationController popViewControllerAnimated:NO];
            }
        }
        else
        {
            [_delegate dataFromSRGoInvisibleViewController:dict];
            [self.navigationController popViewControllerAnimated:NO];
        }
     }
}


#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (goInvisibleSwitchOn) {
         // If everyone Except switch is on
        return tempArray.count;
    }
    else
        return [menuItemArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
    if ([nibObjects count] > 0) {
        SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *) nibObjects[0];
        cell = customCell;
    }

    // Manage subviews hide and unhide
    ((SRHomeMenuCustomCell *)cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemTwitterBTN.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuIconImage.hidden = YES;
    ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = YES;
    
    // Swich
    UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-60, 4, 60, 25)];
    objSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
    objSwitch.tintColor = [UIColor whiteColor];
    [objSwitch setOnTintColor:[UIColor orangeColor]];
    objSwitch.backgroundColor = [UIColor lightGrayColor];
    objSwitch.layer.cornerRadius = 16.0f;
    if (indexPath.row==9)
    {
        objSwitch.tag = indexPath.row+2;
    }
    else if (indexPath.row>=4)
        objSwitch.tag = indexPath.row+1;
    else
        objSwitch.tag = indexPath.row;
    NSLog(@"%ld",(long)indexPath.row);
    if (indexPath.row>=4)
    {
        if ([finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%ld",indexPath.row+1]] && goInvisibleSwitchOn==NO)
        {
            [objSwitch setOn:YES];
        }
        else
        {
            // If Contacts,Connections,Family or favourites switch is selected
            if ([multiplSelectedArray containsObject:[NSString stringWithFormat:@"%ld",indexPath.row]]) {
                [objSwitch setOn:YES];
            }
            else
                [objSwitch setOn:NO];
        }
    }
    else
    {
         if ([finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%ld",indexPath.row]] && goInvisibleSwitchOn==NO)
        {
            // If Everyone,Nobody or Everyone except switch is selected
            [objSwitch setOn:YES];
        }
        else
        {
            // If Contacts,Connections,Family or favourites switch is selected
            if ([multiplSelectedArray containsObject:[NSString stringWithFormat:@"%ld",indexPath.row]]) {
                [objSwitch setOn:YES];
            }
            else
                [objSwitch setOn:NO];
        }
    }
    [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [((SRHomeMenuCustomCell *)cell).contentView addSubview:objSwitch];
    
    // Label
    
    ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = CGRectMake(20, 10, 300, 15);
    if (goInvisibleSwitchOn)
    {
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.text = tempArray[indexPath.row];
        if (indexPath.row >= 6) {
            CGRect frame = ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame;
            frame.origin.x = 40;
            ((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = frame;
            //If subMenus from Everyone except is selected
            if (indexPath.row == 9 && [subSelectedArray containsObject:[NSString stringWithFormat:@"%ld",indexPath.row+2]])
            {
                [objSwitch setOn:YES];
            }
            else if ([subSelectedArray containsObject:[NSString stringWithFormat:@"%ld",indexPath.row+1]])
            {
                [objSwitch setOn:YES];
            }
        }
        if (5 == indexPath.row)
        {
            [objSwitch setOn:YES];
        }
    }
    else
        ((SRHomeMenuCustomCell *)cell).MenuItemLabel.text = menuItemArray[indexPath.row];
    ((SRHomeMenuCustomCell *)cell).backgroundColor = [UIColor clearColor];
    
    // Return
    return cell;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


#pragma mark
#pragma mark UISwitch value change action
#pragma mark

- (void)changeSwitch:(UISwitch *)sender
{
    // Go invisible switch
    if (sender.tag == 0 ||sender.tag == 5 || sender.tag == 6)
    {
        // If Everyone,Nobody or Everyone except switch is selected
        if (sender.tag == 6) {
            goInvisibleSwitchOn = YES;
        }
        else
            goInvisibleSwitchOn = NO;
        if (![sender isOn])
        {
            goInvisibleSwitchOn = NO;
            [finalSelectedOptionsArr removeAllObjects];
            NSString *go_invisibleStr = [[server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:@"go_invisible"];
            NSArray *go_invisibleArr = [go_invisibleStr componentsSeparatedByString:@","];
            // Add saved setting to final array
            for (int i = 0; i < go_invisibleArr.count; i++) {
                NSInteger numkey = [go_invisibleArr[(NSUInteger) i] integerValue];
                numkey = numkey - 1;
                [finalSelectedOptionsArr addObject:[NSString stringWithFormat:@"%ld",numkey]];
            }
        }
        else
        {
            [finalSelectedOptionsArr removeAllObjects];
            [finalSelectedOptionsArr addObject:[NSString stringWithFormat:@"%ld",sender.tag]];
        }
        
        [self.tblView reloadData];
        [multiplSelectedArray removeAllObjects];
    }
    else if (sender.tag >0 && sender.tag < 5 )
    {
        goInvisibleSwitchOn = NO;
        // Remove if everyone,everyone except and nobody is selected
        for(int i = 0 ; i < subSelectedArray.count; i++)
        {
            if ([finalSelectedOptionsArr containsObject:subSelectedArray[i]])
            {
                [finalSelectedOptionsArr removeObject:subSelectedArray[i]];
            }
        }
        if ([finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%d",5]])
        {
            [finalSelectedOptionsArr removeObject:[NSString stringWithFormat:@"%d",5]];
        }
        if ([finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%d",6]])
        {
            [finalSelectedOptionsArr removeObject:[NSString stringWithFormat:@"%d",6]];
        }
        if ([finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%d",0]])
        {
            [finalSelectedOptionsArr removeObject:[NSString stringWithFormat:@"%d",0]];
        }

        // If Contacts,Connections,Family or favourites switch is selected
        if ([sender isOn]) {
             [multiplSelectedArray addObject:[NSString stringWithFormat:@"%ld",sender.tag]];
        }
        else{
             [multiplSelectedArray removeObject:[NSString stringWithFormat:@"%ld",sender.tag]];
        }
        [self.tblView reloadData];
    }
    else
    {
        //If subMenus from Everyone except is selected
        if ([sender isOn])
        {
            if (![subSelectedArray containsObject:[NSString stringWithFormat:@"%ld",sender.tag]])
            {
                [subSelectedArray addObject:[NSString stringWithFormat:@"%ld",sender.tag]];
                
            }
        }
        else
        {
            if ([subSelectedArray containsObject:[NSString stringWithFormat:@"%ld",sender.tag]])
            {
                if (subSelectedArray.count > 1)
                {
                    [subSelectedArray removeObject:[NSString stringWithFormat:@"%ld",sender.tag]];
                }
                else
                {
                    [sender setOn:YES];
                    [SRModalClass showAlert:@"When going Invisible and selecting ""Everyone except"", please be sure select as many of the four options to which you want to go Invisible."];
                }
            }
        }
    }

    if ([sender isOn])
    {
        if (![finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%ld",sender.tag]])
        {
            [finalSelectedOptionsArr addObject:[NSString stringWithFormat:@"%ld", sender.tag]];
        }
    }
    else
    {
        if ([finalSelectedOptionsArr containsObject:[NSString stringWithFormat:@"%ld",sender.tag]]) {
            [finalSelectedOptionsArr removeObject:[NSString stringWithFormat:@"%ld",sender.tag]];
        }
    }
}
@end
