//
//  SRGoInvisibleViewController.h
//  Serendipity
//
//  Created by Leo on 07/12/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRProfileImageView.h"

#pragma mark
#pragma mark Protocol
#pragma mark

// Custom Delegate to pass Data to SRPrivacyViewController
@protocol SRGoInvisibleViewControllerDelegate <NSObject>
- (void)dataFromSRGoInvisibleViewController:(NSDictionary *)data;
@end

@interface SRGoInvisibleViewController : ParentViewController
{
    //instance variables
    NSMutableDictionary *infoDict;
    NSMutableArray *menuItemArray,*subMenuItemArray,*tempArray,*multiplSelectedArray,*singleSelectedArray,*subSelectedArray,*finalSelectedOptionsArr;
    
    NSString *navTitle;
    //Server
    SRServerConnection *server;
    BOOL goInvisibleSwitchOn;
}

// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *bckProfileImg;
@property (strong, nonatomic) IBOutlet UIImageView *bckClrImg;
@property (strong, nonatomic) IBOutlet UITableView *tblView;

// Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
               inData:(NSDictionary *)inDetailDict;

@end
