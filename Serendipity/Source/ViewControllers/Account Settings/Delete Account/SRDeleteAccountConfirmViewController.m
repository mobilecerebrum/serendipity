//
//  SRDeleteAccountConfirmViewController.m
//  Serendipity
//
//  Created by Leo on 18/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRDeleteAccountConfirmViewController.h"
#import "SRModalClass.h"
#import "SRCountryPickerViewController.h"
#import "DBManager.h"

@interface SRDeleteAccountConfirmViewController () <CountryPickerDelegate>

@end

@implementation SRDeleteAccountConfirmViewController



#pragma mark
#pragma mark - Init & Dealloc Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRDeleteAccountConfirmViewController" bundle:nil];
    
    if (self) {
        // Initialization
        server = inServer;
        
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(deleteAccountSucceed:)
                              name:kKeyNotificationDeleteAccountSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(deleteAccountFailed:)
                              name:kKeyNotificationDeleteAccountFailed
                            object:nil];
    }
    
    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.delete_acc.text", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:NSLocalizedString(@"cancel.button.title.text", @"") barImage:nil forViewNavCon:self offset:-5];
    [leftButton addTarget:self action:@selector(actionOnCancel) forControlEvents:UIControlEventTouchUpInside];
    
    [self.lblDeleteMsg setText:NSLocalizedString(@"txt.info.delete_acc.text", @"")];
    
    // Set search icon to textfield
    UIView *container =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    container.backgroundColor=[UIColor clearColor];
    
    self.lblPhoneCode =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 30)];
    self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaRegular size:15.0];
    self.lblPhoneCode.text = @" +1";
    NSString *countryCode = [server.loggedInUserInfo valueForKey:kKeyCountryMasterId];
    for (int i = 0; i < server.countryArr.count; i++) {
        if ([countryCode isEqualToString:[server.countryArr[i] valueForKey:kKeyId]]) {
            
            if(server.countryArr && server.countryArr.count && i < server.countryArr.count && [server.countryArr[i] valueForKey:kKeyPhoneCode])
            {
                self.lblPhoneCode.text = [@" +" stringByAppendingString:[server.countryArr[i] valueForKey:kKeyPhoneCode]];
                
            }
            // Set current country code & name
            [self.btnCountryPicker setTitle:[server.countryArr[i] valueForKey:kkeyCountryName] forState:UIControlStateNormal];
        }
    }
    [container addSubview:self.lblPhoneCode];
    
    self.txtPhoneNo.leftView=container;
    self.txtPhoneNo.leftViewMode=UITextFieldViewModeAlways;
    
    [self.deleteBeginsView setHidden:TRUE];
    
    
    
}
// ----------------------------------------------------------------------------------------------
// actionOnCancel

- (void)actionOnCancel {
    // Pop controller
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    [self.navigationController popToViewController:self.navigationController.viewControllers[(NSUInteger) (numberOfViewControllers - 3)] animated:NO];
    
}
// -------------------------------------------------------------------------------------------------------
// actionOnBtnCountry:

- (IBAction)actionOnDeleteClick:(UIButton *)sender {
    
    NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtPhoneNo.text];

    if (mobile_Number.length >= 9) {
        [self.txtPhoneNo resignFirstResponder];
    } else {
        [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];        
        return;
    }
    
    [self.deleteBeginsView setHidden:FALSE];
    // Show the indicator
    [APP_DELEGATE showActivityIndicator];
    
    // Call Api
    NSDictionary *params = @{kkeyFeedback: self.reviewTxt, kKeyMobileNumber: [SRModalClass RemoveSpecialCharacters:self.txtPhoneNo.text]};
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, server.loggedInUserInfo[kKeyId]];
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:NO inMethodType:kDELETE];
}
// -------------------------------------------------------------------------------------------------------
// actionOnBtnCountry:

- (IBAction)actionOnCountryClick:(UIButton *)sender {
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc]initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}

#pragma mark
#pragma mark Country Picker Delegates Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// pickCountry:
- (void) pickCountry :(NSDictionary *)countryDict
{
    if ([countryDict count] != 0)
    {
        [self.btnCountryPicker setTitle:[countryDict valueForKey:kkeyCountryName] forState:UIControlStateNormal];
        if(countryDict && [countryDict valueForKey:kKeyPhoneCode])
        {
            [self.lblPhoneCode setText:[@" +" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];
            
        }
    }
}

// ---------------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}
// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    self.txtPhoneNo.text = [SRModalClass formatPhoneNumber:self.txtPhoneNo.text deleteLastChar:NO];
}
// --------------------------------------------------------------------------------
// textFieldShouldBeginEditing:
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (SCREEN_HEIGHT <= 568) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    }
    
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldShouldEndEditing:
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (SCREEN_HEIGHT <= 568) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    [self.view endEditing:YES];
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isValid = YES;
    NSString *errorMsg = nil;
    NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];
    
    if ([textStrToCheck length] > 17) {
        errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
    }
    else if (errorMsg == nil) {
        BOOL flag = [SRModalClass numberValidation:string];
        
        textField.text = [SRModalClass formatPhoneNumber:self.txtPhoneNo.text deleteLastChar:NO];
        
        if (!flag) {
            errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
        }
    }
    // Set flag and show alert
    if (errorMsg) {
        isValid = NO;
        [SRModalClass showAlert:errorMsg];
    }
    
    // Return
    return isValid;
}
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,SCREEN_WIDTH,SCREEN_HEIGHT)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)];
}

// ----------------------------------------------------------------------------------------------------------------------
// deleteAccountSucceed:
- (void)deleteAccountSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    if ([fileManager fileExistsAtPath:loginDataPath])
        [fileManager removeItemAtPath:loginDataPath error:&error];
    NSString * chatDBPath= paths[0];
    chatDBPath=[chatDBPath stringByAppendingPathComponent:@"chat_app.db"];
    if ([fileManager fileExistsAtPath:chatDBPath])
        [fileManager removeItemAtPath:chatDBPath error:&error];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:server.loggedInUserInfo[kKeyId] forKey:kKeyUserID];
    [server makeAsychronousRequest:kKeyClassUpdateUserActiveStatus inParams:dict isIndicatorRequired:NO inMethodType:kPOST];
    // Login present
    [DBManager clearInstance];
}

// --------------------------------------------------------------------------------
// deleteAccountFailed:

- (void)deleteAccountFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:inNotify.object];
    [self.deleteBeginsView setHidden:TRUE];
    // Show the indicator
    [APP_DELEGATE hideActivityIndicator];
}


#pragma mark
#pragma mark Notification Method
#pragma mark

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
