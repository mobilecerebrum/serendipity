//
//  SRCountryPickerViewController.m
//  Serendipity
//
//  Created by Leo on 18/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRCountryPickerViewController.h"
#import "SRModalClass.h"

@interface SRCountryPickerViewController ()
{
    NSArray *countryListArr,*searchData;
}
@end

@implementation SRCountryPickerViewController
@synthesize delegate;
#pragma mark
#pragma mark - Init & Dealloc Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRCountryPickerViewController" bundle:nil];
    
    if (self) {
        // Initialization
        server = inServer;
    
    }
    
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
// -----------------------------------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    countryListArr = [[NSArray alloc]initWithArray:server.countryArr];
    searchData = [[NSArray alloc]init];
    searchData = countryListArr;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"title.country_picker.txt", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnCancel) forControlEvents:UIControlEventTouchUpInside];

    //Text field search left view
    UIView *container =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    container.backgroundColor=[UIColor clearColor];
    
    UIImageView *searchIcon =[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 20, 20)];
    searchIcon.image =[UIImage imageNamed:@"ic_search"];
    [container addSubview:searchIcon];
    
    self.txfSearchField.leftView=container;
    self.txfSearchField.leftViewMode=UITextFieldViewModeAlways;
    self.txfSearchField.attributedPlaceholder = [[NSAttributedString alloc]initWithString:NSLocalizedString(@"txt.placeholder.search", "") attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}
// -----------------------------------------------------------------------------------------------
// viewWillAppear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
}

// ----------------------------------------------------------------------------------------------
// actionOnCancel

- (void)actionOnCancel {
     [self.delegate pickCountry:nil];
    // Pop controller
    [self.navigationController popViewControllerAnimated:NO];
    
}


#pragma mark
#pragma mark Tableview delegate methods
#pragma mark
// ---------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// ---------------------------------------------------------------------------------------
// numberOfRowsInSection

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        return [searchData count];
}

// ---------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CountryCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    
    cell.textLabel.text = [[searchData objectAtIndex:indexPath.row]valueForKey:kkeyCountryName];
    
    if (searchData && indexPath.row<searchData.count && [searchData objectAtIndex:indexPath.row] && [[searchData objectAtIndex:indexPath.row] valueForKey:kKeyPhoneCode]) {
        
        cell.detailTextLabel.text = [@"+" stringByAppendingString:[[searchData objectAtIndex:indexPath.row]valueForKey:kKeyPhoneCode]];

    }

    cell.textLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:13.0];
    cell.detailTextLabel.font=  [UIFont fontWithName:kFontHelveticaMedium size:13.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
// ---------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}
// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate pickCountry:[searchData objectAtIndex:indexPath.row]];
    
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark
#pragma mark Search Methods
#pragma mark
// --------------------------------------------------------------------------------
// searchEventsForSearchText:

- (void)searchEventForSearchText:(NSString *)searchText {
    if ([searchText length] > 0) {
        NSPredicate *resultPredicate = [NSPredicate
        predicateWithFormat:@"nicename BEGINSWITH[c] %@",
         searchText];

        searchData = [countryListArr filteredArrayUsingPredicate:resultPredicate];
        [self.CountryTable reloadData];
    }
    else
    {
        searchData = countryListArr;
        [self.CountryTable reloadData];
    }
}

#pragma mark
#pragma mark TextField Delegate method
#pragma mark

// --------------------------------------------------------------------------------
//  textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    // Search
    [self searchEventForSearchText:textField.text];
    
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self searchEventForSearchText:str];
    
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldShouldBeginEditing:

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Add overlay view
    self.CountryTable.scrollEnabled = NO;
    
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Add overlay view
    self.CountryTable.scrollEnabled = NO;
}

// --------------------------------------------------------------------------------
// textFieldShouldEndEditing:

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // Remove  overlay view from view
    self.CountryTable.scrollEnabled = YES;
    
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidEndEditing

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // Search
    [self searchEventForSearchText:nil];
    
    [textField resignFirstResponder];
    
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txfSearchField resignFirstResponder];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
