//
//  SRCountryPickerViewController.h
//  Serendipity
//
//  Created by Leo on 18/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRServerConnection.h"

@protocol CountryPickerDelegate <NSObject>
@required
- (void) pickCountry :(NSDictionary *)countryDict;
@end

@interface SRCountryPickerViewController : ParentViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    // Server
    SRServerConnection *server;
    
    // Delegate
    id <CountryPickerDelegate> delegate;
    
}

// Property
@property (weak, nonatomic) IBOutlet UITextField *txfSearchField;
@property (nonatomic, strong) id <CountryPickerDelegate> delegate;
@property (strong,nonatomic) IBOutlet UITableView *CountryTable;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
