//
//  SRDeleteAccountViewController.m
//  Serendipity
//
//  Created by Leo on 18/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRDeleteAccountViewController.h"
#import "SRModalClass.h"
#import "SRDeleteAccountConfirmViewController.h"

@interface SRDeleteAccountViewController ()

@end

@implementation SRDeleteAccountViewController

#pragma mark
#pragma mark - Init & Dealloc Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRDeleteAccountViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
    }

    // Return
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.delete_acc.text", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:NSLocalizedString(@"cancel.button.title.text", @"") barImage:nil forViewNavCon:self offset:-5];
    [leftButton addTarget:self action:@selector(actionOnCancel) forControlEvents:UIControlEventTouchUpInside];

    UIButton *rightButton = [SRModalClass setRightNavBarItem:NSLocalizedString(@"nav.bar.next", @"") barImage:nil forViewNavCon:self offset:-5];
    [rightButton addTarget:self action:@selector(actionOnNext:) forControlEvents:UIControlEventTouchUpInside];

    self.lblHateMsg.text = NSLocalizedString(@"txt.alert_msg.delete_acc.text", @"");
    self.imgViewSadFace.layer.cornerRadius = self.imgViewSadFace.frame.size.width / 2.0;
    self.imgViewSadFace.layer.masksToBounds = YES;

    self.txtReview.layer.borderColor = [[UIColor blackColor] CGColor];
    self.txtReview.layer.borderWidth = 1.0;
    self.txtReview.layer.cornerRadius = 5.0;
    self.txtReview.layer.masksToBounds = YES;
}
// ----------------------------------------------------------------------------------------------
// actionOnCancel

- (void)actionOnCancel {
    // Pop controller
    [self.navigationController popViewControllerAnimated:NO];
}

// ----------------------------------------------------------------------------------------------
// actionOnNext:

- (void)actionOnNext:(id)sender {
    // save button action
    [self.view endEditing:YES];
    SRDeleteAccountConfirmViewController *confirmDeleteAcc = [[SRDeleteAccountConfirmViewController alloc] initWithNibName:@"SRDeleteAccountConfirmViewController" bundle:nil server:server];
    confirmDeleteAcc.reviewTxt = self.txtReview.text;
    [self.navigationController pushViewController:confirmDeleteAcc animated:YES];

}

// --------------------------------------------------------------------------------
// textViewDidBeginEditing:

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (textView == self.txtReview) {
        self.lblPlaceholder.hidden = YES;
    }
}

// --------------------------------------------------------------------------------
// textViewDidEndEditing:

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    if (![textView hasText]) {
        self.lblPlaceholder.hidden = NO;
    }
}
// --------------------------------------------------------------------------------
// textViewDidChange:

- (void)textViewDidChange:(UITextView *)textView {
    if (![textView hasText]) {
        self.lblPlaceholder.hidden = NO;
    } else {
        self.lblPlaceholder.hidden = YES;
    }
}

// ---------------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        if (![self.txtReview hasText]) {
            self.lblPlaceholder.hidden = NO;
        } else {
            self.lblPlaceholder.hidden = YES;
        }

        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
