//
//  SRDeleteAccountConfirmViewController.h
//  Serendipity
//
//  Created by Leo on 18/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRServerConnection.h"

@interface SRDeleteAccountConfirmViewController : ParentViewController<UITextFieldDelegate>
{
    // Server
    SRServerConnection *server;
}

// Properties
@property (strong, nonatomic) NSString *reviewTxt;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBG;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewWarning;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCountryBtnBG;
@property (strong, nonatomic) IBOutlet UILabel *lblDeleteMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (strong, nonatomic) UILabel *lblPhoneCode;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (strong, nonatomic) IBOutlet UIButton *btnCountryPicker;
@property (strong, nonatomic) IBOutlet UIView *deleteBeginsView;
#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
