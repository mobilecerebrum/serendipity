//
//  SRDeleteAccountViewController.h
//  Serendipity
//
//  Created by Leo on 18/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRServerConnection.h"

@interface SRDeleteAccountViewController : ParentViewController<UITextViewDelegate>
{
    // Server
    SRServerConnection *server;
}

// Properties
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBG;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewSadFace;
@property (strong, nonatomic) IBOutlet UILabel *lblHateMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceholder;
@property (strong, nonatomic) IBOutlet UITextView *txtReview;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
