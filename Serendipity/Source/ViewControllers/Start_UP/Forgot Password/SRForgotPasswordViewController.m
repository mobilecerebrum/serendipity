//
//  forgotPasswordViewController.m
//  Serendipity
//
//  Created by Leo on 06/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRForgotPasswordViewController.h"
#import "SRCountryPickerViewController.h"

@interface SRForgotPasswordViewController () <CountryPickerDelegate> {
    NSString *mobile_Number, *sms_reference_code;
    NSDictionary *forgotPwdDict;
}
@end

@implementation SRForgotPasswordViewController

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRForgotPasswordViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;

        // Register Notifications

        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(forgotPasswordGetCodeSucceed:)
                              name:kKeyNotificationGetForgotPasswordCodeSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(forgotPasswordGetCodeFailed:)
                              name:kKeyNotificationGetForgotPasswordCodeFailed
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(forgotPasswordSucceed:)
                              name:kKeyNotificationForgotPasswordSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(forgotPasswordFailed:)
                              name:kKeyNotificationForgotPasswordFailed
                            object:nil];
        [defaultCenter addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];

        [defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    }


    // Return
    return self;
}


- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
    [self.view addGestureRecognizer:tapView];

    // Hide fields
    self.codeView.hidden = TRUE;
    // Become first responder
    //[self.txtFldCode1 becomeFirstResponder];
    [self.txtFldCode1 setTag:1];
    [self.txtFldCode2 setTag:2];
    [self.txtFldCode3 setTag:3];
    [self.txtFldCode4 setTag:4];
    [self.txtFldCode5 setTag:5];
    [self.txtFldCode6 setTag:6];
    
    [self prefillCountryCode];
}

// -----------------------------------------------------------------------------------------------
// viewWillAppear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.forgotPwd", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnBack) forControlEvents:UIControlEventTouchUpInside];

    self.lblFirstMsg.text = NSLocalizedString(@"lbl.first.message.txt", @"");
    self.lblSecondMsg.text = NSLocalizedString(@"lbl.forgot.message.txt", @"");
    self.txtFldPhone.placeholder = NSLocalizedString(@"txt.placeholder.phone", @"");
    self.txtFldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txtfld.placeholder.pwd", @"") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldConfPwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.placeholder.confPassword", @"") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"title.country_picker.txt", @"") attributes:@{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldPhone.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.placeholder.phone", @"") attributes:@{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];

    [self.btnReset setTitle:NSLocalizedString(@"btn.forgot.reset", @"") forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"btn.forgot.submit", @"") forState:UIControlStateNormal];
    self.imgViewBckEmail.layer.cornerRadius = 5.0;
    self.imgViewBckCountry.layer.cornerRadius = 5.0;
    self.imgViewBckPassword.layer.cornerRadius = 5.0;
    self.imgViewBckConfPwd.layer.cornerRadius = 5.0;

    // Scroll view content size
    if (SCREEN_HEIGHT == 480) {
        self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 500);
    } else if (SCREEN_HEIGHT == 568) {
        self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 600);
    }
}

-(void) prefillCountryCode{
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSArray *countryArray = server.countryArr;

    for (NSDictionary *dict in countryArray) {
        if ([[dict valueForKey:@"iso"] isEqualToString:countryCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]];

            [self.txtFldCountry setText:[dict valueForKey:kkeyCountryName]];

            UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];

            container.backgroundColor = [UIColor clearColor];
            [self pickCountry:dict];
        }
    }
}

#pragma mark - Tap gesture method

- (void)resignKeyboard {
    [self.view endEditing:YES];
}

#pragma mark
#pragma mark Action Method
#pragma mark

// ----------------------------------------------------------------------------------------------
// actionOnBack

- (void)actionOnBack {
    // Pop controller
    [self.navigationController popViewControllerAnimated:NO];
}
// ----------------------------------------------------------------------------------------------
// actionOnCountryBtn

- (IBAction)actionOnCountryBtn {
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}

#pragma mark - Country Picker Delegates Methods

- (void)pickCountry:(NSDictionary *)countryDict {
    if ([countryDict count] != 0) {
        countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyId]];

        [self.txtFldCountry setText:[countryDict valueForKey:kkeyCountryName]];

        // Set search icon to textfield
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
        container.backgroundColor = [UIColor clearColor];

        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaRegular size:14.0];
        self.lblPhoneCode.textColor = [UIColor blackColor];

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];

        }

        [container addSubview:self.lblPhoneCode];

        self.txtFldPhone.leftView = container;
        self.txtFldPhone.leftViewMode = UITextFieldViewModeAlways;
    }
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

// ------------------------------------------------------------------------------------------------------------------------------
// actionOnBtnClick:

- (IBAction)actionOnBtnClick:(UIButton *)sender {
    // Check button type and perform action accordingly
    if (sender == self.btnSubmit) {
        if (self.txtFldCountry.text.length == 0) {
            [SRModalClass showAlert:NSLocalizedString(@"empty.country.alert", @"")];
        } else {
            mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtFldPhone.text];
            if (mobile_Number.length >= 4) {

                [self.txtFldPhone resignFirstResponder];

                // Call server api to get verify code for password
                NSDictionary *params = @{kKeyMobileNumber: mobile_Number, kKeyCountryId: countryMasterId};

                [server makeAsychronousRequest:kKeyClassForgotPassword
                                      inParams:params
                           isIndicatorRequired:YES
                                  inMethodType:kPOST];
            } else {
                [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
            }

        }
    } else if (sender == self.btnReset) {
        // Pop controller
        if ([self validate]) {
            // Call server api to reset password
            NSString *codeStr = [NSString stringWithFormat:@"%@%@%@%@%@%@", [self.txtFldCode1 text], [self.txtFldCode2 text], [self.txtFldCode3 text], [self.txtFldCode4 text], [self.txtFldCode5 text], [self.txtFldCode6 text]];

            NSString *password = [self.txtFldPassword text];
            NSDictionary *params = @{kKeyVerifyCode: codeStr, smsReferenceID: [forgotPwdDict valueForKey:smsReferenceID], kKeyPassword: password};

            NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassForgotPassword, [[forgotPwdDict valueForKey:kKeyUser] objectForKey:kKeyId]];
            [server makeAsychronousRequest:urlStr
                                  inParams:params
                       isIndicatorRequired:YES
                              inMethodType:kPUT];
        }
    }
}

#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldShouldReturn:
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // Set flag and show alert
    if (textField == self.txtFldCode1) {
        [self.txtFldCode1 resignFirstResponder];
        [self.txtFldCode2 becomeFirstResponder];
    } else if (textField == self.txtFldCode2) {
        [self.txtFldCode2 resignFirstResponder];
        [self.txtFldCode3 becomeFirstResponder];
    } else if (textField == self.txtFldCode3) {
        [self.txtFldCode3 resignFirstResponder];
        [self.txtFldCode4 becomeFirstResponder];
    } else if (textField == self.txtFldCode4) {
        [self.txtFldCode4 resignFirstResponder];
        [self.txtFldCode5 becomeFirstResponder];
    } else if (textField == self.txtFldCode5) {
        [self.txtFldCode5 resignFirstResponder];
        [self.txtFldCode6 becomeFirstResponder];
    } else if (textField == self.txtFldCode6) {
        [self.txtFldCode6 resignFirstResponder];
    } else if (textField == self.txtFldPassword) {
        [self.txtFldPassword resignFirstResponder];
    } else if (textField == self.txtFldConfPwd) {
        [self.txtFldConfPwd resignFirstResponder];
    }
    // Return
    return YES;
}

- (BOOL)validate {
    if ([[self.txtFldCode1 text] length] == 0 ||
            [[self.txtFldCode2 text] length] == 0 ||
            [[self.txtFldCode3 text] length] == 0 ||
            [[self.txtFldCode4 text] length] == 0 ||
            [[self.txtFldCode5 text] length] == 0 ||
            [[self.txtFldCode6 text] length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.enter.correctCode", @"")];
        return NO;
    } else if ([self.txtFldPassword.text length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.password.alert", "")];
        [self.txtFldPassword becomeFirstResponder];
        return NO;
    } else if ([self.txtFldPassword.text length] < 8) {
        [SRModalClass showAlert:NSLocalizedString(@"lbl.pwd.hint", @"")];
        [self.txtFldPassword becomeFirstResponder];
        return NO;
    } else if ([self.txtFldConfPwd.text length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"confirm.password.alert", "")];
        [self.txtFldConfPwd becomeFirstResponder];
        return NO;
    } else if (![self.txtFldConfPwd.text isEqualToString:self.txtFldPassword.text]) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.password.match.text", "")];
        [self.txtFldConfPwd becomeFirstResponder];
        return NO;
    } else
        return YES;
}
// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    currentTF = sender;
    if (![currentTF isEqual:self.txtFldPhone] && ![currentTF isEqual:self.txtFldCountry] && ![currentTF isEqual:self.txtFldPassword] && ![currentTF isEqual:self.txtFldConfPwd]) {
        if (currentTF.tag == 6) {
            if (self.txtFldCode5.text.length == 0) {
                [self.txtFldCode5 becomeFirstResponder];
            } else {
                [self.txtFldCode6 becomeFirstResponder];
            }
        }
        if (currentTF.tag == 5) {
            if ([currentTF isEqual:self.txtFldCode5] && self.txtFldCode6.text.length > 0) {
                [self.txtFldCode5 resignFirstResponder];
            } else if (self.txtFldCode4.text.length == 0) {
                [self.txtFldCode4 becomeFirstResponder];
            } else {
                [self.txtFldCode5 becomeFirstResponder];
            }
        }
        if (currentTF.tag == 4) {
            if ([currentTF isEqual:self.txtFldCode4] && self.txtFldCode5.text.length > 0) {
                [self.txtFldCode4 resignFirstResponder];
            } else if (self.txtFldCode3.text.length == 0) {
                [self.txtFldCode3 becomeFirstResponder];
            } else {
                [self.txtFldCode4 becomeFirstResponder];
            }
        }
        if (currentTF.tag == 3) {
            if ([currentTF isEqual:self.txtFldCode3] && self.txtFldCode4.text.length > 0) {
                [self.txtFldCode3 resignFirstResponder];
            } else if (self.txtFldCode2.text.length == 0) {
                [self.txtFldCode2 becomeFirstResponder];
            } else {
                [self.txtFldCode3 becomeFirstResponder];
            }
        }
        if (currentTF.tag == 2) {
            if ([currentTF isEqual:self.txtFldCode2] && self.txtFldCode3.text.length > 0) {
                [self.txtFldCode2 resignFirstResponder];
            } else if (self.txtFldCode1.text.length == 0) {
                [self.txtFldCode1 becomeFirstResponder];
            } else {
                [self.txtFldCode2 becomeFirstResponder];
            }
        }
        if (currentTF.tag == 1) {
            if ([currentTF isEqual:self.txtFldCode1] && self.txtFldCode2.text.length > 0) {
                [self.txtFldCode1 resignFirstResponder];
            } else {
                [self.txtFldCode1 becomeFirstResponder];
            }
        }
    } else if ([currentTF isEqual:self.txtFldPassword] || [currentTF isEqual:self.txtFldConfPwd]) {
        if ([currentTF isEqual:self.txtFldPassword]) {
            [self.pwdImg setImage:[UIImage imageNamed:@"signup-password-orange.png"]];
            [self.confPwdImg setImage:[UIImage imageNamed:@"signup-password.png.png"]];
        } else if ([currentTF isEqual:self.txtFldConfPwd]) {
            [self.confPwdImg setImage:[UIImage imageNamed:@"signup-password-orange.png"]];
            [self.pwdImg setImage:[UIImage imageNamed:@"signup-password.png.png"]];
        }
    }
}
// s--------------------------------------------------------------------------------
// textFieldDidEndEditing:

- (void)textFieldDidEndEditing:(UITextField *)sender {
    currentTF = nil;
    [self.confPwdImg setImage:[UIImage imageNamed:@"signup-password.png.png"]];
    [self.pwdImg setImage:[UIImage imageNamed:@"signup-password.png.png"]];
}
// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    currentTF = textField;
    if ([currentTF isEqual:self.txtFldPassword] || [currentTF isEqual:self.txtFldConfPwd]) {
        return YES;
    }
    if ([currentTF isEqual:self.txtFldPhone]) {
        BOOL isValid = YES;
        NSString *errorMsg = nil;
        NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];

        if ([textStrToCheck length] > 17) {
            errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
        } else if (errorMsg == nil) {
            BOOL flag = [SRModalClass numberValidation:string];

            textField.text = [SRModalClass formatPhoneNumber:self.txtFldPhone.text deleteLastChar:NO];

            if (!flag) {
                errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
            }
        }
        // Set flag and show alert
        if (errorMsg) {
            isValid = NO;
            [SRModalClass showAlert:errorMsg];
        }

        // Return
        return isValid;
    }
    if (string.length == 0 && textField.text.length) {
        [self performSelector:@selector(setPreviousResponder:) withObject:textField afterDelay:0.0];
        return NO;
    } else {
        //textField.text=@"";
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;

        // This 'tabs' to next field when entering digits
        if (oldLength == 1) {
            if (textField == self.txtFldCode1) {
                [self setNextResponder:self.txtFldCode2 andStr:string];
            } else if (textField == self.txtFldCode2) {
                [self setNextResponder:self.txtFldCode3 andStr:string];
            } else if (textField == self.txtFldCode3) {
                [self setNextResponder:self.txtFldCode4 andStr:string];
            } else if (textField == self.txtFldCode4) {
                [self setNextResponder:self.txtFldCode5 andStr:string];
            } else if (textField == self.txtFldCode5) {
                [self setNextResponder:self.txtFldCode6 andStr:string];
            } else if (textField == self.txtFldCode6) {
                [self.txtFldCode6 resignFirstResponder];
            }
        }
        // Return
        return newLength <= 1;
    }
    return NO;
}

// --------------------------------------------------------------------------------
// setPreviousResponder:
- (void)setPreviousResponder:(UITextField *)prevResponder {
    [prevResponder resignFirstResponder];
    if ([prevResponder isEqual:self.txtFldCode6]) {
        self.txtFldCode6.text = @"";
        [self.txtFldCode5 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode5]) {
        self.txtFldCode5.text = @"";
        [self.txtFldCode4 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode4]) {
        self.txtFldCode4.text = @"";
        [self.txtFldCode3 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode3]) {
        self.txtFldCode3.text = @"";
        [self.txtFldCode2 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode2]) {
        self.txtFldCode2.text = @"";
        [self.txtFldCode1 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode1]) {
        self.txtFldCode1.text = @"";
        [self.txtFldCode1 becomeFirstResponder];
    }
}

// --------------------------------------------------------------------------------
// setNextResponder:
- (void)setNextResponder:(UITextField *)nextResponder andStr:(NSString *)str {
    [nextResponder becomeFirstResponder];
    if (nextResponder.text.length == 0) {
        nextResponder.text = str;
    }
}

#pragma mark
#pragma mark Notification Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// forgotPasswordGetCodeSucceed:
- (void)forgotPasswordGetCodeSucceed:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    forgotPwdDict = [[NSDictionary alloc] initWithDictionary:notification.object];
    [self.phoneNoBackView setHidden:TRUE];
    // Hide fields
    self.codeView.hidden = FALSE;
    [self.txtFldCode1 becomeFirstResponder];
}

// --------------------------------------------------------------------------------
// forgotPasswordGetCodeFailed:
- (void)forgotPasswordGetCodeFailed:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    self.txtFldPhone.text = @"";
    [self.txtFldPhone resignFirstResponder];
    [SRModalClass showAlert:notification.object];
}

// --------------------------------------------------------------------------------
// forgotPasswordSucceed:
- (void)forgotPasswordSucceed:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:notification.object];
    [self.navigationController popViewControllerAnimated:YES];
}

// --------------------------------------------------------------------------------
// forgotPasswordFailed:
- (void)forgotPasswordFailed:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:notification.object];
    self.txtFldCode1.text = @"";
    self.txtFldCode2.text = @"";
    self.txtFldCode3.text = @"";
    self.txtFldCode4.text = @"";
    self.txtFldCode5.text = @"";
    self.txtFldCode6.text = @"";
    [self.txtFldCode1 becomeFirstResponder];
}

// --------------------------------------------------------------------------------
// keyboardWasShown:
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, currentTF.frame.origin)) {
        [self.scrollView scrollRectToVisible:currentTF.frame animated:YES];
    }
}

// --------------------------------------------------------------------------------
// keyboardWillBeHidden:
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;

    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
