//
//  forgotPasswordViewController.h
//  Serendipity
//
//  Created by Leo on 06/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRServerConnection.h"
#import "SRModalClass.h"

@interface SRForgotPasswordViewController : ParentViewController<UITextFieldDelegate>
{
    // Instance variable
    NSString *countryMasterId;
     UITextField *currentTF;
    // Server
    SRServerConnection *server;
   
}
@property (strong, nonatomic) IBOutlet UIView *phoneNoBackView, *codeView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckEmail,*imgViewBckCountry;
@property (strong, nonatomic) IBOutlet UIButton *countryBtn;
@property (strong, nonatomic) IBOutlet UITextField *txtFldPhone,*txtFldCountry;
@property (strong, nonatomic) IBOutlet UIImageView *countryImg,*phoneImg;
@property (strong,nonatomic) UILabel *lblPhoneCode;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *lblFirstMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblSecondMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstMessage;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode1;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode2;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode3;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode4;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode5;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode6;
@property (strong, nonatomic) IBOutlet UILabel *lblCode1;
@property (strong, nonatomic) IBOutlet UILabel *lblCode2;
@property (strong, nonatomic) IBOutlet UILabel *lblCode3;
@property (strong, nonatomic) IBOutlet UILabel *lblCode4;
@property (strong, nonatomic) IBOutlet UILabel *lblCode5;
@property (strong, nonatomic) IBOutlet UILabel *lblCode6;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtFldPassword;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckConfPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtFldConfPwd;
@property (strong, nonatomic) IBOutlet UIButton *btnReset;
@property (strong, nonatomic) IBOutlet UIImageView *pwdImg;
@property (strong, nonatomic) IBOutlet UIImageView *confPwdImg;
#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnBtnClick:(UIButton *)sender;

@end
