#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import "AVFoundation/AVFoundation.h"
#import "M13ProgressViewBorderedBar.h"
#import "SRServerConnection.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "ParentViewController.h"
#import <WebKit/WebKit.h>
@interface SRPreLoginViewController : ParentViewController  <WKNavigationDelegate,UIScrollViewDelegate>

{
	// Instance variable

	// Server
	SRServerConnection *server;
    
}
@property (nonatomic,strong) ACAccount *facebookAccount;
// Properties
@property (strong, nonatomic) IBOutlet UIButton *btnLearnMore;
@property (strong, nonatomic) IBOutlet UILabel *lblLogoTxt;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp,*btnFacebook;
//@property (strong, nonatomic) IBOutlet FBSDKLoginButton *btnFacebook;
@property (strong, nonatomic) IBOutlet UILabel *lblLogInTxt;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet WKWebView *videoWebView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewClose;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UILabel *learnMore;
// Properties for logo animation
@property (nonatomic, weak) IBOutlet UIView *progressContainer;
@property (nonatomic, weak) IBOutlet M13ProgressViewBorderedBar *progressView;
@property (nonatomic, weak) IBOutlet UILabel *lblProgressCount;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewLogo;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewUpperLayer;

// Other properties
@property (strong, nonatomic) AVPlayer *videoPlayer;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnBtnClick:(UIButton *)sender;

@end
