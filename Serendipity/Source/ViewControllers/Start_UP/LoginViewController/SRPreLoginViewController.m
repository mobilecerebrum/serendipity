#import "Constant.h"
#import "SRLoginViewController.h"
#import "SRSignupViewController.h"
#import "SRSplashViewController.h"
#import "SRPreLoginViewController.h"
#import "SRModalClass.h"
#import "NSArray+Plist.h"

@implementation SRPreLoginViewController

#pragma mark - Private Method

// -----------------------------------------------------------------------------------------------
// removeAnimatedView

- (void)removeAnimatedView {
    self.imgViewUpperLayer.hidden = YES;
    self.progressContainer.hidden = YES;
}

#pragma mark - Init Method

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRPreLoginViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
    }

    // Return
    return self;
}

#pragma mark - Standard Overrides Method

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];
    // Prepare view design
    [self.btnLearnMore setTitle:NSLocalizedString(@"btn.learn.more", @"") forState:UIControlStateNormal];
    [self.btnSignUp setTitle:NSLocalizedString(@"btn.text.sign.up", @"") forState:UIControlStateNormal];
    [self.btnFacebook setTitle:NSLocalizedString(@"btn.text.facebook.sign.up", @"") forState:UIControlStateNormal];
//    self.btnFacebook.layer.cornerRadius = 5;
//    self.btnFacebook.layer.masksToBounds = YES;

    if (!server) {
        server = [APP_DELEGATE server];
    }

    if (SCREEN_HEIGHT > 568) {
        self.lblLogInTxt.font = [UIFont fontWithName:kFontHelveticaRegular size:14];
        self.btnLogin.titleLabel.font = [UIFont fontWithName:kFontHelveticaRegular size:15];
    }

    NSDictionary *detailTxtDict = @{
            NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0],
            NSForegroundColorAttributeName: [UIColor whiteColor]
    };

    NSDictionary *loginTxtDict = @{
            NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:13.0],
            NSForegroundColorAttributeName: [UIColor whiteColor]
    };

    NSString *msgOne = NSLocalizedString(@"lbl.have.acnt", @"");
    NSMutableAttributedString *detailTxt = [[NSMutableAttributedString alloc] initWithString:msgOne attributes:detailTxtDict];

    NSString *msgTwo = NSLocalizedString(@"lbl.dtllogin.txt", @"");
    NSMutableAttributedString *attrMsgPartOne = [[NSMutableAttributedString alloc] initWithString:msgTwo attributes:loginTxtDict];
    NSMutableAttributedString *string = detailTxt;
    [string appendAttributedString:attrMsgPartOne];
    self.lblLogInTxt.attributedText = (NSAttributedString *) string;

    // Add subview for video play
    [self.view addSubview:self.videoView];
    self.videoView.hidden = YES;
    self.btnClose.layer.cornerRadius = self.btnClose.frame.size.width / 2;

    // Hide status bar
    self.navigationController.navigationBar.hidden = YES;
//	[[UIApplication sharedApplication] setStatusBarHidden:YES];

    // Progress
    self.progressView.progressDirection = M13ProgressViewBorderedBarProgressDirectionLeftToRight;
    self.progressView.cornerType = M13ProgressViewBorderedBarCornerTypeCircle;
    self.progressView.bounds = CGRectMake(0, 0, 210, 6);

    [self.progressView setProgress:0.74 animated:YES];
    self.lblProgressCount.text = @"74%";

    self.lblLogoTxt.text = NSLocalizedString(@"lbl.world.quote", @"");
    self.lblLogoTxt.font = [UIFont fontWithName:kFontSharikSansBold size:12.0];

}

// ------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    // Hide status bar

    self.navigationController.navigationBar.hidden = YES;
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];

    // Register notifications
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(loginSucceded:)
                          name:kKeyNotificationLoginSuccess
                        object:nil];

    [defaultCenter addObserver:self
                      selector:@selector(loginFailed:)
                          name:kKeyNotificationLoginFailed
                        object:nil];

    [defaultCenter addObserver:self
                      selector:@selector(getCountryListSucceed:)
                          name:kKeyNotificationCountryListSucceed
                        object:nil];

    [defaultCenter addObserver:self
                      selector:@selector(getCountryListFailed:)
                          name:kKeyNotificationCountryListFailed
                        object:nil];


    NSArray *cachedCountries = [NSArray readFromPlistFile:@"countries.plist"];
    
    if (cachedCountries == nil) {
        [self preloadCountries];
    } else {
        if (cachedCountries.count == 0) {
            [self preloadCountries];
        } else {
            server.countryArr = cachedCountries;
            [self checkLogin];
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------------------
// viewWillDisappear:

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    //[server.loggedInFbUserInfo removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationLoginSuccess object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationLoginFailed object:nil];
}
// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Remove observer for notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Login Process

- (void)animateLogo {
    // Animate the logo to become large
    CGFloat shrinkDuration = 1.0 * 0.3;

    [UIView animateWithDuration:shrinkDuration delay:0 usingSpringWithDamping:0.7f initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGAffineTransform scaleTransform = CGAffineTransformMakeScale(0.75, 0.75);
        self.imgViewLogo.transform = scaleTransform;
    }                completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            CGAffineTransform scaleTransform = CGAffineTransformMakeScale(20, 20);
            self.imgViewLogo.transform = scaleTransform;
            self.imgViewUpperLayer.alpha = 0.6;
            [self.progressView setProgress:0.99 animated:YES];
            self.lblProgressCount.text = @"99%";
            self.lblLogoTxt.hidden = YES;
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:0.4 animations:^{
                self.imgViewLogo.alpha = 0.0;
                [self.progressView setProgress:1.0 animated:YES];
                self.lblProgressCount.text = @"100%";
            }                completion:^(BOOL finished) {
                self.imgViewUpperLayer.alpha = 0.0;
                self.imgViewUpperLayer.hidden = YES;
                self.progressContainer.hidden = YES;
                self.imgViewLogo.alpha = 1.0;
                self.imgViewLogo.transform = CGAffineTransformMakeScale(1, 1);
                self.lblLogoTxt.hidden = NO;
                self.lblLogoTxt.text = NSLocalizedString(@"lbl.logo.text", @"");
                if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {
                    self.lblLogoTxt.font = [UIFont fontWithName:kFontSharikSansBold size:30.0];
                } else
                    self.lblLogoTxt.font = [UIFont fontWithName:kFontSharikSansBold size:25.0];
                [[UIApplication sharedApplication] setStatusBarHidden:NO];
            }];
        }];
    }];
}

- (void)preloadCountries {
    [server makeSynchronousRequest:kKeyClassGetCountryList inParams:nil inMethodType:kGET isIndicatorRequired:NO];
}

- (BOOL)isLoggedIn {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:loginDataPath];
}

- (void)checkLogin {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:loginDataPath]) {
        NSDictionary *loggedInDict = [[NSDictionary alloc] initWithContentsOfFile:loginDataPath];
        NSString *deviceToken = [loggedInDict valueForKey:@"deviceToken"];

        if (deviceToken == nil) {
            if ([[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"] == nil) {
                deviceToken = @"";
                server.deviceToken = @"";
            } else {
                deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
            }
        }

        NSString *fromFb = [loggedInDict valueForKey:kKeyIsFromFacebook];

        [APP_DELEGATE showActivityIndicator];

        if ([fromFb isEqualToString:@"1"]) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *FBDataPath = paths[0];
            FBDataPath = [FBDataPath stringByAppendingPathComponent:@"FBData.plist"];
            NSFileManager *fileManager = [NSFileManager defaultManager];

            if ([fileManager fileExistsAtPath:FBDataPath]) {
                NSDictionary *theDict = [NSDictionary dictionaryWithContentsOfFile:FBDataPath];

                server.loggedInFbUserInfo = (NSMutableDictionary *) theDict;
            }

            NSString *userName = [loggedInDict valueForKey:kKeyEmail];
            NSString *social_id = [loggedInDict valueForKey:kKeyId];
            NSDictionary *params = @{kKeyEmail: userName != nil ? userName : @"",
                                     kkeySocialId: social_id != nil ? social_id : @"",
                                     kkeyLoginType: @"facebook", kkeyIsLogin: @"1",
                                     kKeyDeviceToken: deviceToken != nil ? deviceToken : @"",
                                     kKeyDeviceType: kKeyDeviceName,
                                     kDeviceId : [APP_DELEGATE getDeviceId],
                                     @"add_info": [APP_DELEGATE getAdditionalInfo]};

            [server makeAsychronousRequest:kKeyClassLogin inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        } else {
            // restore login session
            NSString *mobileNumber = [loggedInDict valueForKey:kKeyMobileNumber];
            NSString *password = [loggedInDict valueForKey:kKeyPassword];
            NSString *country_id = [loggedInDict valueForKey:kKeyCountryMasterId];
            
            NSDictionary *params = @{kKeyEmail: mobileNumber != nil ? mobileNumber : @"",
                                     kKeyPassword: password != nil ? password : @"",
                                     kKeyCountryId: country_id != nil ? country_id : @"",
                                     kkeyIsLogin: @"1",
                                     kKeyDeviceToken: deviceToken == nil ? @"" : deviceToken,
                                     kKeyDeviceType: kKeyDeviceName,
                                     kDeviceId : [APP_DELEGATE getDeviceId],
                                     @"add_info": [APP_DELEGATE getAdditionalInfo]};

            [server makeAsychronousRequest:kKeyClassLogin inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        }
    } else {
        // 💬 if user not logged in
       // NSLog(@"💬 show login");
        [self removeAnimatedView];
    }
}

#pragma mark - IBAction Method

// ------------------------------------------------------------------------------------------------------------------------------
// actionOnBtnClick:

- (IBAction)actionOnBtnClick:(UIButton *)sender {
    // Check button type and perform action accordingly
    if (sender == self.btnLearnMore) {
        // Add subview for video play
        [APP_DELEGATE showActivityIndicator];
        CGFloat safeAreaTopInset = 0;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat topPadding = window.safeAreaInsets.top;
            safeAreaTopInset = topPadding;
        }
        self.videoView.frame = CGRectMake(0, safeAreaTopInset, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        [UIView animateWithDuration:0.2f animations:^{
            [self.videoView setAlpha:0.0f];
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:0.8f animations:^{
                self.videoView.hidden = NO;
                [self.videoView setAlpha:1.0f];
            }                completion:nil];
        }];

        // Prepare video play
        NSURL *targetURL = [NSURL URLWithString:@"http://serendipity.app/mobile/source.html"];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        self.videoWebView.navigationDelegate = self;
        [self.videoWebView loadRequest:request];

        // Bring other views to front
        [self.videoView bringSubviewToFront:self.imgViewClose];
        [self.videoView bringSubviewToFront:self.learnMore];
        [self.videoView bringSubviewToFront:self.btnClose];
    } else if (sender == self.btnClose) {
        [UIView animateWithDuration:0.2f animations:^{
            [self.videoView setAlpha:1.0f];
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0f animations:^{
                [self.videoView setAlpha:0.0f];
            }                completion:^(BOOL finished) {
                self.videoView.hidden = YES;
            }];
        }];
    } else if (sender == self.btnSignUp) {
        // Push to sign up
        SRSignupViewController *signupViewCon = [[SRSignupViewController alloc] initWithNibName:nil bundle:nil server:server];
        signupViewCon.delegate = self;
        [self.navigationController pushViewController:signupViewCon animated:NO];
    } else if (sender == self.btnLogin) {
        // Go to Login Screen
        SRLoginViewController *loginViewCon = [[SRLoginViewController alloc] initWithNibName:nil bundle:nil server:server];
        [self.navigationController pushViewController:loginViewCon animated:YES];
    } else if (sender == self.btnFacebook) {
        
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [APP_DELEGATE hideActivityIndicator];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 20; // set as you want
}

#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    self.videoWebView.scrollView.maximumZoomScale = 20; // set similar to previous.
}

//
//-----------------------------------------------------------
// Get info from facebook and send it to sign up view
- (void)getUserInfo {
    
}

#pragma mark - Notifications Method

- (void)getCountryListSucceed:(NSNotification *)inNotify {
    server.countryArr = [inNotify object];
    [APP_DELEGATE hideActivityIndicator];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];


    [((NSArray *) [inNotify object]) writeToPlistFile:@"countries.plist"];

    [self animateLogo];
    if ([self isLoggedIn]) {
        [self checkLogin];
    }
}

- (void)getCountryListFailed:(NSNotification *)inNotify {
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.countrylist.failed", @"")];
}

// -----------------------------------------------------------------------------------------------
// loginSucceded:
- (NSString *)directoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *loggedInUser = [NSString stringWithFormat:@"%@", server.loggedInUserInfo[kKeyId]];
   // NSLog(@"%@", loggedInUser);
    NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:loggedInUser];

    NSError *error;

    if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return dirPath;
}

- (void)loginSucceded:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];

    NSMutableDictionary *loggedInDict = [[NSMutableDictionary alloc] init];
    [loggedInDict setValue:server.loggedInFbUserInfo[kKeyEmail] forKey:kKeyEmail];
    [loggedInDict setValue:[server.loggedInFbUserInfo valueForKey:kKeyId] forKey:kKeyId];
    [loggedInDict setValue:@"1" forKey:kKeyIsFromFacebook];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
    [userDefaults synchronize];
    //  [server.loggedInFbUserInfo removeAllObjects];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:loginDataPath]) {
        NSError *error;
        [fileManager removeItemAtPath:loginDataPath error:&error];
    }
    // Write file to path
    BOOL success = [loggedInDict writeToFile:loginDataPath atomically:YES];
    if (success)
        addTextInSignificantTxt(@"Login Credential written successfully");
    else
        addTextInSignificantTxt(@"Login Credential written Unsuccessfully");

//    [(APP_DELEGATE) cometChatLogin];
}

- (void)loginFailed:(NSNotification *)inNotify {
    // Push to sign up
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [APP_DELEGATE hideActivityIndicator];
    [self animateLogo];
    if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"] && ![[inNotify object] isEqualToString:@"You are already logged in on another device"]) {
        if ([[inNotify object] isEqualToString:@"Mobile number is not verified."]) {
            [SRModalClass showAlert:[inNotify object]];
        }
        SRSignupViewController *signupViewCon = [[SRSignupViewController alloc] initWithNibName:nil bundle:nil server:server];
        signupViewCon.delegate = self;
        [self.navigationController pushViewController:signupViewCon animated:NO];
    } else {
        // Show alerts
        dispatch_async(dispatch_get_main_queue(), ^{
            [SRModalClass showAlert:[inNotify object] fromController:self];
        });

    }
}

@end
