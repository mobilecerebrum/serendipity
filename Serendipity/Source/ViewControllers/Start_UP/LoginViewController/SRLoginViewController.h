#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import "AVFoundation/AVFoundation.h"
#import "SRServerConnection.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "SRCountryPickerViewController.h"
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreLocation/CoreLocation.h>

@interface SRLoginViewController : ParentViewController <WKNavigationDelegate, UIScrollViewDelegate, CountryPickerDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>
{
    NSString *countryMasterId;
	SRServerConnection *server;
}

@property (nonatomic,strong) ACAccount *facebookAccount;
@property (strong, nonatomic) IBOutlet UIButton *btnLearnMore;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *bckViewLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblLogoTxt;
@property (strong, nonatomic) IBOutlet UIView *emailBckView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckCountry;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCountry;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFldEmail;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtFldPwd;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin, *btnFacebook,*btnCountry;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPwd;
@property (strong, nonatomic) IBOutlet UILabel *lblDetailTxt,*lblPhoneCode;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet WKWebView *videoWebView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewClose;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UILabel *learnMore;
@property (strong, nonatomic) AVPlayer *videoPlayer;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, assign) BOOL isCountrySelected;

#pragma mark - Init Method

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil server:(SRServerConnection *)inServer;

#pragma mark - IBAction Method

- (IBAction)actionOnBtnClick:(UIButton *)sender;

@end
