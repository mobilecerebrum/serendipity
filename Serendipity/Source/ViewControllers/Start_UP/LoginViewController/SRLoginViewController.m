#import "Constant.h"
#import "SRSignupViewController.h"
#import "SRLoginViewController.h"
#import "SRForgotPasswordViewController.h"
#import "SRCompleteSignUpViewController.h"
#import "SRCodeViewController.h"
#import <WebKit/WebKit.h>


@implementation SRLoginViewController

#pragma mark - Standard Overrides Method

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;

    if (SCREEN_HEIGHT == 480.0) {
        CGRect frame = self.bckViewLogo.frame;
        frame.origin.y = 3.0;
        self.bckViewLogo.frame = frame;
        [self.scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, 530)];
        self.emailBckView.frame = CGRectMake(self.emailBckView.frame.origin.x, self.emailBckView.frame.origin.y + 90, self.emailBckView.frame.size.width, self.emailBckView.frame.size.height);
    }

    if (SCREEN_HEIGHT == 568) {
        [self.scrollView setContentSize:CGSizeMake(SCREEN_WIDTH, 580)];
        self.emailBckView.frame = CGRectMake(self.emailBckView.frame.origin.x, self.emailBckView.frame.origin.y + 50, self.emailBckView.frame.size.width, self.emailBckView.frame.size.height);
    }

    [self initButtons];

    if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {
        self.lblLogoTxt.font = [UIFont fontWithName:kFontSharikSansBold size:30.0];
    } else {
        self.lblLogoTxt.font = [UIFont fontWithName:kFontSharikSansBold size:25.0];
    }

    self.lblLogoTxt.text = NSLocalizedString(@"lbl.logo.text", @"");

    self.imgViewBckEmail.layer.cornerRadius = 5.0;
    self.imgViewBckPwd.layer.cornerRadius = 5.0;
    self.imgViewBckCountry.layer.cornerRadius = 5.0;

    UIColor *color = [UIColor blackColor];

    self.txtFldCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"title.country_picker.txt", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:14.0]}];
    self.txtFldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.placeholder.phone", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:14.0]}];
    self.txtFldPwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.placeholder.pwd", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:14.0]}];

    NSDictionary *detailTxtDict = @{
            NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0],
            NSForegroundColorAttributeName: [UIColor whiteColor]
    };

    NSDictionary *loginTxtDict = @{
            NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:13.0],
            NSForegroundColorAttributeName: [UIColor whiteColor]
    };

    NSString *msgOne = NSLocalizedString(@"lbl.detail.txt", @"");
    NSMutableAttributedString *detailTxt = [[NSMutableAttributedString alloc] initWithString:msgOne attributes:detailTxtDict];
    NSString *msgTwo = NSLocalizedString(@"lbl.SignUp.txt", @"");
    NSMutableAttributedString *attrMsgPartOne = [[NSMutableAttributedString alloc] initWithString:msgTwo attributes:loginTxtDict];
    NSMutableAttributedString *string = detailTxt;

    [string appendAttributedString:attrMsgPartOne];

    self.lblDetailTxt.attributedText = (NSAttributedString *) string;

    [self.view addSubview:self.videoView];

    self.videoView.hidden = YES;
    self.btnClose.layer.cornerRadius = self.btnClose.frame.size.width / 2;

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];

    [self.scrollView addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    [self.locationManager startMonitoringSignificantLocationChanges];

    self.currentLocation = [[CLLocation alloc] init];

    (APP_DELEGATE).lastLocation = nil;
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver:self
                      selector:@selector(loginSucceded:)
                          name:kKeyNotificationLoginSuccess
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(loginFailed:)
                          name:kKeyNotificationLoginFailed
                        object:nil];
    
    [defaultCenter addObserver:self
                         selector:@selector(EmailSendSucceded:)
                             name:kKeyNotificationSendEmailSuccess
                           object:nil];
       
       [defaultCenter addObserver:self
                         selector:@selector(EmailSendFailed:)
                             name:kKeyNotificationSendEmailFail
                           object:nil];
    
    
    [self prefillCountryCode];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    

    self.navigationController.navigationBar.hidden = YES;

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
//    [self prefillCountryCode];
//    if (!self.isCountrySelected && self.txtFldCountry.text.length == 0) {
//        [(APP_DELEGATE) showActivityIndicator];
//
//        [self.locationManager startUpdatingLocation];
//    }
}

-(void) prefillCountryCode{
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSArray *countryArray = server.countryArr;

    for (NSDictionary *dict in countryArray) {
        if ([[dict valueForKey:@"iso"] isEqualToString:countryCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]];

            [self.txtFldCountry setText:[dict valueForKey:kkeyCountryName]];

            UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];

            container.backgroundColor = [UIColor clearColor];

            self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
            self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
            self.lblPhoneCode.textColor = [UIColor blackColor];

            if (dict && [dict valueForKey:kKeyPhoneCode]) {
                [self.lblPhoneCode setText:[@"+" stringByAppendingString:[NSString stringWithFormat:@"%@", [dict valueForKey:kKeyPhoneCode]]]];
            }

            [container addSubview:self.lblPhoneCode];

            self.txtFldEmail.leftView = container;
            self.txtFldEmail.leftViewMode = UITextFieldViewModeAlways;
        }
    }
}
#pragma mark - Private Method

- (void)initButtons {
    [self.btnLearnMore setTitle:NSLocalizedString(@"btn.learn.more", @"") forState:UIControlStateNormal];
    [self.btnLogin setTitle:NSLocalizedString(@"lbl.login.txt", @"") forState:UIControlStateNormal];
    [self.btnForgotPwd setTitle:NSLocalizedString(@"btn.fogort.pwd", @"") forState:UIControlStateNormal];
    [self.btnFacebook setTitle:NSLocalizedString(@"btn.text.facebook.login", @"") forState:UIControlStateNormal];

    self.btnFacebook.layer.cornerRadius = 5;
    self.btnFacebook.layer.masksToBounds = YES;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.txtFldEmail.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad{
    [self.txtFldEmail resignFirstResponder];
    self.txtFldEmail.text = @"";
}

-(void)doneWithNumberPad{
    [self.txtFldEmail resignFirstResponder];
    [self.txtFldPwd becomeFirstResponder];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];

    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark - Init & Dealloc Method

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRLoginViewController" bundle:nil];
    if (self) {
        server = inServer;
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - LocationManager Delegate Method

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            //
        }
            break;

        case kCLAuthorizationStatusDenied: {
            //
        }
            break;

        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [self.locationManager startUpdatingLocation];
        }
            break;

        case kCLAuthorizationStatusAuthorizedAlways: {
            [self.locationManager startUpdatingLocation];
        }
            break;

        default:
            //
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
//    self.currentLocation = locations[0];

    [self.locationManager stopUpdatingLocation];

//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    [geocoder reverseGeocodeLocation:self.currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//        if (!(error)) {
//            CLPlacemark *placemark = placemarks[0];
//
//            if (![placemark.country isKindOfClass:(id) [NSNull class]] || placemark.country.length != 0) {
//                [(APP_DELEGATE) hideActivityIndicator];
//                NSArray *countryArray = server.countryArr;
//
//                for (NSDictionary *dict in countryArray) {
//                    if ([[dict valueForKey:kkeyCountryName] isEqualToString:placemark.country]) {
//                        countryMasterId = [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]];
//
//                        [self.txtFldCountry setText:placemark.country];
//
//                        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
//
//                        container.backgroundColor = [UIColor clearColor];
//
//                        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
//                        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
//                        self.lblPhoneCode.textColor = [UIColor blackColor];
//
//                        if (dict && [dict valueForKey:kKeyPhoneCode]) {
//                            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[NSString stringWithFormat:@"%@", [dict valueForKey:kKeyPhoneCode]]]];
//                        }
//
//                        [container addSubview:self.lblPhoneCode];
//
//                        self.txtFldEmail.leftView = container;
//                        self.txtFldEmail.leftViewMode = UITextFieldViewModeAlways;
//                    }
//                }
//
//                if (self.txtFldCountry.text.length == 0) {
//                    [self displayDefaultCountry];
//                }
//            } else {
//                [self displayDefaultCountry];
//            }
//        } else {
//            [self displayDefaultCountry];
//        }
//    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [(APP_DELEGATE) hideActivityIndicator];

    [self displayDefaultCountry];
}

#pragma mark - Private Method

- (void)displayDefaultCountry {
//    self.txtFldCountry.text = @"United States";
//    countryMasterId = @"226";
//
//    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
//
//    container.backgroundColor = [UIColor clearColor];
//
//    self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
//    self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
//    self.lblPhoneCode.textColor = [UIColor blackColor];
//
//    [self.lblPhoneCode setText:@"+1"];
//
//    [container addSubview:self.lblPhoneCode];
//
//    self.txtFldEmail.leftView = container;
//    self.txtFldEmail.leftViewMode = UITextFieldViewModeAlways;
}


#pragma mark - IBAction Method

- (IBAction)actionOnBtnClick:(UIButton *)sender {
    if (sender == self.btnLearnMore) {
        [self.view endEditing:YES];
        CGFloat safeAreaTopInset = 0;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            CGFloat topPadding = window.safeAreaInsets.top;
            safeAreaTopInset = topPadding;
        }        
        self.videoView.frame = CGRectMake(0, safeAreaTopInset, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        [self.scrollView setContentOffset:CGPointMake(0, 0)];
        [APP_DELEGATE showActivityIndicator];

        [UIView animateWithDuration:0.2f animations:^{
            [self.videoView setAlpha:0.0f];
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:0.8f animations:^{
                self.videoView.hidden = NO;
                [self.videoView setAlpha:1.0f];
            }                completion:nil];
        }];

        NSURL *targetURL = [NSURL URLWithString:@"http://serendipity.app/mobile/source.html"];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        self.videoWebView.navigationDelegate = self;
        [self.videoWebView loadRequest:request];

        [self.videoView bringSubviewToFront:self.imgViewClose];
        [self.videoView bringSubviewToFront:self.learnMore];
        [self.videoView bringSubviewToFront:self.btnClose];
    } else if (sender == self.btnClose) {
        [UIView animateWithDuration:0.5f animations:^{
            [self.videoView setAlpha:1.0f];
        }                completion:^(BOOL finished) {
            [UIView animateWithDuration:1.0f animations:^{
                [self.videoView setAlpha:0.0f];
            }                completion:^(BOOL finished) {
                self.videoView.hidden = YES;
            }];
        }];
    } else if (sender == self.btnLogin) {
        [self.btnLogin setEnabled:FALSE];
        [self.view endEditing:YES];
        [self.scrollView setContentOffset:CGPointMake(0, 0)];

        NSString *errMsg = nil;
        NSString *userName = [self.txtFldEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

        if ([userName length] == 0) {
            [self.btnLogin setEnabled:TRUE];
            errMsg = NSLocalizedString(@"empty.contact.alert", @"");
            [self.txtFldEmail becomeFirstResponder];
        } else if ([self.txtFldPwd.text length] == 0 && errMsg == nil) {
            [self.btnLogin setEnabled:TRUE];
            errMsg = NSLocalizedString(@"empty.password.error", @"");
            [self.txtFldPwd becomeFirstResponder];
        } else if (self.txtFldCountry.text.length == 0) {
            [self.btnLogin setEnabled:TRUE];
            errMsg = NSLocalizedString(@"empty.country.alert", @"");
        }

        if (errMsg != nil) {
            [SRModalClass showAlert:errMsg];
        } else {
            NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtFldEmail.text];

            if (mobile_Number.length >= 9) {
                [self.txtFldEmail resignFirstResponder];

                [APP_DELEGATE showActivityIndicator];

                NSString *deviceToken = server.deviceToken ? server.deviceToken: @"";
                
                NSDictionary *params = @{kKeyEmail: mobile_Number,
                                         kKeyPassword: self.txtFldPwd.text,
                                         kKeyCountryId: countryMasterId,
                                         kkeyIsLogin: @"1",
                                         kDeviceId : [APP_DELEGATE getDeviceId],
                                         kKeyDeviceToken: deviceToken,
                                         kKeyDeviceType: kKeyDeviceName,
                                         @"add_info": [(APP_DELEGATE) getAdditionalInfo]};

                [server makeSynchronousRequest:kKeyClassLogin inParams:params inMethodType:kPOST isIndicatorRequired:YES];
            } else {
                [self.btnLogin setEnabled:TRUE];

                [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
            }
        }
    } else if (sender == self.btnForgotPwd) {
        [APP_DELEGATE hideActivityIndicator];

        SRForgotPasswordViewController *forgotPwdView = [[SRForgotPasswordViewController alloc] initWithNibName:nil bundle:nil server:server];

        [self.navigationController pushViewController:forgotPwdView animated:NO];
    } else if (sender == self.btnSignUp) {
        [APP_DELEGATE hideActivityIndicator];

        SRSignupViewController *signupViewCon = [[SRSignupViewController alloc] initWithNibName:nil bundle:nil server:server];

        signupViewCon.delegate = self;

        [self.navigationController pushViewController:signupViewCon animated:NO];
    } else if (sender == self.btnFacebook) {
//        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
//
//        [loginManager logOut];
//        [loginManager logInWithReadPermissions:@[@"public_profile", @"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//            if (error) {
//                [SRModalClass showAlert:@"Unable to process user info from facebook."];
//            } else if (result.isCancelled) {
//                //
//            } else {
//                [APP_DELEGATE showActivityIndicator];
//                [self getUserInfo];
//            }
//        }];
    }
}




- (IBAction)actionOnCountryBtn {
    [APP_DELEGATE hideActivityIndicator];

    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];

    countryPicker.delegate = self;

    [self.navigationController pushViewController:countryPicker animated:YES];
}

#pragma mark - Country Picker Delegates Methods

- (void)pickCountry:(NSDictionary *)countryDict {
    self.isCountrySelected = YES;

    if (countryDict && [countryDict count] != 0) {
        countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyId]];

        [self.txtFldCountry setText:[countryDict valueForKey:kkeyCountryName]];

        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];

        container.backgroundColor = [UIColor clearColor];

        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
        self.lblPhoneCode.textColor = [UIColor blackColor];

        if ([countryDict valueForKey:kKeyPhoneCode]) {
            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];
        }

        [container addSubview:self.lblPhoneCode];

        self.txtFldEmail.leftView = container;
        self.txtFldEmail.leftViewMode = UITextFieldViewModeAlways;
        [self.txtFldEmail becomeFirstResponder];
    }
}

#pragma mark - WKWebview Delegate Methods
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [APP_DELEGATE hideActivityIndicator];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 20; // set as you want
}

#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    self.videoWebView.scrollView.maximumZoomScale = 20;
}

- (void)getUserInfo {
    
}

#pragma mark - TextField Delegate Method

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFldEmail) {
        [self.txtFldEmail resignFirstResponder];
        [self.txtFldPwd becomeFirstResponder];
    } else if (textField == self.txtFldPwd) {
        [self.txtFldPwd resignFirstResponder];
        [self.scrollView setContentOffset:CGPointMake(0, 0)];
    }

    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    if (sender == self.txtFldEmail || sender == self.txtFldPwd) {
        if (SCREEN_HEIGHT <= 568) {
            if (sender == self.txtFldEmail) {
                [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.frame.origin.y + 80.0)];
            } else if (sender == self.txtFldPwd) {
                [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.frame.origin.y + 130.0)];
            }
        } else {
            [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.frame.origin.y + 50.0)];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ([textField isEqual:self.txtFldPwd]) {
        return YES;
    }

    if ([textField isEqual:self.txtFldEmail]) {
        BOOL isValid = YES;
        NSString *errorMsg = nil;
        NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];

        if ([textStrToCheck length] > 17) {
            errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
        } else if (errorMsg == nil) {
            BOOL flag = [SRModalClass numberValidation:string];

            textField.text = [SRModalClass formatPhoneNumber:self.txtFldEmail.text deleteLastChar:NO];

            if (!flag) {
                errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
            }
        }

        if (errorMsg) {
            isValid = NO;
            [SRModalClass showAlert:errorMsg];
        }

        return isValid;
    }

    return NO;
}

#pragma mark - Notifications Method

- (NSString *)directoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *loggedInUser = [NSString stringWithFormat:@"%@", server.loggedInUserInfo[kKeyId]];
    NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:loggedInUser];

    NSError *error;

    if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];
    }

    return dirPath;
}


- (void)CallAPiForSendOTPOnEmail{
    
    [APP_DELEGATE showActivityIndicator];
    NSString *deviceToken = server.deviceToken ? server.deviceToken: @"";
    NSDictionary *params = @{kKeyDeviceToken: deviceToken,
                             kDeviceId : [APP_DELEGATE getDeviceId],
                             kKeyDeviceType: kKeyDeviceName};

    [server makeSynchronousRequest:kKeyUserDeviceVerifications inParams:params inMethodType:kPOST isIndicatorRequired:YES];
}

- (void)EmailSendSucceded:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSMutableDictionary *responceDict = [inNotify object];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:server.loggedInUserInfo];

    SRCodeViewController *phoneCodeViewCon = [[SRCodeViewController alloc] initWithNibName:nil bundle:nil server:server data:params];
    phoneCodeViewCon.email = [[responceDict valueForKey:kKeyUser] valueForKey:@"email"];
    phoneCodeViewCon.password = self.txtFldPwd.text;
    phoneCodeViewCon.isUpdateDeviceToken = YES;
    phoneCodeViewCon.sms_reference_code = [[responceDict valueForKey:kKeyUser] valueForKey:deviceReferenceID];
    [self.navigationController pushViewController:phoneCodeViewCon animated:NO];
}
- (void)EmailSendFailed:(NSNotification *)inNotify {
    [self.view setUserInteractionEnabled:true];
    [APP_DELEGATE hideActivityIndicator];
}

- (void)loginSucceded:(NSNotification *)inNotify {
    [self.btnLogin setEnabled:TRUE];

    NSMutableDictionary *loggedInDict;

    if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
        loggedInDict = [[NSMutableDictionary alloc] init];
        [loggedInDict setValue:server.loggedInFbUserInfo[kKeyEmail] forKey:kKeyEmail];
        [loggedInDict setValue:[server.loggedInFbUserInfo valueForKey:kKeyId] forKey:kKeyId];
        [loggedInDict setValue:@"1" forKey:kKeyIsFromFacebook];

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
        [userDefaults synchronize];
    } else {
        NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtFldEmail.text];

        loggedInDict = [[NSMutableDictionary alloc] init];
        [loggedInDict setValue:mobile_Number forKey:kKeyMobileNumber];
        [loggedInDict setValue:self.txtFldPwd.text forKey:kKeyPassword];
        [loggedInDict setValue:countryMasterId forKey:kKeyCountryMasterId];
        [loggedInDict setValue:@"0" forKey:kKeyIsFromFacebook];

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
        [userDefaults synchronize];
    }

    [APP_DELEGATE hideActivityIndicator];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];

    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];

    NSFileManager *fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:loginDataPath]) {
        NSError *error;
        [fileManager removeItemAtPath:loginDataPath error:&error];
    }

    BOOL success = [loggedInDict writeToFile:loginDataPath atomically:YES];

    if (success) {
        addTextInSignificantTxt(@"Login Credential written successfully");
    } else {
        addTextInSignificantTxt(@"Login Credential written Unsuccessfully");
    }

    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"isClearAllRegions"]) {
        for (CLRegion *region in [_locationManager monitoredRegions].allObjects) {
            [_locationManager stopMonitoringForRegion:region];
        }

        BOOL isClearAllRegion = YES;

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        [userDefaults setValue:@(isClearAllRegion) forKey:@"isClearAllRegions"];
        [userDefaults synchronize];
    }
    // [(APP_DELEGATE) cometChatLogin];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (0 == buttonIndex) {

        server.loggedInUserInfo = (NSMutableDictionary *) (APP_DELEGATE).dictUserData;

        NSString *imageName, *imageFilePath;

        if ([server.loggedInUserInfo[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *profileDict = server.loggedInUserInfo[kKeyProfileImage];
            if (profileDict != nil) {
                imageName = profileDict[kKeyImageName];
            }
        }

        if ([imageName length] > 0) {
            imageFilePath = [NSString stringWithFormat:@"%@%@%@", kSerendipityServer, kKeyUserProfileImage, imageName];
        }

        SRCompleteSignUpViewController *completeSignUpCon = [[SRCompleteSignUpViewController alloc] initWithNibName:nil bundle:nil server:server];
        server.userImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageFilePath]]];

        completeSignUpCon.email = [server.loggedInUserInfo valueForKey:@"email"];
        completeSignUpCon.password = self.txtFldPwd.text;

        [self.navigationController pushViewController:completeSignUpCon animated:NO];
    }
}

- (void)loginFailed:(NSNotification *)inNotify {
    [self.btnLogin setEnabled:TRUE];
    [self.view setUserInteractionEnabled:true];

    [APP_DELEGATE hideActivityIndicator];

    if ([[inNotify object] isEqualToString:@"Mobile number is not verified."]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.text", "")
                                                            message:[inNotify object]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

            [self.view bringSubviewToFront:alert];
        });
    } else if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"] && ![[inNotify object] isEqualToString:@"You are already logged in on another device"]) {
        SRSignupViewController *signupViewCon = [[SRSignupViewController alloc] initWithNibName:nil bundle:nil server:server];

        signupViewCon.delegate = self;

        [self.navigationController pushViewController:signupViewCon animated:NO];
    } else {
        [SRModalClass showAlert:[inNotify object]];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];

    if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

@end
