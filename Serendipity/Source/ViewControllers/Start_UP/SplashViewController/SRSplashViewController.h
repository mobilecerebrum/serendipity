#import <UIKit/UIKit.h>
#import "Constant.h"
#import "CBZSplashView.h"
#import "M13ProgressViewBorderedBar.h"
#import "SRServerConnection.h"
#import "ParentViewController.h"

@interface SRSplashViewController : ParentViewController
{
	// Instance variable
	CBZSplashView *splashView;
    
    // Server
    SRServerConnection *server;
}

// Properties
@property (nonatomic, weak) IBOutlet UILabel *lblQuote;
@property (nonatomic, weak) IBOutlet UIImageView *imgViewLargeIcon;
@property (nonatomic, weak) IBOutlet UIView *progressContainer;
@property (nonatomic, weak) IBOutlet M13ProgressViewBorderedBar *progressView;
@property (nonatomic, weak) IBOutlet UILabel *lblProgressCount;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - Public Method
#pragma mark

- (void)pushToPreLoginView;

@end
