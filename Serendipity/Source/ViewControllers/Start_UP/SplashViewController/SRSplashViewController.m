#import "SRPreLoginViewController.h"
#import "SRSplashViewController.h"

@implementation SRSplashViewController

#pragma mark
#pragma mark - Private Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// pushToPreLoginView

- (void)pushToPreLoginView {
    [self.progressView setProgress:1.0 animated:YES];
    self.lblProgressCount.text = @"100%";

    SRPreLoginViewController *preViewcon = [[SRPreLoginViewController alloc] initWithNibName:nil bundle:nil server:server];
    [self.navigationController pushViewController:preViewcon animated:YES];
}

#pragma mark
#pragma mark - Init & Dealloc Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRSplashViewController" bundle:nil];

    if (self) {
        server = inServer;
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super.
    [super viewDidLoad];

    // Hide navigation bar
    self.navigationController.navigationBar.hidden = YES;

    // Localized string for label
    self.lblQuote.text = NSLocalizedString(@"lbl.world.quote", @"");

    UIColor *color = [UIColor clearColor];
    __unused UIImage *icon = [UIImage imageNamed:@"logo.png"];

    splashView = [CBZSplashView splashViewWithIcon:icon backgroundColor:color andManaginViewController:self];
    splashView.animationDuration = 1.2;
    [self.view addSubview:splashView];

    // Bring text to front
    [self.view bringSubviewToFront:self.lblQuote];

    // Progress
    self.progressView.progressDirection = M13ProgressViewBorderedBarProgressDirectionLeftToRight;
    self.progressView.cornerType = M13ProgressViewBorderedBarCornerTypeCircle;
    self.progressView.bounds = CGRectMake(0, 0, 210, 6);

    [self.progressView setProgress:0.44 animated:YES];
    self.lblProgressCount.text = @"74%";

    CGRect frame = self.lblQuote.frame;
    frame.origin.y = self.view.center.y + 30;
    self.lblQuote.frame = frame;

    frame = self.lblProgressCount.frame;
    frame.origin.x = self.progressView.frame.origin.x + 213;
    self.lblProgressCount.frame = frame;
}

// ------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    // Call Super
    [super viewWillAppear:YES];

    // Hide status bar
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

// ------------------------------------------------------------------------------------------------------------------------------
// viewDidAppear:

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [splashView startAnimation];
    });
}

@end
