#import "Constant.h"
#import "SRFetchDataClass.h"
#import "SRModalClass.h"
#import "SRCompleteSignUpViewController.h"
#import "SRLoginViewController.h"
#import "SRTerms&ConditionsViewController.h"
#import "SRSignupViewController.h"

@interface SRSignupViewController () {
    UIImageView *imgView_email;
    
    //UITextField *activeTxtfield;
    
}
@end

@implementation SRSignupViewController

#pragma mark
#pragma mark - Init & Dealloc Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRSignupViewController" bundle:nil];
    
    if (self) {
        // Initialization
        server = inServer;
        
        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(registerUserSucceed:)
                              name:kKeyNotificationRegisterUserSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(registerUserFailed:)
                              name:kKeyNotificationRegisterUserFailed
                            object:nil];
        
        [defaultCenter addObserver:self selector:@selector(keyboardWasShown:)
                              name:UIKeyboardDidShowNotification object:nil];
        
        [defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
        genderOptions = @[@{
                              @"name":@"Male",
                              @"api":@"0",
        },@{
                              @"name":@"Female",
                              @"api":@"1",
        },@{
                              @"name":@"Prefer Not to Say",
                              @"api":@"2",
        }];
        age13Countries = @[@"Belgium",@"Denmark",@"Estonia",@"Finland",@"Iceland",@"Latvia",@"Norway",@"Poland",@"Portugal",@"Spain",@"Sweden",@"United Kingdom"];
        age14Countries = @[@"Austria",@"Bulgaria",@"Cyprus",@"Italy"];
        age15Countries = @[@"Czech Republic",@"France",@"Greece",@"Slovenia"];
        age16Countries = @[@"Croatia",@"Germany",@"Hungary",@"Ireland",@"Liechtenstein",@"Lithuania",@"Luxembourg",@"Malta",@"Romania",@"Slovakia",@"Switzerland",@"Netherland"];
        
    }
    
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupTxtField:(UITextField *)txtfield withImageName:(NSString *)imageName WithIediting:(BOOL)isediting {
    if (txtfield == self.txtFldConfEmail || txtfield == self.txtFldEmail || txtfield == self.txtFldPwd || txtfield == self.txtFldCountry || txtfield == self.txtFldDob || txtfield == self.txtFldGender) {
        UIView* viewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        UIImageView *imgViewemail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [imgViewemail setImage:[UIImage imageNamed:imageName]];
        imgViewemail.contentMode = UIViewContentModeScaleAspectFit;
        imgViewemail.center = viewContainer.center;
        [viewContainer addSubview:imgViewemail];
        txtfield.leftView = viewContainer;
        txtfield.leftViewMode = UITextFieldViewModeAlways;
        
    }
    txtfield.layer.cornerRadius = 5.0;
    
    if (isediting) {
        txtfield.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    } else {
        txtfield.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    }
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];
    // hide confirmation view
    self.customAlertView.hidden = YES;
    
    [self setupTxtField:self.txtFldEmail withImageName:@"signup-email.png" WithIediting:NO];
    
    [self setupTxtField:self.txtFldConfEmail withImageName:@"signup-email.png" WithIediting:NO];
    [self setupTxtField:self.txtFldPwd withImageName:@"signup-password.png" WithIediting:NO];
    [self setupTxtField:self.txtFldFirstName withImageName:nil WithIediting:NO];
    [self setupTxtField:self.txtFldLastName withImageName:nil WithIediting:NO];
    
    //HS:FIX
    [self setupTxtField:self.txtFldDob withImageName:@"icon_dob" WithIediting:NO];
    [self setupTxtField:self.txtFldGender withImageName:@"gender" WithIediting:NO];
    
    
    
    [self setupTxtField:self.txtFldCountry withImageName:@"signup-geography-white.png" WithIediting:NO];
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
    [comps setYear:[comps year] - 100];
    NSDate *hundredYearsAgo = [gregorian dateFromComponents:comps];
    self.pickerView = [[UIDatePicker alloc] init];
    self.pickerView.minimumDate = hundredYearsAgo;
    //    self.pickerView.maximumDate = [NSDate date];
    self.pickerView.datePickerMode = UIDatePickerModeDate;
    
    
    //HS:Gender Picker
    self.genderPickerView = [[UIPickerView alloc] init];
    self.genderPickerView.dataSource = self;
    self.genderPickerView.delegate = self;
    
    
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.signup", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *rightButton = [SRModalClass setRightNavBarItem:NSLocalizedString(@"nav.bar.next", @"") barImage:nil forViewNavCon:self offset:-5];
    [rightButton addTarget:self action:@selector(actionOnNext:) forControlEvents:UIControlEventTouchUpInside];
    
    self.lblAddPic.text = NSLocalizedString(@"lbl.addpic.txt", @"");
    self.lblDetailInfo.text = NSLocalizedString(@"lbldetail.info.txt", @"");
    
    self.btnAddProfilePic.layer.cornerRadius = self.btnAddProfilePic.frame.size.width / 2.0;
    self.btnAddProfilePic.layer.masksToBounds = YES;
    
    UIColor *color = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
    self.txtFldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txtfld.placeholder.email", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldConfEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txtfld.placeholder.confEmail", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldPwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txtfld.placeholder.pwd", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.firstname", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.lastname", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    
    self.txtFldGender.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txtfld.placeholder.gender", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    
    
    self.txtFldDob.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txtfld.placeholder.dob", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"title.country_picker.txt", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.lblPolicyDetail.text = NSLocalizedString(@"lbl.policy.detail", @"");
    
    if (SCREEN_HEIGHT > 667) {
        self.lblPolicyDetail.font = [UIFont fontWithName:kFontHelveticaRegular size:14];
        self.lblDetailInfo.font = [UIFont fontWithName:kFontHelveticaRegular size:15];
    } else {
        self.lblPolicyDetail.font = [UIFont fontWithName:kFontHelveticaRegular size:12];
    }
    //	NSDictionary *detailTxtDict = @{
    //		NSFontAttributeName :[UIFont fontWithName:kFontHelveticaRegular size:13.0],
    //		NSForegroundColorAttributeName : [UIColor whiteColor]
    //	};
    //
    //	NSDictionary *termsTxtDict = @{
    //		NSFontAttributeName :[UIFont fontWithName:kFontHelveticaBold size:13.0],
    //		NSForegroundColorAttributeName : [UIColor whiteColor], NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
    //	};
    
    //	NSString *msgOne = NSLocalizedString(@"lbl.terms.detail.txt", @"");
    //	NSMutableAttributedString *detailTxt = [[NSMutableAttributedString alloc]initWithString:msgOne attributes:detailTxtDict];
    
    
    //	self.lblAcceptTerms.attributedText = (NSAttributedString *)string;
    
    
    
    
    //    self.lblAcceptTerms.numberOfLines = 5;
    //    [self.lblAcceptTerms sizeToFit];
    CGRect myFrame = self.lblAcceptTerms.frame;
    self.lblAcceptTerms.frame = CGRectMake(myFrame.origin.x, myFrame.origin.y, myFrame.size.width, myFrame.size.height);
    self.lblAcceptTerms.delegate = self;
    [self.lblAcceptTerms setTextColor:[UIColor whiteColor]];
    self.lblAcceptTerms.linkAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                           NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                           NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:13.0]};
    
    
    NSRange range1 = [self.lblAcceptTerms.text rangeOfString:NSLocalizedString(@"lbl.terms&condition.text", @"")];
    [self.lblAcceptTerms addLinkToURL:[NSURL URLWithString:@"http://www.serendipityawaits1.com"] withRange:range1];
    
    NSRange range2 = [self.lblAcceptTerms.text rangeOfString:NSLocalizedString(@"lbl.licenseAgreement.text", @"")];
    [self.lblAcceptTerms addLinkToURL:[NSURL URLWithString:@"http://www.serendipityawaits2.com"] withRange:range2];
    
    NSRange range3 = [self.lblAcceptTerms.text rangeOfString:NSLocalizedString(@"lbl.privacyPolicy.text", @"")];
    [self.lblAcceptTerms addLinkToURL:[NSURL URLWithString:@"http://www.serendipityawaits3.com"] withRange:range3];
    
    self.lblAcceptTerms.delegate = self;
    
    
    //    NSRange r = [self.lblAcceptTerms.text rangeOfString:NSLocalizedString(@"lbl.terms&condition.text", @"")];
    //    [self.lblAcceptTerms addLinkToURL:[NSURL URLWithString:@"http://www.serendipityawaits.com"] withRange:r];
    
    
    
    //    [self.contentView addSubview:self.lblAcceptTerms];
    //    [self.view addSubview:self.lblAcceptTerms];
    self.lblAcceptTerms.userInteractionEnabled = YES;
    
    
    //    // Set gesture to dismiss keyboard
    //    UITapGestureRecognizer *lblTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionOnTermsBtn)];
    //    [self.lblAcceptTerms addGestureRecognizer:lblTapGesture];
    
    
    // Set gesture to dismiss keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    //	[self.scrollView addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];
    //    [self.scrollView addSubview:self.lblAcceptTerms];
    [self.lblAcceptTerms bringSubviewToFront:self.scrollView];
    //     [self.contentView bringSubviewToFront: self.lblAcceptTerms];
    
    
    
    // Set values if login using facebook
    if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
        self.txtFldPwd.hidden = TRUE;
        self.txtPwdConstraint.constant = 0.1;
        [self.view layoutIfNeeded];
        
        self.txtFldEmail.text = [server.loggedInFbUserInfo valueForKey:kKeyEmail];
        self.txtFldConfEmail.text = [server.loggedInFbUserInfo valueForKey:kKeyEmail];
        self.txtFldFirstName.text = [server.loggedInFbUserInfo valueForKey:kKeyFirstName];
        self.txtFldLastName.text = [server.loggedInFbUserInfo valueForKey:kKeyLastName];
        self.txtFldEmail.tag = 1;
        self.txtFldConfEmail.tag = 2;
        self.txtFldFirstName.tag = 3;
        self.txtFldLastName.tag = 4;
        self.txtFldDob.tag = 5;
        self.txtFldCountry.tag = 7;
        
        if ([[server.loggedInFbUserInfo valueForKey:kkeyPicture] isKindOfClass:[NSDictionary class]]) {
            if ([[[server.loggedInFbUserInfo valueForKey:kkeyPicture] valueForKey:kkeyData] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *imgDict = [[server.loggedInFbUserInfo valueForKey:kkeyPicture] valueForKey:kkeyData];
                NSString *imageName = [imgDict valueForKey:kkeyUrl];
                
                imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageName]];
                self.imgViewProfilePic.image = [UIImage imageWithData:imageData];
                self.imgViewProfilePic.layer.borderColor = [UIColor clearColor].CGColor;
                self.imgViewProfilePic.layer.borderWidth = 3;
                self.imgViewProfilePic.layer.cornerRadius = self.imgViewProfilePic.frame.size.width / 2.0;
                self.imgViewProfilePic.layer.masksToBounds = YES;
                server.userImage = [UIImage imageWithData:imageData];
            }
        }
    } else {
        self.txtFldEmail.tag = 1;
        self.txtFldConfEmail.tag = 2;
        self.txtFldPwd.tag = 3;
        self.txtFldFirstName.tag = 4;
        self.txtFldLastName.tag = 5;
        self.txtFldDob.tag = 6;
        self.txtFldCountry.tag = 7;
    }
    
    if (SCREEN_WIDTH > 320) {
        // Date picker toolbar button space
        UIBarButtonItem *fixedItemSpaceWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedItemSpaceWidth.width = 255.0f; // or whatever you want
        self.pickerToolBar.items = @[self.cancelBarBtn, fixedItemSpaceWidth, self.doneBarBtn];
    }
    
    //HS:
    //    NSLocale *locale = [NSLocale currentLocale];
    //    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    //
    //    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    //
    //    NSString *country = [usLocale displayNameForKey: NSLocaleCountryCode value: countryCode];
    //    [self.txtFldCountry setText: country ];
    
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    
   // NSLog(@"didSelectLinkWithURL:%@", url);
    
    if ([url isEqual:[NSURL URLWithString:@"http://www.serendipityawaits1.com"]]) {
        SRTerms_ConditionsViewController *termsCon = [[SRTerms_ConditionsViewController alloc] initWithNibName:@"Terms & Policy1" bundle:nil server:server];
        termsCon.delegate = self;
        [self.navigationController pushViewController:termsCon animated:YES];
    } else if ([url isEqual:[NSURL URLWithString:@"http://www.serendipityawaits2.com"]]) {
        SRTerms_ConditionsViewController *termsCon = [[SRTerms_ConditionsViewController alloc] initWithNibName:@"Terms & Policy2" bundle:nil server:server];
        termsCon.delegate = self;
        [self.navigationController pushViewController:termsCon animated:YES];
    } else if ([url isEqual:[NSURL URLWithString:@"http://www.serendipityawaits3.com"]]) {
        SRTerms_ConditionsViewController *termsCon = [[SRTerms_ConditionsViewController alloc] initWithNibName:@"Terms & Policy3" bundle:nil server:server];
        termsCon.delegate = self;
        [self.navigationController pushViewController:termsCon animated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.txtFldDob.inputView = self.pickerView;
    self.txtFldDob.inputAccessoryView = self.pickerToolBar;
    
    self.txtFldGender.inputView = self.genderPickerView;
    self.txtFldGender.inputAccessoryView = self.pickerToolBar;
    
}

#pragma mark - Action Method

- (IBAction)actionOnCountryBtn {
    [APP_DELEGATE hideActivityIndicator];
    
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    
    countryPicker.delegate = self;
    
    [self.navigationController pushViewController:countryPicker animated:YES];
}

#pragma mark - Country Picker Delegates Methods

- (void)pickCountry:(NSDictionary *)countryDict {
    self.isCountrySelected = YES;
    
    if (countryDict && [countryDict count] != 0) {
        countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyId]];
        
        [self.txtFldCountry setText:[countryDict valueForKey:kkeyCountryName]];
        
        self.txtFldCountry.layer.borderColor = [[UIColor clearColor] CGColor];
        self.txtFldCountry.layer.borderWidth = 3;

        
        //        if (self.txtFldGender.text.length == 0){
        //            [self.txtFldGender becomeFirstResponder];
        //        }else if (self.txtFldDob.text.length == 0){
        //            [self.txtFldDob becomeFirstResponder];
        //        }else
        //            if (self.txtFldEmail.text.length == 0){
        //            [self.txtFldEmail becomeFirstResponder];
        //        }else if (self.txtFldConfEmail.text.length == 0){
        //            [self.txtFldConfEmail becomeFirstResponder];
        //        }else if (self.txtFldPwd.text.length == 0){
        //            [self.txtFldPwd becomeFirstResponder];
        //        }else if (self.txtFldFirstName.text.length == 0){
        //            [self.txtFldFirstName becomeFirstResponder];
        //        }else if (self.txtFldLastName.text.length == 0){
        //            [self.txtFldLastName becomeFirstResponder];
        //        }
    }
}

// ----------------------------------------------------------------------------------------------
// editBtnAction:
- (IBAction)editBtnAction:(id)sender {
    self.customAlertView.hidden = YES;
    [self.txtFldFirstName becomeFirstResponder];
}

// ----------------------------------------------------------------------------------------------
// yesBtnAction:
- (IBAction)yesBtnAction:(id)sender {
    self.customAlertView.hidden = YES;
    [self.view endEditing:YES];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
        params[kkeySocialId] = [server.loggedInFbUserInfo valueForKey:kKeyId];
        params[kkeyLoginType] = @"facebook";
    } else {
        params[kkeySocialId] = @"";
        params[kkeyLoginType] = @"applogin";
    }
    params[kKeyEmail] = self.txtFldEmail.text;
    params[kKeyPassword] = self.txtFldPwd.text;
    params[kKeyFirstName] = self.txtFldFirstName.text;
    params[kKeyLastName] = self.txtFldLastName.text;
    params[kKeyDOB] = self.txtFldDob.text;
    params[kKeyCountryId] = countryMasterId;
    params[kKeyGender] = self.userGender;
    
    NSString *deviceToken = server.deviceToken ? server.deviceToken: @"";
    params[kKeyDeviceToken] = deviceToken;
    params[kKeyDeviceType] = kKeyDeviceName;
    params[kDeviceId] = [APP_DELEGATE getDeviceId];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *dateOfBirth = [dateFormatter dateFromString:params[kKeyDOB]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    params[kKeyDOB] = [dateFormatter stringFromDate:dateOfBirth];
    
    
    NSDate *now = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:dateOfBirth
                                       toDate:now
                                       options:0];
    int currentAge = (int)[ageComponents year];
    
    if (![APP_DELEGATE isRestrictedAge:currentAge forCountry:countryMasterId]) {
        // Call Api
        [server asychronousRequestWithData:params imageData:imageData forKey:kKeyProfileImage
                               toClassType:kKeyClassRegisterUser inMethodType:kPOST];
    } else {
        [SRModalClass showAlert:@"Sorry, this app is unavailable to you based upon your age and country."];
    }
}
// ----------------------------------------------------------------------------------------------
// actionOnBack

- (void)actionOnBack {
    // Pop controller
    [server.loggedInFbUserInfo removeAllObjects];
    [self.navigationController popToViewController:self.delegate animated:NO];
}


- (void)actionOnNext:(id)sender {
    // save button action
    [self.view endEditing:YES];
    server.userImage = [UIImage imageWithData:imageData];
    // Save image data
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationProfileImageCached object:imageData];
    
    errMsg = [self validateFields];
    if (errMsg) {
        if ([errMsg isEqualToString:NSLocalizedString(@"empty.dob.text", @"")] || [errMsg isEqualToString:NSLocalizedString(@"alert.minimumRegister_age.text", "")]) {
            UIAlertView *validateAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:errMsg delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
            validateAlert.tag = 1;
            [validateAlert show];
        } else {
            [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(showAlert:) userInfo:nil repeats:NO];
        }
    } else {
        if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
            NSString *email = [server.loggedInFbUserInfo valueForKey:kKeyEmail];
            if (email.length == 0) {
                email = _txtFldEmail.text;
                [server.loggedInFbUserInfo setValue:email forKey:kKeyEmail];
            }
        }
        [self.view endEditing:YES];
        self.customAlertView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 150);
        self.confirmMsgLbl.text = NSLocalizedString(@"alert.signup_info_confirmation.txt", @"");
        self.editInfoBtn.layer.cornerRadius = 5;
        self.editInfoBtn.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.editInfoBtn.layer.borderWidth = 1.0;
        self.editInfoBtn.layer.masksToBounds = YES;
        
        self.doneInfoBtn.layer.cornerRadius = 5;
        self.doneInfoBtn.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.doneInfoBtn.layer.borderWidth = 1.0;
        self.doneInfoBtn.layer.masksToBounds = YES;
        
        self.customAlertView.hidden = NO;
        [self.view addSubview:self.customAlertView];
    }
}

- (void)showAlert:(id)sender {
    UIAlertView *validateAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:errMsg delegate:nil cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
    //validateAlert.tag = 2;
    [validateAlert show];
}

//
// -----------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [self.txtFldDob becomeFirstResponder];
    }
}
// -------------------------------------------------------------------------------------------------------
// pickerButtonDone:

- (IBAction)pickerButtonDone:(id)sender {
    
    if (self.pickerToolBar.tag == 0){
        [self setupTxtField:self.txtFldDob withImageName:@"icon_dob" WithIediting:NO];
        
        // Hide picker view and set the value
        // update date in textfield
        NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:self.pickerView.date];
        NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:destinationGMTOffset sinceDate:self.pickerView.date];
        NSString *formattedDate = [SRModalClass returnStringInMMDDYYYY:destinationDate];
        NSDate *selectedDate = [SRModalClass returnDateInMMDDYYYY:formattedDate];
        
        NSString *today = [SRModalClass returnStringInMMDDYYYY:[NSDate date]];
        NSDate *todayDate = [SRModalClass returnDateInMMDDYYYY:today];
        
        if ([selectedDate compare:todayDate] == NSOrderedDescending) {
            
            [SRModalClass showAlert:NSLocalizedString(@"alert.wrong_birthdate.txt", @"")];
            
        } else if ([selectedDate compare:todayDate] == NSOrderedAscending || [selectedDate compare:todayDate] == NSOrderedSame) {
            
            self.txtFldDob.attributedText = [[NSAttributedString alloc] initWithString:formattedDate attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:13.0]}];
        }
        self.txtFldDob.layer.borderColor = [[UIColor clearColor] CGColor];
        self.txtFldDob.layer.borderWidth = 3;
    }else{
        self.txtFldGender.attributedText = [[NSAttributedString alloc] initWithString:[[genderOptions objectAtIndex:[self.genderPickerView selectedRowInComponent:0]] valueForKey:@"name"] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:13.0]}];
        
        self.userGender = [[genderOptions objectAtIndex:[self.genderPickerView selectedRowInComponent:0]] valueForKey:@"api"];
        
        self.txtFldGender.layer.borderColor = [[UIColor clearColor] CGColor];
        self.txtFldGender.layer.borderWidth = 3;
        [self setupTxtField:self.txtFldGender withImageName:@"gender" WithIediting:NO];

    }
    [self.view endEditing:YES];
}

// -------------------------------------------------------------------------------------------------------
// pickerButtonCancel:
- (IBAction)pickerButtonCancel:(id)sender {
    
    if (self.pickerToolBar.tag == 0){
        [self setupTxtField:self.txtFldDob withImageName:@"icon_dob" WithIediting:NO];
    }else{
        [self setupTxtField:self.txtFldDob withImageName:@"gender" WithIediting:NO];
    }
    [self.view endEditing:YES];
}

// -------------------------------------------------------------------------------------------------------
// actionOnBtnClick:

- (IBAction)actionOnBtnClick:(UIButton *)sender {
    // End editting
    [self.view endEditing:YES];
    if (sender == self.btnAddProfilePic) {
        UIActionSheet *actionSheet = nil;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            // Present action sheet
            actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), NSLocalizedString(@"camera.text", ""), nil];
        } else {
            // Present action sheet
            actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "")
                                        destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), nil];
        }
        // Show sheet
        [actionSheet showFromRect:[sender frame] inView:self.view animated:YES];
    } else if (sender == self.btnCheckMark) {
        if ([self.btnCheckMark isSelected]) {
            [self.btnCheckMark setSelected:NO];
            [self.btnCheckMark setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
        } else {
            self.btnCheckMark.layer.borderColor = [[UIColor clearColor] CGColor];
            [self.btnCheckMark setSelected:YES];
            [self.btnCheckMark setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFldEmail) {
        [self.txtFldEmail resignFirstResponder];
        [self setupTxtField:self.txtFldEmail withImageName:@"signup-email.png" WithIediting:NO];
    }else if (textField == self.txtFldConfEmail) {
        [self.txtFldConfEmail resignFirstResponder];
        [self setupTxtField:self.txtFldConfEmail withImageName:@"signup-email.png" WithIediting:NO];
    } else if (textField == self.txtFldPwd) {
        [self.txtFldPwd resignFirstResponder];
        [self setupTxtField:self.txtFldPwd withImageName:@"signup-password.png" WithIediting:NO];
    } else if (textField == self.txtFldFirstName) {
        [self.txtFldFirstName resignFirstResponder];
    } else if (textField == self.txtFldLastName) {
        [self.txtFldLastName resignFirstResponder];
        
    } else if (textField == self.txtFldDob || textField == self.txtFldCountry) {
        [textField resignFirstResponder];
    }
    textField.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    // Return
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        if ([nextResponder isKindOfClass:[UITextField class]]) {
            [nextResponder becomeFirstResponder];
        }
        
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}


// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    self.customAlertView.hidden = YES;
    activeField = sender;
    
    [self setupTxtField:self.txtFldEmail withImageName:@"signup-email.png" WithIediting:NO];
    [self setupTxtField:self.txtFldConfEmail withImageName:@"signup-email.png" WithIediting:NO];
    [self setupTxtField:self.txtFldPwd withImageName:@"signup-password.png" WithIediting:NO];
    [self setupTxtField:self.txtFldDob withImageName:@"icon_dob" WithIediting:NO];
    [self setupTxtField:self.txtFldGender withImageName:@"gender" WithIediting:NO];
    
    
    if (sender == self.txtFldEmail) {
        [self setupTxtField:self.txtFldEmail withImageName:@"signup-email-orange.png" WithIediting:YES];
    }else if (sender == self.txtFldDob){
        self.pickerToolBar.tag = 0;
        [self setupTxtField:self.txtFldDob withImageName:@"icon_dob-orange" WithIediting:NO];
    } else if (sender == self.txtFldConfEmail) {
        [self setupTxtField:self.txtFldConfEmail withImageName:@"signup-email-orange.png" WithIediting:YES];
    } else if (sender == self.txtFldPwd) {
        [self setupTxtField:self.txtFldPwd withImageName:@"signup-password-orange.png" WithIediting:YES];
    }else if (sender == self.txtFldGender){
        self.pickerToolBar.tag = 1;
        [self setupTxtField:self.txtFldGender withImageName:@"gender_orange" WithIediting:NO];
    }
    sender.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    BOOL isValid = YES;
    textField.layer.borderColor = [[UIColor clearColor] CGColor];
    NSString *errorMsg = nil;
    NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];
    if (textField == self.txtFldFirstName || textField == self.txtFldLastName) {
        if (textField == self.txtFldFirstName && [textStrToCheck length] > 25) {
            errorMsg = NSLocalizedString(@"firstname.char.error", @"");
        } else if (textField == self.txtFldLastName && [textStrToCheck length] > 25) {
            errorMsg = NSLocalizedString(@"lastname.char.error", @"");
        }
    }
    // Set flag and show alert
    if (errorMsg) {
        isValid = NO;
        [SRModalClass showAlert:errorMsg];
    }
    
    // Return
    return isValid;
}

// --------------------------------------------------------------------------------
//textFieldDidEndEditing
- (void)textFieldDidEndEditing:(UITextField *)textField {
    textField.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    activeField = nil;
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin)) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark
#pragma mark ActionSheet Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// clickedButtonAtIndex:

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Instantiate image picker and set delegate
    UIImagePickerController *imagePickerCon = [[UIImagePickerController alloc] init];
    imagePickerCon.delegate = self;
    imagePickerCon.allowsEditing = YES;
    [imagePickerCon setModalPresentationStyle:UIModalPresentationFullScreen];
    
    if (buttonIndex == 0) {
        imagePickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        CATransition *transition = [CATransition animation];
        transition.duration = 1;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self presentViewController:imagePickerCon animated:NO completion:nil];
    } else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePickerCon.sourceType = UIImagePickerControllerSourceTypeCamera;
        CATransition *transition = [CATransition animation];
        transition.duration = 1;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        
        [self presentViewController:imagePickerCon animated:NO completion:nil];
    }
}

#pragma mark
#pragma mark Image Picker Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// didFinishPickingMediaWithInfo:

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // Selection of image done set it to button
    UIImage *selectedImage = [info valueForKey:UIImagePickerControllerEditedImage];
    self.imgViewProfilePic.image = selectedImage;
    imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    
    self.imgViewProfilePic.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgViewProfilePic.layer.borderWidth = 3;
    self.imgViewProfilePic.layer.cornerRadius = self.imgViewProfilePic.frame.size.width / 2.0;
    self.imgViewProfilePic.layer.masksToBounds = YES;
    // Dismiss picker
    [picker dismissViewControllerAnimated:NO completion:nil];
}

// --------------------------------------------------------------------------------
// imagePickerControllerDidCancel:

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark
#pragma mark Touches Event Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // End editing
    [self.view endEditing:YES];
}


// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
    // End editing
    [self textFieldShouldReturn:activeField];
    [self.customAlertView setHidden:YES];
}
// --------------------------------------------------------------------------------
// hideKeyboard

- (void)actionOnTermsBtn {
    SRTerms_ConditionsViewController *termsCon = [[SRTerms_ConditionsViewController alloc] initWithNibName:@"Terms of Service, Mobile App End User License Agreement & Privacy Policy" bundle:nil server:server];
    termsCon.delegate = self;
    [self.navigationController pushViewController:termsCon animated:YES];
}

// --------------------------------------------------------------------------------
// validateFields

- (NSString *)validateFields {
    errMsg = nil;
    // Check for fields
    NSString *email = self.txtFldEmail.text;
    NSString *confEmail = self.txtFldConfEmail.text;
    NSString *dob = self.txtFldDob.text;
    NSString *password = [self.txtFldPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *firstName = [self.txtFldFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *lastName = [self.txtFldLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // Get age of user
    NSDate *now = [NSDate date];
    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:self.pickerView.date
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    
    self.txtFldDob.layer.borderColor = [[UIColor clearColor] CGColor];
    self.txtFldDob.layer.borderWidth = 3;
    self.txtFldConfEmail.layer.borderColor = [[UIColor clearColor] CGColor];
    self.txtFldConfEmail.layer.borderWidth = 3;
    self.txtFldEmail.layer.borderColor = [[UIColor clearColor] CGColor];
    self.txtFldEmail.layer.borderWidth = 3;
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"] invertedSet];
    
    // User name validation
    if (errMsg == nil && countryMasterId == nil) {
        errMsg = NSLocalizedString(@"empty.country.alert", "");
        self.txtFldCountry.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldCountry.layer.borderWidth = 3;
    }else if([self.txtFldGender.text length] == 0){
        errMsg = NSLocalizedString(@"empty.gender.alert", "");
        self.txtFldGender.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldGender.layer.borderWidth = 3;
    }else if([self.txtFldDob.text length] == 0){
        errMsg = NSLocalizedString(@"empty.dob.text", "");
        self.txtFldDob.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldDob.layer.borderWidth = 3;
    }else if ([email length] == 0) {
        errMsg = NSLocalizedString(@"empty.email.error", "");
        [self.txtFldEmail becomeFirstResponder];
        self.txtFldEmail.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldEmail.layer.borderWidth = 3;
    } else if ([email length] > 0 && ![SRModalClass NSStringIsValidEmail:email]) {
        errMsg = NSLocalizedString(@"email.validation.alert", "");
        self.txtFldEmail.layer.borderColor = [[UIColor redColor] CGColor];
        [self.txtFldEmail becomeFirstResponder];
    } else if ([confEmail length] == 0 && errMsg == nil) {
        errMsg = NSLocalizedString(@"empty.emailConfirm.error", "");
        [self.txtFldConfEmail becomeFirstResponder];
        self.txtFldConfEmail.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldConfEmail.layer.borderWidth = 3;
    } else if (![email isEqualToString:confEmail]) {
        errMsg = NSLocalizedString(@"alert.email.match.text", "");
        [self.txtFldConfEmail becomeFirstResponder];
        self.txtFldConfEmail.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldConfEmail.layer.borderWidth = 3;
        self.txtFldEmail.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldEmail.layer.borderWidth = 3;
    } else if (![[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"] && [password length] < 8 && errMsg == nil) {
        errMsg = NSLocalizedString(@"lbl.pwd.hint", "");
        [self.txtFldPwd becomeFirstResponder];
        self.txtFldPwd.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldPwd.layer.borderWidth = 3;
    } else if (errMsg == nil && [firstName length] == 0) {
        errMsg = NSLocalizedString(@"empty.firstname.text", "");
        [self.txtFldFirstName becomeFirstResponder];
        self.txtFldFirstName.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldFirstName.layer.borderWidth = 3;
    }
    //    else if ([firstName rangeOfCharacterFromSet:set].location != NSNotFound)
    //    {
    //        errMsg = NSLocalizedString(@"special_char.firstname.text", "");
    //        [self.txtFldFirstName becomeFirstResponder];
    //        self.txtFldFirstName.layer.borderColor = [[UIColor redColor]CGColor];
    //        self.txtFldFirstName.layer.borderWidth = 3;
    //    }
    else if (errMsg == nil && [lastName length] == 0) {
        errMsg = NSLocalizedString(@"empty.lastname.text", "");
        [self.txtFldLastName becomeFirstResponder];
        self.txtFldLastName.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldLastName.layer.borderWidth = 3;
    }
    //    else if ([lastName rangeOfCharacterFromSet:set].location != NSNotFound)
    //    {
    //        errMsg = NSLocalizedString(@"special_char.lastname.text", "");
    //        [self.txtFldLastName becomeFirstResponder];
    //        self.txtFldLastName.layer.borderColor = [[UIColor redColor]CGColor];
    //        self.txtFldLastName.layer.borderWidth = 3;
    //    }
    else if (errMsg == nil && [dob length] == 0) {
        errMsg = NSLocalizedString(@"empty.dob.text", "");
        self.txtFldDob.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldDob.layer.borderWidth = 3;
    } else if (age < 13) {
        errMsg = NSLocalizedString(@"alert.minimumRegister_age.text", "");
        self.txtFldDob.layer.borderColor = [[UIColor redColor] CGColor];
        self.txtFldDob.layer.borderWidth = 3;
    } else if (errMsg == nil && ![self.btnCheckMark isSelected]) {
        self.btnCheckMark.layer.borderColor = [[UIColor redColor] CGColor];
        self.btnCheckMark.layer.borderWidth = 3;
        errMsg = NSLocalizedString(@"terms.condition.alert", "");
    } else if (imageData.length == 0) {
        errMsg = NSLocalizedString(@"empty.profilePicture.error", "");
        self.imgViewProfilePic.layer.masksToBounds = YES;
        self.imgViewProfilePic.layer.borderColor = [UIColor redColor].CGColor;
        self.imgViewProfilePic.layer.borderWidth = 3;
    }
    // Return error msg
    return errMsg;
}

#pragma mark
#pragma mark Notification Method
#pragma mark

- (void)registerUserSucceed:(NSNotification *)inNotify {
    // Navigate to code genrator view
    [APP_DELEGATE hideActivityIndicator];
    server.userImage = [UIImage imageWithData:imageData];
    SRCompleteSignUpViewController *completeSignUpCon = [[SRCompleteSignUpViewController alloc] initWithNibName:nil bundle:nil server:server];
    completeSignUpCon.email = self.txtFldEmail.text;
    completeSignUpCon.password = self.txtFldPwd.text;
    self.delegate = self;
    [self.navigationController pushViewController:completeSignUpCon animated:NO];
}

// --------------------------------------------------------------------------------
// registerUserFailed:

- (void)registerUserFailed:(NSNotification *)inNotify {
    NSString *error = [inNotify object];
    [SRModalClass showAlert:error];
    [APP_DELEGATE hideActivityIndicator];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [genderOptions count] ;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[genderOptions objectAtIndex:row] valueForKey:@"name"];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //    _userAge = row ;
    //    _userAgeText = [genderOptions objectAtIndex:row];
}
@end
