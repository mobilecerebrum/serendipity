#import <UIKit/UIKit.h>
#import "SRServerConnection.h"
#import "SRMenuViewController.h"
#import "SRAccountSettingsViewController.h"
#import "SRProfileImageView.h"
#import "SRAddConnectionTabViewController.h"

@interface SRCodeViewController : ParentViewController<UIAlertViewDelegate>

{
    // Instance variable
    NSDictionary *signUpData;
    UITextField *activeField;
    // Server
    SRServerConnection *server;
}

// Property
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblSecondMsg;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode1;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode2;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode3;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode4;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode5;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCode6;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (strong, nonatomic) IBOutlet SRProfileImageView *BackGroundPhotoImage;
@property (strong,nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *countryCode,*sms_reference_code,*password,*email,*countryMasterId;

@property BOOL codeFlag;
@property BOOL isUpdateDeviceToken;
@property BOOL backNavigationFlag;


// Others
@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
                 data:(NSDictionary *)inData;


@end
