#import "SRModalClass.h"
#import "SRCodeViewController.h"
#import "SRLoginViewController.h"

#define MAX_LENGTH 1


@implementation SRCodeViewController

#pragma mark
#pragma mark Private Method
#pragma mark
// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
    // End editing
    [self.view endEditing:YES];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark
#pragma mark - Init & Dealloc Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
                 data:(NSDictionary *)inData {
    // Call super
    self = [super initWithNibName:@"SRCodeViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
        signUpData = inData;

        // Register notification
        [[NSNotificationCenter defaultCenter] removeObserver:self];

        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        
        
        [defaultCenter addObserver:self
                          selector:@selector(codeVerificationSucceed:)
                              name:kKeyNotificationUserDeviceVerifySuccess
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(codeVerificationFailed:)
                              name:kKeyNotificationUserDeviceVerifyFailed
                            object:nil];
        
        
        
        [defaultCenter addObserver:self
                          selector:@selector(codeVerificationSucceed:)
                              name:kKeyNotificationVerifyCodeSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(codeVerificationFailed:)
                              name:kKeyNotificationVerifyCodeFailed
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(getUserDetailsSucceed:)
                              name:kKeyNotificationCompleteSignUPSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getUserDetailsFailed:)
                              name:kKeyNotificationCompleteSignUPFailed object:nil];
        [defaultCenter addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];

        [defaultCenter addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    }
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];
     
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(actionOnBack) forControlEvents:UIControlEventTouchUpInside];

    if (_isUpdateDeviceToken){
        self.lblFirstMsg.text = NSLocalizedString(@"lbl.firstemail.message.txt", @"");
        self.lblSecondMsg.text = NSLocalizedString(@"lbl.secondemail.message.txt", @"");
        [SRModalClass setNavTitle:self.email forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }else{
        self.lblFirstMsg.text = NSLocalizedString(@"lbl.first.message.txt", @"");
        self.lblSecondMsg.text = NSLocalizedString(@"lbl.second.message.txt", @"");
        if (self.codeFlag) {
            // Mobile number seperation by 3 letters
            NSRange range = NSMakeRange(0, 3);
            NSString *strPartOne = self.phoneNumber;
            if (strPartOne.length > 10) {
                strPartOne = [strPartOne substringWithRange:range];

                NSString *strPartTwo = [self.phoneNumber substringWithRange:NSMakeRange(3, 3)];
                NSString *strPartThree = [self.phoneNumber substringWithRange:NSMakeRange(6, 4)];;
                strPartOne = [NSString stringWithFormat:@"+%@(%@) %@ %@", self.countryCode, strPartOne, strPartTwo, strPartThree];
            } else
                strPartOne = [NSString stringWithFormat:@"+%@%@", self.countryCode, strPartOne];
            // Title
            [SRModalClass setNavTitle:strPartOne forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        } else {
            NSString *strPartOne = [SRModalClass formatPhoneNumber:signUpData[kKeyMobileNumber] deleteLastChar:NO];
            if (signUpData && signUpData[kKeyCountry] && [signUpData[kKeyCountry] objectForKey:kKeyPhoneCode]) {
                strPartOne = [NSString stringWithFormat:@"+%@ %@", [signUpData[kKeyCountry] objectForKey:kKeyPhoneCode], strPartOne];
            }
            // Title
            [SRModalClass setNavTitle:strPartOne forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        }
    }
    

    // hide confirmation view
    self.navigationItem.hidesBackButton = YES;

    // Right Bar button
    UIButton *rightButton = [SRModalClass setRightNavBarItem:NSLocalizedString(@"nav.bar.next", @"") barImage:nil forViewNavCon:self offset:-5];
    [rightButton addTarget:self action:@selector(actionOnNext:) forControlEvents:UIControlEventTouchUpInside];

    

    // Set gesture to dismiss keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];

    // Become first responder
    [self.txtFldCode1 becomeFirstResponder];
    [self.txtFldCode1 setTag:1];
    [self.txtFldCode2 setTag:2];
    [self.txtFldCode3 setTag:3];
    [self.txtFldCode4 setTag:4];
    [self.txtFldCode5 setTag:5];
    [self.txtFldCode6 setTag:6];
}

// -----------------------------------------------------------------------------------------------
// viewWillDisappear
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    // Dealloc all register notifications
    //[[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark
#pragma mark Action Method
#pragma mark

// ----------------------------------------------------------------------------------------------
// actionOnBack

- (void)actionOnBack {
    [self.navigationController popViewControllerAnimated:YES];
}
// ----------------------------------------------------------------------------------------------
// actionOnNext:

- (void)actionOnNext:(id)sender {
    // Save button action
    [self.view endEditing:YES];
    // NSString *errMsg = nil;

    if ([[self.txtFldCode1 text] length] == 0 ||
            [[self.txtFldCode2 text] length] == 0 ||
            [[self.txtFldCode3 text] length] == 0 ||
            [[self.txtFldCode4 text] length] == 0 ||
            [[self.txtFldCode5 text] length] == 0 ||
            [[self.txtFldCode6 text] length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.enter.correctCode", @"")];
    } else {
        // Call server api
        NSString *codeStr = [NSString stringWithFormat:@"%@%@%@%@%@%@", [self.txtFldCode1 text], [self.txtFldCode2 text], [self.txtFldCode3 text], [self.txtFldCode4 text], [self.txtFldCode5 text], [self.txtFldCode6 text]];
        
        NSString *deviceToken = server.deviceToken ? server.deviceToken: @"";
        
        if (_isUpdateDeviceToken){
            NSDictionary *params = @{
                           kKeydeviceVerifyCode: codeStr,
                           deviceReferenceID: self.sms_reference_code,
                           kDeviceId : [APP_DELEGATE getDeviceId],
                           kKeyDeviceToken: deviceToken,
                           kKeyDeviceType: kKeyDeviceName,
                           @"dvc_ver_type" : @"0",
                       };
                       
            NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassVerifydeviceCode];
            [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        }else{
            NSDictionary *params = @{
                kKeyVerifyCode: codeStr,
                smsReferenceID: self.sms_reference_code,
                kDeviceId : [APP_DELEGATE getDeviceId],
                kKeyDeviceToken: deviceToken,
                kKeyDeviceType: kKeyDeviceName,
            };
            
            if (self.codeFlag) {
                 NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassVerifyPhoneCode];
                [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
            }else{
                NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassVerifyCode];
                [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
            }
        }
        
    }
}

#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldShouldReturn:
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // Set flag and show alert
    if (textField == self.txtFldCode1) {
        [self.txtFldCode1 resignFirstResponder];
        [self.txtFldCode2 becomeFirstResponder];
    } else if (textField == self.txtFldCode2) {
        [self.txtFldCode2 resignFirstResponder];
        [self.txtFldCode3 becomeFirstResponder];
    } else if (textField == self.txtFldCode3) {
        [self.txtFldCode3 resignFirstResponder];
        [self.txtFldCode4 becomeFirstResponder];
    } else if (textField == self.txtFldCode4) {
        [self.txtFldCode4 resignFirstResponder];
        [self.txtFldCode5 becomeFirstResponder];
    } else if (textField == self.txtFldCode5) {
        [self.txtFldCode5 resignFirstResponder];
        [self.txtFldCode6 becomeFirstResponder];
    } else if (textField == self.txtFldCode6) {
        [self.txtFldCode6 resignFirstResponder];
    }

    // Return
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
    if (activeField.tag == 6) {
        if (self.txtFldCode5.text.length == 0) {
            [self.txtFldCode5 becomeFirstResponder];
        } else {
            [self.txtFldCode6 becomeFirstResponder];
        }
    }
    if (activeField.tag == 5) {
        if ([activeField isEqual:self.txtFldCode5] && self.txtFldCode6.text.length > 0) {
            [self.txtFldCode5 resignFirstResponder];
        } else if (self.txtFldCode4.text.length == 0) {
            [self.txtFldCode4 becomeFirstResponder];
        } else {
            [self.txtFldCode5 becomeFirstResponder];
        }
    }
    if (activeField.tag == 4) {
        if ([activeField isEqual:self.txtFldCode4] && self.txtFldCode5.text.length > 0) {
            [self.txtFldCode4 resignFirstResponder];
        } else if (self.txtFldCode3.text.length == 0) {
            [self.txtFldCode3 becomeFirstResponder];
        } else {
            [self.txtFldCode4 becomeFirstResponder];
        }
    }
    if (activeField.tag == 3) {
        if ([activeField isEqual:self.txtFldCode3] && self.txtFldCode4.text.length > 0) {
            [self.txtFldCode3 resignFirstResponder];
        } else if (self.txtFldCode2.text.length == 0) {
            [self.txtFldCode2 becomeFirstResponder];
        } else {
            [self.txtFldCode3 becomeFirstResponder];
        }
    }
    if (activeField.tag == 2) {
        if ([activeField isEqual:self.txtFldCode2] && self.txtFldCode3.text.length > 0) {
            [self.txtFldCode2 resignFirstResponder];
        } else if (self.txtFldCode1.text.length == 0) {
            [self.txtFldCode1 becomeFirstResponder];
        } else {
            [self.txtFldCode2 becomeFirstResponder];
        }
    }
    if (activeField.tag == 1) {
        if ([activeField isEqual:self.txtFldCode1] && self.txtFldCode2.text.length > 0) {
            [self.txtFldCode1 resignFirstResponder];
        } else {
            [self.txtFldCode1 becomeFirstResponder];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeField = nil;
}
// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (string.length == 0 && textField.text.length) {
        [self performSelector:@selector(setPreviousResponder:) withObject:textField afterDelay:0.0];
        return NO;
    } else {
        //textField.text=@"";
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;

        // This 'tabs' to next field when entering digits
        if (oldLength == 1) {
            if (textField == self.txtFldCode1) {
                [self setNextResponder:self.txtFldCode2 andStr:string];
            } else if (textField == self.txtFldCode2) {
                [self setNextResponder:self.txtFldCode3 andStr:string];
            } else if (textField == self.txtFldCode3) {
                [self setNextResponder:self.txtFldCode4 andStr:string];
            } else if (textField == self.txtFldCode4) {
                [self setNextResponder:self.txtFldCode5 andStr:string];
            } else if (textField == self.txtFldCode5) {
                [self setNextResponder:self.txtFldCode6 andStr:string];
            } else if (textField == self.txtFldCode6) {
                [self.txtFldCode6 resignFirstResponder];
            }
        }
        // Return
        return newLength <= 1;
    }
    return NO;
}

// --------------------------------------------------------------------------------
// setPreviousResponder:
- (void)setPreviousResponder:(UITextField *)prevResponder {
    [prevResponder resignFirstResponder];
    if ([prevResponder isEqual:self.txtFldCode6]) {
        self.txtFldCode6.text = @"";
        [self.txtFldCode5 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode5]) {
        self.txtFldCode5.text = @"";
        [self.txtFldCode4 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode4]) {
        self.txtFldCode4.text = @"";
        [self.txtFldCode3 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode3]) {
        self.txtFldCode3.text = @"";
        [self.txtFldCode2 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode2]) {
        self.txtFldCode2.text = @"";
        [self.txtFldCode1 becomeFirstResponder];
    } else if ([prevResponder isEqual:self.txtFldCode1]) {
        self.txtFldCode1.text = @"";
        [self.txtFldCode1 becomeFirstResponder];
    }
}

// --------------------------------------------------------------------------------
// setNextResponder:
- (void)setNextResponder:(UITextField *)nextResponder andStr:(NSString *)str {
    [nextResponder becomeFirstResponder];
    if (nextResponder.text.length == 0) {
        nextResponder.text = str;
    }
}

# pragma
# pragma NOtification methods
# pragma
//
//----------------------------------------------------------------
//alertview:clickedButtonAtIndex

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self callLoginForRegisterAPI];
        }
    }
}
-(void)callLoginForRegisterAPI{
    
    
    // Call login api
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *loggedInDict = [[NSMutableDictionary alloc] init];
    [loggedInDict setValue:server.loggedInFbUserInfo[kKeyEmail] forKey:kKeyEmail];
    [loggedInDict setValue:[server.loggedInFbUserInfo valueForKey:kKeyId] forKey:kKeyId];
    [loggedInDict setValue:[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] forKey:kKeyIsFromFacebook];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
    [userDefaults synchronize];

    if ([[server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
        NSDictionary *params = @{kKeyEmail: [server.loggedInFbUserInfo valueForKey:kKeyEmail], kkeySocialId: [server.loggedInFbUserInfo valueForKey:kKeyId], kkeyLoginType: @"facebook", kkeyIsLogin: @"1", kKeyDeviceToken: server.deviceToken,kDeviceId : [APP_DELEGATE getDeviceId], kKeyDeviceType: kKeyDeviceName, @"add_info": [(APP_DELEGATE) getAdditionalInfo]};
        [server makeAsychronousRequest:kKeyClassLogin inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    } else {
        NSMutableDictionary *loggedInDict = [[NSMutableDictionary alloc] init];
        [loggedInDict setValue:self.email forKey:kKeyMobileNumber];
        [loggedInDict setValue:self.password forKey:kKeyPassword];
        [loggedInDict setValue:[[server.loggedInUserInfo valueForKey:kKeyCountry] valueForKey:kKeyId] forKey:kKeyCountryMasterId];
        [loggedInDict setValue:@"0" forKey:kKeyIsFromFacebook];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
        [userDefaults synchronize];
        
        NSString * deviceToken = server.deviceToken ? server.deviceToken : @"";

        NSDictionary *params = @{
                                 kKeyEmail: self.email,
                                 kKeyPassword: self.password,
                                 kKeyCountryId: [[server.loggedInUserInfo valueForKey:kKeyCountry] valueForKey:kKeyId],
                                 kkeyIsLogin: @"1",
                                 kKeyDeviceToken: deviceToken,
                                 kKeyDeviceType: kKeyDeviceName,
                                 kDeviceId : [APP_DELEGATE getDeviceId],
                                 @"add_info": [(APP_DELEGATE) getAdditionalInfo]
                                };
        [server makeAsychronousRequest:kKeyClassLogin inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    }
    
}


// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:
- (void)getUserDetailsSucceed:(NSNotification *)inNotify {
    // Display Data
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// getUserDetailsFailed:

- (void)getUserDetailsFailed:(NSNotification *)inNotify {
    // Display Data
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// codeVerificationSucceed:

- (void)codeVerificationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationVerifyCodeSucceed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationVerifyCodeFailed object:nil];

    // To add setting dictionary in response
    NSDictionary *payload = @{
            kKeyFieldName: kkeyBroadcastLocation,
            kKeyFieldValue: @"1"
    };
    NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
    [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];

    if (self.codeFlag) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *loginDataPath = paths[0];
        loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:loginDataPath]) {
            NSError *error;
            NSDictionary *theDict = [NSDictionary dictionaryWithContentsOfFile:loginDataPath];
            if (theDict[@"mobile_number"]) {
                [theDict setValue:self.phoneNumber forKey:@"mobile_number"];
                [fileManager removeItemAtPath:loginDataPath error:&error];
                // Write file to path
                BOOL success = [theDict writeToFile:loginDataPath atomically:YES];
                if (success)
                    addTextInSignificantTxt(@"Login Credential written successfully");
                else
                    addTextInSignificantTxt(@"Login Credential written Unsuccessfully");
            }
        }
        server.loggedInUserInfo[kKeyMobileNumber] = self.phoneNumber;
        server.loggedInUserInfo[kKeyCountryMasterId] = self.countryMasterId;
        
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[SRAccountSettingsViewController class]]) {
                [self.navigationController popToViewController:controller animated:NO];
            }
        }
    } else {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setValue:@"no" forKey:kKeyIsImportPhoneContact];
        [userDefault synchronize];
        [self callLoginForRegisterAPI];
//        [APP_DELEGATE setInitialController];
//        if (self.isUpdateDeviceToken){
//            [APP_DELEGATE setInitialController];
//        }else{
//
//            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            [userDefault setValue:@"no" forKey:kKeyIsImportPhoneContact];
//            [userDefault synchronize];
//
//            /*Contact Flow start from here*/
//
//            UIAlertView *contactAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:NSLocalizedString(@"alert.contact_import.text", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
//            contactAlert.tag = 1;
//            [contactAlert show];
//
//            (APP_DELEGATE).isSignUpSuccess = YES;
//
//            SRAddConnectionTabViewController *newConn = [[SRAddConnectionTabViewController alloc] initWithNibName:kKeyIsFromSignUp bundle:nil];
//                   self.navigationController.hidesBottomBarWhenPushed = NO;
//                   [self.navigationController pushViewController:newConn animated:NO];
//                   self.navigationController.hidesBottomBarWhenPushed = NO;
//        }
    }
}

//--------------------------------------------------------------------------------
// codeVerificationFailed:

- (void)codeVerificationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationVerifyCodeSucceed object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKeyNotificationVerifyCodeFailed object:nil];
    [SRModalClass showAlert:[inNotify object]];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification *)aNotification {
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [info[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin)) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification *)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}
@end
