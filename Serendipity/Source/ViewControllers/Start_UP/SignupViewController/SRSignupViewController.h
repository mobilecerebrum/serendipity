#import <UIKit/UIKit.h>
#import "SRServerConnection.h"
#import "TTTAttributedLabel.h"
#import "SRCountryPickerViewController.h"

@interface SRSignupViewController : ParentViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate,UIAlertViewDelegate,TTTAttributedLabelDelegate, CountryPickerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{

    NSString *countryMasterId;
    NSData *imageData;
    NSString *errMsg ;
    // Server
    SRServerConnection *server;
    UITextField *activeField;
    NSArray *genderOptions;
    NSArray *age13Countries;
    NSArray *age14Countries;
    NSArray *age15Countries;
    NSArray *age16Countries;



    // Flag
    BOOL isAccpeted;
}

@property (weak, nonatomic) IBOutlet UITextField *txtFldCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtFldGender;

@property (nonatomic, assign) BOOL isCountrySelected;
// Properties
@property (strong, nonatomic) IBOutlet UIView *customAlertView;
@property (strong, nonatomic) IBOutlet UILabel *confirmLbl;
@property (strong, nonatomic) IBOutlet UILabel *confirmMsgLbl;
@property (strong, nonatomic) IBOutlet UIButton *editInfoBtn;
@property (strong, nonatomic) IBOutlet UIButton *doneInfoBtn;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewBG;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewProfilePic;
@property (strong, nonatomic) IBOutlet UIButton *btnAddProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *lblAddPic;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *lblDetailInfo;
@property (strong, nonatomic) IBOutlet UITextField *txtFldEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFldConfEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFldPwd;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *txtPwdConstraint;

@property (strong, nonatomic) IBOutlet UITextField *txtFldDob;
@property (strong, nonatomic) IBOutlet UITextField *txtFldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtFldLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblPolicyDetail;

@property (strong, nonatomic) IBOutlet UIButton *btnCheckMark;
@property (strong, nonatomic) IBOutlet TTTAttributedLabel *lblAcceptTerms;
@property (strong, nonatomic)  UIPickerView *genderPickerView;

@property (strong, nonatomic)  UIDatePicker *pickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneBarBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lbllblAcceptTermsConstraint;

@property (nonatomic, assign) NSString *userGender;

// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnBtnClick:(UIButton *)sender;
- (IBAction)pickerButtonDone:(id)sender;
- (IBAction)pickerButtonCancel:(id)sender;

@end
