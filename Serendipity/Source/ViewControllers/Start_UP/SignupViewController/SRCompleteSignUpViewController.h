#import <UIKit/UIKit.h>
#import "SRServerConnection.h"
#import "SRCountryPickerViewController.h"
#import "SRLoginViewController.h"

@interface SRCompleteSignUpViewController : ParentViewController<CountryPickerDelegate,CLLocationManagerDelegate,UIAlertViewDelegate>

{
	// Instance Variable
	NSDictionary *signUpData;

    NSString *countryMasterId;
    
	// Server
	SRServerConnection *server;
}

// Properties
@property (strong, nonatomic) NSString *password,*email;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBg;
@property (strong, nonatomic) UILabel *lblPhoneCode;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckCountry;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewProfile;
@property (strong, nonatomic) IBOutlet UIImageView *imgVIewCountry;
@property (strong, nonatomic) IBOutlet UITextField *txtFldCountry;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckPhNo;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewPhNo;
@property (strong, nonatomic) IBOutlet UITextField *txtFldPhNo;
@property (strong, nonatomic) IBOutlet UIButton *countryBtn;

// Phone number confirmation view
@property (strong, nonatomic) IBOutlet UIView *confirmationView;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblisCorrectNum;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, assign) BOOL isCountrySelected;
@property (nonatomic, strong) CLLocation *currentLocation;

// Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
