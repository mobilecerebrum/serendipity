#import "SRModalClass.h"
#import "SRHomeParentViewController.h"
#import "SRCompleteSignUpViewController.h"
#import "SRCodeViewController.h"

@implementation SRCompleteSignUpViewController

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
    // End editing
    [self.view endEditing:YES];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}



// --------------------------------------------------------------------------------
// validateFields

- (BOOL)validateFields {
    NSString *errMsg = nil;

    // Check for fields
    NSString *countryName = self.txtFldCountry.text;
    NSString *phoneNo = self.txtFldPhNo.text;

    // User name validation
    if ([countryName length] == 0 && [phoneNo length] == 0) {
        errMsg = NSLocalizedString(@"alert.allempty.fields.text", "");
        [SRModalClass showAlert:errMsg];
        return NO;
    } else if ([phoneNo length] < 4) {
        errMsg = NSLocalizedString(@"telephone.no.lenght.error", "");
        [SRModalClass showAlert:errMsg];
        return NO;
    } else if ([countryName length] == 0) {
        errMsg = NSLocalizedString(@"empty.country.alert", "");
        [SRModalClass showAlert:errMsg];
        [self.txtFldCountry becomeFirstResponder];
        return NO;
    } else if ([phoneNo length] == 0) {
        errMsg = NSLocalizedString(@"empty.emailfld.error", "");
        [SRModalClass showAlert:errMsg];
        [self.txtFldPhNo becomeFirstResponder];
        return NO;
    } else
        return YES;
}

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRCompleteSignUpViewController" bundle:nil];
    if (self) {
        // Initialization
        server = inServer;

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(signUPUserSucceed:)
                              name:kKeyNotificationSignUPSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(signUPUserFailed:)
                              name:kKeyNotificationSignUPFailed
                            object:nil];
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // hide confirmation view
    self.navigationItem.hidesBackButton = YES;
    self.confirmationView.hidden = YES;
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.signup", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *rightButton = [SRModalClass setRightNavBarItem:NSLocalizedString(@"barbtn.done.txt", @"") barImage:nil forViewNavCon:self offset:-5];
    [rightButton addTarget:self action:@selector(actionOnDone:) forControlEvents:UIControlEventTouchUpInside];

    self.imgViewBckCountry.layer.cornerRadius = 5.0;
    self.imgViewBckPhNo.layer.cornerRadius = 5.0;

    UIColor *color = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
    self.txtFldCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"title.country_picker.txt", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtFldPhNo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"txt.placeholder.phone", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];


    if (server.userImage != nil) {
        self.imgViewProfile.image = server.userImage;
    } else {
        self.imgViewBg.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    }
    // Set gesture to dismiss keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];

    //Get Current Location
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    if ([APP_DELEGATE isLocationTutorialDone]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    self.currentLocation = [[CLLocation alloc] init];
}

// -----------------------------------------------------------------------------------------------
// viewWillAppear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (server.userImage != nil) {
        self.imgViewProfile.image = server.userImage;
    } else {
        self.imgViewBg.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    }

    //Call Location Manager to get Current Location
    if (!self.isCountrySelected && self.txtFldCountry.text.length == 0) {

//        [(APP_DELEGATE) showActivityIndicator];
        [self.locationManager startUpdatingLocation];
    }

}

// ----------------------------------------------------------------------------------------------
// editBtnAction:
- (IBAction)editBtnAction:(id)sender {
    [self.confirmationView removeFromSuperview];
    [self.txtFldPhNo becomeFirstResponder];
}

// ----------------------------------------------------------------------------------------------
// yesBtnAction:
- (IBAction)yesBtnAction:(id)sender {
    self.confirmationView.hidden = YES;
    [self.view endEditing:YES];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[kKeyCountryId] = countryMasterId;
    params[kKeyMobileNumber] = [SRModalClass RemoveSpecialCharacters:self.txtFldPhNo.text];
    
    NSString *deviceToken = server.deviceToken ? server.deviceToken: @"";
    params[kKeyDeviceToken] = deviceToken;
    params[kKeyDeviceType] = kKeyDeviceName;
    params[kDeviceId] = [APP_DELEGATE getDeviceId];

    // Call api
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassSendVerifyCode];
    [server makeSynchronousRequest:urlStr inParams:params inMethodType:kPOST isIndicatorRequired:YES];
    //[server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}
// ----------------------------------------------------------------------------------------------
// actionOnDone:

- (void)actionOnDone:(id)sender {
    if ([self validateFields]) {
        [self.view endEditing:YES];
        self.confirmationView.hidden = NO;
        self.confirmationView.frame = CGRectMake((self.view.frame.size.width / 2) - (self.confirmationView.frame.size.width / 2), (self.view.frame.size.height / 2) - (self.confirmationView.frame.size.height / 2), self.confirmationView.frame.size.width,
                self.confirmationView.frame.size.height);
        [self.confirmationView setCenter:CGPointMake(CGRectGetMidX(self.view.bounds), self.confirmationView.frame.origin.y + self.confirmationView.frame.size.height / 2)];

        self.lblNewPhoneNumber.text = [[self.lblPhoneCode.text stringByAppendingString:@" "] stringByAppendingString:self.txtFldPhNo.text];
        self.confirmationView.layer.cornerRadius = 5;
        self.confirmationView.layer.masksToBounds = YES;
        [self.view addSubview:self.confirmationView];
    }
}

- (IBAction)actionOnCountryBtn {
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}

#pragma mark
#pragma mark LocationManager Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// didChangeAuthorizationStatus:

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
        }
            break;

        case kCLAuthorizationStatusDenied: {
        }
            break;

        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [self.locationManager startUpdatingLocation];
        }
        case kCLAuthorizationStatusAuthorizedAlways: {
            [self.locationManager startUpdatingLocation]; //Will update location immediately
        }
            break;

        default:
            break;
    }
}

// --------------------------------------------------------------------------------
// didUpdateLocations:

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.currentLocation = locations[0];
    [self.locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:self.currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (!(error)) {
            CLPlacemark *placemark = placemarks[0];
            if (![placemark.country isKindOfClass:(id) [NSNull class]] || placemark.country.length != 0) {
                [(APP_DELEGATE) hideActivityIndicator];
                NSArray *countryArray = server.countryArr;
                for (NSDictionary *dict in countryArray) {
                    if ([[dict valueForKey:kkeyCountryName] isEqualToString:placemark.country]) {

                        countryMasterId = [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]];

                        [self.txtFldCountry setText:placemark.country];

                        // Set search icon to textfield
                        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
                        container.backgroundColor = [UIColor clearColor];

                        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
                        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
                        self.lblPhoneCode.textColor = [UIColor whiteColor];

                        if (dict && [dict valueForKey:kKeyPhoneCode]) {
                            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[dict valueForKey:kKeyPhoneCode]]];

                        }

                        [container addSubview:self.lblPhoneCode];

                        self.txtFldPhNo.leftView = container;
                        self.txtFldPhNo.leftViewMode = UITextFieldViewModeAlways;
                    }

                }
                if (self.txtFldCountry.text.length == 0) {
                    [self displayDefaultCountry];
                }

            } else {
                [self displayDefaultCountry];
            }
        } else {
            [self displayDefaultCountry];
        }
    }];

}

// --------------------------------------------------------------------------------
// didFailWithError:

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [(APP_DELEGATE) hideActivityIndicator];
    [self displayDefaultCountry];
}

#pragma mark Private Method

- (void)displayDefaultCountry {
    self.txtFldCountry.text = @"United States";
    countryMasterId = @"226";
    // Set search icon to textfield
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
    container.backgroundColor = [UIColor clearColor];

    self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
    self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
    self.lblPhoneCode.textColor = [UIColor whiteColor];
    [self.lblPhoneCode setText:@"+1"];
    [container addSubview:self.lblPhoneCode];

    self.txtFldPhNo.leftView = container;
    self.txtFldPhNo.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark
#pragma mark Country Picker Delegates Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// pickCountry:
- (void)pickCountry:(NSDictionary *)countryDict {
    if ([countryDict count] != 0) {
        countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyId]];

        [self.txtFldCountry setText:[countryDict valueForKey:kkeyCountryName]];

        // Set search icon to textfield
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
        container.backgroundColor = [UIColor clearColor];

        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 45, 30)];
        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaRegular size:13.0];
        self.lblPhoneCode.textColor = [UIColor whiteColor];

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];
        }

        [container addSubview:self.lblPhoneCode];

        self.txtFldPhNo.leftView = container;
        self.txtFldPhNo.leftViewMode = UITextFieldViewModeAlways;
    }
}


#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFldPhNo) {
        [self.txtFldPhNo resignFirstResponder];
    }
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    if (sender == self.txtFldCountry) {
        [self.scrollView setContentOffset:CGPointMake(0, 140)];
        self.imgViewBckCountry.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        self.imgViewBckPhNo.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        [self.imgVIewCountry setImage:[UIImage imageNamed:@"signup-geography-orange.png"]];
        [self.imgViewPhNo setImage:[UIImage imageNamed:@"signup-cellphone-white.png"]];
    } else if (sender == self.txtFldPhNo) {
        [self.scrollView setContentOffset:CGPointMake(0, 140)];
        self.imgViewBckCountry.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        self.imgViewBckPhNo.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        [self.imgVIewCountry setImage:[UIImage imageNamed:@"signup-geography-white.png"]];
        [self.imgViewPhNo setImage:[UIImage imageNamed:@"signup-cellphone-orange.png"]];
    }
}

// --------------------------------------------------------------------------------
// textFieldDidEndEditing:
- (void)textFieldDidEndEditing:(UITextField *)sender {
    [self.imgVIewCountry setImage:[UIImage imageNamed:@"signup-geography-white.png"]];
    [self.imgViewPhNo setImage:[UIImage imageNamed:@"signup-cellphone-white.png"]];
}
// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL isValid = YES;
    NSString *errorMsg = nil;
    NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];

    if ([textStrToCheck length] > 17) {
        errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
    } else if (errorMsg == nil) {
        BOOL flag = [SRModalClass numberValidation:string];
        textField.text = [SRModalClass formatPhoneNumber:self.txtFldPhNo.text deleteLastChar:NO];
        if (!flag) {
            errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
        }
    }
    // Set flag and show alert
    if (errorMsg) {
        isValid = NO;
        [SRModalClass showAlert:errorMsg];
    }

    // Return
    return isValid;
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// signUPUserSucceed:

- (void)signUPUserSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSMutableDictionary *responceDict = [inNotify object];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:server.loggedInUserInfo];

    SRCodeViewController *phoneCodeViewCon = [[SRCodeViewController alloc] initWithNibName:nil bundle:nil server:server data:params];
    phoneCodeViewCon.email = self.email;
    phoneCodeViewCon.password = self.password;
    phoneCodeViewCon.sms_reference_code = [[responceDict valueForKey:kKeyUser] valueForKey:smsReferenceID];
    [self.navigationController pushViewController:phoneCodeViewCon animated:NO];
}

// --------------------------------------------------------------------------------
// signUPUserFailed:

- (void)signUPUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    if ([[inNotify object] isEqualToString:@"The mobile number has already been taken."]) {
        UIAlertView *phoneAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", @"") message:[inNotify object] delegate:self cancelButtonTitle:NSLocalizedString(@"Edit", @"") otherButtonTitles:NSLocalizedString(@"Login", @""), nil];
        phoneAlert.tag = 1;
        [phoneAlert show];
    } else {
        [SRModalClass showAlert:[inNotify object]];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            // Go to Login Screen
            SRLoginViewController *loginViewCon = [[SRLoginViewController alloc] initWithNibName:nil bundle:nil server:server];
            [self.navigationController setViewControllers:@[loginViewCon] animated:YES];
        }
    }
}

@end
