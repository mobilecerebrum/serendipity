//
//  TripDetailTableCell.h
//  Serendipity
//
//  Created by Jonish Sangwan on 28/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TripDetailTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeCapturedLocation;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UILabel *speedLbl;

@end

NS_ASSUME_NONNULL_END
