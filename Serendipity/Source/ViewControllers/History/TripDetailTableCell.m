//
//  TripDetailTableCell.m
//  Serendipity
//
//  Created by Jonish Sangwan on 28/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "TripDetailTableCell.h"

@implementation TripDetailTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
