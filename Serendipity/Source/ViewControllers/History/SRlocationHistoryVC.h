//
//  SRlocationHistoryVC.h
//  Serendipity
//
//  Created by Jonish Sangwan on 14/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import "SRCompassWindow.h"
#import "SRMapProfileView.h"
#import "SRServerConnection.h"
#import "MarkerDefaultCell.h"
#import "MarkerLiveTracking.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRlocationHistoryVC : UIViewController<GMSMapViewDelegate,UIAlertViewDelegate>
{
    GMSMarker *dropMarker;
    GMSMarker *selectedMarker;
    NSMutableDictionary *userDictionary;
    NSDictionary *userDataDict;
    MarkerDefaultCell *compassWindow;
    MarkerLiveTracking *liveTrackingInfoView;
    NSString *adressStr;
    BOOL isAdded,isPanoramaLoaded;
    BOOL fromEventOrGroup;
    NSMutableAttributedString * wholeString;
    IBOutlet UIButton *streetViewBtn;
    IBOutlet UIButton *btnSatelite;
    IBOutlet UIButton *btnStreet;
    CLLocationCoordinate2D panoramaLastLoc;
    float delayTime;
    BOOL isDropped;
    NSNumber *zoomLevel;
    float distanceFilter;
    SRServerConnection *server;
    BOOL isViewUp;
    NSDictionary *locationHistory;
    NSMutableArray *locationHistoryArray;
    NSMutableArray *selcetedIndexArr;
    UIToolbar* toolbar;
    UIDatePicker* picker;
    NSString *selectedDate;
    NSString *userID;
    NSInteger selectedTableSection;
    CGSize lblHeight;
    BOOL liveBtnSelected;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *locationListView;
@property (weak, nonatomic) IBOutlet UIButton *upBtnOutlet;
@property (weak, nonatomic) IBOutlet UIView *upperView;
@property (weak, nonatomic) IBOutlet UIImageView *viewBgOutlet;
@property (weak, nonatomic) IBOutlet UILabel *selectedDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *liveBtnClicked;
@property (weak, nonatomic) IBOutlet UIView *datePickView;
@property (weak,nonatomic) IBOutlet UIButton *zoomInBtn;
@property (weak,nonatomic) IBOutlet UIButton *zoomOutBtn;
@property (weak, nonatomic) IBOutlet UILabel *noDataFoundLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationViewTop;

- (id)initWithNibName:(NSString *)nibNameOrNil
bundle:(NSBundle *)nibBundleOrNil
server:(SRServerConnection *)inServer;

@end
NS_ASSUME_NONNULL_END
