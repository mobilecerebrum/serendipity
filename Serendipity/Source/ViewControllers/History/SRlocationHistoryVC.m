//
//  SRlocationHistoryVC.m
//  Serendipity
//
//  Created by Jonish Sangwan on 14/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "SRlocationHistoryVC.h"
#import "SRModalClass.h"
#import "SRPanoramaViewController.h"
#import "FrequencyUpdateView.h"
#import "TripDetailTableCell.h"
#import "HeaderViewCell.h"

#define infoWidth  250.0
#define infoHeight 100

//@interface SRlocationHistoryVC ()
//
//@end

@implementation SRlocationHistoryVC

#pragma mark Init Method

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRlocationHistoryVC" bundle:nibBundleOrNil];
    if (self) {
        server = inServer;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    server = (APP_DELEGATE).server;
    isViewUp = liveBtnSelected = true;
    locationHistoryArray = [[NSMutableArray alloc] init];
    selcetedIndexArr = [[NSMutableArray alloc] init];
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(locationHistorySucceed:)
                          name:kKeylocationHistorySucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(locationHistoryFailure:)
                          name:kKeylocationHistoryFailure object:nil];
    // Do any additional setup after loading the view from its nib.
//    (APP_DELEGATE).topBarView.hidden = YES;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    // Title
    if (fromEventOrGroup) {
        [SRModalClass setNavTitle:[NSString stringWithFormat:@"%@", [userDictionary valueForKey:kKeyName]] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    } else {
        [SRModalClass setNavTitle:[NSString stringWithFormat:@"%@ %@", [userDictionary valueForKey:kKeyFirstName], [userDictionary valueForKey:kKeyLastName]] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }


    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnBack) forControlEvents:UIControlEventTouchUpInside];

    _mapView.myLocationEnabled = YES;
    _mapView.settings.myLocationButton = YES;
//    _mapView.padding = UIEdgeInsetsMake(64, 0, 0, SCREEN_WIDTH/2-30);//Comment By Sunil for issue On didTapMarker pin goes to left side (minus coordinates)

    //[streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    // Title
    userID = @"721";//(APP_DELEGATE).selectedConnection[@"id"];
    [SRModalClass setNavTitle:NSLocalizedString(@"View Location History", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    liveTracking(self);
    [_datePickView setHidden:YES];
}

static void getLocationHistory(SRlocationHistoryVC *object, NSString *userId, NSString *date) {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = userId;
    params[kkeyCurrentDate] = date;
    params[@"page"] = @"0";
    [object->server makeAsychronousRequest:kkeyLocationHistory inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

static void drawStrokePath(SRlocationHistoryVC *object) {
    GMSMutablePath *path = [GMSMutablePath path];
    [path addCoordinate:CLLocationCoordinate2DMake(29.9695, 76.8783)];
    [path addCoordinate:CLLocationCoordinate2DMake(30.3752, 76.7821)];
    [path addCoordinate:CLLocationCoordinate2DMake(30.7191, 76.7487)];
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeWidth = 10.f;
    polyline.geodesic = YES;
    UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];
    polyline.strokeColor = appOrangeColor;
    polyline.map = object.mapView;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _locationViewTop.constant = self.view.frame.size.height - 105;
    drawStrokePath(self);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:panoramaLastLoc.latitude longitude:panoramaLastLoc.longitude zoom:6];
        [self.mapView animateToCameraPosition:camera];
    }
    UIPanGestureRecognizer * pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    pan.maximumNumberOfTouches = 99;
    pan.minimumNumberOfTouches = 1;
    [self.upperView addGestureRecognizer:pan];
}

#pragma mark - Action

- (IBAction)upDownBtnAction:(id)sender {
    
    if(isViewUp){
        _locationViewTop.constant = 50;
        isViewUp = false;
    }else{
        _locationViewTop.constant = self.view.frame.size.height - 105;
        isViewUp = true;
    }
    _upBtnOutlet.frame = CGRectMake(_locationListView.frame.size.width/2 - 40,_locationListView.frame.origin.y - 45, 80 ,45);
}

    - (void)pan:(UIPanGestureRecognizer *)aPan; {
        CGPoint currentPoint = [aPan locationInView:self.view];
        CGFloat ht = 105;
        float nh = self.view.frame.size.height-currentPoint.y;
        [UIView animateWithDuration:0.01f animations:^{
            if(currentPoint.y > 105 && currentPoint.y < self.view.frame.size.height - ht){
                _locationViewTop.constant = currentPoint.y;
            }
        }];
        if (nh == ht) {
            isViewUp = false;
        }else
        {
            isViewUp = true;
        }
        _upBtnOutlet.frame = CGRectMake(_locationListView.frame.size.width/2 - 40,_locationListView.frame.origin.y - 45, 80 ,45);

}

- (IBAction)showDatePicker:(id)sender {
    picker = [[UIDatePicker alloc] init];
    picker.backgroundColor = [UIColor whiteColor];
    [picker setValue:[UIColor blackColor] forKey:@"textColor"];

    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    picker.datePickerMode = UIDatePickerModeDate;

    [picker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
    picker.frame = CGRectMake(0.0, [UIScreen mainScreen].bounds.size.height - 300, [UIScreen mainScreen].bounds.size.width, 300);
    [self.view addSubview:picker];

    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 300, [UIScreen mainScreen].bounds.size.width, 50)];
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(onDoneButtonClick)]];
    [toolbar sizeToFit];
    [self.view addSubview:toolbar];
}

-(void) dueDateChanged:(UIDatePicker *)sender {

    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    _selectedDateLbl.text = [dateFormatter stringFromDate:[sender date]];

    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [sender date];
    NSString *dateString = [dateFormatter stringFromDate:date];
    selectedDate = dateString;
}

-(void)onDoneButtonClick {
    [toolbar removeFromSuperview];
    [picker removeFromSuperview];
    getLocationHistory(self, userID, selectedDate);
}

static void liveTracking(SRlocationHistoryVC *object) {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    NSDate *date = [NSDate date];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    object->_selectedDateLbl.text = [dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    object->selectedDate = dateString;
    getLocationHistory(object, object->userID, object->selectedDate);
}

- (IBAction)liveBtnClicked:(UIButton*)button {
    
    button.selected = !button.selected;
    if(button.selected){
        liveBtnSelected = true;
        [_datePickView setHidden:NO];
        [_liveBtnClicked setHidden:YES];
    }else{
        liveTracking(self);
        liveBtnSelected = true;
        [_liveBtnClicked setHidden:NO];
        [_datePickView setHidden:YES];
    }
    [self displayData];
}

- (IBAction)actionOnBtnClick:(UIButton *)inSender {
    if (inSender.tag == 0) {
        self.mapView.mapType = kGMSTypeSatellite;
        [btnStreet setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    } else {
        self.mapView.mapType = kGMSTypeNormal;
        [btnStreet setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}


// --------------------------------------------------------------------------------
// zoomInMap
- (IBAction)zoomInMap {
    [_mapView animateToZoom:12];
}

- (IBAction)zoomOutMap {
    [_mapView animateToZoom:2];
}
#pragma mark - GMSMapView Delegate and Datasource

// --------------------------------------------------------------------------------
// didTapMarker:
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {

    //change pin location from center to left side
    CGPoint point = [_mapView.projection pointForCoordinate:marker.position];
    //point.x = point.x + SCREEN_WIDTH / 2 - 20;
    GMSCameraUpdate *camera =
            [GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
    [_mapView animateWithCameraUpdate:camera];
        selectedMarker = marker;
    NSDictionary *markerData = marker.userData;
        CGPoint markerPoint = [self.mapView.projection pointForCoordinate:marker.position];
        CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    lblHeight = [self findHeightForText:markerData[@"live_address"] havingWidth:infoWidth - infoWidth/3 andFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [self updateInfoWindowPosition:pointInViewCoords];
    if(liveBtnSelected){
        liveTrackingInfoView.address.text = markerData[@"live_address"];
        NSString *speed = [NSString stringWithFormat:@"%@%@", markerData[@"speed"], @"mph"];
        liveTrackingInfoView.speed.text = speed;
        liveTrackingInfoView.name.text = (APP_DELEGATE).selectedConnection[@"name"];
        
        // User image
        if ([(APP_DELEGATE).selectedConnection[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([(APP_DELEGATE).selectedConnection[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [(APP_DELEGATE).selectedConnection[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [liveTrackingInfoView.image sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    liveTrackingInfoView.image.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                liveTrackingInfoView.image.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            liveTrackingInfoView.image.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            [self.mapView addSubview:liveTrackingInfoView];
    }else{
        compassWindow.address.text = markerData[@"live_address"];
        NSString *str = [NSString stringWithFormat:@"%@%@", markerData[@"coords_accuracy"], @"Feet"];
        NSString *speed = [NSString stringWithFormat:@"%@%@", markerData[@"speed"], @"mph"];
        compassWindow.acurancy.text = str;
        compassWindow.time.text = markerData[@"location_captured_time"];
        compassWindow.speed.text = speed;
        compassWindow.battrySetting.text = markerData[@"battery_setting"];
            [self.mapView addSubview:compassWindow];
    }
    
    return YES;
}

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

// --------------------------------------------------------------------------------
// didChangeCameraPosition:

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    [self updateInfoWindowPosition:pointInViewCoords];
}

// --------------------------------------------------------------------------------
// idleAtCameraPosition:

// This method gets called whenever the map was moving but has now stopped

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    [self updateInfoWindowPosition:pointInViewCoords];
}

// --------------------------------------------------------------------------------
// didTapAtCoordinate:

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    if (selectedMarker) {
        //selectedMarker.icon = [self markerImageForUser]; //[UIImage imageNamed:@"radar-pin-orange-30px"];
    }
}

- (void)updateInfoWindowPosition:(CGPoint )pointMaker {
    
    if(liveBtnSelected){
        lblHeight.height = lblHeight.height - 30;
    }else{
        lblHeight.height = lblHeight.height;
    }
    CGRect frame = CGRectMake((SCREEN_WIDTH - infoWidth)/2, (SCREEN_HEIGHT - infoHeight)/2, infoWidth, infoHeight + lblHeight.height);
    if(liveBtnSelected){
        if (liveTrackingInfoView == nil) {
            liveTrackingInfoView = [[[NSBundle mainBundle] loadNibNamed:@"MarkerLiveTracking" owner:self options:nil] objectAtIndex:0];
        } else {
            liveTrackingInfoView.frame = frame;
        }
        liveTrackingInfoView.center = CGPointMake(pointMaker.x, pointMaker.y - infoHeight/2 - 26);
    }else{
        if (compassWindow == nil) {
            compassWindow = [[[NSBundle mainBundle] loadNibNamed:@"MarkerDefaultCell" owner:self options:nil] objectAtIndex:0];
        } else {
            compassWindow.frame = frame;
        }
        compassWindow.center = CGPointMake(pointMaker.x, pointMaker.y - infoHeight/2 - 26);
    }
     //30 is icon maker height
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}

- (void)displayData {
    // Animate map to that position
    if (locationHistoryArray.count > 0) {
        NSDictionary *locationHistoryDict = locationHistoryArray[selectedTableSection];
        NSMutableArray *locArr = locationHistoryDict[@"data"];
        NSDictionary *locDict = locArr.firstObject;
        if ([locDict valueForKey:kKeyLattitude] != nil && [locDict valueForKey:kKeyRadarLong] != nil) {
            CLLocationCoordinate2D coordinate = {[[locDict valueForKey:kKeyLattitude] floatValue], [[locDict valueForKey:kKeyRadarLong] floatValue]};
            if (!isDropped) {
                [self.mapView clear];
                
                GMSMutablePath *path = [GMSMutablePath path];
                for(int i=0;i<locArr.count;i++){
                    NSDictionary *obj = locArr[i];
                    if ([obj valueForKey:kKeyLattitude] != nil && [obj valueForKey:kKeyRadarLong] != nil) {
                        CLLocationCoordinate2D coordinateLatest = {[[obj valueForKey:kKeyLattitude] floatValue], [[obj valueForKey:kKeyRadarLong] floatValue]};
                        [path addCoordinate:coordinateLatest];
                        dropMarker = [[GMSMarker alloc] init];
                        dropMarker.position = coordinateLatest;
                        dropMarker.title = [[userDictionary[kKeyFirstName] stringByAppendingString:@" "] stringByAppendingString:userDictionary[kKeyLastName]];
                        dropMarker.userData = obj;
                        dropMarker.appearAnimation = kGMSMarkerAnimationPop;
                        dropMarker.map = _mapView;
                        _mapView.delegate = self;
                        
                    }
                }
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth = 10.f;
                polyline.geodesic = YES;
                UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];
                polyline.strokeColor = appOrangeColor;
                polyline.map = self.mapView;
                
                CLLocationCoordinate2D coordinate = {[[locDict valueForKey:kKeyLattitude] floatValue], [[locDict valueForKey:kKeyRadarLong] floatValue]};
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:14];
                [self.mapView animateToCameraPosition:camera];
                
                // Create current selected marker
                FrequencyUpdateView *__updateMarker = [[FrequencyUpdateView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_HEIGHT > 800) ? 80 : 43, (SCREEN_HEIGHT > 800) ? 80 : 43)];
                [__updateMarker updateFrequencyWithUserDict:userDictionary];
                [__updateMarker layoutIfNeeded];
                
            } else {
                [CATransaction begin];
                [CATransaction setAnimationDuration:4.0];
                [self.mapView animateToLocation:coordinate];
                
                FrequencyUpdateView *__updateMarker = [[FrequencyUpdateView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_HEIGHT > 800) ? 80 : 43, (SCREEN_HEIGHT > 800) ? 80 : 43)];
                [__updateMarker updateFrequencyWithUserDict:userDictionary];
                
                dropMarker = [[GMSMarker alloc] init];
                dropMarker.position = coordinate;
                dropMarker.title = [[userDictionary[kKeyFirstName] stringByAppendingString:@" "] stringByAppendingString:userDictionary[kKeyLastName]];
                dropMarker.userData = userDictionary;
                dropMarker.position = coordinate;
                [CATransaction commit];
                [__updateMarker layoutIfNeeded];
            }
        }
    }
}

// actionOnBack
- (void)actionOnBack {
    // Pop controller
    // [self.tabBarController.tabBar setHidden:NO];
    //[_updateTimer invalidate];
    //[self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:NO];
    [self.navigationController popViewControllerAnimated:NO];

}

#pragma mark TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        if(locationHistoryArray.count == 0){
            _noDataFoundLbl.hidden = NO;
            [self.mapView clear];
            
        }else{
            _noDataFoundLbl.hidden = YES;
        }
    return locationHistoryArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *s = [NSString stringWithFormat:@"%ld", (long)section];
    if([selcetedIndexArr containsObject: s]){
        NSMutableArray *capturedTimeSlot = locationHistoryArray[section][@"data"];
        return capturedTimeSlot.count;
    }else{
        return 2;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TripDetailTableCell *tripTblCell = [tableView dequeueReusableCellWithIdentifier:@"tripDetailTableCell"];
    NSMutableArray *capturedTimeSlot = locationHistoryArray[indexPath.section][@"data"];
    NSMutableArray *filteredArr = [[NSMutableArray alloc] init];
    NSString *s = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
    if([selcetedIndexArr containsObject: s]){
        filteredArr = capturedTimeSlot;
    }else{
        [filteredArr addObject:[capturedTimeSlot firstObject]];
        [filteredArr addObject:[capturedTimeSlot lastObject]];
    }
    NSDictionary *obj = filteredArr[indexPath.row];
    tripTblCell.timeCapturedLocation.text = obj[@"location_captured_time"];
    tripTblCell.locationLbl.text = obj[@"live_address"];
    NSString *str = [NSString stringWithFormat:@"%@%@", obj[@"coords_accuracy"], @"Feet"];
    tripTblCell.distance.text = str;
    NSString *speed = [NSString stringWithFormat:@"%@%@", obj[@"speed"], @"mph"];
    tripTblCell.speedLbl.text = speed;
    [self displayData];
    return tripTblCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HeaderViewCell *headerView = [[[NSBundle mainBundle] loadNibNamed:@"HeaderViewCell" owner:self options:nil] objectAtIndex:0];
    NSDictionary *capturedTimeSlot = locationHistoryArray[section];
    NSString *inStr = [NSString stringWithFormat: @"%ld", (long)section];
    [headerView.tripIndexOutlet setTitle:inStr forState:UIControlStateNormal];
    headerView.timeSlotLbl.text = capturedTimeSlot[@"session"];
    NSInteger i = [capturedTimeSlot[@"distance"] integerValue];
    NSString *s = [NSString stringWithFormat:@"%ld", (long)i];
    NSString *speed = [NSString stringWithFormat:@"%@%@%@", s,@"m .", capturedTimeSlot[@"time"]];
    headerView.distanceTimeLbl.text = speed;
    headerView.didSelectBtnOutlet.tag = section;
    [headerView.didSelectBtnOutlet addTarget:self action:@selector(actionOnSelectTableCell:) forControlEvents:UIControlEventTouchUpInside];
    return headerView;
}

- (void)actionOnSelectTableCell:(UIButton *)inSender {
    NSInteger i = inSender.tag;
    NSString *s = [NSString stringWithFormat:@"%ld", (long)i];
    if([selcetedIndexArr containsObject: s]){
        [selcetedIndexArr removeObject:s];
    }else{
        [selcetedIndexArr addObject:s];
    }
    selectedTableSection = i;
    [self displayData];
    [_tableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 56;
}
#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// location history Succeed:

- (void)locationHistorySucceed:(NSNotification *)inNotify {
    NSDictionary *userInfo = [inNotify userInfo];
    locationHistory = userInfo;
    NSMutableArray *res = userInfo[@"response"];
    if(res.count>0){
        locationHistoryArray = userInfo[@"response"][@"history"];
    }else{
        [locationHistoryArray removeAllObjects];
    }
    [_tableView reloadData];
}

- (void)locationHistoryFailure:(NSNotification *)inNotify {
    NSDictionary *userInfo = [inNotify userInfo];
}


@end
