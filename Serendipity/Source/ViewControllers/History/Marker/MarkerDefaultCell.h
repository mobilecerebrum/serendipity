//
//  MarkerDefaultCell.h
//  Serendipity
//
//  Created by Jonish Sangwan on 02/11/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MarkerDefaultCell : UIView
@property (weak, nonatomic) IBOutlet UIButton *image;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *speed;
@property (weak, nonatomic) IBOutlet UILabel *acurancy;
@property (weak, nonatomic) IBOutlet UILabel *battrySetting;

@end

NS_ASSUME_NONNULL_END
