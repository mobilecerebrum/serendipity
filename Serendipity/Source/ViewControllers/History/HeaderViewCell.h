//
//  HeaderViewCell.h
//  Serendipity
//
//  Created by Jonish Sangwan on 28/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *tripIndexOutlet;
@property (weak, nonatomic) IBOutlet UILabel *timeSlotLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceTimeLbl;
@property (weak, nonatomic) IBOutlet UIButton *arrowBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *didSelectBtnOutlet;

@end

NS_ASSUME_NONNULL_END
