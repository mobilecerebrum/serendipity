#import "SRAchivementsViewController.h"

@implementation SRAchivementsViewController


#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
	self = [super initWithNibName:@"SRAchivementsViewController" bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		server = inServer;
	}

	// Return
	return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
	//call super
	[super viewDidLoad];

	// Set navigation bar
	self.navigationController.navigationBar.hidden = NO;
	self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
	self.navigationController.navigationBar.translucent = NO;

	// Title
	[SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.Achivements", " ") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

	

	//Back Button Image with Action
	UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
	[leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

	//Initialize LiskItemArray
	menuItemArray = [[NSMutableArray alloc] initWithObjects:NSLocalizedString(@"lbl.User Slots.txt", @""), NSLocalizedString(@"lbl.Family Slots.txt", @""), NSLocalizedString(@"lbl.Profile Pictures.txt", @""), NSLocalizedString(@"lbl.Discovery_Radius.txt", @""), NSLocalizedString(@"lbl.Friend Notification.txt", @""), nil];

	//Call method to Show Expadnable table
	[self ExpandableTableData];
	self.objtableView.tableFooterView = [[UIView alloc] init];
    
    // Set profile pic
    self.ProfileImage.layer.cornerRadius = self.ProfileImage.frame.size.width / 2;
    self.ProfileImage.layer.masksToBounds = YES;
      
}

- (void)ExpandableTableData {
	// 3rd level
	NSMutableDictionary *dict3 = [[NSMutableDictionary alloc] init];
	[dict3 setValue:@"20 more Connection until next unlock!" forKey:@"name"];

	NSMutableArray *Objects3 = [[NSMutableArray alloc] init];
	[Objects3 addObject:dict3];

	//Achivements Screen
	NSString *s1 = @"Invisible User Slots";
	NSString *s2 = @"Family Member Slots";
	NSString *s3 = @"Viewable User Profile Pictures";
	NSString *s4 = @"Discovery Radius";
	NSString *s5 = @"Friend Notification Redius";

	NSMutableArray *dtArray1 = [[NSMutableArray alloc] initWithObjects:s1, s2, s3, s4, s5, nil];

	NSMutableArray *dtArray2 = [[NSMutableArray alloc] initWithObjects:@"50 Connection", @"100 Connection", @"Unlock 150 Connection", @"Unlock 200 Connection", @"Unlock 250 Connection", @"Unlock 500 Connection", nil];

	NSMutableArray *dtArray3 = [[NSMutableArray alloc] initWithObjects:@"20 more Connection until unlock", nil];

	//3rd level

	NSMutableArray *tempArray3 = [[NSMutableArray alloc] init];

	for (int i = 0; i < [dtArray3 count]; i++) {
		NSMutableDictionary *tempDict1 = [[NSMutableDictionary alloc] init];

		[tempDict1 setValue:[dtArray2 objectAtIndex:i] forKey:@"name"];
		[tempDict1 setObject:Objects3 forKey:@"Objects"];

		[tempArray3 addObject:tempDict1];
	}




	//2nd Level
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
	NSMutableArray *tempArray1 = [[NSMutableArray alloc] init];

	for (int i = 0; i < [dtArray2 count]; i++) {
		NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];

		[tempDict setValue:[dtArray2 objectAtIndex:i] forKey:@"name"];
		[tempDict setObject:tempArray3 forKey:@"Objects"];

		[tempArray1 addObject:tempDict];
	}


	//1st level

	for (int i = 0; i < [dtArray1 count]; i++) {
		NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];

		[tempDict setValue:[dtArray1 objectAtIndex:i] forKey:@"name"];
		[tempDict setObject:tempArray1 forKey:@"Objects"];


		[tempArray addObject:tempDict];
	}
	menuItemArray = [[NSMutableArray alloc] init];
	[menuItemArray addObjectsFromArray:tempArray];
}

#pragma mark
#pragma mark Action Methods
#pragma mark
// ---------------------------------------------------------------------------------------------------------------------------------
// backBtnAction:

- (void)actionOnBack:(id)sender {
	[self.navigationController popToViewController:self.delegate animated:NO];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [menuItemArray count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell;
	if (cell == nil) {
		NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
		if ([nibObjects count] > 0) {
			SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *)[nibObjects objectAtIndex:0];
			cell = customCell;
		}
	}
	((SRHomeMenuCustomCell *)cell).selectionStyle = UITableViewCellSelectionStyleNone;
	// Manage subviews hide and unhide
	//((CustomCell *)cell).MenuItemCountLabel.hidden = YES;
	((SRHomeMenuCustomCell *)cell).MenuItemFacebookBTN.hidden = YES;
	((SRHomeMenuCustomCell *)cell).MenuItemGoogleBTN.hidden = YES;
	((SRHomeMenuCustomCell *)cell).MenuItemLinkedInBTN.hidden = YES;
	((SRHomeMenuCustomCell *)cell).MenuItemTwitterBTN.hidden = YES;
	((SRHomeMenuCustomCell *)cell).MenuIconImage.hidden = YES;

	((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = NO;
	((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.layer.cornerRadius = ((SRHomeMenuCustomCell *)cell).MenuIconImage.frame.size.width / 3;
	((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.layer.masksToBounds = YES;
	((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.clipsToBounds = YES;
	((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = NO;


	if (!is2ndLevelSelectedRow && !is3rdLevelSelectedRow) {
		if (indexPath.row == 0) {
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = @"2";
		}
		else if (indexPath.row == 1) {
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = @"13";
		}
		else if (indexPath.row == 2) {
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = @"7";
		}
		else if (indexPath.row == 3) {
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.frame = CGRectMake(220, 10, 100, 15);
			[((SRHomeMenuCustomCell *)cell).MenuItemCountLabel setNeedsLayout];
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = @"200 miles";
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.backgroundColor = [UIColor clearColor];
		}
		else if (indexPath.row == 4) {
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.frame = CGRectMake(220, 10, 100, 15);
			[((SRHomeMenuCustomCell *)cell).MenuItemCountLabel setNeedsLayout];
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = @"50 miles";
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.backgroundColor = [UIColor clearColor];
		}
	}
	((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = CGRectMake(20, 10, 300, 15);
	//((SRHomeMenuCustomCell *)cell).MenuItemLabel.text = [menuItemArray objectAtIndex:indexPath.row];
	((SRHomeMenuCustomCell *)cell).MenuItemLabel.text = [[menuItemArray objectAtIndex:indexPath.row] valueForKey:@"name"];

	if (is2ndLevelSelectedRow) {
		// ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = YES;

		((SRHomeMenuCustomCell *)cell).backgroundColor = [UIColor whiteColor];
		((SRHomeMenuCustomCell *)cell).MenuIconImage.hidden = NO;
		((SRHomeMenuCustomCell *)cell).MenuIconImage.image = [UIImage imageNamed:@"unlock.png"];
		((SRHomeMenuCustomCell *)cell).MenuIconImage.clipsToBounds = YES;
		((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = CGRectMake(60, 10, 300, 15);
		((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.frame = CGRectMake(220, 10, 100, 15);
		[((SRHomeMenuCustomCell *)cell).MenuItemCountLabel setNeedsLayout];
		((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = [[NSString stringWithFormat:@"%ld", (long)indexPath.row] stringByAppendingString:@" User"];
		((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.textColor = [UIColor blackColor];
		((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.backgroundColor = [UIColor clearColor];


		if (is2ndLevelSelectedRow && is3rdLevelSelectedRow) {
			((SRHomeMenuCustomCell *)cell).MenuIconImage.hidden = YES;
			((SRHomeMenuCustomCell *)cell).MenuItemLabel.frame = CGRectMake(70, 12, 300, 15);
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = NO;
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.frame = CGRectMake(40, 10, 20, 20);
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.layer.cornerRadius = ((SRHomeMenuCustomCell *)cell).MenuIconImage.frame.size.width / 3;
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.layer.masksToBounds = YES;
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.clipsToBounds = YES;
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = @"20";
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.textColor = [UIColor whiteColor];
			((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.backgroundColor = [UIColor blackColor];
		}
	}

	else {
		((SRHomeMenuCustomCell *)cell).backgroundColor = [UIColor clearColor];
	}

	// Return
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//set flag yes when select row

	is2ndLevelSelectedRow = YES;

	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	NSDictionary *d = [menuItemArray objectAtIndex:indexPath.row];
	if ([d valueForKey:@"Objects"]) {
		NSArray *ar = [d valueForKey:@"Objects"];
		if ([ar count] == 1) {
			is3rdLevelSelectedRow = YES;
		}

		BOOL isAlreadyInserted = NO;

		for (NSDictionary *dInner in ar) {
			NSInteger index = [menuItemArray indexOfObjectIdenticalTo:dInner];
			isAlreadyInserted = (index > 0 && index != NSIntegerMax);
			if (isAlreadyInserted) break;
		}

		if (isAlreadyInserted) {
			[self miniMizeThisRows:ar];
		}
		else {
			NSUInteger count = indexPath.row + 1;
			NSMutableArray *arCells = [NSMutableArray array];
			for (NSDictionary *dInner in ar) {
				[arCells addObject:[NSIndexPath indexPathForRow:count inSection:0]];
				[menuItemArray insertObject:dInner atIndex:count++];
			}
			[tableView insertRowsAtIndexPaths:arCells withRowAnimation:UITableViewRowAnimationLeft];
		}
	}
}

- (void)miniMizeThisRows:(NSArray *)ar {
	//set flag selected row to no
	is2ndLevelSelectedRow = NO;
	is3rdLevelSelectedRow = NO;
	//cellcount = 0;

	for (NSDictionary *dInner in ar) {
		NSUInteger indexToRemove = [menuItemArray indexOfObjectIdenticalTo:dInner];
		NSArray *arInner = [dInner valueForKey:@"Objects"];
		if (arInner && [arInner count] > 0) {
			[self miniMizeThisRows:arInner];
		}

		if ([menuItemArray indexOfObjectIdenticalTo:dInner] != NSNotFound) {
			[menuItemArray removeObjectIdenticalTo:dInner];
			[self.objtableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:
            [NSIndexPath indexPathForRow:indexToRemove inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
		}
	}
}

@end
