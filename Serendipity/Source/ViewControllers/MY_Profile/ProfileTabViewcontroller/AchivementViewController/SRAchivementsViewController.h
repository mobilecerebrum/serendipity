#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRProfileImageView.h"
@interface SRAchivementsViewController : ParentViewController
{
	// Instance Varialble
	NSMutableArray *menuItemArray;
	BOOL is2ndLevelSelectedRow;
	BOOL is3rdLevelSelectedRow;

	// Server
	SRServerConnection *server;
}

//Properties
@property (strong, nonatomic) IBOutlet UITableView *objtableView;
@property (strong, nonatomic) IBOutlet SRProfileImageView *screenBackGroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *ProfileBackgroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackgroundColor;
@property (strong, nonatomic) IBOutlet SRProfileImageView *ProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *lblProfileName;
@property (strong, nonatomic) IBOutlet UILabel *lblConnection;

// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
