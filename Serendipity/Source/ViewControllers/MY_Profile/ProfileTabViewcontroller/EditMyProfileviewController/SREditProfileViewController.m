#import "SREditProfileViewController.h"
#import "SRMyProfileViewController.h"
#import "SRProfileTabViewController.h"
#import "SRMenuViewController.h"

#define kLeftBtnTag 500
#define kRightBtnTag 501
#define kProfileAddButtonTag 700
#define kInterestAddButtonTag 800

@implementation SREditProfileViewController

#pragma mark
#pragma mark Private Method
#pragma mark

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SREditProfileViewController" bundle:nil];
    if (self) {
        server = inServer;

        profileImgsArr = [[NSMutableArray alloc] init];
        interestImgsArr = [[NSMutableArray alloc] init];
        deleteImagesIdsArr = [[NSMutableArray alloc] init];


        // Notification register
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getUserDetailsSucceed:)
                              name:kKeyNotificationCompleteSignUPSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getUserDetailsFailed:)
                              name:kKeyNotificationCompleteSignUPFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(uploadAlbumImageSucceed:)
                              name:kKeyNotificationUploadUserAlbumSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(uploadAlbumImageFailed:)
                              name:kKeyNotificationUploadUserAlbumFailed object:nil];
    }

    // Return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {

        // Date picker toolbar button space
        UIBarButtonItem *fixedItemSpaceWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedItemSpaceWidth.width = 280.0f; // or whatever you want
        self.pickerToolBar.items = @[self.cancelBarBtn, fixedItemSpaceWidth, self.doneBarBtn];


        // Increase imgYourNameBGImage background image width by 70
        self.imgYourNameBGImage.frame = CGRectMake(self.imgYourNameBGImage.frame.origin.x, self.imgYourNameBGImage.frame.origin.y, self.imgYourNameBGImage.frame.size.width + 45, 40);

        // Increase txtFirstName width by 45
        self.txtFirstName.frame = CGRectMake(self.txtFirstName.frame.origin.x, self.txtFirstName.frame.origin.y, self.txtFirstName.frame.size.width + 45, 40);

        // Increase txtFldLastName background image width by 45
        self.imgLastNameBck.frame = CGRectMake(self.imgYourNameBGImage.frame.size.width + -22, self.imgLastNameBck.frame.origin.y, self.imgLastNameBck.frame.size.width + 45, 40);

        // Increase txtLastName width by 45
        self.txtLastName.frame = CGRectMake(self.imgYourNameBGImage.frame.size.width + -10, self.txtLastName.frame.origin.y, self.txtLastName.frame.size.width + 45, 40);

        // Increase imgDateBGImage width by 27
        self.imgDateBGImage.frame = CGRectMake(self.imgDateBGImage.frame.origin.x, self.imgDateBGImage.frame.origin.y, self.imgDateBGImage.frame.size.width + 27, 40);

        // Increase imgDateBGImage width by 27
        self.txtDate.frame = CGRectMake(self.txtDate.frame.origin.x, self.txtDate.frame.origin.y, self.txtDate.frame.size.width + 27, 40);

        // Increase imgOccupationBGImage width by 27
        self.imgOccupationBGImage.frame = CGRectMake(self.imgOccupationBGImage.frame.origin.x - 12, self.imgOccupationBGImage.frame.origin.y, self.imgOccupationBGImage.frame.size.width + 27, 40);

        // Increase txtOccupation width by 27
        self.txtOccupation.frame = CGRectMake(self.txtOccupation.frame.origin.x - 12, self.txtOccupation.frame.origin.y, self.txtOccupation.frame.size.width + 27, 40);

        // Increase imgOccupationBGImage width by 27
        self.imgLocationBGImage.frame = CGRectMake(self.imgLocationBGImage.frame.origin.x - 27, self.imgLocationBGImage.frame.origin.y, self.imgLocationBGImage.frame.size.width + 27, 40);

        // Increase txtOccupation width by 27
        self.txtLocation.frame = CGRectMake(self.txtLocation.frame.origin.x - 27, self.txtLocation.frame.origin.y, self.txtLocation.frame.size.width + 27, 40);
    } else if (SCREEN_WIDTH == 375 && SCREEN_HEIGHT == 667) {
        // Date picker toolbar button space
        UIBarButtonItem *fixedItemSpaceWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedItemSpaceWidth.width = 255.0f; // or whatever you want
        self.pickerToolBar.items = @[self.cancelBarBtn, fixedItemSpaceWidth, self.doneBarBtn];


        // Increase imgYourNameBGImage background image width by 70
        self.imgYourNameBGImage.frame = CGRectMake(self.imgYourNameBGImage.frame.origin.x, self.imgYourNameBGImage.frame.origin.y, self.imgYourNameBGImage.frame.size.width + 25, 40);

        // Increase txtFirstName width by 45
        self.txtFirstName.frame = CGRectMake(self.txtFirstName.frame.origin.x, self.txtFirstName.frame.origin.y, self.txtFirstName.frame.size.width + 25, 40);

        // Increase txtFldLastName background image width by 45
        self.imgLastNameBck.frame = CGRectMake(self.imgLastNameBck.frame.origin.x - 25, self.imgLastNameBck.frame.origin.y, self.imgLastNameBck.frame.size.width + 25, 40);

        // Increase txtLastName width by 45
        self.txtLastName.frame = CGRectMake(self.txtLastName.frame.origin.x - 25, self.txtLastName.frame.origin.y, self.txtLastName.frame.size.width + 25, 40);

        // Increase imgDateBGImage width by 15
        self.imgDateBGImage.frame = CGRectMake(self.imgDateBGImage.frame.origin.x, self.imgDateBGImage.frame.origin.y, self.imgDateBGImage.frame.size.width + 15, 40);

        // Increase imgDateBGImage width by 15
        self.txtDate.frame = CGRectMake(self.txtDate.frame.origin.x, self.txtDate.frame.origin.y, self.txtDate.frame.size.width + 15, 40);

        // Increase imgOccupationBGImage width by 15
        self.imgOccupationBGImage.frame = CGRectMake(self.imgOccupationBGImage.frame.origin.x - 7, self.imgOccupationBGImage.frame.origin.y, self.imgOccupationBGImage.frame.size.width + 15, 40);

        // Increase txtOccupation width by 15
        self.txtOccupation.frame = CGRectMake(self.txtOccupation.frame.origin.x - 7, self.txtOccupation.frame.origin.y, self.txtOccupation.frame.size.width + 15, 40);

        // Increase imgOccupationBGImage width by 15
        self.imgLocationBGImage.frame = CGRectMake(self.imgLocationBGImage.frame.origin.x - 15, self.imgLocationBGImage.frame.origin.y, self.imgLocationBGImage.frame.size.width + 15, 40);

        // Increase txtOccupation width by 15
        self.txtLocation.frame = CGRectMake(self.txtLocation.frame.origin.x - 15, self.txtLocation.frame.origin.y, self.txtLocation.frame.size.width + 15, 40);
    }
    else {
        // Date picker toolbar button space
        UIBarButtonItem *fixedItemSpaceWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        fixedItemSpaceWidth.width = 280.0f; // or whatever you want
        self.pickerToolBar.items = @[self.cancelBarBtn, fixedItemSpaceWidth, self.doneBarBtn];
        
        
        // Increase imgYourNameBGImage background image width by 70
        self.imgYourNameBGImage.frame = CGRectMake(self.imgYourNameBGImage.frame.origin.x, self.imgYourNameBGImage.frame.origin.y, self.imgYourNameBGImage.frame.size.width + 45, 40);
        
        // Increase txtFirstName width by 45
        self.txtFirstName.frame = CGRectMake(self.txtFirstName.frame.origin.x, self.txtFirstName.frame.origin.y, self.txtFirstName.frame.size.width + 45, 40);
        
        // Increase txtFldLastName background image width by 45
        self.imgLastNameBck.frame = CGRectMake(self.imgYourNameBGImage.frame.size.width + -22, self.imgLastNameBck.frame.origin.y, self.imgLastNameBck.frame.size.width + 45, 40);
        
        // Increase txtLastName width by 45
        self.txtLastName.frame = CGRectMake(self.imgYourNameBGImage.frame.size.width + -10, self.txtLastName.frame.origin.y, self.txtLastName.frame.size.width + 45, 40);
        
        // Increase imgDateBGImage width by 27
        self.imgDateBGImage.frame = CGRectMake(self.imgDateBGImage.frame.origin.x, self.imgDateBGImage.frame.origin.y, self.imgDateBGImage.frame.size.width + 27, 40);
        
        // Increase imgDateBGImage width by 27
        self.txtDate.frame = CGRectMake(self.txtDate.frame.origin.x, self.txtDate.frame.origin.y, self.txtDate.frame.size.width + 27, 40);
        
        // Increase imgOccupationBGImage width by 27
        self.imgOccupationBGImage.frame = CGRectMake(self.imgOccupationBGImage.frame.origin.x - 12, self.imgOccupationBGImage.frame.origin.y, self.imgOccupationBGImage.frame.size.width + 27, 40);
        
        // Increase txtOccupation width by 27
        self.txtOccupation.frame = CGRectMake(self.txtOccupation.frame.origin.x - 12, self.txtOccupation.frame.origin.y, self.txtOccupation.frame.size.width + 27, 40);
        
        // Increase imgOccupationBGImage width by 27
        self.imgLocationBGImage.frame = CGRectMake(self.imgLocationBGImage.frame.origin.x - 27, self.imgLocationBGImage.frame.origin.y, self.imgLocationBGImage.frame.size.width + 27, 40);
        
        // Increase txtOccupation width by 27
        self.txtLocation.frame = CGRectMake(self.txtLocation.frame.origin.x - 27, self.txtLocation.frame.origin.y, self.txtLocation.frame.size.width + 27, 40);
    }
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.editProfile", " ") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    leftButton.tag = kLeftBtnTag;
    [leftButton addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"edit_profile_white.png"] forViewNavCon:self offset:-23];
    rightButton.tag = kRightBtnTag;
    [rightButton addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    //textFields Placehoders
    UIColor *color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    self.txtFirstName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.firstname", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtLastName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.lastname", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.date", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtOccupation.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.occupation", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtLocation.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.location", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtSmAboutYou.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.smAboutYou", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.txtWebsiteURL.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.websiteURL", @"") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:13.0]}];
    self.lblOpenForDating.text = NSLocalizedString(@"lbl.openForDating.txt", @"");
    self.lblInterests.text = NSLocalizedString(@"lbl.interests.txt", @"");

    // Set corner radius to imageviews;
    self.imgYourNameBGImage.layer.cornerRadius = 5.0;
    self.imgYourNameBGImage.layer.masksToBounds = YES;
    self.imgLastNameBck.layer.cornerRadius = 5.0;
    self.imgLastNameBck.layer.masksToBounds = YES;
    self.imgDateBGImage.layer.cornerRadius = 5.0;
    self.imgDateBGImage.layer.masksToBounds = YES;
    self.imgOccupationBGImage.layer.cornerRadius = 5.0;
    self.imgOccupationBGImage.layer.masksToBounds = YES;
    self.imgLocationBGImage.layer.cornerRadius = 5.0;
    self.imgLocationBGImage.layer.masksToBounds = YES;
    self.imgSmAboutYouBGImage.layer.cornerRadius = 5.0;
    self.imgSmAboutYouBGImage.layer.masksToBounds = YES;
    self.imgWebsiteBGImage.layer.cornerRadius = 5.0;
    self.imgWebsiteBGImage.layer.masksToBounds = YES;

    // Set image view gredient
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.imgProfileDetailBlackColorImage.alpha = 0.9;
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.imgProfileDetailBlackColorImage.bounds;
        gradient.colors = @[(id) [[UIColor blackColor] CGColor],
                (id) [[UIColor whiteColor] CGColor]];
        [self.imgProfileDetailBlackColorImage.layer insertSublayer:gradient atIndex:0];
    });

    // Set gesture to dismiss keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.objDetailsScrollview addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];

    // Set initially content size to profile details field scroll view
    self.objDetailsScrollview.contentSize = CGSizeMake(0, self.interestScrollView.frame.size.height + self.interestScrollView.frame.origin.y + 10);
    self.objProfileImagesScrollview.scrollEnabled = NO;
    self.interestScrollView.scrollEnabled = NO;

    // Dating Switch Background
    self.objDatingSwitch.tintColor = [UIColor whiteColor];
    [self.objDatingSwitch setOnTintColor:[UIColor orangeColor]];
    self.objDatingSwitch.backgroundColor = [UIColor lightGrayColor];
    self.objDatingSwitch.layer.cornerRadius = 16.0f;
    [self.objDatingSwitch addTarget:self action:@selector(datingSwitchAction:) forControlEvents:UIControlEventValueChanged];

    // Date Picker
    self.pickerView.hidden = YES;
    self.datePicker.maximumDate = [NSDate date];
    //self.btnCloseImage.hidden = YES;

    // Display the data
    [self displayDetails];

    // Set Close Profile Pic Btn Cirecular
    self.btnCloseImage.layer.cornerRadius = self.btnCloseImage.frame.size.width / 2;
    self.btnCloseImage.layer.masksToBounds = YES;
}


// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
    // End editing
    [self.view endEditing:YES];
    self.pickerView.hidden = YES;
    [self.objDetailsScrollview setContentOffset:CGPointMake(0, 0)];
}



// -----------------------------------------------------------------------------------------------------
// updateDateInTextField

- (void)updateDateInTextField:(UIDatePicker *)picker {

    NSString *formattedDate = [SRModalClass returnStringInMMDDYYYY:picker.date];
    self.txtDate.text = formattedDate;
    
    //Check user age is between 13 to 18
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSDate *dateOfBirth = picker.date;
//    NSDate *now = [NSDate date];
//    NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
//                                       components:NSCalendarUnitYear
//                                       fromDate:dateOfBirth
//                                       toDate:now
//                                       options:0];
//    NSInteger age = [ageComponents year];
//    
//    [self.objDatingSwitch setHidden:(age >= 13 && age < 18)];
//    [self.imgDatingImage setHidden:(age >= 13 && age < 18)];
//    [self.lblOpenForDating setHidden:(age >= 13 && age < 18)];
}

// -----------------------------------------------------------------------------------------------------
// selectImageFromPicker:

- (void)selectImageFromPicker:(UIButton *)sender {
    // Image picker action will happen here
    tag = sender.tag;

    UIActionSheet *actionSheet = nil;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // Present action sheet
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), NSLocalizedString(@"camera.text", ""), nil];
    } else {
        // Present action sheet
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "")
                                    destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), nil];
    }

    // Show sheet
    [actionSheet showFromRect:[sender frame] inView:self.view animated:YES];
}

// -----------------------------------------------------------------------------------------------------
// displayDetails

- (void)displayDetails {
    // First Name
    self.txtFirstName.text = server.loggedInUserInfo[kKeyFirstName];

    // Last Name
    self.txtLastName.text = server.loggedInUserInfo[kKeyLastName];

    // Date of birth
    if ([server.loggedInUserInfo[kKeyDOB] length] > 0) {
        NSString *dobStr = server.loggedInUserInfo[kKeyDOB];
        NSDate *date = [SRModalClass returnDateInYYYYMMDD:dobStr];
        dobStr = [SRModalClass returnStringInMMDDYYYY:date];
        self.txtDate.text = dobStr;
    }

    // Occupation
    if ([server.loggedInUserInfo[kKeyOccupation] length] > 0) {
        self.txtOccupation.text = server.loggedInUserInfo[kKeyOccupation];
    }
    // Location
    if ([[server.loggedInUserInfo valueForKey:kKeyAddress] length] > 0) {
        self.txtLocation.text = [server.loggedInUserInfo valueForKey:kKeyAddress];
    }
    // Description
    
    NSString *str = server.loggedInUserInfo[kKeyAboutUser];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    if (goodValue == nil){
        goodValue = str;
    }

    [server.loggedInUserInfo setValue:goodValue forKey:kKeyAboutUser];

    if ([server.loggedInUserInfo[kKeyAboutUser] length] > 0) {
        NSString *str = server.loggedInUserInfo[kKeyAboutUser];
        self.txtSmAboutYou.text = str;
    }
    // Website url
    if ([server.loggedInUserInfo[kKeyUserWebUrl] length] > 0) {
        self.txtWebsiteURL.text = server.loggedInUserInfo[kKeyUserWebUrl];
    }

    // Dating image
    if ([APP_DELEGATE isUserBelow18]) {
        [self.objDatingSwitch setHidden:YES];
        [self.imgDatingImage setHidden:YES];
        [self.lblOpenForDating setHidden:YES];
    } else {
        [self.objDatingSwitch setOn:NO];
        if ([server.loggedInUserInfo[kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *settingDict = server.loggedInUserInfo[kKeySetting];
            if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
                self.imgDatingImage.image = [UIImage imageNamed:@"dating-orange-40px.png"];
                [self.objDatingSwitch setOn:YES];
            } else {
                self.imgDatingImage.image = [UIImage imageNamed:@"ic_dating_white.png"];
            }
        } else {
            self.imgDatingImage.image = [UIImage imageNamed:@"ic_dating_white.png"];
        }

    }


    // TODO: Interest


    // Display albums
    profileImgsArr = server.loggedInUserInfo[kKeyMedia];
    [self addItemsToScrollView:YES];
}

#pragma mark
#pragma mark Scroll Customization Method
#pragma mark

// --------------------------------------------------------------------------------
// addDashedBorderWithColor:

- (CAShapeLayer *)addDashedBorderWithColor:(CGFloat)lineNo {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    CGSize frameSize = CGSizeMake(65, 65);
    CGRect shapeRect = CGRectMake(0.0f, 0.0f, frameSize.width, frameSize.height);
    [shapeLayer setBounds:shapeRect];
    [shapeLayer setPosition:CGPointMake(frameSize.width / 2, frameSize.height / 2)];

    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[[UIColor whiteColor] CGColor]];
    [shapeLayer setLineWidth:lineNo];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineDashPattern:
            @[@10,
                    @2]];

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:shapeRect cornerRadius:frameSize.width / 2];
    [shapeLayer setPath:path.CGPath];

    // Return
    return shapeLayer;
}

// --------------------------------------------------------------------------------
// addItemsToScrollView:

- (void)addItemsToScrollView:(BOOL)isProfile {
    // Remove all objects from scroll view
    NSArray *imgsArr;
    if (isProfile) {
        imgsArr = profileImgsArr;
    } else
        imgsArr = interestImgsArr;

    CGRect latestFrame = CGRectZero;

    // Add images to scroll view accordingly
    for (NSUInteger i = 0; i < [imgsArr count]; i++) {
        NSDictionary *dict = imgsArr[i];

        UIView *subView = [[UIView alloc] init];
        if (i == 0) {
            subView.frame = CGRectMake(5, 0, 65, 70);
        } else
            subView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 4, 0, 65, 70);
        latestFrame = subView.frame;
        subView.backgroundColor = [UIColor clearColor];

        // Add image view
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.image = [UIImage imageNamed:@"profile_thumbnail.png"];
        UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 46, -2, 20, 20)];

        if (isProfile) {
            // Get result
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                UIImage *img = nil;

                if (dict[kKeyImageName] != nil) {
                    NSString *imageName = dict[kKeyImageName];
                    if ([imageName length] > 0) {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                        img = [UIImage imageWithData:
                                [NSData dataWithContentsOfURL:
                                        [NSURL URLWithString:imageUrl]]];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (img) {
                            imgView.image = img;
                        }
                    });
                }
            });

            [btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
            btnDelete.tag = i;
            [btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            imgView.image = [UIImage new];
            imgView.layer.cornerRadius = imgView.frame.size.width / 2;
            imgView.clipsToBounds = YES;

            CAShapeLayer *layer = [self addDashedBorderWithColor:2];
            [imgView.layer addSublayer:layer];

            UILabel *lblImageName = [[UILabel alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 20, 64, 60, 15)];
            lblImageName.textColor = [UIColor whiteColor];
            lblImageName.font = [UIFont fontWithName:kFontHelveticaRegular size:11.0];
            lblImageName.text = [NSString stringWithFormat:@"image%ld", (unsigned long) i];
            [subView addSubview:lblImageName];
            [btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
            if (isProfile) {
                btnDelete.tag = kProfileAddButtonTag + i;
            } else
                btnDelete.tag = kInterestAddButtonTag + i;

            [btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];
        }
        //Add tapGesture On ImageView to set as profile pic
        imgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTappedAction:)];
        imgView.tag = i;
        [imgView addGestureRecognizer:tapGesture];

        [subView addSubview:imgView];
        [subView addSubview:btnDelete];

        // Add to the scroll view according to flag
        if (isProfile) {
            [self.objProfileImagesScrollview addSubview:subView];
        } else
            [self.interestScrollView addSubview:subView];
    }

    // Check if lastest frame is not empty
    if (CGRectEqualToRect(latestFrame, CGRectZero)) {
        latestFrame = CGRectMake(5, 0, 65, 70);
    } else
        latestFrame = CGRectMake(latestFrame.origin.x + 70, 0, 65, 70);

    UIView *btnSuperView = [[UIView alloc] init];
    btnSuperView.frame = latestFrame;

    // Add to scroll view
    UIButton *btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
    [btnAdd addTarget:self action:@selector(actionOnBtnAdd:) forControlEvents:UIControlEventTouchUpInside];
    [btnSuperView addSubview:btnAdd];

    if (isProfile) {
        btnAdd.tag = kProfileAddButtonTag;
        [btnAdd setImage:[UIImage imageNamed:@"profile_thumbnail_add.png"] forState:UIControlStateNormal];
        [self.objProfileImagesScrollview addSubview:btnSuperView];
    } else {
        btnAdd.tag = kInterestAddButtonTag;
        [btnAdd.layer addSublayer:[self addDashedBorderWithColor:1]];
        [btnAdd setImage:[UIImage imageNamed:@"miles-set-plus.png"] forState:UIControlStateNormal];
        [self.interestScrollView addSubview:btnSuperView];
    }

    if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
        if (isProfile) {
            self.objProfileImagesScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
            self.objProfileImagesScrollview.scrollEnabled = YES;
            self.objProfileImagesScrollview.backgroundColor = [UIColor clearColor];
        } else {
            self.interestScrollView.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
            self.interestScrollView.scrollEnabled = YES;
            self.interestScrollView.backgroundColor = [UIColor clearColor];
        }
    }
}

// -----------------------------------------------------------------------------------------------
// actionOnBtnDelete:

- (void)actionOnBtnDelete:(UIButton *)inSender {
    if (inSender.tag < kInterestAddButtonTag) {
        // Remove object at index
        // To remove image from media scroll
        NSInteger i = inSender.tag;/// kProfileAddButtonTag;

        NSMutableArray *imagesArray = [[NSMutableArray alloc] initWithArray:profileImgsArr];
        NSDictionary *dict = profileImgsArr[i];
        [imagesArray removeObject:dict];
        [deleteImagesIdsArr addObject:[dict valueForKey:kKeyId]];
        profileImgsArr = imagesArray;

        for (UIView *subview in[self.objProfileImagesScrollview subviews]) {
            if ([subview isKindOfClass:[UIView class]]) {
                [subview removeFromSuperview];
            }
        }

        [self addItemsToScrollView:YES];
    } else {
        for (UIView *subview in[self.interestScrollView subviews]) {
            if ([subview isKindOfClass:[UIView class]]) {
                [subview removeFromSuperview];
            }
        }
        // Remove object at index
        NSInteger i = inSender.tag / kInterestAddButtonTag;
        [interestImgsArr removeObjectAtIndex:i];

        [self addItemsToScrollView:NO];
    }
}

// -----------------------------------------------------------------------------------------------
// actionOnBtnAdd:

- (void)actionOnBtnAdd:(UIButton *)inSender {
    if (inSender.tag < kInterestAddButtonTag) {
        [self selectImageFromPicker:inSender];
    } else {
        for (UIView *subview in[self.interestScrollView subviews]) {
            if ([subview isKindOfClass:[UIView class]]) {
                [subview removeFromSuperview];
            }
        }
        // Add object at index
        NSInteger i = inSender.tag / kInterestAddButtonTag;
        i++;
        NSString *string = [NSString stringWithFormat:@"%ld", (long) i];
        [interestImgsArr addObject:string];
        [self addItemsToScrollView:NO];
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnButtonClick:(id)sender {
    // End editing
    [self.view endEditing:YES];
    [self.objDetailsScrollview setContentOffset:CGPointMake(0, 0)];
    if (((UIButton *) sender).tag == kLeftBtnTag) {
        [self.navigationController popViewControllerAnimated:NO];
    } else if (((UIButton *) sender).tag == kRightBtnTag) {
        // If images are deleted then save it.
        if (deleteImagesIdsArr.count > 0) {
            for (int i = 0; i < deleteImagesIdsArr.count; i++) {
                NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyMedia, deleteImagesIdsArr[i]];
                [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:YES inMethodType:kDELETE];
            }
        }
        // Send all edit info to server
        NSString *errMsg = nil;
        if ([self.txtFirstName.text length] == 0) {
            errMsg = NSLocalizedString(@"empty.firstname.text", @"");
        }
        if ([self.txtLastName.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.lastname.text", @"");
        }
        if (errMsg) {
            [SRModalClass showAlert:errMsg];
        } else {
            isAfterDeleted = NO;
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            params[kKeyFirstName] = self.txtFirstName.text;
            params[kKeyLastName] = self.txtLastName.text;
            if ([self.txtOccupation.text length] > 0)
                params[kKeyOccupation] = self.txtOccupation.text;
            if ([self.txtWebsiteURL.text length] > 0)
                params[kKeyUserWebUrl] = self.txtWebsiteURL.text;
            if ([self.txtSmAboutYou.text length] > 0) {
                NSData *data = [self.txtSmAboutYou.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
                NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                params[kKeyAboutUser] = goodValue;
            }

            if ([self.txtLocation.text length] > 0)
                params[kKeyAddress] = self.txtLocation.text;
            if ([self.txtDate.text length] > 0) {
                params[kKeyDOB] = self.txtDate.text;
            }
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSDate *dateOfBirth = [dateFormatter dateFromString:params[kKeyDOB]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            
            if (dateOfBirth != nil){
                params[kKeyDOB] = [dateFormatter stringFromDate:dateOfBirth];
            }

            
            if ([self.objDatingSwitch isOn]) {
                params[kKeyOpenForDating] = @1;
            } else
                params[kKeyOpenForDating] = @0;

            params[@"_method"] = kPUT;

            //[APP_DELEGATE showActivityIndicator];
            NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, server.loggedInUserInfo[kKeyId]];
            [server asychronousRequestWithData:params imageData:imageData forKey:kKeyProfileImage toClassType:urlStr inMethodType:kPOST];
        }
    } else if (sender == self.btnCloseImage) {
        // Close button action
        NSDictionary *profileImgDict = server.loggedInUserInfo[kKeyProfileImage];
        if ([profileImgDict isKindOfClass:[NSDictionary class]] && profileImgDict[kKeyId]) {
            //[APP_DELEGATE showActivityIndicator];
            NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyMedia, profileImgDict[kKeyId]];
            [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
        }
    } else if (sender == self.btnEditImage) {
        [self selectImageFromPicker:self.btnEditImage];
    } else if (sender == self.btnDate) {
        [self.objDetailsScrollview setContentOffset:CGPointMake(0, self.objDetailsScrollview.frame.origin.y + 150.0)];
        self.pickerView.hidden = NO;

        // Action on date
        if (sender == self.btnDate) {
            [self.datePicker setDatePickerMode:UIDatePickerModeDate];
            [self.datePicker addTarget:self action:@selector(updateDateInTextField:) forControlEvents:UIControlEventValueChanged];

            if ([[self.txtDate text] length] == 0) {
            } else {
                NSString *txtFldDateStr = self.txtDate.text;
                NSDate *date = [SRModalClass returnDateInMMDDYYYY:txtFldDateStr];
                [self.datePicker setDate:date];
            }
        }
    } else if (sender == self.doneBarBtn) {
        [self updateDateInTextField:self.datePicker];
        self.pickerView.hidden = YES;
    } else if (sender == self.cancelBarBtn) {
        self.pickerView.hidden = YES;
        // If already birthdate is there set user's birthdate otherwise set it empty
        if ([server.loggedInUserInfo[kKeyDOB] length] > 0) {
            NSString *dobStr = server.loggedInUserInfo[kKeyDOB];

            NSDate *date = [SRModalClass returnDateInYYYYMMDD:dobStr];
            dobStr = [SRModalClass returnStringInMMDDYYYY:date];
            self.txtDate.text = dobStr;
        } else
            self.txtDate.text = @"";
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------
// datingSwitchAction:

- (IBAction)pickLocation {
    // Location from map
    SRLocationSetViewController *objLocationView = [[SRLocationSetViewController alloc] initWithNibName:nil bundle:nil server:server];
    objLocationView.delegate = self;
    objLocationView.getLocation = YES;
    objLocationView.address = self.txtLocation.text;
    self.navigationController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:objLocationView animated:YES];
    self.navigationController.hidesBottomBarWhenPushed = YES;
}
// ---------------------------------------------------------------------------------------------------------------------------------
// datingSwitchAction:

- (void)datingSwitchAction:(id)Sender {
    if ([Sender isOn]) {
        self.imgDatingImage.image = [UIImage imageNamed:@"dating-orange-40px.png"];
    } else {
        self.imgDatingImage.image = [UIImage imageNamed:@"ic_dating_white.png"];
    }
}

// ------------------------------------------------------------------------------------------------------
// imageTappedAction:

- (void)imageTappedAction:(UITapGestureRecognizer *)tapGesture {
    // Get data
    UIImageView *imgView = (UIImageView *) tapGesture.view;
    UIImage *img = imgView.image;
    //imageData = UIImagePNGRepresentation(img);
    imageData = UIImageJPEGRepresentation(img, 1.0);
    // Present action sheet
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"setasprofilepic.text", ""), nil];

//	actionSheet.tag = 100;
    actionSheet.tag = imgView.tag;

    // Show sheet
    [actionSheet showFromRect:[self.view frame] inView:self.view animated:YES];
}

#pragma mark
#pragma mark TextFields Delegates
#pragma mark

// -----------------------------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtFirstName) {
        [self.txtFirstName resignFirstResponder];
        [self.txtLastName becomeFirstResponder];
    } else if (textField == self.txtOccupation) {
        [self.txtOccupation resignFirstResponder];
        [self.txtSmAboutYou becomeFirstResponder];
    } else if (textField == self.txtSmAboutYou) {
        [self.txtSmAboutYou resignFirstResponder];
        [self.txtWebsiteURL becomeFirstResponder];
    } else if (textField == self.txtWebsiteURL) {
        [self.txtWebsiteURL resignFirstResponder];
    }

    // Return
    return YES;
}

// -----------------------------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.txtLocation) {
        [textField resignFirstResponder];
        [self pickLocation];
    }
    self.pickerView.hidden = YES;
    if (SCREEN_HEIGHT == 480.0) {
        self.objDetailsScrollview.contentOffset = CGPointMake(0, 290);
    } else {
        self.objDetailsScrollview.contentOffset = CGPointMake(0, 230);
    }
}

#pragma mark
#pragma mark ActionSheet Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// clickedButtonAtIndex:

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {


    if (buttonIndex == 0 && [[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"setasprofilepic.text", "")]) {

        isProfilePicUpdated = YES;

        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyMedia, [profileImgsArr[popup.tag] valueForKey:kKeyId]];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[kKeyMediaIsDefault] = @"1";
        params[kKeyId] = [profileImgsArr[popup.tag] valueForKey:kKeyId];
        [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPUT];
    } else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"cancel.button.title.text", "")]) {
        [popup dismissWithClickedButtonIndex:buttonIndex animated:YES];
    } else {
        // Instantiate image picker and set delegate
        UIImagePickerController *imagePickerCon = [[UIImagePickerController alloc] init];
        imagePickerCon.delegate = self;
        imagePickerCon.allowsEditing = YES;
        [imagePickerCon setModalPresentationStyle:UIModalPresentationFullScreen];
        if (buttonIndex == 0) {
            imagePickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

            CATransition *transition = [CATransition animation];
            transition.duration = 1;
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromBottom;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self presentViewController:imagePickerCon animated:NO completion:nil];
        } else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            imagePickerCon.sourceType = UIImagePickerControllerSourceTypeCamera;

            CATransition *transition = [CATransition animation];
            transition.duration = 1;
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromBottom;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];

            [self presentViewController:imagePickerCon animated:NO completion:nil];
        }
    }

    /*if (popup.tag == 100) {
        if (buttonIndex ==  0) {

//			NSMutableDictionary *params = [NSMutableDictionary dictionary];
//			[params setObject:self.txtFirstName.text forKey:kKeyFirstName];
//			[params setObject:self.txtLastName.text forKey:kKeyLastName];
//			if ([self.txtOccupation.text length] > 0)
//				[params setObject:self.txtOccupation.text forKey:kKeyOccupation];
//			if ([self.txtWebsiteURL.text length] > 0)
//				[params setObject:self.txtWebsiteURL.text forKey:kKeyUserWebUrl];
//			if ([self.txtSmAboutYou.text length] > 0)
//				[params setObject:self.txtSmAboutYou.text forKey:kKeyAboutUser];
//			if ([self.txtLocation.text length] > 0)
//				[params setObject:self.txtLocation.text forKey:kKeyAddress];
//			if ([self.txtDate.text length] > 0) {
//				[params setObject:self.txtDate.text forKey:kKeyDOB];
//			}
//			[params setObject:kPUT forKey:@"_method"];
//
//
//			NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, [server.loggedInUserInfo objectForKey:kKeyId]];
//			[server asychronousRequestWithData:params imageData:imageData forKey:kKeyProfileImage toClassType:urlStr inMethodType:kPOST];
        }
    }
    else {
        // Instantiate image picker and set delegate
        UIImagePickerController *imagePickerCon = [[UIImagePickerController alloc]init];
        imagePickerCon.delegate = self;
        imagePickerCon.allowsEditing = YES;

        if (buttonIndex ==  0) {
            imagePickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

            CATransition *transition = [CATransition animation];
            transition.duration = 1;
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromBottom;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];
            [self presentViewController:imagePickerCon animated:NO completion:nil];
        }
        else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            imagePickerCon.sourceType = UIImagePickerControllerSourceTypeCamera;

            CATransition *transition = [CATransition animation];
            transition.duration = 1;
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromBottom;
            [self.view.window.layer addAnimation:transition forKey:kCATransition];

            [self presentViewController:imagePickerCon animated:NO completion:nil];
        }
    }*/
}

#pragma mark
#pragma mark Image Picker Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// didFinishPickingMediaWithInfo:

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    imgPickerCon = picker;

    // Selection of image done set it to button
    UIImage *selectedImage = [info valueForKey:UIImagePickerControllerEditedImage];

    if (tag >= 0) {
        //    UIImage *scaledImg = [SRModalClass scaleImage:selectedImage toSize:CGSizeMake(kKeyImageWidth, kKeyImageHeight)];
        //NSData *albumData = UIImagePNGRepresentation(selectedImage);
        NSData *albumData = UIImageJPEGRepresentation(selectedImage, 1.0);
        // Call Server api to upload
        NSDictionary *params = @{kKeyMediaIsDefault: @"0", kKeyMediaType: @"1", kKeyMediaReferenceId: server.loggedInUserInfo[kKeyId]};

        [server asychronousRequestWithData:params imageData:albumData forKey:kKeyMediaImage toClassType:kKeyClassUserAlbum inMethodType:kPOST];
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        //UIImage *scaledImg = [SRModalClass scaleImage:selectedImage toSize:CGSizeMake(kKeyImageWidth, kKeyImageHeight)];
        //imageData = UIImagePNGRepresentation(selectedImage);
        imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
        self.imgBackgroundPhotoImage.image = selectedImage;
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
}

// --------------------------------------------------------------------------------
// imagePickerControllerDidCancel:

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark
#pragma mark Location Controller Deelegate
#pragma mark

// -----------------------------------------------------------------------------------------------------
// dataFromSRLocationViewController

- (void)dataFromSRLocationViewController:(NSString *)data {
    self.txtLocation.text = data;
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// getUserDetailsSucceed:

- (void)getUserDetailsSucceed:(NSNotification *)inNotify {
    if (!isAfterDeleted) {
        // Back to my profile
        [(APP_DELEGATE) hideActivityIndicator];
        [self.navigationController popViewControllerAnimated:YES];
        
//        for (UIViewController* vc in self.navigationController.viewControllers){
//            if ([vc isKindOfClass:SRDI]){
//
//            }
//            NSLog(@"%@",vc);
            
            NSInteger topSafeArea = 0;
            
            if (@available(iOS 11.0, *)) {
                UIWindow *window = UIApplication.sharedApplication.keyWindow;
                topSafeArea = window.safeAreaInsets.top;
            }
            
            [[APP_DELEGATE topBarView] removeFromSuperview];
            
            [APP_DELEGATE setTopBarView:[[SRDiscoveryTopBarView alloc] initWithFrame:CGRectMake(0, 44 + topSafeArea, SCREEN_WIDTH, 75)]];
            [self.tabBarController.view addSubview:[APP_DELEGATE topBarView]];

//        }
    }
    //[SRModalClass showAlert:@"profile updated successfully"];

}

// -----------------------------------------------------------------------------------------------------
// getUserDetailsFailed:

- (void)getUserDetailsFailed:(NSNotification *)inNotify {
    [(APP_DELEGATE) hideActivityIndicator];
    if ([inNotify.object isKindOfClass:[NSString self]]){
        [SRModalClass showAlert:inNotify.object];
    }else{
        [SRModalClass showAlert:@"Sorry, your profile did not update properly. Please try again."];
    }
}

// -----------------------------------------------------------------------------------------------------
// uploadAlbumImageSucced:

- (void)uploadAlbumImageSucceed:(NSNotification *)inNotify {
    // Add dict
    [APP_DELEGATE hideActivityIndicator];
    id mediaDict = [inNotify object];
    if ([mediaDict isKindOfClass:[NSDictionary class]]) {

        [profileImgsArr addObject:mediaDict];

        // Dismiss picker
        [imgPickerCon dismissViewControllerAnimated:NO completion:nil];
    } else if ([mediaDict isKindOfClass:[NSString class]]) {
        // Delete The item from array
        NSDictionary *postInfo = [inNotify userInfo];
        NSString *objectUrl = postInfo[kKeyObjectUrl];
        NSArray *containsArr = [objectUrl componentsSeparatedByString:@"/"];

        BOOL isFound = NO;

        if ([containsArr count] > 1) {
            NSString *mediaId = containsArr[1];

            NSDictionary *dict = server.loggedInUserInfo[kKeyProfileImage];
            if ([dict[kKeyId] isEqualToString:mediaId]) {
                isFound = YES;
                self.imgBackgroundPhotoImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                [SRModalClass removeImage:kKeyFolderProfileImg inUser:server.loggedInUserInfo[kKeyId]];

                [APP_DELEGATE hideActivityIndicator];
            }

            // Find in the array and delete
            for (NSUInteger i = 0; i < [profileImgsArr count] && !isFound; i++) {
                NSDictionary *dict = profileImgsArr[i];
                NSString *idMedia = [NSString stringWithFormat:@"%@", dict[kKeyId]];
                if ([idMedia isEqualToString:mediaId]) {
                    isFound = YES;
                    [profileImgsArr removeObject:dict];

                    [server.loggedInUserInfo setValue:profileImgsArr forKey:kKeyMedia];
                    //					NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, [server.loggedInUserInfo objectForKey:kKeyId]];
//					[server makeAsychronousRequest:urlStr
//					                      inParams:nil
//					           isIndicatorRequired:NO
//					                  inMethodType:kGET];
                }
            }
        }

        // Call server api
        isAfterDeleted = YES;
    } else if ([mediaDict isKindOfClass:[NSArray class]]) {
        NSArray *mediaArray = [inNotify object];
        server.loggedInUserInfo[kKeyMedia] = mediaArray;
        for (NSDictionary *dict in mediaArray) {
            if ([[dict valueForKey:kKeyMediaIsDefault] boolValue]) {
                if (dict[kKeyImageName]) {
                    server.loggedInUserInfo[kKeyProfileImage] = dict;
                    // Post Notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationProfileImageCached object:nil];

                }
            }
        }
//                  // Back to my profile
//                  [self.navigationController popViewControllerAnimated:NO];
    }

    // Add the content to profile image array and reload the scroll view
    for (UIView *subview in[self.objProfileImagesScrollview subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    [self addItemsToScrollView:YES];
}

// -----------------------------------------------------------------------------------------------------
// uploadAlbumImageFailed:

- (void)uploadAlbumImageFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

@end
