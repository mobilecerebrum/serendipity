#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRActivityIndicator.h"
#import "SRServerConnection.h"
#import "SRLocationSetViewController.h"
#import "SRProfileImageView.h"

@interface SREditProfileViewController : ParentViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIActionSheetDelegate>
{
	// Instance variable
    UIImagePickerController *imgPickerCon;

    // Model
    NSMutableArray *profileImgsArr,*deleteImagesIdsArr;
    NSMutableArray *interestImgsArr;
    
    NSString *dobDate;
    
    NSData *imageData;
    
    NSInteger tag;
    BOOL isAfterDeleted;
    BOOL isProfilePicUpdated;
    
    // Server
    SRServerConnection *server;
}

// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *imgBackgroundPhotoImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackgroundWhiteColorImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfileDetailBlackColorImage;

@property (strong, nonatomic) IBOutlet UIButton *btnEditImage;
@property (strong, nonatomic) IBOutlet UIScrollView *objProfileImagesScrollview;
@property (strong, nonatomic) IBOutlet UIScrollView *objDetailsScrollview;

@property (strong, nonatomic) IBOutlet UIImageView *imgYourNameBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UIImageView *imgLastNameBck;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;

@property (strong, nonatomic) IBOutlet UIImageView *imgDateBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDate;

@property (strong, nonatomic) IBOutlet UIImageView *imgOccupationBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtOccupation;

@property (strong, nonatomic) IBOutlet UIImageView *imgLocationBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtLocation;

@property (strong, nonatomic) IBOutlet UIImageView *imgSmAboutYouBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtSmAboutYou;

@property (strong, nonatomic) IBOutlet UIImageView *imgWebsiteBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtWebsiteURL;

@property (strong, nonatomic) IBOutlet UIImageView *imgDatingImage;
@property (strong, nonatomic) IBOutlet UILabel *lblOpenForDating;

@property (strong, nonatomic) IBOutlet UISwitch *objDatingSwitch;
@property (strong, nonatomic) IBOutlet UILabel *lblInterests;

@property (strong, nonatomic) IBOutlet UIScrollView *interestScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imgLocationImage;
@property (strong, nonatomic) IBOutlet UIButton *btnLocationFind;
@property (strong, nonatomic) IBOutlet UIButton *btnCloseImage;
@property (strong, nonatomic) NSString *locationString;

// Date Picker View
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneBarBtn;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnButtonClick:(id)sender;

@end
