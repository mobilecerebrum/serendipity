#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRAchivementsViewController.h"
#import "SREditProfileViewController.h"
#import "SRMyProfileViewController.h"
#import "SRPingsViewController.h"

@interface SRProfileTabViewController : UITabBarController <UITabBarControllerDelegate>
{
	// Instance variables
	//UIButton *rightButton;
}

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil;

@end
