#import "SRProfileTabViewController.h"

@implementation SRProfileTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//-----------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    // Call super
    self = [super initWithNibName:@"SRProfileTabViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
    }

    // Return
    return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//-----------------------------------------------------------------------------------------------------------
// Standard Overrides

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Register notification
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(getProfileDetailsSucceed:)
                          name:kKeyNotificationCompleteSignUPSucceed
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getProfileDetailsFailed:)
                          name:kKeyNotificationCompleteSignUPFailed
                        object:nil];

    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];
    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f], NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f], NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];

    // Set Profile tab
    SRMyProfileViewController *objSRProfile = [[SRMyProfileViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-user-female.png"];
    UITabBarItem *objSRProfileTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.profile.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-user-female-active.png"]];
    [objSRProfileTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRProfile.tabBarItem = objSRProfileTabItem;
    objSRProfileTabItem.tag = 0;

    /*
	//  Set Achivment tab
	SRAchivementsViewController *objSRAchivementsView = [[SRAchivementsViewController alloc]initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
	tabImage = [UIImage imageNamed:@"tabbar-achievement.png"];
	UITabBarItem *SRAchivementsViewTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"tab.achivements.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-achievement-active.png"]];
	[SRAchivementsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
	objSRAchivementsView.tabBarItem = SRAchivementsViewTabItem;
	SRAchivementsViewTabItem.tag = 1;
     */

    //Set Pings tab
    SRPingsViewController *objSRPingsView = [[SRPingsViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-ping.png"];
    UITabBarItem *SRPingsViewTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.ping.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-ping-active.png"]];
    [SRPingsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRPingsView.tabBarItem = SRPingsViewTabItem;

//	self.viewControllers = [NSArray arrayWithObjects:objSRProfile, objSRAchivementsView, objSRPingsView, nil];
    self.viewControllers = @[objSRProfile, objSRPingsView];
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    self.selectedViewController = self.viewControllers[0];
    // Title
    [SRModalClass setNavTitle:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"first_name"] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    self.tabBar.unselectedItemTintColor = appTintColor;

//	UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
//	[leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark Action Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    //[SRModalClass hideTabBar:self];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:

- (void)getProfileDetailsSucceed:(NSNotification *)inNotify {
    // Title
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass setNavTitle:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"first_name"] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
}

// --------------------------------------------------------------------------------
// getProfileDetailsFailed:

- (void)getProfileDetailsFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}


@end
