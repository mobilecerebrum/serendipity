
#import "ProfileViewController.h"
const CGFloat kScrollObjHeight	= 568.0;
const CGFloat kScrollObjWidth	= 320.0;
const NSUInteger kNumImages		= 5;


@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict {
    self = [super initWithNibName:@"ProfileViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark
//---------------------------------------------------------------------------------------------------------------------------------
//Standard Methods
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addImagesInScrollview];
    
    
    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Title
    [SRModalClass setNavTitle:@"Profile" forViewNavCon:self.navigationItem];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    if (self.imgFamily1) {
              [self.FamilyPhotosScrollview setContentSize:CGSizeMake(400, 67)];
       
    }
    else
    {
        [self.ConnectionPhotosScrollview setContentSize:CGSizeMake(400, 67)];
    }
    
    
    //code for appnendting two string for Profile label address.
    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc]initWithString:@"26, Lawyer in "];
    NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@" New York" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Italic" size:12]}];
    [muAtrStr appendAttributedString:atrStr];
    
    self.lblProfileAddress.numberOfLines = 0;
    [self.lblProfileAddress setAttributedText:muAtrStr];
    ReadMoreBtnOldSizeY = self.btnReadMore.frame.origin.y;
   
}

- (void)actionOnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ------------------------------------------------------------------------------------------------------
// btnReadMoreClick:
-(IBAction)btnReadMoreClick:(id)sender
{
    NSUInteger lenghth = self.lblProfileDesc.text.length;
    NSLog(@"Length=%lu",(unsigned long)lenghth);
    float oldheight = self.lblProfileDesc.frame.size.height;
    
     NSLog(@"Size=%f", self.lblProfileDesc.frame.size.height);
    if (self.lblProfileDesc.text.length >= 135) {
        self.btnReadMore.hidden=NO;
        if ([self.btnReadMore.titleLabel.text  isEqualToString: @" << "] )
        {
            CGRect frame = self.lblProfileDesc.frame;
            self.lblProfileDesc.text= @"";
            self.lblProfileDesc.text=[self.lblProfileDesc.text stringByAppendingPathComponent:@"Lorem ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis Lorem ipsum dolor sit er elit lamet..."];
            
            CGRect textRect = [self.lblProfileDesc.text boundingRectWithSize:CGSizeMake(self.lblProfileDesc.frame.size.width, CGFLOAT_MAX)
                                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                                  attributes:@{NSFontAttributeName:self.lblProfileDesc.font}
                                                                     context:nil];
            
            CGRect size = textRect;
            self.lblProfileDesc.frame = size;
            NSLog(@"Size=%f", self.lblProfileDesc.frame.size.height);
            float newheight = self.lblProfileDesc.frame.size.height;
            float extraheight=newheight - oldheight;
            NSLog(@"new Size=%f", extraheight);
            self.lblProfileDesc.frame = CGRectMake(frame.origin.x,
                                                   frame.origin.y,
                                                   frame.size.width,
                                                   self.lblProfileDesc.frame.size.height);
            
            self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x,
                                                ReadMoreBtnOldSizeY,
                                                self.btnReadMore.frame.size.width,
                                                self.btnReadMore.frame.size.height);
            [self.btnReadMore setTitle:@"read more" forState:UIControlStateNormal];
            
            CGRect fitRectForTextView = CGRectMake(self.objDatingView.frame.origin.x, self.objDatingView.frame.origin.y + extraheight, self.objDatingView.frame.size.width,self.objDatingView.frame.size.height);
            self.objDatingView.frame = fitRectForTextView;
            
            CGRect fitRectForTextView1 = CGRectMake(self.objFamilyView.frame.origin.x, self.objFamilyView.frame.origin.y + extraheight, self.objFamilyView.frame.size.width,self.objFamilyView.frame.size.height);
            self.objFamilyView.frame=fitRectForTextView1;
            
            CGRect fitRectForTextView2 = CGRectMake(self.objConnectionView.frame.origin.x, self.objConnectionView.frame.origin.y + extraheight, self.objConnectionView.frame.size.width,self.objConnectionView.frame.size.height);
            self.objConnectionView.frame = fitRectForTextView2;
            
            self.ProfileDetailsScrollview.contentSize = CGSizeMake(self.ProfileDetailsScrollview.frame.size.width, self.ProfileDetailsScrollview.frame.size.height + extraheight + self.objDatingView.frame.size.height + self.objFamilyView.frame.size.height + self.objConnectionView.frame.size.height);

        }
        else
        {
            
            
            //self.lblProfileDesc.frame = size;
            
            CGRect frame = self.lblProfileDesc.frame;
            
           self.lblProfileDesc.text=[self.lblProfileDesc.text stringByAppendingPathComponent:@"check sum ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis nostrud exercitation ullamco laboris nisi ut aliquip adipisicing.26, Lawyer in New York Lorem ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis nostrud exercitation ullamco laboris nisi ut aliquip adipisicing."];
            
            CGRect textRect = [self.lblProfileDesc.text boundingRectWithSize:CGSizeMake(self.lblProfileDesc.frame.size.width, CGFLOAT_MAX)
                                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                                  attributes:@{NSFontAttributeName:self.lblProfileDesc.font}
                                                                     context:nil];
            
            CGRect size = textRect;
            self.lblProfileDesc.frame = size;
            
            float newheight = self.lblProfileDesc.frame.size.height;
            float extraheight=newheight - oldheight;
            NSLog(@"new Size=%f", extraheight);
            self.lblProfileDesc.frame = CGRectMake(frame.origin.x,
                                                   frame.origin.y,
                                                   frame.size.width,
                                                   self.lblProfileDesc.frame.size.height);
            self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x ,
                                                ReadMoreBtnOldSizeY + extraheight +self.btnReadMore.frame.size.height + 5,
                                                self.btnReadMore.frame.size.width,
                                                self.btnReadMore.frame.size.height);
            [self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
            
            CGRect fitRectForTextView = CGRectMake(self.objDatingView.frame.origin.x, self.objDatingView.frame.origin.y + extraheight, self.objDatingView.frame.size.width,self.objDatingView.frame.size.height);
            self.objDatingView.frame = fitRectForTextView;
            
            CGRect fitRectForTextView1 = CGRectMake(self.objFamilyView.frame.origin.x, self.objFamilyView.frame.origin.y + extraheight, self.objFamilyView.frame.size.width,self.objFamilyView.frame.size.height);
            self.objFamilyView.frame=fitRectForTextView1;
            
            CGRect fitRectForTextView2 = CGRectMake(self.objConnectionView.frame.origin.x, self.objConnectionView.frame.origin.y + extraheight, self.objConnectionView.frame.size.width,self.objConnectionView.frame.size.height);
            self.objConnectionView.frame = fitRectForTextView2;
            
            
            self.ProfileDetailsScrollview.contentSize = CGSizeMake(self.ProfileDetailsScrollview.frame.size.width, self.ProfileDetailsScrollview.frame.size.height + extraheight + self.objDatingView.frame.size.height + self.objFamilyView.frame.size.height + self.objConnectionView.frame.size.height);
        }
    }
    else
    {
        self.btnReadMore.hidden=YES;
    }
    
  
}



// ------------------------------------------------------------------------------------------------------
// btnAlbumClick:

-(IBAction)btnAlbumClick:(id)sender
{
  /*
    [UIView animateWithDuration:2.0
                          delay:0.5
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                        self.objProfileDetailUIview.frame = CGRectMake(0, 700, 320, 460);
                     }
                     completion:^(BOOL finished)
                        {
                         if (finished)
                             [self.objProfileDetailUIview removeFromSuperview];
                         self.objBackGroundScrollview.pagingEnabled = YES;
                         NSArray *colors = [NSArray arrayWithObjects:[UIColor colorWithPatternImage:[UIImage imageNamed:@"menu_back.png"]],[UIColor redColor], [UIColor greenColor], [UIColor blueColor],[UIColor orangeColor],[UIColor purpleColor], nil];
                         for (int i = 0; i < colors.count; i++) {
                             CGRect frame;
                             frame.origin.x = self.objBackGroundScrollview.frame.size.width * i;
                             frame.origin.y = 0;
                             frame.size = self.objBackGroundScrollview.frame.size;
                             
                             UIView *subview = [[UIView alloc] initWithFrame:frame];
                             
                             UILabel *countLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
                             countLbl.font=[UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
                             NSString *str1 = [NSString stringWithFormat:@"%d of 5",i];
                             countLbl.textColor = [UIColor whiteColor];
                             countLbl.text=str1;
                             [subview addSubview:countLbl];
                             
                             UIButton *ContactBtn=[[UIButton alloc] initWithFrame:CGRectMake(275, 5, 25, 25)];
                             [ContactBtn setBackgroundImage:[UIImage imageNamed:@"tabbar-contact.png"] forState:UIControlStateNormal];
                             [ContactBtn addTarget:self action:@selector(ContactBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                             [subview addSubview:ContactBtn];
                            
                             subview.backgroundColor = [colors objectAtIndex:i];
                            [self.objBackGroundScrollview addSubview:subview];
                           
                         }
                         
                        self.objBackGroundScrollview.contentSize = CGSizeMake(self.objBackGroundScrollview.frame.size.width * colors.count, self.objBackGroundScrollview.frame.size.height);
                     
                     }];
                     [self.view addSubview:self.objBackGroundScrollview];
    
    */
    
    
}

-(void)ContactBtnClick:(id)sender
{
    [self.objBackGroundScrollview clearsContextBeforeDrawing];
    [self.view addSubview:self.objProfileDetailUIview];
}

-(void)addImagesInScrollview
{
    self.FamilyPhotosScrollview.scrollEnabled = YES;
 
    
    float x = 10;
    float y = 0;
    float width = 40;
    float height = 40;
    for(int i =0;i<5;i++)
    {
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setFrame:CGRectMake(x, y, width, height)];
        imageView.image=[UIImage imageNamed:[NSString stringWithFormat:
                                             @"image%d.jpg", i]];
        imageView.layer.cornerRadius = imageView.frame.size.width / 2;
        imageView.clipsToBounds = YES;
        [self.FamilyPhotosScrollview addSubview:imageView];
       
        
        UILabel *imageName=[[UILabel alloc] init];
        [imageName setFrame:CGRectMake(x, imageView.frame.size.height +5, width, 10)];
        imageName.font=[UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        NSString *str1 = [NSString stringWithFormat:@"image%d",i];
        imageName.textColor = [UIColor whiteColor];
        imageName.text=str1;
        [self.FamilyPhotosScrollview addSubview:imageName];
        
        
        x = x + width + 20;
        
        if((i+1)%5==0)
        {
            x = 10;
            //y = y + height + 20;
        }
    }
}

@end
