
#import "SRLocationSetViewController.h"
#import "SRAnnotationViewController.h"
#import "SREditProfileViewController.h"
@interface SRLocationSetViewController ()

@end

@implementation SRLocationSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
     self.locationManager = [[CLLocationManager alloc]init];
    if ([CLLocationManager locationServicesEnabled])
    {
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter= 1000.0f;
        [self.locationManager startUpdatingLocation];
    }
    
    
    latitudeArray = [[NSMutableArray alloc]initWithObjects:@"51.5171",@"37.7873",@"21.2827", nil];
    longitudeArray= [[NSMutableArray alloc]initWithObjects:@"0.1062",@"-122.4082",@"-157.8294", nil];
    
    annotationArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<[latitudeArray count]; i++)
    {
        
        
        self.objAnnotationView = [[SRAnnotationViewController alloc] init];
        CLLocationCoordinate2D coordinateValues;
        coordinateValues.latitude = [[latitudeArray objectAtIndex:i] floatValue];
        coordinateValues.longitude = [[longitudeArray objectAtIndex:i] floatValue];
        self.objAnnotationView.coordinate = coordinateValues;
        
        MKCoordinateSpan spanOfMap;
        spanOfMap.latitudeDelta = .0005;
        spanOfMap.longitudeDelta = .0005;
       
        MKCoordinateRegion regionObj;
        regionObj.center = coordinateValues;
        regionObj.span= spanOfMap;
        
        self.objAnnotationView.title=@"N";
        self.objAnnotationView.subtitle= @"P";
        [annotationArray insertObject:self.objAnnotationView atIndex:i];
        [self.mapViewObj addAnnotations:annotationArray];
        //[self.mapViewObj setRegion:regionObj animated:YES];
        self.objAnnotationView = nil;
    }

    
   
    
    
    self.mapViewObj.centerCoordinate = CLLocationCoordinate2DMake(37.32, -122.03);
    self.mapViewObj.showsUserLocation = YES;
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    
    //    //For Map
    MKCoordinateSpan spanOfMap;
    spanOfMap.latitudeDelta = 100;
    spanOfMap.longitudeDelta = 100;
    
    MKCoordinateRegion regionObj;
    regionObj.center = newLocation.coordinate;
    regionObj.span= spanOfMap;
    
    //For dropping pin on current location
    self.objAnnotationView = [[SRAnnotationViewController alloc] init];
    self.mapViewObj.delegate= self;
    
    
    CLLocationCoordinate2D coordinateValues;
    coordinateValues.latitude = newLocation.coordinate.latitude;
    coordinateValues.longitude = newLocation.coordinate.longitude;
    self.objAnnotationView.coordinate = coordinateValues;
    self.objAnnotationView.title = @"John";
    self.objAnnotationView.subtitle = @"Pune";
    [self.mapViewObj addAnnotation:self.objAnnotationView];
   // [self.mapViewObj setRegion:regionObj animated:YES];
    
}
#pragma mark MapView Delegates

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    static NSString *AnnotationIdentifier = @"AnnotationIdentifier";
    MKPinAnnotationView *pinView = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
    pinView.animatesDrop = YES;
    pinView.canShowCallout = YES;
    pinView.pinColor = MKPinAnnotationColorPurple;
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    pinView.rightCalloutAccessoryView = rightBtn;
    
    UIImageView *imageViewObj = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"profilepic.jpeg"]];
    imageViewObj.frame = CGRectMake(5, 5, 30, 30);
    pinView.leftCalloutAccessoryView=imageViewObj;
    
    return pinView;
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    SREditProfileViewController *objSREditProfileViewController = [[SREditProfileViewController alloc]initWithNibName:@"SREditProfileViewController" bundle:nil];
    objSREditProfileViewController.locationString=@"NewLoc";
    [self.navigationController pushViewController:objSREditProfileViewController animated:YES];
}
//method for drag and drop pin
- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
   fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding)
    {
        CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
        NSLog(@"dropped at %f,%f", droppedAt.latitude, droppedAt.longitude);
    }
}
@end
