

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapKit/MapKit.h"
@class SRAnnotationViewController;
@class SREditProfileViewController;
@interface SRLocationSetViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate>
{
  
    
    
    NSMutableArray *latitudeArray;
    NSMutableArray *longitudeArray;
    NSMutableArray *annotationArray;
}
@property(nonatomic,strong)IBOutlet MKMapView *mapViewObj;
@property(nonatomic,strong)SRAnnotationViewController *objAnnotationView;
@property(nonatomic,strong)CLLocationManager *locationManager;

@end
