#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRServerConnection.h"
#import "SRLocationSetViewController.h"
@interface SREditProfileViewController : UIViewController
{
	// Instance variable
    // Model
    NSMutableArray *profileImgsArr;
    NSMutableArray *interestImgsArr;
    
    // Server
    SRServerConnection *server;
}

// Properties
@property (strong, nonatomic) IBOutlet UIImageView *imgScreenBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackgroundPhotoImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackgroundColorImage;
@property (strong, nonatomic) IBOutlet UIButton *btnEditImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (strong, nonatomic) IBOutlet UIScrollView *objProfileImagesScrollview;
@property (strong, nonatomic) IBOutlet UIScrollView *objDetailsScrollview;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfileDetailBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgYourNameBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtYourName;
@property (strong, nonatomic) IBOutlet UIImageView *imgDateBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtDate;
@property (strong, nonatomic) IBOutlet UIImageView *imgOccupationBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtOccupation;
@property (strong, nonatomic) IBOutlet UIImageView *imgLocationBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imgSmAboutYouBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtSmAboutYou;
@property (strong, nonatomic) IBOutlet UIImageView *imgWebsiteBGImage;
@property (strong, nonatomic) IBOutlet UITextField *txtWebsiteURL;
@property (strong, nonatomic) IBOutlet UIImageView *imgDatingImage;
@property (strong, nonatomic) IBOutlet UILabel *lblOpenForDating;
@property (strong, nonatomic) IBOutlet UISwitch *objDatingSwitch;
@property (strong, nonatomic) IBOutlet UILabel *lblInterests;
@property (strong, nonatomic) IBOutlet UIScrollView *objInterestsImagesScrollview;
@property (strong, nonatomic) IBOutlet UIButton *btnAddImage;
@property (strong, nonatomic) IBOutlet UIScrollView *interestScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imgLocationImage;
@property (strong, nonatomic) IBOutlet UIButton *btnLocationFind;
@property (strong, nonatomic) NSString *locationString;
#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
-(IBAction)btnLocationFindAction:(id)sender;
@end
