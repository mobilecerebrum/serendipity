#import "SREditProfileViewController.h"

@implementation SREditProfileViewController

#pragma mark
#pragma mark Scroll Customization Method
#pragma mark

// --------------------------------------------------------------------------------
// addItemsToScrollView:

- (void)addItemsToScrollView:(BOOL)isProfile {
	NSArray *imgsArr;
	if (isProfile) {
		imgsArr = profileImgsArr;
	}
	else
		imgsArr = interestImgsArr;

	CGRect latestFrame = CGRectZero;

	// Add images to scroll view accordingly
	for (NSUInteger i = 0; i < 5; i++) {
		UIView *subView = [[UIView alloc]init];
		if (i == 0) {
			subView.frame = CGRectMake(5, 0, 65, 70);
		}
		else
			subView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 4, 0, 65, 70);
		latestFrame = subView.frame;
        subView.backgroundColor = [UIColor clearColor];
        
		// Add image view
		UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 65, 65)];
		imgView.contentMode = UIViewContentModeScaleAspectFit;
		imgView.image = nil;
        imgView.image=[UIImage imageNamed:@"menu_familyperson_image.png"];
        if (!isProfile) {
            imgView.image=[UIImage imageNamed:@"menu_familyperson_image.png"];
            imgView.contentMode=UIViewContentModeScaleToFill;
            imgView.layer.cornerRadius = imgView.frame.size.width / 2;
            imgView.clipsToBounds = YES;
            
            UILabel *lblImageName = [[UILabel alloc]initWithFrame:CGRectMake(imgView.frame.origin.x + 20, 64, 60, 15)];
            lblImageName.textColor=[UIColor whiteColor];
            lblImageName.font = [UIFont fontWithName:@"HelveticaNeue" size:11.0];
            lblImageName.text = [NSString stringWithFormat:@"image%ld",i];
            [subView addSubview:lblImageName];
        }
        else
        {
        imgView.backgroundColor = [UIColor orangeColor];
        }
		[subView addSubview:imgView];

		UIButton *btnDelete = [[UIButton alloc]initWithFrame:CGRectMake(imgView.frame.origin.x + 46, -2, 20, 20)];
		[btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
		btnDelete.tag = i;
		[btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];
        //btnDelete.backgroundColor = [UIColor blueColor];
		[subView addSubview:btnDelete];

		// Add to the scroll view according to flag
		if (isProfile) {
			[self.objProfileImagesScrollview addSubview:subView];
		}
		else
			[self.interestScrollView addSubview:subView];
	}

	// Check if lastest frame is not empty
	if (CGRectEqualToRect(latestFrame, CGRectZero)) {
		latestFrame = CGRectMake(5, 0, 65, 70);
	}
    else
        latestFrame = CGRectMake(latestFrame.origin.x + 70, 0, 65, 70);
	UIView *btnSuperView = [[UIView alloc]init];
	btnSuperView.frame = latestFrame;
	UIButton *btnAdd = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 65, 70)];
	[btnAdd setImage:[UIImage imageNamed:@"plus-125px.png"] forState:UIControlStateNormal];
	[btnAdd addTarget:self action:@selector(actionOnBtnAdd:) forControlEvents:UIControlEventTouchUpInside];
	[btnSuperView addSubview:btnAdd];

	CAShapeLayer *border = [CAShapeLayer layer];
	border.strokeColor = [UIColor lightGrayColor].CGColor;
	border.fillColor = nil;
	border.lineDashPattern = @[@4, @2];
	[btnAdd.layer addSublayer:border];
    btnAdd.backgroundColor = [UIColor greenColor];

	// Add to scroll view
	if (isProfile) {
		[self.objProfileImagesScrollview addSubview:btnSuperView];
	}
	else
		[self.interestScrollView addSubview:btnSuperView];

	if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
		if (isProfile) {
			self.objProfileImagesScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
			self.objProfileImagesScrollview.scrollEnabled = YES;
		}
		else {
			self.interestScrollView.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
			self.interestScrollView.scrollEnabled = YES;
		}
	}
}

// -----------------------------------------------------------------------------------------------
// actionOnBtnDelete:

- (void)actionOnBtnDelete:(UIButton *)inSender {
    
        [profileImgsArr removeObjectAtIndex:(NSUInteger )inSender];
    [self addItemsToScrollView:YES];
}

// -----------------------------------------------------------------------------------------------
// actionOnBtnAdd:

- (void)actionOnBtnAdd:(UIButton *)inSender {
}

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
	// End editing
	[self.view endEditing:YES];
	[self.objDetailsScrollview setContentOffset:CGPointMake(0, 0)];
}

// --------------------------------------------------------------------------------
//  showAlert:

- (void)showAlert:(NSString *)inErrorStr {
	UIAlertView *alertView = [[UIAlertView alloc]
	                          initWithTitle:NSLocalizedString(@"alert.title.text", "")
	                                    message:inErrorStr
	                                   delegate:nil
	                          cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", "")
	                          otherButtonTitles:nil, nil];
	[alertView show];
}

// -----------------------------------------------------------------------------------------------------
// updateTextField

- (void)updateTextField {
	UIDatePicker *picker = (UIDatePicker *)self.txtDate.inputView;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy"];
    NSString *formattedDate = [df stringFromDate:picker.date];
    self.txtDate.text =formattedDate;
   // [picker removeFromSuperview];
}

// -----------------------------------------------------------------------------------------------------
// validateUrl:

- (BOOL)validateUrl:(NSString *)candidate {
	NSString *urlRegEx =
	    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
	NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
	return [urlTest evaluateWithObject:candidate];
}

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
	self = [super initWithNibName:@"SREditProfileViewController" bundle:nil];
	if (self) {
		server = inServer;

		profileImgsArr = [[NSMutableArray alloc]init];
		interestImgsArr = [[NSMutableArray alloc]init];
	}

	// Return
	return self;
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
	[super viewDidLoad];

	// Set navigation bar
	self.navigationController.navigationBar.hidden = NO;
	self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
	self.navigationController.navigationBar.translucent = NO;

	// Title
	[SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.editProfile", " ") forViewNavCon:self.navigationItem];

	UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
	[leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
	UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-man-pencil.png"] forViewNavCon:self offset:-23];
	[rightButton addTarget:self action:@selector(editBtnAction:) forControlEvents:UIControlEventTouchUpInside];


	//textFields Placehoders
	UIColor *color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
	self.txtYourName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.yourName", @"") attributes:@{ NSForegroundColorAttributeName: color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0] }];
	self.txtDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.date", @"") attributes:@{ NSForegroundColorAttributeName: color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:11.0] }];
	self.txtOccupation.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.occupation", @"") attributes:@{ NSForegroundColorAttributeName: color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:11.0] }];
	self.txtLocation.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.location", @"") attributes:@{ NSForegroundColorAttributeName: color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:11.0] }];
	self.txtSmAboutYou.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.smAboutYou", @"") attributes:@{ NSForegroundColorAttributeName: color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:15.0] }];
	self.txtWebsiteURL.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"placeholder.websiteURL", @"") attributes:@{ NSForegroundColorAttributeName: color, NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:15.0] }];
    self.lblOpenForDating.text = NSLocalizedString(@"lbl.openForDating.txt", @"");
    self.lblInterests.text = NSLocalizedString(@"lbl.interests.txt", @"");
  
    //set corner radius to imageviews;
    // Get the Layer of any view
    self.imgYourNameBGImage.layer.cornerRadius = 5.0;
    self.imgYourNameBGImage.layer.masksToBounds = YES;
    self.imgDateBGImage.layer.cornerRadius = 5.0;
    self.imgDateBGImage.layer.masksToBounds = YES;
    self.imgOccupationBGImage.layer.cornerRadius = 5.0;
    self.imgOccupationBGImage.layer.masksToBounds = YES;
    self.imgLocationBGImage.layer.cornerRadius = 5.0;
    self.imgLocationBGImage.layer.masksToBounds = YES;
    self.imgSmAboutYouBGImage.layer.cornerRadius = 5.0;
    self.imgSmAboutYouBGImage.layer.masksToBounds = YES;
    self.imgWebsiteBGImage.layer.cornerRadius = 5.0;
    self.imgWebsiteBGImage.layer.masksToBounds = YES;
    
	// Set gesture to dismiss keyboard
	UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
	[self.objDetailsScrollview addGestureRecognizer:tapGesture];
	[tapGesture setCancelsTouchesInView:YES];
    
    // Load images to scroll
    [self addItemsToScrollView:YES];
    
    //Dating Switch Background
    self.objDatingSwitch.tintColor = [UIColor whiteColor];
    [self.objDatingSwitch setOnTintColor:[UIColor orangeColor]];
    self.objDatingSwitch.backgroundColor = [UIColor lightGrayColor];
    self.objDatingSwitch.layer.cornerRadius = 16.0f;
    [self.objDatingSwitch addTarget:self action:@selector(datingSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    
    
    
    //Date Picker
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(100, 100, 200, 200)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.backgroundColor = [UIColor blueColor];
    [datePicker addTarget:self action:@selector(updateTextField) forControlEvents:UIControlEventValueChanged];
    self.txtDate.inputView = datePicker;
    
//    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, -20,self.view.bounds.size.width,40)];
//    [pickerToolbar sizeToFit];
//    pickerToolbar.barStyle = UIBarStyleBlackTranslucent;
//    NSMutableArray *barItems = [[NSMutableArray alloc] init];
//    
//    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonSystemItemCancel target:self action:@selector(cancel_clicked:)];
//    [barItems addObject:cancelBtn];
//    
//    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    [barItems addObject:flexSpace];
//    
//    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done_clicked:)];
//    [barItems addObject:doneBtn];
//    
//    
//    [pickerToolbar setItems:barItems animated:YES];
//    
//    [datePicker addSubview:pickerToolbar];

    
    
    
}

#pragma mark
#pragma mark Action Methods
#pragma mark
// ---------------------------------------------------------------------------------------------------------------------------------
// datingSwitchAction:
-(void)datingSwitchAction:(id)Sender
{
    
}
// ---------------------------------------------------------------------------------------------------------------------------------
// backBtnAction:

- (void)actionOnBack:(id)sender {
	//(APP_DELEGATE).isZero = YES;
	[self.navigationController popViewControllerAnimated:YES];
}

// -----------------------------------------------------------------------------------------------------
// editBtnAction:

- (void)editBtnAction:(id)sender {
}

#pragma mark
#pragma mark TextFields Delegates
#pragma mark

// -----------------------------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.txtYourName) {
		[self.txtYourName resignFirstResponder];
	}
	else if (textField == self.txtDate) {
		[self.txtDate resignFirstResponder];
	}
	else if (textField == self.txtOccupation) {
		[self.txtOccupation resignFirstResponder];
	}
	else if (textField == self.txtLocation) {
		[self.txtLocation resignFirstResponder];
	}
	else if (textField == self.txtSmAboutYou) {
		[self.txtSmAboutYou resignFirstResponder];
	}
	else if (textField == self.txtWebsiteURL) {
		[self.txtWebsiteURL resignFirstResponder];
	}

	// Return
	return YES;
}


// -----------------------------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	//[textField setReturnKeyType:UIReturnKeyDone];
self.objDetailsScrollview.contentSize = CGSizeMake(self.objDetailsScrollview.frame.size.width, self.objDetailsScrollview.frame.size.height + 200);
    
	if (textField == self.txtYourName) {
        self.objDetailsScrollview.contentOffset = CGPointMake(0, textField.frame.origin.y + 50);
     
	}
	else if (textField == self.txtDate) {
        
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(100, 100, 200, 200)];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.backgroundColor = [UIColor whiteColor];
        [datePicker addTarget:self action:@selector(updateTextField) forControlEvents:UIControlEventValueChanged];
        self.txtDate.inputView = datePicker;
		[textField resignFirstResponder];
        self.objDetailsScrollview.contentOffset = CGPointMake(0, textField.frame.origin.y);
	}
	else if (textField == self.txtOccupation) {
		self.objDetailsScrollview.contentOffset = CGPointMake(0, textField.frame.origin.y);
	}
	else if (textField == self.txtLocation) {
	self.objDetailsScrollview.contentOffset = CGPointMake(0, textField.frame.origin.y);
	}
	else if (textField == self.txtSmAboutYou) {
        self.objDetailsScrollview.contentSize = CGSizeMake(self.objDetailsScrollview.frame.size.width, self.objDetailsScrollview.frame.size.height + 400);
	self.objDetailsScrollview.contentOffset = CGPointMake(0, textField.frame.origin.y);
	}
	else if (textField == self.txtWebsiteURL) {
		self.objDetailsScrollview.contentOffset = CGPointMake(0, textField.frame.origin.y);
	}
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)    textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
    replacementString:(NSString *)string {
	BOOL isValid = YES;

	NSString *errorMsg = nil;
	NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];
	if (textField == self.txtYourName) {
		if ([textStrToCheck length] > 5) {
			errorMsg = NSLocalizedString(@"yourName.char.error", @"");
		}
	}

	if (textField == self.txtOccupation) {
		if ([textStrToCheck length] > 5) {
			errorMsg = NSLocalizedString(@"occupation.char.error", @"");
		}
		else if (textField == self.txtLocation) {
		}
		else if (textField == self.txtDate) {
			
		}
	}
	if (textField == self.txtWebsiteURL) {
		BOOL flag = [self validateUrl:textStrToCheck];
		if (flag) {
			errorMsg = NSLocalizedString(@"invalid.url.alert.text", "");
		}
	}
	if (textField == self.txtSmAboutYou) {
		if ([textStrToCheck length] > 5) {
			errorMsg = NSLocalizedString(@"smAboutYou.char.error", @"");
		}
	}

	// Set flag and show alert
	if (errorMsg) {
		isValid = NO;
		[self showAlert:errorMsg];
	}

	// Return
	return isValid;
}

#pragma mark
#pragma mark Touch Delegates
#pragma mark

// ---------------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[[self view] endEditing:YES];
	[self.objDetailsScrollview setContentOffset:CGPointMake(0, 0)];
}


#pragma mark
#pragma mark btnLocationFindAction
#pragma mark

// -----------------------------------------------------------------------------------------------------
//   Find Location Action


-(IBAction)btnLocationFindAction:(id)sender
{
    SRLocationSetViewController *objLocationView=[[SRLocationSetViewController alloc] initWithNibName:@"SRLocationSetViewController" bundle:nil];
    
    [self.navigationController pushViewController:objLocationView animated:YES];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if (![self.locationString isEqualToString:@""]) {
        self.txtLocation.text = self.locationString;
    }
}
@end
