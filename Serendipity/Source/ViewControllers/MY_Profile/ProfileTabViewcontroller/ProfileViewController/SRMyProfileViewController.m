#import "SRMyProfileViewController.h"

const CGFloat kScrollObjHeight  = 568.0;
const CGFloat kScrollObjWidth   = 320.0;
const NSUInteger kNumImages     = 5;

@implementation SRMyProfileViewController

#pragma mark
#pragma mark Private Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// setScrollContentSize

- (void)setScrollContentSize {
	CGRect frame = self.lblProfileDesc.frame;
	CGRect textRect = [self.lblProfileDesc.text boundingRectWithSize:CGSizeMake(self.lblProfileDesc.frame.size.width, CGFLOAT_MAX)
	                                                         options:NSStringDrawingUsesLineFragmentOrigin
	                                                      attributes:@{ NSFontAttributeName:self.lblProfileDesc.font }
	                                                         context:nil];

	frame.size.height = textRect.size.height;
	self.lblProfileDesc.frame = frame;
	self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x,
	                                    self.lblProfileDesc.frame.origin.y + self.lblProfileDesc.frame.size.height - 10,
	                                    self.btnReadMore.frame.size.width,
	                                    self.btnReadMore.frame.size.height);
	frame = self.objDatingView.frame;
	frame.origin.y = self.lblProfileDesc.frame.origin.y + self.lblProfileDesc.frame.size.height + 8;
	self.objDatingView.frame = frame;

	frame = self.objFamilyView.frame;
	frame.origin.y = self.objDatingView.frame.origin.y + self.objDatingView.frame.size.height + 8;
	self.objFamilyView.frame = frame;

	frame = self.objConnectionView.frame;
	frame.origin.y = self.objFamilyView.frame.origin.y + self.objFamilyView.frame.size.height + 8;
	self.objConnectionView.frame = frame;

	self.profileDetailsScrollview.contentSize = CGSizeMake(0,  self.objConnectionView.frame.origin.y + self.objConnectionView.frame.size.height + 50);
	self.profileDetailsScrollview.scrollEnabled = YES;
}

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict {
	// Call super
	self = [super initWithNibName:@"SRMyProfileViewController" bundle:nibBundleOrNil];

	if (self) {
		// Custom initialization
	}

	// Return
	return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
	// Call super
	[super viewDidLoad];

    //call method for family & Connection photoArray in scrollview
    [self addImagesInScrollview];
    
	// Prepare view design
	detailViewFrame = self.profileDetailsScrollview.frame;
   
	// Code for appnendting two string for Profile label address.
	NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc]initWithString:@"26, Lawyer in "];
	NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@" New York" attributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaItalicMedium size:12] }];
	[muAtrStr appendAttributedString:atrStr];

	self.lblProfileAddress.numberOfLines = 0;
	[self.lblProfileAddress setAttributedText:muAtrStr];

	// Display images in scroll view
	NSArray *colors = [NSArray arrayWithObjects:[UIColor colorWithPatternImage:[UIImage imageNamed:@"menu_back.png"]], [UIColor redColor], [UIColor greenColor], [UIColor blueColor], [UIColor orangeColor], [UIColor purpleColor], nil];
	for (int i = 0; i < colors.count; i++) {
		CGRect frame;
		frame.origin.x = self.objBackGroundScrollview.frame.size.width * i;
		frame.origin.y = 0;
		frame.size = self.objBackGroundScrollview.frame.size;

		UIView *subview = [[UIView alloc] initWithFrame:frame];
		UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
		imageView.contentMode = UIViewContentModeScaleToFill;
		[subview addSubview:imageView];
		imageView.image = [UIImage imageNamed:@"menu_back.png"];
		UILabel *countLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
		countLbl.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
		NSString *str1 = [NSString stringWithFormat:@"%d of 5", i];
		countLbl.textColor = [UIColor whiteColor];
		countLbl.text = str1;
		[subview addSubview:countLbl];

		UIButton *ContactBtn = [[UIButton alloc] initWithFrame:CGRectMake(275, 5, 25, 25)];
		[ContactBtn setBackgroundImage:[UIImage imageNamed:@"tabbar-contact.png"] forState:UIControlStateNormal];
		[ContactBtn addTarget:self action:@selector(contactBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
		[subview addSubview:ContactBtn];

		if (i == 0) {
			lblCountOfImages = countLbl;
			lblCountOfImages.hidden = YES;
			btnProfileDetail = ContactBtn;
			btnProfileDetail.hidden = YES;
		}

		subview.backgroundColor = [colors objectAtIndex:i];
		[self.objBackGroundScrollview addSubview:subview];
	}
	self.objBackGroundScrollview.contentSize = CGSizeMake(self.objBackGroundScrollview.frame.size.width * colors.count, 0);
	self.objBackGroundScrollview.scrollEnabled = NO;

	// Bring view to front
	[self.objBackGroundScrollview bringSubviewToFront:self.btnAlbum];
    [self.objBackGroundScrollview bringSubviewToFront:self.imgViewBckScroll];
    [self.objBackGroundScrollview bringSubviewToFront:self.profileDetailsScrollview];
	[self setScrollContentSize];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ------------------------------------------------------------------------------------------------------
// btnReadMoreClick:
- (IBAction)btnReadMoreClick:(id)sender {
	if (self.lblProfileDesc.text.length >= 135) {
		self.btnReadMore.hidden = NO;
		if ([self.btnReadMore.titleLabel.text isEqualToString:@" << "]) {
			self.lblProfileDesc.text = @"Lorem ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis Lorem ipsum dolor sit er elit lamet...";
			[self.btnReadMore setTitle:@"read more" forState:UIControlStateNormal];
		}
		else {
			self.lblProfileDesc.text = @"check sum ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis nostrud exercitation ullamco laboris nisi ut aliquip adipisicing.26, Lawyer in New York Lorem ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis nostrud exercitation ullamco laboris nisi ut aliquip adipisicing adipisicing pecu, quis Lorem ipsum dolor sit er elit adipisicing pecu, quis Lorem ipsum dolor sit er elit .";

			[self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
		}
		[self setScrollContentSize];
	}
	else {
		self.btnReadMore.hidden = YES;
	}
}

// ------------------------------------------------------------------------------------------------------
// btnAlbumClick:

- (IBAction)btnAlbumClick:(id)sender {
	lblCountOfImages.hidden = NO;
	btnProfileDetail.hidden = NO;
	self.btnAlbum.hidden = YES;
	self.imgViewBckScroll.hidden=YES;
	self.objBackGroundScrollview.scrollEnabled = YES;
    self.profileDetailsScrollview.frame = CGRectMake(0, 700, self.profileDetailsScrollview.frame.size.width, self.profileDetailsScrollview.frame.size.height);
    self.profileDetailsScrollview.hidden = YES;
    
    
    
}

// ------------------------------------------------------------------------------------------------------
// contactBtnClicked:

- (void)contactBtnClicked:(id)sender {
	lblCountOfImages.hidden = YES;
	btnProfileDetail.hidden = YES;
	self.btnAlbum.hidden = NO;
    self.imgViewBckScroll.hidden=NO;
    self.profileDetailsScrollview.frame = detailViewFrame;
    self.objBackGroundScrollview.scrollEnabled = NO;
	self.profileDetailsScrollview.hidden = NO;
    self.profileDetailsScrollview.scrollEnabled = YES;
    [self.objBackGroundScrollview bringSubviewToFront:self.profileDetailsScrollview];
	self.profileDetailsScrollview.contentSize = CGSizeMake(0, self.profileDetailsScrollview.frame.size.height + 100);
    
  
    
    
}


// ------------------------------------------------------------------------------------------------------
// addImagesInScrollview:

- (void)addImagesInScrollview {
 
    float x = 5;
    float y = 0;
    float width = 40;
    float height = 40;
    
    
    for(int i =0;i<3;i++)
    {
//        if ([FamilyImageArray count]>5 || [ConnectionImageArrya count]>5) {
//            self.familyPhotosScrollview.scrollEnabled=YES;
//        }
        
          self.familyPhotosScrollview.scrollEnabled=NO;
         self.connectionPhotosScrollview.scrollEnabled=NO;
        UIImageView *objImageView=[[UIImageView alloc] init];
        [objImageView setFrame:CGRectMake(x, y, width, height)];
        objImageView.image = [UIImage imageNamed:@"menu_familyperson_image.png"];
        objImageView.contentMode=UIViewContentModeScaleToFill;
        objImageView.layer.cornerRadius = objImageView.frame.size.width / 2;
        objImageView.clipsToBounds = YES;
        [self.familyPhotosScrollview addSubview:objImageView];
        
        
        UILabel *lblImageName=[[UILabel alloc] init];
        [lblImageName setFrame:CGRectMake(x+5, y+24, width, height)];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        NSString *str1 = [NSString stringWithFormat:@"Image%d", i];
        lblImageName.textColor = [UIColor whiteColor];
        lblImageName.text = str1;
        [self.familyPhotosScrollview addSubview:lblImageName];
        
        
        self.familyPhotosScrollview.contentSize = CGSizeMake(self.familyPhotosScrollview.frame.size.width + 10, self.familyPhotosScrollview.frame.size.height);
        
        
       objImageView=[[UIImageView alloc] init];
        [objImageView setFrame:CGRectMake(x, y, width, height)];
        objImageView.image = [UIImage imageNamed:@"menu_familyperson_image.png"];

        
        lblImageName=[[UILabel alloc] init];
        [lblImageName setFrame:CGRectMake(x+5, y+24, width, height)];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        str1 = [NSString stringWithFormat:@"Image%d", i];
        lblImageName.textColor = [UIColor whiteColor];
        lblImageName.text = str1;
        
        
        [self.connectionPhotosScrollview addSubview:lblImageName];
        [self.connectionPhotosScrollview addSubview:objImageView];
        self.connectionPhotosScrollview.contentSize = CGSizeMake(0,detailViewFrame.size.height);
        
        x = x + width + 5;
        
        
    }
}


@end
