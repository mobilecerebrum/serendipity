#import <UIKit/UIKit.h>
#import "SRModalClass.h"

@interface SRMyProfileViewController : UIViewController<UIScrollViewDelegate>
{
    // Instance variable
    UILabel *lblCountOfImages;
    UIButton *btnProfileDetail;
    CGRect detailViewFrame;
}

// Properties
@property (nonatomic, strong) IBOutlet UIScrollView *objBackGroundScrollview;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileHeadName;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileAddress;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileDesc;
@property (nonatomic, strong) IBOutlet UIImageView *imgDating;
@property (nonatomic, strong) IBOutlet UILabel *lblDatingTitle;
@property (nonatomic, strong) IBOutlet UIButton *btnAlbum;
@property (nonatomic, strong) IBOutlet UIButton *btnReadMore;
@property (nonatomic, strong) IBOutlet UIScrollView *profileDetailsScrollview;
@property (nonatomic, strong) IBOutlet UIView *objDatingView;
@property (nonatomic, strong) IBOutlet UIView *objFamilyView;
@property (nonatomic, strong) IBOutlet UIView *objConnectionView;
@property (nonatomic, strong) IBOutlet UIScrollView *familyPhotosScrollview;
@property (nonatomic, strong) IBOutlet UIScrollView *connectionPhotosScrollview;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewBckScroll;
@property (nonatomic, strong) IBOutlet UILabel *lblFamilyCount;
@property (nonatomic, strong) IBOutlet UILabel *lblConnectionCount;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)btnAlbumClick:(id)sender;
- (IBAction)btnReadMoreClick:(id)sender;

@end
