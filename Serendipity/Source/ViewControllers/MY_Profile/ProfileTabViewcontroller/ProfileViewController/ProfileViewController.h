//
//  ProfileViewController.h
//  SerendipityDemo
//
//  Created by Sunil Dhokare on 7/22/15.
//  Copyright (c) 2015 Leo Tecnosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRModalClass.h"
@interface ProfileViewController : UIViewController<UIScrollViewDelegate>
{
  
    float ReadMoreBtnOldSizeY;
 
    
}
@property IBOutlet UIScrollView *objBackGroundScrollview;
@property IBOutlet UIImageView *ProfileBackgroundImage;
@property IBOutlet UILabel *lblProfileHeadName;
@property IBOutlet UILabel *lblProfileAddress;
@property IBOutlet UILabel *lblProfileDesc;
@property IBOutlet UIImageView *imgDating;
@property IBOutlet UILabel *lblDatingTitle;
@property IBOutlet UIImageView *imgFamily1;
@property IBOutlet UIImageView *imgFamily2;
@property IBOutlet UIImageView *imgFamily3;
@property IBOutlet UIButton *btnAlbum;
@property IBOutlet UIButton *btnReadMore;
@property IBOutlet UIScrollView *ProfileDetailsScrollview;
@property IBOutlet UIView *objDatingView;
@property IBOutlet UIView *objFamilyView;
@property IBOutlet UIView *objConnectionView;
@property IBOutlet UIScrollView *FamilyPhotosScrollview;
@property IBOutlet UIScrollView *ConnectionPhotosScrollview;
@property IBOutlet UIView *objProfileDetailUIview;
- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict;

-(IBAction)btnAlbumClick:(id)sender;
-(IBAction)btnReadMoreClick:(id)sender;
@end
