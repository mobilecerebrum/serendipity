#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SRAnnotationViewController : NSObject<MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}

@property(nonatomic,assign) CLLocationCoordinate2D coordinate;
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *subtitle;

-(id)initWithTitle:(NSString *)inTitle andCoordinate:
(CLLocationCoordinate2D)coordinate2d;

@end
