#import "SRAnnotationViewController.h"

@implementation SRAnnotationViewController

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

-(id)initWithTitle:(NSString *)title andCoordinate:
(CLLocationCoordinate2D)coordinate2d{
    self.title = title;
    self.coordinate =coordinate2d;
    return self;
}

@end
