#import "SRModalClass.h"
#import "SRLocationSetViewController.h"
#import "SRPanoramaViewController.h"
#import "MKMapView+ZoomLevel.h"

@implementation SRLocationSetViewController

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// addAnnotationPointWithDetail:

- (void)addAnnotationPointWithDetail:(CLLocation *)location {
    // Geocoder object to reverse geocode location in details
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (!(error)) {
            CLPlacemark *placemark = placemarks[0];
            //locationName = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
            if (placemark.locality != nil)
                locationName = placemark.locality;
            else
                locationName = placemark.subLocality;

            NSString *Country = @"";
            if (placemark.country != nil)
                Country = [[NSString alloc] initWithString:placemark.country];

            // Add an annotation
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            point.coordinate = center;
            point.title = locationName;
            point.subtitle = Country;
            [self.mapViewObj addAnnotation:point];

            // Stop Animation and hide view
            [self.indicatorView stopAnimating];
            self.activityView.hidden = YES;
        } else {
            // Failed to found the results alert
        }
    }];
}

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    self = [super initWithNibName:@"SRLocationSetViewController" bundle:nil];
    if (self) {
        server = inServer;
    }

    // Return
    return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

// --------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    // Call super view
    [super viewDidLoad];
    if (@available(iOS 13.0, *)) {
        _searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
    } else {
        // Fallback on earlier versions
    }
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.location.view", " ") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    // Left Bar Buttton
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    // Street view
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    self.mapViewObj.showsUserLocation = NO;
    [self.indicatorView startAnimating];

    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width / 2.0;
    _profileImageView.layer.masksToBounds = YES;



    // Lat long for address
    if (self.getLocation && self.address.length != 0 && ![self.address isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {

        NSString *address = self.address;
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:address
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         if (placemarks && placemarks.count > 0) {
                             CLPlacemark *topResult = placemarks[0];
                             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];

                             MKCoordinateSpan span = MKCoordinateSpanMake(1.0, 1.1);
                             MKCoordinateRegion region = MKCoordinateRegionMake(MKCoordinateForMapPoint(MKMapPointForCoordinate(placemark.coordinate)), span);
                             [self.mapViewObj setRegion:region animated:YES];

                             center = placemark.coordinate;

                             MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                             point.coordinate = placemark.coordinate;
                             point.title = server.loggedInUserInfo[kKeyAddress];

                             CLLocation *loc = [[CLLocation alloc] initWithLatitude:placemark.coordinate.latitude longitude:placemark.coordinate.longitude];
                             [geocoder reverseGeocodeLocation:loc
                                            completionHandler:^(NSArray *placemarks, NSError *error) {
                                                CLPlacemark *placemark = placemarks[0];
                                                //locationName = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                                                if (placemark.locality != nil)
                                                    locationName = placemark.locality;
                                                else
                                                    locationName = placemark.subLocality;
                                                point.title = locationName;
                                                point.subtitle = [NSString stringWithFormat:@"%@", placemark.country];

                                                // Show view
                                                self.activityView.hidden = YES;
                                                [self.indicatorView stopAnimating];
                                            }];


                             [self.mapViewObj setRegion:region animated:YES];
                             [self.mapViewObj addAnnotation:point];

                             // Show view
                             self.activityView.hidden = YES;
                             [self.indicatorView stopAnimating];
                         } else {
                             [self getCurrentLocation];
                         }

                     }

        ];

    } else {
        [self getCurrentLocation];
    }

    //self.searchTable.hidden = TRUE;
    // Auto complete
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = _tableDataSource;
    _resultsController.tableView.dataSource = _tableDataSource;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated]; //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        [self.mapViewObj setCenterCoordinate:panoramaLastLoc zoomLevel:5 animated:YES];
    }
}

#pragma mark
#pragma mark Action Method
#pragma mark

// --------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {

    if ([_delegate respondsToSelector:@selector(dataFromSRLocationViewController:)]) {
        [_delegate dataFromSRLocationViewController:locationName];
    }
    if ([_delegate respondsToSelector:@selector(locationAddressWithLatLong:centerCoordinate:)]) {
        [_delegate locationAddressWithLatLong:locationName centerCoordinate:center];
    }
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// getCurrentLocation:

- (void)getCurrentLocation {
    // Location Manager Initialization
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 &&
            [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse
            ) {
        // Will open an confirm dialog to get user's approval
        [self.locationManager requestWhenInUseAuthorization];
    } else {
        [self.locationManager startUpdatingLocation]; //Will update location immediately
    }

}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

//
// -----------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
}

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
    NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [self.mapViewObj convertPoint:touchPoint toCoordinateFromView:self.view];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_mapViewObj camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [_mapViewObj setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {

                    if (!(CGRectContainsPoint(streetViewBtn.frame, touchPoint))) {
                        if (!isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapViewObj addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapViewObj];
    CLLocationCoordinate2D tapPoint = [self.mapViewObj convertPoint:point toCoordinateFromView:self.view];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_mapViewObj camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [_mapViewObj setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}

#pragma mark
#pragma mark MapView Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// didUpdateUserLocation:

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self.mapViewObj setCenterCoordinate:userLocation.coordinate animated:YES];
}

// --------------------------------------------------------------------------------
// viewForAnnotation:

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKPinAnnotationView * pin = (MKPinAnnotationView * )
    [mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];

    if (!pin) {
        if ([annotation isKindOfClass:MKUserLocation.class]) {
            // return
            return nil;
        } else
            pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
    } else {
        pin.annotation = annotation;
    }

    pin.draggable = YES; // Customize the functions and looks
    pin.animatesDrop = YES;
    pin.canShowCallout = YES;
    pin.pinColor = MKPinAnnotationColorPurple;

    if (_profileImageView == nil) {
        _profileImageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    }


    _profileImageView.frame = CGRectMake(0, 0, 30, 30);
    pin.leftCalloutAccessoryView = _profileImageView;

    // Return
    return pin;
}

// --------------------------------------------------------------------------------
// annotationViewdidChangeDragState:

- (void)   mapView:(MKMapView *)mapView
    annotationView:(MKAnnotationView *)annotationView
didChangeDragState:(MKAnnotationViewDragState)newState
      fromOldState:(MKAnnotationViewDragState)oldState {
    if (newState == MKAnnotationViewDragStateEnding) {
        // drop the pin, and set state to none
        center = annotationView.annotation.coordinate;
        MKPointAnnotation *annotationPoint = (MKPointAnnotation *) annotationView.annotation;
        annotationPoint.title = @"";
        annotationPoint.subtitle = @"";

        // Show view
        self.activityView.hidden = NO;
        [self.indicatorView startAnimating];

        CLGeocoder *ceo = [[CLGeocoder alloc] init];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:center.latitude longitude:center.longitude]; //insert your coordinates

        [ceo reverseGeocodeLocation:loc
                  completionHandler:^(NSArray *placemarks, NSError *error) {
                      CLPlacemark *placemark = placemarks[0];
                      //locationName = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      if (placemark.locality != nil)
                          locationName = placemark.locality;
                      else
                          locationName = placemark.subLocality;
                      annotationPoint.title = locationName;
                      annotationPoint.subtitle = [NSString stringWithFormat:@"%@", placemark.country];

                      // Show view
                      self.activityView.hidden = YES;
                      [self.indicatorView stopAnimating];
                  }];
    }
}

// --------------------------------------------------------------------------------
// didDeselectAnnotationView:

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    [view setSelected:NO];
}

#pragma mark
#pragma mark LocationManager Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// didChangeAuthorizationStatus:

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
        }
            break;

        case kCLAuthorizationStatusDenied: {
        }
            break;

        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [self.locationManager startUpdatingLocation]; //Will update location immediately
        }
            break;

        default:
            break;
    }
}

// --------------------------------------------------------------------------------
// didUpdateLocations:

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if (!isOnce) {
        isOnce = YES;
        CLLocation *location = [locations lastObject];
        center.latitude = location.coordinate.latitude;
        center.longitude = location.coordinate.longitude;

        // Set Pin
        MKCoordinateSpan span = MKCoordinateSpanMake(1.0, 1.1);
        MKCoordinateRegion region = MKCoordinateRegionMake(MKCoordinateForMapPoint(MKMapPointForCoordinate(center)), span);
        [self.mapViewObj setRegion:region animated:YES];

        // Add an annotation
        [self addAnnotationPointWithDetail:location];

        // Stop updating location
        [self.locationManager stopUpdatingLocation];
    }
}

// --------------------------------------------------------------------------------
// didFailWithError:

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [SRModalClass showAlert:NSLocalizedString(@"alert.get_loc_failure.txt", @"")];
}

#pragma mark
#pragma mark Search Bar Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// textDidChange:

- (void)searchBar:(UISearchBar *)inSearchBar textDidChange:(NSString *)searchText {
    [_tableDataSource sourceTextHasChanged:searchText];
}

// --------------------------------------------------------------------------------
// searchBarShouldBeginEditing:

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)inSearchBar {
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidBeginEditing:

- (void)searchBarTextDidBeginEditing:(UISearchBar *)inSearchBar {
    // Add overlay view
    [self addChildViewController:_resultsController];
    _resultsController.view.frame = CGRectMake(0, 44 + [self getTopSafeArea], SCREEN_WIDTH, 150);
    _resultsController.view.alpha = 0.0f;
    [self.view addSubview:_resultsController.view];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 1.0f;
                     }];
    [_resultsController didMoveToParentViewController:self];
}

// --------------------------------------------------------------------------------
// searchBarShouldEndEditing:

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)inSearchBar {
    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidEndEditing:

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // Remove  overlay view from view
    [_resultsController willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [_resultsController.view removeFromSuperview];
                         [_resultsController removeFromParentViewController];
                     }];
}

// --------------------- -----------------------------------------------------------
// searchBarSearchButtonClicked:

- (void)searchBarSearchButtonClicked:(UISearchBar *)inSearchBar {
    // Search
    [_tableDataSource sourceTextHasChanged:inSearchBar.text];
}

#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // Return
    return YES;
}

#pragma mark
#pragma mark GMSAutocompleteTableDataSource Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// didAutocompleteWithPlace:

- (void) tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    [self.searchBar resignFirstResponder];
    self.searchBar.text = place.name;
    locationName = place.name;

    //Create your annotation
    float zoomLevel = 0.5;
    MKCoordinateRegion region = MKCoordinateRegionMake(place.coordinate, MKCoordinateSpanMake(zoomLevel, zoomLevel));
    [self.mapViewObj setRegion:[self.mapViewObj regionThatFits:region] animated:YES];
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    // Set your annotation to point at your coordinate
    point.coordinate = place.coordinate;
    point.title = place.name;
    //If you want to clear other pins/annotations this is how to do it
    for (id annotation in self.mapViewObj.annotations) {
        [self.mapViewObj removeAnnotation:annotation];
    }
    //Drop pin on map
    [self.mapViewObj addAnnotation:point];
}

// --------------------------------------------------------------------------------
// didFailAutocompleteWithError:

- (void)     tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [self.searchBar resignFirstResponder];
    self.searchBar.text = @"";
    [SRModalClass showAlert:@"Sorry, we are not able to properly map user locations at the moment. Please close and reopen the app."];
}

// --------------------------------------------------------------------------------
// didUpdateAutocompletePredictionsForTableDataSource:

- (void)didUpdateAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

// --------------------------------------------------------------------------------
// didRequestAutocompletePredictionsForTableDataSource:

- (void)didRequestAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}
@end
