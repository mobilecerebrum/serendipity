#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapKit/MapKit.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "SRProfileImageView.h"
#pragma mark
#pragma mark Protocol
#pragma mark

// Custom Delegate to pass Location Data to SREditViewController
@protocol SRLocationSetViewControllerDelegate <NSObject>
- (void)dataFromSRLocationViewController:(NSString *)data;
- (void)locationAddressWithLatLong:(NSString *)data centerCoordinate:(CLLocationCoordinate2D)inCenter;
@end

@interface SRLocationSetViewController : ParentViewController <CLLocationManagerDelegate, MKMapViewDelegate,UISearchBarDelegate, GMSAutocompleteTableDataSourceDelegate,UIAlertViewDelegate>
{
	//Instance Variables
	NSString *locationName;
    NSDictionary *infoDict;

	CLLocationCoordinate2D center;

	// Server
	SRServerConnection *server;

	BOOL isOnce,isPanoramaLoaded;
    // Controllers
    UITableViewController *_resultsController;
    GMSAutocompleteTableDataSource *_tableDataSource;
    
    IBOutlet UIButton *streetViewBtn;
    CLLocationCoordinate2D panoramaLastLoc;
}

//Properties
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (nonatomic, strong) IBOutlet MKMapView *mapViewObj;
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) IBOutlet SRProfileImageView *profileImageView;

//@property (weak, nonatomic) IBOutlet UITableView *searchTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) BOOL getLocation;
@property (strong, nonatomic) NSArray *searchResults;
@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
