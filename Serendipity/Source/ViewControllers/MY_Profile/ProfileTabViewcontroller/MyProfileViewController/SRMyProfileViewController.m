#import "SRMyProfileViewController.h"
#import "SRUserTabViewController.h"
#import "SREditProfileViewController.h"

const CGFloat kScrollObjHeight = 568.0;
const CGFloat kScrollObjWidth = 320.0;
const NSUInteger kNumImages = 5;

@implementation SRMyProfileViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRMyProfileViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;

        // Register notification
        /* NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
         [defaultCenter addObserver:self
                           selector:@selector(getProfileDetailsSucceed:)
                               name:kKeyNotificationCompleteSignUPSucceed
                             object:nil];
         [defaultCenter addObserver:self
                           selector:@selector(getProfileDetailsFailed:)
                               name:kKeyNotificationCompleteSignUPFailed
                             object:nil];
         [defaultCenter addObserver:self
                           selector:@selector(familyActionSucceed:)
                               name:kKeyNotificationFamilyActionSucceed object:nil];
         [defaultCenter addObserver:self
                           selector:@selector(familyActionFailed:)
                               name:kKeyNotificationFamilyActionFailed object:nil];*/
    }
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Tap guesture on profile image
    UITapGestureRecognizer *tapProfileImg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnAlbumClick:)];
    [self.imgViewProfile setUserInteractionEnabled:TRUE];
    [self.imgViewProfile addGestureRecognizer:tapProfileImg];

    //
    self.profileDetailsScrollview.scrollEnabled = NO;

    // Family List
    familyListArr = [[NSMutableArray alloc] init];
    self.lblFamilyCount.hidden = TRUE;

    //
    self.objInterestsView.hidden = YES;
    self.objConnectionView.hidden = YES;

    // Set image view gredient
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.imgViewBckScroll.alpha = 0.95;
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = self.imgViewBckScroll.bounds;
        gradient.colors = @[(id) [[UIColor blackColor] CGColor],
                (id) [[UIColor whiteColor] CGColor]];
        [self.imgViewBckScroll.layer insertSublayer:gradient atIndex:0];
    });

    // Hide the scroll at first
    self.objBackGroundScrollview.hidden = YES;
    self.familyPhotosScrollview.scrollEnabled = NO;
    self.connectionPhotosScrollview.scrollEnabled = NO;
    self.interestsPhotosScrollview.scrollEnabled = NO;

    //set Constant text to labels
    self.lblFamilyCount.text = NSLocalizedString(@"lbl.family.txt", @"");
    self.lblConnectionCount.text = NSLocalizedString(@"lbl.connections.txt", @"");
    self.lblInterestsCount.text = NSLocalizedString(@"lbl.interests.txt", @"");

    // TODO: Later family and connections
    /*[self addImagesInScrollView:YES];
     [self addImagesInScrollView:NO];*/

    //Add Shadow to btnAlbum
    [self.btnAlbum.layer setShadowOffset:CGSizeMake(5, 5)];
    [self.btnAlbum.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.btnAlbum.layer setShadowOpacity:8.5];

}

// ------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    // Call Super
    [super viewWillAppear:animated];
    UIImage *image3 = [UIImage imageNamed:@"menubar-man-pencil.png"];
    CGRect frameimg = CGRectMake(15, 5, 22, 22);
    UIButton *rightButton = [[UIButton alloc] initWithFrame:frameimg];
    [rightButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(editBtnAction:)
          forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *mailbutton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.tabBarController.navigationItem.rightBarButtonItem = mailbutton;

    CGRect frameimg1 = CGRectMake(5, 5, 18, 18);
    UIButton *leftButton = [[UIButton alloc] initWithFrame:frameimg1];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"menubar-back.png"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.tabBarController.navigationItem.leftBarButtonItem = leftBarBtn;


    // Display details
    [self displayDetails];
    [self setScrollContentSize];
    // Call api
    // [APP_DELEGATE showActivityIndicator];
    // [server makeAsychronousRequest:kKeyClassFamily inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}
// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)backBtnAction:(id)sender {
    //[SRModalClass hideTabBar:self];
    [APP_DELEGATE hideActivityIndicator];
    if ([self.tabBarController.selectedViewController isKindOfClass:[SRMyProfileViewController class]]) {
        if (self.objBackGroundScrollview.isHidden) {
            [self.tabBarController.navigationController popViewControllerAnimated:YES];
        } else {
            self.objBackGroundScrollview.hidden = YES;
            self.profileDetailsScrollview.hidden = NO;
            self.imgViewBckScroll.hidden = NO;
            self.btnAlbum.hidden = NO;
        }
    } else
        [self.navigationController popViewControllerAnimated:YES];
}
// ----------------------------------------------------------------------------------------------------------
// editBtnAction:

- (void)editBtnAction:(id)sender {
    SREditProfileViewController *objSREditProfileViewController = [[SREditProfileViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:objSREditProfileViewController animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

#pragma mark
#pragma mark Private Method
#pragma mark

- (void)getCurrentAddress {
    self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");;
    //Call Google API for correct distance and address from two locations
    if (server.myLocation != nil && [server.loggedInUserInfo[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[server.loggedInUserInfo[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[server.loggedInUserInfo[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];

        NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f&lon=%f", kNominatimServer, kAddressApi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude];
       // NSLog(@"Nominatim URl=%@", urlStr);

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.timeoutInterval = 60;
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if (!error) {
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                       Boolean isAddressPresent = true;
                                       for (NSString *keyStr in json) {
                                           if ([keyStr isEqualToString:@"error"]) {
                                               isAddressPresent = false;
                                           }
                                       }
                                       if (isAddressPresent) {
                                           NSString *strAddress = [json valueForKey:@"display_name"];
                                           NSString *lblStr = @"";
                                           if ([server.loggedInUserInfo[kKeyDOB] length] > 0) {
                                               NSString *birthDate = server.loggedInUserInfo[kKeyDOB];
                                               NSDate *todayDate = [NSDate date];
                                               NSDate *dobDate = [SRModalClass returnDateInYYYYMMDD:birthDate];
                                               NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                                               NSDateComponents *components = [calendar components:NSCalendarUnitYear
                                                                                          fromDate:dobDate
                                                                                            toDate:todayDate
                                                                                           options:0];
                                               lblStr = [NSString stringWithFormat:@"%ld", (long) components.year];
                                           }
                                           if ([server.loggedInUserInfo[kKeyOccupation] length] > 0) {
                                               if ([lblStr length] > 0) {
                                                   lblStr = [NSString stringWithFormat:@"%@, %@", lblStr, server.loggedInUserInfo[kKeyOccupation]];
                                               } else {
                                                   lblStr = server.loggedInUserInfo[kKeyOccupation];
                                               }
                                           }
                                           if (lblStr.length) {
                                               lblStr = [lblStr stringByAppendingString:[NSString stringWithFormat:@", in %@", strAddress]];
                                           } else {
                                               lblStr = [NSString stringWithFormat:@"in %@", strAddress];
                                           }
                                           if (lblStr.length) {
                                               lblStr = [lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                               lblStr = [lblStr stringByReplacingOccurrencesOfString:@",," withString:@","];
                                           }
                                           self.lblProfileAddress.text = lblStr;
                                           [self setScrollContentSize];
                                       } else {
                                           self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                                           [self setScrollContentSize];
                                       }
                                   } else {
                                       self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                                       [self setScrollContentSize];
                                   }
                               }];
    }
}
//---------------------------------------------------------------------------------------------------------------------------------
// setScrollContentSize

- (void)setScrollContentSize {
    // height for Profile Address Details
    int y;
    CGRect frame = self.lblProfileAddress.frame;
    CGRect textRect = [self.lblProfileAddress.text boundingRectWithSize:CGSizeMake(self.lblProfileAddress.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{NSFontAttributeName: self.lblProfileAddress.font} context:nil];

    // height for about user Details
    y = self.lblProfileAddress.frame.origin.y + self.lblProfileAddress.frame.size.height + 5;
    if ([server.loggedInUserInfo[kKeyAboutUser] length] > 0) {
        frame.size.height = textRect.size.height;
        self.lblProfileDesc.frame = frame;
        frame = self.lblProfileDesc.frame;
        frame.origin.y = y + 10;
        self.lblProfileDesc.frame = frame;

        //height for Profile Description Details

        textRect = [self.lblProfileDesc.text boundingRectWithSize:CGSizeMake(self.lblProfileDesc.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName: self.lblProfileDesc.font} context:nil];

        frame.size.height = textRect.size.height;
        self.lblProfileDesc.frame = frame;
        self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x, self.lblProfileDesc.frame.origin.y + self.lblProfileDesc.frame.size.height, self.btnReadMore.frame.size.width, self.btnReadMore.frame.size.height);

        y = y + self.lblProfileDesc.frame.size.height + self.btnReadMore.frame.size.height;
    }

    // height for dating view
    if ([server.loggedInUserInfo[kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *settingDict = server.loggedInUserInfo[kKeySetting];
        if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
            [self.objDatingView setHidden:FALSE];
            frame = self.objDatingView.frame;
            frame.origin.y = y + 8;
            self.objDatingView.frame = frame;

            y = y + self.objDatingView.frame.size.height;
        } else {
            [self.objDatingView setHidden:TRUE];
        }
    }

    // height for family scroll
    frame = self.objFamilyView.frame;
    frame.origin.y = y + 12;
    self.objFamilyView.frame = frame;


    y = y + self.objFamilyView.frame.size.height + 10;
    // height for connection scroll
    frame = self.objConnectionView.frame;
    frame.origin.y = self.objFamilyView.frame.origin.y + self.objFamilyView.frame.size.height + 8;
    self.objConnectionView.frame = frame;

    // height for interest scroll
    frame = self.objInterestsView.frame;
    frame.origin.y = self.objConnectionView.frame.origin.y + self.objConnectionView.frame.size.height + 8;
    self.objInterestsView.frame = frame;

    //Set contect size for profile scroll using family scroll as connectin and interest scroll is hidden
    self.profileDetailsScrollview.contentSize = CGSizeMake(self.profileDetailsScrollview.frame.size.width, y + 50);

    //Disable scrolling in iphone 6s plus
    if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {

        self.profileDetailsScrollview.scrollEnabled = NO;
    } else
        self.profileDetailsScrollview.scrollEnabled = YES;
}

//---------------------------------------------------------------------------------------------------------------------------------
// displayDetails

- (void)displayDetails {
    NSDictionary *dict = server.loggedInUserInfo;
    // Name
    self.lblProfileHeadName.text = [NSString stringWithFormat:@"%@ %@", [dict valueForKey:kKeyFirstName], [dict valueForKey:kKeyLastName]];

    // Display the values
    //Date of birth
    NSString *lblStr = @"";
    if ([dict[kKeyDOB] length] > 0) {
        NSString *birthDate = dict[kKeyDOB];
        NSDate *todayDate = [NSDate date];
        NSDate *dobDate = [SRModalClass returnDateInYYYYMMDD:birthDate];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear
                                                   fromDate:dobDate
                                                     toDate:todayDate
                                                    options:0];

        lblStr = [NSString stringWithFormat:@"%ld", (long) components.year];
    }

    // Occupation
    if ([dict[kKeyOccupation] length] > 0) {
        if ([lblStr length] > 0) {
            lblStr = [NSString stringWithFormat:@"%@, %@", lblStr, dict[kKeyOccupation]];
        } else {
            lblStr = dict[kKeyOccupation];
        }
    }
    // Address
    BOOL isMutatedStrSet = NO;
    NSString *address = dict[kKeyAddress];
    if (address.length > 0) {
        if ([lblStr length] > 0) {
            NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:lblStr];
            NSString *str = [NSString stringWithFormat:@" in %@", address];
            NSAttributedString *atrStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaItalicMedium size:12]}];
            [muAtrStr appendAttributedString:atrStr];
            [self.lblProfileAddress setAttributedText:muAtrStr];
            isMutatedStrSet = YES;
        } else {
            lblStr = address;
            lblStr = [lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }

    } else {
        if ([dict[kKeyLocation] objectForKey:kKeyLiveAddress] != nil) {
            if ([lblStr length] > 0 && [[dict[kKeyLocation] valueForKey:kKeyLiveAddress] length] > 0) {
                NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:lblStr];
                NSString *str = [NSString stringWithFormat:@" in %@", [dict[kKeyLocation] valueForKey:kKeyLiveAddress]];
                NSAttributedString *atrStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaItalicMedium size:12]}];
                [muAtrStr appendAttributedString:atrStr];
                [self.lblProfileAddress setAttributedText:muAtrStr];
                isMutatedStrSet = YES;
            } else if ([[dict[kKeyLocation] valueForKey:kKeyLiveAddress] length] > 0) {
                lblStr = [dict[kKeyLocation] valueForKey:kKeyLiveAddress];
                lblStr = [lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            }
        }
        if ([[dict[kKeyLocation] valueForKey:kKeyLattitude] length] > 0 && [[dict[kKeyLocation] valueForKey:kKeyLattitude] length] > 0) {
            //Get Address from GoogleAPI
            [self getCurrentAddress];
        }
    }

    // Check whether lbl string has some thing to display
    if ([lblStr length] > 0 && !isMutatedStrSet) {
        self.lblProfileAddress.text = lblStr;
    } else if ([lblStr length] == 0) {
        self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
    }
    
    
    NSString *str = dict[kKeyAboutUser];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

    if (goodValue == nil){
        goodValue = str;
    }
    
    [dict setValue:goodValue forKey:kKeyAboutUser];
    [server.loggedInUserInfo setValue:goodValue forKey:kKeyAboutUser];

    // Display Description
    if ([dict[kKeyAboutUser] length] > 0) {
        NSString *str = dict[kKeyAboutUser];
      //  NSLog(@"%@", dict[kKeyAboutUser]);
        if ([dict[kKeyAboutUser] length] > 135) {
            NSString *str = [NSString stringWithFormat:@"%@\n%@",dict[kKeyAboutUser],dict[kKeyUserWebUrl]];
            NSString *subStr = [str substringWithRange:NSMakeRange(0, 135)];
            subStr = [NSString stringWithFormat:@"%@...", subStr];
            self.lblProfileDesc.text = subStr;
            self.btnReadMore.hidden = NO;
        } else {
            NSString *str = [NSString stringWithFormat:@"%@\n%@",dict[kKeyAboutUser],dict[kKeyUserWebUrl]];
            self.lblProfileDesc.text = str;
            self.btnReadMore.hidden = YES;
        }
    } else {
        self.lblProfileDesc.text = @" ";
        self.btnReadMore.hidden = YES;
    }

    // Open dating set image accrodingly
    if ([APP_DELEGATE isUserBelow18]) {
        [self.imgDating setHidden:YES];
        [self.lblDatingTitle setHidden:YES];
        [showAgeContainer setHidden:YES];
    } else {
        [showAgeContainer setHidden:NO];
        if ([dict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *settingDict = dict[kKeySetting];
            if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
                self.imgDating.hidden = NO;
                self.lblDatingTitle.hidden = NO;
                //self.lblDatingTitle.textColor = [UIColor orangeColor];
                self.imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
            } else {
                self.imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
                self.imgDating.hidden = YES;
                self.lblDatingTitle.hidden = YES;
            }
            
            [showAgeSlider setOn:[settingDict[@"show_age"] isEqualToString:@"1"]];
            
        } else {
            self.imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
            self.imgDating.hidden = YES;
            self.lblDatingTitle.hidden = YES;
        }
    }



    // Display images in scroll view
    NSMutableArray *mediaArr = dict[kKeyMedia];
    // Insert profile image at 0th index
    if ([[server.loggedInUserInfo valueForKey:kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        if (![mediaArr containsObject:[server.loggedInUserInfo valueForKey:kKeyProfileImage]]) {
            NSDictionary *imgDict = [server.loggedInUserInfo valueForKey:kKeyProfileImage];
            [mediaArr insertObject:imgDict atIndex:0];
        }
    }
    for (int i = 0; i < mediaArr.count; i++) {
        NSDictionary *mediaDict = mediaArr[i];

        CGRect frame;
        frame.origin.x = self.objBackGroundScrollview.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.objBackGroundScrollview.frame.size;

        UIView *subview = [[UIView alloc] initWithFrame:frame];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor blackColor];

        imageView.userInteractionEnabled = YES;
        //Add tapGesture On ImageView to set as profile pic
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTappedAction:)];
        [imageView addGestureRecognizer:tapGesture];

        [subview addSubview:imageView];
        // User image
        if (mediaDict[kKeyImageName] != nil) {
            NSString *imageName = mediaDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];

                [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                imageView.image = [UIImage imageNamed:@"profile_menu.png"];
        } else
            imageView.image = [UIImage imageNamed:@"profile_menu.png"];

        UILabel *countLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
        countLbl.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
        NSString *str1 = [NSString stringWithFormat:@"%d of %lu", i + 1, (unsigned long) mediaArr.count];
        countLbl.textColor = [UIColor whiteColor];
        countLbl.text = str1;
        [subview addSubview:countLbl];

        UIButton *ContactBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 30, 5, 25, 25)];
        [ContactBtn setBackgroundImage:[UIImage imageNamed:@"back-to-profile.png"] forState:UIControlStateNormal];
        [ContactBtn addTarget:self action:@selector(contactBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [subview addSubview:ContactBtn];

        if (i == 0) {
            lblCountOfImages = countLbl;
            lblCountOfImages.hidden = YES;
            btnProfileDetail = ContactBtn;
            btnProfileDetail.hidden = YES;
        }
        [self.objBackGroundScrollview addSubview:subview];
    }
    self.objBackGroundScrollview.contentSize = CGSizeMake(self.objBackGroundScrollview.frame.size.width * mediaArr.count, 0);
    self.objBackGroundScrollview.scrollEnabled = NO;

    [self.familyPhotosScrollview setHidden:NO];
    [self.connectionPhotosScrollview setHidden:YES];
    [self.interestsPhotosScrollview setHidden:YES];

}


#pragma mark
#pragma mark Action Methods
#pragma mark

// ------------------------------------------------------------------------------------------------------
// btnReadMoreClick:

- (IBAction)btnReadMoreClick:(id)sender {
    if ([self.btnReadMore.titleLabel.text isEqualToString:@" << "]) {
        NSString *str = [NSString stringWithFormat:@"%@\n%@",server.loggedInUserInfo[kKeyAboutUser],server.loggedInUserInfo[kKeyUserWebUrl]];
        NSString *subStr = [str substringWithRange:NSMakeRange(0, 135)];
        subStr = [NSString stringWithFormat:@"%@...", subStr];
        self.lblProfileDesc.text = subStr;

        [self.btnReadMore setTitle:NSLocalizedString(@"btn.read_more.txt", @"") forState:UIControlStateNormal];
    } else {
        NSString *str = [NSString stringWithFormat:@"%@\n%@",server.loggedInUserInfo[kKeyAboutUser],server.loggedInUserInfo[kKeyUserWebUrl]];
        self.lblProfileDesc.text = str;
        [self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
    }

    // Set Scroll Size
    [self setScrollContentSize];
}

// ------------------------------------------------------------------------------------------------------
// btnAlbumClick:

- (IBAction)btnAlbumClick:(id)sender {
    NSArray *mediaArr = server.loggedInUserInfo[kKeyMedia];
    if ([mediaArr count] > 0) {
        lblCountOfImages.hidden = NO;
        btnProfileDetail.hidden = NO;

        self.btnAlbum.hidden = YES;

        self.imgViewBckScroll.hidden = YES;
        self.profileDetailsScrollview.hidden = YES;

        self.objBackGroundScrollview.hidden = NO;
        self.objBackGroundScrollview.scrollEnabled = YES;
    } else {
        [SRModalClass showAlert:NSLocalizedString(@"alert.no.albums.text", @"")];
    }
}

// ------------------------------------------------------------------------------------------------------
// contactBtnClicked:

- (void)contactBtnClicked:(UIButton *)sender {
    UIView *view = [sender superview];
    for (UIView *subView in[view subviews]) {
        if ([subView isKindOfClass:[UILabel class]] || [subView isKindOfClass:[UIButton class]]) {
            subView.hidden = YES;
            if ([subView isKindOfClass:[UILabel class]]) {
                lblCountOfImages = (UILabel *) subView;
            } else {
                btnProfileDetail = (UIButton *) subView;
            }
        }
    }
    self.btnAlbum.hidden = NO;
    self.imgViewBckScroll.hidden = NO;
    self.profileDetailsScrollview.hidden = NO;
    self.profileDetailsScrollview.scrollEnabled = YES;

    self.objBackGroundScrollview.hidden = YES;
    self.objBackGroundScrollview.scrollEnabled = NO;
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)addImagesInScrollView:(BOOL)isFamily {
    // Frane at starting point
    CGRect latestFrame = CGRectZero;


    // Adding images to scroll view
    for (int i = 0; i < [familyListArr count]; i++) {
        self.lblFamilyCount.hidden = true;
        NSDictionary *relativeDict = [familyListArr[i] objectForKey:kKeyRelative];

        NSDictionary *imgDict = [relativeDict valueForKey:kKeyProfileImage];


        UIImageView *objImageView = [[UIImageView alloc] init];
        if (i == 0) {
            [objImageView setFrame:CGRectMake(5, 0, 40, 40)];
        } else
            objImageView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 25, 0, 40, 40);

        latestFrame = objImageView.frame;
        objImageView.image = [UIImage imageNamed:@"profile_menu.png"];

        if ([imgDict isKindOfClass:[NSDictionary class]] && imgDict[kKeyImageName] != nil) {

            NSString *imagename = [imgDict valueForKey:kKeyImageName];
            if ([imagename length] > 1) {

                NSString *stringUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imagename];

                [objImageView sd_setImageWithURL:[NSURL URLWithString:stringUrl]];
            }
        }

        objImageView.contentMode = UIViewContentModeScaleToFill;
        objImageView.layer.cornerRadius = objImageView.frame.size.width / 2;
        objImageView.clipsToBounds = YES;
        objImageView.tag = i;
        objImageView.userInteractionEnabled = TRUE;
        UITapGestureRecognizer *familyImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(familyImageTapDetected:)];
        [objImageView addGestureRecognizer:familyImageTap];


        UILabel *lblImageName = [[UILabel alloc] init];
        [lblImageName setFrame:CGRectMake(latestFrame.origin.x, latestFrame.origin.y + 40, 65, 18)];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaMedium size:10];

        if ([relativeDict isKindOfClass:[NSDictionary class]]) {

            if (![[relativeDict valueForKey:kKeyFirstName] isKindOfClass:[NSNull class]] && [relativeDict valueForKey:kKeyFirstName] != nil && ![[relativeDict valueForKey:kKeyLastName] isKindOfClass:[NSNull class]] && [relativeDict valueForKey:kKeyLastName] != nil) {


                NSString *lnameStr = [[relativeDict valueForKey:kKeyLastName] substringToIndex:1];

                lblImageName.text = [NSString stringWithFormat:@"%@ %@.", [relativeDict valueForKey:kKeyFirstName], lnameStr];
            } else
                lblImageName.text = @"";
        }

        lblImageName.textColor = [UIColor whiteColor];

        //lblImageName.text = str1;

        if (isFamily) {

            [self.familyPhotosScrollview addSubview:objImageView];
            [self.familyPhotosScrollview addSubview:lblImageName];

            if ((latestFrame.origin.x + 55) > SCREEN_WIDTH) {
                self.familyPhotosScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.familyPhotosScrollview.scrollEnabled = YES;

            }
        } else {
            [self.connectionPhotosScrollview addSubview:lblImageName];
            [self.connectionPhotosScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.connectionPhotosScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.connectionPhotosScrollview.scrollEnabled = YES;
            }

            // Add images in Interests Scrollview
            [self.interestsPhotosScrollview addSubview:lblImageName];
            [self.interestsPhotosScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.interestsPhotosScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.interestsPhotosScrollview.scrollEnabled = YES;
            }
        }

    }

}

// ------------------------------------------------------------------------------------------------------
// familyImageTapDetected:
- (void)familyImageTapDetected:(UIGestureRecognizer *)tapRecognizer {
    UIView *viewTiedWithRecognizer = tapRecognizer.view;

    // Get profile
    NSDictionary *userDict = familyListArr[viewTiedWithRecognizer.tag];
    NSMutableDictionary *mutatedDict = [[NSMutableDictionary alloc] init];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyId] forKey:kKeyId];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyFirstName] forKey:kKeyFirstName];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyLastName] forKey:kKeyLastName];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyAboutUser] forKey:kKeyAboutUser];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyAddress] forKey:kKeyAddress];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyMobileNumber] forKey:kKeyMobileNumber];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyDOB] forKey:kKeyDOB];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyOccupation] forKey:kKeyOccupation];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyProfileImage] forKey:kKeyProfileImage];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeySetting] forKey:kKeySetting];
    mutatedDict[kKeyIsFromConnection] = @1;
    // For showind direct connection in degree of seperation view
    [mutatedDict setValue:@1 forKey:kKeyDegree];

    //Connection Dictionary
    NSMutableDictionary *connDict = [[NSMutableDictionary alloc] init];
    [connDict setValue:@"2" forKey:kKeyConnectionStatus];
    [connDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyId] forKey:kKeyConnectionID];
    [mutatedDict setValue:connDict forKey:kKeyConnection];

    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:mutatedDict];
    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:tabCon animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)imageTappedAction:(UITapGestureRecognizer *)tapGesture {
    /*
       // Image picker action will happen here

         // Present action sheet
         actionSheet = [[UIActionSheet alloc]initWithTitle:nil
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "")destructiveButtonTitle:nil                                       otherButtonTitles:NSLocalizedString(@"setasprofilepic.text", ""), nil];


       // Show sheet
       [actionSheet showFromRect:[self.view frame] inView:self.view animated:YES];*/
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        // NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, [server.loggedInUserInfo objectForKey:kKeyId]];
        //[server asychronousRequestWithData:nil imageData:imageData forKey:kKeyProfileImage toClassType:urlStr inMethodType:kPOST];

        UIButton *btn;
        [self contactBtnClicked:btn];
    } else if (buttonIndex == 1) {
    }
}

- (IBAction)showAgeChanged:(id)sender {
    
    BOOL showAge = showAgeSlider.isOn;
    
    [[SRAPIManager sharedInstance] updateSettings:@"show_age" withValue:showAge ? @"1" : @"0"];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:showAge ? @"1" : @"0" forKey:@"show_age"];
    
}


#pragma mark
#pragma mark Notification Method
#pragma mark


// --------------------------------------------------------------------------------
// familyActionSucceed:

- (void)familyActionSucceed:(NSNotification *)inNotify {

    id object = [inNotify object];
    if ([object isKindOfClass:[NSArray class]]) {

        // Remove all objects add new
        [familyListArr removeAllObjects];
        [familyListArr addObjectsFromArray:object];
    } else if ([object isKindOfClass:[NSDictionary class]]) {

        // Search for dict if present then update or add
        NSDictionary *postInfo = [inNotify userInfo];
        NSDictionary *requestedParam = postInfo[kKeyRequestedParams];
        if (requestedParam[@"_method"] != nil) {


        }
    }
    self.lblFamilyCount.text = [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"lbl.family.txt", @""), (unsigned long) [familyListArr count]];

    NSLocalizedString(@"lbl.family.txt", @"");
    //[self addImagesInScrollView:YES];
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// familyActionFailed:

- (void)familyActionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:

- (void)getProfileDetailsSucceed:(NSNotification *)inNotify {
    [self displayDetails];
}

// --------------------------------------------------------------------------------
// getProfileDetailsFailed:

- (void)getProfileDetailsFailed:(NSNotification *)inNotify {
}

@end
