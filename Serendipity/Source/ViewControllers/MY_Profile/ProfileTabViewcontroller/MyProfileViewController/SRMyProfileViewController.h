#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRProfileImageView.h"

@interface SRMyProfileViewController : ParentViewController<UIScrollViewDelegate,UIActionSheetDelegate>
{
    // Instance variable
    UILabel *lblCountOfImages;
    UIButton *btnProfileDetail;
    
    // Server
    SRServerConnection *server;
    NSArray *countryListArr;
    UIActionSheet *actionSheet;
    NSMutableArray *familyListArr;
    __weak IBOutlet UIView *showAgeContainer;
    __weak IBOutlet UISwitch *showAgeSlider;
    
}

// Properties
@property (nonatomic, strong) IBOutlet SRProfileImageView *imgViewProfile;

@property (nonatomic, strong) IBOutlet UIScrollView *objBackGroundScrollview;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileHeadName;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileAddress;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileDesc;
@property (nonatomic, strong) IBOutlet UIImageView *imgDating;
@property (nonatomic, strong) IBOutlet UILabel *lblDatingTitle;
@property (nonatomic, strong) IBOutlet UIButton *btnAlbum;
@property (nonatomic, strong) IBOutlet UIButton *btnReadMore;
@property (nonatomic, strong) IBOutlet UIScrollView *profileDetailsScrollview;
@property (nonatomic, strong) IBOutlet UIView *objDatingView;
@property (nonatomic, strong) IBOutlet UIView *objFamilyView;
@property (nonatomic, strong) IBOutlet UIView *objConnectionView;
@property (nonatomic, strong) IBOutlet UIScrollView *familyPhotosScrollview;
@property (nonatomic, strong) IBOutlet UIScrollView *connectionPhotosScrollview;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewBckScroll;
@property (nonatomic, strong) IBOutlet UILabel *lblFamilyCount;
@property (nonatomic, strong) IBOutlet UILabel *lblConnectionCount;
@property (nonatomic, strong) IBOutlet UIView *objInterestsView;
@property (nonatomic, strong) IBOutlet UIScrollView *interestsPhotosScrollview;
@property (nonatomic, strong) IBOutlet UILabel *lblInterestsCount;

//Added new Button for Geo Track
@property (nonatomic, strong) IBOutlet UIButton *btnTrack;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)btnAlbumClick:(id)sender;
- (IBAction)btnReadMoreClick:(id)sender;

@end
