//
//  SRChatMessage.m
//  Serendipity
//
//  Created by Mcuser on 4/7/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRChatMessage.h"
#import "NSDate+SRCustomDate.h"

@implementation SRChatMessage

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.msgId = [NSNumber numberWithInt:[dictionary[@"id"] intValue]];
        self.type = dictionary[@"type"];
        self.sender_id = dictionary[@"sender_id"];
        self.receiver_id = dictionary[@"receiver_id"];
        self.is_seen = [dictionary[@"is_seen"] boolValue];
        self.created_at = [dictionary[@"created_at"] longLongValue];
        self.data = dictionary[@"data"];
        self.strSenderImage = dictionary[@"sender"][@"avatar"];
    }
    return self;
}

-(BOOL)isText {
    if ([@"text" isEqualToString:self.type] || [@"sendingmessage" isEqualToString:self.type] || [@"sending_message" isEqualToString:self.type]) {
        return YES;
    }
    return NO;
}

-(BOOL)isImage {
    if ([@"image" isEqualToString:self.type] || [@"sending_image" isEqualToString:self.type] || [@"sendingimage" isEqualToString:self.type]) {
        return YES;
    }
    return NO;
}

-(BOOL)isUploadingImage {
    return [@"sending_image" isEqualToString:self.type];
}

-(BOOL)isVideo {
    if ([@"video" isEqualToString:self.type] || [@"sending_video" isEqualToString:self.type] || [@"sendingvideo" isEqualToString:self.type]) {
        return YES;
    }
    return NO;
}

-(BOOL)isUploadingVideo {
    return [@"sending_video" isEqualToString:self.type];
}

-(NSString *)messageTime {
    NSDate *messageDate = [NSDate dateWithTimeIntervalSince1970:self.created_at];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSString *messageDateStr = [dateFormatter stringFromDate:messageDate];
    return [NSDate chatTime:messageDateStr];
   
}

-(NSString *)messageData {
    if ([self isText]) {
        return [self.data valueForKey:@"text"];
    } else if ([self isImage]) {
        return [self.data valueForKey:@"image"];
    } else if ([self isVideo]) {
        return [self.data valueForKey:@"video"];
    }
    return @"";
}

- (NSString*)messageText {
    if ([self isText]) {
        return [self.data valueForKey:@"text"];
    } else if ([self isImage]) {
        return [self.data valueForKey:@"text"];
    } else if ([self isVideo]) {
        return [self.data valueForKey:@"text"];
    }
    return @"";
}

-(NSString *)videoPreview {
    if ([self isVideo]) {
        return [self.data valueForKey:@"preview"];
    }
    return @"";
}

-(UIImage*)imageToUpload {
    if ([self isUploadingImage]) {
        return [self.data objectForKey:@"image"];
    }
    return nil;
}

@end
