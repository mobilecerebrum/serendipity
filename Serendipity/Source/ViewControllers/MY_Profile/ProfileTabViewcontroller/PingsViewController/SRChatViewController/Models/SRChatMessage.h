//
//  SRChatMessage.h
//  Serendipity
//
//  Created by Mcuser on 4/7/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRChatMessage : NSObject

@property (nonatomic, strong) NSNumber* msgId;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* sender_id;
@property (nonatomic, strong) NSString* receiver_id;
@property (nonatomic) BOOL is_seen;
@property (nonatomic) long long created_at;
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) UIImage *imageToSend;
@property (nonatomic, strong) NSString *strSenderImage;

@property (nonatomic) BOOL whileUploadVideo;
- (BOOL)isImage;
- (BOOL)isText;
- (BOOL)isVideo;
- (BOOL)isUploadingImage;
- (BOOL)isUploadingVideo;
- (NSString*)videoPreview;
- (NSString*)messageTime;
- (NSString*)messageData;
- (NSString*)messageText;
- (UIImage*)imageToUpload;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end

NS_ASSUME_NONNULL_END
