//
//  ChatMessageBox.m
//  Serendipity
//
//  Created by Mcuser on 1/29/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "ChatMessageBox.h"
#import "SRLocationPreviewViewController.h"
#import <Serendipity-Swift.h>
#import "SRAttachmentViewCell.h"
#import "SRModalClass.h"

#define MESSAGE_BOX_HEIGHT 63
#define MAX_NUMBER_OF_ATTACHMENTS 8
#define ATTACHMENTS_PADDING 8
#define ATTACHMENTS_COLUMNS 3
#define KEEP_ATTACHMENTS YES

@interface ChatMessageBox() <ImagePickerDelegate, GMImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate,UIVideoEditorControllerDelegate>
@property (nonatomic, strong) ImagePickerController *imgPicker;
@property (nonatomic, strong) UINavigationController *navigation;


@property NSArray<UIViewController*> *controllers;
@end

@implementation ChatMessageBox

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder]){
        [self initializePicker];
    }
    return self;
}

- (id)initWithFrame:(CGRect)rect{
    if(self = [super initWithFrame:rect]){
        [self initializePicker];
    }
    return self;
}

-(void)initialize {
    if (initialized) return;
    [self enableDisableSendButton];
    initialized = YES;
    initialY = (float) self.frame.origin.y;
    self.textBox.delegate = self;
    attachments = [NSMutableArray new];
    self.attachmentsCollection.delegate = self;
    self.attachmentsCollection.dataSource = self;
    [self initializeKeyboardListener];
    [self registerTableCells];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.itemSize = CGSizeMake(self.attachmentsCollection.frame.size.width / ATTACHMENTS_COLUMNS - (ATTACHMENTS_PADDING * 2), self.attachmentsCollection.frame.size.width / ATTACHMENTS_COLUMNS - (ATTACHMENTS_PADDING * 2));
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.minimumInteritemSpacing = 0;
    flow.minimumLineSpacing = 0;
    
    self.attachmentsCollection.collectionViewLayout = flow;
    self.controllers = ((UIViewController*)self.delegate).tabBarController.viewControllers;
    
}

-(void)registerTableCells {
    UINib *myMessageNib = [UINib nibWithNibName:@"SRAttachmentViewCell" bundle:nil];
    [self.attachmentsCollection registerNib:myMessageNib forCellWithReuseIdentifier:@"SRAttachmentViewCell"];
}

-(void)initializePicker {
    galleryPicker = [[UIImagePickerController alloc] init];
    galleryPicker.delegate = self;
    galleryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    galleryPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:galleryPicker.sourceType];
    galleryPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    galleryPicker.videoMaximumDuration = 120 ;
}

-(void)initializeKeyboardListener {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(float)attachmentsTableHeight {
    return (float) (((attachments.count + ATTACHMENTS_COLUMNS - 1) / ATTACHMENTS_COLUMNS) * (self.attachmentsCollection.frame.size.width / ATTACHMENTS_COLUMNS));
}

-(IBAction)onSendLocation:(id)sender {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        SRLocationPreviewViewController *locationPreview = [[SRLocationPreviewViewController alloc] initWithNibName:@"SRLocationPreviewViewController" bundle:nil];
//        locationPreview.delegate = self.delegate;
//        [(UIViewController*)self.delegate presentViewController:locationPreview animated:YES completion:nil];
//        locationPreview.usersList = self.usersList;
//    });
    
    if (self.delegate) {
        [self.delegate onShowLocations];
    }
}

//-(IBAction)sendMessage:(id)sender {
//    if (self.delegate) {
//        if (self.textBox.text.length != 0) {
//            [self.delegate onSendMessage:self.textBox.text];
//            [self.textBox setText:@""];
//            self.sendButton.enabled = YES;
//            self.sendButton.alpha = 1.0;
//        }
//
//        if (attachments.count > 0) {
//            for (NSDictionary *item in attachments) {
//                if ([[item valueForKey:@"type"] isEqualToString:@"image"]) {
//                    [self.delegate onSendAttachment:item[@"image"]];
//                } else if ([[item valueForKey:@"type"] isEqualToString:@"video"]) {
//                    [self.delegate onSendVideo:item[@"path"]];
//                }
//            }
//
//            [attachments removeAllObjects];
//            [self.attachmentsCollection reloadData];
//            [self updateOnScreenPosition];
//        }
//    }
//}

-(IBAction)sendMessage:(id)sender {
    if (self.delegate) {
        
        NSString *trimmed = [self.textBox.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        self.textBox.text = trimmed;
        
        if (self.textBox.text.length != 0) {
            if (attachments.count == 0) {
                [self.delegate onSendMessage:self.textBox.text];
                [self.textBox setText:@""];
                self.constrainTextBox.constant = 35;
                self.hintLabel.hidden = NO;
            }
            self.sendButton.enabled = YES;
            self.sendButton.alpha = 1.0;
        }
        if (attachments.count > 0) {
            for (NSDictionary *item in attachments) {
                if ([[item valueForKey:@"type"] isEqualToString:@"image"]) {
                    [self.delegate onSendAttachment:item[@"image"]];
                } else if ([[item valueForKey:@"type"] isEqualToString:@"video"]) {
                    [self.delegate onSendVideo:item[@"path"]];
                }
            }
            [self.textBox setText:@""];
            self.hintLabel.hidden = NO;
            self.constrainTextBox.constant = 35;
            [attachments removeAllObjects];
            [self.attachmentsCollection reloadData];
            [self updateOnScreenPosition];
        }
        
    }
}

-(IBAction)sendAttachment:(id)sender {

    self.imgPicker = [ImagePickerController new];
    self.imgPicker.delegate = self;
    [self.navigation setModalPresentationStyle:UIModalPresentationFullScreen];
    self.navigation = [[UINavigationController alloc] initWithRootViewController:self.imgPicker];
    self.navigation.navigationBar.hidden = YES;
    [self.navigation setModalPresentationStyle:UIModalPresentationFullScreen];
    [(UIViewController*)self.delegate presentViewController:self.navigation animated:YES completion:nil];
   // [self.delegate chatMessageDidSelectCamera];
}

-(void)updateOnScreenPosition {
    [self.attachmentsCollection setHidden:!(attachments.count > 0)];
    self.attachmentsHeight.constant = [self attachmentsTableHeight];
    if (self.delegate) {
        [self.delegate updateMessageBoxPosition:keyBoardHeight];
    }
    self.selfHeightConstraint.constant = MESSAGE_BOX_HEIGHT + self.attachmentsHeight.constant;
    self.attachmentsHeight.constant = [self attachmentsTableHeight];
    [self layoutIfNeeded];
}

-(void)storeImageAttachment:(UIImage*)image {
    NSDictionary * dict = @{@"type" : @"image",
                            @"image" : image};
    [attachments addObject:dict];
    [self updateOnScreenPosition];
    [self enableDisableSendButton];
}

-(void)storeVideoAttachment:(NSURL*)videoPath withPreview:(UIImage*)image {
    NSDictionary * dict = @{@"type" : @"video",
                            @"path" : videoPath,
                            @"image" : image};
    [attachments addObject:dict];
    [self updateOnScreenPosition];
    [self enableDisableSendButton];
}

#pragma mark - Attachment Delete Button Action

-(void)onDeleteAttachment:(UIButton*)button {
    [attachments removeObjectAtIndex:(NSUInteger) button.tag];
    [self.attachmentsCollection reloadData];
    [self updateOnScreenPosition];
}

#pragma mark - UICollection View Delegates

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SRAttachmentViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SRAttachmentViewCell" forIndexPath:indexPath];
    cell.attachmentPreview.image = attachments[(NSUInteger) indexPath.row][@"image"];
    cell.deleteButton.tag = indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(onDeleteAttachment:) forControlEvents:(UIControlEventTouchUpInside)];
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return attachments ? attachments.count : 0;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return CGSizeMake((self.attachmentsCollection.bounds.size.width - 5) / ATTACHMENTS_COLUMNS,
//            (self.attachmentsCollection.bounds.size.width - 5) / ATTACHMENTS_COLUMNS);
    return CGSizeMake(self.attachmentsCollection.frame.size.width / ATTACHMENTS_COLUMNS - (ATTACHMENTS_PADDING * 2), self.attachmentsCollection.frame.size.width / ATTACHMENTS_COLUMNS - (ATTACHMENTS_PADDING * 2));
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(ATTACHMENTS_PADDING, ATTACHMENTS_PADDING, ATTACHMENTS_PADDING, ATTACHMENTS_PADDING);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}

#pragma mark - ImagePicker Delegate

-(void)wrapperDidPress:(ImagePickerController *)imagePicker images:(NSArray<UIImage *> *)images {
    [galleryPicker setModalPresentationStyle:UIModalPresentationFullScreen];
    [self.imgPicker presentViewController:galleryPicker animated:YES completion:nil];

}

-(void) videoReacordingDone:(ImagePickerController *)imagePicker videos:(NSArray<NSString *> *)videos{
        NSString* path = videos.firstObject;
        [self processVideo:path];
    
//        UIVideoEditorController *editorController = [[UIVideoEditorController alloc] init];
//
//        if(path.length > 0 && [UIVideoEditorController canEditVideoAtPath:path]){
//
//            editorController.videoPath = path;
//            editorController.delegate = self;
//            [editorController setModalPresentationStyle:UIModalPresentationFullScreen];
    //        editorController.videoQuality = UIImagePickerControllerQualityTypeHigh;
//            [self.navigation presentViewController:editorController animated:NO completion:nil];
             [self.navigation dismissViewControllerAnimated:YES completion:nil];
//        }
}

- (void)videoEditorControllerDidCancel:(UIVideoEditorController *)editor{
    [editor dismissViewControllerAnimated:NO completion:nil];
}

- (void)videoEditorController:(UIVideoEditorController *)editor didSaveEditedVideoToPath:(NSString *)editedVideoPath{
    [self.navigation dismissViewControllerAnimated:YES completion:nil];
    editor.delegate = nil;
    //NSURL *urlvideo = [NSURL URLWithString:editedVideoPath];
    [self processVideo:editedVideoPath];
    
}

-(void) processVideo:(NSString*) editedVideoPath{
    NSURL *urlvideo = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@",editedVideoPath]];
    
    //Error Container
    NSError *attributesError;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:editedVideoPath error:&attributesError];
    NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
    long long fileSize = [fileSizeNumber longLongValue];
    if (fileSize / (1024 * 1024) > 50) {
        [SRModalClass showAlert:@"Sorry, we only accept media attachments of 50mb or less.  Please reduce the file size and try again."];
        return;
    }

    if (!KEEP_ATTACHMENTS) {
        if (self.delegate) {
            [self.delegate onSendVideo:urlvideo];
        }
    } else {
        [self storeVideoAttachment:urlvideo withPreview:[self getVideoFrameOfVideoAtPath:urlvideo]];
        [self.attachmentsCollection reloadData];
        [self updateOnScreenPosition];
    }
}

-(void)doneButtonDidPress:(ImagePickerController *)imagePicker images:(NSArray<UIImage *> *)images {
    if (self.imgPicker) {
        [self.imgPicker dismissViewControllerAnimated:YES completion:nil];
    }
    if (self.delegate) {
        if (!KEEP_ATTACHMENTS) {
            for (UIImage *image in images) {
                [self.delegate onSendAttachment:image];
            }
        } else {
            for (UIImage *image in images) {
                [self storeImageAttachment:image];
            }
            [self.attachmentsCollection reloadData];
        }
    }
}

-(void)cancelButtonDidPress:(ImagePickerController *)imagePicker {
    if (self.imgPicker) {
        [self.imgPicker dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Keyboard data

- (void)keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    NSDictionary* info = [note userInfo];
    CGFloat kbHeight = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    CGFloat safeAreaBottomInset = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        safeAreaBottomInset = bottomPadding;
    }
    float margin = 0;
    
    float tabHeight = (float) ((UIViewController*)self.delegate).tabBarController.tabBar.frame.size.height;
    if (self.controllers.count != 0) {
        if (safeAreaBottomInset == 0) {
            margin = tabHeight - tabHeight > 0 ? ((25)) : 0;
        } else {
            margin = 0;//(tabHeight > 0 ?  -((safeAreaBottomInset + 8)) : 0);
        }
    }
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        keyBoardHeight = kbHeight - safeAreaBottomInset - margin;
        [self updateOnScreenPosition];
    } completion:nil];
    
}

- (void)keyboardWillHide:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        //  [self setHeightOfComposeViewForText:self.textView.text andAttachment:self.filesArray.count forKeyBoardHeight:0];
        keyBoardHeight = 0;
        [self updateOnScreenPosition];
    } completion:nil];
    
    
}

#pragma mark - UITextView Delegates

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL flag;
    
    // If return key pressed
    if ([text isEqualToString:@"\n"]) {
         if (textView == self.textBox) {
//             [self.textBox resignFirstResponder];
//             [self.scrollView setContentOffset:CGPointMake(0, 0)];
//             self.sendButton.enabled  = NO;
//             self.sendButton.alpha = 0.7;
//             [self.sendButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//             [self sendMessage:nil];
         }
        flag = YES;
    } else {
        
        flag = YES;
    }
    return flag;
}

//func textViewDidChange(textView: UITextView)
//{
//    if textView.contentSize.height >= self.textViewMaxHeight
//    {
//        textView.scrollEnabled = true
//    }
//    else
//        {
//        textView.frame.size.height = textView.contentSize.height
//        textView.scrollEnabled = false
//    }
//}

-(void)textViewDidChange:(UITextView *)textView {
    [self.attachmentsCollection setHidden:(attachments.count == 0)];
//    [textView setScrollEnabled:(textView.contentSize.height >= 150)];
//    CGRect frame = textView.frame;
//    if ((textView.contentSize.height >= 150)) {
//        frame.size.height = 150;
//    } else {
//        frame.size.height = textView.contentSize.height;
//    }
//    textView.frame = frame;
    self.hintLabel.hidden = [textView hasText];
    self.constrainTextBox.constant = (textView.contentSize.height >= 150) ? 150 : textView.contentSize.height;
    [self layoutSubviews];
    [self enableDisableSendButton];
}

-(void) enableDisableSendButton{
    if (attachments.count == 0){
        NSString *trimmed = [self.textBox.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        
        [_sendButton setEnabled:trimmed.length > 0];
        if (trimmed.length > 0){
            [_sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }else{
            [_sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }else{
        [_sendButton setEnabled:YES];
        [_sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }

}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.hintLabel.hidden = YES;
    [self.attachmentsCollection setHidden:(attachments.count == 0)];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![textView hasText]) {
        self.constrainTextBox.constant = 35;
    } else {
        self.constrainTextBox.constant = (textView.contentSize.height >= 150) ? 150 : textView.contentSize.height;
    }
    self.hintLabel.hidden = [textView hasText];
}

#pragma mark - GMImagePickerControllerDelegate
- (UIImage*)getVideoFrameOfVideoAtPath:(NSURL*)videoPath {
    //////NSLog(@"File path %@",videoPath);
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoPath options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    return [[UIImage alloc] initWithCGImage:imgRef];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedMediaImage;
    if (self.imgPicker) {
        [self.imgPicker dismissViewControllerAnimated:YES completion:nil];
    }
     NSString *selectedType = info[UIImagePickerControllerMediaType];
    
    if ([selectedType isEqualToString:(NSString *)kUTTypeVideo] || [selectedType isEqualToString:(NSString *)kUTTypeMovie]) {
        // picked video
        NSURL *urlvideo = info[UIImagePickerControllerMediaURL];
        //Error Container
        NSError *attributesError;
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[urlvideo path] error:&attributesError];
        NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
        long long fileSize = [fileSizeNumber longLongValue];
        if (fileSize / (1024 * 1024) > 50) {
            [SRModalClass showAlert:@"Sorry, we only accept media attachments of 50mb or less.  Please reduce the file size and try again."];
            return;
        }
        if (self.imgPicker) {
            [self.imgPicker dismissViewControllerAnimated:YES completion:nil];
        }
        if (!KEEP_ATTACHMENTS) {
            if (self.delegate) {
                [self.delegate onSendVideo:urlvideo];
            }
        } else {
            [self storeVideoAttachment:urlvideo withPreview:[self getVideoFrameOfVideoAtPath:urlvideo]];
            [self.attachmentsCollection reloadData];
            [self updateOnScreenPosition];
        }
        
    } else {
        // picked image
        if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"]) {
            /* Selected media is image */
            selectedMediaImage = info[UIImagePickerControllerOriginalImage];

        } else {
            selectedMediaImage = [self getVideoFrameOfVideoAtPath:info[UIImagePickerControllerMediaURL]];
        }
        
        NSData *imageData = UIImageJPEGRepresentation(selectedMediaImage, 0.0);
        selectedMediaImage = [UIImage imageWithData:imageData] ;
        if (imageData.length > 600000) {
            CGSize size = CGSizeMake(1500,1500);
            UIGraphicsBeginImageContext(size);
            [selectedMediaImage drawInRect:CGRectMake(0,0,size.width,size.height)];
            UIImage *contextImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            imageData = UIImageJPEGRepresentation(contextImage, 0.5f);
            selectedMediaImage = [UIImage imageWithData:imageData] ;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!KEEP_ATTACHMENTS) {
                if (self.delegate) {
                    [self.delegate onSendAttachment:selectedMediaImage];
                }
            } else {
                [self storeImageAttachment:selectedMediaImage];
                [self.attachmentsCollection reloadData];
                [self updateOnScreenPosition];
            }
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.imgPicker) {
            [self.imgPicker dismissViewControllerAnimated:YES completion:nil];
        }
    });
}

-(void)audioVideoCompression {
    
}

- (NSMutableArray *)createImageFilesWithData:(NSData *)data name:(NSString *)mediaFileName image:(UIImage *)originalImage {
    if (originalImage != nil) {
        data =  UIImageJPEGRepresentation(originalImage, 0.0);
        if (data.length > 600000) {
            originalImage = [UIImage imageWithData:data];
            data = UIImageJPEGRepresentation(originalImage, 0.5);
        }
    }
    NSMutableArray *pathNameArray = [NSMutableArray new];
    NSString *mediaFilePath = nil;
    NSString *mediaFolderPath=[self getMediaFolderPath:@"1"];
    mediaFilePath = [mediaFolderPath stringByAppendingPathComponent:mediaFileName];
    [data writeToFile:mediaFilePath atomically:NO];
    [pathNameArray addObject:mediaFilePath];
    
    UIImage *image = [UIImage imageWithData:data];
    CGSize size = CGSizeMake(500,500);
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width,size.height)];
    UIImage *contextImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    data = UIImageJPEGRepresentation(contextImage, 0.5f);
    mediaFileName = [NSString stringWithFormat:@"thumbpic_%@",mediaFileName];
    mediaFilePath = [mediaFolderPath stringByAppendingPathComponent:mediaFileName];
    [data writeToFile:mediaFilePath atomically:NO];
    [pathNameArray addObject:mediaFilePath];
    return pathNameArray;
}

- (NSString *)getMediaFolderPath:(NSString *)mediaType {
    
    NSString *mediaFolderPath;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    mediaFolderPath = [documentsDirectory stringByAppendingPathComponent:@"/SDKCometChatMediaFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    if ([mediaType isEqualToString:@"1"]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        mediaFolderPath = [mediaFolderPath stringByAppendingPathComponent:@"/Images"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        mediaFolderPath = [mediaFolderPath stringByAppendingPathComponent:@"/Sent"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
    } else if ([mediaType isEqualToString:@"2"]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        mediaFolderPath = [mediaFolderPath stringByAppendingPathComponent:@"/Videos"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        mediaFolderPath=[mediaFolderPath stringByAppendingPathComponent:@"/Sent"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
    } else {
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        mediaFolderPath = [mediaFolderPath stringByAppendingPathComponent:@"/Thumbnail"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
        mediaFolderPath = [mediaFolderPath stringByAppendingPathComponent:@"/Sent"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
        }
    }
    
    return mediaFolderPath;
}

-(void)checkMediaFolder:(NSString*)folderName {
    NSString *mediaFolderPath;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    mediaFolderPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", folderName]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:mediaFolderPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:mediaFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
}

- (void)createAndSendVideoFileWithURL:(NSURL*)videoURL data:(NSData*)mediaData fileName:(NSString*)mediaFileName {
}

@end
