#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "GrowingTextViewHandler.h"
#import "GMImagePickerController.h"
#import "MBProgressHUD.h"
#import "ChatMessageBox.h"
#import "ChatMapView.h"
#import "SRChatMessage.h"
#import "SRBluredImage.h"

@interface SRChatViewController : ParentViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate, UIScrollViewDelegate>
{
    // Instance variable
    // Model
    @private NSMutableArray<SRChatMessage *> *chatArr;
    NSDictionary *objectDict;
    NSDictionary *localEventDict;
    NSString * msgid;
    NSMutableArray *usersList;
    NSInteger count;
    // Server
    SRServerConnection *server;
    // Frame
    CGRect tblViewFrame;
    
    UIImage *friendProfileImg;
    MBProgressHUD *hud;
    // Map
    BOOL isBtnLocationClicked,hudShowing,isDownloadClicked,isRefreshList,isTableViewLoaded,isImageClicked,isPreviewOpen;
    CLLocationCoordinate2D *center;
    NSArray *arrAsset;
    IBOutlet NSLayoutConstraint *mapContainerHeight;
    IBOutlet ChatMapView *mapContainerView;
    NSString *lastMessageId;
    //
}

// Properties
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (strong, nonatomic) GrowingTextViewHandler *handler;
@property (nonatomic, strong) IBOutlet UIImageView *attachedImgView;

@property (nonatomic, strong) IBOutlet SRBluredImage *profileImgView;

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet ChatMessageBox *containerView;

@property (nonatomic, strong) IBOutlet UIView *imgCancelView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerBottomConstraint;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (nonatomic) float tabBarHeight;
@property (weak) id delegate;

@property BOOL isKeyboardOpen;



- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
         inObjectInfo:(NSDictionary *)inObject;
- (UIImage*)getVideoFrameOfVideoAtPath:(NSURL*)videoPath;


#pragma mark IBAction Method
- (void)actionOnBack:(id)sender;
- (IBAction)actionOnButtonClick:(id)sender;
- (IBAction)actionOnCancelButtonClick:(id)sender;
@end

