//
//  ChatMapView.m
//  Serendipity
//
//  Created by Mcuser on 2/15/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "ChatMapView.h"
#import "SRMapProfileView.h"
#import "UIImageView+WebCache.h"

@interface ChatMapView()<MKMapViewDelegate, CLLocationManagerDelegate>
@property PinAnnotation *myViewPin;
@property PinAnnotation *otherUser;
@end

@implementation ChatMapView

-(void)setOtherChatUserLocation:(NSDictionary*)location andImageUrl:(NSString*)url {
    self.otherChatUserLocation = location;
    if (!self.usersList) {
        self.usersList = [NSMutableArray new];
    }
    [self.usersList addObject:location];
}

-(void)initialize {
    self.mapView.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myLocationUpdated)
                                                 name:kKeyNotificationLocationChanged
                                               object:nil];
}

-(void)focusOnOtherLocation {
    if (self.otherChatUserLocation) {
        self.otherUser = nil;
        NSString *lat = [[self.otherChatUserLocation objectForKey:@"location"] valueForKey:@"lat"];
        NSString *lon = [[self.otherChatUserLocation objectForKey:@"location"] valueForKey:@"lon"];
        CLLocationCoordinate2D otherCoordinate = CLLocationCoordinate2DMake([lat doubleValue], [lon doubleValue]);
        [self.mapView removeAnnotations:self.mapView.annotations];
        self.otherUser = [[PinAnnotation alloc] init];
        self.otherUser.annId = [self.otherChatUserLocation valueForKey:@"id"];
        self.otherUser.coordinate = otherCoordinate;
        [self.mapView addAnnotation:self.otherUser];
        
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", [[self.otherChatUserLocation objectForKey:@"profile_image"] valueForKey:@"file"]];
        UIImageView *userProfileView = [UIImageView new];
//        [ sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//
//        }];
        
        // Add to view
        UIImage *img = [SRModalClass imageFromUIView:userProfileView];
        //self.otherUser.calloutAnnotation. = img;
       
//        [self.mapView setCenterCoordinate:otherCoordinate animated:YES];
        [self.mapView setCenterCoordinate:otherCoordinate zoomLevel:13 animated:true];
        
//        [self focusOnMyLocation];
    }
}

-(void)focusOnMyLocation {
    if ((APP_DELEGATE).lastLocation) {
        _myViewPin = nil;
        [self.mapView removeAnnotations:self.mapView.annotations];
        self.myViewPin = [[PinAnnotation alloc] init];
        self.myViewPin.coordinate = (APP_DELEGATE).lastLocation.coordinate;
        [self.mapView addAnnotation:self.myViewPin];
        //[self.mapView setCenterCoordinate:(APP_DELEGATE).lastLocation.coordinate animated:YES];
    }
}

#pragma mark - Custom events

-(void)drawUsers {
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    for (int i = 0; i < self.usersList.count; i++) {
        NSDictionary *userDic = [NSDictionary dictionaryWithDictionary:[self.usersList objectAtIndex:i]];
        userDic = [SRModalClass removeNullValuesFromDict:userDic];
        
        if ([[userDic valueForKey:kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            PinAnnotation *pinAnnotation = [[PinAnnotation alloc] init];
            CLLocationCoordinate2D coordinate;
            
            float lat = [[NSString stringWithFormat:@"%@",[userDic valueForKeyPath:@"location.lat"]] floatValue];//server.myLocation.coordinate.latitude;
            float lon = [[NSString stringWithFormat:@"%@",[userDic valueForKeyPath:@"location.lon"]] floatValue];//server.myLocation.coordinate.longitude;
            coordinate.latitude  = lat;
            coordinate.longitude = lon;
            pinAnnotation.annId = [NSString stringWithFormat:@"%@",[userDic valueForKey:kKeyId]];
            
            pinAnnotation.coordinate = coordinate;
            [self.mapView addAnnotation:pinAnnotation];
        }
    }
    [self.mapView showAnnotations:[self.mapView annotations] animated:YES];
}

#pragma mark MapView Delegates

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    //    MKZoomScale currentZoomScale = self.mapViewNearBy.bounds.size.width / self.mapViewNearBy.visibleMapRect.size.width;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation> )annotation {
    MKAnnotationView *annotationView;
    
    // check annotation is not user location
    if ([annotation isEqual:[mapView userLocation]]) {
        // Return for user pin
        return nil;
    }
    if ([annotation isKindOfClass:[PinAnnotation class]]) {
        // Pin annotation.
        annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"Pin"];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Pin"];
        }
        else {
            annotationView.annotation = annotation;
        }
        
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        SRMapProfileView *userProfileView = [nibArray objectAtIndex:1];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [(PinAnnotation *)annotation annId]];
        NSArray *filterArr = [self.usersList filteredArrayUsingPredicate:predicate];
        
        if ([filterArr count] > 0)
        {
            NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:[filterArr objectAtIndex:0]];
            
            if ([[userInfo objectForKey:kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *profileDict = [userInfo objectForKey:kKeyProfileImage];
                
                NSString *imageName = [profileDict objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                    [userProfileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                    }];
                    
                }
            }
        }
        // Provide round rect
        userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y - 2, 47, 49);
        
        userProfileView.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
        userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2.0;
        userProfileView.userProfileImg.clipsToBounds = YES;
        userProfileView.userProfileImg.layer.masksToBounds = YES;
        
        // Add to view
        UIImage *img = [SRModalClass imageFromUIView:userProfileView];
        annotationView.image = img;
        
    }
    // No callout
    [annotationView setCanShowCallout:NO];
    [annotationView setEnabled:YES];
    
    return annotationView;
}

//-----------------------------------------------------------------------------------------------------------------
// didSelectAnnotationView:

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
}

//-----------------------------------------------------------------------------------------------------------------
// didDeselectAnnotationView:

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    //Hide ProfilesScrollview if unhide
    
}

//-----------------------------------------------------------------------------------------------------------------
// didUpdateUserLocation:

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // Zoom to region containing the user location
}

#pragma mark - Location Delegates

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
}

-(void)myLocationUpdated {
    if (self.myViewPin) {
        self.myViewPin.coordinate = (APP_DELEGATE).lastLocation.coordinate;
    } else {
        [self focusOnMyLocation];
    }
}

@end
