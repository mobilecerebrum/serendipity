//
//  ChatMessageBox.h
//  Serendipity
//
//  Created by Mcuser on 1/29/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMImagePickerController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "SRImagePreviewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ChatMessageBoxDelegate <NSObject>

-(void)onTyping;
-(void)onShowLocations;
-(void)onSendMessage:(NSString*)message;
-(void)onSendAttachment:(UIImage*)attachment;
-(void)onSendVideo:(NSURL*)videoPath;
-(void)onSendLocationLon:(float)lontitude lat:(float)latitude;
-(void)updateMessageBoxPosition:(float)attachmentsHeight;
-(void)chatMessageDidSelectCamera;

@end;

@interface ChatMessageBox : UIView<UITextViewDelegate, GMImagePickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    CGFloat keyBoardHeight;
    UIImagePickerController *galleryPicker;
    NSMutableArray<NSDictionary*> *attachments;
    BOOL initialized;
    float initialY;
}

@property (strong, nonatomic) IBOutlet UILabel *hintLabel;
@property (strong, nonatomic) IBOutlet UIButton *attachmentButton;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UITextView *textBox;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UICollectionView *attachmentsCollection;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *attachmentsHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *selfHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constrainTextBox;
@property (strong, nonatomic) id<ChatMessageBoxDelegate> delegate;
@property NSMutableArray *usersList;

-(void)initialize;
-(float)attachmentsTableHeight;

-(void)storeImageAttachment:(UIImage*)image;
@end

NS_ASSUME_NONNULL_END
