//
//  SROtherMessageTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 1/28/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBaseChatTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SROtherMessageTableViewCell : SRBaseChatTableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *friendIMage;

@end

NS_ASSUME_NONNULL_END
