//
//  MyImageTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBaseChatTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MyVideoCellDelegate <NSObject>

-(void)updateUploadedVideoWithUrl:(NSString*)url forIndex:(NSInteger)index;
-(void)updateUploadedVideoAt:(int)position withResponse:(NSDictionary*)response;
- (void)didFailToSendMessage:(NSError*)error;

@end

@interface MyVideoTableViewCell : SRBaseChatTableViewCell {
    NSString *videoUrl;
    NSString *previewUrl;
    UIImage *videoPreview;
    BOOL isLoadingPreview;
}
@property (strong, nonatomic) IBOutlet UIImageView *imagePreview;
@property (strong, nonatomic) IBOutlet UILabel *msgDate;
@property (strong, nonatomic) IBOutlet UIImageView *playIcon;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) id<MyVideoCellDelegate> delegate;

//-(void)uploadVideo:(NSURL *)videoPath withText:(NSString*)str sender:(NSString *)sender receiverId:(NSString *)receiver;
-(void)uploadVideo:(NSURL *)videoPath withText:(NSString*)str sender:(NSString *)sender receiverId:(NSString *)receiver tag:(NSInteger)tag;
-(void)uploadVideo:(NSURL*)videoPath sender:(NSString*)sender receiverId:(NSString*)receiver;
-(void)configureCellForVideoUrl:(NSString*)video;
-(void)setVideoPreviewUrl:(NSString*)url;
-(void) hideShowLoadingIndicatorIsHidden:(BOOL) isHidden;
@end

NS_ASSUME_NONNULL_END
