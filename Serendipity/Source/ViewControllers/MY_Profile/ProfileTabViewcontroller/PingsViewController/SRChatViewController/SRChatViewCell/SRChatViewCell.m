#import "SRChatViewCell.h"

@implementation SRChatViewCell

#pragma mark
#pragma mark Standards Method
#pragma mark

// --------------------------------------------------------------------------------------------------------------------
// awakeFromNib

- (void)awakeFromNib {
    [super awakeFromNib];

	// Initialization code
	self.imgProfilePhoto.layer.cornerRadius = self.imgProfilePhoto.frame.size.width / 2.0;
	self.myProfileImg.layer.cornerRadius = self.myProfileImg.frame.size.width / 2.0;
}

// --------------------------------------------------------------------------------------------------------------------
// setSelected:

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
}

@end
