//
//  SRAttachmentTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 4/10/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRAttachmentTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
