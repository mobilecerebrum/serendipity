#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface SRChatViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SRProfileImageView *myProfileImg;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileBG;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePhoto,*imgVideo,*imgDownload,*imgPlay,*imgRetry;
@property (weak, nonatomic) IBOutlet UILabel *lblTextMessage,*lblSenderName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@end
