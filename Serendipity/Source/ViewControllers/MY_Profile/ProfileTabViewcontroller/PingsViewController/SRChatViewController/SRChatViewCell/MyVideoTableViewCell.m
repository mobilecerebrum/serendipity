//
//  MyImageTableViewCell.m
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "MyVideoTableViewCell.h"
#import "SRAPIManager.h"
#import "NSDate+SRCustomDate.h"

@implementation MyVideoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setVideoPreviewUrl:(NSString *)url {
    videoUrl = url;
    [self.imagePreview sd_setImageWithURL:[NSURL URLWithString:url]];
}

-(void)configureCellForVideoUrl:(NSString *)video {
    videoUrl = video;
    if (!videoPreview) {
        if (isLoadingPreview) return;
        isLoadingPreview = YES;
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:videoUrl]];
        AVAssetImageGenerator *_generator;
        _generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:playerItem.asset];
        
        AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef image, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error) {
            if (result == AVAssetImageGeneratorSucceeded) {
                UIImage *thumb = [UIImage imageWithCGImage:image];
                videoPreview = thumb;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.imagePreview.image = videoPreview;
                });
            } else {
               //  NSLog(@"%s - error: %@", __PRETTY_FUNCTION__, error);
            }
           
        };
        
        [_generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(0, 0)]] completionHandler:handler];
    }
}

-(void)uploadVideo:(NSURL *)videoPath sender:(NSString *)sender receiverId:(NSString *)receiver {
    self.playIcon.hidden = YES;
    self.loadingIndicator.hidden = NO;
    [[SRAPIManager sharedInstance] sendVideo:videoPath forSender:sender receiver:receiver forGroup:self.isGroup completion:^(id  _Nonnull result, NSError * _Nonnull error) {
        self.loadingIndicator.hidden = YES;
        if (!error) {
            self.playIcon.hidden = NO;
            if (self.delegate) {
                NSString *previewPath = [[[result objectForKey:@"response"] objectForKey:@"data"] objectForKey:@"preview"];
                [self.imagePreview sd_setImageWithURL:[NSURL URLWithString:previewPath]];
                [self.delegate updateUploadedVideoAt:(int)self.tag withResponse:[result objectForKey:@"response"]];
            }
        } else {
            if (self.delegate) {
                [self.delegate didFailToSendMessage:error];
            }
        }
    }];
}

-(void)uploadVideo:(NSURL *)videoPath withText:(NSString*)str sender:(NSString *)sender receiverId:(NSString *)receiver tag:(NSInteger)tag {
    self.playIcon.hidden = YES;
    self.loadingIndicator.hidden = NO;
    [self.loadingIndicator startAnimating];
    [[SRAPIManager sharedInstance] sendVideo:videoPath withText:str forSender:sender receiver:receiver forGroup:self.isGroup completion:^(id  _Nonnull result, NSError * _Nonnull error) {
        self.loadingIndicator.hidden = YES;
        if (!error) {
            self.playIcon.hidden = NO;
            if (self.delegate) {
                NSString *previewPath = [[[result objectForKey:@"response"] objectForKey:@"data"] objectForKey:@"preview"];
                [self.imagePreview sd_setImageWithURL:[NSURL URLWithString:previewPath]];
                [self.delegate updateUploadedVideoAt:(int)tag withResponse:[result objectForKey:@"response"]];
            }
        } else {
            if (self.delegate) {
                [self.delegate didFailToSendMessage:error];
            }
        }
    }];
}

-(void) hideShowLoadingIndicatorIsHidden:(BOOL) isHidden{
    self.loadingIndicator.hidden = isHidden;
    self.playIcon.hidden = !isHidden;

    if (isHidden){
        [self.loadingIndicator stopAnimating];
    }else{
        [self.loadingIndicator startAnimating];
    }
}

@end
