//
//  SRMyMessageTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 1/27/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBaseChatTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SRMyMessageCellDelegate <NSObject>

-(void)onMyMessageSent:(int)position withResponse:(NSDictionary*)response;
- (void)didFailToSendMessage:(NSError*)error;

@end

@interface SRMyMessageTableViewCell : SRBaseChatTableViewCell {
    
}

@property (nonatomic, strong) id<SRMyMessageCellDelegate> delegate;

-(void)sendMessage:(NSDictionary*)message;

@end

NS_ASSUME_NONNULL_END
