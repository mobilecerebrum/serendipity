//
//  OtherImageTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBaseChatTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface OtherVideoTableViewCell : SRBaseChatTableViewCell {
    NSString *videoUrl;
    NSString *previewUrl;
    UIImage *videoPreview;
    BOOL isLoadingPreview;
}
@property (strong, nonatomic) IBOutlet UIImageView *imagePreview;
@property (strong, nonatomic) IBOutlet UILabel *msgDate;
@property (strong, nonatomic) IBOutlet UIImageView *friendIMage;

-(void)configureCellForVideoUrl:(NSString*)video;
-(void)setVideoPreviewUrl:(NSString*)url;

@end

NS_ASSUME_NONNULL_END
