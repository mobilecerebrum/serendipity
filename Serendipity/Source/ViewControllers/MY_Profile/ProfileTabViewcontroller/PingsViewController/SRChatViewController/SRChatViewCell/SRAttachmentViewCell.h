//
//  SRAttachmentViewCell.h
//  Serendipity
//
//  Created by Mcuser on 4/13/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRAttachmentViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *attachmentPreview;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@end

NS_ASSUME_NONNULL_END
