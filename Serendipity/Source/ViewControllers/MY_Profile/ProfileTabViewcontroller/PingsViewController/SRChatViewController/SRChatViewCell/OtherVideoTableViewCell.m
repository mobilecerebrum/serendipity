//
//  OtherImageTableViewCell.m
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "OtherVideoTableViewCell.h"

@implementation OtherVideoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setVideoPreviewUrl:(NSString *)url {
    videoUrl = url;
    [self.imagePreview sd_setImageWithURL:[NSURL URLWithString:url]];
}

-(void)configureCellForVideoUrl:(NSString *)video {
    videoUrl = video;
    if (!videoPreview) {
        if (isLoadingPreview) return;
        isLoadingPreview = YES;
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:videoUrl]];
        AVAssetImageGenerator *_generator;
        _generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:playerItem.asset];
        
        AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef image, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error) {
            if (result == AVAssetImageGeneratorSucceeded) {
                UIImage *thumb = [UIImage imageWithCGImage:image];
                videoPreview = thumb;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.imagePreview.image = videoPreview;
                });
            } else {
              //  NSLog(@"%s - error: %@", __PRETTY_FUNCTION__, error);
            }
            
        };
        
        [_generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:CMTimeMakeWithSeconds(0, 0)]] completionHandler:handler];
    }
}

@end
