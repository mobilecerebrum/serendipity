//
//  MyLocationMessageCell.h
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBaseChatTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyLocationMessageCell : SRBaseChatTableViewCell

@end

NS_ASSUME_NONNULL_END
