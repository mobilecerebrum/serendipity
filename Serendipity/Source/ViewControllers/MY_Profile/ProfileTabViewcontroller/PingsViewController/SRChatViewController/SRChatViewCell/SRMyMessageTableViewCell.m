//
//  SRMyMessageTableViewCell.m
//  Serendipity
//
//  Created by Mcuser on 1/27/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRMyMessageTableViewCell.h"
#import "SRAPIManager.h"
#import "SRChatMessage.h"

@implementation SRMyMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)sendMessage:(SRChatMessage *)message {
        [[SRAPIManager sharedInstance] sendMessageSender:message.sender_id receiverId:[NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID] message:[message messageData] forGorup:self.isGroup completion:^(id  _Nonnull result, NSError * _Nonnull error) {
            if (!error) {
                if (self.delegate) {
                    [self.delegate onMyMessageSent:(int)self.tag withResponse:[result objectForKey:@"response"]];
                }
            } else {
                if (self.delegate) {
                    [self.delegate didFailToSendMessage:error];
                }
            }
        }];

}

@end
