//
//  SRBaseChatTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 1/28/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRBaseChatTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SRProfileImageView *userIcon;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *messageLbl;
@property BOOL isGroup;

@end

NS_ASSUME_NONNULL_END
