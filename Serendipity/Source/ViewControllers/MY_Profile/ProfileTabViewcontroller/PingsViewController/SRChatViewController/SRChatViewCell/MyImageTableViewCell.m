//
//  MyImageTableViewCell.m
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "MyImageTableViewCell.h"
#import "SRAPIManager.h"
#import "NSDate+SRCustomDate.h"

@implementation MyImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)uploadImage:(UIImage *)image sender:(NSString *)sender receiverId:(NSString *)receiver {
    self.imagePreview.image = image;
        [[SRAPIManager sharedInstance] sendImage:image forSender:sender receiver:receiver forGroup:self.isGroup completion:^(id  _Nonnull result, NSError * _Nonnull error) {
            if (!error) {
                if (self.delegate) {
                    [self.delegate updateUploadedImageAt:(int)self.tag withResponse:[result objectForKey:@"response"]];
                }
            } else {
               if (self.delegate) {
                   [self.delegate didFailToSendMessage:error];
               }
           }
        }];
}

-(void)uploadImage:(UIImage *)image withText:(NSString*)str sender:(NSString *)sender receiverId:(NSString *)receiver {
    self.imagePreview.image = image;
    [[SRAPIManager sharedInstance] sendImage:image withText:str forSender:sender receiver:receiver forGroup:self.isGroup completion:^(id  _Nonnull result, NSError * _Nonnull error) {
            if (!error) {
                if (self.delegate) {
                    [self.delegate updateUploadedImageAt:(int)self.tag withResponse:[result objectForKey:@"response"]];
                }
            } else {
                   if (self.delegate) {
                       [self.delegate didFailToSendMessage:error];
                   }
               }
        }];
}

@end
