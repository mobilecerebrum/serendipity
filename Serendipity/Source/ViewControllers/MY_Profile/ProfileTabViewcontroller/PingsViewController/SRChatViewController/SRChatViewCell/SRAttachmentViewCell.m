//
//  SRAttachmentViewCell.m
//  Serendipity
//
//  Created by Mcuser on 4/13/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAttachmentViewCell.h"

@implementation SRAttachmentViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.deleteButton.hidden = NO;
}

-(void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
//    self.deleteButton.hidden = !selected;
//
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        self.deleteButton.hidden = YES;
//    });
}

@end
