//
//  MyImageTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 1/31/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBaseChatTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MyImageCellDelegate <NSObject>

-(void)updateUploadedImageWithUrl:(NSString*)url forIndex:(NSInteger)index;
-(void)updateUploadedImageAt:(int)position withResponse:(NSDictionary*)response;
- (void)didFailToSendMessage:(NSError*)error;

@end

@interface MyImageTableViewCell : SRBaseChatTableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imagePreview;
@property (strong, nonatomic) IBOutlet UILabel *msgDate;
@property (strong, nonatomic) id<MyImageCellDelegate> delegate;

-(void)uploadImage:(UIImage*)image sender:(NSString*)sender receiverId:(NSString*)receiver;
-(void)uploadImage:(UIImage *)image withText:(NSString*)str sender:(NSString *)sender receiverId:(NSString *)receiver;

@end

NS_ASSUME_NONNULL_END
