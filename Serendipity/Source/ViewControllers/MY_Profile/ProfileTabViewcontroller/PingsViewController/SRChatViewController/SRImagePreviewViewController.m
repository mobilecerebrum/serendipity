//
//  SRImagePreviewViewController.m
//  Serendipity
//
//  Created by Mcuser on 1/29/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRImagePreviewViewController.h"

@interface SRImagePreviewViewController ()
@property (strong, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation SRImagePreviewViewController



-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.imageForPreview) {
        self.imageView.image = self.imageForPreview;
    }
    if (self.isPreviewingChat) {
        [self.sendButton setTitle:@"Download" forState:(UIControlStateNormal)];
    }
    
    if ([self.imageUrl isKindOfClass:[NSString class]]) {
        if (self.imageUrl.length != 0) {
            [self loadImageFromUrl:self.imageUrl];
        }
    } else if ([self.imageUrl isKindOfClass:[UIImage class]]) {
        self.imageView.image = self.imageUrl;
    }
}

-(void)loadImageFromUrl:(NSString *)url {
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        self.imageForPreview = image;
        self.imageView.image = image;
        [self.sendButton setTitle:@"Download" forState:(UIControlStateNormal)];
    }];
}

- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSend:(id)sender {
    
    if (self.delegate && !self.isPreviewingChat) {
        [self.delegate onSendAttachment:self.imageForPreview];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if (self.isPreviewingChat) {
        if (self.imageForPreview) {
            UIImageWriteToSavedPhotosAlbum(self.imageForPreview, nil, @selector(saveAlert), nil);
            [self saveAlert];
        }
    }
    
}

-(void)saveAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Saved" message:@"Image was saved to your photo album" preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:action];
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

@end
