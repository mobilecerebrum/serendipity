//
//  SRImagePickerViewController.m
//  Serendipity
//
//  Created by Mcuser on 2/21/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRImagePickerViewController.h"
#import <FastttCamera/FastttCamera.h>

@interface SRImagePickerViewController () <FastttCameraDelegate>
@property (nonatomic, strong) FastttCamera *fastCamera;
@property (strong, nonatomic) IBOutlet UIView *cameraPreview;
@property (strong, nonatomic) IBOutlet UIButton *switchCameraBtn;
@property (strong, nonatomic) IBOutlet UIButton *torchBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *takePhotoBtn;
@property (strong, nonatomic) IBOutlet UIButton *galleryBtn;
@end

@implementation SRImagePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _fastCamera = [FastttCamera new];
    self.fastCamera.delegate = self;
    
    [self fastttAddChildViewController:self.fastCamera];
    self.fastCamera.view.frame = self.view.frame;
}

- (IBAction)changeTorch:(UIButton *)sender {
}

- (IBAction)swapCamera:(UIButton *)sender {
}

- (IBAction)showGalleryAction:(id)sender {
}

- (IBAction)takePhotoAction:(id)sender {
    if (self.fastCamera.isReadyToCapturePhoto) {
        [self.fastCamera takePicture];
    }
}

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - IFTTTFastttCameraDelegate

- (void)cameraController:(FastttCamera *)cameraController didFinishCapturingImage:(FastttCapturedImage *)capturedImage {
    /**
     *  Here, capturedImage.fullImage contains the full-resolution captured
     *  image, while capturedImage.rotatedPreviewImage contains the full-resolution
     *  image with its rotation adjusted to match the orientation in which the
     *  image was captured.
     */
}

- (void)cameraController:(FastttCamera *)cameraController didFinishScalingCapturedImage:(FastttCapturedImage *)capturedImage {
    /**
     *  Here, capturedImage.scaledImage contains the scaled-down version
     *  of the image.
     */
}

- (void)cameraController:(FastttCamera *)cameraController didFinishNormalizingCapturedImage:(FastttCapturedImage *)capturedImage {
    /**
     *  Here, capturedImage.fullImage and capturedImage.scaledImage have
     *  been rotated so that they have image orientations equal to
     *  UIImageOrientationUp. These images are ready for saving and uploading,
     *  as they should be rendered more consistently across different web
     *  services than images with non-standard orientations.
     */
}

@end
