//
//  SRImagePreviewViewController.m
//  Serendipity
//
//  Created by Mcuser on 1/29/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRVideoPreviewViewController.h"
#import <AVKit/AVKit.h>
#import <Photos/Photos.h>
#import "HSLocationManager.h"
#import "SRModalClass.h"
@import GUIPlayerView;

@interface SRVideoPreviewViewController ()<GUIPlayerViewDelegate> {
    AVPlayerViewController *playerViewController;
    AVPlayerLayer *playerLayer;
    GUIPlayerView *videoPlayerView;
    NSString  *filePath;
}
@property (strong, nonatomic) IBOutlet UIButton *sendButton;

@end

@implementation SRVideoPreviewViewController



-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
        [self.sendButton setTitle:@"Download" forState:(UIControlStateNormal)];
    
//    if (self.imageUrl.length != 0) {
//        [self loadImageFromUrl:self.imageUrl];
//    }
}

-(void)loadVideoFromUrl:(NSString *)url {
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"Downloading Started");
        NSString *urlToDownload = url;
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData ) {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"preview.mov"];
            [urlData writeToFile:filePath atomically:YES];
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                self->item = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"file://%@", filePath]]];
                self->player = [AVPlayer playerWithPlayerItem:self->item];
                self->playerViewController = [AVPlayerViewController new];
                self->playerViewController.player = player;

                CGRect containerFrame = self.videoContainer.frame;
                containerFrame.origin.x = 0;
                containerFrame.origin.y = 0;
                
                playerViewController.view.frame = containerFrame;
                _activityIndicator.hidden = YES;
                [self addChildViewController:playerViewController];
                [self.videoContainer addSubview:playerViewController.view];
                [playerViewController didMoveToParentViewController:self];
                [player play];
            });
        }
    });
}

- (IBAction)onCancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSend:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
           [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:[NSURL URLWithString:filePath]];
       }
                                         completionHandler:^(BOOL success, NSError *error) {
                                             if (success)
                                             {
                                              //   NSLog(@"Video saved");
                                                 filePath = nil;
                                                 [self saveAlert];
                                             }else{
                                               //  NSLog(@"%@",error.description);
                                             }
                                         }];
    });
    
}

-(void)saveAlert {
    
//   UIViewController* currentVC = [[HSLocationManager sharedManager] topViewController];
//   UIViewController *presentedVC = currentVC.presentingViewController;  // you may need to loop through presentedViewControllers if you have more than one
//
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SRModalClass showAlertWithTitle:@"Video was saved to your album" alertTitle:@"Saved"];
       });
    
    
    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Saved" message:@"Video was saved to your album" preferredStyle:(UIAlertControllerStyleAlert)];
//
//    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }];
//
//    [alert addAction:action];
//
//    [self presentViewController:alert animated:YES completion:^{
//
//    }];
}

@end
