//
//  SRLocationPreviewViewController.h
//  Serendipity
//
//  Created by Mcuser on 2/5/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatMessageBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRLocationPreviewViewController : ParentViewController


@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property NSMutableArray *usersList;
@property (strong, nonatomic) id delegate;
@property CLLocationCoordinate2D viewCoordinates;

-(void)showLocationLontitude:(float)lon latitude:(float)lat;

@end

NS_ASSUME_NONNULL_END
