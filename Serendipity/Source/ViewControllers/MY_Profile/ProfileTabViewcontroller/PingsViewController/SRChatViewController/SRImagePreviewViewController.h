//
//  SRImagePreviewViewController.h
//  Serendipity
//
//  Created by Mcuser on 1/29/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatMessageBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRImagePreviewViewController : ParentViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property UIImage *imageForPreview;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) id delegate;
@property BOOL isPreviewingChat;
-(void)loadImageFromUrl:(NSString*)url;

@end

NS_ASSUME_NONNULL_END
