
#import "SRModalClass.h"
#import "SRChatViewCell.h"
#import "SRChatViewController.h"
#import "SRUserTabViewController.h"
#import "SREventDetailTabViewController.h"
#import "SRGroupDetailTabViewController.h"
#import "MBProgressHUD.h"
#import "UITableView+Scroll.h"
#import "DBManager.h"
#import <objc/runtime.h>
#import "SRBaseChatTableViewCell.h"
#import "SRMyMessageTableViewCell.h"
#import "NSDate+SRCustomDate.h"
#import "SRImagePreviewViewController.h"
#import "ComposeView.h"
#import "MyImageTableViewCell.h"
#import "OtherImageTableViewCell.h"
#import "MyVideoTableViewCell.h"
#import "OtherVideoTableViewCell.h"
#import "SROtherMessageTableViewCell.h"
#import "SRLocationPreviewViewController.h"
#import "SRVideoPreviewViewController.h"
#import "NSDate+SRCustomDate.h"
#import "UIScrollView+InfiniteScroll.h"


@import SocketIO;

@interface SRChatViewController () <ChatMessageBoxDelegate, MyImageCellDelegate, SRMyMessageCellDelegate, MyVideoCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
}
@end

@implementation SRChatViewController {
    SocketIOClient *socketClient;
    SocketManager *manager;
    ComposeView *composeView;
    BOOL isGroupChat;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
         inObjectInfo:(NSDictionary *)inObject {
    
    self = [super initWithNibName:@"SRChatViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        objectDict = inObject;
        chatArr = [[NSMutableArray alloc] init];
        
        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        
        [defaultCenter addObserver:self
                          selector:@selector(getUserlistSuccess:)
                              name:kKeyNotificationSearchUsersByIdSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getUserlistFailed:)
                              name:kKeyNotificationSearchUsersByIdFailed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(refreshList:)
                              name:kKeyNotificationRefreshList
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(refreshMedia:)
                              name:kKeyNotificationRefreshMedia
                            object:nil];
        // register for keyboard notifications
        [defaultCenter addObserver:self selector:@selector(keyboardWillShow:)
                              name:UIKeyboardWillShowNotification
                            object:self.view.window];
        // register for keyboard notifications
        [defaultCenter addObserver:self selector:@selector(keyboardWillHide:)
                              name:UIKeyboardWillHideNotification
                            object:self.view.window];
        [defaultCenter addObserver:self selector:@selector(backTabBar:)
                              name:kKeyNotificationBackClicked
                            object:nil];
        
    }
    
    //return
    return self;
}

// --------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Remove notification
    (APP_DELEGATE).isOnChat = false;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Lyfecycle

// --------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call Super
    [super viewDidLoad];
    self.tblView.transform = CGAffineTransformMakeScale (1,-1);
    
    [self registerTableCells];
//    (APP_DELEGATE).topBarView.hidden = YES;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    // self.handler = [[GrowingTextViewHandler alloc]initWithTextView:self.txtViewMessage withHeightConstraint:self.heightConstraint];
    [self.handler updateMinimumNumberOfLines:1 andMaximumNumberOfLine:2];
    self.attachedImgView.hidden = YES;
    self.imgCancelView.hidden = YES;
    // Navigation Bar BackGround
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    (APP_DELEGATE).isOnChat = true;
    
    //Allocate userlist show on map
    usersList = [[NSMutableArray alloc] init];
    
    // Set gesture to dismiss keyboard
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
//    tapGesture.cancelsTouchesInView = NO;
//     [self.tblView addGestureRecognizer:tapGesture];
    
    NSString *isGroupStr = [NSString stringWithFormat:@"%@", [objectDict objectForKey:@"isGroup"]];
    
    BOOL isOnChatList = (APP_DELEGATE).isOnChatList;
    if (([isGroupStr isEqualToString:@"1"])) {
        NSNumber *roomid;
        NSString *name;
        if (nil != objectDict[kKeyName]) {
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            roomid = [f numberFromString:objectDict[@"id"]];
            name = objectDict[kKeyName];
        } else {
            roomid = [objectDict[kKeyId] numberValue];
            name = objectDict[kKeyFirstName];
        }
        (APP_DELEGATE).chatUserID = roomid;
        [SRModalClass setNavTitle:name forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        NSString *imageName = @"";
        if ((APP_DELEGATE).isOnChatList) {
            imageName = objectDict[@"imageName"];
        } else {
            if (objectDict != nil) {
                if ([objectDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *profileDict = objectDict[kKeyMediaImage];
                    if (profileDict != nil) {
                        imageName = profileDict[kKeyImageName];
                    }
                }
            }
        }
        if ([imageName length] > 0) {
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
            [self.profileImgView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                friendProfileImg = image;                
                
                UIImage* blurImage = [self.profileImgView blurImage:image];
                self.profileImgView.image = blurImage;

            }];
        }
        isGroupChat = YES;
        localEventDict = objectDict;
        [self reloadTable:[[DBManager getSharedInstance] getGroupChatData:roomid]];
        [self.tblView setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    } else if ((NSNumber*)[objectDict valueForKey:@"EventNumber"]) {
        NSNumber *eventId = (NSNumber*)[objectDict valueForKey:@"group_ids"];
        (APP_DELEGATE).chatUserID = eventId;
        isGroupChat = YES;
        localEventDict = objectDict;
    } else {
        isGroupChat = NO;
        (APP_DELEGATE).chatUserID = objectDict[kKeyId];
        NSString *name = [NSString stringWithFormat:@"%@ %@", objectDict[kKeyFirstName], objectDict[kKeyLastName]];

//        NSString *name = [NSString stringWithFormat:@"%@ %@", objectDict[kKeyFirstName], [objectDict[kKeyLastName] length] > 0 ? [objectDict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)] : @""];
        [SRModalClass setNavTitle:name forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
        NSString *imageName = @"";
        if (nil != objectDict[@"imageName"]) {
            imageName = objectDict[@"imageName"];
        } else {
            NSDictionary *profileDict = objectDict[kKeyProfileImage];
            if ([profileDict isKindOfClass:[NSDictionary class]]) {
                if (profileDict[kKeyImageName]) {
                    imageName = profileDict[kKeyImageName];
                }
            }
        }
        if ([imageName length] > 0) {
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
            [self.profileImgView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                friendProfileImg = image;
                UIImage* blurImage = [self.profileImgView blurImage:image];
                self.profileImgView.image = blurImage;
            }];
        }
        // Get chat array
        [self reloadTable:[[DBManager getSharedInstance] getdata:server.loggedInUserInfo[kKeyId] receiver:objectDict[kKeyId]]];
        [self.tblView setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
    }
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.tblView.rowHeight = UITableViewAutomaticDimension;
    self.tblView.estimatedRowHeight = 150;
    
    //    self.tblView.contentInset = UIEdgeInsetsMake(24, 0, 24, 0);
    
    [self initComposeView];
    [self initSocket];
    [self.tblView setAllowsSelection:YES];
    [self loadChatHistory];
    
    // Add infinite scroll handler
    __weak typeof(self) weakSelf = self;
    
    [self.tblView addInfiniteScrollWithHandler:^(UITableView *tableView) {
        [weakSelf loadChatHistory];
    }];
    
    [self.tblView setShouldShowInfiniteScrollHandler:^BOOL(UIScrollView * _Nonnull scrollView) {
        return [[self->chatArr lastObject].msgId stringValue] != self -> lastMessageId;
    }];
    

    
//    SRBluredImage* imageView = [self.view viewWithTag:10000];
//    UIImage* blurImage = [imageView imageWithBlurredImageWithImage:imageView.image andBlurInsetFromBottom:imageView.frame.size.height withBlurRadius:3];
//    imageView.image = blurImage;
}

// ---------------------------------------------------------------------------------------------------------
// viewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    // Call Super
    [super viewWillAppear:animated];
    [self performSelector:@selector(doActionAfterSometime) withObject:nil afterDelay:3.0f];
    [self.containerView initialize];
    if (!isGroupChat) {
        if (objectDict[@"location"]) {
            if ([objectDict[@"profile_image"] isKindOfClass:[NSDictionary class]]) {
                [mapContainerView setOtherChatUserLocation:objectDict andImageUrl:[objectDict[@"profile_image"] valueForKey:@"file"]];
            }
        }
    }
    [mapContainerView initialize];
    if (self->isPreviewOpen == NO){
        [self scrollTableToBottom];
    }else{
        self->isPreviewOpen = NO;
    }
    (APP_DELEGATE).topBarView.hidden = true;
}

- (void)viewDidDisappear:(BOOL)animated {
    
}

- (void)animateMapBox {
    int newHight = 0;
    if (mapContainerHeight.constant == 0) {
        newHight = 200;
        [mapContainerView focusOnOtherLocation];
    } else {
        newHight = 0;
    }
    [UIView animateWithDuration:1
                     animations:^{
        mapContainerHeight.constant = newHight;
    }];
}

- (void)doActionAfterSometime {
    [self getUsersLocation];
    //Get user online status for avoid push notification
    [APP_DELEGATE hideActivityIndicator];
}

- (void)initComposeView {
    self.containerView.delegate = self;
}

-(void)loadChatHistoryForGroup:(NSString*)fromId {
    
    self->lastMessageId = [[chatArr lastObject].msgId stringValue];
    if (lastMessageId != nil){
        [[SRAPIManager sharedInstance] getMessageForGroup:[(APP_DELEGATE).chatUserID stringValue] andSender:fromId lastMessageId:self->lastMessageId completion:^(id  _Nonnull result, NSError * _Nonnull error) {
            [self processHistoryResults:error result:result];
        }];
    }
    
    //    [[SRAPIManager sharedInstance] getMessageForGroup:[(APP_DELEGATE).chatUserID stringValue] andSender:fromId completion:^(id  _Nonnull result, NSError * _Nonnull error) {
    //        [self processHistoryResults:error result:result];
    //    }];
}

- (void)processHistoryResults:(NSError *)error result:(id)result{
    
    [_tblView finishInfiniteScroll];
    
    if (!error) {
        //        chatArr = [NSMutableArray new];
        NSDictionary *res = (NSDictionary *) result;
        
        NSMutableArray *chatHistory = [[NSMutableArray alloc] initWithArray:res[@"response"][@"list"]];
        
        if (chatHistory) {
            for (NSDictionary *dc in chatHistory) {
                SRChatMessage *message = [[SRChatMessage alloc] initWithDictionary:dc];
                [chatArr addObject:message];
            }
            [self.tblView reloadData];
        }
    }
}

- (void)locadChatHistoryForUser:(NSString *)fromId {
    
    self -> lastMessageId = [[chatArr lastObject].msgId stringValue];
    [[SRAPIManager sharedInstance] getMessages:fromId receiverId:[NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID] lastMessageId:self -> lastMessageId completion:^(id  _Nonnull result, NSError * _Nonnull error) {
        [self processHistoryResults:error result:result];
    }];
}

- (void)loadChatHistory {
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    [[SRAPIManager sharedInstance] updateToken:(APP_DELEGATE).server.token];
    if (isGroupChat) {
        [self loadChatHistoryForGroup:fromId];
    } else {
        [self locadChatHistoryForUser:fromId];
    }
}

- (void)scrollTableToBottom {
//    [self.tblView scrollToBottom:NO];
    if (chatArr.count > 0){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

#pragma mark - Socket.IO methods

- (void)socketDisconnect {
    [socketClient disconnect];
}

- (void)initSocket {
    NSString *prodUrl;
    //prodUrl = @"http://35.163.216.102:3000";
    //prodUrl = @"http://localhost:3000";
    prodUrl = [NSString stringWithFormat:@"%@:%@", kSerendipityChatServer, kSerendipityChatPort];
    
    NSURL *url = [[NSURL alloc] initWithString:prodUrl];
    manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"compress": @YES}];
    socketClient = manager.defaultSocket;//[[SocketIOClient alloc] initWithManager:manager nsp:@"test"];
    [self socketSetListeners];
    [socketClient connect];
    
}

- (void)registerTableCells {
    UINib *myMessageNib = [UINib nibWithNibName:@"SRMyMessageTableViewCell" bundle:nil];
    [self.tblView registerNib:myMessageNib forCellReuseIdentifier:@"SRMyMessageTableViewCell"];
    
    UINib *otherMessageNib = [UINib nibWithNibName:@"SROtherMessageTableViewCell" bundle:nil];
    [self.tblView registerNib:otherMessageNib forCellReuseIdentifier:@"SROtherMessageTableViewCell"];
    
    UINib *otherImageMessageNib = [UINib nibWithNibName:@"OtherImageTableViewCell" bundle:nil];
    [self.tblView registerNib:otherImageMessageNib forCellReuseIdentifier:@"OtherImageTableViewCell"];
    
    UINib *myImageMessageNib = [UINib nibWithNibName:@"MyImageTableViewCell" bundle:nil];
    [self.tblView registerNib:myImageMessageNib forCellReuseIdentifier:@"MyImageTableViewCell"];
    
    UINib *myLocationNib = [UINib nibWithNibName:@"MyLocationMessageCell" bundle:nil];
    [self.tblView registerNib:myLocationNib forCellReuseIdentifier:@"MyLocationMessageCell"];
    
    UINib *otherLocationNib = [UINib nibWithNibName:@"OtherLocationMessageCell" bundle:nil];
    [self.tblView registerNib:otherLocationNib forCellReuseIdentifier:@"OtherLocationMessageCell"];
    
    UINib *myVideoNib = [UINib nibWithNibName:@"MyVideoTableViewCell" bundle:nil];
    [self.tblView registerNib:myVideoNib forCellReuseIdentifier:@"MyVideoTableViewCell"];
    
    UINib *otherVideoNib = [UINib nibWithNibName:@"OtherVideoTableViewCell" bundle:nil];
    [self.tblView registerNib:otherVideoNib forCellReuseIdentifier:@"OtherVideoTableViewCell"];
}

- (void)socketSetListeners {
    NSAssert(socketClient != nil, @"Socket client should be initialized");
    
    //    [socketClient onAny:^(SocketAnyEvent * event) {
    //        NSLog(@"Socket event: %@", event.event);
    //    }];
    //
    //
    //    [socketClient on:@"typing" callback:^(NSArray * _Nonnull arrayData, SocketAckEmitter * _Nonnull ack) {
    //        NSLog(@"User is typing");
    //    }];
    
    [socketClient on:[self getSocketMessagesLink] callback:^(NSArray *_Nonnull arrayData, SocketAckEmitter *_Nonnull ack) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
           // NSLog(@"IO: Got message");
            NSDictionary *message = (NSDictionary *) arrayData[0];
            SRChatMessage *chatMessage = [[SRChatMessage alloc] initWithDictionary:message];
            
            if (chatArr.count != 0) {
                SRChatMessage *lastMessage = chatArr[chatArr.count - 1];
                if ([chatMessage.sender_id isKindOfClass:[NSString class]]) {
                    if ([[NSString stringWithFormat:@"%@", lastMessage.sender_id] isEqualToString:[NSString stringWithFormat:@"%@", chatMessage.sender_id]]) {
                        return;
                    }
                } else if ([chatMessage.sender_id isKindOfClass:[NSNumber class]]) {
                    if ([[NSString stringWithFormat:@"%@", lastMessage.sender_id] isEqualToString:[NSString stringWithFormat:@"%@", chatMessage.sender_id]]) {
                        return;
                    }
                }
            }
            if ([chatMessage isImage]) {
                NSString *incomingImage = [chatMessage messageData];
                if (![self historyContainsImage:incomingImage]) {
                    [self storeIncomeMessage:chatMessage];
                }
            } else {
                if (![self historyContainsMessageWithId:chatMessage.msgId]) {
                    [self storeIncomeMessage:chatMessage];
                }
            }
        });
    }];
    
    [socketClient on:@"error" callback:^(NSArray *_Nonnull arrayData, SocketAckEmitter *_Nonnull ack) {
     //   NSLog(@"error with -- %@ && ACK -- %@", [arrayData description], [ack debugDescription]);
    }];
}

- (NSString *)getMessageTime:(SRChatMessage *)chatMessage {
    if (chatMessage.created_at == 0) {
        return @"";
    }
    long long messageTime = chatMessage.created_at;
    NSDate *messageDate = [NSDate dateWithTimeIntervalSince1970:messageTime];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSString *messageDateStr = [dateFormatter stringFromDate:messageDate];
    messageDateStr = [NSDate chatTime:messageDateStr];
    return messageDateStr;
}

- (void)storeIncomeMessage:(SRChatMessage *)message {
    [chatArr insertObject:message atIndex:0];
//    chatArr = [[self sortAllMessages] mutableCopy];
    [self.tblView reloadData];
    [self scrollTableToBottom];
}

-(NSMutableArray<SRChatMessage *> *)sortAllMessages {
    //    NSMutableArray<SRChatMessage *> * arrKeys = [[chatArr sortedArrayUsingComparator:^NSComparisonResult(SRChatMessage* obj1, SRChatMessage* obj2) {
    //        NSDate *d1 = [NSDate dateWithTimeIntervalSince1970:obj1.created_at];
    //        NSDate *d2 = [NSDate dateWithTimeIntervalSince1970:obj2.created_at];
    //        return [d1 compare:d2] == NSOrderedDescending;
    //    }] mutableCopy] ;
    return chatArr;//[arrKeys mutableCopy];
}

- (NSString *)getSocketMessagesLink {
    NSString *result = @"message-";
    
    if (!isGroupChat) {
        NSString *fromId = server.loggedInUserInfo[kKeyId];
        NSString *receverId = [NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@-%@", fromId, receverId]];
    } else {
        result = @"group-message-";
        NSString *fromId = server.loggedInUserInfo[kKeyId];
        NSString *receverId = [NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID];
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@", receverId]];
        
    }
    return [result stringByAppendingString:@":message"];
}

- (void)socketListenMessages {
    NSAssert(socketClient != nil, @"Socket client should be initialized");
    
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    NSString *token = [server.token stringByReplacingOccurrencesOfString:@"Bearer " withString:@""];
    [socketClient on:[NSString stringWithFormat:@"/api/messages?sender_id=%@&receiver_id=%@&token=%@", fromId, (APP_DELEGATE).chatUserID, token] callback:^(NSArray *_Nonnull dataArr, SocketAckEmitter *_Nonnull ack) {
//NSLog(@"Got messages");
    }];
}

#pragma mark Private Method

// --------------------------------------------------------------------------------
// profilePicTapAction


- (void)refreshMedia:(NSNotification *)notification {
    isDownloadClicked = (APP_DELEGATE).IsDownloadStarted;
    NSIndexPath *rowToReload = [NSIndexPath indexPathForRow:[chatArr count] - 1 inSection:0];
    NSArray *rowsToReload = @[rowToReload];
    [_tblView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
}

- (BOOL)historyContainsImage:(NSString *)imageUrl {
    if (chatArr.count != 0) {
        for (int i = (int) (chatArr.count - 1); i != 0; i--) {
            SRChatMessage *msg = chatArr[i];
            if ([msg isImage]) {
                NSString *imgUrl = [msg messageData];
                if ([imageUrl isEqualToString:imgUrl]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (BOOL)historyContainsMessageWithId:(NSNumber *)messageId {
    if (chatArr.count != 0) {
        for (int i = (int) (chatArr.count - 1); i != 0; i--) {
            SRChatMessage *msg = chatArr[i];
            
            if (msg.msgId == messageId) {
                return YES;
            }
        }
    }
    return NO;
}

- (void)refreshList:(NSNotification *)notification {
    if ((localEventDict[@"chat_room_id"] != nil || ((APP_DELEGATE).isOnChatList && [localEventDict[@"isGroup"] isEqualToString:@"1"]))) {
        if (objectDict[@"chat_room_id"] != nil) {
            if ([(APP_DELEGATE).msgType isEqual:@10]) {
                [self refreshTableWithNewItem:[[DBManager getSharedInstance] getGroupChatData:[localEventDict valueForKey:@"chat_room_id"]]];
            } else {
                [self reloadTable:[[DBManager getSharedInstance] getGroupChatData:[localEventDict valueForKey:@"chat_room_id"]]];
            }
        } else {
            if ([(APP_DELEGATE).msgType isEqual:@10]) {
                [self refreshTableWithNewItem:[[DBManager getSharedInstance] getGroupChatData:[localEventDict valueForKey:kKeyId]]];
            } else {
                [self reloadTable:[[DBManager getSharedInstance] getGroupChatData:[localEventDict valueForKey:kKeyId]]];
            }
        }
    } else {
        if ([(APP_DELEGATE).msgType isEqual:@10]) {
            [self refreshTableWithNewItem:[[DBManager getSharedInstance] getdata:server.loggedInUserInfo[kKeyId] receiver:objectDict[kKeyId]]];
        } else {
            [self reloadTable:[[DBManager getSharedInstance] getdata:server.loggedInUserInfo[kKeyId] receiver:objectDict[kKeyId]]];
        }
    }
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (CGRect)resizeLabelWithText:(NSString *)inStr messageType:(NSDictionary *)msgDict {
    //Calculate the expected size based on the font and linebreak mode of your label
    CGFloat width;
    if ([msgDict[@"self"] boolValue] == 1) {
        width = SCREEN_WIDTH - 110;
    } else {
        width = SCREEN_WIDTH - 110;
    }
    CGSize maximumLabelSize = CGSizeMake(width, CGFLOAT_MAX);
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributesDictionary = @{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:14.0], NSParagraphStyleAttributeName: paragraph};
    CGRect rect = [inStr boundingRectWithSize:maximumLabelSize
                                      options:(NSStringDrawingUsesLineFragmentOrigin |
                                               NSStringDrawingUsesFontLeading)
                                   attributes:attributesDictionary
                                      context:nil];
    maximumLabelSize = rect.size;
    rect.size.width = width;
    return rect;
}

// reloadTableViewWIthMessages

- (void)reloadTableViewWithMessages:(NSArray *)inChatMsgArr {
}

// --------------------------------------------------------------------------------
//  Get User lOcation for show on map
- (void)getUsersLocation {
}

- (void)reloadTable:(NSMutableArray *)arr {
    
}

- (void)refreshTableWithNewItem:(NSMutableArray *)arr {
}

#pragma mark NavBarAction Methods

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self socketDisconnect];
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark IBAction Method

- (void)backTabBar:(NSNotification *)notification {
    (APP_DELEGATE).isPreviewOpen = false;
    if (isImageClicked) {
        isImageClicked = false;
        self.attachedImgView.frame = CGRectMake(self.attachedImgView.frame.origin.x, self.attachedImgView.frame.origin.y, self.view.frame.size.width, self.attachedImgView.frame.size.height - 55);
        self.tblView.userInteractionEnabled = YES;
    }
    self.attachedImgView.hidden = YES;
    self.imgCancelView.hidden = YES;
}

- (IBAction)actionOnCancelButtonClick:(id)sender {
    [self actionOnBack:sender];
}

- (IBAction)actionOnButtonClick:(id)sender {
    
}

#pragma mark - SRMyMessageCellDelegate

- (void)onMyMessageSent:(int)position withResponse:(NSDictionary *)response {
    SRChatMessage *newMessage = [[SRChatMessage alloc] initWithDictionary:response];
    chatArr[0] = newMessage;
}

-(void)didFailToSendMessage:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning" message:@"You can't send message to this user." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:true];
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark - Video Cell Delegate

- (void)updateUploadedVideoAt:(int)position withResponse:(NSDictionary *)response {
    SRChatMessage *newMessage = [[SRChatMessage alloc] initWithDictionary:response];
    chatArr[0] = newMessage;
    [self.tblView reloadData];
}

#pragma mark - Image Cell Delegate

- (void)updateUploadedImageAt:(int)position withResponse:(NSDictionary *)response {
    SRChatMessage *newMessage = [[SRChatMessage alloc] initWithDictionary:response];
    chatArr[0] = newMessage;
    [self.tblView reloadData];
}

- (void)updateUploadedImageWithUrl:(NSString *)url forIndex:(NSInteger)index {
    if (index != 0) {
        index = index - 1;
    }
    SRChatMessage *mDict = chatArr[index];
    mDict.type = @"image";
    mDict.data = @{@"image": url};
    chatArr[index] = mDict;
    [self.tblView reloadData];
}


#pragma mark - ChatBox Delegates

- (void)updateMessageBoxPosition:(float)attachmentsHeight {
    if (attachmentsHeight > 100) {
        attachmentsHeight -= self.tabBarHeight;
    }
    float margin = 0;
    NSArray<UIViewController*> *controllers = self.tabBarController.viewControllers;
    if (controllers.count != 0) {
        // margin = (float) self.tabBarController.tabBar.frame.size.height;
    }
    self.containerBottomConstraint.constant = -attachmentsHeight + margin;
}

- (void)onTyping {
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    [socketClient emit:@"typing" with:@[
        @{@"from": fromId}
    ]];
}

- (void)onSendVideo:(NSURL *)videoPath {
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    
    NSDictionary *uploadingImage = @{
        @"type": @"sending_video",
        @"sender_id": fromId,
        @"receiver_id": [NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID],
        @"created_at": @([[NSDate date] timeIntervalSince1970]),
        @"data": @{@"video": [videoPath absoluteString], @"text": self.containerView.textBox.text}
    };
    SRChatMessage *message = [[SRChatMessage alloc] initWithDictionary:uploadingImage];
    [chatArr insertObject:message atIndex:0];
    [_tblView beginUpdates];
    [_tblView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [_tblView endUpdates];
//    chatArr = [[self sortAllMessages] mutableCopy];
//    [self.tblView reloadData];
    [self scrollTableToBottom];
    
}

- (void)onSendAttachment:(UIImage *)attachment {
    
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    
    NSDictionary *uploadingImage = @{
        @"type": @"sending_image",
        @"sender_id": fromId,
        @"receiver_id": [NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID],
        @"created_at": @([[NSDate date] timeIntervalSince1970]),
        @"data": @{@"image": attachment, @"text": self.containerView.textBox.text}
    };
    
    SRChatMessage *message = [[SRChatMessage alloc] initWithDictionary:uploadingImage];
    message.imageToSend = attachment;
    
    [chatArr insertObject:message atIndex:0];
    
    [_tblView beginUpdates];
    [_tblView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [_tblView endUpdates];
//    chatArr = [[self sortAllMessages] mutableCopy];
//    [self.tblView reloadData];
    [self scrollTableToBottom];
}

- (void)onSendMessage:(NSString *)message {
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    
    NSDictionary *newMessage = @{
        @"type": @"sending_message",
        @"sender_id": fromId,
        @"receiver_id": [NSString stringWithFormat:@"%@", (APP_DELEGATE).chatUserID],
        @"created_at": @([[NSDate date] timeIntervalSince1970]),
        @"data": @{@"text": message}
    };
    
    SRChatMessage *chatMessage = [[SRChatMessage alloc] initWithDictionary:newMessage];
    [chatArr insertObject:chatMessage atIndex:0];
//    chatArr = [[self sortAllMessages] mutableCopy];
    [_tblView beginUpdates];
    [_tblView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [_tblView endUpdates];
//    [self.tblView reloadData];
    [self scrollTableToBottom];
}

- (void)onSendLocationLon:(float)lontitude lat:(float)latitude {
    //    long long currentTime = (long long)([[NSDate date] timeIntervalSince1970] * 1000);
    //    NSString *fromId = [server.loggedInUserInfo objectForKey:kKeyId];
    //    NSString *fromFirstName = [server.loggedInUserInfo objectForKey:kKeyName];
    //    NSString *fromLastName = [server.loggedInUserInfo objectForKey:kKeyLastName];
    //
    //    [socketClient emit:@"createLocationMessage" with:@[@{@"from" : fromId,
    //                                                      @"from_name" : [NSString stringWithFormat: @"%@ %@", fromFirstName, fromLastName],
    //                                                         @"to" : (APP_DELEGATE).chatUserID,
    //                                                         @"type" : @"location",
    //                                                         @"lon" : [NSNumber numberWithFloat:lontitude],
    //                                                         @"lat" : [NSNumber numberWithFloat:latitude],
    //                                                         @"createdAt" : [NSString stringWithFormat:@"%lld", currentTime]
    //                                                      }]];
}

- (void)onShowLocations {
    [self animateMapBox];
}

#pragma mark TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  //  NSLog(@"%lu", (unsigned long)[chatArr count]);
    return [chatArr count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isKeyboardOpen){
        [self.view endEditing:YES];
        return;
    }
    SRChatMessage *chatMessage = chatArr[indexPath.row];
    if ([chatMessage isVideo]) {
        
        // open video
        SRVideoPreviewViewController *videoPreview = [[SRVideoPreviewViewController alloc] initWithNibName:@"SRVideoPreviewViewController" bundle:nil];
        [videoPreview loadVideoFromUrl:[chatMessage messageData]];
        [videoPreview setModalPresentationStyle:UIModalPresentationFullScreen];
        self->isPreviewOpen = YES;
        [self presentViewController:videoPreview animated:YES completion:nil];
        
    } else if ([chatMessage isImage]) {
        SRImagePreviewViewController *imagePreview = [[SRImagePreviewViewController alloc] initWithNibName:@"SRImagePreviewViewController" bundle:nil];
        [imagePreview setModalPresentationStyle:UIModalPresentationFullScreen];
        
        if ([chatMessage isUploadingImage]) {
            if ([[chatMessage valueForKey:@"image"] containsString:@"http"]) {
                //[imagePreview.imageView sd_setImageWithURL:[NSURL URLWithString:[chatMessage valueForKey:@"image"]]];
                //[imagePreview loadImageFromUrl:[chatMessage valueForKey:@"image"]];
                imagePreview.imageUrl = [chatMessage messageData];
            } else {
                imagePreview.imageView.image = [chatMessage imageToUpload];
            }
        } else {
            NSString *imgUrl = [chatMessage messageData];
            imagePreview.imageUrl = imgUrl;
        }
        
        imagePreview.isPreviewingChat = YES;
        self->isPreviewOpen = YES;
        [self presentViewController:imagePreview animated:YES completion:nil];
    }
    //        else if ([messageType isEqualToString:@"location"]) {
    //            SRLocationPreviewViewController *locationView = [[SRLocationPreviewViewController alloc] initWithNibName:@"SRLocationPreviewViewController" bundle:nil];
    //            locationView.viewCoordinates = CLLocationCoordinate2DMake([[chatMessage valueForKey:@"lat"] floatValue], [[chatMessage valueForKey:@"lon"] floatValue]);
    //            [self presentViewController:locationView animated:YES completion:nil];
    //        }
    //}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    SRChatMessage *chatMessage = chatArr[indexPath.row];
   // NSLog(@"%@", chatMessage);
    if ([chatMessage isVideo]) {
        cell = [self createVideoCell:chatMessage withRowIndex:indexPath.row];
    } else if ([chatMessage isText]) {
        cell = [self createMessageCellForMessage:chatMessage withRowIndex:indexPath.row];
    } else if ([chatMessage isImage]) {
        cell = [self createImageCellForMessage:chatMessage];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.transform = CGAffineTransformMakeScale (1,-1);
    return cell;
}

- (UITableViewCell *)createVideoCell:(SRChatMessage *)message withRowIndex:(NSInteger)rowIndex {
    UITableViewCell *cell;
    if ([self isMyMessage:message] || [message.type isEqualToString:@"sending_video"]) {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"MyVideoTableViewCell"];
        ((SRBaseChatTableViewCell*)cell).isGroup = isGroupChat;
        if ([message isVideo]) {
            ((MyVideoTableViewCell *) cell).delegate = self;
            ((MyVideoTableViewCell *) cell).tag = chatArr.count - 1;
            //  ((MyVideoTableViewCell*)cell).timeLbl.text = [self getMessageTime:message];
            
            if ([message.type isEqualToString:@"sending_video"]) {
                if (message.whileUploadVideo == FALSE) {
                    message.whileUploadVideo = true;
                    [((MyVideoTableViewCell *) cell) uploadVideo:[NSURL URLWithString:[message messageData]] withText:[message messageText] sender:message.sender_id receiverId:message.receiver_id tag:rowIndex];
                }
                [((MyVideoTableViewCell *) cell) hideShowLoadingIndicatorIsHidden:NO];
                [((MyVideoTableViewCell *) cell).imagePreview sd_setImageWithURL:[NSURL URLWithString:[message videoPreview]]];
                ((MyVideoTableViewCell *) cell).timeLbl.text = [message messageTime];
                ((MyVideoTableViewCell *) cell).messageLbl.text = [message messageText];
            } else {
                [((MyVideoTableViewCell *) cell) hideShowLoadingIndicatorIsHidden:YES];
                [((MyVideoTableViewCell *) cell).imagePreview sd_setImageWithURL:[NSURL URLWithString:[message videoPreview]]];
                ((MyVideoTableViewCell *) cell).timeLbl.text = [message messageTime];
                ((MyVideoTableViewCell *) cell).messageLbl.text = [message messageText];
            }
        } else {
            [((MyVideoTableViewCell *) cell) hideShowLoadingIndicatorIsHidden:YES];
            [((MyVideoTableViewCell *) cell) setVideoPreviewUrl:[message videoPreview]];
            [((MyVideoTableViewCell *) cell) configureCellForVideoUrl:[message messageData]];
            ((MyVideoTableViewCell *) cell).timeLbl.text = [message messageTime];
            ((MyVideoTableViewCell *) cell).messageLbl.text = [message messageText];
        }
    } else {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"OtherVideoTableViewCell"];
        if (friendProfileImg == nil) {
                [((OtherVideoTableViewCell *) cell).friendIMage sd_setImageWithURL:[NSURL URLWithString:message.strSenderImage] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (image != nil){
                        ((OtherVideoTableViewCell *) cell).friendIMage.image = image;
                    }
                }];
        } else {
            ((OtherVideoTableViewCell *) cell).friendIMage.image = friendProfileImg;
        }
        [((OtherVideoTableViewCell *) cell) setVideoPreviewUrl:[message videoPreview]];
        [((OtherVideoTableViewCell *) cell) configureCellForVideoUrl:[message messageData]];
        ((OtherVideoTableViewCell *) cell).timeLbl.text = [message messageTime];
        ((OtherVideoTableViewCell *) cell).messageLbl.text = [message messageText];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (UITableViewCell *)createImageCellForMessage:(SRChatMessage *)message {
    UITableViewCell *cell;
    NSString *base64Image = [message messageData];
    if ([self isMyMessage:message] || [message isUploadingImage]) {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"MyImageTableViewCell"];
        ((SRBaseChatTableViewCell*)cell).isGroup = isGroupChat;
        if (base64Image && !message.isUploadingImage && ![message.type isEqualToString:@"sendingimage"]) {
            NSString *imageUrl = [message messageData];
            [((MyImageTableViewCell *) cell).imagePreview sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            NSString *text = [message messageText];
            NSString *time = [self getMessageTime:message];
            ((MyImageTableViewCell *) cell).messageLbl.text = text;
            ((MyImageTableViewCell *) cell).timeLbl.text = time;
        } else if ([message isUploadingImage] || [message.type isEqualToString:@"sendingimage"]) {
            ((MyImageTableViewCell *) cell).imagePreview.tag = chatArr.count;
            ((MyImageTableViewCell *) cell).delegate = self;
            ((MyImageTableViewCell *) cell).tag = chatArr.count - 1;
            NSString *text = [message messageText];
            NSString *time = [self getMessageTime:message];
            ((MyImageTableViewCell *) cell).timeLbl.text = time;
            ((MyImageTableViewCell *) cell).messageLbl.text = text;
            if ([message isUploadingImage]) {
                [((MyImageTableViewCell *) cell) uploadImage:[message imageToUpload] withText:[message messageText] sender:message.sender_id receiverId:message.receiver_id];
                
                message.type = @"sendingimage";
            }
        }
    } else {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"OtherImageTableViewCell"];
        if (base64Image && !message.isUploadingImage && ![message.type isEqualToString:@"sendingimage"]) {
            NSString *imageUrl = [message messageData];
            [((OtherImageTableViewCell *) cell).imagePreview sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            ((OtherImageTableViewCell *) cell).msgDate.text = [self getMessageTime:message];
        } else {
            ((OtherImageTableViewCell *) cell).imagePreview.image = [message imageToUpload];
            
        }
        if (friendProfileImg == nil) {
            [((OtherVideoTableViewCell *) cell).friendIMage sd_setImageWithURL:[NSURL URLWithString:message.strSenderImage] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image != nil){
                    ((OtherVideoTableViewCell *) cell).friendIMage.image = image;
                }
            }];
        } else {
            ((OtherImageTableViewCell *) cell).friendIMage.image = friendProfileImg;
        }
        NSString *text = [message messageText];
        NSString *time = [self getMessageTime:message];
        ((OtherImageTableViewCell *) cell).messageLbl.text = text;
        ((OtherImageTableViewCell *) cell).timeLbl.text = time;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (UITableViewCell *)createMessageCellForMessage:(SRChatMessage *)message withRowIndex:(NSInteger)rowIndex {
    UITableViewCell *cell;
    if ([self isMyMessage:message]) {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"SRMyMessageTableViewCell"];
    } else {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"SROtherMessageTableViewCell"];
        if (friendProfileImg == nil) {
            ((SROtherMessageTableViewCell *) cell).friendIMage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            
            [((SROtherMessageTableViewCell *) cell).friendIMage sd_setImageWithURL:[NSURL URLWithString:message.strSenderImage] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image != nil){
                    ((SROtherMessageTableViewCell *) cell).friendIMage.image = image;
                }
            }];

        } else {
            ((SROtherMessageTableViewCell *) cell).friendIMage.image = friendProfileImg;
        }
    }
    
    NSString *messageText = @"";
    ((SRBaseChatTableViewCell*)cell).isGroup = isGroupChat;
    if ([message.type isEqualToString:@"sending_message"]) {
        messageText = [message messageData];
        [((SRMyMessageTableViewCell *) cell) sendMessage:message];
        ((SRMyMessageTableViewCell *) cell).delegate = self;
        ((SRMyMessageTableViewCell *) cell).tag = rowIndex;
        message.type = @"sendingmessage";
        
    }
    messageText = [message messageData];
    
    ((SRBaseChatTableViewCell *) cell).messageLbl.text = messageText;
    
    ((SRBaseChatTableViewCell *) cell).timeLbl.text = [self getMessageTime:message];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (UITableViewCell *)createLocationCellForMessage:(NSDictionary *)message {
    UITableViewCell *cell;
    if ([self isMyMessage:message]) {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"MyLocationMessageCell"];
    } else {
        cell = [self.tblView dequeueReusableCellWithIdentifier:@"OtherLocationMessageCell"];
    }
    return cell;
}

- (BOOL)isMyMessage:(SRChatMessage *)message {
    NSString *fromId = server.loggedInUserInfo[kKeyId];
    NSString *fromMessage = [NSString stringWithFormat:@"%@", message.sender_id];
    return [fromId isEqualToString:fromMessage];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    SRChatMessage *chatMessage = chatArr[(NSUInteger) indexPath.row];
    if (chatMessage.type) {
        NSString *messageType = chatMessage.type;
        if ([messageType isEqualToString:@"location"]) {
            return 268;
        } else if ([messageType isEqualToString:@"image"]) {
            return UITableViewAutomaticDimension;
        }
    }
    return UITableViewAutomaticDimension;
}

- (void)playVideo:(UITapGestureRecognizer *)gesture {
    
}

- (void)resendMedia:(UITapGestureRecognizer *)gesture {
    
}

- (void)showImage:(UITapGestureRecognizer *)gesture {
    
}

- (void)btnDownloadClicked:(UITapGestureRecognizer *)gesture {
    
}

#pragma mark- keyborad show/hide Notification Action

- (void)keyboardWillHide:(NSNotification *)notification {
    self.isKeyboardOpen = NO;
//    NSDictionary *userInfo = [notification userInfo];
//
//    // get the size of the keyboard
//    //   CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    NSValue *value = userInfo[UIKeyboardAnimationDurationUserInfoKey];
//    NSTimeInterval duration = 0;
//    [value getValue:&duration];
//    float margin = 0;//self.tabBarHeight;//For avoid tab bar gap issue
//    // [self.view setFrame:viewFrame];
//    NSArray<UIViewController*> *controllers = self.tabBarController.viewControllers;
//    if (controllers.count != 0) {
//        margin = (float) self.tabBarController.tabBar.frame.size.height;
//    }
//    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//
//        self.containerBottomConstraint.constant = -[self.containerView attachmentsTableHeight] + margin;
//        // [self.view updateConstraints];
//        [self.view layoutIfNeeded];
//
//    }                completion:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    self.isKeyboardOpen = YES;
    
//    NSDictionary *userInfo = [notification userInfo];
//    
//    // get the size of the keyboard
//    CGSize keyboardSize = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//    
//    if (keyboardSize.height == 0)
//        return;
//    
//    NSValue *value = userInfo[UIKeyboardAnimationDurationUserInfoKey];
//    NSTimeInterval duration = 0;
//    [value getValue:&duration];
//    
//    float margin = 0;//self.tabBarHeight;//For avoid tab bar gap issue
//    NSArray<UIViewController*> *controllers = self.tabBarController.viewControllers;
//    if (controllers.count != 0) {
//        margin = (float) self.tabBarController.tabBar.frame.size.height;
//    }
    
//    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//
//        // self.containerBottomConstraint.constant = -(keyboardSize.height + margin + [self.containerView attachmentsTableHeight]);
//        //[self.view layoutIfNeeded];
//
//    }                completion:nil];
    
//    [self.tblView scrollToBottom:YES];
}

#pragma mark Notification Methods

- (void)getOfflineEventGroupChat:(NSNotification *)inNotify {
    [self reloadTableViewWithMessages:[inNotify object]];
}

#pragma mark - Get User List Callbacks

- (void)getUserlistSuccess:(NSNotification *)inNotify {
    usersList = [NSMutableArray arrayWithArray:[inNotify object]];
    self.containerView.usersList = usersList;
}


- (void)getUserlistFailed:(NSNotification *)inNotify {
   // NSLog(@"Get user list failed");
}
 
- (void)chatMessageDidSelectCamera {
    UIAlertController *customActionSheet = [UIAlertController alertControllerWithTitle:@"Select from Photos Library" message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *firstButton = [UIAlertAction actionWithTitle:@"Select Image" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //click action
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
        [imagePicker setModalPresentationStyle: UIModalPresentationFullScreen];
        [self presentViewController:imagePicker animated:YES completion: nil];
        
        
        
    }];
    [firstButton setValue:[UIColor blackColor] forKey:@"titleTextColor"];
    

    UIAlertAction *secondButton = [UIAlertAction actionWithTitle:@"Select Video" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //click action
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
        [imagePicker setModalPresentationStyle: UIModalPresentationFullScreen];
        [self presentViewController:imagePicker animated:YES completion: nil];
    }];
    [secondButton setValue:[UIColor blackColor] forKey:@"titleTextColor"];

    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //cancel
    }];
    [cancelButton setValue:[UIColor redColor] forKey:@"titleTextColor"];

    [customActionSheet addAction:firstButton];
    [customActionSheet addAction:secondButton];
    [customActionSheet addAction:cancelButton];

    [self presentViewController:customActionSheet animated:YES completion:nil];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
   // NSLog(@"Image picked");


    self.imageView.image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];

    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        NSData * data = UIImageJPEGRepresentation([info valueForKey:@"UIImagePickerControllerOriginalImage"], 0.5);

        NSString * path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/image.JPEG"];
        [data writeToFile:path atomically:true];
        
//        [self onSendAttachment:[UIImage imageWithData:data]];
        [self.containerView storeImageAttachment:[UIImage imageWithData:data ]];
        [self.containerView.attachmentsCollection reloadData];

    }
    else
    {
        
    }

    [self dismissViewControllerAnimated:true completion:nil];

}

@end

