//
//  SRImagePreviewViewController.h
//  Serendipity
//
//  Created by Mcuser on 1/29/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatMessageBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRVideoPreviewViewController : ParentViewController {
    AVPlayer *player;
    AVPlayerItem *item;
}

@property (strong, nonatomic) IBOutlet UIView *videoContainer;
@property UIImage *imageForPreview;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) id delegate;
@property BOOL isPreviewingChat;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void)loadVideoFromUrl:(NSString*)url;

@end

NS_ASSUME_NONNULL_END
