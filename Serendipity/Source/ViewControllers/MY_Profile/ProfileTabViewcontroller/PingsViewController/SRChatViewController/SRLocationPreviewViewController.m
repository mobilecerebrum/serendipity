//
//  SRLocationPreviewViewController.m
//  Serendipity
//
//  Created by Mcuser on 2/5/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRLocationPreviewViewController.h"
#import "PinAnnotation.h"
#import "MapKit/MapKit.h"
#import "SRMapProfileView.h"
#import "UIImageView+WebCache.h"
#import "SRModalClass.h"

@interface SRLocationPreviewViewController () {
    PinAnnotation *createdPin;
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation SRLocationPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //location-white-outline.png
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.usersList) {
        [self drawUsers];
    }
    self.mapView.showsUserLocation = YES;
    
    if (self.viewCoordinates.latitude != 0.0 && self.viewCoordinates.longitude != 0.0) {
        PinAnnotation *viewPin = [[PinAnnotation alloc] init];
        viewPin.coordinate = self.viewCoordinates;
        [self.mapView addAnnotation:viewPin];
    } else {
        [self initMapGesture];
    }
}

-(void)initMapGesture {
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleMapLongPress:)];
    lpgr.minimumPressDuration = 1.0; //user needs to press for 2 seconds
    [self.mapView addGestureRecognizer:lpgr];
}

- (void)handleMapLongPress:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    
    if (!createdPin) {
        createdPin = [[PinAnnotation alloc] init];
        self.sendButton.enabled = YES;
        [self.sendButton setTitleColor:[UIColor orangeColor] forState:(UIControlStateNormal)];
    }
    createdPin.coordinate = touchMapCoordinate;
    [self.mapView addAnnotation:createdPin];
}

#pragma mark - Custom events

-(void)drawUsers {
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    for (int i = 0; i < self.usersList.count; i++) {
        NSDictionary *userDic = [NSDictionary dictionaryWithDictionary:[self.usersList objectAtIndex:i]];
        userDic = [SRModalClass removeNullValuesFromDict:userDic];
    
        if ([[userDic valueForKey:kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            PinAnnotation *pinAnnotation = [[PinAnnotation alloc] init];
            CLLocationCoordinate2D coordinate;
    
            float lat = [[NSString stringWithFormat:@"%@",[userDic valueForKeyPath:@"location.lat"]] floatValue];//server.myLocation.coordinate.latitude;
            float lon = [[NSString stringWithFormat:@"%@",[userDic valueForKeyPath:@"location.lon"]] floatValue];//server.myLocation.coordinate.longitude;
            coordinate.latitude  = lat;
            coordinate.longitude = lon;
            pinAnnotation.annId= [NSString stringWithFormat:@"%@",[userDic valueForKey:kKeyId]];
    
            pinAnnotation.coordinate = coordinate;
            [self.mapView addAnnotation:pinAnnotation];
        }
    }
    [self.mapView showAnnotations:[self.mapView annotations] animated:YES];
}

#pragma mark - UI Actions

- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onSend:(id)sender {
    if (self.delegate) {
        if (createdPin) {
            [self.delegate onSendLocationLon:(float)createdPin.coordinate.longitude lat:(float)createdPin.coordinate.latitude];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark MapView Delegates

//-------------------------------------------------------------------------------------------------------------
// Check Zoom Level:

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    //    MKZoomScale currentZoomScale = self.mapViewNearBy.bounds.size.width / self.mapViewNearBy.visibleMapRect.size.width;
}

//-----------------------------------------------------------------------------------------------------------------------
// viewForAnnotation:

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation> )annotation {
    MKAnnotationView *annotationView;
    
    // check annotation is not user location
    if ([annotation isEqual:[mapView userLocation]]) {
        // Return for user pin
        return nil;
    }
    if ([annotation isKindOfClass:[PinAnnotation class]]) {
        // Pin annotation.
        annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"Pin"];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Pin"];
        }
        else {
            annotationView.annotation = annotation;
        }
        
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        SRMapProfileView *userProfileView = [nibArray objectAtIndex:1];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [(PinAnnotation *)annotation annId]];
        NSArray *filterArr = [self.usersList filteredArrayUsingPredicate:predicate];
        
        if ([filterArr count] > 0)
        {
            NSDictionary *userInfo =[NSDictionary dictionaryWithDictionary:[filterArr objectAtIndex:0]];
            
            if ([[userInfo objectForKey:kKeyProfileImage]isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *profileDict = [userInfo objectForKey:kKeyProfileImage];
                
                NSString *imageName = [profileDict objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                    [userProfileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                    }];
                    
                }
            }
        }
        // Provide round rect
        userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y - 2, 37, 37);
        
        userProfileView.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
        userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2.0;
        userProfileView.userProfileImg.clipsToBounds = YES;
        userProfileView.userProfileImg.layer.masksToBounds = YES;
        
        // Add to view
        UIImage *img = [SRModalClass imageFromUIView:userProfileView];
        annotationView.image = img;
        
    }
    // No callout
    [annotationView setCanShowCallout:NO];
    [annotationView setEnabled:YES];
    
    return annotationView;
}

//-----------------------------------------------------------------------------------------------------------------
// didSelectAnnotationView:

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
}

//-----------------------------------------------------------------------------------------------------------------
// didDeselectAnnotationView:

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    //Hide ProfilesScrollview if unhide
    
}

//-----------------------------------------------------------------------------------------------------------------
// didUpdateUserLocation:

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // Zoom to region containing the user location
}

-(void)showLocationLontitude:(float)lon latitude:(float)lat {
    
}

@end
