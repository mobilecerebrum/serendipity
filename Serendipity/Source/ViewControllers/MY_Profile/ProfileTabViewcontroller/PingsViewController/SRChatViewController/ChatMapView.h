//
//  ChatMapView.h
//  Serendipity
//
//  Created by Mcuser on 2/15/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PinAnnotation.h"
#import "MapKit/MapKit.h"
#import "SRMapProfileView.h"
#import "UIImageView+WebCache.h"
#import "SRModalClass.h"
#import "MKMapView+ZoomLevel.h"


NS_ASSUME_NONNULL_BEGIN

@interface ChatMapView : UIView

@property NSMutableArray *usersList;
@property NSDictionary *otherChatUserLocation;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

-(void)initialize;
-(void)focusOnMyLocation;
-(void)focusOnOtherLocation;
-(void)setOtherChatUserLocation:(NSDictionary*)location andImageUrl:(NSString*)url;

@end

NS_ASSUME_NONNULL_END
