#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SRModalClass.h"
#import "SRPingsViewCell.h"
#import "SRAchivementsViewController.h"
#import "SRProfileTabViewController.h"
#import "SRContactsViewController.h"
#import "SRProfileImageView.h"
#import "HSProfileImageView.h"

@interface SRPingsViewController : ParentViewController
{
	// Instance variable
	NSArray *pingsListArr;
	NSArray *sourceArr;
    NSArray *searchedChatData;
    BOOL isSearching;
    NSMutableArray *cacheImgArr;
    UIActivityIndicatorView *_activityIndicator;
	// Server
	SRServerConnection *server;
    UIButton *rightButton;
    UITextField *searchTextField;
}

// Properties
@property (strong, nonatomic) IBOutlet HSProfileImageView *profileImg;
@property (strong, nonatomic) IBOutlet UITableView *objtableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
//Other Properties
@property (weak) id delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblNoData;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
