#import "SRChatViewController.h"
#import "SRMyConnectionsViewController.h"
#import "SRPingsViewController.h"
#import "DBManager.h"
#import "SRChatMessage.h"

@implementation SRPingsViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SRPingsViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        cacheImgArr = [[NSMutableArray alloc] init];
        searchedChatData = [[NSArray alloc] init];
    }
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(refreshList:)
                          name:kKeyNotificationRefreshList
                        object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                      selector:@selector(getChatData)
                          name:UIApplicationDidBecomeActiveNotification
                        object:nil];

    //return
    return self;
}

- (void)refreshList:(NSNotification *)notification {
    self.lblNoData.hidden = true;
    self.objtableView.hidden = false;
    [self.view bringSubviewToFront:self.objtableView];
    NSMutableArray *array = [[DBManager getSharedInstance] getRecentList];

    sourceArr = array;
    pingsListArr = sourceArr;

    // Sort List By name
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyTime
                                                                   ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSArray *sortedArray = [pingsListArr sortedArrayUsingDescriptors:sortDescriptors];
    pingsListArr = sortedArray;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.objtableView reloadData];
    });

}

- (void)dealloc {
    // Dealloc all register notifications
    if (!(APP_DELEGATE).isFromNotification)
        (APP_DELEGATE).isOnChatList = false;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];
//    if (@available(iOS 13.0, *)) {
//        self.searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
//        self.searchBar.searchTextField.textColor = [UIColor grayColor];
//    }
    if (@available(iOS 13.0, *)) {
        self->searchTextField = self.searchBar.searchTextField;
        self->searchTextField.backgroundColor = [UIColor whiteColor];
        self->searchTextField.textColor = [UIColor grayColor];
    } else {
        self->searchTextField = [_searchBar valueForKey:@"searchField"];
        self->searchTextField.backgroundColor = [UIColor whiteColor];
        self->searchTextField.textColor = [UIColor grayColor];
    }
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    (APP_DELEGATE).isOnChatList = TRUE;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.Pings.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    //Empty Data Label
    self.lblNoData.hidden = true;
    self.lblNoData.textAlignment = NSTextAlignmentCenter;
    self.lblNoData.textColor = [UIColor orangeColor];
    [self.lblNoData setFont:[UIFont fontWithName:kFontHelveticaMedium size:16]];
    self.lblNoData.backgroundColor = self.searchBar.backgroundColor;
    self.lblNoData.numberOfLines = 5;
    if ((APP_DELEGATE).isFromNotification) {
//        (APP_DELEGATE).topBarView.hidden = YES;
        (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
        [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
        (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //NavigationBar button
//    self.tabBarController.navigationItem.rightBarButtonItem = nil;

    UIImage *image3 = [UIImage imageNamed:@"plus-orange.png"];
    CGRect frameimg = CGRectMake(18, 5, 18, 18);

    UIButton *rightButton1 = [[UIButton alloc] initWithFrame:frameimg];
    [rightButton1 setBackgroundImage:image3 forState:UIControlStateNormal];
    [rightButton1 addTarget:self action:@selector(editBtnAction:)
           forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *rightBarbutton = [[UIBarButtonItem alloc] initWithCustomView:rightButton1];
    self.tabBarController.navigationItem.rightBarButtonItem = rightBarbutton;
    
    // Get Users list for the display
    self.lblNoData.hidden = true;
    [self getChatData];
}

- (void) getChatData{
    [[SRAPIManager sharedInstance] getChatsHistoryWithCompletion:^(id  _Nonnull result, NSError * _Nonnull error) {
        if (!error) {
            [self updateChatHistory:result];
            self.lblNoData.hidden = YES;
        } else {
            self.lblNoData.hidden = NO;
            self.lblNoData.text = NSLocalizedString(@"lbl.noChatHistory.txt", @"");
            [self.objtableView reloadData];
        }
    }];
}

-(void)updateChatHistory:(NSMutableArray*)array {
   // NSMutableArray *array = [[DBManager getSharedInstance] getRecentList];
    
    sourceArr = array;
    pingsListArr = sourceArr;
    
    // Sort List By name
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyTime
                                                                   ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSArray *sortedArray = [pingsListArr sortedArrayUsingDescriptors:sortDescriptors];
    pingsListArr = sortedArray;
    
    if (pingsListArr.count <= 0) {
        addTextInSignificantTxt(@"label set");
        self.lblNoData.hidden = false;
        self.objtableView.hidden = true;
        self.lblNoData.text = NSLocalizedString(@"lbl.noChatHistory.txt", @"");
    } else {
        self.lblNoData.hidden = true;
        self.objtableView.hidden = false;
        [self.objtableView reloadData];
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    if ((APP_DELEGATE).isFromNotification) {
        (APP_DELEGATE).isOnChatList = false;
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
        (APP_DELEGATE).isFromNotification = NO;
        (APP_DELEGATE).isNotificationFromClosed = FALSE;
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshList
                                                            object:nil];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

// ----------------------------------------------------------------------------------------------------------
// plusBtnAction:

- (void)plusBtnAction:(id)sender {
    SRMyConnectionsViewController *connectionCon = [[SRMyConnectionsViewController alloc] initWithNibName:nil bundle:nil server:server];
    connectionCon.delegate = self;
    [self.navigationController pushViewController:connectionCon animated:YES];
}

// ----------------------------------------------------------------------------------------------------------
// editBtnAction:

- (void)editBtnAction:(id)sender {
    SRMyConnectionsViewController *objSREditProfileViewController = [[SRMyConnectionsViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:objSREditProfileViewController animated:YES];
    self.hidesBottomBarWhenPushed = YES;
}


#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (pingsListArr.count == 0) {
//        self.lblNoData.frame = tableView.frame;
//        tableView.backgroundView=self.lblNoData;
//        return 0;

    } else {
        if (isSearching) {
//            if (searchedChatData.count==0) {
//                self.lblNoData.frame = tableView.frame;
//                tableView.backgroundView=self.lblNoData;
//                return 0;
//            }
//            else
//            {
//                tableView.backgroundView=nil;
            return searchedChatData.count;
//            }
        } else {
//            addTextInSignificantTxt(@"tableview load");
//            self.lblNoData.hidden=YES;
//            tableView.backgroundView=nil;
//            tableView.backgroundColor = [UIColor clearColor];
            return pingsListArr.count;
        }
    }
    return [pingsListArr count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRPingsViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRPingsViewCell *customCell = (SRPingsViewCell *) nibObjects[0];
            cell = customCell;
        }
    }

    ((SRPingsViewCell *) cell).MenuItemCountLabel.layer.cornerRadius = 8.0;
    ((SRPingsViewCell *) cell).MenuItemCountLabel.layer.masksToBounds = YES;
    ((SRPingsViewCell *) cell).MenuItemCountLabel.clipsToBounds = YES;
    
    // Configure the cell...
    NSDictionary *userDict;
    NSDictionary *mesageDict;
    if (isSearching) {
        userDict = (searchedChatData && indexPath.section >= 0 && indexPath.section < searchedChatData.count) ? searchedChatData[indexPath.section][@"user"] : nil;
        mesageDict = (searchedChatData && indexPath.section >= 0 && indexPath.section < searchedChatData.count) ? searchedChatData[indexPath.section][@"message"] : nil;
    } else {
        // Details
        userDict = (pingsListArr && indexPath.section >= 0 && indexPath.section < pingsListArr.count) ? pingsListArr[indexPath.section][@"user"] : nil;
         mesageDict = (pingsListArr && indexPath.section >= 0 && indexPath.section < pingsListArr.count) ? pingsListArr[indexPath.section][@"message"] : nil;
    }
//    NSDictionary *dict = [userDict objectForKey:kKeyProfileImage];
//    if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
//        NSDictionary *profileDict = userDict[kKeyProfileImage];
//        if (profileDict[kKeyImageName] != nil) {
//            NSString *imageName = profileDict[kKeyImageName];
//            if ([imageName length] > 0) {
//                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
//                [((SRPingsViewCell *) cell).imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
//            } else {
//                [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
//            }
//        } else {
//             [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
//        }
//    } else {
//         [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
//    }
    
    
    ///// USER PROFILE
    
    
    if ([userDict[kKeySetting][@"profile_photo"]intValue] == 5 ) {
        [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
    } else if ([userDict[kKeySetting][@"profile_photo"]intValue] == 4 ) {
        [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
        if ([userDict[kKeyIsFavourite]intValue] == 1 ) {
            
            NSString* strProfileURL = [self getProfileImageURL:userDict];
            if ([strProfileURL length] > 0){
                [((SRPingsViewCell *) cell).imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
            }else{
                [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
            }
        }
    } else {
        NSString* strProfileURL = [self getProfileImageURL:userDict];
        if ([strProfileURL length] > 0){
            [((SRPingsViewCell *) cell).imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
        }else{
            [((SRPingsViewCell *) cell).imgProfilePhoto setImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
        }
    }
    
    ////////

    // Name of user
    NSString *name = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];
    ((SRPingsViewCell *) cell).lblProfileName.text = name;
    
    if ([self countOfNotifications:userDict[kKeyId]] == 0) {
        ((SRPingsViewCell *) cell).MenuItemCountLabel.hidden = YES;
    } else {
        ((SRPingsViewCell *) cell).MenuItemCountLabel.hidden = NO;
        ((SRPingsViewCell *) cell).MenuItemCountLabel.text = [NSString stringWithFormat:@"%d", [self countOfNotifications:userDict[kKeyId]]];
    }
    
    ((SRPingsViewCell *) cell).notificationIds = [self idsOfNotifications:userDict[kKeyId]];
    
    if (mesageDict) {
        SRChatMessage *latestChatMessage = [[SRChatMessage alloc] initWithDictionary:mesageDict];
        if ([latestChatMessage isText]) {
            ((SRPingsViewCell *) cell).lblProfileDetails.text = [latestChatMessage messageData];
        } else if ([latestChatMessage isImage]) {
            ((SRPingsViewCell *) cell).lblProfileDetails.text = @"Photo";
            ((SRPingsViewCell *) cell).imgCamera.image = [UIImage imageNamed:@"photochat.png"];
        } else if ([latestChatMessage isVideo]) {
            ((SRPingsViewCell *) cell).lblProfileDetails.text = @"Video";
            ((SRPingsViewCell *) cell).imgCamera.image = [UIImage imageNamed:@"videochat.png"];
        }
        NSString *datestr = [NSDate makeChatTimeStamp:[NSString stringWithFormat:@"%lld", [latestChatMessage created_at]]];
            NSString *timeStamp = [NSDate getChatTimeStamp:datestr];
        ((SRPingsViewCell *) cell).lblTimeLabel.hidden = YES;
        ((SRPingsViewCell *) cell).lblTimeLabel.text = [NSDate dateTimeAgo:[NSString stringWithFormat:@"%lld", [latestChatMessage created_at]]];
    }
    
    if ([userDict[@"status"] intValue] == 1) {
        ((SRPingsViewCell *) cell).lblActiveProfile.hidden = NO;
    }else{
        ((SRPingsViewCell *) cell).lblActiveProfile.hidden = YES;
    }
//
//    if (([userDict[@"message"] rangeOfString:@"SDKCometChatMediaFiles"].location != NSNotFound) || ([userDict[@"message"] rangeOfString:@"unencryptedfilename"].location != NSNotFound)) {
//        NSURL *url = [NSURL URLWithString:userDict[@"message"]];
//        ((SRPingsViewCell *) cell).imgCamera.hidden = NO;
//        NSString *strLastComponent;
//        if ([userDict[@"message"] rangeOfString:@"unencryptedfilename"].location != NSNotFound) {
//            NSArray *arrFileData = [userDict[@"message"] componentsSeparatedByString:@"unencryptedfilename"];
//            strLastComponent = arrFileData[1];
//            NSLog(@"%@", arrFileData[1]);
//        } else
//            strLastComponent = [url lastPathComponent];
//        NSArray *arrFileData = [strLastComponent componentsSeparatedByString:@"."];
//        if ([arrFileData[1] isEqualToString:@"mp4"]) {
//            ((SRPingsViewCell *) cell).lblProfileDetails.text = @"Video";
//            ((SRPingsViewCell *) cell).imgCamera.image = [UIImage imageNamed:@"videochat.png"];
//        } else {
//            ((SRPingsViewCell *) cell).lblProfileDetails.text = @"Photo";
//            ((SRPingsViewCell *) cell).imgCamera.image = [UIImage imageNamed:@"photochat.png"];
//        }
//    } else {
//        ((SRPingsViewCell *) cell).lblProfileDetails.text = userDict[@"message"];
//    }
    
  
//    if ([userDict[@"notificationCount"] intValue] > 0) {
//        ((SRPingsViewCell *) cell).MenuItemCountLabel.hidden = NO;
//       // ((SRPingsViewCell *) cell).MenuItemCountLabel.text = userDict[@"notificationCount"];
//    } else {
//        ((SRPingsViewCell *) cell).MenuItemCountLabel.hidden = YES;
//    }
    // Time

//    NSLog(@"%@", );
//
    // Selection style
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    // return
    return cell;
}


-(NSString*) getProfileImageURL:(NSDictionary*) userDict{
    
    if  ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary self]])  {
        NSDictionary *profileDict = userDict[kKeyProfileImage];
        if (profileDict[kKeyImageName] != nil){
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                
                return imageUrl;
            }
        }
    }
    
    return @"";
}

// ---------------------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }
    return height;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }

    // Return
    return headerView;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *userDict;
    if (isSearching) {
        isSearching = NO;
        self.searchBar.text = nil;
        [self.searchBar resignFirstResponder];
        userDict = searchedChatData[indexPath.section][@"user"];
    } else {
        userDict = pingsListArr[indexPath.section][@"user"];
    }
    
    NSMutableArray *nIds = ((SRPingsViewCell*)[tableView cellForRowAtIndexPath:indexPath]).notificationIds;
    
    if (nIds != nil) {
        for (NSString *nId in nIds) {
            NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, nId];
            NSDictionary *param = @{kKeyStatus: @1};
            [(APP_DELEGATE).server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
        }
    }
    
    if (userDict.count > 0) {
        SRChatViewController *objChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:server inObjectInfo:userDict];
        objChatView.delegate = self;
        [(APP_DELEGATE) callNotificationsApi];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.objtableView reloadData];
        });
        [self.navigationController pushViewController:objChatView animated:YES];

    }
}

#pragma mark
#pragma mark SearchDisplayController Delegates Methods
#pragma mark

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (pingsListArr.count <= 0) {
        [SRModalClass showAlert:@"Sorry, there are no results that match your search."];
        return NO;
    }

    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
    isSearching = NO;
    searchedChatData = nil;
    if (searchedChatData.count <= 0) {
        self.objtableView.hidden = true;
        self.lblNoData.text = NSLocalizedString(@"lbl.nosearchfound.txt", @"");
        self.lblNoData.hidden = false;
    } else {
        self.objtableView.hidden = false;
        self.lblNoData.hidden = true;
        [self.objtableView reloadData];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

    //Remove all objects first.
//    searchedChatData = nil;

    if ([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    } else {
        isSearching = NO;

    }
    if (searchedChatData.count <= 0) {
        self.objtableView.hidden = true;
        self.lblNoData.hidden = false;
        self.lblNoData.text = NSLocalizedString(@"lbl.nosearchfound.txt", @"");
    } else {
        self.objtableView.hidden = false;
        self.lblNoData.hidden = true;
        [self.objtableView reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    isSearching = NO;
    self.searchBar.text = nil;
    searchedChatData = [NSArray new];
    [self.objtableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self searchTableList];
  
}

- (void)searchTableList {
    NSString *searchString = self.searchBar.text;
    NSPredicate *resultPredicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        NSDictionary *user = evaluatedObject[@"user"];
        if (user) {
            if ([user[kKeyLastName] rangeOfString:self.searchBar.text].location != NSNotFound || [user[kKeyFirstName] rangeOfString:self.searchBar.text].location != NSNotFound) {
                return YES;
            }
        }
        return NO;
    }];
    /*[NSPredicate predicateWithFormat:@"(first_name CONTAINS[cd] %@) OR (name CONTAINS[cd] %@)", searchString, searchString];*/
    searchedChatData = [pingsListArr filteredArrayUsingPredicate:resultPredicate];
}

-(int)countOfNotifications:(NSString*)userId {
    int result = 0;
    for (NSDictionary *notification in server.notificationArr) {
        if ([[notification valueForKey:kKeySenderId]isEqualToString:userId] && [notification[@"reference_type_id"]  isEqual: @"13"]) {
            result++;
        }
    }
    return result;
}

-(NSMutableArray*)idsOfNotifications:(NSString*)userId {
    NSMutableArray* result = [NSMutableArray new];
    for (NSDictionary *notification in server.notificationArr) {
        if ([[notification valueForKey:kKeySenderId] isEqualToString:userId]) {
            [result addObject:notification[@"id"]];
        }
    }
    return result;
}

- (void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}

@end
