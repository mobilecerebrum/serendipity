#import <UIKit/UIKit.h>

@interface SRPingsViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePhoto,*imgCamera;
@property (weak, nonatomic) IBOutlet UILabel *lblActiveProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeLabel;
@property(strong,nonatomic)IBOutlet UILabel *MenuItemCountLabel;
@property (strong, nonatomic) NSMutableArray *notificationIds;

@end
