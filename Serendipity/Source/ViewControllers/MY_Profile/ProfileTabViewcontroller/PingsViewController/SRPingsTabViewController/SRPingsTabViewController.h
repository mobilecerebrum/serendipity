#import <UIKit/UIKit.h>
#import "SRModalClass.h"
#import "SRPingsViewController.h"
#import "SRContactsViewController.h"

@interface SRPingsTabViewController : UITabBarController <UITabBarControllerDelegate>
{
	// Instance variables
    
    //Server
    SRServerConnection *server;
}

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;


@end
