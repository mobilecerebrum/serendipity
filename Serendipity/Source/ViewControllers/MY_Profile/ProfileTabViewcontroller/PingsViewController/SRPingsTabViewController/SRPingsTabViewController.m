#import "SRPingsTabViewController.h"

@implementation SRPingsTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//-----------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRPingsTabViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        server = inServer;
    }

    // Return
    return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//-----------------------------------------------------------------------------------------------------------
// Standard Overrides

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];

    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;

    [[UITabBarItem appearance]                      setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f],
            NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance]              setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f],
            NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];

    // Set Profile tab
    SRPingsViewController *objSRPingsView = [[SRPingsViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-ping.png"];
    UITabBarItem *objSRPingsViewTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabbarItem.title.Pings.view", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-ping-active.png"]];
    [objSRPingsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRPingsView.tabBarItem = objSRPingsViewTabItem;

    //  Set Contact View tab
    SRContactsViewController *objSRContactsView = [[SRContactsViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-contact-active.png"];
    UITabBarItem *objSRContactsViewTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabbarItem.title.Contacts.view", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-contact.png"]];
    [objSRContactsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRContactsView.tabBarItem = objSRContactsViewTabItem;

    self.viewControllers = @[objSRPingsView, objSRContactsView];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.Pings.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    [self setDelegate:self];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [SRModalClass hideTabBar:self];
    [self.navigationController popViewControllerAnimated:NO];
}

// ----------------------------------------------------------------------------------------------------------
// editBtnAction:

- (void)createMsgBtnAction:(id)sender {
}

#pragma mark
#pragma mark TabBarDelegate Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// didSelectViewController:

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if (tabBarController.selectedIndex == 0) {
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.Pings.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    } else if (tabBarController.selectedIndex == 1) {
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.MyContacts.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }
}

@end
