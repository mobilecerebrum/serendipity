
#import "SRContactsViewController.h"
@implementation SRContactsViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRContactsViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
    }
    
    // Return
    return self;
}

#pragma mark
#pragma mark Statndard Method
#pragma mark

// ---------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];
    
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.MyContacts.view", "") forViewNavCon:self.navigationItem forFontSize:15];
    
    //NavigationBar button
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    //TODO: Dummy Data
    self.friendListSection = [NSArray arrayWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",nil];
    
    //call method to get contact from address book
        [self btnGetAllContacts];
    
   //tableview section index background
    if ([self.friendsTable respondsToSelector:@selector(setSectionIndexColor:)]) {
        self.friendsTable.sectionIndexColor = [UIColor whiteColor];
        self.friendsTable.sectionIndexBackgroundColor = [UIColor blackColor];
    }
    
    
    //code to set inside backgroundcolor of searchbar
    for (UIView* subview in [[self.searchBar.subviews lastObject] subviews]) {
        if ([subview isKindOfClass:[UITextField class]])
        {
            UITextField *textField = (UITextField*)subview;
            [textField setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
            textField.textColor = [UIColor whiteColor];
        }
    }
    
}

#pragma mark
#pragma mark Action Method
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

//-----------------------------------------------------------------------------------------
// btnGetAllContacts:

- (void)btnGetAllContacts {
    
    //ABAddressBookRef m_addressbook = ABAddressBookCreate();
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    // ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) {
        // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }
    
    else {
        // We are on iOS 5 or Older
        accessGranted = YES;
        [self getContactsWithAddressBook:addressBook];
    }
    if (accessGranted) {
        [self getContactsWithAddressBook:addressBook];
    }
    
}

//-----------------------------------------------------------------------------------------------------------------------
// getContactsWithAddressBook:

- (void)getContactsWithAddressBook:(ABAddressBookRef )addressBook {
    
    contactList = [[NSMutableArray alloc] init];
    contactNameList = [[NSMutableArray alloc] init];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i=0;i < nPeople;i++) {
        NSMutableDictionary *dOfPerson=[NSMutableDictionary dictionary];
        
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople,i);
        
        //For username and surname
        ABMultiValueRef phones =(__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(ref, kABPersonPhoneProperty));
        
        CFStringRef firstName, lastName;
        firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        lastName  = ABRecordCopyValue(ref, kABPersonLastNameProperty);
        [dOfPerson setObject:[NSString stringWithFormat:@"%@ %@", firstName, lastName] forKey:@"name"];
        
        
        NSString *fullNameStr = [[[NSString stringWithFormat:@"%@",ABRecordCopyValue(ref, kABPersonFirstNameProperty)] stringByAppendingString:@" "] stringByAppendingString:[NSString stringWithFormat:@"%@",ABRecordCopyValue(ref, kABPersonLastNameProperty)]];
        [contactNameList addObject:fullNameStr];
        
        
        //For Email ids
        ABMutableMultiValueRef eMail  = ABRecordCopyValue(ref, kABPersonEmailProperty);
        if(ABMultiValueGetCount(eMail) > 0) {
            [dOfPerson setObject:(__bridge NSString *)ABMultiValueCopyValueAtIndex(eMail, 0) forKey:@"email"];
            
        }
        
        //For Phone number List
        NSString* mobileLabel;
        
        for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
            mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
            if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
            }
            else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
            {
                [dOfPerson setObject:(__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i) forKey:@"Phone"];
                break ;
            }
        }
        [contactList addObject:dOfPerson];
       // NSLog(@"Contact List:%@",contactList);
    }
}

#pragma mark
#pragma mark Section Overview Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// sectionForSectionIndexTitle:

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.friendListSection indexOfObject:title];
}

// ---------------------------------------------------------------------------------------
// titleForHeaderInSection:

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.friendListSection sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]objectAtIndex:section];
}

// ---------------------------------------------------------------------------------------
// sectionIndexTitlesForTableView:

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return  self.friendListSection;
}

#pragma mark
#pragma mark TableView Delegates
#pragma mark

// ---------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.friendListSection count];
}

// ---------------------------------------------------------------------------------------
// numberOfRowsInSection

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString* beginsWithRequirement =[self.friendListSection objectAtIndex:section];;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] %@", beginsWithRequirement];
        subArray = [searchData filteredArrayUsingPredicate:predicate];
        return [subArray count];
        
    } else {
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] %@", beginsWithRequirement];
        subArray = [contactNameList filteredArrayUsingPredicate:predicate];
        return [subArray count];
    }
}

// ---------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSString* beginsWithRequirement =[self.friendListSection objectAtIndex:indexPath.section];;
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[cd] %@", beginsWithRequirement];
    subArray = [contactNameList filteredArrayUsingPredicate:predicate];
    cell.textLabel.textColor = [UIColor orangeColor];
    cell.backgroundColor = [UIColor clearColor];
    UIFont *myFont = [ UIFont fontWithName: kFontHelveticaMedium size: 14.0 ];
    cell.textLabel.font  = myFont;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        cell.textLabel.text = [searchData objectAtIndex:indexPath.row];
    } else {
        cell.textLabel.text = [subArray objectAtIndex:indexPath.row];
    }
    return  cell;
}


#pragma mark
#pragma mark TableView Datasource
#pragma mark

// ---------------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

// ---------------------------------------------------------------------------------------
//viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(20, 0, 300, 20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
   // view.backgroundColor = [UIColor lightGrayColor];
    view.backgroundColor = [UIColor colorWithRed:170.0 green:170.0 blue:170.0 alpha:0.6];
    [view addSubview:label];
    
    return view;
}


#pragma mark
#pragma mark SearchDisplayController Delegates Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// filterContentForSearchText:
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [ self btnGetAllContacts];
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    searchData = [contactNameList filteredArrayUsingPredicate:resultPredicate];
    
}
// ---------------------------------------------------------------------------------------
// shouldReloadTableForSearchString:
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}


@end
