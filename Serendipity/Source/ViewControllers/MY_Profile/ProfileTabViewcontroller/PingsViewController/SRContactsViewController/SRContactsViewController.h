
#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SRModalClass.h"
#import "SRProfileImageView.h"

@interface SRContactsViewController : UIViewController<ABPeoplePickerNavigationControllerDelegate>
{
    //Server
    SRServerConnection *server;
    
    //Instance Variables
    NSMutableArray *menuArray;
    NSArray *searchData;
    NSMutableArray *contactList;
    NSMutableArray *contactNameList;
    NSArray* subArray;
}

//@property AddFriendCellView *pop;
@property (weak, nonatomic) IBOutlet SRProfileImageView *impProfileImage;
@property (weak, nonatomic) IBOutlet UITableView *friendsTable;
@property (strong,nonatomic) IBOutlet UISearchBar *searchBar;

@property (retain, nonatomic) NSArray *friendListSection;
@property  CFArrayRef contactAdd;
@property  CFMutableArrayRef  filteredData;
@property ABRecordRef persons;
@property ABAddressBookRef addressBook;
@property (retain,nonatomic) ABPersonViewController *currentPersonViews;


//Other Property
@property(weak)id delegate;


#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

@end
