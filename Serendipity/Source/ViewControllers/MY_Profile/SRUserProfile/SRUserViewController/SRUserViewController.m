#import "SRModalClass.h"
#import "SRUserViewController.h"

@implementation SRUserViewController



#pragma mark
#pragma mark Private Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// setScrollContentSize

- (void)setScrollContentSize {
    //height for Profile Address Details
    CGRect frame = self.addressLbl.frame;
    CGRect textRect = [self.addressLbl.text boundingRectWithSize:CGSizeMake(self.addressLbl.frame.size.width, CGFLOAT_MAX)
                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{ NSFontAttributeName:self.addressLbl.font }
                                                                context:nil];
    
    frame.size.height = textRect.size.height;
    self.addressLbl.frame = frame;
    
    
    frame = self.descriptionLbl.frame;
    frame.origin.y = self.addressLbl.frame.origin.y + self.addressLbl.frame.size.height + 5;
    self.descriptionLbl.frame = frame;
    
    //height for Profile Description Details
    
    textRect = [self.descriptionLbl.text boundingRectWithSize:CGSizeMake(self.descriptionLbl.frame.size.width, CGFLOAT_MAX)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{ NSFontAttributeName:self.descriptionLbl.font }
                                                      context:nil];
    
    frame.size.height = textRect.size.height;
    self.descriptionLbl.frame = frame;
    self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x,
                                        self.descriptionLbl.frame.origin.y + self.descriptionLbl.frame.size.height,
                                        self.btnReadMore.frame.size.width,
                                        self.btnReadMore.frame.size.height);
    frame = self.datingView.frame;
    frame.origin.y = self.descriptionLbl.frame.origin.y + self.descriptionLbl.frame.size.height + 8;
    self.datingView.frame = frame;
    
    frame = self.familyView.frame;
    frame.origin.y = self.datingView.frame.origin.y + self.datingView.frame.size.height + 8;
    self.familyView.frame = frame;
    
    frame = self.connectionView.frame;
    frame.origin.y = self.familyView.frame.origin.y + self.familyView.frame.size.height + 8;
    self.connectionView.frame = frame;
    
    frame = self.objInterestsView.frame;
    frame.origin.y = self.connectionView.frame.origin.y + self.connectionView.frame.size.height + 8;
    self.objInterestsView.frame = frame;
    
    self.profileDetailScrollview.contentSize = CGSizeMake(0,  self.objInterestsView.frame.origin.y + self.objInterestsView.frame.size.height + 50);
    self.profileDetailScrollview.scrollEnabled = YES;
    
   
}

//---------------------------------------------------------------------------------------------------------------------------------
// displayDetails

- (void)displayDetails {
    NSDictionary *dict = server.loggedInUserInfo;
    
    self.profileNameLbl.text = [NSString stringWithFormat:@"%@ %@", [dict valueForKey:kKeyFirstName], [dict valueForKey:kKeyLastName]];
    
    // Display the values
    NSString *lblStr = @"";
    if ([[dict objectForKey:kKeyDOB]length] > 0) {
        NSString *birthDate = [dict objectForKey:kKeyDOB];
        NSDate *todayDate = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dobDate = [dateFormatter dateFromString:birthDate];
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        birthDate = [dateFormatter stringFromDate:dobDate];
        dobDate = [dateFormatter dateFromString:birthDate];
        
        int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:birthDate]];
        int allDays = (((time / 60) / 60) / 24);
        int days = allDays % 365;
        int years = (allDays - days) / 365;
        
        lblStr = [NSString stringWithFormat:@"%d", years];
    }
    if ([[dict objectForKey:kKeyOccupation]length] > 0) {
        if ([lblStr length] > 0) {
            lblStr = [NSString stringWithFormat:@"%@, %@", lblStr, [dict objectForKey:kKeyOccupation]];
        }
        else {
            lblStr = [dict objectForKey:kKeyOccupation];
        }
    }
    
    BOOL isMutatedStrSet = NO;
    if ([lblStr length] > 0 && [[dict objectForKey:kKeyAddress]length] > 0) {
        NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc]initWithString:lblStr];
        NSString *str = [NSString stringWithFormat:@" in %@", [dict objectForKey:kKeyAddress]];
        NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:str attributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaItalicMedium size:12] }];
        [muAtrStr appendAttributedString:atrStr];
        [self.addressLbl setAttributedText:muAtrStr];
        isMutatedStrSet = YES;
    }
    else if ([[dict objectForKey:kKeyAddress]length] > 0) {
        lblStr = [dict objectForKey:kKeyAddress];
    }
    
    // Check whether lbl string has some thing to display
    if ([lblStr length] > 0 && !isMutatedStrSet) {
        self.addressLbl.text = lblStr;
    }
    else if ([lblStr length] == 0) {
        self.addressLbl.text = @"---";
    }
    
    // Display Description
    if (![[dict objectForKey:kKeyDescription] isEqual:[NSNull null]]) {
        if ([[dict objectForKey:kKeyDescription]length] > 0) {
        if ([[dict objectForKey:kKeyDescription]length] > 135) {
            NSString *str = [dict objectForKey:kKeyDescription];
            NSString *subStr = [str substringWithRange:NSMakeRange(0, 135)];
            subStr = [NSString stringWithFormat:@"%@...", subStr];
            self.descriptionLbl.text = subStr;
            self.btnReadMore.hidden = NO;
        }
        else {
            self.descriptionLbl.text = [dict objectForKey:kKeyDescription];
            self.btnReadMore.hidden = YES;
        }
    }
    }
    else {
        self.descriptionLbl.text = @"----";
        self.btnReadMore.hidden = YES;
    }
    
    // TODO: Open dating set image accrodingly
    
    // Display images in scroll view
    NSArray *mediaArr = [dict objectForKey:kKeyMedia];
    for (int i = 0; i < mediaArr.count; i++) {
        NSDictionary *mediaDict = [mediaArr objectAtIndex:i];
        
        CGRect frame;
        frame.origin.x = self.BGScrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.BGScrollView.frame.size;
        
        UIView *subview = [[UIView alloc] initWithFrame:frame];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor blackColor];
        
        imageView.userInteractionEnabled = YES;
        //Add tapGesture On ImageView to set as profile pic
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTappedAction:)];
        [imageView addGestureRecognizer:tapGesture];
        
        [subview addSubview:imageView];
        
        // Get result
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIImage *img = nil;
            
            if ([mediaDict objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [mediaDict objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kServerAddress, kKeyUserProfileImage, imageName];
                    img = [UIImage imageWithData:
                           [NSData dataWithContentsOfURL:
                            [NSURL URLWithString:imageUrl]]];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (img) {
                        imageView.image = img;
                    }
                });
            }
        });
        
        UILabel *countLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
        countLbl.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
        NSString *str1 = [NSString stringWithFormat:@"%d of %lu", i + 1, (unsigned long)mediaArr.count];
        countLbl.textColor = [UIColor whiteColor];
        countLbl.text = str1;
        [subview addSubview:countLbl];
        
        UIButton *ContactBtn = [[UIButton alloc] initWithFrame:CGRectMake(275, 5, 25, 25)];
        [ContactBtn setBackgroundImage:[UIImage imageNamed:@"back-to-profile.png"] forState:UIControlStateNormal];
        [ContactBtn addTarget:self action:@selector(contactBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [subview addSubview:ContactBtn];
        
        if (i == 0) {
            lblCountOfImages = countLbl;
            lblCountOfImages.hidden = YES;
            btnProfileDetail = ContactBtn;
            btnProfileDetail.hidden = YES;
        }
        [self.BGScrollView addSubview:subview];
    }
    self.BGScrollView.contentSize = CGSizeMake(self.BGScrollView.frame.size.width * mediaArr.count, 0);
    self.BGScrollView.scrollEnabled = NO;
}


#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer{
    // Call super
    self = [super initWithNibName:@"SRUserViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
    }
    
    // Return
    return self;
}

#pragma mark
#pragma mark Statndard Method
#pragma mark

// ---------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    [super viewDidLoad];
    // Set image view gredient
    self.profileBGImage.alpha = 0.95;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.profileBGImage.bounds;
    gradient.colors = @[(id)[[UIColor blackColor] CGColor],
                        (id)[[UIColor whiteColor] CGColor]];
    [self.profileBGImage.layer insertSublayer:gradient atIndex:0];
    
    // Hide the scroll at first
    self.BGScrollView.hidden = YES;
    self.familyScrollview.scrollEnabled = NO;
    self.connectionScrollview.scrollEnabled = NO;
    self.interestsPhotosScrollview.scrollEnabled = NO;
    
    //set Constant text to labels
    self.familyLbl.text = NSLocalizedString(@"lbl.family.txt", @"");
    self.connectionLbl.text = NSLocalizedString(@"lbl.connections.txt", @"");
    self.lblInterestsCount.text = NSLocalizedString(@"lbl.interests.txt", @"");
    
    // TODO: Later family and connections
    /*[self addImagesInScrollView:YES];
     [self addImagesInScrollView:NO];*/
    
    //Add Shadow to btnAlbum
    [self.btnAlbum.layer setShadowOffset:CGSizeMake(5, 5)];
    [self.btnAlbum.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.btnAlbum.layer setShadowOpacity:8.5];
    
    self.degreeLbl.layer.cornerRadius = self.degreeLbl.frame.size.width / 2;
    self.degreeLbl.layer.masksToBounds = YES;
    NSString *degreeString = [NSString stringWithFormat:@"%d%@", 6, @"\u00B0"];
    self.degreeLbl.text = degreeString;
    
    
}

// ------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    // Call Super
    [super viewWillAppear:NO];
    
    // Display details
    [self displayDetails];
    [self setScrollContentSize];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ------------------------------------------------------------------------------------------------------
// btnReadMoreClick:

- (IBAction)btnReadMoreClick:(id)sender {
    if ([self.btnReadMore.titleLabel.text isEqualToString:@" << "]) {
        NSString *str = [server.loggedInUserInfo objectForKey:kKeyDescription];
        NSString *subStr = [str substringWithRange:NSMakeRange(0, 135)];
        subStr = [NSString stringWithFormat:@"%@...", subStr];
        self.descriptionLbl.text = subStr;
        
        [self.btnReadMore setTitle:@"read more" forState:UIControlStateNormal];
    }
    else {
        NSString *str = [server.loggedInUserInfo objectForKey:kKeyDescription];
        self.descriptionLbl.text = str;
        [self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
    }
    
    // Set Scroll Size
    [self setScrollContentSize];
}

// ------------------------------------------------------------------------------------------------------
// btnAlbumClick:

- (IBAction)btnAlbumClick:(id)sender {
    NSArray *mediaArr = [server.loggedInUserInfo objectForKey:kKeyMedia];
    if ([mediaArr count] > 0) {
        lblCountOfImages.hidden = NO;
        btnProfileDetail.hidden = NO;
        
        self.btnAlbum.hidden = YES;
        
        self.profileBGImage.hidden = YES;
        self.profileDetailScrollview.hidden = YES;
        
        self.BGScrollView.hidden = NO;
        self.BGScrollView.scrollEnabled = YES;
    }
    else {
        [SRModalClass showAlert:NSLocalizedString(@"alert.no.albums.text", @"")];
    }
}

// ------------------------------------------------------------------------------------------------------
// contactBtnClicked:

- (void)contactBtnClicked:(UIButton *)sender {
    UIView *view = [sender superview];
    for (UIView *subView in[view subviews]) {
        if ([subView isKindOfClass:[UILabel class]] || [subView isKindOfClass:[UIButton class]]) {
            subView.hidden = YES;
            if ([subView isKindOfClass:[UILabel class]]) {
                lblCountOfImages = (UILabel *)subView;
            }
            else {
                btnProfileDetail = (UIButton *)subView;
            }
        }
    }
    self.btnAlbum.hidden = NO;
    self.profileBGImage.hidden = NO;
    self.profileDetailScrollview.hidden = NO;
    self.profileDetailScrollview.scrollEnabled = YES;
    
    self.BGScrollView.hidden = YES;
    self.BGScrollView.scrollEnabled = NO;
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)addImagesInScrollView:(BOOL)isFamily {
    // Frane at starting point
    CGRect latestFrame = CGRectZero;
    
    // Adding images to scroll view
    for (int i = 0; i < 4; i++) {
        UIImageView *objImageView = [[UIImageView alloc] init];
        if (i == 0) {
            [objImageView setFrame:CGRectMake(5, 0, 40, 40)];
        }
        else
            objImageView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 5, 0, 40, 40);
        latestFrame = objImageView.frame;
        
        objImageView.image = [UIImage imageNamed:@"menu_familyperson_image.png"];
        objImageView.contentMode = UIViewContentModeScaleToFill;
        objImageView.layer.cornerRadius = objImageView.frame.size.width / 2;
        objImageView.clipsToBounds = YES;
        
        UILabel *lblImageName = [[UILabel alloc] init];
        [lblImageName setFrame:CGRectMake(latestFrame.origin.x + 5, latestFrame.origin.y + 35, 40, 18)];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        NSString *str1 = [NSString stringWithFormat:@"Image%d", i];
        lblImageName.textColor = [UIColor whiteColor];
        lblImageName.text = str1;
        
        if (isFamily) {
            [self.familyScrollview addSubview:objImageView];
            [self.familyScrollview addSubview:lblImageName];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.familyScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.familyScrollview.scrollEnabled = YES;
            }
        }
        else {
            [self.connectionScrollview addSubview:lblImageName];
            [self.connectionScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.connectionScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.connectionScrollview.scrollEnabled = YES;
            }
            
            // Add images in Interests Scrollview
            [self.interestsPhotosScrollview addSubview:lblImageName];
            [self.interestsPhotosScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.interestsPhotosScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.interestsPhotosScrollview.scrollEnabled = YES;
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)imageTappedAction:(UITapGestureRecognizer *)tapGesture {
    /*
     // Image picker action will happen here
     
     // Present action sheet
     actionSheet = [[UIActionSheet alloc]initWithTitle:nil
     delegate:self
     cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "")destructiveButtonTitle:nil                                       otherButtonTitles:NSLocalizedString(@"setasprofilepic.text", ""), nil];
     
     
     // Show sheet
     [actionSheet showFromRect:[self.view frame] inView:self.view animated:YES];*/
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex ==  0) {
        // NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, [server.loggedInUserInfo objectForKey:kKeyId]];
        //[server asychronousRequestWithData:nil imageData:imageData forKey:kKeyProfileImage toClassType:urlStr inMethodType:kPOST];
        
        UIButton *btn;
        [self contactBtnClicked:btn];
    }
    else if (buttonIndex == 1) {
    }
}


@end
