
#import <UIKit/UIKit.h>

@interface SRSOPTableViewCell : UITableViewCell
{
    //Instance Variables
    UIImageView *CellImage;
    BOOL isTap;
    
}

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)singleTapGesture:(id)sender;

@end
