

#import <UIKit/UIKit.h>

@interface SRUserViewController : UIViewController
{
    // Instance variable
    UILabel *lblCountOfImages;
    UIButton *btnProfileDetail;
    NSDictionary *dataDict;
    
    //Server
    SRServerConnection *server;
}

@property (weak, nonatomic) IBOutlet UIScrollView *BGScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnAlbum;
@property (weak, nonatomic) IBOutlet UIImageView *profileBGImage;
@property (weak, nonatomic) IBOutlet UIScrollView *profileDetailScrollview;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UIButton *btnReadMore;
@property (weak, nonatomic) IBOutlet UIView *datingView;
@property (weak, nonatomic) IBOutlet UILabel *datingLbl;
@property (weak, nonatomic) IBOutlet UIImageView *datingImage;
@property (weak, nonatomic) IBOutlet UIView *familyView;
@property (weak, nonatomic) IBOutlet UILabel *familyLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *familyScrollview;
@property (weak, nonatomic) IBOutlet UIView *connectionView;
@property (weak, nonatomic) IBOutlet UILabel *connectionLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *connectionScrollview;
@property (weak, nonatomic) IBOutlet UIButton *flagBtn;
@property (weak, nonatomic) IBOutlet UILabel *degreeLbl;

- (IBAction)btnAlbumClick:(id)sender;
- (IBAction)btnReadMoreClick:(id)sender;


//Other Property
@property(weak)id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
