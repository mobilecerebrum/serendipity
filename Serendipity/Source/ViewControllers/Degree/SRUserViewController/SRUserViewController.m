

#import "SRUserViewController.h"


@implementation SRUserViewController


#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRUserViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        dataDict = inProfileDict;
    }
    
    // Return
    return self;
}

#pragma mark
#pragma mark Statndard Method
#pragma mark

// ---------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    [super viewDidLoad];
    // Code for appnendting two string for Profile label address.
    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc]initWithString:@"26, Lawyer in "];
    NSAttributedString *atrStr = [[NSAttributedString alloc]initWithString:@" New York" attributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaItalicMedium size:12] }];
    [muAtrStr appendAttributedString:atrStr];
    
    self.descriptionLbl.numberOfLines = 0;
    [self.addressLbl setAttributedText:muAtrStr];
    self.degreeLbl.layer.cornerRadius = self.degreeLbl.frame.size.width / 2;
    self.degreeLbl.layer.masksToBounds = YES;
    
    // Display images in scroll view
    NSArray *colors = [NSArray arrayWithObjects:[UIColor colorWithPatternImage:[UIImage imageNamed:@"menu_back.png"]], [UIColor redColor], [UIColor greenColor], [UIColor blueColor], [UIColor orangeColor], [UIColor purpleColor], nil];
    for (int i = 0; i < colors.count; i++) {
        CGRect frame;
        frame.origin.x = self.BGScrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.BGScrollView.frame.size;
        
        UIView *subview = [[UIView alloc] initWithFrame:frame];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageView.contentMode = UIViewContentModeScaleToFill;
        [subview addSubview:imageView];
        if (i % 2 == 0) {
            
             imageView.image = [UIImage imageNamed:
                                   [dataDict valueForKey:@"ProfileImage"]];
            
            //Set Profile Name get from Server
            self.profileNameLbl.text = [NSString stringWithFormat:@"%@ %@",[server.loggedInUserInfo valueForKey:@"first_name"],[server.loggedInUserInfo valueForKey:@"last_name"] ];
        }
        else
        {
            imageView.image = [UIImage imageNamed:
                               [dataDict valueForKey:@"ProfileImage"]];
            
            //Set Profile Name get from Server
            self.profileNameLbl.text = [dataDict valueForKey:@"ProfileName"];
        }
        
        
        UILabel *countLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
        countLbl.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
        NSString *str1 = [NSString stringWithFormat:@"%d of 5", i];
        countLbl.textColor = [UIColor whiteColor];
        countLbl.text = str1;
        [subview addSubview:countLbl];
        
        UIButton *ContactBtn = [[UIButton alloc] initWithFrame:CGRectMake(275, 5, 25, 25)];
        [ContactBtn setBackgroundImage:[UIImage imageNamed:@"back-to-profile.png"] forState:UIControlStateNormal];
        [ContactBtn addTarget:self action:@selector(contactBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [subview addSubview:ContactBtn];
        
        if (i == 0) {
            lblCountOfImages = countLbl;
            lblCountOfImages.hidden = YES;
            btnProfileDetail = ContactBtn;
            btnProfileDetail.hidden = YES;
        }
        [self.BGScrollView addSubview:subview];
    }
    self.BGScrollView.contentSize = CGSizeMake(self.BGScrollView.frame.size.width * colors.count, 0);
    self.BGScrollView.scrollEnabled = NO;
    
    // Set image view gredient
    self.profileBGImage.alpha = 0.95;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.profileBGImage.bounds;
    gradient.colors = @[(id)[[UIColor blackColor] CGColor],
                        (id)[[UIColor whiteColor] CGColor]];
    [self.profileBGImage.layer insertSublayer:gradient atIndex:0];
    
    [self setScrollContentSize];
    
    // Call method for family & Connection photoArray in scrollview
    self.familyScrollview.scrollEnabled = NO;
    self.connectionScrollview.scrollEnabled = NO;
    [self addImagesInScrollView:YES];
    [self addImagesInScrollView:NO];
    
   
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ------------------------------------------------------------------------------------------------------
// btnReadMoreClick:
- (IBAction)btnReadMoreClick:(id)sender {
    if (self.descriptionLbl.text.length >= 135) {
        self.btnReadMore.hidden = NO;
        if ([self.btnReadMore.titleLabel.text isEqualToString:@" << "]) {
            self.descriptionLbl.text = @"Lorem ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis Lorem ipsum dolor sit er elit lamet...";
            [self.btnReadMore setTitle:@"read more" forState:UIControlStateNormal];
        }
        else {
            self.descriptionLbl.text = @"check sum ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis nostrud exercitation ullamco laboris nisi ut aliquip adipisicing.26, Lawyer in New York Lorem ipsum dolor sit er elit lamet, abvd gthjkll dfgd consectetaur cillium adipisicing pecu, quis nostrud exercitation ullamco laboris nisi ut aliquip adipisicing adipisicing pecu, quis Lorem ipsum dolor sit er elit adipisicing pecu, quis Lorem ipsum dolor sit er elit .";
            
            [self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
        }
        [self setScrollContentSize];
    }
    else {
        self.btnReadMore.hidden = YES;
    }
}

// ------------------------------------------------------------------------------------------------------
// btnAlbumClick:

- (IBAction)btnAlbumClick:(id)sender {
    lblCountOfImages.hidden = NO;
    btnProfileDetail.hidden = NO;
    self.btnAlbum.hidden = YES;
    self.profileBGImage.hidden = YES;
    self.BGScrollView.scrollEnabled = YES;
    self.profileDetailScrollview.hidden = YES;
}

// ------------------------------------------------------------------------------------------------------
// contactBtnClicked:

- (void)contactBtnClicked:(UIButton *)sender {
    UIView *view = [sender superview];
    for (UIView *subView in[view subviews]) {
        if ([subView isKindOfClass:[UILabel class]] || [subView isKindOfClass:[UIButton class]]) {
            subView.hidden = YES;
            if ([subView isKindOfClass:[UILabel class]]) {
                lblCountOfImages = (UILabel *)subView;
            }
            else {
                btnProfileDetail = (UIButton *)subView;
            }
        }
    }
    self.btnAlbum.hidden = NO;
    self.profileBGImage.hidden = NO;
    self.BGScrollView.scrollEnabled = NO;
    self.profileDetailScrollview.hidden = NO;
    self.profileDetailScrollview.scrollEnabled = YES;
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)addImagesInScrollView:(BOOL)isFamily {
    // Frane at starting point
    CGRect latestFrame = CGRectZero;
    
    // Adding images to scroll view
    for (int i = 0; i < 4; i++) {
        UIImageView *objImageView = [[UIImageView alloc] init];
        if (i == 0) {
            [objImageView setFrame:CGRectMake(5, 0, 40, 40)];
        }
        else
            objImageView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 5, 0, 40, 40);
        latestFrame = objImageView.frame;
        
        objImageView.image = [UIImage imageNamed:@"menu_familyperson_image.png"];
        objImageView.contentMode = UIViewContentModeScaleToFill;
        objImageView.layer.cornerRadius = objImageView.frame.size.width / 2;
        objImageView.clipsToBounds = YES;
        
        UILabel *lblImageName = [[UILabel alloc] init];
        [lblImageName setFrame:CGRectMake(latestFrame.origin.x + 5, latestFrame.origin.y + 35, 40, 18)];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        NSString *str1 = [NSString stringWithFormat:@"Image%d", i];
        lblImageName.textColor = [UIColor whiteColor];
        lblImageName.text = str1;
        
        if (isFamily) {
            [self.familyScrollview addSubview:objImageView];
            [self.familyScrollview addSubview:lblImageName];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.familyScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.familyScrollview.scrollEnabled = YES;
            }
        }
        else {
            [self.connectionScrollview addSubview:lblImageName];
            [self.connectionScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.connectionScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.connectionScrollview.scrollEnabled = YES;
            }
        }
    }
}


//---------------------------------------------------------------------------------------------------------------------------------
// setScrollContentSize

- (void)setScrollContentSize {
    CGRect frame = self.descriptionLbl.frame;
    CGRect textRect = [self.descriptionLbl.text boundingRectWithSize:CGSizeMake(self.descriptionLbl.frame.size.width, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{ NSFontAttributeName:self.descriptionLbl.font }
                                                             context:nil];
    
    frame.size.height = textRect.size.height;
    self.descriptionLbl.frame = frame;
    self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x,
                                        self.descriptionLbl.frame.origin.y + self.descriptionLbl.frame.size.height - 10,
                                        self.btnReadMore.frame.size.width,
                                        self.btnReadMore.frame.size.height);
    frame = self.datingView.frame;
    frame.origin.y = self.descriptionLbl.frame.origin.y + self.descriptionLbl.frame.size.height + 8;
    self.datingView.frame = frame;
    
    frame = self.familyView.frame;
    frame.origin.y = self.datingView.frame.origin.y + self.datingView.frame.size.height + 8;
    self.familyView.frame = frame;
    
    frame = self.connectionView.frame;
    frame.origin.y = self.familyView.frame.origin.y + self.familyView.frame.size.height + 8;
    self.connectionView.frame = frame;
    
    self.profileDetailScrollview.contentSize = CGSizeMake(0,  self.connectionView.frame.origin.y + self.connectionView.frame.size.height + 50);
    self.profileDetailScrollview.scrollEnabled = YES;
}


@end
