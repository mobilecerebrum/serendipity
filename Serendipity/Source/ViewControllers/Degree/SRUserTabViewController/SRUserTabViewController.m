#import "SRModalClass.h"
#import "SRUserViewController.h"
#import "SRPingsViewController.h"
#import "SRSOPViewController.h"
#import "SRUserTabViewController.h"


@implementation SRUserTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    dataDict = inProfileDict;
    self = [super initWithNibName:@"SRUserTabViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        
    }
    
    // Return
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Title
    //[SRModalClass setNavTitle:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"first_name"] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    [SRModalClass setNavTitle:[dataDict valueForKey:@"ProfileName"] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-man-plus.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(addProfileBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];
    
    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaMedium size:10.0f],
                                                         NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaRegular size:10.0f],
                                                         NSForegroundColorAttributeName : appTintColor } forState:UIControlStateNormal];
    
    // Set Profile tab
    SRUserViewController *objUser = [[SRUserViewController alloc]initWithNibName:nil bundle:nil profileData:dataDict server:(APP_DELEGATE).server];
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-user-female.png"];
    UITabBarItem *objUserTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"tab.profile.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-user-female-active.png"]];
    [objUserTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objUser.tabBarItem = objUserTabItem;
    
   
    //  Set Degree of Seperation tab
    SRSOPViewController *objSRSOP = [[SRSOPViewController alloc]initWithNibName:nil bundle:nil profileData:dataDict server:server];
    tabImage = [UIImage imageNamed:@"tabbar-degree.png"];
    UITabBarItem *objSRSOPTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"Degree Of Seperation", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-degree-active.png"]];
    [objSRSOPTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRSOP.tabBarItem = objSRSOPTabItem;
    
    
    //Set Pings tab
    SRPingsViewController *objSRPingsView = [[SRPingsViewController alloc]initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-ping-active.png"];
    UITabBarItem *SRPingsViewTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"tab.ping.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-ping-active.png"]];
    [SRPingsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRPingsView.tabBarItem = SRPingsViewTabItem;
    
    self.viewControllers = [NSArray arrayWithObjects:objUser, objSRSOP, objSRPingsView, nil];
    self.selectedIndex = 0;

}

#pragma mark
#pragma mark Action Methodss
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

// ---------------------------------------------------------------------------------------
// plusBtnAction:

- (void)addProfileBtnAction:(id)sender {
}



@end
