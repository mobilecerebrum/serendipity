
#import <UIKit/UIKit.h>

@interface SRUserTabViewController : UITabBarController
{
    //Instance Variables
    NSDictionary *dataDict;
    
    //Server
    SRServerConnection *server;
}



#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
