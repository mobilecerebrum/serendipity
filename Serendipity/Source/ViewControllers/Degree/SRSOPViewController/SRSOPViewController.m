#import "SRSOPTableViewCell.h"

#import "SRSOPViewController.h"


@implementation SRSOPViewController


#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    dataDict = inProfileDict;
    self = [super initWithNibName:@"SRSOPViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        
    }
    
    // Return
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //self.objTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //DegreeBGView Background Appearance
    self.degreeBGView.layer.borderWidth = 1.0;
    self.degreeBGView.layer.borderColor = (__bridge CGColorRef)([UIColor blackColor]);
    self.degreeBGView.layer.cornerRadius = self.degreeBGView.frame.size.width / 2;
    self.degreeBGView.clipsToBounds = YES;
   
    
    //DegreeBTN Background Appearance
    
    self.degreeBtn.layer.borderWidth = 1.0;
    self.degreeBtn.layer.borderColor = (__bridge CGColorRef)([UIColor blackColor]);
    self.degreeBtn.layer.cornerRadius = self.degreeBGView.frame.size.width / 2.5;
    self.degreeBtn.clipsToBounds = YES;
    
    
    //Text Degree on Button
    NSString *degreeText = [NSString stringWithFormat:@"5%@", @"\u00B0"];
    [self.degreeBtn setTitle:degreeText forState:UIControlStateNormal];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// --------------------------------------------------------------------------------
// degreeBtnAction:

- (IBAction)degreeBtnAction:(id)sender
{
    
    
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

// ----------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SRSOPTableViewCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRSOPTableViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            cell = (SRSOPTableViewCell *)[nibObjects objectAtIndex:0];
         
        }
    }
   
    cell.profileImage.image = [UIImage imageNamed:@"menu_familyperson_image.png"];
       
    // Return
    return cell;
}

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}


@end
