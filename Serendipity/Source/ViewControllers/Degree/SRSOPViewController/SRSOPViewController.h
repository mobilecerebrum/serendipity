
#import <UIKit/UIKit.h>

@interface SRSOPViewController : UIViewController
{
    //Instance Variables
    NSDictionary *dataDict;
    
    //Server
    SRServerConnection *server;

}

//Prperties
@property (strong, nonatomic) IBOutlet UITableView *objTableView;
@property (strong, nonatomic) IBOutlet UIView *degreeBGView;
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn;

//Other Prperties
@property (weak) id delegate;


//IBActions
- (IBAction)degreeBtnAction:(id)sender;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
