
#import "SRSOPTableViewCell.h"

@implementation SRSOPTableViewCell

- (void)awakeFromNib {
    // Initialization code
    CellImage = [[UIImageView alloc] initWithFrame:CGRectMake(135, 5, 50, 50)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (IBAction)singleTapGesture:(id)sender {
    
    CellImage.image= self.profileImage.image;
    
    if (!isTap) {
        
        isTap = YES;
        
        
        for (UIView *subview in self.scrollView.subviews) {
            [subview removeFromSuperview];
        }
        [self addImagesInScrollView:YES];
       
    }
    else
    {
        isTap = NO;
        
        for (UIView *subview in self.scrollView.subviews) {
            [subview removeFromSuperview];
        }
        //        // Trigger a new centering (center will change the content offset)
        //        scrollView.contentInset = UIEdgeInsetsZero;
        //        [scrollView ensureContentIsCentered];
        [self.scrollView addSubview:CellImage];
        

        
    }
    
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)addImagesInScrollView:(BOOL)isTap {
    
       // Frame at starting point
    CGRect latestFrame = CGRectZero;
    
    // Adding images to scroll view
    for (int i = 0; i < 6; i++) {
        UIImageView *objImageView = [[UIImageView alloc] init];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]init];
        tapGesture.numberOfTapsRequired = 1;
        [tapGesture addTarget:self action:@selector(ImageTapped:)];
        [objImageView addGestureRecognizer:tapGesture];
        if (i == 0) {
            [objImageView setFrame:CGRectMake(5, 5, 50, 50)];
        }
        else
            objImageView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 5, 5, 50, 50);
        latestFrame = objImageView.frame;
        
        objImageView.image = [UIImage imageNamed:@"menu_familyperson_image.png"];
        objImageView.contentMode = UIViewContentModeScaleToFill;
        objImageView.layer.cornerRadius = objImageView.frame.size.width / 2;
        objImageView.clipsToBounds = YES;
        
        
        [self.scrollView addSubview:objImageView];
      
        if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
            self.scrollView.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
            self.scrollView.scrollEnabled = YES;
        }
        
    }
}

-(void)ImageTapped:(UITapGestureRecognizer *) Tapped
{
    
}
@end
