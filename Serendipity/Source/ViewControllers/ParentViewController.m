//
//  ParentViewController.m
//  Serendipity
//
//  Created by Hitesh Surani on 02/04/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "ParentViewController.h"
#import "SRDiscoveryListViewController.h"
#import "SRDiscoveryMapViewController.h"
#import "SRDiscoveryRadarViewController.h"
#import "HSLocationManager.h"

@interface ParentViewController ()

@end

@implementation ParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   // NSLog(@"\n========= \nClass Name: %@ \n=========",self);
    [self hideShowTopBar];
}

- (void)viewDidAppear:(BOOL)animated{
    [self hideShowTopBar];
}

-(void) hideShowTopBar{
    if ([self isKindOfClass:[SRDiscoveryListViewController self]] || [self isKindOfClass:[SRDiscoveryMapViewController self]] || [self isKindOfClass:[SRDiscoveryRadarViewController self]]){
            (APP_DELEGATE).topBarView.hidden = NO;
    }else{
        (APP_DELEGATE).topBarView.hidden = YES;
    }
}

-(void) hsHideShowTopBar{
    UIViewController* vc = [[HSLocationManager sharedManager]topViewController];
    if ([vc isKindOfClass:[SRDiscoveryListViewController self]] || [vc isKindOfClass:[SRDiscoveryMapViewController self]] || [vc isKindOfClass:[SRDiscoveryRadarViewController self]]){
            (APP_DELEGATE).topBarView.hidden = NO;
    }else{
        (APP_DELEGATE).topBarView.hidden = YES;
    }
}

- (CGFloat)getBottomSafeArea{
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        return window.safeAreaInsets.bottom;
    }
    return 0.0;
}


- (CGFloat)getTopSafeArea{
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        return window.safeAreaInsets.top;
    }
    return 0.0;
}

@end
