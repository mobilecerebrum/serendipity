#import "SRModalClass.h"
#import "SwipeableTableViewCell.h"
#import "SRConnectionCellView.h"
#import "SRNewEventViewController.h"
#import "SRNewGroupViewController.h"
#import "SRAddConnectionTabViewController.h"
#import "SRConnectionNotificationCell.h"
#import "SRUserTabViewController.h"
#import "SRNewGroupViewController.h"
#import "SRMyConnectionsViewController.h"
#import "SRMapViewController.h"
#import "UIImage+animatedGIF.h"
#import "SRDistanceClass.h"
#import "SRModalClass.h"
#import "SRContactsViewController.h"

@interface myConnDistanceClass : NSObject

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *distance;
@property(nonatomic, strong) NSString *dataId;
@property(strong, nonatomic) CLLocation *myLocation;
@property(nonatomic, strong) NSDictionary *userDict;

+ (myConnDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation;

@end

@implementation myConnDistanceClass

+ (myConnDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation {

    myConnDistanceClass *obj = [[myConnDistanceClass alloc] init];
    obj.dataId = userDict[kKeyId];
    NSDictionary *locationDict = userDict[kKeyLocation];
    obj.lat = locationDict[kKeyLattitude];
    obj.longitude = locationDict[kKeyRadarLong];
    obj.distance = userDict[kKeyDistance];
    obj.myLocation = myLocation;
    obj.userDict = userDict;

    return obj;
}

@end


@interface SRMyConnectionsViewController ()


@end


@implementation SRMyConnectionsViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SRMyConnectionsViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        updatedNotiList = [[NSMutableArray alloc] init];
        membersArray = [[NSMutableArray alloc] init];
        searchedUsersArr = [[NSMutableArray alloc] init];
        connectionList = [[NSMutableArray alloc] init];

        _matrixRequestConnArray = [[NSMutableArray alloc] init];
        _matrixResponseConnArray = [[NSMutableArray alloc] init];

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        
        
        [defaultCenter addObserver:self
        selector:@selector(deletBlockConnectionSucceed:)
            name:kKeyNotificationGetDeleteBlockConnectionSucceed object:nil];
        [defaultCenter addObserver:self
        selector:@selector(deletBlockConnectionFailed:)
            name:kKeyNotificationGettDeleteBlockConnectionFailed object:nil];
        
        
        
        [defaultCenter addObserver:self
                          selector:@selector(getConnectionSucceed:)
                              name:kKeyNotificationGetConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getConnectionFailed:)
                              name:kKeyNotificationGetConnectionFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateConnectionSucceed:)
                              name:kKeyNotificationUpdateConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateConnectionFailed:)
                              name:kKeyNotificationUpdateConnectionFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(getNotificationSucceed:)
                              name:kKeyNotificationGetNotificationSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationFailed:)
                              name:kKeyNotificationGetNotificationFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(updateNotificationSucceed:)
                              name:kKeyNotificationUpdateNotificationSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateNotificationFailed:)
                              name:kKeyNotificationUpdateNotificationFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(onlineStatusChanged)
                              name:kKeyNotificationOnlineStatus
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(addUserInFavouriteSucceed:)
                              name:kKeyNotificationAddFavouritesSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(addUserInFavouriteFailed:)
                              name:kKeyNotificationAddFavouritesFailed
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(addUserInvisibleSucceed:)
                              name:kKeyNotificationAddInvisibleSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(addUserInvisibleFailed:)
                              name:kKeyNotificationAddInvisibleFailed
                            object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(CreateGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(CreateGroupFailed:)
                              name:kKeyNotificationCreateGroupFailed object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                          selector:@selector(getConnectionData)
                              name:UIApplicationDidBecomeActiveNotification
                            object:nil];

        //[defaultCenter addObserver:self
        // selector:@selector(blockConnectionSucceed:)
        //    name:kKeyNotificationUpdateNotificationFailed object:nil];
        //

    }

    //return
    return self;
}

-(void) getConnectionData{
    [server makeAsychronousRequest:kKeyClassGetConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];
    addTextInSignificantTxt(@"my connection loaded");
    // Overlay image view
    searchQueue = [[NSOperationQueue alloc] init];
    overlayImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.objtableView.frame.origin.y, SCREEN_WIDTH, self.objtableView.frame.size.height)];
    overlayImgView.alpha = 0.4;
    overlayImgView.userInteractionEnabled = YES;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = overlayImgView.bounds;
        gradient.colors = @[(id) [[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0] CGColor], (id) [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.8] CGColor]];
        [overlayImgView.layer insertSublayer:gradient atIndex:0];
    });

    // Manage search bar
  //  [self.searchBar becomeFirstResponder];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.MyConnections.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    //Empty Data Label
    self.lblNoData = [[UILabel alloc] init];
    self.lblNoData.textAlignment = NSTextAlignmentCenter;
    self.lblNoData.textColor = [UIColor orangeColor];
    self.lblNoData.text = NSLocalizedString(@"lbl.noConnectionFnd.txt", @"");
    if ((APP_DELEGATE).isFromNotification) {
//        (APP_DELEGATE).topBarView.hidden = YES;
        (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
        [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
        (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    }
    
    arrSelectedUsers = [[NSMutableArray alloc] init];
}

// ----------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    // Call super
    [super viewWillAppear:NO];

    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    if (!isOnce) {
        isOnce = YES;

        // Server call Get Connection
        [APP_DELEGATE showActivityIndicator];
        [server makeAsychronousRequest:kKeyClassGetConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];

        notificationsList = server.notificationArr;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"1"];
        notificationsList = [notificationsList filteredArrayUsingPredicate:predicate];

        if ([notificationsList count] == 0) {
            self.btnNotifications.hidden = YES;
            self.imgNatificationsBG.hidden = YES;
            self.imgDropDown.hidden = YES;
            self.notificationsTable.hidden = YES;

            self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        } else {
            self.notificationsTable.frame = CGRectMake(0, 40, self.view.frame.size.width, 0);
            self.notificationsTable.hidden = YES;

            // Set Title to Notification Button
            NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [notificationsList count]]];
            [self.btnNotifications setTitle:str forState:UIControlStateNormal];
        }
    } else {
        [self.objtableView reloadData];
    }
}

#pragma mark
#pragma mark NavBar Action Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    if ((APP_DELEGATE).isFromNotification && (APP_DELEGATE).isOnChatList == FALSE) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
        (APP_DELEGATE).isFromNotification = NO;
        (APP_DELEGATE).isNotificationFromClosed = FALSE;
    } else {
        [APP_DELEGATE hideActivityIndicator];
        if (self.delegate) {
            [self.navigationController popToViewController:self.delegate animated:NO];
        } else {
            [self.navigationController popViewControllerAnimated:NO];
        }
    }

}

// ----------------------------------------------------------------------------------------------------------
// plusBtnAction:

- (void)plusBtnAction:(id)sender {
    if ([APP_DELEGATE isUserBelow18]){
        SRContactsViewController *objContacts = [[SRContactsViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:(APP_DELEGATE).server];
        objContacts.connectionViewTag = kKeyIsFromConnection;
        [self.navigationController pushViewController:objContacts animated:NO];


    }else{
        SRAddConnectionTabViewController *newConn = [[SRAddConnectionTabViewController alloc] initWithNibName:nil bundle:nil];
        self.navigationController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:newConn animated:NO];
        self.navigationController.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark
#pragma mark CellButton Action Methods
#pragma mark
// ---------------------------------------------------------------------------------------------------------------------------------
// blockUserAction:

- (void)blockUserAction:(UIButton *)sender {

    NSInteger i = [sender tag];
    NSDictionary *userDict = connectionList[i];

    // Call notification API
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = userDict[kKeyId];//connectionParams[kKeyConnectionID];
    params[kKeyType] = @"block";
    params[kKeyIsContact] = @0;
    params[kKeyStatus] = @1;
    
    NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
    [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    [connectionList removeObjectAtIndex:i];
    [self.objtableView reloadData];

//    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
//
//        NSDictionary *connectionDict = userDict[kKeyConnection];
//        strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, connectionDict[kKeyId]];
//        [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPUT];
//    } else {
//
//        [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
//
//    }
}
- (void)deleteUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    NSInteger i = [btnSender tag];
    NSDictionary *userDict = connectionList[i];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = userDict[kKeyId];//connectionParams[kKeyConnectionID];
    params[kKeyType] = @"delete";
    params[kKeyIsContact] = @0;
    params[kKeyStatus] = @1;
    NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
    
    [APP_DELEGATE showActivityIndicator];
    [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    [connectionList removeObjectAtIndex:i];
    [self.objtableView reloadData];

//    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
//        if ([userDict[kKeyDegree] integerValue] == 1) {
//            NSDictionary *connectionDict = userDict[kKeyConnection];
//            params[kKeyConnectionStatus] = @kKeyConnectionBlock;
//            strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, connectionDict[kKeyId]];
//            [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPUT];
//        }
//    } else {
//        params[kKeyConnectionStatus] = @kKeyConnectionBlockForNonConn;
//        [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
//    }
}
// ---------------------------------------------------------------------------------------------------------------------------------
// pingUserAction:

- (void)pingUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    if (btnSender.tag == btnSender.tag) {

        BOOL flag = NO;

        NSDictionary *userDict = connectionList[btnSender.tag];;

        if ([[userDict valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[userDict valueForKey:kKeySetting]];

//            if (![[setting valueForKey:kKeyallowPings]boolValue])
//                flag=NO;
            NSUInteger pingStatus = [[setting valueForKey:kKeyallowPings] integerValue];
            if (pingStatus == 1) {
                flag = YES;
            } else if (pingStatus == 2) {
                flag = YES;
            } else if (pingStatus == 3) {
                if ([[userDict valueForKey:kKeyConnection] isKindOfClass:[NSDictionary class]]) {
                    flag = YES;
                }
            } else if (pingStatus == 4) {
                if ([[userDict valueForKey:kKeyToFavourite] boolValue]) {
                    flag = YES;
                }
            } else if (pingStatus == 5) {
                if ([[userDict valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
                    flag = YES;
                }
            } else if (pingStatus == 6) {
                flag = NO;
            }
        }
        if (flag) {
            [SwipeableTableViewCell closeAllCells];
            SRChatViewController *objChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:server inObjectInfo:userDict];
            objChatView.delegate = self;
            [self.navigationController pushViewController:objChatView animated:YES];
        } else {
            [SRModalClass showAlert:@"Sorry, this user has disabled chat so they cannot receive any messages."];
            [SwipeableTableViewCell closeAllCells];
        }
    }
}
// ----------------------------------------------------------------------------------------------------------------------
// favouriteUserAction:

- (void)favouriteUserAction:(UIButton *)btnSender {
    favTag = btnSender.tag;
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE showActivityIndicator];

    NSDictionary *userDict = connectionList[btnSender.tag];
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassfavourites];
    NSDictionary *params = @{kKeyFavUserID: [userDict valueForKey:kKeyId], kKeyUserID: server.loggedInUserInfo[kKeyId]};
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:NO inMethodType:kPOST];
}

// ----------------------------------------------------------------------------------------------------------------------
// invisibleUserAction:

- (void)invisibleUserAction:(UIButton *)btnSender {
    invisibleTag = btnSender.tag;
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE showActivityIndicator];
    NSDictionary *userDict = connectionList[btnSender.tag];
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassInvisible];
    NSDictionary *params = @{kKeyInvisibleUserID: [userDict valueForKey:kKeyId], kKeyUserID: server.loggedInUserInfo[kKeyId]};
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:NO inMethodType:kPOST];

}
// ---------------------------------------------------------------------------------------------------------------------------------
// showHideNotificationTable:

- (IBAction)showHideNotificationTable:(id)sender {
    if (!isClicked) {
        //[self.view addSubview:self.notificationsTable];
        self.notificationsTable.hidden = NO;
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGFloat height = 50 * notificationsList.count + 2;
                             if (height > 200) {
                                 height = 200;
                             }
                             self.notificationsTable.frame = CGRectMake(0, 40, self.view.bounds.size.width, height);
                             self.objtableView.frame = CGRectMake(0, self.notificationsTable.frame.size.height + self.notificationsTable.frame.origin.y - 4, self.objtableView.frame.size.width, self.objtableView.frame.size.height);
                         }];
        isClicked = YES;
        self.imgDropDown.image = [UIImage imageNamed:@"down.png"];

        // Reload data
        [self.notificationsTable reloadData];

        // Update notifications status
        for (NSUInteger i = 0; i < [notificationsList count]; i++) {
            NSDictionary *notifyDict = notificationsList[i];
            if ([notifyDict[kKeyReferenceStatus] integerValue] == 2 || [notifyDict[kKeyReferenceStatus] integerValue] == 3) {
                NSDictionary *param = @{kKeyStatus: @1};
                NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
            }
        }
    } else {
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.notificationsTable.frame = CGRectMake(0, 40, self.view.bounds.size.width, 0);
                             self.objtableView.frame = CGRectMake(0, self.btnNotifications.frame.origin.y + self.btnNotifications.frame.size.height, self.objtableView.frame.size.width, self.objtableView.frame.size.height);
                         } completion:^(BOOL finished) {
                    //[self.notificationsTable removeFromSuperview];
                    self.notificationsTable.hidden = YES;
                }];

        isClicked = NO;
        self.imgDropDown.image = [UIImage imageNamed:@"caret-down.png"];

        NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
        for (NSString *notiId in updatedNotiList) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", kKeyId, notiId];
            NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
            if (filterArr.count > 0) {
                [array removeObject:filterArr[0]];
            }
        }

        // Remove all objects
        [updatedNotiList removeAllObjects];
        notificationsList = array;
        if ([notificationsList count] == 0) {
            self.btnNotifications.hidden = YES;
            self.imgNatificationsBG.hidden = YES;
            self.imgDropDown.hidden = YES;
            self.notificationsTable.hidden = YES;

            self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// acceptFriendRequest:

- (void)acceptFriendRequest:(UIButton *)sender {
    NSInteger i = [sender tag];
    NSDictionary *notiDict = notificationsList[i];

    // Call notification API
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = server.loggedInUserInfo[kKeyId];
    params[kKeyConnectionID] = notiDict[kKeyReferenceID];
    params[kKeyConnectionStatus] = @kKeyConnectionAccept;

    NSString *urlStr = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, notiDict[kKeyReferenceID]];
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPUT];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// rejectFriendRequest:

- (void)rejectFriendRequest:(UIButton *)sender {
    NSInteger i = [sender tag];
    NSDictionary *notiDict = notificationsList[i];

    // Call notification API
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = server.loggedInUserInfo[kKeyId];
    params[kKeyConnectionID] = notiDict[kKeyReferenceID];
    params[kKeyConnectionStatus] = @kKeyConnectionReject;

    NSString *urlStr = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, notiDict[kKeyReferenceID]];
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPUT];
}

// ---------------------------------------------------------------------------------------
// actionOnProfileCellBtn:
- (void)actiononCellClick:(UITapGestureRecognizer *)gesture {
    NSInteger viewTag = gesture.view.tag;
    NSDictionary *userDict = connectionList[viewTag];
    NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionaryWithDictionary:userDict];
    mutatedDict[kKeyIsFromConnection] = @1;
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:mutatedDict];
    // For showind direct connection in degree of seperation view
    [server.friendProfileInfo setValue:@1 forKey:kKeyDegree];

    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:tabCon animated:YES];
}

- (void)actionOnProfileCellBtn:(UIButton *)sender {

    // Get profile
    NSDictionary *userDict = connectionList[sender.tag];
    NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionaryWithDictionary:userDict];
    mutatedDict[kKeyIsFromConnection] = @1;
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:mutatedDict];
    // For showind direct connection in degree of seperation view
    [server.friendProfileInfo setValue:@1 forKey:kKeyDegree];

    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:tabCon animated:YES];
}

// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTappedCaptured:(UIButton *)sender {
    NSMutableDictionary *userDict = connectionList[sender.tag];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:userDict server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

#pragma mark
#pragma mark Tap Gesture
#pragma mark

// ---------------------------------------------------------------------------------------
// singleTapGestureCaptured:

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)sender {
    UIButton *btn = (UIButton *) sender.view;
    NSDictionary *userDict = connectionList[btn.tag];
    NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionaryWithDictionary:userDict];
    mutatedDict[kKeyIsFromConnection] = @1;
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:mutatedDict];
    // For showind direct connection in degree of seperation view
    [server.friendProfileInfo setValue:@1 forKey:kKeyDegree];

    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:tabCon animated:YES];
}


#pragma mark
#pragma mark Private Methods
#pragma mark

- (NSDictionary *)updateDistance:(NSDictionary *)dict {
    __block NSDictionary *updatedDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    float distance = [[updatedDict valueForKey:kKeyDistance] floatValue];

    BOOL shouldCallMatrixAPI = NO;

    if ([distanceUnit isEqualToString:kkeyUnitMiles]) {
        distance = distance * 1.1508;
        [updatedDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 20) {

            shouldCallMatrixAPI = YES;

        }
    } else {
        distance = distance * 1.852;
        [updatedDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 32) {

            shouldCallMatrixAPI = YES;
        }
    }

    if (shouldCallMatrixAPI) {

        [_matrixRequestConnArray addObject:updatedDict];


    }

    return updatedDict;

}

#pragma mark GoogleMap Distance Matrics Methods

- (void)callDistanceAPI:(NSDictionary *)userDict completionBlock:(void (^)(NSDictionary *dict, NSError *error))completionBlock {
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];


    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];
    //        NSLog(@"urlStr::%@",urlStr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    request.timeoutInterval = 180;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                                  // NSLog(@"\n\nMATRIX API RESPONSE:%@ \n\n", json);

                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = rows[0];
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];

                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       double convertDist = [distanceWithoutCommas doubleValue];
                                       if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                           //meters to killometer
                                           convertDist = convertDist / 1000;
                                       } else {
                                           //meters to miles
                                           convertDist = (convertDist * 0.000621371192);
                                       }

                                       [userDict setValue:[NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit] forKey:kKeyDistance];
                                       __block NSDictionary *sourceUserDict = userDict;

                                       NSUInteger indexOfUserObject = [connectionList indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *_Nonnull stop) {

                                           if ([obj[kKeyId] isEqualToString:sourceUserDict[kKeyId]]) {
                                               return YES;
                                               *stop = YES;

                                           } else {

                                               return NO;

                                           }

                                       }];

                                       if (indexOfUserObject != NSNotFound && indexOfUserObject < connectionList.count) {

                                           connectionList[indexOfUserObject] = sourceUserDict;

                                       }


                                       NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldConnDataArray indexOfObjectPassingTest:^BOOL(myConnDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                                           if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                               return YES;
                                           } else {

                                               return NO;
                                           }

                                       }];

                                       if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldConnDataArray.count) {
                                           myConnDistanceClass *obj = [[myConnDistanceClass alloc] init];
                                           obj.dataId = userDict[kKeyId];
                                           NSDictionary *locationDict = userDict[kKeyLocation];
                                           obj.lat = locationDict[kKeyLattitude];
                                           obj.longitude = locationDict[kKeyRadarLong];
                                           obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit];
                                           obj.myLocation = server.myLocation;
                                           obj.userDict = userDict;

                                           (APP_DELEGATE).oldConnDataArray[indexOfOldDataObject] = obj;

                                       }

                                       dispatch_async(dispatch_get_main_queue(), ^{


                                           NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName ascending:YES];
                                           NSArray *sortDescriptors = @[sortDescriptor];
                                           NSArray *sortResults = [connectionList sortedArrayUsingDescriptors:sortDescriptors];
                                           connectionList = [NSMutableArray arrayWithArray:sortResults];


                                           // Reload table
                                           [self.objtableView reloadData];

                                       });

                                   } else {

                                       completionBlock(userDict, error);
                                   }
                               } else {

                                   completionBlock(userDict, error);
                               }
                           }];

}


- (void)callDistanceAPIForMatrixRequestArr {

    NSMutableArray *requestsArray = [[NSMutableArray alloc] init];
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    __block NSString *checkURL = @"";
    NSMutableArray *arrLocation = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSDictionary *userDict in _matrixRequestConnArray) {

        if (!isNullObject([userDict valueForKey:kKeyLocation])) {
            arrLocation[i] = userDict;
            CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            request.timeoutInterval = 180;
            [requestsArray addObject:request];
            i++;
        }

    }

    if (requestsArray.count) {
        __block NSInteger currentRequestIndex = 0;

        __weak typeof(self) weakSelf = self;

        _handler = ^void(NSURLResponse *response, NSData *data, NSError *error) {

            if (!error) {

                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

                if ([weakSelf IsValidResponse:data]) {
                    dict[@"data"] = data; //add graphhopper response

                    if (currentRequestIndex < weakSelf.matrixRequestConnArray.count) {
                        dict[@"userDict"] = weakSelf.matrixRequestConnArray[currentRequestIndex];
                        [weakSelf.matrixResponseConnArray addObject:dict];
                    }
                    currentRequestIndex++;
                    if (currentRequestIndex < [requestsArray count]) {
                        NSURLRequest *requ = requestsArray[currentRequestIndex];
                        NSString *requestPath = [[requ URL] absoluteString];
                        checkURL = requestPath;
                        [NSURLConnection sendAsynchronousRequest:requestsArray[currentRequestIndex]
                                                           queue:[NSOperationQueue mainQueue]
                                               completionHandler:weakSelf.handler];
                    } else {
                        [weakSelf MatrixAPICallCompleted];
                    }
                }
            }
        };
        NSURLRequest *requ = requestsArray[0];
        NSString *requestPath = [[requ URL] absoluteString];
        checkURL = requestPath;
        [NSURLConnection sendAsynchronousRequest:requestsArray[0]
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:weakSelf.handler];
    }
}

- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }

}


- (void)MatrixAPICallCompleted {

    [_matrixRequestConnArray removeAllObjects];
    if (_matrixResponseConnArray && _matrixResponseConnArray.count) {
        BOOL shouldReloadData = NO;

        for (NSDictionary *dict in _matrixResponseConnArray) {

            NSData *data = dict[@"data"];
            NSDictionary *userDict = dict[@"userDict"];
            NSError *error;
            Boolean isjsonValid = false;
            double convertDist = 0.0;
            if (data.length > 0) {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
              //  NSLog(@"\n\nMATRIX API RESPONSE:%@ \n\n", json);

                if ([json[@"paths"] count]) {
                    isjsonValid = true;
                    //parse data
                    NSDictionary *dictOSM = [json valueForKey:@"paths"];
                    NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                    NSString *distance = arrdistance[0];
                    convertDist = [distance doubleValue];
                    if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                        //meters to killometer
                        convertDist = convertDist / 1000;
                    } else {
                        //meters to miles
                        convertDist = (convertDist * 0.000621371192);
                    }
                } else {

                //    NSLog(@"Matrix JSON not ok");

                    NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldConnDataArray indexOfObjectPassingTest:^BOOL(myConnDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                        if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                            return YES;
                        } else {

                            return NO;
                        }

                    }];

                    if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldConnDataArray.count) {
                        [(APP_DELEGATE).oldConnDataArray removeObjectAtIndex:indexOfOldDataObject];
                    }
                }
            } else {
                isjsonValid = true;
                convertDist = [[userDict valueForKey:kKeyDistance] doubleValue];
            }
            if (isjsonValid) {
                [userDict setValue:[NSString stringWithFormat:@"%.2f", convertDist] forKey:kKeyDistance];

                NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldConnDataArray indexOfObjectPassingTest:^BOOL(myConnDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                    if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                        return YES;
                    } else {

                        return NO;
                    }

                }];

                if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldConnDataArray.count) {
                    myConnDistanceClass *obj = [[myConnDistanceClass alloc] init];
                    obj.dataId = userDict[kKeyId];
                    NSDictionary *locationDict = userDict[kKeyLocation];
                    obj.lat = locationDict[kKeyLattitude];
                    obj.longitude = locationDict[kKeyRadarLong];
                    obj.distance = [NSString stringWithFormat:@"%.2f", convertDist];
                    obj.myLocation = server.myLocation;
                    obj.userDict = userDict;

                    (APP_DELEGATE).oldConnDataArray[indexOfOldDataObject] = obj;

                }
                __block NSDictionary *sourceUserDict = userDict;

                NSUInteger indexOfUserObject = [connectionList indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *_Nonnull stop) {

                    if ([obj[kKeyId] isEqualToString:sourceUserDict[kKeyId]]) {
                        return YES;
                        *stop = YES;

                    } else {

                        return NO;

                    }

                }];

                if (indexOfUserObject != NSNotFound && indexOfUserObject < connectionList.count) {

                    connectionList[indexOfUserObject] = sourceUserDict;

                }
            }
        }

        if (shouldReloadData) {
            dispatch_async(dispatch_get_main_queue(), ^{

                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName ascending:YES];
                NSArray *sortDescriptors = @[sortDescriptor];
                NSArray *sortResults = [connectionList sortedArrayUsingDescriptors:sortDescriptors];
                connectionList = [NSMutableArray arrayWithArray:sortResults];


                // Reload table
                [self.objtableView reloadData];

            });

        } else {

         //   NSLog(@"All Matrix API failed");
        }

        [_matrixResponseConnArray removeAllObjects];
    }

}


#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = 0;
    if (tableView == self.notificationsTable) {
        count = notificationsList.count;
        // Return
        return count;
    } else {
//		count = connectionList.count;
        //jonish sprint 2
        if (_isShowInvisibleConnection == YES){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"is_invisible", @"1"];
            NSArray *dotsFilterArr = [connectionList filteredArrayUsingPredicate:predicate];
            connectionList = [dotsFilterArr mutableCopy];
        }
        //jonish sprint 2
        if (connectionList.count == 0) {

            self.lblNoData.frame = tableView.frame;
            tableView.backgroundView = self.lblNoData;
            return 0;

        } else {
            tableView.backgroundView = nil;
            count = connectionList.count;
            return count;
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// --------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"Cell1";

    if (tableView == self.notificationsTable) {
        SRConnectionNotificationCell *customCell = (SRConnectionNotificationCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionNotificationCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            customCell = (SRConnectionNotificationCell *) nibObjects[0];
        }
        // Assign values
        NSDictionary *notiDict = notificationsList[indexPath.section];
        NSDictionary *userDetails = notiDict[kKeySender];
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", userDetails[kKeyFirstName], userDetails[kKeyLastName]];

        if ([notiDict[kKeyReferenceStatus] isEqualToString:@"2"]) {
            CGRect frame = customCell.lblNotification.frame;
            frame.size.width = customCell.frame.size.width;
            customCell.lblNotification.frame = frame;

            customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has accepted your connection request ", fullName];
            customCell.btnYesNotification.hidden = YES;
            customCell.btnNoNotification.hidden = YES;
        } else if ([notiDict[kKeyReferenceStatus] isEqualToString:@"3"]) {
            CGRect frame = customCell.lblNotification.frame;
            frame.size.width = customCell.frame.size.width;
            customCell.lblNotification.frame = frame;

            customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has declined your connection request ", fullName];
            customCell.btnYesNotification.hidden = YES;
            customCell.btnNoNotification.hidden = YES;
        } else {
            customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has added you as a connection", fullName];
            customCell.btnYesNotification.hidden = NO;
            customCell.btnNoNotification.hidden = NO;
        }
        customCell.btnYesNotification.tag = indexPath.section;
        [customCell.btnYesNotification addTarget:self action:@selector(acceptFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
        customCell.btnNoNotification.tag = indexPath.section;
        [customCell.btnNoNotification addTarget:self action:@selector(rejectFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
        customCell.selectionStyle = UITableViewCellSelectionStyleNone;

        // Return
        return customCell;
    } else {
        static NSString *cellId = @"connectionCell";
        NSDictionary *userDict = connectionList[indexPath.section];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];

        // For the purposes of this demo, just return a random cell.
        SwipeableTableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[SwipeableTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.frame = self.view.bounds;
            cell.backgroundColor = [UIColor clearColor];

            // Tap Gesture
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
            singleTap.view.tag = indexPath.section;
            cell.tag = indexPath.section;
            NSLog(@"%ld", indexPath.section);

            //Block button
            UIButton *blockButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideLeft];
            blockButton.backgroundColor = [UIColor orangeColor];
            [blockButton setImage:[UIImage imageNamed:@"ic_cancel"] forState:UIControlStateNormal];
            [blockButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 25.0, 20.0, 15.0)];
            [blockButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -40.0, 5.0, 5.0)];
            [blockButton setTitle:[NSString stringWithFormat:@"Block %@", userDict[kKeyFirstName]] forState:UIControlStateNormal];
            blockButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            [blockButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            blockButton.tag = indexPath.section;
            [blockButton addTarget:self action:@selector(blockUserAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            //(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right)
            //////////Delete user
            UIButton *deletButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideLeft];
            deletButton.backgroundColor = [UIColor redColor];
            [deletButton setImage:[UIImage imageNamed:@"close-icon.png"] forState:UIControlStateNormal];
            [deletButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 30.0, 20.0, 15.0)];
            [deletButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -40.0, 5.0, 5.0)];
            [deletButton setTitle:[NSString stringWithFormat:@"Delete %@", userDict[kKeyFirstName]] forState:UIControlStateNormal];
            deletButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            [deletButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            deletButton.tag = indexPath.section;
            [deletButton addTarget:self action:@selector(deleteUserAction:) forControlEvents:UIControlEventTouchUpInside];
            
            

            //Ping button
            UIButton *pingsButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideRight];
            pingsButton.backgroundColor = [UIColor orangeColor];
            [pingsButton setImage:[UIImage imageNamed:@"mainmenu-pings.png"] forState:UIControlStateNormal];
            [pingsButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 20.0, 15.0, 15.0)];
            [pingsButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -40.0, 5.0, 5.0)];
//			[pingsButton setTitle:@"Ping" forState:UIControlStateNormal];
            [pingsButton setTitle:NSLocalizedString(@"lbl.Pings.txt", @"") forState:UIControlStateNormal];
            pingsButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            [pingsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            pingsButton.tag = indexPath.section;
            [pingsButton addTarget:self action:@selector(pingUserAction:) forControlEvents:UIControlEventTouchUpInside];

            //Favourite button
            UIButton *favButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideRight];
            favButton.backgroundColor = [UIColor orangeColor];
            [favButton setImage:[UIImage imageNamed:@"favorite-white.png"] forState:UIControlStateNormal];
            [favButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 20.0, 15.0, 15.0)];
            [favButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -45.0, 5.0, 5.0)];
            if ([userDict[kKeyIsFavourite] boolValue] == true) {
                [favButton setTitle:@"Unfavorite" forState:UIControlStateNormal];
            } else
                [favButton setTitle:@"Favorite" forState:UIControlStateNormal];
            favButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            [favButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            favButton.tag = indexPath.section;
            [favButton addTarget:self action:@selector(favouriteUserAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //Invisible button
            UIButton *invisibleBtn = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideRight];
            invisibleBtn.backgroundColor = [UIColor orangeColor];
            [invisibleBtn setImage:[UIImage imageNamed:@"invisible-white.png"] forState:UIControlStateNormal];
            [invisibleBtn setImageEdgeInsets:UIEdgeInsetsMake(5.0, 20.0, 15.0, 15.0)];
            [invisibleBtn setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -45.0, 5.0, 5.0)];
            if ([userDict[kKeyIsInvisible] boolValue] == true) {
                [invisibleBtn setTitle:@"Visible" forState:UIControlStateNormal];
            } else
                [invisibleBtn setTitle:@"Invisible" forState:UIControlStateNormal];
            invisibleBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            [invisibleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            invisibleBtn.tag = indexPath.section;
            [invisibleBtn addTarget:self action:@selector(invisibleUserAction:) forControlEvents:UIControlEventTouchUpInside];

            // Assign values

            SRConnectionCellView *contentView = (SRConnectionCellView *) cell.scrollViewContentView;
            contentView.delegate = self;
            contentView.lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];
            
            //            if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            //
            //                NSDictionary *profileDict = userDict[kKeyProfileImage];
            //                if (profileDict[kKeyImageName] != nil) {
            //                    NSString *imageName = profileDict[kKeyImageName];
            //                    if ([imageName length] > 0) {
            //                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
            //
            //                        [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            //                    } else
            //                        contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            //                } else
            //                    contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            //            } else
            //                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            
            if ([userDict[kKeySetting][@"profile_photo"]intValue] == 5 ) {
                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else if ([userDict[kKeySetting][@"profile_photo"]intValue] == 4 ) {
                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                if ([userDict[kKeyIsFavourite]intValue] == 1 ) {
                    
                    NSString* strProfileURL = [self getProfileImageURL:userDict];
                    if ([strProfileURL length] > 0){
                        [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
                    }else{
                        contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    }
                }
            } else {
                NSString* strProfileURL = [self getProfileImageURL:userDict];
                if ([strProfileURL length] > 0){
                    [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
                }else{
                    contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
            }

            if ([userDict[kKeyOccupation] length] > 0)
                contentView.lblProfession.text = userDict[kKeyOccupation];
            else
                contentView.lblProfession.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");
            //          }
            if ([contentView.lblProfession.text isEqualToString:@""]) {
                contentView.lblName.frame = CGRectMake(contentView.lblName.frame.origin.x, contentView.lblName.frame.origin.y + 15, contentView.lblName.frame.size.width, contentView.lblName.frame.size.height);
            }
            //Favourite
            contentView.favImg.layer.shadowOffset = CGSizeMake(0, 1);
            contentView.favImg.layer.shadowRadius = 1.0;
            contentView.favImg.layer.shadowOpacity = 1;
            contentView.favImg.layer.masksToBounds = NO;
            if ([userDict[kKeyIsFavourite] boolValue] == true) {
                contentView.favImg.hidden = FALSE;
            } else
                contentView.favImg.hidden = TRUE;

            //Invisible
            if ([userDict[kKeyIsInvisible] boolValue] == true) {
                contentView.btnInvisible.hidden = FALSE;
                [contentView.btnInvisible addTarget:self action:@selector(invisibleUserAction:) forControlEvents:UIControlEventTouchUpInside];
                contentView.btnInvisible.tag = indexPath.section;
                contentView.blackOverlay.hidden = FALSE;
            } else {
                contentView.btnInvisible.hidden = TRUE;
                contentView.blackOverlay.hidden = TRUE;

            }


            //Set Address and distance here

            // Location
            if (!isNullObject(userDict[kKeyLocation])) {

                if ([userDict[kKeyLocation] objectForKey:kKeyLiveAddress] && [[userDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 1) {
                    contentView.lblAddress.text = [userDict[kKeyLocation] objectForKey:kKeyLiveAddress];
                    [contentView.lblAddress sizeToFit];
                    if ((APP_DELEGATE).isOnChatList) {
                        contentView.lblDistance.hidden = TRUE;
                    } else {
                        if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                            if ([userDict[kKeyDistance] floatValue] > 32) {
                                contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@ %@", [userDict[kKeyDistance] floatValue], distanceUnit, @"approx"];
                            } else
                                contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@", [userDict[kKeyDistance] floatValue], distanceUnit];
                        } else {
                            if ([userDict[kKeyDistance] floatValue] > 20) {
                                contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@ %@", [userDict[kKeyDistance] floatValue], distanceUnit, @"approx"];
                            } else
                                contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@", [userDict[kKeyDistance] floatValue], distanceUnit];
                        }
                    }
                } else {
                    contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                    if ((APP_DELEGATE).isOnChatList) {
                        contentView.lblDistance.hidden = TRUE;
                    } else {
                        contentView.lblDistance.text = @"      N/A";
                        contentView.lblDistance.textAlignment = NSTextAlignmentCenter;
                    }
                }
            } else {
                contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                if ((APP_DELEGATE).isOnChatList) {
                    contentView.lblDistance.hidden = TRUE;
                } else {
                    contentView.lblDistance.text = @"      N/A";
                    contentView.lblDistance.textAlignment = NSTextAlignmentCenter;
                }
            }

            if ([contentView.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {
                if ((APP_DELEGATE).isOnChatList) {
                    contentView.lblDistance.hidden = TRUE;
                } else {
                    contentView.lblDistance.text = @"      N/A";
                    contentView.lblDistance.textAlignment = NSTextAlignmentCenter;
                }
            }
//            if ([[userDict objectForKey:kKeyFamily]isKindOfClass:[NSDictionary class]]) {
//                // NSDictionary *familyDict = [userDict objectForKey:kKeyFamily];
//                contentView.lblDating.hidden = YES;
//                contentView.imgDating.hidden = YES;
//                contentView.lblFamily.hidden = YES;
//            }else
            // Dating image
            if ([userDict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *settingDict = userDict[kKeySetting];
                contentView.lblFamily.hidden = YES;

                if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
                    contentView.lblDating.hidden = NO;
                    contentView.imgDating.hidden = NO;
                    contentView.imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
                } else {
                    contentView.lblDating.hidden = YES;
                    contentView.imgDating.hidden = YES;
                }
            } else {
                contentView.lblDating.hidden = YES;
                contentView.imgDating.hidden = YES;
                contentView.lblFamily.hidden = YES;
            }

            // Degree
            contentView.btnDegree.hidden = YES;
            contentView.btnGesture.hidden = FALSE;
            [contentView.btnGesture addGestureRecognizer:singleTap];
            contentView.btnGesture.tag = indexPath.section;
            singleTap.cancelsTouchesInView = NO;
            if ((APP_DELEGATE).isOnChatList) {
                contentView.btnCompass.hidden = TRUE;
                contentView.btnChat.hidden = TRUE;
                [contentView.btnCheckBox setHidden:FALSE];
                contentView.btnChat.tag = indexPath.section;
                [contentView.btnChat addTarget:self action:@selector(pingUserAction:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                contentView.btnCompass.hidden = FALSE;
                contentView.btnChat.hidden = TRUE;
                [contentView.btnCheckBox setHidden:TRUE];
                if ([userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
                    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                            server.myLocation.coordinate.longitude};
                    NSDictionary *locationDict = userDict[kKeyLocation];

                    CLLocationCoordinate2D userLoc = {[[locationDict valueForKey:kKeyLattitude] floatValue], [[locationDict valueForKey:kKeyRadarLong] floatValue]};

                    [contentView.btnCompass addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
                    contentView.btnCompass.tag = indexPath.section;

                    NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
                    if (directionValue == kKeyDirectionNorth) {
                        contentView.lblCompassDirection.text = @"N";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionEast) {
                        contentView.lblCompassDirection.text = @"E";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionSouth) {
                        contentView.lblCompassDirection.text = @"S";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionWest) {
                        contentView.lblCompassDirection.text = @"W";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionNorthEast) {
                        contentView.lblCompassDirection.text = @"NE";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionNorthWest) {
                        contentView.lblCompassDirection.text = @"NW";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionSouthEast) {
                        contentView.lblCompassDirection.text = @"SE";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
                    } else if (directionValue == kKeyDirectionSoutnWest) {
                        contentView.lblCompassDirection.text = @"SW";
                        [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
                    }
                }
            }
            // Tap for profile image
//            [contentView.btnProfileView addTarget:self action:@selector(actionOnProfileCellBtn:) forControlEvents:UIControlEventTouchUpInside];
//            contentView.btnProfileView.tag = indexPath.section;
//            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actiononCellClick:)];
//            [contentView addGestureRecognizer:tapGesture];
//            contentView.tag=indexPath.section;
            // Adjust green dot label appearence
            contentView.lblOnline.hidden = YES;
            CGRect frame = contentView.lblOnline.frame;

            if ([userDict[kKeyStatus] isEqualToString:@"1"]) {
                contentView.lblOnline.hidden = NO;
                // Adjust green dot label appearence
                contentView.lblOnline.layer.cornerRadius = contentView.lblOnline.frame.size.width / 2;
                contentView.lblOnline.layer.masksToBounds = YES;
                contentView.lblOnline.clipsToBounds = YES;

                contentView.lblName.frame = CGRectMake(frame.origin.x + 15, contentView.lblName.frame.origin.y, contentView.lblName.frame.size.width + 10, contentView.lblName.frame.size.height);

            } else {
                contentView.lblOnline.hidden = YES;
                contentView.lblName.frame = CGRectMake(frame.origin.x, contentView.lblName.frame.origin.y, contentView.lblName.frame.size.width + 10, contentView.lblName.frame.size.height);
            }
            [contentView.btnCheckBox setSelected:[arrSelectedUsers containsObject:@(indexPath.section)]];
            contentView.tag = indexPath.section;
        }
        [cell.scrollView setContentOffset:CGPointZero animated:YES];
        
        // Return
        return cell;
    }
}

-(NSString*) getProfileImageURL:(NSDictionary*) userDict{
    
    if  ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary self]])  {
        NSDictionary *profileDict = userDict[kKeyProfileImage];
        if (profileDict[kKeyImageName] != nil){
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                
                return imageUrl;
            }
        }
    }
    
    return @"";
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.notificationsTable) {
        return 50;
    } else {
        return 100;
    }
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat headerHeight = 1.0;
    if (section == 0) {
        headerHeight = 0.0;
    }

    // Return
    return headerHeight;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }

    // Return
    return headerView;
}

// --------------------------------------------------------------------------------
// willDisplayCell:

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove insets and margins from cells.
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// blockConnectionSucceed:

- (void)blockConnectionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *userInfo = [inNotify userInfo];

    NSMutableArray *mutatedArr = [NSMutableArray arrayWithArray:connectionList];
    NSDictionary *postInfo = userInfo[kKeyRequestedParams];

    BOOL isFound = NO;
    for (NSUInteger i = 0; i < [mutatedArr count] && !isFound; i++) {
        NSDictionary *userDict = mutatedArr[i];
        if ([userDict[kKeyId] isEqualToString:postInfo[kKeyConnectionID]]) {
            isFound = YES;
            [mutatedArr removeObject:userDict];
        }
    }

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName
                                                                   ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSArray *sortResults = [mutatedArr sortedArrayUsingDescriptors:sortDescriptors];
    connectionList = [NSMutableArray arrayWithArray:sortResults];

    // Reload table
    [self.objtableView reloadData];
}

// --------------------------------------------------------------------------------
// blockConnectionFailed:

- (void)blockConnectionFailed:(NSNotification *)inNotify {
}

- (void)deletBlockConnectionFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];
    [self.objtableView reloadData];
}
- (void)deletBlockConnectionSucceed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object] fromController:self];
    [self.objtableView reloadData];
}


// --------------------------------------------------------------------------------
// getConnectionSucceed:

- (void)getConnectionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if ((APP_DELEGATE).oldConnDataArray && (APP_DELEGATE).oldConnDataArray.count && connectionList.count) {
        if ([connectionList count] != [[inNotify object] count]) {
            connectionList = [inNotify object];
        } else {
            for (NSDictionary *userDict in [inNotify object]) {
                NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                NSArray *tempArr = [(APP_DELEGATE).oldConnDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                if (tempArr && tempArr.count) {
                    myConnDistanceClass *distanceObj = [tempArr firstObject];
                    NSDictionary *currentLocationDict = userDict[kKeyLocation];
                    NSDictionary *previousLocationDict = distanceObj.userDict[kKeyLocation];
                    BOOL isBLocationChanged = NO;
                    BOOL isALocationChanged = NO;
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *previousUpdateDate = [dateFormatter dateFromString:previousLocationDict[kKeyLastSeenDate]];
                    NSDate *currentDate = [dateFormatter dateFromString:currentLocationDict[kKeyLastSeenDate]];
                    if ([previousUpdateDate compare:currentDate] != NSOrderedSame) {
                        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:[[currentLocationDict valueForKey:kKeyLattitude] floatValue] longitude:[[currentLocationDict valueForKey:kKeyRadarLong] floatValue]];
                        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[previousLocationDict valueForKey:kKeyLattitude] floatValue] longitude:[[previousLocationDict valueForKey:kKeyRadarLong] floatValue]];
                        CLLocationDistance userBdistance = [toLoc distanceFromLocation:fromLoc];
                        if (userBdistance >= DISTANCEFILTER) {
                            isBLocationChanged = YES;
//                            NSLog(@"id: %@ location changed", userDict[kKeyId]);
//                            NSLog(@"Previous Lat: %@ Current Lat:%@", distanceObj.lat, currentLocationDict[kKeyLattitude]);
//                            NSLog(@"Previous Long: %@ Current Long:%@", distanceObj.longitude, currentLocationDict[kKeyRadarLong]);
                        }
                    }
                    CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];
                    if (distance >= DISTANCEFILTER) {
                        isALocationChanged = YES;
                    }
                    if (isBLocationChanged || isALocationChanged) {
                        [arr addObject:userDict];
                    }
                } else {
                    [arr addObject:userDict];
                    if ([userDict valueForKey:kKeyLocation] != (id) [NSNull null]) {
                        [(APP_DELEGATE).oldConnDataArray addObject:[myConnDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
                    }
                }
            }
        }
    } else {
        [(APP_DELEGATE).oldConnDataArray removeAllObjects];
        [arr addObjectsFromArray:[inNotify object]];
        for (NSDictionary *userDict in [inNotify object]) {
            if ([userDict valueForKey:kKeyLocation] != (id) [NSNull null]) {
                [(APP_DELEGATE).oldConnDataArray addObject:[myConnDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
            }
        }
    }
    if (arr && arr.count) {
       // NSLog(@"arr count: %lu", (unsigned long) arr.count);
        for (NSDictionary *dict in arr) {
            __block NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
            NSDictionary *distanceDict = [self updateDistance:updatedDict];
          //  NSLog(@"id:%d", [distanceDict[kKeyId] intValue]);
            updatedDict = distanceDict;
            NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldConnDataArray indexOfObjectPassingTest:^BOOL(myConnDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([anotherObj.dataId isEqualToString:updatedDict[kKeyId]]) {
                    return YES;
                } else {
                    return NO;
                }
            }];
            if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldConnDataArray.count) {
                if ([updatedDict valueForKey:kKeyLocation] != (id) [NSNull null]) {
                    myConnDistanceClass *obj = [myConnDistanceClass getObjectFromDict:updatedDict myLocation:server.myLocation];
                    (APP_DELEGATE).oldConnDataArray[indexOfOldDataObject] = obj;
                }
            }
            NSUInteger indexOfObject = [connectionList indexOfObjectPassingTest:^BOOL(NSDictionary *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([anotherObj[kKeyId] isEqualToString:updatedDict[kKeyId]]) {
                    return YES;
                } else {
                    return NO;
                }
            }];
            if (indexOfObject != NSNotFound && connectionList && connectionList.count && indexOfObject < connectionList.count) {
                connectionList[indexOfObject] = updatedDict;
            } else {
                [connectionList addObject:updatedDict];
            }
        }
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSArray *sortResults = [connectionList sortedArrayUsingDescriptors:sortDescriptors];
    connectionList = [NSMutableArray arrayWithArray:sortResults];

    if (connectionList.count <= 0) {
        NSLocalizedString(@"lbl.nodata.txt", @"");
    }
    // Reload table
    [self.objtableView reloadData];
    [self callDistanceAPIForMatrixRequestArr];
}

// --------------------------------------------------------------------------------
// getConnectionFailed:

- (void)getConnectionFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];
    [APP_DELEGATE hideActivityIndicator];
    [self.objtableView reloadData];
    self.lblNoData.text = NSLocalizedString(@"lbl.noConnectionFnd.txt", @"");
}

// --------------------------------------------------------------------------------
// updateConnectionSucceed:

- (void)updateConnectionSucceed:(NSNotification *)inNotify {

    NSDictionary *userInfo = [inNotify userInfo];
    NSDictionary *postInfo = userInfo[kKeyRequestedParams];
    if ([postInfo[kKeyConnectionStatus] integerValue] == kKeyConnectionBlock) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];

        NSMutableArray *mutatedArr = [NSMutableArray arrayWithArray:connectionList];
        NSDictionary *postInfo = userInfo[kKeyRequestedParams];

        BOOL isFound = NO;
        for (NSUInteger i = 0; i < [mutatedArr count] && !isFound; i++) {
            NSDictionary *userDict = mutatedArr[i];
            if ([userDict[kKeyId] isEqualToString:postInfo[kKeyConnectionID]]) {
                isFound = YES;
                [mutatedArr removeObject:userDict];
            }
        }

        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        NSArray *sortResults = [mutatedArr sortedArrayUsingDescriptors:sortDescriptors];
        connectionList = [NSMutableArray arrayWithArray:sortResults];

        // Reload table
        [self.objtableView reloadData];

    } else if ([postInfo[kKeyConnectionStatus] integerValue] == kKeyConnectionReject) {
        NSDictionary *param = @{kKeyStatus: @1};
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyReferenceID, postInfo[kKeyConnectionID]];
        NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];

        NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
        if (array.count > 0) {
            [array removeObject:filterArr[0]];
        }
        notificationsList = array;
        NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [notificationsList count]]];
        [self.btnNotifications setTitle:str forState:UIControlStateNormal];

        if ([notificationsList count] == 0) {
            [self showHideNotificationTable:nil];
        }
        [self.notificationsTable reloadData];

        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, [filterArr[0] objectForKey:kKeyId]];
        [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];

        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassAddConnection, postInfo[kKeyConnectionID]];
        [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
    } else if ([postInfo[kKeyConnectionStatus] integerValue] == kKeyConnectionAccept) {
        NSDictionary *param = @{kKeyStatus: @1};
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyReferenceID, postInfo[kKeyConnectionID]];
        NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];

        NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
        if (array.count > 0) {
            [array removeObject:filterArr[0]];
        }
        notificationsList = array;
        NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [notificationsList count]]];
        [self.btnNotifications setTitle:str forState:UIControlStateNormal];

        if ([notificationsList count] == 0) {
            [self showHideNotificationTable:nil];
        }
        [self.notificationsTable reloadData];

        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, [filterArr[0] objectForKey:kKeyId]];
        [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];

        [server makeAsychronousRequest:kKeyClassGetConnection inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    }

    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassGetNotification, server.loggedInUserInfo[kKeyId]];
    [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

// --------------------------------------------------------------------------------
// updateConnectionFailed:

- (void)updateConnectionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.connection_update_fail.txt", @"")];
}

// --------------------------------------------------------------------------------
// getNotificationSucceed:

- (void)getNotificationSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    if (!isClicked) {
        notificationsList = server.notificationArr;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"1"];
        notificationsList = [notificationsList filteredArrayUsingPredicate:predicate];

        if ([notificationsList count] == 0) {
            self.btnNotifications.hidden = YES;
            self.imgNatificationsBG.hidden = YES;
            self.imgDropDown.hidden = YES;

            self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.notificationsTable reloadData];
            self.notificationsTable.hidden = YES;
        } else {
            self.btnNotifications.hidden = NO;
            self.imgNatificationsBG.hidden = NO;
            self.imgDropDown.hidden = NO;

            self.objtableView.frame = CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height - 40);

            // Set Title to Notification Button
            NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [notificationsList count]]];
            [self.btnNotifications setTitle:str forState:UIControlStateNormal];
        }
    }
}

// --------------------------------------------------------------------------------
// getNotificationFailed:

- (void)getNotificationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// updateNotificationSucceed:

- (void)updateNotificationSucceed:(NSNotification *)inNotify {
    NSDictionary *userInfo = [inNotify userInfo];
    NSString *inObjectUrl = userInfo[kKeyObjectUrl];
    NSArray *componentArr = [inObjectUrl componentsSeparatedByString:@"/"];
    [updatedNotiList addObject:componentArr[1]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, componentArr[1]];
    NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];

    NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
    if (array.count > 0) {
        if (filterArr.count > 0) {
            [array removeObject:filterArr[0]];
        }

    }
    NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [array count]]];
    [self.btnNotifications setTitle:str forState:UIControlStateNormal];
    [self.objtableView reloadData];

    // Update notification count in menu view
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdatedNotificationCount object:componentArr[1] userInfo:nil];
}

// --------------------------------------------------------------------------------
// updateNotificationFailed:

- (void)updateNotificationFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
//online status change
- (void)onlineStatusChanged {
    [self.objtableView reloadData];
}

- (void)addUserInFavouriteSucceed:(NSNotification *)inNotify {
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE hideActivityIndicator];
    //    [SRModalClass showAlert:inNotify.object];
    if (connectionList.count > 0) {
        NSMutableDictionary *favDict = connectionList[favTag];
        if ([inNotify.object isEqualToString:@"favourite false"]) {
            [favDict setValue:@false forKey:kKeyIsFavourite];
            [SRModalClass showAlertWithTitle:@"Successfully remove from favorite" alertTitle:@"Success"];
        } else{
            [favDict setValue:@true forKey:kKeyIsFavourite];
            [SRModalClass showAlertWithTitle:@"Successfully add to favorite" alertTitle:@"Success"];
        }
            
        connectionList[favTag] = favDict;
        [self.objtableView reloadSections:[NSIndexSet indexSetWithIndex:favTag] withRowAnimation:UITableViewRowAnimationNone];
    }
}

// --------------------------------------------------------------------------------
// addUserInFavouriteFailed:

- (void)addUserInFavouriteFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:inNotify.object];
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// addUserInvisibleSucceed:

- (void)addUserInvisibleSucceed:(NSNotification *)inNotify {
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE hideActivityIndicator];
    //    [SRModalClass showAlert:inNotify.object];
    if (connectionList.count > 0) {
        NSMutableDictionary *invisibleDict = connectionList[invisibleTag];
        if ([inNotify.object isEqualToString:@"invisible false"]) {
            [invisibleDict setValue:@false forKey:kKeyIsInvisible];
        } else{
            [invisibleDict setValue:@true forKey:kKeyIsInvisible];
        }
        connectionList[invisibleTag] = invisibleDict;
        //jonish sprint 2
        [self.objtableView reloadData];
        //[self.objtableView reloadSections:[NSIndexSet indexSetWithIndex:invisibleTag] withRowAnimation:UITableViewRowAnimationNone];
        //jonish sprint 2
    }
}

// --------------------------------------------------------------------------------
// addUserInvisibleFailed:

- (void)addUserInvisibleFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:inNotify.object];
    [APP_DELEGATE hideActivityIndicator];
}


-(void)viewDidClickedCheckBox:(SRConnectionCellView *)view {
    if (view.btnCheckBox.isSelected) {
        [arrSelectedUsers addObject:@(view.tag)];
        
    } else {
        if ([arrSelectedUsers containsObject:@(view.tag)]) {
            [arrSelectedUsers removeObject:@(view.tag)];
        }
    }
    [self updateRightBarButton];
    [self.objtableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:view.tag]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)updateRightBarButton {
    if (arrSelectedUsers.count > 0) {
        UIImageView *btnRight = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [btnRight setImage:[UIImage imageNamed:@"group-add-orange.png"]];
        [btnRight setContentMode:UIViewContentModeScaleAspectFit];
        
        UITapGestureRecognizer *tapOn = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(createGroup)];
        [btnRight addGestureRecognizer:tapOn];
        
        UIBarButtonItem *rightBar = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
        [rightBar setTintColor:[UIColor colorWithRed:250/255.0 green:150/255.0 blue:0 alpha:1.0]];
        
        self.navigationItem.rightBarButtonItems = @[rightBar];
        
    } else {
        UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
        [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}


-(void)createGroup {
    NSString* groupName = @""; //server.loggedInUserInfo[kKeyFirstName]
    //Add your name in group name
//    if (((NSString*)server.loggedInUserInfo[kKeyLastName]).length > 0) {
//        groupName = [groupName stringByAppendingFormat:@" %@",server.loggedInUserInfo[kKeyLastName]];
//    }
//    groupName = [groupName stringByAppendingFormat:@","];
    
    NSString* memberIDs = @"";
    for (int i = 0; i < arrSelectedUsers.count; i++) {
        NSNumber* index = [arrSelectedUsers objectAtIndex:i];
        NSDictionary *userDict = connectionList[index.integerValue];
        groupName = [groupName stringByAppendingFormat:@"%@",userDict[kKeyFirstName]];
        if (((NSString*)userDict[kKeyLastName]).length > 0) {
            groupName = [groupName stringByAppendingFormat:@" %@",userDict[kKeyLastName]];
        }
        memberIDs = [memberIDs stringByAppendingFormat:@"%@",userDict[kKeyId]];
        
        if (i != arrSelectedUsers.count - 1) {
            groupName = [groupName stringByAppendingFormat:@","];
            memberIDs = [memberIDs stringByAppendingFormat:@","];
        }
    }
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyName] = groupName;
    params[kKeyDescription] = @"";
    params[kKeyAddress] = @"";
    params[kKeyGroupTag] = @"";
    params[kKeyLattitude] = [NSString stringWithFormat:@"%f", (APP_DELEGATE).lastLocation.coordinate.latitude];
    params[kKeyRadarLong] = [NSString stringWithFormat:@"%f", (APP_DELEGATE).lastLocation.coordinate.longitude];
    
    params[kKeyGroupType] = @"1";
    params[kKeyEnableTracking] = @"1";
    params[kKeyGroup_is] = @"1";
    
    params[kKeyGroup_Member_Ids] = memberIDs;
    // Create group for xmpp chat
    [server asychronousRequestWithData:params imageData:nil forKey:kKeyMediaImage toClassType:kKeyClassCreateGroup inMethodType:kPOST];
}

- (void)CreateGroupSucceed:(NSNotification *)inNotify {
    // Group created successfully
    NSDictionary *dictionary = [inNotify object];
    if (dictionary[kKeyGroupList] == nil) {
        (APP_DELEGATE).server.groupDetailInfo = [inNotify object];

        // Create group for xmpp chat
        NSDictionary *groupDict = [inNotify object];
        NSMutableDictionary *groupMutatedDict = [NSMutableDictionary dictionaryWithDictionary:groupDict];

        NSMutableArray *groupList = [NSMutableArray array];
        for (NSDictionary *userdict in groupDict[kKeyGroupMembers]) {
            NSDictionary *userDetails = userdict[kKeyUser];

            NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionary];
            [mutatedDict setValue:userDetails[kKeyId] forKey:kKeyId];
            [mutatedDict setValue:userDetails[kKeyFirstName] forKey:kKeyFirstName];
            [mutatedDict setValue:userDetails[kKeyLastName] forKey:kKeyLastName];
            if ([userDetails[kKeyProfileImage] isKindOfClass:[NSDictionary class]] && [userDetails[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                [mutatedDict setValue:[userDetails[kKeyProfileImage] objectForKey:kKeyImageName] forKey:kKeyProfileImage];
            }
            [groupList addObject:mutatedDict];
        }
        [groupMutatedDict removeObjectForKey:kKeyGroupMembers];
        
        NSString *imageName = @"";
        if ([dictionary[@"image"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dictImage = dictionary[@"image"];
            imageName = dictImage[@"file"];
        }
        //group created successfully.
        [self openGroupDetailController:(APP_DELEGATE).server.groupDetailInfo];
    }
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
}

- (void)CreateGroupFailed:(NSNotification *)inNotify {
    [(APP_DELEGATE) hideActivityIndicator];
    [SRModalClass showAlert:[NSString stringWithFormat:@"%@", [inNotify object]]];
}

-(void)openGroupDetailController:(NSDictionary*)groupDict {
    [self.view endEditing:YES];
    arrSelectedUsers = [NSMutableArray new];
    [self.objtableView reloadData];
    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:groupDict];
    if ([groupDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [groupDict[kKeyMediaImage] objectForKey:kKeyImageName]];
//        NSArray *dotsFilterArr = [dotsGroupPicsArr filteredArrayUsingPredicate:predicate];

//        if ([dotsFilterArr count] > 0) {
//            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
//        }
    }
    [dictionary setObject:[NSString stringWithFormat:@"1"] forKey:@"isGroup"];
    server.groupDetailInfo = [SRModalClass removeNullValuesFromDict:dictionary];
    
//    SRGroupDetailTabViewController *objGroupTab = [[SRGroupDetailTabViewController alloc] initWithNibName:nil bundle:nil];
    SRChatViewController *objSRPingsView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:(APP_DELEGATE).server.groupDetailInfo];
    [self.navigationController pushViewController:objSRPingsView animated:YES];
}


@end
