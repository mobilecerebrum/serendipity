#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "SRChatViewController.h"
#import "SRConnectionCellView.h"
#import "SRGroupDetailTabViewController.h"

// Custom Delegate to pass Data to SRNewEventViewController
@protocol SRNewEventViewController <NSObject>
- (void)dataFromSRConnectionViewController:(NSMutableArray *)data;
@end

@interface SRMyConnectionsViewController : ParentViewController<UITableViewDataSource, UITableViewDelegate, SRConnectionCellViewProtocol>
{
    // Instance variable
    NSMutableArray *connectionList;
    NSArray *notificationsList;
    NSMutableArray *cacheImgArr;
    NSMutableArray *membersArray;
    NSMutableArray *searchedUsersArr;
    NSMutableArray *arrSelectedUsers;
   
    CGFloat lblOldHeight;
    CGFloat lblNewHeight;
    NSInteger favTag,invisibleTag;
    NSMutableArray *updatedNotiList;
    
    NSString *distanceUnit;
    // Notification view
    BOOL isClicked;
    BOOL isOnce;
    BOOL isNotificationUpdated;
    BOOL isSelected;

    // Server
    SRServerConnection *server;
    UIImageView *overlayImgView;
    NSOperationQueue *searchQueue;
    BOOL isSearching;
}

// Properties
@property (strong,nonatomic) IBOutlet SRProfileImageView *profileImg;
@property (strong,nonatomic) IBOutlet UITableView *objtableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgNatificationsBG;
@property (weak, nonatomic) IBOutlet UIButton *btnNotifications;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropDown;
@property (strong,nonatomic) IBOutlet UITableView *notificationsTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

// Other
//@property (strong, nonatomic) UITableView *notificationsTable;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (strong, nonatomic) UILabel *lblNoData;


//For Google Matrix API call and response
@property (nonatomic,strong) __block NSMutableArray *matrixRequestConnArray;
@property (nonatomic,strong) __block NSMutableArray *matrixResponseConnArray;
@property (nonatomic,copy) void (^handler)(NSURLResponse *response, NSData *data, NSError *error);

@property (weak) id delegate;
@property BOOL isShowInvisibleConnection;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)showHideNotificationTable:(id)sender;

@end
