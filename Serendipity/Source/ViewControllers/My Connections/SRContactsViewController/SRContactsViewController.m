#import "SRNewGroupViewController.h"
#import "SRContactsViewController.h"
#import "SRAddConnectionTableViewCell.h"
#import "SRChatViewController.h"
#import "SRNotifyMeViewController.h"
#import "SRConnectionNotificationCell.h"
#import "NotifyMeVC.h"

@implementation SRContactsViewController


#pragma mark Init Method


// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    if ([nibNameOrNil isEqualToString:@"fromMenuView"]) {
        isFromMenu = YES;
    } else
        isFromMenu = FALSE;
    self = [super initWithNibName:@"SRContactsViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        server = inServer;

        // Register notification
        [[NSNotificationCenter defaultCenter] removeObserver:self];

        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];

        [defaultCenter addObserver:self
                          selector:@selector(contactUploadSuccessNotifier:)
                              name:kKeyNotificationContactUploadSucceed
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(contactUploadFailedNotifier:)
                              name:kKeyNotificationContactUploadFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(inviteUserSucceed:)
                              name:kKeyNotificationInviteUserSucceed object:nil];

        [defaultCenter addObserver:self selector:@selector(inviteUserFailed:) name:kKeyNotificationInviteUserFailed object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(updateNotificationSucceed:)
                              name:kKeyContactNotificationUpdateNotificationSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateNotificationFailed:)
                              name:kKeyContactNotificationUpdateNotificationFailed object:nil];
        
        
//        [defaultCenter addObserver:self selector:@selector(contactDeleteSuccessNotifier:) name:kKeyNotificationContactDeleteSucceed object:nil];
//        [defaultCenter addObserver:self selector:@selector(contactBlockSuccessNotifier:) name:kKeyNotificationContactBlockSucceed object:nil];
//        [defaultCenter addObserver:self selector:@selector(contactBlockFaildNotifier:) name:kKeyNotificationContactBlockFailed object:nil];
//        [defaultCenter addObserver:self selector:@selector(contactDeleteFaildNotifier:) name:kKeyNotificationContactDeleteFailed object:nil];
        
        
    }

    // Return
    return self;
}


#pragma mark Statndard Method


// ---------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 13.0, *)) {
        self->searchTextField = self.searchBar.searchTextField;
        self->searchTextField.backgroundColor = [UIColor whiteColor];
        self->searchTextField.textColor = [UIColor grayColor];
    } else {
        self->searchTextField = [_searchBar valueForKey:@"searchField"];
        self->searchTextField.backgroundColor = [UIColor whiteColor];
        self->searchTextField.textColor = [UIColor grayColor];
    }

    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver:self
                      selector:@selector(getSocialConatctUsersSucceed:)
                          name:kKeyNotificationUserSocialContactGetSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getSocialConatctUsersFailed:)
                          name:kKeyNotificationUserSocialContactGetFailed object:nil];
    

    
    [defaultCenter addObserver:self
    selector:@selector(deletBlockConnectionSucceed:)
        name:kKeyNotificationGetDeleteBlockConnectionSucceed object:nil];
    [defaultCenter addObserver:self
    selector:@selector(deletBlockConnectionFailed:)
        name:kKeyNotificationGettDeleteBlockConnectionFailed object:nil];
    
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.MyContacts.view", "") forViewNavCon:self.navigationItem forFontSize:15];

    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    if (!isFromMenu) {
        CGRect frame;
        frame = self.friendsTable.frame;
        frame.size.height = frame.size.height - 46;
        self.friendsTable.frame = frame;
    }
    
    if (isFromMenu) {
        UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
        [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // Add subview for add new connection
           self.viewAddMember.layer.cornerRadius = 5;
           self.viewAddMember.layer.masksToBounds = YES;
           [self.view addSubview:self.viewAddMember];
           self.viewAddMember.hidden = YES;
        self.txtCountryName.delegate = self;
        self.txtName.delegate = self;
        self.txtPhoneNumber.delegate = self;

        UITapGestureRecognizer *singleFingerTap =
          [[UITapGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(handleSingleTap:)];
        [self.viewAddMember addGestureRecognizer:singleFingerTap];

    }
    
    
    //TODO: Dummy Data
    self.friendListSection = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];

    //call method to get contact from address book (Check here to not refresh webservice every time)
//    if (server.contactList.count == 0) {
//        [self btnActionGetAllContacts];
//    } else {
//        ContactNumberList = server.contactList;
//        importContactArray = server.importContactArr;
//    }
  
    [self getUserPhoneContact];
    
    //tableview section index background
    if ([self.friendsTable respondsToSelector:@selector(setSectionIndexColor:)]) {
        self.friendsTable.sectionIndexColor = [UIColor whiteColor];
        self.friendsTable.sectionIndexBackgroundColor = [UIColor blackColor];
    }


    //code to set inside backgroundcolor of searchbar
//    for (UIView *subview in [[self.searchBar.subviews lastObject] subviews]) {
//        if ([subview isKindOfClass:[UITextField class]]) {
//            UITextField *textField = (UITextField *) subview;
//            [textField setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
//            textField.textColor = [UIColor whiteColor];
//        }
//    }
//    [(APP_DELEGATE) showActivityIndicator];
//    [[SRAPIManager sharedInstance] getConnectionsWithStatuses:@"1,2,3,4,5" completion:^(id  _Nonnull result, NSError * _Nonnull error) {
//        if (!error) {
//            importedConnections = [NSMutableArray new];
//            for (NSDictionary *user in [result objectForKey:@"response"]) {
//                [importedConnections addObject:user];//[user objectForKey:@"mobile_number"]
//                //[server.contactList addObject:[user objectForKey:@"mobile_number"]];
//            }
//
//            [self btnActionGetAllContacts];
//            [self.friendsTable reloadData];
//        }
//        [APP_DELEGATE hideActivityIndicator];
//    }];
    
    _searchBar.delegate = self;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self notificationViewShow];
    //NavigationBar button
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 20)];
    [rightButton setTitle:NSLocalizedString(@"nav.bar.refresh", @"") forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(getUserPhoneContact) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaRegular size:14];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.tabBarController.navigationItem.rightBarButtonItem = button;
}

- (void) initializeSearchController {
}

-(void)notificationViewShow{
    if (!isOnce) {
    isOnce = YES;
    notificationsList = server.notificationArr;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"17"];
    notificationsList = [notificationsList filteredArrayUsingPredicate:predicate];
    
    if ([notificationsList count] == 0) {
        self.btnNotifications.hidden = YES;
        self.imgNatificationsBG.hidden = YES;
        self.imgDropDown.hidden = YES;
        self.notificationsTable.hidden = YES;
        // Here hieght need to 0
        _heightNotification.constant = 0;
        self.friendsTable.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } else {
        _heightNotification.constant = 40;
        self.notificationsTable.frame = CGRectMake(0, 84, self.view.frame.size.width, 0);
        self.notificationsTable.hidden = YES;
        
        // Set Title to Notification Button
        NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [notificationsList count]]];
        [self.btnNotifications setTitle:str forState:UIControlStateNormal];
    }
    }
}

- (IBAction)showHideNotificationTable:(id)sender {
    if (!isClicked) {
        //[self.view addSubview:self.notificationsTable];
        self.notificationsTable.hidden = NO;
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGFloat height = 50 * notificationsList.count + 2;
                             if (height > 200) {
                                 height = 200;
                             }
                             self.notificationsTable.frame = CGRectMake(0, 84, self.view.bounds.size.width, height);
                             self.friendsTable.frame = CGRectMake(0, height + self.notificationsTable.frame.origin.y - 4, self.friendsTable.frame.size.width, self.friendsTable.frame.size.height);
                         }];
        isClicked = YES;
        self.imgDropDown.image = [UIImage imageNamed:@"down.png"];

        // Reload data
        [self.notificationsTable reloadData];

        // Update notifications status
        for (NSUInteger i = 0; i < [notificationsList count]; i++) {
            NSDictionary *notifyDict = notificationsList[i];
            if ([notifyDict[kKeyReferenceStatus] integerValue] == 2 || [notifyDict[kKeyReferenceStatus] integerValue] == 3) {
                NSDictionary *param = @{kKeyStatus: @1};
                NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
            }
        }
    } else {
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.notificationsTable.frame = CGRectMake(0, 44, self.view.bounds.size.width, 0);
                             self.friendsTable.frame = CGRectMake(0, self.btnNotifications.frame.origin.y + self.btnNotifications.frame.size.height, self.friendsTable.frame.size.width, self.friendsTable.frame.size.height);
                         } completion:^(BOOL finished) {
                    //[self.notificationsTable removeFromSuperview];
                    self.notificationsTable.hidden = YES;
                }];

        isClicked = NO;
        self.imgDropDown.image = [UIImage imageNamed:@"caret-down.png"];

        NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
        for (NSString *notiId in updatedNotiList) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", kKeyId, notiId];
            NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
            if (filterArr.count > 0) {
                [array removeObject:filterArr[0]];
            }
        }

        // Remove all objects
        [updatedNotiList removeAllObjects];
        notificationsList = array;
        if ([notificationsList count] == 0) {
            self.btnNotifications.hidden = YES;
            self.imgNatificationsBG.hidden = YES;
            self.imgDropDown.hidden = YES;
            self.notificationsTable.hidden = YES;

            self.friendsTable.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
}

#pragma mark Action Method


// ---------------------------------------------------------------------------------------
// actionOnBack:
- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

//-----------------------------------------------------------------------------------------
// btnActionGetAllContacts:

- (void)btnActionGetAllContacts {
    //ABAddressBookRef m_addressbook = ABAddressBookCreate();

    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    // ABAddressBookRef addressBook = ABAddressBookCreate();

    __block BOOL accessGranted = NO;

    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });

        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    } else {
        // We are on iOS 5 or Older
        accessGranted = YES;
        [self getContactsWithAddressBook:addressBook];
    }
    if (accessGranted) {
        [self getContactsWithAddressBook:addressBook];
    }

    // Send contact list to server
    // WHY WE NEED IT?
//    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
//    if (contactList.count > 0) {
//        [dataDict setValue:contactList forKey:kKeyGroupUsers];
//        [dataDict setValue:kKeyContacts forKey:kKeyType];
//        [server makeAsychronousRequest:kKeyClassUploadContacts inParams:dataDict isIndicatorRequired:YES inMethodType:kPOST];
//    }
}

//-----------------------------------------------------------------------------------------------------------------------
// getContactsWithAddressBook:

- (void)getContactsWithAddressBook:(ABAddressBookRef)addressBook {
    contactList = [[NSMutableArray alloc] init];
    ContactNumberList = [[NSMutableArray alloc] init];
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);

    for (int i = 0; i < nPeople; i++) {
        NSMutableDictionary *dOfPerson = [NSMutableDictionary dictionary];
        NSMutableDictionary *listPerson = [NSMutableDictionary dictionary];

        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople, i);

        //For username and surname
        NSString *firstNameStr = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonFirstNameProperty) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonFirstNameProperty);

        if (firstNameStr.length > 0) {
            dOfPerson[kKeyFirstName] = firstNameStr;
        } else {
            dOfPerson[kKeyFirstName] = @"";
        }

        NSString *lastNameStr = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonLastNameProperty) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonLastNameProperty);

        if (lastNameStr.length > 0) {
            dOfPerson[kKeyLastName] = lastNameStr;
        } else
            dOfPerson[kKeyLastName] = @"";

        NSString *fullNameStr = [[[NSString stringWithFormat:@"%@", firstNameStr] stringByAppendingString:@" "] stringByAppendingString:[NSString stringWithFormat:@"%@", lastNameStr]];


        //For Email ids
        ABMutableMultiValueRef eMail = ABRecordCopyValue(ref, kABPersonEmailProperty);
        if (ABMultiValueGetCount(eMail) > 0) {
            dOfPerson[kKeyEmail] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(eMail, 0);

            [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(eMail, 0) forKey:kKeyEmail];
        }
        CFRelease(eMail);

        //For Phone number List
        ABMultiValueRef phones = (__bridge ABMultiValueRef) ((__bridge NSString *) ABRecordCopyValue(ref, kABPersonPhoneProperty));

        NSString *mobileLabel;
        for (CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
            mobileLabel = (__bridge_transfer NSString *) ABMultiValueCopyLabelAtIndex(phones, i);
            if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneMobileLabel]) {
                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);

                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
            } else if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneIPhoneLabel]) {
                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);

                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
            }
        }
        CFRelease(phones);
        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil) {
            NSString *mobileNo = [SRModalClass RemoveSpecialCharacters:[dOfPerson valueForKey:kKeyMobileNumber]];
            if (mobileNo.length > 10) {
                mobileNo = [mobileNo substringWithRange:NSMakeRange(mobileNo.length - 10, 10)];
            }
            [dOfPerson setValue:mobileNo forKey:kKeyMobileNumber];
        }

        if ([dOfPerson valueForKey:kKeyEmail] == nil) {
            [dOfPerson setValue:@"" forKey:kKeyEmail];
        }

        // Add contact number in array only when if mobile number is not empty
        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil) {
            [contactList addObject:dOfPerson];
        }



        // Create array of user dictionaries
        [listPerson setValue:firstNameStr forKey:kKeyFirstName];
        [listPerson setValue:lastNameStr forKey:kKeyLastName];
        [listPerson setValue:fullNameStr forKey:kKeyName];
        [listPerson setValue:[dOfPerson valueForKey:kKeyMobileNumber] forKey:kKeyMobileNumber];
        [listPerson setValue:[dOfPerson valueForKey:kKeyEmail] forKey:kKeyEmail];
        [ContactNumberList addObject:listPerson];
    }
    [ContactNumberList addObjectsFromArray:importedConnections];
    server.contactList = ContactNumberList;
    CFRelease(allPeople);
}

-(void)getUserPhoneContact{
    
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyoffset] = @0;
    params[kKeyNumRecords] = @0;
  //  params[kKeyContactType] = kKeyContactValue;
    params[@"q"] = self->searchTextField.text;
    
    [server makeAsychronousRequest:kKeyClassUserContactsGet inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    
    
    
    
}

#pragma mark Section Overview Methods


// ---------------------------------------------------------------------------------------
// sectionForSectionIndexTitle:

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    
    if (tableView == self.notificationsTable) {
        return 0;
    }else{
      return [self.friendListSection indexOfObject:title];
    }
    
    
}

// ---------------------------------------------------------------------------------------
// titleForHeaderInSection:


- (CGFloat)tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.notificationsTable) {
        return 0.001;
    }else{
       return 30.0;
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == self.notificationsTable) {
        return @"";
    }else{
        return [self.friendListSection sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)][section];
    }
    
}

// ---------------------------------------------------------------------------------------
// sectionIndexTitlesForTableView:

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.notificationsTable) {
        NSArray * sectionTitle = [[NSArray alloc] init];
        return sectionTitle;
    }else{
      return self.friendListSection;
    }
}


#pragma mark - MGSwipeTableViewCell Delegates


-(BOOL)swipeTableCell:(MGSwipeTableCell *)cell canSwipe:(MGSwipeDirection)direction fromPoint:(CGPoint)point {
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    swipeSettings.transition = MGSwipeTransitionBorder;
    expansionSettings.buttonIndex = 0;
    
    if (direction == MGSwipeDirectionLeftToRight) {
        expansionSettings.fillOnTrigger = NO;
        expansionSettings.threshold = 2;
        return @[[MGSwipeButton buttonWithTitle:@"DELETE" backgroundColor:[UIColor redColor]]];
    }
    return nil;
}


#pragma mark - TableView Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *beginsWithRequirement = self.friendListSection[indexPath.section];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"first_name BEGINSWITH[c] %@", beginsWithRequirement];
    
    NSArray * arrPredictVal = [searchData filteredArrayUsingPredicate:predicate];
    subArray = [ContactNumberList filteredArrayUsingPredicate:predicate];
    NSDictionary *dict = subArray[indexPath.row];

    if ([self.searchBar.text length] > 0) {
        dict = arrPredictVal[indexPath.row];
    }else{
        dict = subArray[indexPath.row];
    }

    int cellIndex = cell.tag;
//    if (searchData.count == 0) searchData = subArray;
//    dict = searchData[cellIndex];
    
    if ([self.connectionViewTag isEqualToString:kKeyIsFromTracking]) {
        if (importContactArray.count > 0) {
            if ([[dict allKeys] containsObject:kKeyId]) {
                if ([[dict[kKeySetting] objectForKey:kkeytracking_me] isEqualToString:@"1"]) {
                    
                    UIStoryboard *s = [UIStoryboard storyboardWithName:@"TrackingRequest" bundle:nil];
                                           NotifyMeVC *controller = [s instantiateViewControllerWithIdentifier:@"NotifyMeVC"];
                                            controller.server = server;
                                            controller.profileDict = dict;
                                       
                                           [self.navigationController pushViewController:controller animated:true];
                    
                    
//                    SRNotifyMeViewController *notifyMe = [[SRNotifyMeViewController alloc] initWithNibName:nil bundle:nil profileData:dict server:server];
//                    [self.navigationController pushViewController:notifyMe animated:YES];
                } else {
                    [SRModalClass showAlert:@"Sorry, this user has disabled the tracking feature."];
                }
            }
        }
    } else {
        if ([dict[@"is_registered"]  isEqual: @1]){
            if (![self.connectionViewTag isEqualToString:kKeyIsFromConnection]) {
                // Set chat view
                // Get profile if user is from pingview
                NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionaryWithDictionary:dict];
                mutatedDict[kKeyIsFromConnection] = @1;
                server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:mutatedDict];
                // For showind direct connection in degree of seperation view
                [server.friendProfileInfo setValue:@1 forKey:kKeyDegree];

                SRChatViewController *objSRChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:server.friendProfileInfo];
                self.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:objSRChatView animated:YES];
                self.hidesBottomBarWhenPushed = NO;
            }
        } else {
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            if ([MFMessageComposeViewController canSendText]) {
                
                NSString *textToShare =  @"Hello, I would like you to join Serendipity, an app where you do so many cool things such as:\n• Keep track of your friends and family anywhere in the world in real time!\n• View how you\'re connected to others through our six degrees of separation functionality.\n• Maintain your privacy which allows to keep your location a secret at anytime.\nSay goodbye to missed connections and lost family and friends. Join today at http://www.serendipity.app and make sure your friends and family are safe at all times!";
                controller.body = textToShare;
                controller.delegate = self;
                controller.recipients = @[[subArray[indexPath.row] valueForKey:@"mobile_number"]];
                controller.messageComposeDelegate = self;
                
                [controller setModalPresentationStyle:UIModalPresentationFullScreen];
                [self presentViewController:controller animated:YES completion:nil];
        }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.notificationsTable) {
        return 1;
    } else {
      return [self.friendListSection count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    if (tableView == self.notificationsTable) {
        count = notificationsList.count;
        return count;
    }else{
        NSString *beginsWithRequirement = self.friendListSection[section];
        if ([self.searchBar.text length] > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"first_name BEGINSWITH[c] %@", beginsWithRequirement];


            subArray = [searchData filteredArrayUsingPredicate:predicate];
            return [subArray count];
        } else {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"first_name BEGINSWITH[c] %@", beginsWithRequirement];
            
            subArray = [ContactNumberList filteredArrayUsingPredicate:predicate];
            return [subArray count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SRAddConnectionTableViewCell *cell;

    static NSString *CellIdentifier1 = @"Cell1";
        if (tableView == self.notificationsTable) {
            SRConnectionNotificationCell *customCell = (SRConnectionNotificationCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionNotificationCell" owner:self options:nil];
            if ([nibObjects count] > 0) {
                customCell = (SRConnectionNotificationCell *) nibObjects[0];
            }
            // Assign values
            NSDictionary *notiDict = notificationsList[indexPath.row];
            NSDictionary *userDetails = notiDict[kKeySender];
            NSString *fullName = [NSString stringWithFormat:@"%@ %@", userDetails[kKeyFirstName], userDetails[kKeyLastName]];
            
    //        if ([notiDict[kKeyReferenceStatus] isEqualToString:@"2"]) {
    //            CGRect frame = customCell.lblNotification.frame;
    //            frame.size.width = customCell.frame.size.width;
    //            customCell.lblNotification.frame = frame;
    //
    //            customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has accepted your connection request ", fullName];
    //            customCell.btnYesNotification.hidden = YES;
    //            customCell.btnNoNotification.hidden = YES;
    //        } else if ([notiDict[kKeyReferenceStatus] isEqualToString:@"3"]) {
    //            CGRect frame = customCell.lblNotification.frame;
    //            frame.size.width = customCell.frame.size.width;
    //            customCell.lblNotification.frame = frame;
    //
    //            customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has declined your connection request ", fullName];
    //            customCell.btnYesNotification.hidden = YES;
    //            customCell.btnNoNotification.hidden = YES;
    //        } else {
                customCell.lblNotification.text = [NSString stringWithFormat:@"Your contact %@ has recently joined Serendipity", fullName];
                customCell.btnYesNotification.hidden = NO;
                customCell.btnNoNotification.hidden = NO;
           // }
            customCell.btnYesNotification.tag = indexPath.section;
            [customCell.btnYesNotification addTarget:self action:@selector(acceptFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
            customCell.btnNoNotification.tag = indexPath.section;
            [customCell.btnNoNotification addTarget:self action:@selector(rejectFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
            customCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // Return
            return customCell;
        }else{
            if (cell == nil) {
                NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRAddConnectionTableViewCell" owner:self options:nil];
                if ([nibObjects count] > 0) {
                    cell = (SRAddConnectionTableViewCell *) nibObjects[0];
                }
            }
            NSString *beginsWithRequirement = self.friendListSection[indexPath.section];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"first_name BEGINSWITH[c] %@", beginsWithRequirement];
            
            subArray = [ContactNumberList filteredArrayUsingPredicate:predicate];
            NSArray * arrPredictVal = [searchData filteredArrayUsingPredicate:predicate];
            cell.lblName.textColor = [UIColor orangeColor];
            cell.backgroundColor = [UIColor clearColor];
            
            cell.lblName.font = [UIFont fontWithName:kFontHelveticaMedium size:15.0];
            NSString *currentNumber = @"";
            NSString *usedId = @"";
            NSDictionary *currentUser = nil;
            if ([self.searchBar.text length] > 0) {
                cell.lblName.text = [arrPredictVal[indexPath.row] valueForKey:kKeyfullName];
                currentNumber = [arrPredictVal[indexPath.row] valueForKey:kKeyMobileNumber];
                currentUser = arrPredictVal[indexPath.row];
            } else {
                cell.lblName.text = [subArray[indexPath.row] valueForKey:kKeyfullName];
                currentNumber = [subArray[indexPath.row] valueForKey:kKeyMobileNumber];
                usedId = [subArray[indexPath.row] valueForKey:kKeyId];
                currentUser = subArray[indexPath.row];
            }
            
            // Set serendipity icon for SD user
            if (importContactArray.count > 0) {
                NSDictionary *dict;
                if ([self.searchBar.text length] > 0) {
                    dict = arrPredictVal[indexPath.row];
                }else{
                    dict = subArray[indexPath.row];
                }
                
                if ([dict[@"is_registered"]  isEqual: @1]){
                    cell.SDIconImage.image = [UIImage imageNamed:@"tabbar-logo-active.png"];
                    [cell.invitelbl setHidden:TRUE];
                    cell.userId = usedId;
                    cell.user = currentUser;
                    [cell configureForRightSwipe];
                }else{
                    [cell.SDIconImage setHidden:TRUE];
                    [cell.invitelbl setHidden:FALSE];
                }
                if ([[NSString stringWithFormat:@"%@",dict[@"contact_type"]] isEqual:@"0"]){
                    //cell.imgContactType.image = [UIImage imageNamed:@""];
                    [cell.imgContactType setHidden:YES];
                }else if ([[NSString stringWithFormat:@"%@",dict[@"contact_type"]] isEqual:@"1"]){
                    cell.imgContactType.image = [UIImage imageNamed:@"social-googleplus.png"];
                    [cell.imgContactType setHidden:NO];
                }else if ([[NSString stringWithFormat:@"%@",dict[@"contact_type"]] isEqual:@"2"]){
                    cell.imgContactType.image = [UIImage imageNamed:@"social-yahoo.png"];
                    [cell.imgContactType setHidden:NO];
                }else if ([[NSString stringWithFormat:@"%@",dict[@"contact_type"]] isEqual:@"3"]){
                    cell.imgContactType.image = [UIImage imageNamed:@"social-outlook.png"];
                    [cell.imgContactType setHidden:NO];
                }
                
                
                
                
                
                //        if ([[dict allKeys] containsObject:kKeyId]) {
                //            cell.SDIconImage.image = [UIImage imageNamed:@"tabbar-logo-active.png"];
                //            [cell.invitelbl setHidden:TRUE];
                //            cell.userId = usedId;
                //            cell.user = currentUser;
                //            [cell configureForRightSwipe];
                //        } else {
                //            [cell.SDIconImage setHidden:TRUE];
                //            [cell.invitelbl setHidden:FALSE];
                //        }
            }
            //    else if (![self isConcatAdded:currentNumber]){
            //        [cell.SDIconImage setHidden:TRUE];
            //        [cell.invitelbl setHidden:FALSE];
            //    }
            else {
                [cell.SDIconImage setHidden:NO];
                cell.userId = usedId;
                cell.user = currentUser;
                [cell configureForRightSwipe];
                [cell.invitelbl setHidden:YES];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.tag = indexPath.row;
            cell.server = server;
            return cell;
        }
}

- (UIView *)viewWithImageName:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeCenter;
    return imageView;
}

#pragma mark TableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.notificationsTable) {
        return 50;
    }
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView == self.notificationsTable) {
        return nil;
    }else{
        NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
            if (sectionTitle == nil) {
                return nil;
            }
            // Create label with section title
            UILabel *label = [[UILabel alloc] init];
            label.frame = CGRectMake(10, 0, 300, 20);
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor whiteColor];
            label.text = sectionTitle;

            // Create header view and add label as a subview
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
            view.backgroundColor = [UIColor lightGrayColor];
        //    view.backgroundColor = [UIColor colorWithRed:211 green:211 blue:211 alpha:1];
            [view addSubview:label];

            return view;
    }
     
}

#pragma mark - SearchBar Delegates Methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self filterContentForSearchText:searchBar.text scope:[self.searchBar scopeButtonTitles][[self.searchBar selectedScopeButtonIndex]]];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
   [self filterContentForSearchText:searchBar.text scope:[self.searchBar scopeButtonTitles][[self.searchBar selectedScopeButtonIndex]]];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

//public void UpdateSearchResultsForSearchController (UISearchController searchController)
//{
//    throw new NotImplementedException ();
//}

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
    NSPredicate *resultPredicate = [NSPredicate
            predicateWithFormat:@"full_name CONTAINS[cd] %@",
                                searchText];
    searchData = [ContactNumberList filteredArrayUsingPredicate:resultPredicate];
    [self.friendsTable reloadData];
}

#pragma mark - Message Composer delegate


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Upload contact Notificiation


- (void)contactUploadSuccessNotifier:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    [APP_DELEGATE hideActivityIndicator];
    NSArray *importContactArr = [notification object];

    if (importContactArr.count > 0) {
        importContactArray = [[NSMutableArray alloc] initWithArray:importContactArr];
        server.importContactArr = importContactArray;
        if (importContactArray.count > 0) {
            for (int i = 0; i < importContactArray.count; i++) {
                NSDictionary *dict = importContactArray[i];

                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@ || %K == %@", kKeyMobileNumber, [dict valueForKey:kKeyMobileNumber], kKeyEmail, [dict valueForKey:kKeyEmail]];
                NSArray *filteredArr = [ContactNumberList filteredArrayUsingPredicate:predicate];
                if (filteredArr.count > 0) {
                    int index = (int) [ContactNumberList indexOfObject:[filteredArr firstObject]];
                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyName] forKey:kKeyName];
                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyFirstName] forKey:kKeyFirstName];
                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyLastName] forKey:kKeyLastName];
                    ContactNumberList[index] = dict;
                }
            }
        }
        server.contactList = ContactNumberList;
        [self.friendsTable reloadData];
    }

//    // On sign up code verification it already shows contact imported alert so no need to show it twice
//    if (![self.connectionViewTag isEqualToString:kKeyIsFromSignUp]) {
//        [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import.text", @"")];
//    }
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
// ----------------------------------------------------------------------------------------------------------
// plusBtnAction:

- (void)plusBtnAction:(id)sender {
    
    if ([self.viewAddMember isHidden]){
        self.viewAddMember.hidden = NO;
        [self.searchBar resignFirstResponder];
        [self.viewAddMember setFrame:CGRectMake(0, 10, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 45) ];
    }else{
        [self removeInviteView];
    }
    
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
}


//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
  CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [self removeInviteView];
  //Do stuff here...
}

- (IBAction)btnAddNewContactAction:(id)sender {
    if (self.txtName.text.length == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.yourName.text", @"")];
    } else if (countryMasterId == nil) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.country.alert", @"")];
    } else if (self.txtPhoneNumber.text.length == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
    } else {
        // Call login api
        NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtPhoneNumber.text];
        if (mobile_Number.length == 10) {

            [self.txtPhoneNumber resignFirstResponder];
            mobile_Number = [countryMasterId stringByAppendingString:mobile_Number];
            // Call server api to get verify code for password
            [APP_DELEGATE showActivityIndicator];
            // Call api
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            params[kKeyMobileNumber] = mobile_Number;
//            params[kKeyMessage] = [NSString stringWithFormat:@"Hi,%@ your are invited in group via Serendipity app. To see download Serendipity app from:http://serendipity.app", self.txtName.text];

            
            params[kKeyMessage] = @"Hello, I would like you to join Serendipity, an app where you do so many cool things such as:\n• Keep track of your friends and family anywhere in the world in real time!\n• View how you\'re connected to others through our six degrees of separation functionality.\n• Maintain your privacy which allows to keep your location a secret at anytime.\nSay goodbye to missed connections and lost family and friends. Join today at http://www.serendipity.app and make sure your friends and family are safe at all times!";
            
            NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassSendSMS];
            [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        } else {
            [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
        }
    }
}
- (void)pickCountry:(NSDictionary *)countryDict {
    if (countryDict != nil) {

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyPhoneCode]];

        }

        [self.txtCountryName setText:[countryDict valueForKey:kkeyCountryName]];

        // Set search icon to textfield
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(40, 0, 45, 30)];
        container.backgroundColor = [UIColor clearColor];

        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
        self.lblPhoneCode.textColor = [UIColor blackColor];

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];

        }

        [container addSubview:self.lblPhoneCode];

        self.txtPhoneNumber.leftView = container;
        self.txtPhoneNumber.leftViewMode = UITextFieldViewModeAlways;
        [self.txtPhoneNumber becomeFirstResponder];
    } else {
        //[self.lblPhoneCode setText:nil];
        [self.txtCountryName setText:nil];
        [self.txtPhoneNumber resignFirstResponder];
    }
}

- (IBAction)actionOnCountryBtn {
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (![textField isEqual:self.txtPhoneNumber]) {
        return YES;
    }
    if ([textField isEqual:self.txtPhoneNumber]) {
        BOOL isValid = YES;
        NSString *errorMsg = nil;
        NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];

        if ([textStrToCheck length] > 14) {
            errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
        } else if (errorMsg == nil) {
            BOOL flag = [SRModalClass numberValidation:string];

            textField.text = [SRModalClass formatPhoneNumber:self.txtPhoneNumber.text deleteLastChar:NO];

            if (!flag) {
                errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
            }
        }
        // Set flag and show alert
        if (errorMsg) {
            isValid = NO;
            [SRModalClass showAlert:errorMsg];
        }

        // Return
        return isValid;
    }
    return NO;
}

- (void)inviteUserSucceed:(NSNotification *)inNotify {
    [self removeInviteView];
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:@"Invitation send successfully"];
}

- (void)inviteUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

- (IBAction)onTapOutSideArea:(UIControl *)sender {
    [self removeInviteView];
    [self.viewAddMember setHidden:YES];
}
-(void)removeInviteView{
    self.txtName.text = nil;
    self.txtPhoneNumber.text = nil;
    self.txtCountryName.text = nil;
    self.lblPhoneCode.text = nil;
    [self.lblPhoneCode removeFromSuperview];
    [self.viewAddMember setHidden:YES];
}

#pragma mark
// --------------------------------------------------------------------------------
// getDeletedUsersSucceed:

- (void)getSocialConatctUsersSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *contactSuccess = [inNotify object];
    NSArray *importContactArr = contactSuccess[@"list"];
    //self->importContactArr = contactSuccess[@"list"];
    
    if (importContactArr.count > 0) {
        importContactArray = [[NSMutableArray alloc] initWithArray:importContactArr];
        server.importContactArr = importContactArray;
        ContactNumberList = importContactArray;
//        if (importContactArray.count > 0) {
//            for (int i = 0; i < importContactArray.count; i++) {
//                NSDictionary *dict = importContactArray[i];
//                ContactNumberList[i] = dict;
//
////                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@ || %K == %@", kKeyMobileNumber, [dict valueForKey:kKeyMobileNumber], kKeyEmail, [dict valueForKey:kKeyEmail]];
////                NSArray *filteredArr = [ContactNumberList filteredArrayUsingPredicate:predicate];
////                if (filteredArr.count > 0) {
////                    int index = (int) [ContactNumberList indexOfObject:[filteredArr firstObject]];
////                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyName] forKey:kKeyName];
////                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyFirstName] forKey:kKeyFirstName];
////                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyLastName] forKey:kKeyLastName];
////                    ContactNumberList[index] = dict;
////                }
//            }
//        }
        server.contactList = ContactNumberList;
        [self.friendsTable reloadData];
    }
    
    
    
//    if (serendiptyContact.count == 0){
//
//              UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, _tableView.bounds.size.height)];
//              noDataLabel.text             = @"Contacts not found";
//              noDataLabel.textColor        = [UIColor whiteColor];
//              noDataLabel.textAlignment    = NSTextAlignmentCenter;
//              friendsTable.backgroundView = noDataLabel;
//              friendsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
//          }else{
//              friendsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
//              friendsTable.backgroundView = nil;
//          }
    
    //[self.friendsTable reloadData];
}

//// --------------------------------------------------------------------------------
// getDeleteUsersFailed:

- (void)getSocialConatctUsersFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

- (void)deletBlockConnectionFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];
    [self.friendsTable reloadData];
}
- (void)deletBlockConnectionSucceed:(NSNotification *)inNotify {
    NSNumber* deletedUserId = inNotify.userInfo[@"requestedParams"][@"user_id"];
    NSString *type = inNotify.userInfo[@"requestedParams"][@"type"];
    
     for (int i = 0; i < importContactArray.count; i++) {
         NSDictionary*dict = importContactArray[i];
         
         if (dict[@"contact_user_id"] != (id)[NSNull null]){
             if ([deletedUserId isEqualToNumber: dict[@"contact_user_id"]]){
                 [importContactArray removeObjectAtIndex: i];
                 server.importContactArr = importContactArray;
                 ContactNumberList = importContactArray;
                 if ([type isEqualToString: @"block"]){
                  [SRModalClass showAlertWithTitle: @"User Block Successfully!" alertTitle:@"Success"];
                 }else{
                  [SRModalClass showAlertWithTitle: @"User Deleted Successfully!" alertTitle:@"Success"];
                 }
                 
                 break;
             }
         }
    }
    if ([self.searchBar.text length] > 0){
        NSPredicate *resultPredicate = [NSPredicate
                   predicateWithFormat:@"full_name CONTAINS[cd] %@",self.searchBar.text];
        searchData = [ContactNumberList filteredArrayUsingPredicate:resultPredicate];
    }
   
    
    
    [self.friendsTable reloadData];
}


// ---------------------------------------------------------------------------------------------------------------------------------
// acceptFriendRequest:

- (void)acceptFriendRequest:(UIButton *)sender {
    NSInteger i = [sender tag];
    NSDictionary *notiDict = notificationsList[i];

    // Call notification API
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //params[kKeyUserID] = server.loggedInUserInfo[kKeyId];
    params[@"contact_user_id"] = notiDict[kKeyReferenceID];
    params[@"cntc_req_status"] = @2;

    [server makeAsychronousRequest:kKeyClassApproveRejectContact inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// rejectFriendRequest:

- (void)rejectFriendRequest:(UIButton *)sender {
    
    NSInteger i = [sender tag];
    NSDictionary *notiDict = notificationsList[i];

    // Call notification API
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //params[kKeyUserID] = server.loggedInUserInfo[kKeyId];
    params[@"contact_user_id"] = notiDict[kKeyReferenceID];
    params[@"cntc_req_status"] = @3;

    [server makeAsychronousRequest:kKeyClassApproveRejectContact inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    
}

// --------------------------------------------------------------------------------
// updateNotificationSucceed:

- (void)updateNotificationSucceed:(NSNotification *)inNotify {
  [self helperHandleSuccessFailure:inNotify];
}

// --------------------------------------------------------------------------------
// updateNotificationFailed:

- (void)updateNotificationFailed:(NSNotification *)inNotify {
    
    [self helperHandleSuccessFailure:inNotify];
    
}

-(void)helperHandleSuccessFailure:(NSNotification *)inNotify{
    NSDictionary *userInfo = [inNotify userInfo];
    NSNumber *contactId =  userInfo[@"requestedParams"][@"contact_user_id"];
    //  [updatedNotiList addObject:contactId];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reference_id == %@", contactId];
    NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
    if (array.count > 0) {
        if (filterArr.count > 0) {
            [array removeObject:filterArr[0]];
            NSDictionary *param = @{kKeyStatus: @1};
            NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification,filterArr[0][@"id"]];
            [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
        }
    }
    
    notificationsList = array;
    
    NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long) [array count]]];
    [self.btnNotifications setTitle:str forState:UIControlStateNormal];
    [self.notificationsTable reloadData];
    
    // Update notification count in menu view
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdatedNotificationCount object:contactId userInfo:nil];
    
    [UIView animateWithDuration:0.5
    animations:^{
        CGFloat height = 50 * notificationsList.count + 2;
        if (height > 200) {
            height = 200;
        }
        self.notificationsTable.frame = CGRectMake(0, 84, self.view.bounds.size.width, height);
        self.friendsTable.frame = CGRectMake(0, height + self.notificationsTable.frame.origin.y - 4, self.friendsTable.frame.size.width, self.friendsTable.frame.size.height);
    }];
    
    if (notificationsList.count == 0){
        _heightNotification.constant = 0;
        self.btnNotifications.hidden = YES;
        self.imgNatificationsBG.hidden = YES;
        self.imgDropDown.hidden = YES;
        self.notificationsTable.hidden = YES;
    }else{
        _heightNotification.constant = 40;
    }
    
}



@end
