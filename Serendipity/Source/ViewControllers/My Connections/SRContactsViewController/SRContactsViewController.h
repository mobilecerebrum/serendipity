
#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import "SRModalClass.h"
#import "SRProfileImageView.h"
#import <MessageUI/MessageUI.h>
#import "MGSwipeTableCell.h"

@interface SRContactsViewController : ParentViewController<ABPeoplePickerNavigationControllerDelegate, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate, MGSwipeTableCellDelegate,UITextFieldDelegate>
{
    //Server
    SRServerConnection *server;
    
    //Instance Variables
    NSMutableArray *menuArray;
    NSArray *searchData;
    NSMutableArray *contactList, *importContactArray;
    NSMutableArray *ContactNumberList;
    NSArray* subArray;
    NSMutableArray *importedConnections;
    BOOL isFromMenu;
    NSString *countryMasterId;
    NSArray *notificationsList;
    NSMutableArray *updatedNotiList;
    UITextField *searchTextField;
    BOOL isClicked;
    BOOL isOnce;
}
@property (strong, nonatomic) IBOutlet UIView *viewAddMember;

//@property AddFriendCellView *pop;
@property (weak, nonatomic) IBOutlet SRProfileImageView *impProfileImage;
@property (weak, nonatomic) IBOutlet UITableView *friendsTable;
@property (strong,nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong,nonatomic) NSString *connectionViewTag;
@property (strong, nonatomic)UILabel *lblPhoneCode;
@property (retain, nonatomic) NSArray *friendListSection;
@property  CFArrayRef contactAdd;
@property  CFMutableArrayRef  filteredData;
@property ABRecordRef persons;
@property ABAddressBookRef addressBook;
@property (retain,nonatomic) ABPersonViewController *currentPersonViews;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightNotification;

@property (weak, nonatomic) IBOutlet UIView *imgNatificationsBG;
@property (weak, nonatomic) IBOutlet UIButton *btnNotifications;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropDown;
@property (strong,nonatomic) IBOutlet UITableView *notificationsTable;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryName;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *countryPicBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewContact;
- (IBAction)showHideNotificationTable:(id)sender;
//Other Property
@property(weak)id delegate;


#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

@end
