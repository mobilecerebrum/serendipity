#import <UIKit/UIKit.h>
#import "SRActivityIndicator.h"
#import "SRProfileImageView.h"

@interface SRSearchAppUsersController : ParentViewController
{
	//Instance Variables
    UIImageView *overlayImgView;    
    NSOperationQueue *searchQueue;
    NSMutableArray *searchedUsersArr;
    NSMutableArray *cachedImgArr;
    UITextField *searchTextField;
    Boolean *isInviteBtnShow;

	// Server
	SRServerConnection *server;
}

//Prperties
@property (strong, nonatomic) IBOutlet SRProfileImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong,nonatomic) NSString *searchUserTag;
//Other Properties
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inviteBtnHeightOutlet;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtnOutlet;
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end
