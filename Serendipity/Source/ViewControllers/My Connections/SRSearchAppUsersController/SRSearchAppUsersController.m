#import "SRModalClass.h"
#import "SRAddNewConnectionCell.h"
#import "SRSearchAppUsersController.h"
#import "SRNotifyMeViewController.h"
#import "NotifyMeVC.h"

@implementation SRSearchAppUsersController

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// searchFriendForSearchText:

- (void)searchFriendForSearchText:(NSString *)searchText {
    if ([searchText length] > 0) {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@", kKeyClassSearchUser, searchText];
        [server makeSynchronousRequest:urlStr inParams:nil inMethodType:kGET isIndicatorRequired:NO];
    } else {
        // Remove all objects from array
        [searchedUsersArr removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
               [self updateTableView];
               });
        [self performSelectorOnMainThread:@selector(updateTableView) withObject:nil waitUntilDone:NO];
    }
}


//---------------------------------------------------------------------------------------------------------------------------------
// updateTableView

- (void)updateTableView {

    [APP_DELEGATE hideActivityIndicator];

    // Reload data
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSArray *sortedArr = [searchedUsersArr sortedArrayUsingDescriptors:sortDescriptors];
    [searchedUsersArr removeAllObjects];
    [searchedUsersArr addObjectsFromArray:sortedArr];
    [self.tblView reloadData];
}

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SRSearchAppUsersController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        searchQueue = [[NSOperationQueue alloc] init];
        cachedImgArr = [[NSMutableArray alloc] init];
        searchedUsersArr = [[NSMutableArray alloc] init];
        isInviteBtnShow = false;

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(addUserSucceed:)
                              name:kKeyNotificationAddConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(addUserFailed:)
                              name:kKeyNotificationAddConnectionFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(searchUserSucceed:)
                              name:kKeyNotificationSearchUserSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(searchUserFailed:)
                              name:kKeyNotificationSearchUserFailed object:nil];
    }

    //return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];
    //self.tabBarController.navigationController.navigationBarHidden =TRUE;
    // Overlay image view
    
    if (@available(iOS 13.0, *)) {
        self->searchTextField = self.searchBar.searchTextField;
        self->searchTextField.backgroundColor = [UIColor whiteColor];
        self->searchTextField.textColor = [UIColor grayColor];
    } else {
        self->searchTextField = [_searchBar valueForKey:@"searchField"];
        self->searchTextField.backgroundColor = [UIColor whiteColor];
        self->searchTextField.textColor = [UIColor grayColor];
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        overlayImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.tblView.frame.origin.y, SCREEN_WIDTH, self.tblView.frame.size.height)];
        overlayImgView.alpha = 0.4;
        overlayImgView.userInteractionEnabled = FALSE;
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = overlayImgView.bounds;
        gradient.colors = @[(id) [[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0] CGColor], (id) [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.8] CGColor]];
        [overlayImgView.layer insertSublayer:gradient atIndex:0];
    });

    // Manage search bar
  //  [self.searchBar becomeFirstResponder];

    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
}

//---------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //NavigationBar button
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

#pragma mark
#pragma mark Action Methods
#pragma mark

- (IBAction)InviteBtnAction:(id)sender {
}
// --------------------------------------------------------------------------------------------------------------------
// addBtnAction:

- (void)addBtnAction:(UIButton *)sender {
    // Call Server API
    NSDictionary *userDict = searchedUsersArr[sender.tag];
    NSDictionary *params = @{kKeyUserID: server.loggedInUserInfo[kKeyId], kKeyConnectionID: userDict[kKeyId]};
    [server makeAsychronousRequest:kKeyClassAddConnection inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

#pragma mark
#pragma mark TableView DataSource Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [searchedUsersArr count];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// ----------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (cell == nil) {
        static NSString *cellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRAddNewConnectionCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRAddNewConnectionCell *customCell = (SRAddNewConnectionCell *) nibObjects[0];
            cell = customCell;
        }
    }
    // Get details and display
    NSDictionary *userDict = searchedUsersArr[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];

    // Name
    ((SRAddNewConnectionCell *) cell).lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];

    // Occupation
    if ([userDict[kKeyOccupation] length] > 0)
        ((SRAddNewConnectionCell *) cell).lblOccupation.text = userDict[kKeyOccupation];
    else
        ((SRAddNewConnectionCell *) cell).lblOccupation.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");

    if ([((SRAddNewConnectionCell *) cell).lblOccupation.text isEqualToString:@""]) {
        ((SRAddNewConnectionCell *) cell).lblName.frame = CGRectMake(((SRAddNewConnectionCell *) cell).lblName.frame.origin.x, ((SRAddNewConnectionCell *) cell).lblName.frame.origin.y + 15, ((SRAddNewConnectionCell *) cell).lblName.frame.size.width, ((SRAddNewConnectionCell *) cell).lblName.frame.size.height);
    }
    //Set address here
    if (!isNullObject(userDict[kKeyLocation])) {
        if ([userDict[kKeyLocation] objectForKey:kKeyLiveAddress] != nil && [[userDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 0) {
            ((SRAddNewConnectionCell *) cell).lblLocation.text = [userDict[kKeyLocation] objectForKey:kKeyLiveAddress];
        } else {
            ((SRAddNewConnectionCell *) cell).lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        }
    } else {
        ((SRAddNewConnectionCell *) cell).lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
    }
    //For Family show (You can t date Family  member so hide date icon)

    /*if ([[userDict objectForKey:kKeyFamily]isKindOfClass:[NSDictionary class]]) {
        // NSDictionary *familyDict = [userDict objectForKey:kKeyFamily];
        ((SRAddNewConnectionCell *)cell).datingLbl.hidden = YES;
        ((SRAddNewConnectionCell *)cell).datingImage.hidden = YES;
        ((SRAddNewConnectionCell *)cell).lblFamily.hidden = NO;
    }else*/
    // Dating image
    if ([userDict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *settingDict = userDict[kKeySetting];
        ((SRAddNewConnectionCell *) cell).lblFamily.hidden = YES;

        if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
            ((SRAddNewConnectionCell *) cell).datingLbl.hidden = NO;
            ((SRAddNewConnectionCell *) cell).datingImage.hidden = NO;
            ((SRAddNewConnectionCell *) cell).datingImage.image = [UIImage imageNamed:@"dating-orange-40px.png"];
        } else {
            ((SRAddNewConnectionCell *) cell).datingLbl.hidden = YES;
            ((SRAddNewConnectionCell *) cell).datingImage.hidden = YES;
        }
    } else {
        ((SRAddNewConnectionCell *) cell).datingLbl.hidden = YES;
        ((SRAddNewConnectionCell *) cell).datingImage.hidden = YES;
        ((SRAddNewConnectionCell *) cell).lblFamily.hidden = YES;
    }


    //Add connection
    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
        if ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"1"] || [[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"2"]) {
            if ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"1"]) {
                ((SRAddNewConnectionCell *) cell).addBtn.hidden = YES;
                ((SRAddNewConnectionCell *) cell).lblReqSent.hidden = NO;
            } else {
                ((SRAddNewConnectionCell *) cell).addBtn.hidden = YES;
            }

            //Update  ((SRAddNewConnectionCell *)cell).lblLocation frame
            CGRect myFrame = ((SRAddNewConnectionCell *) cell).lblLocation.frame;
            ((SRAddNewConnectionCell *) cell).lblLocation.frame = CGRectMake(myFrame.origin.x, myFrame.origin.y, myFrame.size.width + ((SRAddNewConnectionCell *) cell).addBtn.frame.size.width + 15, myFrame.size.height);

        } else
            ((SRAddNewConnectionCell *) cell).addBtn.hidden = NO;
    } else
        ((SRAddNewConnectionCell *) cell).addBtn.hidden = NO;

    // Profile image
    ((SRAddNewConnectionCell *) cell).profileImg.contentMode = UIViewContentModeScaleAspectFill;
    ((SRAddNewConnectionCell *) cell).profileImg.clipsToBounds = YES;

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userDict[kKeyId]];
    NSArray *filterdArr = [cachedImgArr filteredArrayUsingPredicate:predicate];
    UIImage *storedImg = nil;
    if ([filterdArr count] > 0) {
        NSDictionary *updatedDict = filterdArr[0];
        storedImg = updatedDict[kKeyStoredImage];
    }
    if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]] && storedImg == nil) {
        NSDictionary *profileDict = userDict[kKeyProfileImage];
        if (profileDict[kKeyImageName] != nil) {
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [((SRAddNewConnectionCell *) cell).profileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    } else
        ((SRAddNewConnectionCell *) cell).profileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];



    // Add button target and action
    ((SRAddNewConnectionCell *) cell).addBtn.tag = indexPath.section;
    [((SRAddNewConnectionCell *) cell).addBtn addTarget:self action:@selector(addBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [((SRAddNewConnectionCell *) cell).addBtn.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    ((SRAddNewConnectionCell *) cell).addBtn.layer.cornerRadius = 20;

    // Return
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark
#pragma mark Tableview Delegates
#pragma mark
// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }

    // Return
    return height;
}

// --------------------------------------------------------------------------------
// didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
 
    
    if ([self.searchUserTag isEqualToString:kKeyIsFromTracking]) {
        NSDictionary *userDict = searchedUsersArr[indexPath.section];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];
        //Add connection
        if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
            if ([[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"1"] || [[userDict[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"2"]) {
                if ([[userDict[kKeySetting] objectForKey:kkeytracking_me] isEqualToString:@"1"]) {
                    
                    UIStoryboard *s = [UIStoryboard storyboardWithName:@"TrackingRequest" bundle:nil];
                        NotifyMeVC *controller = [s instantiateViewControllerWithIdentifier:@"NotifyMeVC"];
                         controller.server = server;
                         controller.profileDict = userDict;
                    
                        [self.navigationController pushViewController:controller animated:true];
                      
                    
//                    SRNotifyMeViewController *notifyMe = [[SRNotifyMeViewController alloc] initWithNibName:nil bundle:nil profileData:userDict server:server];
//                    [self.navigationController pushViewController:notifyMe animated:YES];
                } else {
                    [SRModalClass showAlert:@"Sorry, this user has disabled the tracking feature."];
                }
            } else
                [SRModalClass showAlert:@"To track user you need to add him/her in your connection. Confirmation of your friend request is pending."];
        } else
            [SRModalClass showAlert:@"To track a user, the user must first accept your connection request and after this done you can request to track them.  Your connection request has been sent and is pending."];
    }
}

#pragma mark
#pragma mark Search Bar Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// textDidChange:

- (void)searchBar:(UISearchBar *)inSearchBar textDidChange:(NSString *)searchText {
    //inSearchBar.text = searchText.lowercaseString;
    // Cancel any running operations so we only have one search thread
    // running at any given time..
    [searchQueue cancelAllOperations];
    //[APP_DELEGATE showActivityIndicator];
//    [self searchFriendForSearchText:searchText];
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(searchFriendForSearchText:)
                                                                       object:self.searchBar.text];
    [searchQueue addOperation:op];
}

// --------------------------------------------------------------------------------
// searchBarShouldBeginEditing:

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)inSearchBar {
    // Add overlay view
   // [self.view addSubview:overlayImgView];
    self.tblView.scrollEnabled = NO;

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidBeginEditing:

- (void)searchBarTextDidBeginEditing:(UISearchBar *)inSearchBar {
    // Add overlay view
 //   [self.view addSubview:overlayImgView];
    self.tblView.scrollEnabled = NO;
}

// --------------------------------------------------------------------------------
// searchBarShouldEndEditing:

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)inSearchBar {
    // Remove  overlay view from view
    //[overlayImgView removeFromSuperview];
    self.tblView.scrollEnabled = YES;

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarSearchButtonClicked:

- (void)searchBarSearchButtonClicked:(UISearchBar *)inSearchBar {
    // Search
    //[APP_DELEGATE showActivityIndicator];
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(searchFriendForSearchText:) object:self.searchBar.text];
    [searchQueue addOperation:op];

    // Hide keyboard
    [self.searchBar resignFirstResponder];
}

/*// --------------------------------------------------------------------------------
// searchBarCancelButtonClicked:

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[self.searchBar resignFirstResponder];
	[overlayImgView removeFromSuperview];
	self.searchBar.text = @"";
}*/

#pragma mark
#pragma mark Touch Event Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.searchBar resignFirstResponder];
  //  [overlayImgView removeFromSuperview];
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// addUserSucceed:

- (void)addUserSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *userInfo = [inNotify userInfo];
    NSDictionary *object = userInfo[kKeyResponse];
    NSDictionary *postInfo = userInfo[kKeyRequestedParams];

    BOOL isFound = NO;
    for (NSUInteger i = 0; i < [searchedUsersArr count] && !isFound; i++) {
        NSDictionary *searchedDict = searchedUsersArr[i];
        if ([searchedDict[kKeyId] isEqualToString:postInfo[kKeyConnectionID]]) {
            isFound = YES;
            NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionaryWithDictionary:searchedDict];
            NSMutableDictionary *responseMuatated = [NSMutableDictionary dictionaryWithDictionary:object];
            if ([responseMuatated[kKeyConnectionStatus] isKindOfClass:[NSNumber class]]) {
                NSInteger statusId = [responseMuatated[kKeyConnectionStatus] integerValue];
                NSString *strId = [NSString stringWithFormat:@"%ld", (long) statusId];
                responseMuatated[kKeyConnectionStatus] = strId;
            }
            mutatedDict[kKeyConnection] = responseMuatated;

            // Replace dict with new
            searchedUsersArr[i] = mutatedDict;
        }
    }

    [SRModalClass showAlert:NSLocalizedString(@"alert.requestsent.text", @"")];

    // Reload table
    [self.tblView reloadData];

    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
}

// --------------------------------------------------------------------------------
// addUserFailed:

- (void)addUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

// --------------------------------------------------------------------------------
// searchUserSucceed:

- (void)searchUserSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
  //  NSLog(@"%@", inNotify.userInfo[@"objectUrl"]);
    NSString * serachedResText = inNotify.userInfo[@"objectUrl"];
    NSArray *items = [serachedResText componentsSeparatedByString:@"="];
    NSString * textVal = items.lastObject;
    if ([textVal isEqualToString:self.searchBar.text]){
        [APP_DELEGATE hideActivityIndicator];
        if (![searchedUsersArr isEqualToArray:[inNotify object]]) {
            [searchedUsersArr removeAllObjects];
            searchedUsersArr = [inNotify object];
            [self updateTableView];
        }
    }
}

// --------------------------------------------------------------------------------
// searchUserFailed:

- (void)searchUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

@end
