#import "SRModalClass.h"
#import "SRContactsViewController.h"
#import "SRDiscoveryListViewController.h"
#import "SRSearchAppUsersController.h"
#import "SRAddConnectionTabViewController.h"

@implementation SRAddConnectionTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    // Call super
    if ([nibNameOrNil isEqualToString:kKeyIsFromSignUp]) {
        self.navigateTo = kKeyIsFromSignUp;
    } else if ([nibNameOrNil isEqualToString:kKeyIsFromTracking]) {
        self.navigateTo = kKeyIsFromTracking;
    }
    self = [super initWithNibName:@"SRAddConnectionTabViewController" bundle:nibBundleOrNil];


    if (self) {
        // Custom initialization
    }

    // Return
    return self;
}

#pragma mark
#pragma mark Standard Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // NavBar Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.NewConnectionTab.view", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarSmallFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];



    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];

    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;
    self.tabBar.unselectedItemTintColor = appTintColor;
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f],
            NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f],
            NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];

    // Set Serendipity List tab
    SRSearchAppUsersController *searchCon = [[SRSearchAppUsersController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    if ([self.navigateTo isEqualToString:kKeyIsFromTracking]) {
        searchCon.searchUserTag = kKeyIsFromTracking;
    }
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-logo-active.png"];
    UITabBarItem *objnewConnTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.serendipity.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-logo-active.png"]];
    [objnewConnTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    searchCon.tabBarItem = objnewConnTabItem;

    // Set Contacts tab
    SRContactsViewController *objContacts = [[SRContactsViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:(APP_DELEGATE).server];
    objContacts.connectionViewTag = kKeyIsFromConnection;
    tabImage = [UIImage imageNamed:@"tabbar-contact-active.png"];
    UITabBarItem *objContactsTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.Contacts.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-contact-active.png"]];
    [objContactsTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objContacts.tabBarItem = objContactsTabItem;

    self.viewControllers = @[searchCon, objContacts];

    if ([self.navigateTo isEqualToString:kKeyIsFromSignUp]) {
        self.selectedIndex = 1;
        objContacts.connectionViewTag = kKeyIsFromSignUp;
    } else if ([self.navigateTo isEqualToString:kKeyIsFromTracking]) {
        self.selectedIndex = 0;
        objContacts.connectionViewTag = kKeyIsFromTracking;
    } else
        self.selectedIndex = 0;
}

#pragma mark
#pragma mark Action Methodss
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
