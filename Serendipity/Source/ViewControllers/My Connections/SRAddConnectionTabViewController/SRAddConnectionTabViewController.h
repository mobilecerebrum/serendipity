#import <UIKit/UIKit.h>

@interface SRAddConnectionTabViewController : UITabBarController

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil;

@property (nonatomic,strong)NSString *navigateTo;
@end
