//
//  SRCompassWindow.h
//  Serendipity
//
//  Created by Sunil Dhokare on 29/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRCompassWindow : UIView

@property (weak, nonatomic) IBOutlet UIImageView *BGImage;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAddr;
@property (weak, nonatomic) NSDictionary *userDict;

- (void)setBorderColor:(UIColor*)color;

@end
