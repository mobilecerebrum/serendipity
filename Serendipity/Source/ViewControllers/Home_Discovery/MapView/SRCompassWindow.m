//
//  SRCompassWindow.m
//  Serendipity
//
//  Created by Sunil Dhokare on 29/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRCompassWindow.h"

@implementation SRCompassWindow
@synthesize userDict;
#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRCompassWindow" owner:self options:nil];
        self = nibArray[0];
        self.frame = frame;
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        
        self.BGImage.layer.cornerRadius = 5;
        
        
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.height /2;
        self.profilePic.layer.masksToBounds = YES;
        
        [self setBorderColor:[UIColor blackColor]];
        
        
    }
    // Return
    return self;
}

- (void)setBorderColor:(UIColor*)color {
    
    self.BGImage.layer.borderWidth = 2.0;
    self.BGImage.layer.borderColor = [color CGColor];
    
    self.profilePic.layer.borderWidth = 2.0;
    self.profilePic.layer.borderColor = [color CGColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
