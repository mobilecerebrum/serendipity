//
//  SRMapViewController.h
//  Serendipity
//
//  Created by Leo on 01/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import "SRCompassWindow.h"
#import "SRMapProfileView.h"

@interface SRMapViewController : ParentViewController<GMSMapViewDelegate,UIAlertViewDelegate>
{
    SRServerConnection *server;
    GMSMarker *dropMarker;
    GMSMarker *selectedMarker;
    NSMutableDictionary *userDictionary;
    NSDictionary *userDataDict;
    SRCompassWindow *compassWindow;
    NSString *adressStr;
    BOOL isAdded,isPanoramaLoaded;
    BOOL fromEventOrGroup;
    NSMutableAttributedString * wholeString;
    IBOutlet UIButton *streetViewBtn;
    IBOutlet UIButton *btnSatelite;
    IBOutlet UIButton *btnStreet;
    CLLocationCoordinate2D panoramaLastLoc;
    float delayTime;
    BOOL isDropped;
    NSNumber *zoomLevel;
    float distanceFilter;
}
// Property
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong) NSTimer *updateTimer;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *) dict server:(SRServerConnection *)inServer;;

@end
