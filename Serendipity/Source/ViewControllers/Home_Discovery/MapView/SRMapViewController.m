//
//  SRMapViewController.m
//  Serendipity
//
//  Created by Leo on 01/06/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRMapViewController.h"
#import "SRModalClass.h"
#import "SRPanoramaViewController.h"
#import "FrequencyUpdateView.h"

#define infoWidth  240.0
#define infoHeight 80

@interface SRMapViewController ()

- (void)calltoUpdateUserLocation;

@end

@implementation SRMapViewController

#pragma mark - Init Method

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict server:(SRServerConnection *)inServer; {

    self = [super initWithNibName:@"SRMapViewController" bundle:nibBundleOrNil];
    if ([nibNameOrNil isEqualToString:@"FromEventListView"] || [nibNameOrNil isEqualToString:@"FromGroupListView"]) {
        fromEventOrGroup = YES;
    } else
        fromEventOrGroup = NO;

    if (self) {
        // Custom initialization
        userDictionary = dict;
        server = inServer;
    }


    // Register notification
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(getupdatedLocationSucceed:)
                          name:kKeyNotificationSearchUsersByIdSucceed
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getupdatedLocationFailed:)
                          name:kKeyNotificationSearchUsersByIdFailed
                        object:nil];

    // Return
    return self;
}

- (void)dealloc {

    [_updateTimer invalidate];
    _updateTimer = nil;

    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tabBarController.tabBar setHidden:YES];
//    (APP_DELEGATE).topBarView.hidden = YES;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    // Title
    if (fromEventOrGroup) {
        [SRModalClass setNavTitle:[NSString stringWithFormat:@"%@", [userDictionary valueForKey:kKeyName]] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    } else {
        [SRModalClass setNavTitle:[NSString stringWithFormat:@"%@ %@", [userDictionary valueForKey:kKeyFirstName], [userDictionary valueForKey:kKeyLastName]] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }


    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnBack) forControlEvents:UIControlEventTouchUpInside];

    _mapView.myLocationEnabled = YES;
    _mapView.settings.myLocationButton = YES;
//    _mapView.padding = UIEdgeInsetsMake(64, 0, 0, SCREEN_WIDTH/2-30);//Comment By Sunil for issue On didTapMarker pin goes to left side (minus coordinates)

    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    if (fromEventOrGroup) {
        [self displayData];
    } else {
        [self displayData];
        // Set distance offset for user location
        if (![userDictionary[kKeyConnection] isKindOfClass:[NSDictionary class]] && ![[userDictionary[kKeySetting] objectForKey:kkeyDistanceAccuracy] isEqualToString:@"0"] && [userDictionary[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            //KiloMeters to miles
            distanceFilter = [[userDictionary[kKeySetting] objectForKey:kkeyDistanceAccuracy] floatValue];
            distanceFilter = (float) (distanceFilter * 0.3048);
        }
//        [self calltoUpdateUserLocation];
        if ([[userDictionary valueForKey:@"is_tracking"] boolValue] == true && [[userDictionary valueForKey:kKeyDegree] intValue] == 1) {
            [self calculateTimerDelay];

        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:panoramaLastLoc.latitude longitude:panoramaLastLoc.longitude zoom:6];
        [self.mapView animateToCameraPosition:camera];
    }

}

#pragma mark - GMSMapView Delegate and Datasource

// --------------------------------------------------------------------------------
// didTapMarker:
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {

    //change pin location from center to left side
    CGPoint point = [_mapView.projection pointForCoordinate:marker.position];
    //point.x = point.x + SCREEN_WIDTH / 2 - 20;
    GMSCameraUpdate *camera =
            [GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
    [_mapView animateWithCameraUpdate:camera];

    // Check for marker in the list if same marker got tapped then remove the select object
    if (marker.userData != nil) {
        selectedMarker = marker;
//        marker.icon = [UIImage imageNamed:@"radar-pin-black-30px"];
        marker.icon = [self markerImageForUser];
        selectedMarker.userData = userDictionary;

        CGPoint markerPoint = [self.mapView.projection pointForCoordinate:marker.position];
        CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
        
        [self updateInfoWindowPosition:pointInViewCoords];
        
        if (fromEventOrGroup) {
            compassWindow.profilePic.image = [UIImage imageNamed:@"events_avatar1.png"];
            if (userDictionary[kKeyMediaImage] != nil && [userDictionary[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {

                if ([userDictionary[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                    NSString *imageName = [userDictionary[kKeyMediaImage] objectForKey:kKeyImageName];
                    if ([imageName length] > 0) {
                        NSString *imageUrl;
                        if ([[userDictionary valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
                            imageUrl = [NSString stringWithFormat:@"%@", imageName];
                        } else
                            imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                        [compassWindow.profilePic sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                    } else
                        compassWindow.profilePic.image = [UIImage imageNamed:@"events_avatar1.png"];

                } else
                    compassWindow.profilePic.image = [UIImage imageNamed:@"events_avatar1.png"];
            } else
                compassWindow.profilePic.image = [UIImage imageNamed:@"events_avatar1.png"];
            selectedMarker.userData = userDictionary;

            //Set Location Address
            if (userDictionary[kKeyAddress] != nil && ![userDictionary[kKeyAddress] isKindOfClass:[NSNull class]]) {
                adressStr = [NSString stringWithFormat:@"%@.", userDictionary[kKeyAddress]];
            } else {
                adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");
            }

        } else {
            compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            if ([userDictionary[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                if ([userDictionary[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                    NSString *imageName = [userDictionary[kKeyProfileImage] objectForKey:kKeyImageName];
                    if ([imageName length] > 0) {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                        [compassWindow.profilePic sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                    } else
                        compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                } else
                    compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                compassWindow.profilePic.image = [UIImage imageNamed:@"deault_profile_bck.png"];

            //Set Location Address
            if (userDictionary[kKeyLocation] != nil && ![userDictionary[kKeyLocation] isKindOfClass:[NSNull class]]) {
                if ([userDictionary[kKeyLocation] objectForKey:kKeyLiveAddress] != nil && [[userDictionary[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 0 && ![[userDictionary[kKeyLocation] objectForKey:kKeyLiveAddress] isKindOfClass:[NSNull class]]) {
                    adressStr = [NSString stringWithFormat:@"%@.", [userDictionary[kKeyLocation] objectForKey:kKeyLiveAddress]];
                } else {
                    adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                }
            }
        }


        //Set DayTime
        [self displayDayTime];


        NSString *windowText = [NSString stringWithFormat:@"%@ \n %@", [self displayDayTime], adressStr];
        wholeString = [[NSMutableAttributedString alloc] initWithString:windowText];
        NSRange range1 = [windowText rangeOfString:[self displayDayTime]];
        NSDictionary *attrDict1 = @{
                NSFontAttributeName: [UIFont boldSystemFontOfSize:13],
                NSForegroundColorAttributeName: [UIColor whiteColor]
        };
        [wholeString addAttributes:attrDict1 range:range1];
        NSRange range2 = [windowText rangeOfString:adressStr];
        NSDictionary *attrDict2 = @{
                NSFontAttributeName: [UIFont boldSystemFontOfSize:13],
                NSForegroundColorAttributeName: [UIColor orangeColor]
        };
        [wholeString addAttributes:attrDict2 range:range2];
        compassWindow.txtViewAddr.attributedText = wholeString;

        if (adressStr.length > 0) {
            CGRect txtViewFrame = compassWindow.txtViewAddr.frame;
            CGRect compassWindowFrame = compassWindow.frame;
            //compassWindowFrame.size.width = SCREEN_WIDTH - 10;
            compassWindow.txtViewAddr.scrollEnabled = YES;
            compassWindow.txtViewAddr.contentSize = [compassWindow.txtViewAddr sizeThatFits:compassWindow.txtViewAddr.frame.size];
            compassWindowFrame.size.height = txtViewFrame.size.height + 10;
            compassWindow.frame = compassWindowFrame;
        }

        if (!isAdded) {
            isAdded = YES;
            compassWindow.hidden = NO;
            [self.mapView addSubview:compassWindow];
        } else {
            compassWindow.hidden = NO;
        }
    }
    [compassWindow.txtViewAddr scrollRangeToVisible:NSMakeRange(0, 0)];
    
    NSString *batteryUsage = [self getBatterySettings];
    UIColor *color = [APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage];
    [compassWindow setBorderColor:color];

    return YES;
}

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

// --------------------------------------------------------------------------------
// didChangeCameraPosition:

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    [self updateInfoWindowPosition:pointInViewCoords];
}

// --------------------------------------------------------------------------------
// idleAtCameraPosition:

// This method gets called whenever the map was moving but has now stopped

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    [self updateInfoWindowPosition:pointInViewCoords];
}

// --------------------------------------------------------------------------------
// didTapAtCoordinate:

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    if (selectedMarker) {
        compassWindow.hidden = YES;
        selectedMarker.icon = [self markerImageForUser]; //[UIImage imageNamed:@"radar-pin-orange-30px"];
    }
}

- (void)updateInfoWindowPosition:(CGPoint )pointMaker {
    CGRect frame = CGRectMake((SCREEN_WIDTH - infoWidth)/2, (SCREEN_HEIGHT - infoHeight)/2, infoWidth, infoHeight);
    //CGRect frame = CGRectMake(pointInViewCoords.x - 20, pointInViewCoords.y - 55, 240, 80);
    if (compassWindow == nil) {
        compassWindow = [[SRCompassWindow alloc] initWithFrame:frame];
    } else {
        compassWindow.userDict = userDictionary;
        compassWindow.frame = frame;
    }
    compassWindow.center = CGPointMake(pointMaker.x, pointMaker.y - infoHeight/2 - 26); //30 is icon maker height
}

#pragma mark - Private Method

// --------------------------------------------------------------------------------
// actionOnBtnClick
- (IBAction)actionOnBtnClick:(UIButton *)inSender {
    if (inSender.tag == 0) {
        self.mapView.mapType = kGMSTypeSatellite;
        [btnStreet setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    } else {
        self.mapView.mapType = kGMSTypeNormal;
        [btnStreet setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

- (void)calculateTimerDelay {
    if (_updateTimer && [_updateTimer isValid]) {
        // Don't Do anything

        NSLog(@"kya yha pe aaker code fatt gya");
    } else {
        // Set the Timer with loop
        NSString *userBatterySetting = [[userDictionary valueForKey:kKeySetting] valueForKey:kkeyBatteryUsage];
        int updateDuration = 0;

        if ([userBatterySetting caseInsensitiveCompare:NSLocalizedString(@"lbl.minimal.txt", @"")] == NSOrderedSame) {
            updateDuration = 0;
        } else if ([userBatterySetting caseInsensitiveCompare:NSLocalizedString(@"lbl.very_low.txt", @"")] == NSOrderedSame) {
            updateDuration = 34;
        } else if ([userBatterySetting caseInsensitiveCompare:NSLocalizedString(@"lbl.low.txt", @"")] == NSOrderedSame) {
            updateDuration = 68;
        } else if ([userBatterySetting caseInsensitiveCompare:NSLocalizedString(@"lbl.medium.txt", @"")] == NSOrderedSame) {
            updateDuration = 102;
        } else if ([userBatterySetting caseInsensitiveCompare:NSLocalizedString(@"lbl.high.txt", @"")] == NSOrderedSame) {
            updateDuration = 136;
        } else if ([userBatterySetting caseInsensitiveCompare:NSLocalizedString(@"lbl.real_time.txt", @"")] == NSOrderedSame) {
            updateDuration = 170;
        } else {

            updateDuration = 136;
        }

        delayTime = 0;
        if (updateDuration == 0) {
            delayTime = 0;
        } else if (updateDuration == 34) {
            delayTime = (60 * 60);
        } else if (updateDuration == 68) {
            delayTime = (30 * 60);
        } else if (updateDuration == 102) {
            delayTime = (5 * 60);
        } else if (updateDuration == 136) {
            delayTime = 60;
        } else if (updateDuration == 170) {
            delayTime = 10;
        }

        if (delayTime > 0) {
            // Get updated location after given time
            [_updateTimer invalidate];

            if ([self respondsToSelector:@selector(calltoUpdateUserLocation)]) {
                _updateTimer = [NSTimer scheduledTimerWithTimeInterval:delayTime target:self selector:@selector(calltoUpdateUserLocation) userInfo:nil repeats:YES];
            }

        }
    }
}

- (void)displayData {
    // Animate map to that position
    compassWindow.hidden = YES;
    if (fromEventOrGroup) {
        if ([userDictionary isKindOfClass:[NSDictionary class]]) {
            if (userDictionary.count > 0) {
                if ([userDictionary valueForKey:kKeyLattitude] != nil && [userDictionary valueForKey:kKeyRadarLong] != nil) {
                    [self.mapView clear];
                    dropMarker.map = nil;
                    CLLocationCoordinate2D coordinate = {[[userDictionary valueForKey:kKeyLattitude] floatValue], [[userDictionary valueForKey:kKeyRadarLong] floatValue]};
                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:14];
                    [self.mapView animateToCameraPosition:camera];

                    // Create current selected marker
                    dropMarker = [[GMSMarker alloc] init];
                    dropMarker.position = coordinate;
                    //                self.mapView.
                    dropMarker.userData = userDictionary;
                    dropMarker.icon = [UIImage imageNamed:@"radar-pin-orange-30px"];
                    dropMarker.appearAnimation = kGMSMarkerAnimationPop;
                    _mapView.selectedMarker = dropMarker;
                    dropMarker.map = _mapView;
                    _mapView.delegate = self;
                }
            }
        }
    } else {
        // Add location offset
        /*CLLocationCoordinate2D userCoordinate = CLLocationCoordinate2DMake([[[userDictionary objectForKey:kKeyLocation] objectForKey:kKeyLattitude]floatValue], [[[userDictionary objectForKey:kKeyLocation] objectForKey:kKeyRadarLong]floatValue]);
        CLLocationCoordinate2D coordinate = [SRModalClass translateCoord:userCoordinate MetersLat:0.00 MetersLong:distanceFilter];
        
        [[userDictionary objectForKey:kKeyLocation] setValue:[NSString stringWithFormat:@"%f",coordinate.latitude] forKey:kKeyLattitude];
        [[userDictionary objectForKey:kKeyLocation] setValue:[NSString stringWithFormat:@"%f",coordinate.longitude] forKey:kKeyRadarLong];*/

        NSDictionary *locDict = [userDictionary valueForKey:kKeyLocation];
        if ([locDict isKindOfClass:[NSDictionary class]]) {
            if (locDict.count > 0) {
                if ([locDict valueForKey:kKeyLattitude] != nil && [locDict valueForKey:kKeyRadarLong] != nil) {
                    CLLocationCoordinate2D coordinate = {[[locDict valueForKey:kKeyLattitude] floatValue], [[locDict valueForKey:kKeyRadarLong] floatValue]};
                    if (!isDropped) {
                        isDropped = YES;
                        [self.mapView clear];
                        dropMarker.map = nil;
                        CLLocationCoordinate2D coordinate = {[[locDict valueForKey:kKeyLattitude] floatValue], [[locDict valueForKey:kKeyRadarLong] floatValue]};
                        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:14];
                        [self.mapView animateToCameraPosition:camera];

                        // Create current selected marker
                        FrequencyUpdateView *__updateMarker = [[FrequencyUpdateView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_HEIGHT > 800) ? 80 : 43, (SCREEN_HEIGHT > 800) ? 80 : 43)];
                        [__updateMarker updateFrequencyWithUserDict:userDictionary];
                        
                        dropMarker = [[GMSMarker alloc] init];
                        dropMarker.position = coordinate;
                        dropMarker.title = [[userDictionary[kKeyFirstName] stringByAppendingString:@" "] stringByAppendingString:userDictionary[kKeyLastName]];
                        dropMarker.userData = userDictionary;

                        if ([self isMyFriend]) {
                            dropMarker.icon = [self markerImageForUser];
//                            dropMarker.iconView = __updateMarker;
                        } else {
                            dropMarker.icon = [UIImage imageNamed:@"radar-pin-orange-30px"];
                        }
                        dropMarker.appearAnimation = kGMSMarkerAnimationPop;
                        dropMarker.map = _mapView;
                        _mapView.delegate = self;
                        compassWindow.hidden = YES;

                        [__updateMarker layoutIfNeeded];
                    } else {
                        [CATransaction begin];
                        [CATransaction setAnimationDuration:4.0];
                        [self.mapView animateToLocation:coordinate];
                        
                        FrequencyUpdateView *__updateMarker = [[FrequencyUpdateView alloc] initWithFrame:CGRectMake(0, 0, (SCREEN_HEIGHT > 800) ? 80 : 43, (SCREEN_HEIGHT > 800) ? 80 : 43)];
                        [__updateMarker updateFrequencyWithUserDict:userDictionary];
                        
                        dropMarker = [[GMSMarker alloc] init];
                        dropMarker.position = coordinate;
                        dropMarker.title = [[userDictionary[kKeyFirstName] stringByAppendingString:@" "] stringByAppendingString:userDictionary[kKeyLastName]];
                        dropMarker.userData = userDictionary;
                        
                        if ([self isMyFriend]) {
                            dropMarker.iconView = __updateMarker;
                        } else {
                            dropMarker.icon = [UIImage imageNamed:@"radar-pin-orange-30px"];
                        }
                        dropMarker.position = coordinate;
                        [CATransaction commit];
                        
                        [__updateMarker layoutIfNeeded];
                    }
                    
                    [self performSelector:@selector(updateIcon) withObject:nil afterDelay:0.2];
                }
            }
        }
    }
}

- (UIImage*)markerImageForUser {
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
    SRMapProfileView *userProfileView = nibArray[1];
    
    //                    // Provide round rect
    //                    userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y - 2, 37, 37);
    userProfileView.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
    userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2.0;
    userProfileView.userProfileImg.clipsToBounds = YES;
    // userProfileView.userProfileImg.layer.masksToBounds = YES;
    
        if (userDictionary[kKeyLocation] != nil && ![userDictionary[kKeyLocation] isEqual:[NSNull null]]) {
            if ([userDictionary[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                if (![[userDictionary[kKeyProfileImage] objectForKey:kKeyImageName] isKindOfClass:[NSNull class]]) {
                    NSString *imageName = [userDictionary[kKeyProfileImage] objectForKey:kKeyImageName];
                    if (![imageName isKindOfClass:[NSNull class]]) {
                        if ([imageName length] > 0) {
                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                            [userProfileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                        } else
                            userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                    }
                } else
                    userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
            } else {
                userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
            }
            userProfileView.userInfo = userDictionary;
        }
    
    // Show status for user
    [userProfileView updateStatus];
    
    // Add to view
//    [userProfileViewArr addObject:userProfileView];
    UIImage *img = [SRModalClass imageFromUIView:userProfileView];
    return img;
}

- (void)updateIcon {
    if ([dropMarker.iconView isKindOfClass:[FrequencyUpdateView class]]) {
        FrequencyUpdateView *__view = (FrequencyUpdateView *)dropMarker.iconView;
        [__view layoutIfNeeded];
        
        __view.imvIndicatorWidth.constant = (SCREEN_WIDTH > 800) ? 24.0 : 20;
    }
}

- (BOOL)isMyFriend {
    if (userDictionary[@"connection"] != nil) {
        if ([userDictionary[@"connection"] isKindOfClass:[NSDictionary class]]) {
            if ([userDictionary[@"connection"] valueForKey:@"user_id"] != nil) {
                if ([[userDictionary[@"connection"] valueForKey:@"user_id"] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"id"]] || [[userDictionary[@"connection"] valueForKey:@"connection_id"] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"id"]]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

// --------------------------------------------------------------------------------
// displayDayTime:
- (NSString *)displayDayTime {
    NSDate *currentDate;
    if (fromEventOrGroup) {
        NSString *date = [[[userDictionary valueForKey:kKeyEvent_Date] stringByAppendingString:@" "] stringByAppendingString:[userDictionary valueForKey:kKeyEvent_Time]];
        currentDate = [SRModalClass returnFullDate:date];
        if (currentDate == nil) {
            currentDate = [NSDate date];
        }
    } else {
        currentDate = [SRModalClass returnFullDate:[[userDictionary valueForKey:kKeyLocation] valueForKey:kKeyLastSeenDate]];
    }
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components

    NSInteger monthNumber = [components month]; //gives you month
    NSInteger day = [components day]; //gives you day
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSString *monthName = [df monthSymbols][(NSUInteger) (monthNumber - 1)];

    NSDateFormatter *time = [[NSDateFormatter alloc] init];

    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    [time setDateFormat:@"hh:mm a"];
    [time setAMSymbol:@"am"];
    [time setPMSymbol:@"pm"];

    NSString *timeString = [time stringFromDate:currentDate];
    NSString *dayTime = [NSString stringWithFormat:@"%@ %ld at %@ %@ near", monthName, (long) day, timeString, localAbbreviation];

    return dayTime;
}

// --------------------------------------------------------------------------------
// getAddressFromLatLon:
- (void)getAddressFromLatLon:(CLLocation *)bestLocation {
    adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");
    //Call Google API for correct distance and address from two locations
    if (server.myLocation != nil) {
        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:bestLocation.coordinate.latitude longitude:bestLocation.coordinate.longitude];

        NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.timeoutInterval = 60;
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    if (!error) {
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                        if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                            //parse data
                            NSArray *rows = json[@"rows"];
                            NSDictionary *rowsDict = rows[0];
                            NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                            NSDictionary *dict = dataArray[0];
                            NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                            NSString *distance = [distanceDict valueForKey:@"text"];

                            //Live Address
                            NSArray *destinationArray = json[@"destination_addresses"];
                            adressStr = [NSString stringWithFormat:@"in %@", destinationArray[0]];
                            if ([distance floatValue] <= 0) {
                                adressStr = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                            }
                        }
                    }
                }];
    }
}

- (NSString *)getBatterySettings {
    if (userDictionary[@"setting"]) {
        if ([userDictionary[@"setting"] valueForKey:@"battery_usage"]) {
            return [userDictionary[@"setting"] valueForKey:@"battery_usage"];
        }
    }
    return nil;
}
#pragma mark - Stret view Action Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
}

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
   // NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
  //  NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [_mapView.projection coordinateForPoint:touchPoint];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    // Zoom in one zoom level
                    [_mapView animateToZoom:12];
                    [_mapView animateToBearing:90];
                    [_mapView animateToViewingAngle:45];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!(CGRectContainsPoint(streetViewBtn.frame, touchPoint))) {
                        if (!isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
//        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapView addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [_mapView.projection coordinateForPoint:point];
    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    // Zoom in one zoom level
                    [_mapView animateToZoom:12];
                    [_mapView animateToBearing:90];
                    [_mapView animateToViewingAngle:45];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
//        });
    });
    [recognizer removeTarget:self action:nil];
}

#pragma mark - Action Method

//
// ----------------------------------------------------------------------------------------------
// calltoUpdateUserLocation
- (void)calltoUpdateUserLocation {
   // NSLog(@"API Called Time:%@", [NSString stringWithFormat:@"%@", [NSDate date]]);

    NSString *urlStr = [NSString stringWithFormat:@"%@%@", kKeyClassSearchUsersById, [userDictionary objectForKey:kKeyId]];
    [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

//
// ----------------------------------------------------------------------------------------------
// actionOnBack
- (void)actionOnBack {
    // Pop controller
    // [self.tabBarController.tabBar setHidden:NO];
    [_updateTimer invalidate];
    [self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:NO];
    [self.navigationController popViewControllerAnimated:NO];

}

//
// ----------------------------------------------------------------------------------------------
// stopTimer
- (void)stopTimer {
    [_updateTimer invalidate];
}

#pragma mark - Notification Method

// --------------------------------------------------------------------------------
// getupdatedLocationSucceed:
- (void)getupdatedLocationSucceed:(NSNotification *)inNotify {
    NSArray *responseArray = [inNotify object];
    if (responseArray && responseArray.count > 0) {
        userDictionary = [responseArray objectAtIndex:0];
    }

//    [self calculateTimerDelay];

    // Set marker to map
    [self displayData];

}


// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:
- (void)getupdatedLocationFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
