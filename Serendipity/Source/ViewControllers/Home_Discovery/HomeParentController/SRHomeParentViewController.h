#import <UIKit/UIKit.h>
#import "ParentViewController.h"
#import "SlideNavigationController.h"
#import "SRServerConnection.h"

#define kClassRadar @"Radar"
#define kClassMap @"Map"
#define kClassList @"List"

@interface SRHomeParentViewController : ParentViewController // <UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate>
{
	// Instance variable
    NSArray *notificationFilterArr;

	NSString *subClass;
	BOOL isGroupType;
    BOOL isDisappear;

	UIScrollView *popOverScrlView;
	CGPoint coordinates;

    UIButton *badgeBtn;
    NSInteger selectedDegree;
   
	// Server
	SRServerConnection *server;
}

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *bckBarImgView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewConnections;
@property (strong, nonatomic) IBOutlet UILabel *lblConnections;
@property (strong, nonatomic) IBOutlet UIButton *btnConnection;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCommunity;
@property (strong, nonatomic) IBOutlet UILabel *lblCommunity;
@property (strong, nonatomic) IBOutlet UIButton *btnCommunity;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCommunityDropDown;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewFamily;
@property (strong, nonatomic) IBOutlet UILabel *lblFamily;
@property (strong, nonatomic) IBOutlet UIButton *btnFamily;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewGroup;
@property (strong, nonatomic) IBOutlet UILabel *lblGroup;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewGroupDropDown;
@property (strong, nonatomic) IBOutlet UIButton *btnGroup;

@property (strong, nonatomic) IBOutlet UIView *topBarView;


// Other Properties

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
            className:(NSString *)inClassName;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

-(void)refreshControllerData;

//- (IBAction)actionOnTopBarBtnClick:(UIButton *)sender;
//- (IBAction)actionOnCommunityViewBtnClick:(UIButton *)sender;
@end
