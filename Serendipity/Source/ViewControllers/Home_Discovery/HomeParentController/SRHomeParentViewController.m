#import "SRModalClass.h"
#import "SRDiscoveryRadarViewController.h"
#import "SRMenuViewController.h"
#import "SRSignupViewController.h"
#import "SRDiscoveryCustomCell.h"
#import "SREventViewCell.h"
#import "SRHomeParentViewController.h"
#import "DBManager.h"

@implementation SRHomeParentViewController

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
            className:(NSString *)inClassName {
    // Call super
    self = [super initWithNibName:nibNameOrNil bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
        subClass = inClassName;

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationSucceed:)
                              name:kKeyNotificationGetNotificationSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationFailed:)
                              name:kKeyNotificationGetNotificationFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationSucceed:)
                              name:kKeyChatMessageRecieved object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationSucceed:)
                              name:kKeyNotificationMessageStateUpdated object:nil];
        [defaultCenter addObserver:self selector:@selector(getBadgeCount:) name:kKeyNotificationBadgeCountUpdated object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(refreshView:)
                              name:kKeyNotificationRefreshList
                            object:nil];
    }
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark


- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    //  [server.loggedInFbUserInfo removeAllObjects];

    // Prepare view design
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    // [SlideNavigationController sharedInstance].portraitSlideOffset = 60;

    // Navigation bar look and fill
    if ([subClass isEqualToString:kClassRadar]) {
        [SRModalClass setNavTitle:NSLocalizedString(@"tab.radar.title", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    } else if ([subClass isEqualToString:kClassMap]) {
        [SRModalClass setNavTitle:NSLocalizedString(@"tab.map.title", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    } else {
        [SRModalClass setNavTitle:NSLocalizedString(@"tab.list.title", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }
    UIButton *btnMenu = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"ic_menu.png"] forViewNavCon:self offset:18];
    [btnMenu addTarget:self action:@selector(actionOnMenu:) forControlEvents:UIControlEventTouchUpInside];


    // Customize look and fill
    self.imgViewConnections.layer.cornerRadius = self.imgViewConnections.frame.size.width / 2;
    //self.imgViewConnections.layer.masksToBounds = YES;
    self.imgViewCommunity.layer.cornerRadius = self.imgViewCommunity.frame.size.width / 2.0;
    self.imgViewCommunity.layer.masksToBounds = YES;
    self.imgViewGroup.layer.cornerRadius = self.imgViewGroup.frame.size.width / 2;
    self.imgViewGroup.layer.masksToBounds = YES;
    self.imgViewFamily.layer.cornerRadius = self.imgViewGroup.frame.size.width / 2;
    self.imgViewFamily.layer.masksToBounds = YES;

    // Set selected defualt values
    [self.btnConnection setSelected:YES];
    [self.btnCommunity setSelected:YES];
    [self.btnFamily setSelected:YES];
    [self.btnGroup setSelected:YES];

    selectedDegree = 6;

    // Add Badge Icon on Navigation Bar
    badgeBtn = [[UIButton alloc] initWithFrame:CGRectMake(25, 5, 20, 20)];
    badgeBtn.backgroundColor = [UIColor orangeColor];
    badgeBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:14];
    [badgeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    badgeBtn.layer.cornerRadius = badgeBtn.frame.size.width / 2;
    badgeBtn.layer.masksToBounds = YES;
    badgeBtn.userInteractionEnabled = NO;
    [self.navigationController.navigationBar addSubview:badgeBtn];
    badgeBtn.hidden = NO;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];

    (APP_DELEGATE).isZero = NO;
    isDisappear = NO;

//    if ([subClass isEqualToString:kClassList]) {
        (APP_DELEGATE).topBarView.bckBarImgView.backgroundColor = [UIColor orangeColor];
//    } else {
//        (APP_DELEGATE).topBarView.bckBarImgView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.64];
//    }

    // Display counts
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@)", @"0"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@ AND reference_type_id != %@)", @"0", @"14"];

    notificationFilterArr = [server.notificationArr filteredArrayUsingPredicate:predicate];

    // Set Title to Notification Button
    NSInteger count = [[DBManager getSharedInstance] getUnreadMsgCount];
    count = count + [(APP_DELEGATE) getUnreadTrackingLocationCount];
    count = count + [notificationFilterArr count];
    if (count == 0) {
        badgeBtn.hidden = YES;
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    } else {
        badgeBtn.hidden = NO;
        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
        [badgeBtn setTitle:str forState:UIControlStateNormal];
        if (str.length > 2) {
            badgeBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        }
        [UIApplication sharedApplication].applicationIconBadgeNumber = count;
    }
    // Bring to front
    [self.navigationController.navigationBar bringSubviewToFront:badgeBtn];
}

// -----------------------------------------------------------------------------------------------
// viewWillDisappear:

- (void)refreshView:(NSNotification *)notification {
    if (isDisappear == NO) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSInteger count = [[DBManager getSharedInstance] getUnreadMsgCount];
            count = count + [(APP_DELEGATE) getUnreadTrackingLocationCount];
            count = count + [notificationFilterArr count];
            if (count == 0) {
                badgeBtn.hidden = YES;
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            } else {
                badgeBtn.hidden = NO;
                NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                [badgeBtn setTitle:str forState:UIControlStateNormal];
                if (str.length > 2) {
                    badgeBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
                }
                [UIApplication sharedApplication].applicationIconBadgeNumber = count;
            }
        });
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:NO];
    isDisappear = YES;
    badgeBtn.hidden = YES;
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// actionOnMenu:

- (void)actionOnMenu:(id)sender {
    SRMenuViewController *menuCon = [[SRMenuViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
    menuCon.delegate = self;
    badgeBtn.hidden = YES;
//    (APP_DELEGATE).topBarView.hidden = YES;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;

    [APP_DELEGATE hideActivityIndicator];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:menuCon animated:YES];
    self.hidesBottomBarWhenPushed = NO;
    badgeBtn.hidden = YES;

}

#pragma mark Notification Method

- (void)getBadgeCount:(NSNotification *)inNotify {
    if (isDisappear == NO) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@ AND reference_type_id != %@)", @"0", @"14"];;
        notificationFilterArr = [server.notificationArr filteredArrayUsingPredicate:predicate];

        // Set Title to Notification Button
        NSInteger count = [[DBManager getSharedInstance] getUnreadMsgCount];
        count = count + [(APP_DELEGATE) getUnreadTrackingLocationCount];
        count = count + [notificationFilterArr count];
        if (count == 0) {
            badgeBtn.hidden = YES;
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        } else {
            badgeBtn.hidden = NO;
            NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
            [badgeBtn setTitle:str forState:UIControlStateNormal];
            if (str.length > 2) {
                badgeBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
            }
            [UIApplication sharedApplication].applicationIconBadgeNumber = count;
        }
        // Bring to front
        [self.navigationController.navigationBar bringSubviewToFront:badgeBtn];
    }
}

- (void)getNotificationSucceed:(NSNotification *)inNotify {
    // Display counts
    if (!isDisappear) {
        __block NSArray *filterArr;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@)", @"0"];
            //HS:Noti
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@ AND reference_type_id != %@)", @"0", @"14"];

            if (!server.notificationArr) {
                return;
            }
            filterArr = [server.notificationArr filteredArrayUsingPredicate:predicate];
            dispatch_async(dispatch_get_main_queue(), ^{
                // Set Title to Notification Button
                NSInteger count = [[DBManager getSharedInstance] getUnreadMsgCount];
                count = count + [(APP_DELEGATE) getUnreadTrackingLocationCount];
                count = count + [filterArr count];

                if (count == 0) {
                    badgeBtn.hidden = YES;
                    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                } else {
                    if ([self.navigationController.visibleViewController isKindOfClass:[SRHomeParentViewController class]]) {
                        //exists
                        badgeBtn.hidden = NO;
                    } else {
                        badgeBtn.hidden = YES;
                    }

                    NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                    [badgeBtn setTitle:str forState:UIControlStateNormal];
                    if (str.length > 2) {
                        badgeBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
                    }
                    [UIApplication sharedApplication].applicationIconBadgeNumber = count;
                }
            });
        });
    }
}

// --------------------------------------------------------------------------------
// getNotificationFailed:

- (void)getNotificationFailed:(NSNotification *)inNotify {
}

-(void)refreshControllerData {
    
}

@end
