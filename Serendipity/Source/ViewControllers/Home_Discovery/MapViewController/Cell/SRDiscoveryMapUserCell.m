//
//  SRDiscoveryMapUserCell.m
//  Serendipity
//
//  Created by Long Lee on 10/21/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRDiscoveryMapUserCell.h"
#import "FrequencyUpdateView.h"
#import "SRModalClass.h"

@interface SRDiscoveryMapUserCell()

@property (weak, nonatomic) IBOutlet UIImageView *imvAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtvAddress;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation SRDiscoveryMapUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imvAvatar.layer.cornerRadius = 22;
    self.containerView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithUserInfo:(NSDictionary *)info {
    NSDictionary *cellDict = info;
//    NSString *lName = [cellDict[kKeyLastName] capitalizedString];
//    lName = [lName substringToIndex:1];
//    NSString *userName = [NSString stringWithFormat:@"%@ %@.", cellDict[kKeyFirstName], lName];
    
    NSDate* currentDate = [SRModalClass returnFullDate:[[info valueForKey:kKeyLocation] valueForKey:kKeyLastSeenDate]];
    
    NSDateFormatter *formator = [[NSDateFormatter alloc] init];
    
    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    
    [formator setTimeZone:utcTimeZone];
    
    [formator setDateFormat:@"MMMM d 'at' hh:mm a"];
    
    self.lblName.text = [NSString stringWithFormat:@"%@%@ %@ near", [self timeDiffrenceFromLastOnline:info], [formator stringFromDate:currentDate], timeZoneLocal.abbreviation];
    self.lblAddress.text = [cellDict[kKeyLocation][kKeyLiveAddress] capitalizedString];
    // Imageview
    if ([cellDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        
        if ([cellDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [cellDict[kKeyProfileImage] objectForKey:kKeyImageName];
            if ([imageName length] > 0) {
                
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                [self.imvAvatar sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                self.imvAvatar.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            self.imvAvatar.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    } else
        self.imvAvatar.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    
    self.imvAvatar.contentMode = UIViewContentModeScaleAspectFill;
    self.imvAvatar.clipsToBounds = YES;
    self.imvAvatar.layer.borderWidth = 2.0;
    self.containerView.layer.borderWidth = 2.0;
    
    [self setBorderColorForUser:info];
}

- (NSString *)getBatterySettings :(NSDictionary *)info {
    if (info[@"setting"]) {
        if ([info[@"setting"] valueForKey:kkeyBatteryUsage]) {
            return [info[@"setting"] valueForKey:kkeyBatteryUsage];
        }
    }
    return nil;
}


-(void)setBorderColorForUser:(NSDictionary *)info{

    BOOL broadcastLocation = [[info[@"setting"] valueForKey:@"broadcast_location"] boolValue];
    NSString *batteryUsage = [self getBatterySettings:info];
    if (!broadcastLocation) {
        
    } else if (batteryUsage) {
        batteryUsage = [batteryUsage lowercaseString];
        self.imvAvatar.layer.borderColor     = [APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage].CGColor;
        self.containerView.layer.borderColor = [APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage].CGColor;
    }
}


- (NSString *)timeDiffrenceFromLastOnline:(NSDictionary*)info {
    NSString *lastUpdateTime = [info[@"location"] valueForKey:@"location_captured_time"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSDate *dateNow = [NSDate date];
    NSDate *updateTime = [dateFormat dateFromString:lastUpdateTime];//[NSDate dateWithTimeIntervalSinceNow: longLongValue]];
    NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:updateTime];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    int daysOffline = (int) (hoursBetweenDates / 24);
    
    int years = daysOffline / 365;
    int months = daysOffline / 31;
    
    if (years > 0) {
        return [NSString stringWithFormat:@"%dy", years];
    } else if (months > 0) {
        return [NSString stringWithFormat:@"%dm", months];
    } else if (daysOffline >= 21 && daysOffline < 30) {
        return @"(3w) ";
    } else if (daysOffline >= 14 && daysOffline < 21) {
        return @"(2w) ";
    } else if (daysOffline >= 7 && daysOffline < 14) {
        return @"(1w) ";
    } else if (daysOffline == 6) {
        return @"(6d) ";
    } else if (daysOffline == 5) {
        return @"(5d) ";
    } else if (daysOffline == 4) {
        return @"(4d) ";
    } else if (daysOffline == 3) {
        return @"(96h) ";
    } else if (daysOffline == 2) {
        return @"(48h) ";
    } else if (hoursBetweenDates >= 24 && daysOffline < 2) {
        return @"(24h) ";
    }
    return @"";
}


@end
