//
//  SRDiscoveryMapUserCell.h
//  Serendipity
//
//  Created by Long Lee on 10/21/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRDiscoveryMapUserCell : UITableViewCell

- (void)updateWithUserInfo:(NSDictionary *)info;

@end

