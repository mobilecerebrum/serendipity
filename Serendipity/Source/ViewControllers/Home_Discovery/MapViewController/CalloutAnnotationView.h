//
//  CalloutAnnotationView.h
//  CustomCalloutSample
//
//  Created by tochi on 11/05/17.
//  Copyright 2011 aguuu,Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@protocol CalloutAnnotationViewDelegate;
@interface CalloutAnnotationView : MKAnnotationView

@property (weak, nonatomic) IBOutlet UIImageView *toolTipImage;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UIImageView *favUserImage;
@property (weak, nonatomic) IBOutlet UIImageView *invisibleUserImage;
@property (strong, nonatomic) IBOutlet UIButton *pingButton;
@property (strong, nonatomic) IBOutlet UIButton *compassButton;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupation;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblDegree;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewGrp;
@property (weak, nonatomic) IBOutlet UIButton *btnGrpJoin;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *button;

@property (strong, nonatomic) NSDictionary *userDict;

@property (nonatomic, assign) id<CalloutAnnotationViewDelegate> delegate;

#pragma mark-
#pragma mark- Download Methods
#pragma mark-

//- (void)downloadImage:(NSDictionary *) dataDict;

@end

@protocol CalloutAnnotationViewDelegate
@required
- (void)calloutButtonClicked:(NSString *)title;

@end
