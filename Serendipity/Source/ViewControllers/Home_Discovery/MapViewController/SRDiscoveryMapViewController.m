#import "SRModalClass.h"
#import "CalloutAnnotation.h"
#import "SRUserTabViewController.h"
#import "SRMapProfileView.h"
#import "SRDiscoveryMapViewController.h"
#import "REVClusterPin.h"
#import "REVClusterAnnotationView.h"
#import "UIImage+animatedGIF.h"
#import "SRPageControlViewController.h"
#import "SRMapViewController.h"
#import "SRProfileTabViewController.h"
#import "SRPanoramaViewController.h"
#import "RadarDistanceClass.h"
#import "SRDiscoveryMapUserCell.h"

#define degreesToRadians(x) (M_PI * x / 180.0)

const double zoomLevel = 0.007812;


@interface mapDistanceClass : NSObject

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *distance;
@property(nonatomic, strong) NSString *dataId;
@property(strong, nonatomic) CLLocation *myLocation;

@end

@implementation mapDistanceClass

@end


@implementation SRDiscoveryMapViewController

#pragma mark Private Method

- (void)initMapHint {
    
    int screenWidth = [[UIScreen mainScreen] bounds].size.width;
    mapLegend.frame = CGRectMake(0, self.progressContainerView.frame.origin.y - mapLegend.frame.size.height + 5, screenWidth, mapLegend.frame.size.height);
    [mapLegend layoutSubviews];
}

- (void)showFirstHint {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"mapHintShown"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    mapLegend.hidden = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self adjustHints];
        [self initMapHint];
        mapLegend.hidden = YES;
    });
}

- (void)adjustHints {
    NSArray<UIView *> *views = mapLegend.subviews;
    float xPos = 0;
    int screenWidth = [[UIScreen mainScreen] bounds].size.width;
    for (UIView *v in views) {
        if (v.tag == 1) {
            CGRect tempFrame = v.frame;
            tempFrame.size.width = (screenWidth / 4) + 0.8;
            tempFrame.origin.x = xPos;
            v.frame = tempFrame;
            xPos += (screenWidth / 4) + 0.8;
        } else if (v.tag == 2) {
            CGRect tempFrame = v.frame;
            tempFrame.size.width = screenWidth;
            v.frame = tempFrame;
        }
    }
}

- (void)updateMapLegendSize {
    if (mapLegend.isHidden) {
        mapLegend.hidden = NO;
        [self adjustHints];
        //        __block CGRect frame = mapLegend.frame;
        //        frame.size.height = 100;
        //        [UIView animateWithDuration:0.5 animations:^{
        //            mapLegend.frame = frame;
        //
        //        }completion:^(BOOL finished) {
        //        }];
        
    } else {
        mapLegend.hidden = YES;
        //        __block CGRect frame = mapLegend.frame;
        //        frame.origin.y = 0;
        //        frame.size.height = 0;
        //        [UIView animateWithDuration:0.5 animations:^{
        //            mapLegend.frame = frame;
        //
        //        }completion:^(BOOL finished) {
        //            mapLegend.hidden = YES;
        //        }];
        //        [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        //            mapLegend.frame = CGRectMake(0, mapLegend.frame.origin.y, mapLegend.frame.size.width, 0);
        //        } completion:^(BOOL finished) {
        //            mapLegend.hidden = YES;
        //        }];
        
    }
}

- (void)addHintAreaAction {
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateMapLegendSize)];
    tap.numberOfTapsRequired = 1;
    [mapLegendHint addGestureRecognizer:tap];
    [self adjustHints];
}

- (void)hideScrollView:(UITapGestureRecognizer *)gesture {
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;
}

- (void)addViewsOnScrollView:(NSArray *)inArr {
    CGRect frame;
    
    for (UIView *subview in [self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (NSUInteger i = 0; i < [inArr count]; i++) {
        NSDictionary *userDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i == 0) {
            frame = CGRectMake(0, 11, 130, 200);
        } else {
            frame = CGRectMake(frame.origin.x + 126, 11, 130, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;
        
        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(8.0, 8.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = annotationView.userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        annotationView.userProfileImage.layer.mask = maskLayer;
        
        if (userDict[kKeyProfileImage] != nil && [userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if (![imageName isKindOfClass:[NSNull class]]) {
                    if ([imageName length] > 0) {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                        [annotationView.userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                    } else {
                        annotationView.userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    }
                }
            } else {
                annotationView.userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                annotationView.userProfileImage.clipsToBounds = YES;
            }
        } else {
            annotationView.userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            annotationView.userProfileImage.clipsToBounds = YES;
        }
        //Favourite
        if ([userDict[kKeyIsFavourite] boolValue] == true) {
            annotationView.favUserImage.hidden = FALSE;
        } else {
            annotationView.favUserImage.hidden = TRUE;
        }
        
        //Invisible
        if ([userDict[kKeyIsInvisible] boolValue] == true) {
            annotationView.invisibleUserImage.hidden = FALSE;
        } else {
            annotationView.invisibleUserImage.hidden = TRUE;
        }
        
        annotationView.lblProfileName.text = [userDict valueForKey:kKeyFirstName];
        if ([[userDict valueForKey:kKeyOccupation] length] <= 0) {
            annotationView.lblOccupation.text = @"None";
        } else {
            annotationView.lblOccupation.text = userDict[kKeyOccupation];
        }
        double distance = [userDict[kKeyDistance] floatValue];
        annotationView.lblDistance.text = [NSString stringWithFormat:@"%.1f"@" %@", distance, distanceUnit];
        
        if ([[userDict valueForKey:kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            annotationView.lblDegree.text = [NSString stringWithFormat:@" %ld°", (long) degree];
        } else {
            annotationView.lblDegree.text = @" 6°";
        }
        
        // Add Shadow to btnPing
        [annotationView.pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [annotationView.pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [annotationView.pingButton.layer setShadowOpacity:8.5];
        [annotationView.pingButton addTarget:self action:@selector(pingButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //Add Compass Button
        [annotationView.compassButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [annotationView.compassButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [annotationView.compassButton.layer setShadowOpacity:8.5];
        
        if (userDict[kKeyLocation] != nil && ![userDict[kKeyLocation] isEqual:[NSNull null]]) {
            CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                server.myLocation.coordinate.longitude};
            
            NSDictionary *userLocDict = userDict[kKeyLocation];
            CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};
            
            [annotationView.compassButton addTarget:self action:@selector(compassButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
            if (directionValue == kKeyDirectionNorth) {
                annotationView.lblCompassDirection.text = @"N";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionEast) {
                annotationView.lblCompassDirection.text = @"E";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionSouth) {
                annotationView.lblCompassDirection.text = @"S";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionWest) {
                annotationView.lblCompassDirection.text = @"W";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionNorthEast) {
                annotationView.lblCompassDirection.text = @"NE";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionNorthWest) {
                annotationView.lblCompassDirection.text = @"NW";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionSouthEast) {
                annotationView.lblCompassDirection.text = @"SE";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionSoutnWest) {
                annotationView.lblCompassDirection.text = @"SW";
                [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
            }
        }
        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [annotationView.userProfileImage addGestureRecognizer:annotationTapGesture];
        annotationView.userDict = userDict;
        
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }
        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else {
            self.scrollView.scrollEnabled = NO;
        }
        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
    }
}

- (UIImage *)makeRoundedImage:(UIImage *)image radius:(float)radius {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    radius = radius / 2;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

- (void)plotUsersOnMapView :(BOOL)isInMainThread{
    NSMutableArray *pins = [NSMutableArray array];
    CLLocationCoordinate2D coordinate;
    for (NSDictionary *dict in nearByUsersList) {
        if (isGroupTrackUsers) {
            lastSelectedPin = pin;
            for (id key in dict) {
                NSArray *arrdata = [[NSArray alloc] init];
                arrdata = dict[key];
                NSDictionary *dictCordinate = arrdata[0];
                if (dictCordinate[kKeyLocation] != nil && ![dictCordinate[kKeyLocation] isEqual:[NSNull null]]) {
                    float lat = [[dictCordinate[kKeyLocation] objectForKey:kKeyLattitude] floatValue];
                    float lon = [[dictCordinate[kKeyLocation] objectForKey:kKeyRadarLong] floatValue];
                    coordinate.latitude = lat;
                    coordinate.longitude = lon;
                    
                    if ([arrdata count] > 1) {
                        pin.nodes = arrdata;
                    }
                    CLLocationCoordinate2D newCoord = coordinate;
                    pin = [[REVClusterPin alloc] init];
                    pin.title = [NSString stringWithFormat:@"%@", key];
                    pin.coordinate = newCoord;
                    [pins addObject:pin];
                }
            }
        } else {
            if ([dict isKindOfClass:[NSDictionary class]]) {
                if (dict[kKeyLocation] != nil && ![dict[kKeyLocation] isEqual:[NSNull null]]) {
                    float lat = [[dict[kKeyLocation] objectForKey:kKeyLattitude] floatValue];
                    float lon = [[dict[kKeyLocation] objectForKey:kKeyRadarLong] floatValue];
                    coordinate.latitude = lat;
                    coordinate.longitude = lon;
                    
                    CLLocationCoordinate2D newCoord = coordinate;
                    pin = [[REVClusterPin alloc] init];
                    pin.title = [dict valueForKey:kKeyId];
                    pin.coordinate = newCoord;
                    [pins addObject:pin];
                }
            }
        }
    }
    [self.mapView setParam:isGroupTrackUsers GroupList:nearByUsersList];
    [self.mapView addAnnotations:pins];
          
    
   
    
    if ([pins count] == 1 && isGroupTrackUsers && isInMainThread) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView setCenterCoordinate:lastSelectedPin.coordinate zoomLevel:0 animated:true];
            CLLocationCoordinate2D coordinate = pin.coordinate;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.mapView setCenterCoordinate:coordinate zoomLevel:0 animated:true];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.mapView setCenterCoordinate:coordinate zoomLevel:13 animated:true];
                });
            });
            
        });
       /* dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.mapView setCenterCoordinate:pin.coordinate zoomLevel:13 animated:true];
        });*/
        
    } else if (isInMainThread){
        [self.mapView showAnnotations:pins animated:true];
    }
    
    // Add my annotation
    myLocation = server.myLocation;
    
    if (pin == nil) {
        pin = [[REVClusterPin alloc] init];
        pin.coordinate = server.myLocation.coordinate;
    }
}

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    
    self = [super initWithNibName:@"SRDiscoveryMapViewController" bundle:nil server:inServer className:kClassMap];
    
    if (self) {
        // Custom initialization
        server = inServer;
        nearByUsersList = [[NSMutableArray alloc] init];
        userProfileViewArr = [[NSMutableArray alloc] init];
        myLocation = server.myLocation;
        latestDistance = kKeyMaximumRadarMile;
        groupUsers = [[NSMutableArray alloc] init];
        
        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getRadarUsersList:)
                              name:kKeyNotificationMapUserChanges object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updatedLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(filterTheMapList:)
                              name:kKeyNotificationMapFilterApplied
                            object:nil];
        
    }
    return self;
}

- (void)dealloc {
    // Dealloc all registered notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark Standard Method
#pragma mark

- (void)viewDidLoad {
    
    [super viewDidLoad];
    (APP_DELEGATE).isMapDisappear = false;
    zoomInBtn.layer.borderWidth = 1.0;
    zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    zoomOutBtn.layer.borderWidth = 1.0;
    zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
    [self.locationToolBar setBackgroundImage:[UIImage new]
                          forToolbarPosition:UIToolbarPositionAny
                                  barMetrics:UIBarMetricsDefault];
    
    MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    buttonItem.customView.frame = CGRectMake(0, 0, 30, 30);
    
    buttonItem.mapView = self.mapView;
    
    self.locationToolBar.items = @[buttonItem];
    
    // ScrollView
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideScrollView:)];
    [self.scrollView addGestureRecognizer:tapGesture];
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;
    
    for (id subView in _slider1.subviews) {
        [subView removeFromSuperview];
    }
    
    //    self.slider1.maximumValue = 21;
    //    self.slider1.minimumValue = 1;
    //    self.slider1.value = 6;
    
    CGFloat distancevalue = 0.0;
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distancevalue = kKeyMaximumRadarMile;
        distanceUnit = kkeyUnitMiles;
    } else {
        distancevalue = kKeyMaximumRadarKm;
        distanceUnit = kkeyUnitKilometers;
    }
    
    latestDistance = (float) distancevalue;
    settingDistance = (float) distancevalue;
    
    self.slider1.maximumValue = (float) distancevalue;
    self.slider1.minimumValue = kKeyMinimumRadarMile;
    self.slider1.value = (float) distancevalue;
    
    
    self.slider1.popUpViewColor = [UIColor colorWithRed:1 green:0.588 blue:0 alpha:1];
    [self.slider1 addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [pinch setDelegate:self];
    [pinch setDelaysTouchesBegan:YES];
    [self.mapView addGestureRecognizer:pinch];
    
    
    myLocation = server.myLocation;
    // Add my annotation
    if (pin == nil) {
        pin = [[REVClusterPin alloc] init];
        pin.coordinate = server.myLocation.coordinate;
    } else {
        pin.coordinate = server.myLocation.coordinate;
    }
    [self.mapView addAnnotation:pin];
    self.mapView.delegate = self;
    mapZoomLevel = latestDistance / 166;
    mapZoomLevel = 18 - mapZoomLevel + 1;
    
    //selected groupUsers TableView
    self.markerBgImage = [[UIImageView alloc] init];
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            //do your code
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [_radarRefreshOverlay setHidden:YES];
    // Add long press guesture to refresh radar centered ProfileImgView
    UILongPressGestureRecognizer *radarLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressRadar:)];
    radarLongPress.minimumPressDuration = 1.0f;
    //[self.mapView addGestureRecognizer:radarLongPress];
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleGesture:)];
    
    [self.mapView addGestureRecognizer:tgr];
    
    self.mapView.minimumClusterLevel = 200000;
    
    [self addHintAreaAction];
    
    [[SRAPIManager sharedInstance] updateToken:server.token];
}

- (void)refreshRadarBtnClick {
    [_radarRefreshOverlay setHidden:YES];
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [APP_DELEGATE showActivityIndicator];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
}

- (void)tapOnRefreshOverlay:(UITapGestureRecognizer *)recognizer {
    //Hide radar refresh overlay
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            //do your code
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [_radarRefreshOverlay setHidden:YES];
}

-(void)refreshControllerData {
    [UIView animateWithDuration:0.0 delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
        
    } completion:^(BOOL finished) {
        
        [_radarRefreshOverlay setHidden:NO];
        if (!_radarRefreshOverlay) {
            // Add view to refresh radar
            _radarRefreshOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
            _radarRefreshOverlay.backgroundColor = [UIColor blackColor];
            _radarRefreshOverlay.alpha = 0.7;
            _radarRefreshOverlay.userInteractionEnabled = TRUE;
            _radarRefreshOverlay.tag = 100000;
            
            // Add update lbl
            UILabel *updateLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 70, SCREEN_HEIGHT - 160, 140, 20)];
            [updateLbl setText:@"Update map now"];
            updateLbl.opaque = true;
            [updateLbl setTextColor:[UIColor whiteColor]];
            [updateLbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:14]];
            [updateLbl setTextAlignment:NSTextAlignmentCenter];
            [_radarRefreshOverlay addSubview:updateLbl];
            UITapGestureRecognizer *tapGestOnRefreshOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRefreshOverlay:)];
            [_radarRefreshOverlay addGestureRecognizer:tapGestOnRefreshOverlay];
            [self.tabBarController.view addSubview:_radarRefreshOverlay];
        }
        // add refresh btn
        UIButton *refreshBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, SCREEN_HEIGHT - 100, 60, 60)];
        refreshBtn.layer.cornerRadius = refreshBtn.frame.size.width / 2;
        refreshBtn.opaque = true;
        [refreshBtn setImage:[UIImage imageNamed:@"update-radar-white.png"] forState:UIControlStateNormal];
        [refreshBtn setBackgroundColor:[UIColor whiteColor]];
        [refreshBtn addTarget:self action:@selector(refreshRadarBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [refreshBtn setUserInteractionEnabled:TRUE];
        [self.tabBarController.view addSubview:refreshBtn];
        [self.tabBarController.view bringSubviewToFront:refreshBtn];
    }];
}

- (void)longPressRadar:(UILongPressGestureRecognizer *)recognizer {
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    (APP_DELEGATE).isOnMap = YES;
    if ((APP_DELEGATE).isReqInProcess == false) {
        (APP_DELEGATE).isTabClicked = YES;
        (APP_DELEGATE).timer = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateTimer object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
        (APP_DELEGATE).timer = YES;
    }
    (APP_DELEGATE).isBackMenu = NO;
    [self initMapHint];
    [self adjustHints];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"mapHintShown"]) {
        [self showFirstHint];
    } else {
        mapLegend.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    self.tabBarController.tabBar.hidden = NO;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
    if ((!isZoom || (APP_DELEGATE).isMapDisappear) && filterDictionary == nil) {
        isZoom = NO;
        CLLocationCoordinate2D centerCoordinate = server.myLocation.coordinate;
        CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
        [self.mapView setCenterCoordinate:centerLocation.coordinate zoomLevel:6 animated:YES];
    }
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        [self.mapView setCenterCoordinate:panoramaLastLoc zoomLevel:5 animated:YES];
    }
    //Get and Set distance measurement unit
//    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
//        distanceUnit = kkeyUnitMiles;
//    } else {
//        distanceUnit = kkeyUnitKilometers;
//    }
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
        self.slider1.maximumValue = kKeyMaximumRadarMile;
    } else {
        distanceUnit = kkeyUnitKilometers;
        self.slider1.maximumValue = kKeyMaximumRadarKm;
    }
    if ((APP_DELEGATE).isMeasurementChangesForMap == true) {
        if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
            latestDistance = kKeyMaximumRadarMile;
        } else {
            latestDistance = kKeyMaximumRadarKm;
        }
        [self.slider1 setValue:latestDistance];
        (APP_DELEGATE).isMeasurementChangesForMap = false;
    }
    
    [self calculateAndGetNewUsers];
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kKeyMapToolTipFlag] boolValue] == 0 || (APP_DELEGATE).isSignUpSuccess) {
        (APP_DELEGATE).tooltipMapViewFlag = YES;
        [APP_DELEGATE toolTipFlagSaveData];
        
        // Show tooltip
        NSArray *titleArray = @[NSLocalizedString(@"lbl.map_tooltip1.txt", @""), NSLocalizedString(@"", @"")];
        NSArray *imgArray = @[@"Map_sd", @"Under-Menu_sd"];
        
        SRPageControlViewController *pageViewController = [[SRPageControlViewController alloc] initWithNibName:@"SRPageControlViewController" bundle:nil];
        pageViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        pageViewController.imageArr = imgArray;
        pageViewController.titleArr = titleArray;
        pageViewController.showTooltipOnView = @"SRDiscoveryMapViewController";
        [self presentViewController:pageViewController animated:NO completion:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    (APP_DELEGATE).isMapDisappear = true;
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden = NO;
    self.progressContainerView.hidden = NO;
    btnSatelite.hidden = NO;
    btnStreet.hidden = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationStopUpdateTimer object:nil];
    
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
}

#pragma mark
#pragma mark - Slider update
#pragma mark

-(int)distanceStep:(int )target currentValue:(float)current {
    NSArray *targets = @[@5, @15, @25, @50, @100, @500, @1000, @1500, @2000, @2500, @5000, @7500, @10000, @12500];
    
    if (current == 0) {
        return 0;
    }
    BOOL isKm = NO;
    
    float stepValue = kKeyMaximumRadarMile / targets.count;
    if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
        targets = @[@5, @15, @25, @50, @100, @500, @1000, @1500, @2000, @2500, @5000, @7500, @10000, @12500, @15000, @17500, @20000];
        stepValue = kKeyMaximumRadarKm / targets.count;
        isKm = YES;
    }

    int index = (int)round(current/stepValue) - 1;
    int mIndex = (int)round(current/(kKeyMaximumRadarMile / targets.count)) - 1;
//    NSlog(@"\n\n\n");
//    NSlog(@"Step value Km --- %f", kKeyMaximumRadarKm / targets.count);
//    NSlog(@"Step value M --- %f", kKeyMaximumRadarMile / targets.count);
//    NSlog(@"Index Km --- %d of %d", index, (int)targets.count);
//    NSlog(@"Index M --- %d of %d", mIndex, (int)targets.count);
//    NSlog(@"Is Km --- %@", isKm ? @"YES":@"NO");
//    NSlog(@"Current --- %f\n\n", current);
    
    if (round(current/stepValue) == 0.000000) {
        [_slider1 setValue:0];
        return 0;
    }
    if (index >= targets.count) {
        index = (int)targets.count - 1;
    }
    return [targets[index] intValue];
}

- (void)sliderValueChange:(UISlider *)Sender {
    isSlide = YES;
    float sliderValue = Sender.value;
    latestDistance = [self distanceStep:5000 currentValue:(int)sliderValue];
    
    //[_slider1 setValue:latestDistance];
   // latestDistance = sliderValue;
  //  latestDistance = [self distanceStep:5000 currentValue:(int)latestDistance];
    [self reloadDataInMap:YES];
    
    if (Sender.value < 100.0){
        mapZoomLevel = 20;
    }else if (Sender.value < 500.0){
        mapZoomLevel = 17;
    }else if (Sender.value < 1000.0){
        mapZoomLevel = 16;
    }else if (Sender.value < 1500.0){
        mapZoomLevel = 14;
    }else if (Sender.value < 2000.0){
        mapZoomLevel = 12;
    }else if (Sender.value < 3000.0){
        mapZoomLevel = 10;
    }else if (Sender.value < 4000.0){
        mapZoomLevel = 10;
    }else if (Sender.value < 5000.0){
        mapZoomLevel = 9;
    }else if (Sender.value < 6000.0){
        mapZoomLevel = 9;
    }else if (Sender.value < 7000.0){
        mapZoomLevel = 8;
    }else if (Sender.value < 8000.0){
        mapZoomLevel = 7;
    }else if (Sender.value < 9000.0){
        mapZoomLevel = 6;
    }else if (Sender.value < 10000.0){
        mapZoomLevel = 5;
    }else if (Sender.value < 12000.0){
        mapZoomLevel = 4;
    }else if (Sender.value < 14000.0){
        mapZoomLevel = 3;
    }else if (Sender.value < 20000.0){
        mapZoomLevel = 1;
    }
    
    //    mapZoomLevel = (Sender.value-2000)/1000;///166;
    if (mapZoomLevel > 0){
        //            mapZoomLevel = 18 - mapZoomLevel + 1;
        
        if (!isGroupTrackUsers) {
            //            CLLocationCoordinate2D centerCoordinate = self.mapView.centerCoordinate;
            //            CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
            
            //            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance((APP_DELEGATE).lastLocation.coordinate, sliderValue, sliderValue);
            //            MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
            //            [self.mapView setRegion:adjustedRegion animated:YES];
            
            //            [self.mapView setCenterCoordinate:(APP_DELEGATE).lastLocation.coordinate zoomLevel:mapZoomLevel animated:YES];
        }
    }
}


- (void)calculateAndGetNewUsers {
    
    if (latestDistance > -1) {
        
        NSMutableArray *filteredArr = [[NSMutableArray alloc] init];
        [filteredArr removeAllObjects];
        
        if (filterDictionary.count > 0) {
            NSMutableArray *filteredTrackUsers = [[NSMutableArray alloc] init];
            if (filterDictionary[kKeyGroupTag]) {
                if ([filterDictionary[kKeyGroupTag] integerValue] == 2) {
                    [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                            if ([[obj valueForKey:@"is_active_connection"] boolValue]) {
                                [filteredTrackUsers addObject:obj];
                            }
                            //                            [filteredTrackUsers addObject:obj];
                        }
                    }];
                    nearByUsersList = [NSArray arrayWithArray:filteredTrackUsers].mutableCopy;
                } else if (![filterDictionary[kKeyGroupTag] boolValue]) {
                    [filteredArr removeAllObjects];
                    if ([filterDictionary[kKeyGroupType] isEqualToString:@"All Connections"]) {
                        
                        [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                            if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                                [filteredTrackUsers addObject:obj];
                            }
                        }];
                        
                        nearByUsersList = [NSArray arrayWithArray:filteredTrackUsers].mutableCopy;
                    }
                } else if ([filterDictionary[kKeyGroupType] isEqualToString:@"Family"]) {
                    [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && [[obj objectForKey:kKeyFamily] isKindOfClass:[NSDictionary class]] && [[obj objectForKey:@"is_tracking"] boolValue]) {
                            [filteredTrackUsers addObject:obj];
                        }
                    }];
                    
                    nearByUsersList = [NSArray arrayWithArray:filteredTrackUsers].mutableCopy;
                }
            } else if ([grpNotifiArr count] > 0) {
                [filteredArr removeAllObjects];
                [filteredArr addObjectsFromArray:grpNotifiArr];
                nearByUsersList = [NSArray arrayWithArray:filteredArr].mutableCopy;
            } else {
                //group_notification
                if (latestDistance <= settingDistance) {
                    
                    for (NSDictionary *dict in sourceArr) {
                        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
                        if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && [updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyIsFamilyMember] integerValue] == 1) {
                            [filteredArr addObject:dict];
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 2) {
                            [filteredArr addObject:dict];
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 3) {
                            //For All
                            [filteredArr addObject:dict];
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 4) {
                            //For Active
                            if ([[updatedDict valueForKey:@"is_active_connection"] boolValue]) {
                                [filteredArr addObject:dict];
                            }
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 5) {
                            //For favourite
                            if ([[updatedDict valueForKey:kKeyIsFavourite] boolValue]) {
                                [filteredArr addObject:dict];
                            }
                        } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree <= 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {
                            
                            [filteredArr addObject:dict];
                        } else if ((APP_DELEGATE).topBarView.selectedDegree == -1 && (APP_DELEGATE).topBarView.btnCommunity.selected) {
                            filterDictionary = nil;
                            [filteredArr addObject:dict];
                        } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && (APP_DELEGATE).topBarView.selectedDegree == 6 && ![dict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
                            [filteredArr addObject:dict];
                        }
                        
                    }
                    
                    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
                    [filteredArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance) {
                            [tempArr addObject:obj];
                        }
                    }];
                    
                    nearByUsersList = [NSArray arrayWithArray:tempArr].mutableCopy;
                }
            }
        } else {
            for (NSDictionary *dict in sourceArr) {
                NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
                
                if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && (APP_DELEGATE).topBarView.btnConnection.selected && ([(APP_DELEGATE).topBarView.lblConnections.text isEqualToString:NSLocalizedString(@"lbl.family.txt", @"")])) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && (APP_DELEGATE).topBarView.btnConnection.selected && ([(APP_DELEGATE).topBarView.lblConnections.text isEqualToString:NSLocalizedString(@"lbl.Connections.txt", @"")])) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && (APP_DELEGATE).topBarView.btnConnection.selected && ([(APP_DELEGATE).topBarView.lblConnections.text isEqualToString:NSLocalizedString(@"lbl.Favourites.txt", @"")])) {
                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree <= 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {
                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected) {
                    
                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.btnGroup.selected) {
                    
                    if ([grpNotifiArr count] > 0) {
                        [filteredArr removeAllObjects];
                        [filteredArr addObjectsFromArray:grpNotifiArr];
                        
                    }
                } else {
                    [filteredArr removeAllObjects];
                    [filteredArr addObjectsFromArray:sourceArr];
                }
            }
            
            NSMutableArray *tempArr = [[NSMutableArray alloc] init];
            [filteredArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance) {
                    [tempArr addObject:obj];
                }
            }];
            
            nearByUsersList = [NSArray arrayWithArray:tempArr].mutableCopy;
        }
        NSInteger count = nearByUsersList.count;
        if (isGroupTrackUsers){
            count = sourceArr.count;
        }
        
        self.lblInPpl.text = [NSString stringWithFormat:@"%ld people in %.f %@", (unsigned long) count, latestDistance, distanceUnit];
    }
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)Sender {
    isSlide = NO;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    isSlide = NO;
    return YES;
}


#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
}

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
   // NSlog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
   // NSlog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.view];
    
    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[self.mapView camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [self.mapView setCamera:newCamera animated:YES];
                    
                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    
                    if (!(CGRectContainsPoint(streetViewBtn.frame, touchPoint))) {
                        if (!isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapView addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.view];
    
    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        //TODO: - crash, need call on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[self.mapView camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    
                    
                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [self.mapView setCamera:newCamera animated:YES];
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}

// --------------------------------------------------------------------------------
// zoomInMap
- (IBAction)zoomInMap {
    zoomInBtn.userInteractionEnabled = NO;
    zoomInBtn.layer.borderColor = [[UIColor orangeColor] CGColor];
    zoomOutBtn.userInteractionEnabled = YES;
    zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    isSlide = NO;
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta = MIN(region.span.latitudeDelta * 2.0, 180.0);
    region.span.longitudeDelta = MIN(region.span.longitudeDelta * 2.0, 180.0);
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)zoomOutMap {
    zoomInBtn.userInteractionEnabled = YES;
    zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    zoomOutBtn.userInteractionEnabled = NO;
    zoomOutBtn.layer.borderColor = [[UIColor orangeColor] CGColor];
    
    isSlide = NO;
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta /= 2.0;
    region.span.longitudeDelta /= 2.0;
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)actionOnBtnClick:(UIButton *)inSender {
    if (inSender.tag == 0) {
        self.mapView.mapType = MKMapTypeSatellite;
        [btnStreet setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    } else {
        self.mapView.mapType = MKMapTypeStandard;
        [btnStreet setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

#pragma mark
#pragma mark
#pragma mark Gesture methods Action

#pragma mark
#pragma mark
#pragma mark Action methods

- (void)reloadDataInMap:(BOOL)isInMainThread{
    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        [self.mapView removeAnnotation:annotation];
    }
    for (UIView *view in userProfileViewArr) {
        [view removeFromSuperview];
    }
    // Remove tracking location views in ios
    NSArray *annotations = [self.mapView annotations];
    for (id annotation in annotations) {
        if ([annotation isKindOfClass:[SRMapProfileView class]]) {
            continue;
        }
        [self.mapView removeAnnotation:annotation];
    }
    
    // Remove all objects
    [nearByUsersList removeAllObjects];
    
    // Check if users are on same distance
    
    [self calculateAndGetNewUsers];
    
    if (isGroupTrackUsers){
        NSMutableArray *usersArr = [[NSMutableArray alloc] initWithArray:sourceArr];
        if (usersArr.count > 0) {
            nearByUsersList = usersArr;
        }
    }
    
    
    // Now plot users
    [self plotUsersOnMapView:isInMainThread];
    isSlide = NO;
}

- (void)showHideNavbar:(id)sender {
    isSlide = YES;
    // write code to show/hide nav bar here
    // check if the Navigation Bar is shown
    if ([self.navigationController.visibleViewController isKindOfClass:[SRDiscoveryMapViewController class]]) {
        if (self.navigationController.navigationBar.hidden == NO) {
            // hide the Navigation Bar
            //  [self.navigationController setNavigationBarHidden: YES animated:YES];
            self.navigationController.navigationBarHidden = YES;
            self.tabBarController.tabBar.hidden = YES;
            self.progressContainerView.hidden = YES;
            btnSatelite.hidden = YES;
            btnStreet.hidden = YES;
            return;
        }
        // if Navigation Bar is already hidden
        else if (self.navigationController.navigationBar.hidden == YES) {
            // Show the Navigation Bar
            self.navigationController.navigationBarHidden = NO;
            self.tabBarController.tabBar.hidden = NO;
            self.progressContainerView.hidden = NO;
            //            (APP_DELEGATE).topBarView.hidden = NO;
            btnSatelite.hidden = NO;
            btnStreet.hidden = NO;
            return;
        }
    }
}
// --------------------------------------------------------------------------------
// actionOnImageTap:

- (void)actionOnImageTap:(UITapGestureRecognizer *)inTap {
}

// --------------------------------------------------------------------------------
// pingButtonAction

- (void)pingButtonAction:(UIButton *)sender {
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    
    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    
    BOOL flag = NO;//Check first if you allow to ping or not
    
    if ([[dictionary valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[dictionary valueForKey:kKeySetting]];
        
        NSUInteger pingStatus = [[setting valueForKey:kKeyallowPings] integerValue];
        if (pingStatus == 1) {
            flag = YES;
        } else if (pingStatus == 2) {
            flag = YES;
        } else if (pingStatus == 3) {
            if ([[dictionary valueForKey:kKeyConnection] isKindOfClass:[NSDictionary class]]) {
                flag = YES;
            }
        } else if (pingStatus == 4) {
            if ([[dictionary valueForKey:kKeyToFavourite] boolValue]) {
                flag = YES;
            }
        } else if (pingStatus == 5) {
            if ([[dictionary valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
                flag = YES;
            }
        } else if (pingStatus == 6) {
            flag = NO;
        }
    }
    
    if (flag) {
        if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];
            
            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];
        //badgeBtn.hidden = YES;
        
        // Set chat view
        SRChatViewController *objSRChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:server.friendProfileInfo];
        
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRChatView animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    } else {
        [SRModalClass showAlert:@"Sorry, this user has disabled chat so they cannot receive any messages."];
    }
}

// --------------------------------------------------------------------------------
// compassButtonAction

- (void)compassButtonAction:(UIButton *)sender {
    // Get profile
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    NSDictionary *dataDict = [SRModalClass removeNullValuesFromDict:calloutView.userDict];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dataDict];
    SRMapViewController *compassView = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:dictionary server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:compassView animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}
// --------------------------------------------------------------------------------
// annotationTapAction:

- (void)annotationTapAction:(UITapGestureRecognizer *)sender {
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];
    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];
        
        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];
    // Show user public profile
    if (![dictionary[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
        SRUserTabViewController *tabCon;
        if ([dictionary[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
        } else {
            tabCon = [[SRUserTabViewController alloc] initWithNibName:@"DisabledDegreeSeperation" bundle:nil profileData:nil server:server];
        }
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:tabCon animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }
}

#pragma mark
#pragma mark MapView Delegates
#pragma mark


- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude];
    [geocoder reverseGeocodeLocation:loc
                   completionHandler:^(NSArray *placemarks, NSError *error) {
        placemark = placemarks[0];
        
    }];
    //    if (isGroupTrackUsers) {
    //        CGPoint p = [self.mapView convertCoordinate:selectedMarker.annotation.coordinate toPointToView:selectedMarker];
    //
    //        CGRect frame;
    ////        if (groupUsers.count >= 4)
    ////        {
    ////            frame= CGRectMake(p.x - 78, p.y - 240, 150, 210 );
    ////        }
    ////        else
    //        frame = CGRectMake(p.x - 78, p.y - 38 * groupUsers.count - 40, 150, 38 * groupUsers.count);
    //        if (markerTapWindow == nil) {
    //            markerTapWindow = [[SRMapWindowView alloc] initWithFrame:frame];
    //        } else
    //            markerTapWindow.frame = frame;
    //    }
    [self.mapView bringSubviewToFront:markerTapWindow];
    
}


- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    isZoom = YES;
    if (!(zoomInBtn.userInteractionEnabled)) {
        zoomInBtn.userInteractionEnabled = YES;
        zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    }
    if (!(zoomOutBtn.userInteractionEnabled)) {
        zoomOutBtn.userInteractionEnabled = YES;
        zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    }
    
    MKZoomScale currentZoomScale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
    if (currentZoomScale > zoomLevel) {
        if (!isCheckZoomLvl) {
            isCheckZoomLvl = YES;
            for (id <MKAnnotation> annotation in self.mapView.annotations) {
                if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                } else {
                    [self.mapView removeAnnotation:annotation];
                }
            }
            for (UIView *view in userProfileViewArr) {
                [view removeFromSuperview];
            }
            isZoomIn = YES;
        }
        isCheckZoomLvl = YES;
    } else {
        if (isCheckZoomLvl) {
            isCheckZoomLvl = NO;
            for (id <MKAnnotation> annotation in self.mapView.annotations) {
                if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                } else {
                    [self.mapView removeAnnotation:annotation];
                }
            }
            for (UIView *view in userProfileViewArr) {
                [view removeFromSuperview];
            }
            isZoomIn = NO;
        }
    }
    if (!isSlide) {
        isSlide = YES;
        mapZoomLevel = [mapView zoomLevel];
        if (mapZoomLevel == 1) {
            mapZoomLevel = 0;
        }
        // self.slider1.value = 30000 - (mapZoomLevel * 166);
    }
    //    if (isGroupTrackUsers) {
    [self reloadDataInMap:NO];
    //    }
    [self.mapView bringSubviewToFront:markerTapWindow];
}

- (NSInteger)getintDirection:(NSString *)strDirection {
    int direction = 0;
    if (![strDirection isEqual:[NSNull null]]) {
        direction = [strDirection intValue];
    }
    if ((direction <= 360 && direction >= 338) || (direction >= 0 && direction <= 21)) {
        return kKeyDirectionNorth;
    }
    if (direction >= 22 && direction <= 66) {
        return kKeyDirectionNorthEast;
    }
    if (direction >= 67 && direction <= 111) {
        return kKeyDirectionEast;
    }
    if (direction >= 112 && direction <= 156) {
        return kKeyDirectionSouthEast;
    }
    if (direction >= 157 && direction <= 202) {
        return kKeyDirectionSouth;
    }
    if (direction >= 203 && direction <= 247) {
        return kKeyDirectionSoutnWest;
    }
    if (direction >= 248 && direction <= 292) {
        return kKeyDirectionWest;
    }
    if (direction >= 293 && direction <= 337) {
        return kKeyDirectionNorthWest;
    }
    return direction;
}

- (SRMapProfileView *)getProfileView:(NSInteger)directionValue profileInfo:(NSDictionary *)info {
    SRMapProfileView *markerWindow = [[SRMapProfileView alloc] init];
    int nibIndex = 0;
    NSArray *nibArray;
    
    if (directionValue == kKeyDirectionNorth) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[2];
        nibIndex = 2;
        markerWindow.imgEast.hidden = YES;
        markerWindow.imgWest.hidden = YES;
        markerWindow.imgNorth.hidden = YES;//NO;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionNorthEast) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[3];
        nibIndex = 3;
        markerWindow.imgEast.hidden = YES;//NO;
        markerWindow.imgWest.hidden = YES;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionEast) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[3];
        nibIndex = 3;
        markerWindow.imgEast.hidden = YES;//NO;
        markerWindow.imgWest.hidden = YES;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionSouthEast) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[3];
        nibIndex = 3;
        markerWindow.imgEast.hidden = YES;//NO;
        markerWindow.imgWest.hidden = YES;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionSouth) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:
                    self                           options:nil];
        markerWindow = nibArray[3];
        nibIndex = 3;
        markerWindow.imgEast.hidden = YES;
        markerWindow.imgWest.hidden = YES;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;//NO;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionSoutnWest) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[2];
        nibIndex = 2;
        markerWindow.imgEast.hidden = YES;
        markerWindow.imgWest.hidden = YES;//NO;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionWest) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[2];
        nibIndex = 2;
        markerWindow.imgEast.hidden = YES;
        markerWindow.imgWest.hidden = YES;//NO;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else if (directionValue == kKeyDirectionNorthWest) {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[2];
        nibIndex = 2;
        markerWindow.imgEast.hidden = YES;
        markerWindow.imgWest.hidden = YES;//NO;
        markerWindow.imgNorth.hidden = YES;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    } else {
        nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        markerWindow = nibArray[2];
        nibIndex = 2;
        markerWindow.imgEast.hidden = YES;
        markerWindow.imgWest.hidden = YES;
        markerWindow.imgNorth.hidden = YES;//NO;
        markerWindow.imgSouth.hidden = YES;
        markerWindow.imgSE.hidden = YES;
        markerWindow.imgNE.hidden = YES;
        markerWindow.imgSW.hidden = YES;
        markerWindow.imgNW.hidden = YES;
    }
    markerWindow.layer.cornerRadius = 5.0;
    markerWindow.clipsToBounds = YES;
    markerWindow.bgImg.layer.cornerRadius = 7.0;
    markerWindow.bgImg.layer.masksToBounds = YES;
    
    markerWindow.backImg.layer.cornerRadius = 7.0;
    markerWindow.backImg.layer.borderWidth = 5.0;
    
    
    NSString* batteryUsage = [self getBatterySettings:info];
    [markerWindow.backImg.layer setBorderColor:[[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage] CGColor]];
    [markerWindow.backImg setBackgroundColor:[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage]];
    
    [self changeColorIfUserOffline:info onView:markerWindow.backImg];
    
    markerWindow.backImg.layer.masksToBounds = YES;
    
    markerWindow.collectionView.hidden = YES;
    
    if (nibIndex == 2) {
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:markerWindow.userProfileImg.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
//
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.view.bounds;
//        maskLayer.path = maskPath.CGPath;
//        markerWindow.userProfileImg.layer.mask = maskLayer;
        
        UIBezierPath *maskPaths = [UIBezierPath bezierPathWithRoundedRect:markerWindow.bgImg.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        CAShapeLayer *maskLayers = [[CAShapeLayer alloc] init];
        maskLayers.frame = self.view.bounds;
        maskLayers.path = maskPaths.CGPath;
        markerWindow.bgImg.layer.mask = maskLayers;
        
//        UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:markerWindow.img2.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
//
//        CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
//        maskLayer1.frame = self.view.bounds;
//        maskLayer1.path = maskPath1.CGPath;
//        markerWindow.img2.layer.mask = maskLayer1;
//
//        UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:markerWindow.img4.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
//
//        CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
//        maskLayer2.frame = self.view.bounds;
//        maskLayer2.path = maskPath2.CGPath;
//        markerWindow.img4.layer.mask = maskLayer2;
    } else if (nibIndex == 3) {
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:markerWindow.userProfileImg.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.view.bounds;
//        maskLayer.path = maskPath.CGPath;
//        markerWindow.userProfileImg.layer.mask = maskLayer;
        UIBezierPath *maskPaths = [UIBezierPath bezierPathWithRoundedRect:markerWindow.bgImg.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        CAShapeLayer *maskLayers = [[CAShapeLayer alloc] init];
        maskLayers.frame = self.view.bounds;
        maskLayers.path = maskPaths.CGPath;
        markerWindow.bgImg.layer.mask = maskLayers;
//        UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:markerWindow.img1.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
//        CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
//        maskLayer1.frame = self.view.bounds;
//        maskLayer1.path = maskPath1.CGPath;
//        markerWindow.img1.layer.mask = maskLayer1;
        
//        UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:markerWindow.img3.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
//        CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
//        maskLayer1.frame = self.view.bounds;
//        maskLayer1.path = maskPath1.CGPath;
//        markerWindow.img1.layer.mask = maskLayer1;
//
//        UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:markerWindow.img3.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
//
//        CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
//        maskLayer2.frame = self.view.bounds;
//        maskLayer2.path = maskPath2.CGPath;
//        markerWindow.img3.layer.mask = maskLayer2;
    }
    markerWindow.lblMemberCnt.text = @"";
    markerWindow.lblSpeed.text = @"Stationary";
    return markerWindow;
    
}

- (void)changeColorIfUserOffline:(NSDictionary*)userInfo onView:(UIView*)view {
    NSString *lastUpdateTime = [userInfo[@"location"] valueForKey:@"location_captured_time"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSDate *dateNow = [NSDate date];
    NSDate *updateTime = [dateFormat dateFromString:lastUpdateTime];//[NSDate dateWithTimeIntervalSinceNow: longLongValue]];
    NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:updateTime];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    if (hoursBetweenDates > 24) {
        [view.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [view setBackgroundColor:[UIColor grayColor]];
    }
}

- (NSString *)getCurrentUnit {
    if ([distanceUnit isEqualToString:kkeyUnitKilometers])
        return @"kmph";
    else
        return @"mph";
}

- (float)modifySpeedKMML:(float)inSpeed {
    if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
        return inSpeed * 3.6;
    }
    return inSpeed * 2.23694;
}

- (NSDate *)getCurrentTime:(NSDate *)dt {
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate:dt];
    return [NSDate dateWithTimeInterval:seconds sinceDate:dt];
}

- (int)getTimeDifference:(NSString *)locationUpdateDate {
    NSDate *startDate = [SRModalClass returnFullDate:locationUpdateDate];
    startDate = [self getCurrentTime:startDate];
    NSDate *enddate = [NSDate date];
    enddate = [self getCurrentTime:enddate];
    NSTimeInterval sec = [enddate timeIntervalSinceDate:startDate];
    int hours = (int) sec / 3600;
    return (sec - (hours * 3600)) / 60;
}

- (float)getRadians:(int)degree {
    if (degree >= 338 && degree <= 359) {
        degree = degree - 359;
    }
    if ((degree >= 22 && degree <= 66)) {
        degree = degree - 45;
    }
    if ((degree >= 67 && degree <= 111)) {
        degree = degree - 90;
    }
    if ((degree >= 112 && degree <= 156)) {
        degree = degree - 135;
    }
    if ((degree >= 157 && degree <= 202)) {
        degree = degree - 180;
    }
    if ((degree >= 203 && degree <= 247)) {
        degree = degree - 225;
    }
    if ((degree >= 248 && degree <= 292)) {
        degree = degree - 270;
    }
    if ((degree >= 293 && degree <= 337)) {
        degree = degree - 315;
    }
    return degreesToRadians(degree);
}

//-----------------------------------------------------------------------------------------------------------------------
// viewForAnnotation:
- (NSString *)getDirectionString:(float)degree {
    NSArray *arrStr = @[@"N", @"NE", @"E", @"SE", @"S", @"SW", @"W", @"NW", @"N"];
    if (degree == -1){
        return [arrStr objectAtIndex:0];
    }
    return [arrStr objectAtIndex:degree];
}

//clusters
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *AnnotationIdentifier = @"Annotation";
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    MKAnnotationView *customPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
    if (!pinView) {
        if (annotation == mapView.userLocation) {
            if (!isGroupTrackUsers) {
                SRMapProfileView *myProfileView = [[SRMapProfileView alloc] init];
                UIImage *img = [SRModalClass imageFromUIView:myProfileView];
                customPinView.image = img;
                customPinView.userInteractionEnabled = FALSE;
                customPinView.layer.zPosition = 1;
                return customPinView;
            }
        }
    }
    MKAnnotationView *annView;
    if ([annotation isKindOfClass:[REVClusterPin class]]) {
        pin = (REVClusterPin *) annotation;
        if ([pin nodeCount] == 0) {
            if (annotation.coordinate.latitude == server.myLocation.coordinate.latitude && annotation.coordinate.longitude == server.myLocation.coordinate.longitude) {
                // Set profile pic
                self.imgUserProfile.layer.masksToBounds = YES;
                SRMapProfileView *myProfileView = [[SRMapProfileView alloc] init];
                UIImage *img = [SRModalClass imageFromUIView:myProfileView];
                annView.image = img;
                annView.userInteractionEnabled = FALSE;
                return annView;
            }
        }
        if (filterDictionary[kKeyGroupTag]) {
            if (isGroupTrackUsers) {
                SRMapProfileView *markerWindow;
                //Call Method
                for (NSDictionary *DictTemp in sourceArr) {
                    for (id key in DictTemp) {
                        if ([pin.title isEqualToString:[NSString stringWithFormat:@"%@", key]]) {
                            NSArray *arrPinNodes = DictTemp[key];  ///error point
                            if ([arrPinNodes count] == 1) {
                                markerWindow.lblMemberCnt.hidden = YES;
                                markerWindow.collectionView.hidden = YES;
                                NSDictionary *profileDict = [[NSDictionary alloc] init];
                                profileDict = arrPinNodes[0];
                                NSInteger directionValue = [self getintDirection:[profileDict[kKeyLocation] objectForKey:kKeyDirection]];
                                int directionval = 0;
                                
                                if ([NSNull null] != [profileDict[kKeyLocation] objectForKey:kKeyDirection]) {
                                    directionval = [[profileDict[kKeyLocation] objectForKey:kKeyDirection] intValue];
                                }
                                float direction = [self getRadians:directionval];
                                markerWindow = [self getProfileView:directionValue profileInfo:profileDict];
                                
                                if ([profileDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                    if ([profileDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                        NSString *imageName = [profileDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                        if ([imageName length] > 0) {
                                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                            
                                            [markerWindow.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                            
                                        } else
                                            markerWindow.userProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                    } else
                                        markerWindow.userProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                } else
                                    markerWindow.userProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                markerWindow.lblSpeed.text = @"Stationary";
                                if (profileDict[kKeyLocation]) {
                                    float speed = [self modifySpeedKMML:[[profileDict[kKeyLocation] objectForKey:kKeySpeed] floatValue]];
                                    int speedInt = roundf(speed);
                                    //&& [self getTimeDifference:[[profileDict objectForKey:kKeyLocation] objectForKey:kKeyLastSeenDate]] < 5
                                    if (speedInt > 0 && [self getTimeDifference:[profileDict[kKeyLocation] objectForKey:kKeyLastSeenDate]] < 10) {
                                        markerWindow.lblSpeed.text = [NSString stringWithFormat:@"%d %@", speedInt, [self getCurrentUnit]];
                                        if ([[self getDirectionString:directionValue] isEqualToString:@"N"]) {
                                            markerWindow.imgNorth.hidden = NO;
                                            markerWindow.imgNorth.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"S"]) {
                                            markerWindow.imgSouth.hidden = NO;
                                            markerWindow.imgSouth.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"E"]) {
                                            markerWindow.imgEast.hidden = NO;
                                            markerWindow.imgEast.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"NE"]) {
                                            markerWindow.imgNE.hidden = NO;
                                            markerWindow.imgNE.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"SE"]) {
                                            markerWindow.imgSE.hidden = NO;
                                            markerWindow.imgSE.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"SW"]) {
                                            markerWindow.imgSW.hidden = NO;
                                            markerWindow.imgSW.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"NW"]) {
                                            markerWindow.imgNW.hidden = NO;
                                            markerWindow.imgNW.transform = CGAffineTransformMakeRotation(direction);
                                        } else {
                                            markerWindow.imgWest.hidden = NO;
                                            markerWindow.imgWest.transform = CGAffineTransformMakeRotation(direction);
                                        }
                                    }
                                }
                            } else if ([arrPinNodes count] > 1 && [arrPinNodes count] <= 4) {
                                markerWindow.userProfileImg.hidden = YES;
                                markerWindow.lblMemberCnt.hidden = YES;
                                markerWindow.lblMemberCnt.text = [NSString stringWithFormat:@"%lu", (unsigned long) [pin nodeCount]];
                                markerWindow.lblSpeed.text = @"Stationary";
                                NSDictionary *profileDictTemp = [[NSDictionary alloc] init];
                                profileDictTemp = arrPinNodes[0];
                                NSInteger directionValue = [self getintDirection:[profileDictTemp[kKeyLocation] objectForKey:kKeyDirection]];
                                int directionval = 0;
                                if ([NSNull null] != [profileDictTemp[kKeyLocation] objectForKey:kKeyDirection]) {
                                    directionval = [[profileDictTemp[kKeyLocation] objectForKey:kKeyDirection] intValue];
                                }
                                float direction = [self getRadians:directionval];
                                markerWindow = [self getProfileView:directionValue profileInfo:profileDictTemp];
                                markerWindow.collectionView.hidden = NO;
                                if (profileDictTemp[kKeyLocation]) {
                                    float speed = [self modifySpeedKMML:[[profileDictTemp[kKeyLocation] objectForKey:kKeySpeed] floatValue]];
                                    int speedInt = roundf(speed);
                                    if (speedInt > 0 && [self getTimeDifference:[profileDictTemp[kKeyLocation] objectForKey:kKeyLastSeenDate]] < 5) {
                                        markerWindow.lblSpeed.text = [NSString stringWithFormat:@"%d %@", speedInt, [self getCurrentUnit]];
                                        if ([[self getDirectionString:directionValue] isEqualToString:@"N"]) {
                                            markerWindow.imgNorth.hidden = NO;
                                            markerWindow.imgNorth.transform = CGAffineTransformMakeRotation(direction);
                                            
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"S"]) {
                                            markerWindow.imgSouth.hidden = NO;
                                            markerWindow.imgSouth.transform = CGAffineTransformMakeRotation(direction);
                                            
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"E"]) {
                                            markerWindow.imgEast.hidden = NO;
                                            markerWindow.imgEast.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"NE"]) {
                                            markerWindow.imgNE.hidden = NO;
                                            markerWindow.imgNE.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"SE"]) {
                                            markerWindow.imgSE.hidden = NO;
                                            markerWindow.imgSE.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"SW"]) {
                                            markerWindow.imgSW.hidden = NO;
                                            markerWindow.imgSW.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"NW"]) {
                                            markerWindow.imgNW.hidden = NO;
                                            markerWindow.imgNW.transform = CGAffineTransformMakeRotation(direction);
                                        } else {
                                            markerWindow.imgWest.hidden = NO;
                                            markerWindow.imgWest.transform = CGAffineTransformMakeRotation(direction);
                                        }
                                    } else {
                                        for (int i = 1; i < [arrPinNodes count]; i++) {
                                            NSDictionary *dictSpeed = arrPinNodes[i];
                                            float speed = [self modifySpeedKMML:[[dictSpeed[kKeyLocation] objectForKey:kKeySpeed] floatValue]];
                                            int speedInt = roundf(speed);
                                            NSInteger directionValue = [self getintDirection:[dictSpeed[kKeyLocation] objectForKey:kKeyDirection]];
                                            float direction = [[dictSpeed[kKeyLocation] objectForKey:kKeyDirection] floatValue];
                                            if (speedInt > 0 && [self getTimeDifference:[dictSpeed[kKeyLocation] objectForKey:kKeyLastSeenDate]] < 5) {
                                                markerWindow.lblSpeed.text = [NSString stringWithFormat:@"%d %@", speedInt, [self getCurrentUnit]];
                                                if ([[self getDirectionString:directionValue] isEqualToString:@"N"]) {
                                                    markerWindow.imgNorth.hidden = NO;
                                                    markerWindow.imgNorth.transform = CGAffineTransformMakeRotation(direction);
                                                    
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"S"]) {
                                                    markerWindow.imgSouth.hidden = NO;
                                                    markerWindow.imgSouth.transform = CGAffineTransformMakeRotation(direction);
                                                    
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"E"]) {
                                                    markerWindow.imgEast.hidden = NO;
                                                    markerWindow.imgEast.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"NE"]) {
                                                    markerWindow.imgNE.hidden = NO;
                                                    markerWindow.imgNE.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"SE"]) {
                                                    markerWindow.imgSE.hidden = NO;
                                                    markerWindow.imgSE.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"SW"]) {
                                                    markerWindow.imgSW.hidden = NO;
                                                    markerWindow.imgSW.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"NW"]) {
                                                    markerWindow.imgNW.hidden = NO;
                                                    markerWindow.imgNW.transform = CGAffineTransformMakeRotation(direction);
                                                } else {
                                                    markerWindow.imgWest.hidden = NO;
                                                    markerWindow.imgWest.transform = CGAffineTransformMakeRotation(direction);
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                NSDictionary *combDict;
                                NSMutableArray *arrColors = [NSMutableArray.alloc init];
                                for (int i = 0; i < arrPinNodes.count; i++) {
                                    markerWindow.img3.hidden = YES;
                                    markerWindow.img4.hidden = YES;
                                    combDict = [[NSDictionary alloc] initWithDictionary:arrPinNodes[i]];
                                    if (i == 0) {
                                        if ([combDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                            
                                            if ([combDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                                NSString *imageName = [combDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                                if ([imageName length] > 0) {
                                                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                                    
                                                    [markerWindow.img1 sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                                } else
                                                    markerWindow.img1.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                            } else
                                                markerWindow.img1.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        } else
                                            markerWindow.img1.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        
//                                        NSString* batteryUsage = [self getBatterySettings:combDict];
//                                        CGColorRef cgColor = [[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage] CGColor];
//                                        markerWindow.img1.layer.borderColor = cgColor;
//                                        markerWindow.img1.layer.borderWidth = 3.0;
                                    }
                                    if (i == 1) {
                                        if ([combDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                            
                                            if ([combDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                                NSString *imageName = [combDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                                if ([imageName length] > 0) {
                                                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                                    
                                                    [markerWindow.img2 sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                                } else
                                                    markerWindow.img2.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                            } else
                                                markerWindow.img2.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        } else
                                            markerWindow.img2.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        
//                                        NSString* batteryUsage = [self getBatterySettings:combDict];
//                                        CGColorRef cgColor = [[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage] CGColor];
//                                        markerWindow.img2.layer.borderColor = cgColor;
//                                        markerWindow.img2.layer.borderWidth = 3.0;
                                    }
                                    if (i == 2) {
                                        markerWindow.img3.hidden = NO;
                                        if ([combDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                            
                                            if ([combDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                                NSString *imageName = [combDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                                if ([imageName length] > 0) {
                                                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                                    
                                                    [markerWindow.img3 sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                                } else
                                                    markerWindow.img3.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                            } else
                                                markerWindow.img3.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        } else
                                            markerWindow.img3.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        
//                                        NSString* batteryUsage = [self getBatterySettings:combDict];
//                                        CGColorRef cgColor = [[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage] CGColor];
//                                        markerWindow.img3.layer.borderColor = cgColor;
//                                        markerWindow.img3.layer.borderWidth = 3.0;
                                    }
                                    if (i == 3) {
                                        markerWindow.img4.hidden = NO;
                                        if ([combDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                            if ([combDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                                NSString *imageName = [combDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                                if ([imageName length] > 0) {
                                                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                                    
                                                    [markerWindow.img4 sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                                } else {
                                                    markerWindow.img4.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                                }
                                            } else {
                                                markerWindow.img4.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                            }
                                        } else {
                                            markerWindow.img4.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                        }
//                                        NSString* batteryUsage = [self getBatterySettings:combDict];
//                                        CGColorRef cgColor = [[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage] CGColor];
//                                        markerWindow.img4.layer.borderColor = cgColor;
//                                        markerWindow.img4.layer.borderWidth = 3.0;
                                        [markerWindow.collectionView.subviews makeObjectsPerformSelector:@selector(setHidden:) withObject:false];
                                    }
                                    NSString* batteryUsage = [self getBatterySettings:combDict];
                                    [arrColors addObject:[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage]];
                                }
                                if ([arrPinNodes count] < 3) {
                                    markerWindow.img3.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                    markerWindow.img4.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                } else if ([arrPinNodes count] < 4) {
                                    markerWindow.img4.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                }
                                
                                //Apply Multi color border here
                                UIView *borderView = [SRModalClass layerWithMultiColorBorder:arrColors forView:[markerWindow subviews][0]];
                                [markerWindow insertSubview:borderView aboveSubview:markerWindow.backImg];
                            } else if ([arrPinNodes count] > 4) {
                                markerWindow.collectionView.hidden = YES;
                                markerWindow.lblSpeed.text = @"Stationary";
                                NSDictionary *profileDictTemp = [[NSDictionary alloc] init];
                                profileDictTemp = arrPinNodes[0];
                                NSInteger directionValue = [self getintDirection:[profileDictTemp[kKeyLocation] objectForKey:kKeyDirection]];
                                int directionval = 0;
                                if ([NSNull null] != [profileDictTemp[kKeyLocation] objectForKey:kKeyDirection]) {
                                    directionval = [[profileDictTemp[kKeyLocation] objectForKey:kKeyDirection] intValue];
                                }
                                float direction = [self getRadians:directionval];
                                markerWindow = [self getProfileView:directionValue profileInfo:profileDictTemp];
                                if (profileDictTemp[kKeyLocation]) {
                                    float speed = [self modifySpeedKMML:[[profileDictTemp[kKeyLocation] objectForKey:kKeySpeed] floatValue]];
                                    int speedInt = roundf(speed);
                                    if (speedInt > 0 && [self getTimeDifference:[profileDictTemp[kKeyLocation] objectForKey:kKeyLastSeenDate]] < 5) {
                                        markerWindow.lblSpeed.text = [NSString stringWithFormat:@"%d %@", speedInt, [self getCurrentUnit]];
                                        if ([[self getDirectionString:directionValue] isEqualToString:@"N"]) {
                                            markerWindow.imgNorth.hidden = NO;
                                            markerWindow.imgNorth.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"S"]) {
                                            markerWindow.imgSouth.hidden = NO;
                                            markerWindow.imgSouth.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"E"]) {
                                            markerWindow.imgEast.hidden = NO;
                                            markerWindow.imgEast.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"NE"]) {
                                            markerWindow.imgNE.hidden = NO;
                                            markerWindow.imgNE.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"SE"]) {
                                            markerWindow.imgSE.hidden = NO;
                                            markerWindow.imgSE.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"SW"]) {
                                            markerWindow.imgSW.hidden = NO;
                                            markerWindow.imgSW.transform = CGAffineTransformMakeRotation(direction);
                                        } else if ([[self getDirectionString:directionValue] isEqualToString:@"NW"]) {
                                            markerWindow.imgNW.hidden = NO;
                                            markerWindow.imgNW.transform = CGAffineTransformMakeRotation(direction);
                                        } else {
                                            markerWindow.imgWest.hidden = NO;
                                            markerWindow.imgWest.transform = CGAffineTransformMakeRotation(direction);
                                        }
                                    } else {
                                        for (int i = 1; i < [arrPinNodes count]; i++) {
                                            NSDictionary *dictSpeed = arrPinNodes[i];
                                            float speed = [self modifySpeedKMML:[[dictSpeed[kKeyLocation] objectForKey:kKeySpeed] floatValue]];
                                            int speedInt = roundf(speed);
                                            NSInteger directionValue = [self getintDirection:[dictSpeed[kKeyLocation] objectForKey:kKeyDirection]];
                                            float direction = [[dictSpeed[kKeyLocation] objectForKey:kKeyDirection] floatValue];
                                            if (speedInt > 0 && [self getTimeDifference:[dictSpeed[kKeyLocation] objectForKey:kKeyLastSeenDate]] < 5) {
                                                markerWindow.lblSpeed.text = [NSString stringWithFormat:@"%d %@", speedInt, [self getCurrentUnit]];
                                                if ([[self getDirectionString:directionValue] isEqualToString:@"N"]) {
                                                    markerWindow.imgNorth.hidden = NO;
                                                    markerWindow.imgNorth.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"S"]) {
                                                    markerWindow.imgSouth.hidden = NO;
                                                    markerWindow.imgSouth.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"E"]) {
                                                    markerWindow.imgEast.hidden = NO;
                                                    markerWindow.imgEast.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"NE"]) {
                                                    markerWindow.imgNE.hidden = NO;
                                                    markerWindow.imgNE.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"SE"]) {
                                                    markerWindow.imgSE.hidden = NO;
                                                    markerWindow.imgSE.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"SW"]) {
                                                    markerWindow.imgSW.hidden = NO;
                                                    markerWindow.imgSW.transform = CGAffineTransformMakeRotation(direction);
                                                } else if ([[self getDirectionString:directionValue] isEqualToString:@"NW"]) {
                                                    markerWindow.imgNW.hidden = NO;
                                                    markerWindow.imgNW.transform = CGAffineTransformMakeRotation(direction);
                                                } else {
                                                    markerWindow.imgWest.hidden = NO;
                                                    markerWindow.imgWest.transform = CGAffineTransformMakeRotation(direction);
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                                NSDictionary *combDict;
                                if ([arrPinNodes count] > 0) {
                                    combDict = arrPinNodes[0];
                                }
                                if ([combDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                    
                                    if ([combDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                                        NSString *imageName = [combDict[kKeyProfileImage] objectForKey:kKeyImageName];
                                        if ([imageName length] > 0) {
                                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                            
                                            [markerWindow.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                        } else
                                            markerWindow.userProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                    } else
                                        markerWindow.userProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                } else
                                    markerWindow.userProfileImg.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                                markerWindow.lblMemberCnt.text = @"4+";
                                markerWindow.lblMemberCnt.hidden = NO;
                                
                                NSMutableArray *arrColors = [NSMutableArray.alloc init];
                                
                                for (int i = 0; i < arrPinNodes.count; i++) {
                                    combDict = arrPinNodes[i];
                                    NSString* batteryUsage = [self getBatterySettings:combDict];
                                    [arrColors addObject:[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage]];
                                }
                                
                                UIView *borderView = [SRModalClass layerWithMultiColorBorder:arrColors forView:[markerWindow subviews][0]];
                                [markerWindow insertSubview:borderView aboveSubview:markerWindow.backImg];
//                                [markerWindow.backImg.layer setBorderColor:[UIColor colorWithRed:222/255.0 green:184/255.0 blue:135/255.0 alpha:1.0].CGColor];
//                                [markerWindow.backImg setBackgroundColor:[UIColor colorWithRed:222/255.0 green:184/255.0 blue:135/255.0 alpha:1.0]];
                            }
                        }
                        break;
                    }
                }
                markerWindow.lblMemberCnt.font = [UIFont boldSystemFontOfSize:14];
                UIImage *img = [SRModalClass imageFromUIView:markerWindow];
                customPinView.image = img;
                customPinView.userInteractionEnabled = NO;
                return customPinView;
            }
        } else {
            if ([pin nodeCount] > 0) {
                annView = (REVClusterAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
                if (!annView)
                    annView = (REVClusterAnnotationView *) [[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
                annView.image = [UIImage imageNamed:@"radar-pin-map-orange-60px.png"];
                [(REVClusterAnnotationView *) annView setClusterText:
                 [NSString stringWithFormat:@"%lu", (unsigned long) [pin nodeCount]]];
                annView.canShowCallout = NO;
            } else {
                annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
                
                if (!annView)
                    annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
                
                annView.image = [UIImage imageNamed:@"radar-pin-orange-15px.png"];
                
                NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
                SRMapProfileView *userProfileView = nibArray[1];
                
                //                    // Provide round rect
                //                    userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y - 2, 37, 37);
                userProfileView.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2.0;
                userProfileView.userProfileImg.clipsToBounds = YES;
                // userProfileView.userProfileImg.layer.masksToBounds = YES;
                
                for (NSDictionary *user in nearByUsersList) {
                    if (user[kKeyLocation] != nil && ![user[kKeyLocation] isEqual:[NSNull null]]) {
                        if (annotation.coordinate.latitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyLattitude] floatValue] && annotation.coordinate.longitude == [[[user valueForKey:kKeyLocation] valueForKey:kKeyRadarLong] floatValue]) {
                            if ([user[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                                
                                if (![[user[kKeyProfileImage] objectForKey:kKeyImageName] isKindOfClass:[NSNull class]]) {
                                    NSString *imageName = [user[kKeyProfileImage] objectForKey:kKeyImageName];
                                    if (![imageName isKindOfClass:[NSNull class]]) {
                                        if ([imageName length] > 0) {
                                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                            [userProfileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                        } else
                                            userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                                    }
                                } else
                                    userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                            } else {
                                userProfileView.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
                            }
                            userProfileView.userInfo = user;
                        }
                    }
                }
                
                // Show status for user
                [userProfileView updateStatus];
                
                // Add to view
                [userProfileViewArr addObject:userProfileView];
                UIImage *img = [SRModalClass imageFromUIView:userProfileView];
                annView.image = img;
            }
            // No callout
            [annView setEnabled:YES];
            return annView;
        }
    }
    return annView;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    //    if ((APP_DELEGATE).isTrackUserSelected)
    //    {
    for (MKAnnotationView *annView in views) {
        [[annView superview] bringSubviewToFront:annView];
    }
    //    }
}

//-----------------------------------------------------------------------------------------------------------------
// didSelectAnnotationView:

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    if (!isGroupTrackUsers) {
        if ([view.annotation isKindOfClass:[REVClusterPin class]]) {
            pin = (REVClusterPin *) view.annotation;
            MKZoomScale currentZoomScale = mapView.bounds.size.width / mapView.visibleMapRect.size.width;
            isSelectAnnotation = YES;
            
            if ([view.reuseIdentifier isEqualToString:@"pin"] || [(APP_DELEGATE).topBarView.btnConnection isSelected] || currentZoomScale >= zoomLevel) {
                isSlide = NO;
                NSMutableArray *combinationArr = [NSMutableArray array];
                if ([pin.nodes count] > 0) {
                    for (int i = 0; i < [pin.nodes count]; i++) {
                        REVClusterPin *objPin = pin.nodes[i];
                        for (NSDictionary *user in nearByUsersList) {
                            if ([objPin.title isEqualToString:user[kKeyId]]) {
                                [combinationArr addObject:user];
                                break;
                            }
                        }
                    }
                } else {
                    for (NSDictionary *user in nearByUsersList) {
                        if ([pin.title isEqualToString:user[kKeyId]]) {
                            [combinationArr addObject:user];
                            break;
                        }
                    }
                }
                [self addViewsOnScrollView:combinationArr];
            }
        }
    } else {
        [markerTapWindow removeFromSuperview];
        for (id subView in markerTapWindow.subviews) {
            [subView removeFromSuperview];
        }
        
        // Check for marker in the list if same marker got tapped then remove the select object
        selectedMarker = view;
        
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRMapWindowView" owner:self options:nil];
        if ([nibObjects count] > 0) {
            markerTapWindow = (SRMapWindowView *) nibObjects[0];
        }
        
        static NSString *AnnotationIdentifier = @"Annotation";
        MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
        
        if (!pinView) {
            
            if (view.annotation == mapView.userLocation) {
            }
        }
        
        if ([view.annotation isKindOfClass:[REVClusterPin class]]) {
            NSMutableArray *combinationArr = [NSMutableArray array];
            REVClusterPin *selectedPin = (REVClusterPin *) view.annotation;
            for (NSDictionary *dataDict in nearByUsersList) {
                for (id key in dataDict) {
                    if ([selectedPin.title isEqualToString:[NSString stringWithFormat:@"%@", key]]) {
                        NSArray *arrPinNodes = dataDict[key];
                        if ([arrPinNodes count] > 0) {
                            for (NSDictionary *userData in arrPinNodes) {
                                [combinationArr addObject:userData];
                            }
                        }
                    }
                }
            }
            groupUsers = combinationArr;
            CGPoint p = [self.mapView convertCoordinate:view.annotation.coordinate toPointToView:view];
            CGRect frame;
            int userItemHintHeight = 100;
            int userItemHintWidth = 280;
            //            if (groupUsers.count >= 4)
            //            {
            //                frame= CGRectMake(p.x - 78, p.y - 240, 150, 210 );
            // frame= CGRectMake(p.x - 78, p.y - 50 * groupUsers.count-40, 150, 50 * groupUsers.count);
            
            //            }
            //            else
            frame = CGRectMake(p.x - userItemHintWidth/2, p.y - userItemHintHeight * groupUsers.count - 40, userItemHintWidth, userItemHintHeight * groupUsers.count);
            
            markerTapWindow.hidden = NO;
            markerTapWindow.userProfileImage.hidden = YES;
            markerTapWindow.lblDegree.hidden = YES;
            markerTapWindow.lblDistance.hidden = YES;
            markerTapWindow.lblOccupation.hidden = YES;
            markerTapWindow.lblProfileName.hidden = YES;
            markerTapWindow.datingImgView.hidden = YES;
            markerTapWindow.pingButton.hidden = YES;
            markerTapWindow.bgImage.hidden = YES;
            markerTapWindow.backgroundColor = [UIColor clearColor];
            
            self.markerBgImage.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
            self.markerBgImage.image = [UIImage imageNamed:@"white-tooltip.png"];
            self.markerBgImage.contentMode = UIViewContentModeScaleToFill;
            self.markerBgImage.clipsToBounds = YES;
            self.markerBgImage.layer.cornerRadius = 5.0;
            self.markerBgImage.layer.masksToBounds = YES;
            self.markerBgImage.userInteractionEnabled = YES;
            
            self.tableView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
            self.tableView.backgroundColor = [UIColor whiteColor];
            self.tableView.contentMode = UIViewContentModeScaleToFill;
            self.tableView.layer.cornerRadius = 5.0;
            self.tableView.layer.masksToBounds = YES;
            self.tableView.bounces = YES;
            [self.markerBgImage removeFromSuperview];
            [self.tableView removeFromSuperview];
            
            //            [markerTapWindow addSubview:self.markerBgImage];
            self.tableView.backgroundColor = [UIColor clearColor];
            [markerTapWindow addSubview:self.tableView];
            
            if (markerTapWindow == nil) {
                markerTapWindow = [[SRMapWindowView alloc] initWithFrame:frame];
            } else {
                markerTapWindow.frame = frame;
            }
            
            [markerTapWindow removeFromSuperview];
            markerTapWindow.layer.cornerRadius = 5;
            markerTapWindow.layer.masksToBounds = YES;
            //markerTapWindow.userInteractionEnabled=YES;
            if (groupUsers.count > 0) {
                self.tableView.delegate = self;
                self.tableView.dataSource = self;
                [self.tableView reloadData];
                [view addSubview:markerTapWindow];
                [view bringSubviewToFront:markerTapWindow];
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------
// didDeselectAnnotationView:

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    UIView *view = gestureRecognizer.view;
    if (![view isKindOfClass:[MKAnnotationView class]]) {
        
        [markerTapWindow removeFromSuperview];
        
        for (id subView in markerTapWindow.subviews) {
            [subView removeFromSuperview];
        }
        
        //Hide ProfilesScrollview if unhide
        isSelectAnnotation = NO;
        
        self.scrollView.hidden = YES;
        self.toolTipScrollViewImg.hidden = YES;
        selectedCombinatonArr = nil;
        
        /*  if ([view.annotation isKindOfClass:[REVClusterPin class]]) {
         // Deselected the pin annotation.
         REVClusterPin *pinAnnotation = (((REVClusterPin *)view).annotation);
         [mapView removeAnnotation:pinAnnotation.calloutAnnotation];
         pinAnnotation.calloutAnnotation = nil;
         }*/
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    
    /*  [markerTapWindow removeFromSuperview];
     for (id subView in markerTapWindow.subviews)
     {
     [subView removeFromSuperview];
     }
     
     //Hide ProfilesScrollview if unhide
     isSelectAnnotation = NO;
     
     self.scrollView.hidden = YES;
     self.toolTipScrollViewImg.hidden = YES;
     selectedCombinatonArr = nil;*/
    
    if ([view.annotation isKindOfClass:[REVClusterPin class]]) {
        // Deselected the pin annotation.
        REVClusterPin *pinAnnotation = ((REVClusterPin *) view.annotation);
        [mapView removeAnnotation:pinAnnotation.calloutAnnotation];
        pinAnnotation.calloutAnnotation = nil;
    }
}

//-----------------------------------------------------------------------------------------------------------------
// didUpdateUserLocation:

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    //        Zoom to region containing the user location
    //        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
    //        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

#pragma mark UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return groupUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier1 = @"SRDiscoveryMapUserCell";
    SRDiscoveryMapUserCell *customCell = (SRDiscoveryMapUserCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRDiscoveryMapUserCell" owner:self options:nil];
    if ([nibObjects count] > 0) {
        customCell = (SRDiscoveryMapUserCell *) nibObjects[0];
    }
    NSDictionary *cellDict = groupUsers[indexPath.row];
    [customCell updateWithUserInfo:cellDict];
    [customCell setBackgroundColor:[UIColor clearColor]];
    return customCell;
}

// ----------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}


#pragma mark
#pragma mark Delegate Callout Method
#pragma mark

// --------------------------------------------------------------------------------
// calloutButtonClicked:

- (void)calloutButtonClicked:(NSString *)title {
}

- (NSMutableArray *)groupUsers:(NSMutableArray *)trackUsersList {
    NSMutableArray *finalgroupUser = [[NSMutableArray alloc] init];
    int groupIndex = 0;
    filteredArr = [[NSMutableArray alloc] init];
    [filteredArr addObjectsFromArray:trackUsersList];
    while ([trackUsersList count] > 0) {
        NSMutableDictionary *dictgroup = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *dict = trackUsersList[0];
        NSMutableIndexSet *discardedItems = [NSMutableIndexSet indexSet];
        NSMutableArray *matchedChunk = [[NSMutableArray alloc] init];
        
        if ([[dict valueForKey:kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *locDict = [dict valueForKey:kKeyLocation];
            [matchedChunk addObject:dict];
            
            for (int i = 1; i < [trackUsersList count]; i++) {
                NSDictionary *dictUserDetail = trackUsersList[i];
                NSDictionary *dictOtherUSerLoc = [dictUserDetail valueForKey:kKeyLocation];
                int speed = [[locDict valueForKey:@"speed"] intValue];
                int otherSpeed = [[dictOtherUSerLoc valueForKey:@"speed"] intValue];
                int abs_diff = MAX(speed, otherSpeed) - MIN(speed, otherSpeed);
                NSString *locgeohash = [[locDict valueForKey:@"geohash"] substringToIndex:[[locDict valueForKey:@"geohash"] length] - 2];
                NSString *Othergeohash = [[dictOtherUSerLoc valueForKey:@"geohash"] substringToIndex:[[dictOtherUSerLoc valueForKey:@"geohash"] length] - 2];
                if (![Othergeohash isKindOfClass:[NSNull class]] && ![locgeohash isKindOfClass:[NSNull class]] && ![[locDict valueForKey:@"direction"] isKindOfClass:[NSNull class]] && ![[locDict valueForKey:@"speed"] isKindOfClass:[NSNull class]]) {
                    if ([Othergeohash isEqualToString:locgeohash] && ([[locDict valueForKey:@"speed"] isEqualToString:[dictOtherUSerLoc valueForKey:@"speed"]] || abs_diff <= 20) && [[locDict valueForKey:@"direction"] isEqualToString:[dictOtherUSerLoc valueForKey:@"direction"]]) {
                        [matchedChunk addObject:dictUserDetail];
                        [discardedItems addIndex:i];
                    }
                }
            }
            dictgroup[@(groupIndex)] = matchedChunk;
            [finalgroupUser addObject:dictgroup];
            [trackUsersList removeObjectsAtIndexes:discardedItems];
            [trackUsersList removeObjectAtIndex:0];
        }
        groupIndex++;
    }
    return finalgroupUser;
}

#pragma mark Notification Method


//Called when radat API succeded.
- (void)getRadarUsersList:(NSNotification *)inNotify {
    //self.toolTipScrollViewImg.hidden = NO;
    //self.scrollView.hidden=NO;
    
    BOOL isInMainThread = (sourceArr.count == 0);
    
   // NSlog(@"🗺️ getRadarUsersList: 🗺️");
    
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
    sourceArr = [inNotify object];
    oldSourceArr = [inNotify object];
    [filteredArr removeAllObjects];
    
    // For Selected Group Notification and respective  Radar Change
    grpNotifiArr = [filterDictionary valueForKey:@"group_notification"];
    
    NSMutableArray *groupUser = [[NSMutableArray alloc] init];
    NSMutableArray *filteredTrackUsers = [[NSMutableArray alloc] init];
    
    if (filterDictionary[kKeyGroupTag]) {
        if ([filterDictionary[kKeyGroupTag] integerValue] == 2) {
            [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                    if ([[obj valueForKey:@"is_active_connection"] boolValue]) {
                        [filteredTrackUsers addObject:obj];
                    }
                    //                    [filteredTrackUsers addObject:obj];
                }
            }];
        } else if (![filterDictionary[kKeyGroupTag] boolValue]) {
            if ([filterDictionary[kKeyGroupType] isEqualToString:@"All Connections"]) {
                [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                    if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                        [filteredTrackUsers addObject:obj];
                    }
                }];
            }
        } else if ([filterDictionary[kKeyGroupType] isEqualToString:@"Family"]) {
            [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && [[obj objectForKey:kKeyFamily] isKindOfClass:[NSDictionary class]] && [[obj objectForKey:@"is_tracking"] boolValue]) {
                    [filteredTrackUsers addObject:obj];
                }
            }];
        } else if ([filterDictionary[kKeyGroupType] isEqualToString:@"Selected Connections"]) {
            
            NSUInteger userId = [filterDictionary[kKeyGroupTag] integerValue];
            
            [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if (![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue] && ([[obj valueForKey:@"id"] integerValue] == userId)) {
                    [filteredTrackUsers addObject:obj];
                }
            }];
            
        }
        
        if (filteredTrackUsers.count >= 1) {
            isGroupTrackUsers = YES;
            groupUser = [self groupUsers:filteredTrackUsers];
        }
        if (groupUser.count > 0)
            sourceArr = [[NSMutableArray alloc] initWithArray:groupUser];
        else
            sourceArr = [[NSMutableArray alloc] initWithArray:filteredTrackUsers];
        trackUserArr = [[NSMutableArray alloc] initWithArray:filteredTrackUsers];
    } else if (filterDictionary.count > 0) {
        if ([grpNotifiArr count] > 0) {
            
            filteredArr = [grpNotifiArr mutableCopy];
            sourceArr = filteredArr;
        } else {
            for (NSDictionary *dict in sourceArr) {
                NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
                if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && [updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyIsFamilyMember] integerValue] == 1) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && ([[filterDictionary valueForKey:kKeyConnection] integerValue] == 2)) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 3) {
                    //For All
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 4) {
                    //For Active
                    if ([[updatedDict valueForKey:@"is_active_connection"] boolValue]) {
                        [filteredArr addObject:dict];
                    }
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 5) {
                    //For favourite
                    if ([[updatedDict valueForKey:kKeyIsFavourite] boolValue]) {
                        [filteredArr addObject:dict];
                    }
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree < 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {
                    
                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.selectedDegree == -1 && (APP_DELEGATE).topBarView.btnCommunity.selected) {
                    //                    filterDictionary = nil;
                    [filteredArr removeAllObjects];
                    filteredArr = [[NSMutableArray alloc] initWithArray:oldSourceArr];
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && (APP_DELEGATE).topBarView.selectedDegree == 6 && ([updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree || ![dict[kKeyDegree] isKindOfClass:[NSNumber class]])) {
                    [filteredArr addObject:dict];
                }
            }
            sourceArr = filteredArr;
        }
    }
    [self reloadDataInMap:NO];
}

// --------------------------------------------------------------------------------
// updatedLocation:

- (void)updatedLocation:(NSNotification *)inNotify {
    myLocation = server.myLocation;
    
    // Set Pin and set iszoomIn flag for avoid map annotation reset when location update
    if (self.mapView != nil && !isZoomIn) {
        // Add my annotation
        if (pin == nil) {
            pin = [[REVClusterPin alloc] init];
            pin.coordinate = myLocation.coordinate;
        } else
            pin.coordinate = myLocation.coordinate;
        [self.mapView addAnnotation:pin];
    }
}

//This will call when click on menu
- (void)filterTheMapList:(NSNotification *)inNotify {
    isGroupTrackUsers = NO;
    //Remove user card on filter button tap
    filteredArr = [NSMutableArray array];
    filterDictionary = [inNotify object];
    grpNotifiArr = [NSMutableArray array];
    
    [grpNotifiArr removeAllObjects];
    //nearbyUsers = nil;
    [filteredArr removeAllObjects];
    
    // For Selected Group Notification and respective  Radar Change
    grpNotifiArr = [filterDictionary valueForKey:@"group_notification"];
    NSMutableArray *groupUser;
    NSMutableArray *filteredTrackUsers = [[NSMutableArray alloc] init];
    if (filterDictionary[kKeyGroupTag]) {
        if ([filterDictionary[kKeyGroupType] isEqualToString:@"Selected Connections"]) {
            
            NSUInteger userId = [filterDictionary[kKeyGroupTag] integerValue];
            
            [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if (![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue] && ([[obj valueForKey:@"id"] integerValue] == userId)) {
                    [filteredTrackUsers addObject:obj];
                }
            }];
            
        } else if ([filterDictionary[kKeyGroupTag] integerValue] == 2) {
            [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if (![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                    if ([[obj valueForKey:@"is_active_connection"] boolValue]) {
                        [filteredTrackUsers addObject:obj];
                    }
                    //                    [filteredTrackUsers addObject:obj];
                }
            }];
        } else if (![filterDictionary[kKeyGroupTag] boolValue]) {
            [filteredArr removeAllObjects];
            if ([filterDictionary[kKeyGroupType] isEqualToString:@"All Connections"]) {
                [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                    if (![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                        [filteredTrackUsers addObject:obj];
                    }
                }];
            }
        } else if ([filterDictionary[kKeyGroupType] isEqualToString:@"Family"]) {
            [oldSourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj objectForKey:kKeyFamily] isKindOfClass:[NSDictionary class]] && [[obj objectForKey:@"is_tracking"] boolValue]) {
                    [filteredTrackUsers addObject:obj];
                }
            }];
        }
        if (filteredTrackUsers.count >= 1) {
            isGroupTrackUsers = YES;
            groupUser = [self groupUsers:filteredTrackUsers];
        }
        if (groupUser.count > 0) {
            sourceArr = [[NSMutableArray alloc] initWithArray:groupUser];
        } else {
            sourceArr = [[NSMutableArray alloc] initWithArray:filteredTrackUsers];
        }
        trackUserArr = [[NSMutableArray alloc] initWithArray:sourceArr];
    } else if ([grpNotifiArr count] > 0) {
        sourceArr = [[NSMutableArray alloc] initWithArray:grpNotifiArr];
    } else {
        for (NSDictionary *dict in oldSourceArr) {
            NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
            if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && [updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyIsFamilyMember] integerValue] == 1) {
                [filteredArr addObject:dict];
            } else if ([updatedDict[kKeyDegree] integerValue] == 1 && ([[filterDictionary valueForKey:kKeyConnection] integerValue] == 2)) {
                
                [filteredArr addObject:dict];
            } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 3) {
                //For All
                [filteredArr addObject:dict];
            } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 4) {
                //For Active
                if ([[updatedDict valueForKey:@"is_active_connection"] boolValue]) {
                    [filteredArr addObject:dict];
                }
            } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 5) {
                //For favourite
                if ([[updatedDict valueForKey:kKeyIsFavourite] boolValue]) {
                    [filteredArr addObject:dict];
                }
            } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree < 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {
                
                [filteredArr addObject:dict];
            } else if ((APP_DELEGATE).topBarView.selectedDegree == -1 && (APP_DELEGATE).topBarView.btnCommunity.selected) {
                [filteredArr removeAllObjects];
                filteredArr = [[NSMutableArray alloc] initWithArray:oldSourceArr];
            } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && (APP_DELEGATE).topBarView.selectedDegree == 6 && ([updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree || ![dict[kKeyDegree] isKindOfClass:[NSNumber class]])) {
                [filteredArr addObject:dict];
            }
        }
        sourceArr = [[NSArray alloc] initWithArray:filteredArr];
    }
    [self reloadDataInMap:YES];
}

- (NSString *)getBatterySettings :(NSDictionary *)info {
    if (info[@"setting"]) {
        if ([info[@"setting"] valueForKey:kkeyBatteryUsage]) {
            return [info[@"setting"] valueForKey:kkeyBatteryUsage];
        }
    }
    return nil;
}
@end
