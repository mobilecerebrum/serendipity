#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CalloutAnnotationView.h"
#import "PinAnnotation.h"
#import "SRProfileImageView.h"
#import "SRMapWindowView.h"
#import "SRMapProfileView.h"
#import "SRHomeParentViewController.h"
#import "SRChatViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MKMapView+ZoomLevel.h"
#import "REVClusterPin.h"
#import "REVClusterMapView.h"
#import "ASValueTrackingSlider.h"
#import "PinAnnotation.h"

@interface SRDiscoveryMapViewController : SRHomeParentViewController<MKMapViewDelegate,UIGestureRecognizerDelegate,CalloutAnnotationViewDelegate,UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate>
{
    // Instance Variables
    CLPlacemark *placemark;
    NSArray *sourceArr;
    NSArray *oldSourceArr;
    NSDictionary *filterDictionary;
    NSMutableArray *filteredArr;
    NSMutableArray *grpNotifiArr;
    
    NSMutableArray *nearByUsersList;
    NSMutableArray *userProfileViewArr;
    NSArray *selectedCombinatonArr;
    NSArray *dotsUserPicsArr;
    NSMutableArray *trackUserArr;
    CLLocation *myLocation;
    //PinAnnotation *myPinAnnotation;
    REVClusterPin *pin;
    REVClusterPin *lastSelectedPin;
    // Flags
    BOOL isZoomIn,isZoom;
    BOOL isSelectAnnotation;
    BOOL isGestureOnMap;
    
    BOOL isCheckZoomLvl;
    BOOL isCountAdd;
    BOOL isSlide,isPanoramaLoaded,isMapRefreshed;
    // Instance variable
    float latestDistance;
    float settingDistance;
    NSInteger mapZoomLevel;
    NSString *distanceUnit;
    IBOutlet UIButton *btnSatelite;
    IBOutlet UIButton *btnStreet;
    IBOutlet UIButton *zoomInBtn,*zoomOutBtn,*streetViewBtn;
    BOOL isGroupTrackUsers;
    SRMapWindowView *markerTapWindow;
    MKAnnotationView *selectedMarker;
    NSMutableArray *groupUsers;
    PinAnnotation *myPinAnnotation;
    CLLocationCoordinate2D panoramaLastLoc;
    IBOutlet UIView *mapLegendHint;
    IBOutlet UIView *mapLegend; // initial height 100
}
@property (nonatomic, strong) UIView *radarRefreshOverlay;
// Properties
@property (strong, nonatomic) IBOutlet UIToolbar *locationToolBar;
@property (weak, nonatomic) IBOutlet REVClusterMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *layerView;
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (strong, nonatomic) SRProfileImageView *imgUserProfile;

@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider1;
@property (nonatomic, strong) IBOutlet UIView *progressContainerView;
@property (nonatomic, strong) IBOutlet UILabel *lblInPpl;

// Other Properties
@property (weak) id delegate;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImageView *markerBgImage;
@property (nonatomic, strong) IBOutlet UIButton *btnSatelite,*btnStreet;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;


@end
