//
//  CalloutAnnotationView.m
//  CustomCalloutSample
//
//  Created by tochi on 11/05/17.
//  Copyright 2011 aguuu,Inc. All rights reserved.
//

#import "CalloutAnnotationView.h"
#import "CalloutAnnotation.h"

@implementation CalloutAnnotationView
#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CalloutAnnotationView" owner:self options:nil];
        self = nibArray[0];

    }
    [self registerNotification];
    [self rotationCompass:-((APP_DELEGATE).compassHeading * M_PI / 180)];
    // Return
    return self;
}

- (id)initWithAnnotation:(id <MKAnnotation>)annotation
         reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];

    if (self) {
        self.frame = CGRectMake(0.0f, 0.0f, 130, 200);
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
}

// Compass rotation
- (void)registerNotification {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(updateHeadingAngle:)
                          name:kKeyNotificationUpdateHeadingValue
                        object:nil];
}

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    float headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    [self rotationCompass:headingAngle];
}

- (void)rotationCompass:(CGFloat)headingAngle {
    // rotate the compass to heading degree
    self.compassButton.transform = CGAffineTransformMakeRotation(headingAngle);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
