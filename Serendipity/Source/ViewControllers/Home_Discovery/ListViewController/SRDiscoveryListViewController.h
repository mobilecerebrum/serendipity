#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "SRHomeParentViewController.h"

@interface SRDiscoveryListViewController : SRHomeParentViewController<UIGestureRecognizerDelegate>
{
	// Instance variable
    BOOL isPresentListView;
    NSInteger favTag,invisibleTag;
    NSMutableArray *usersListArr;
    NSMutableArray *sourceListArr;
    
    NSArray *dotsUserPicsArr;
    NSString *distanceUnit;
    float headingAngle;
    NSString *updatedLocation;
}

// Properties
@property (strong, nonatomic) IBOutlet UIButton *refreshBtn;
@property (strong, nonatomic) IBOutlet SRProfileImageView *profileImg;
@property (strong, nonatomic) IBOutlet UITableView *objtableView;
@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic , strong) UILabel *lblNoData;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIView *radarRefreshOverlay;
@property(strong, nonatomic) NSTimer *autoTimer;

//Other Properties
@property (weak) id delegate;


#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

@end
