#import "SRModalClass.h"
#import "SwipeableTableViewCell.h"
#import "SRConnectionCellView.h"
#import "SRChatViewController.h"
#import "SRUserTabViewController.h"
#import "SRDiscoveryListViewController.h"
#import "SRMapViewController.h"
#import "UIImage+animatedGIF.h"
#import "SRPageControlViewController.h"
#import "SRDistanceClass.h"
#import "SRProfileTabViewController.h"
#import "SRDiscoveryRadarViewController.h"
#import "SRAPIManager.h"

@interface DistanceClass : NSObject

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *distance;
@property(nonatomic, strong) NSString *dataId;
@property(strong, nonatomic) CLLocation *myLocation;
@end

@implementation DistanceClass

@end

@interface SRDiscoveryListViewController ()

@property(nonatomic, strong) NSOperationQueue *distanceQueue;
@property (assign, nonatomic) BOOL isLoadingRadarUser;


@end

@implementation SRDiscoveryListViewController


#pragma mark - Standard Overrides

- (void)viewDidLoad {
    [super viewDidLoad];
    // Register Notifications
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    
    [defaultCenter addObserver:self
    selector:@selector(deletBlockConnectionSucceed:)
        name:kKeyNotificationGetDeleteBlockConnectionSucceed object:nil];
    [defaultCenter addObserver:self
    selector:@selector(deletBlockConnectionFailed:)
        name:kKeyNotificationGettDeleteBlockConnectionFailed object:nil];
    _distanceQueue = [[NSOperationQueue alloc] init];
    // Table view resizing issue
    self.edgesForExtendedLayout = UIRectEdgeNone;
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [_radarRefreshOverlay setHidden:YES];
    // Add long press guesture to refresh radar centered ProfileImgView
    UILongPressGestureRecognizer *radarLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressRadar:)];
    radarLongPress.minimumPressDuration = 1.0f;
   // [self.objtableView addGestureRecognizer:radarLongPress];
    
    //Setup indicator
    self.indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [self.indicatorView startAnimating];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
       [defaultCenter addObserver:self
                         selector:@selector(addUserInFavouriteSucceed:)
                             name:kKeyNotificationAddFavouritesSucceed
                           object:nil];
       [defaultCenter addObserver:self
                         selector:@selector(addUserInFavouriteFailed:)
                             name:kKeyNotificationAddFavouritesFailed
                           object:nil];
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder cancelGeocode];
    [geoCoder reverseGeocodeLocation:server.myLocation completionHandler:^(NSArray *placemarks, NSError *error) {
       // NSLog(@"Error is %@", error.localizedDescription);
        for (CLPlacemark *placemark in placemarks) {
           // NSLog(@"%@", placemark.ISOcountryCode);
        }
    }];

    [self refreshUsers];

    self.tabBarController.tabBar.hidden = NO;

    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else {
        distanceUnit = kkeyUnitKilometers;
    }

    isPresentListView = TRUE;
    (APP_DELEGATE).isSwipedCell = NO;
    (APP_DELEGATE).swipedCellUserId = nil;
    (APP_DELEGATE).swipedCellIndexPath = nil;
    [self reloadDataInList];

//    (APP_DELEGATE).topBarView.hidden = NO;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;


    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kKeyListToolTipFlag] boolValue] == 0 || (APP_DELEGATE).isSignUpSuccess) {
        (APP_DELEGATE).tooltipListViewFlag = YES;
        [APP_DELEGATE toolTipFlagSaveData];
        // Show tooltip

        NSArray *titleArray = @[NSLocalizedString(@"lbl.list_tooltip1.txt", @""), NSLocalizedString(@"lbl.list_tooltip2.txt", @""), NSLocalizedString(@"", @"")];
        // NSArray *imgArray =[[NSArray alloc]initWithObjects:@"List-edited-optimized_new",@"Swipe-Right-and-Left_new",@"Under-Menu_new", nil];

        NSArray *imgArray = @[@"List_sd", @"Swipe-Right-and-Left", @"Under-Menu_sd"];

        SRPageControlViewController *pageViewController = [[SRPageControlViewController alloc] initWithNibName:@"SRPageControlViewController" bundle:nil];
        pageViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        pageViewController.imageArr = imgArray;
        pageViewController.titleArr = titleArray;
        pageViewController.showTooltipOnView = @"SRDiscoveryListViewController";
        [self presentViewController:pageViewController animated:NO completion:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if ((APP_DELEGATE).isReqInProcess == false) {
        (APP_DELEGATE).isTabClicked = YES;
        (APP_DELEGATE).timer = NO;
        (APP_DELEGATE).isOnMap = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateTimer object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
        (APP_DELEGATE).timer = YES;
    }
    (APP_DELEGATE).isBackMenu = NO;
    _autoTimer = [NSTimer scheduledTimerWithTimeInterval:(30.0)
                                                 target:self
                                               selector:@selector(refreshData:)
                                               userInfo:nil
                                                repeats:YES];
}
// ----------------------------------------------------------------------------------------------------------
// viewWillDisappear:

- (void)viewWillDisappear:(BOOL)animated {
    // Call super
    [super viewWillDisappear:YES];
    isPresentListView = FALSE;
    [_autoTimer invalidate];
    _autoTimer = nil;

    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self name:kKeyNotificationAddFavouritesSucceed object:nil];
    [defaultCenter removeObserver:self name:kKeyNotificationAddFavouritesFailed object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationStopUpdateTimer object:nil];
}

#pragma mark Init Method

- (void)callDistanceAPI:(NSDictionary *)userDict {
   // NSLog(@"Uesrdict::%@", userDict);
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];


    NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    request.timeoutInterval = 60;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = rows[0];
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];

                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       double convertDist = [distanceWithoutCommas doubleValue];
                                       if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                           if ([distance containsString:@"km"]) {

                                           } else {
                                               //meters to killometer
                                               convertDist = convertDist / 1000;
                                           }
                                       } else {
                                           if ([distance containsString:@"km"]) {
                                               //killometer to miles
                                               convertDist = convertDist * 0.621371192;
                                           } else {
                                               //meters to miles
                                               convertDist = (convertDist * 0.000621371192);
                                           }
                                       }

                                       DistanceClass *obj = [[DistanceClass alloc] init];
                                       obj.dataId = userDict[kKeyId];
                                       NSDictionary *locationDict = userDict[kKeyLocation];
                                       obj.lat = locationDict[kKeyLattitude];
                                       obj.longitude = locationDict[kKeyRadarLong];
                                       obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit];
                                       obj.myLocation = server.myLocation;

                                       //Live Address
                                       NSArray *destinationArray = json[@"destination_addresses"];

                                       NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                       NSArray *tempArr = [(APP_DELEGATE).oldDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                       obj.address = [NSString stringWithFormat:@"in %@", destinationArray[0]];

                                       if (convertDist <= 0) {
                                           obj.address = @"";
                                       }

                                       if (tempArr && tempArr.count) {

                                           NSUInteger indexOfObject = [(APP_DELEGATE).oldDataArray indexOfObjectPassingTest:^BOOL(DistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {

                                               if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                                   return YES;
                                               } else {
                                                   return NO;
                                               }

                                           }];
                                           (APP_DELEGATE).oldDataArray[indexOfObject] = obj;
                                       } else {
                                           [(APP_DELEGATE).oldDataArray addObject:obj];
                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           NSUInteger indexOfUserObject = [usersListArr indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {


                                               if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                                   if (indexOfUserObject != NSNotFound && indexOfUserObject < usersListArr.count) {
                                                       [_objtableView reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                                   }
                                                   return YES;
                                               } else {
                                                   return NO;
                                               }
                                           }];
                                        //   NSLog(@"%lu", (unsigned long) indexOfUserObject);
                                       });
                                   }
                               }
                           }];
}


- (void)getDistance {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (!(APP_DELEGATE).oldDataArray.count) {
        [arr addObjectsFromArray:usersListArr];
    } else {

        for (NSDictionary *userDict in usersListArr) {

            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

            NSArray *tempArr = [(APP_DELEGATE).oldDataArray filteredArrayUsingPredicate:predicateForDistanceObj];

            if (tempArr && tempArr.count) {

                DistanceClass *distanceObj = [tempArr firstObject];

                NSDictionary *locationDict = userDict[kKeyLocation];

                BOOL isBLocationChanged = NO;
                BOOL isALocationChanged = NO;

                if (![distanceObj.lat isEqualToString:locationDict[kKeyLattitude]] || ![distanceObj.longitude isEqualToString:locationDict[kKeyRadarLong]]) {
                    isBLocationChanged = YES;
                }

                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];

                if (distance >= 160) {
                    isALocationChanged = YES;
                }

                if (isBLocationChanged || isALocationChanged) {
                    [arr addObject:userDict];
                }
            } else {
                [arr addObject:userDict];
            }
        }
    }


    for (int i = 0; i < arr.count; i++) {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];


        if (server.myLocation != nil && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            NSBlockOperation *op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled) {
                    [self callDistanceAPI:userDict];
                } else {
                   // NSLog(@"Operation Not canceled");
                }
            }];
            [_distanceQueue addOperation:op];
        }
    }
}

-(void)refreshControllerData {
    [UIView animateWithDuration:0.0 delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         
                     } completion:^(BOOL finished) {
                         [_radarRefreshOverlay setHidden:NO];
                         if (!_radarRefreshOverlay) {
                             // Add view to refresh radar
                             _radarRefreshOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                             _radarRefreshOverlay.backgroundColor = [UIColor blackColor];
                             _radarRefreshOverlay.alpha = 0.7;
                             _radarRefreshOverlay.userInteractionEnabled = TRUE;
                             _radarRefreshOverlay.tag = 100000;
                             
                             // Add update lbl
                             UILabel *updateLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 70, SCREEN_HEIGHT - 170, 140, 20)];
                             [updateLbl setText:@"Update list now"];
                             [updateLbl setTextColor:[UIColor whiteColor]];
                             [updateLbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:14]];
                             [updateLbl setTextAlignment:NSTextAlignmentCenter];
                             [_radarRefreshOverlay addSubview:updateLbl];
                             UITapGestureRecognizer *tapGestOnRefreshOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRefreshOverlay:)];
                             [_radarRefreshOverlay addGestureRecognizer:tapGestOnRefreshOverlay];
                             [self.tabBarController.view addSubview:_radarRefreshOverlay];
                         }
                         UIButton *refreshBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, SCREEN_HEIGHT - 130, 60, 60)];
                         refreshBtn.layer.cornerRadius = refreshBtn.frame.size.width / 2;
                         [refreshBtn setImage:[UIImage imageNamed:@"update-radar-white.png"] forState:UIControlStateNormal];
                         [refreshBtn setBackgroundColor:[UIColor whiteColor]];
                         refreshBtn.opaque = true;
                         [refreshBtn addTarget:self action:@selector(refreshRadarBtnClick) forControlEvents:UIControlEventTouchUpInside];
                         [refreshBtn setUserInteractionEnabled:TRUE];
                         [self.tabBarController.view addSubview:refreshBtn];
                         [self.tabBarController.view bringSubviewToFront:refreshBtn];
                     }];
}

- (void)longPressRadar:(UILongPressGestureRecognizer *)recognizer {

    
}
- (void) refreshData : (id)sender{
     [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
}

- (void)refreshRadarBtnClick {
    [_radarRefreshOverlay setHidden:YES];
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [APP_DELEGATE showActivityIndicator];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
}

- (void)tapOnRefreshOverlay:(UITapGestureRecognizer *)recognizer {
    //Hide radar refresh overlay
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            //do your code
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [_radarRefreshOverlay setHidden:YES];
}
//-----------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SRDiscoveryListViewController" bundle:nil server:server className:kClassList];

    if (self) {
        // Custom initialization
        server = inServer;

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        
        [defaultCenter addObserver:self selector:@selector(userRadarStartLoading) name:kKeyNotificationRadarUserStartLoading object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(getRadarUsersList:)
                              name:kKeyNotificationRadarUserChanges object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(updatedLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(imagesGotDownloaded:)
                              name:kKeyNotificationRadarUserImageDownloaded
                            object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(addUserInvisibleSucceed:)
                              name:kKeyNotificationAddInvisibleSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(addUserInvisibleFailed:)
                              name:kKeyNotificationAddInvisibleFailed
                            object:nil];
    }
    return self;
}

//-----------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




#pragma mark Action Methods

- (void)reloadDataInList {
    self.lblNoData.hidden = YES;
    self.indicatorView.hidden = YES;
    [self.objtableView reloadData];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// gotItBtnAction:
- (IBAction)toolTipGotItBtnAction:(id)sender {
    for (UIView *overlay in self.tabBarController.view.subviews) {
        [APP_DELEGATE hideActivityIndicator];
        if (overlay.tag == 1000000) {
            [overlay removeFromSuperview];
        }
    }
}

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:YES];
}

- (void)blockUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    NSInteger i = [btnSender tag];
    NSDictionary *userDict = usersListArr[i];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = userDict[kKeyId];//connectionParams[kKeyConnectionID];
    params[kKeyType] = @"block";
    params[kKeyIsContact] = @0;
    params[kKeyStatus] = @1;
    NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
    [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    [usersListArr removeObjectAtIndex:i];
    [self.objtableView reloadData];

//    if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
//        if ([userDict[kKeyDegree] integerValue] == 1) {
//            NSDictionary *connectionDict = userDict[kKeyConnection];
//            params[kKeyConnectionStatus] = @kKeyConnectionBlock;
//            strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateConnection, connectionDict[kKeyId]];
//            [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPUT];
//        }
//    } else {
//        params[kKeyConnectionStatus] = @kKeyConnectionBlockForNonConn;
//        [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
//    }
}

- (void)deleteUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    NSInteger i = [btnSender tag];
    NSDictionary *userDict = usersListArr[i];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = userDict[kKeyId];//connectionParams[kKeyConnectionID];
    params[kKeyType] = @"delete";
    params[kKeyIsContact] = @0;
    params[kKeyStatus] = @1;
    NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
    [APP_DELEGATE showActivityIndicator];
    [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    [usersListArr removeObjectAtIndex:i];
    [self.objtableView reloadData];
}

- (void)pingUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    // Get profile
    NSDictionary *userDict = usersListArr[btnSender.tag];
    badgeBtn.hidden = YES;

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:userDict];

    BOOL flag = NO;//Check first if you allow to ping or not

    if ([[dictionary valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[dictionary valueForKey:kKeySetting]];

//        if (![[setting valueForKey:kKeyallowPings]boolValue])
//            flag=NO;


        //
        NSUInteger pingStatus = [[setting valueForKey:kKeyallowPings] integerValue];
        if (pingStatus == 1) {
            flag = YES;
        } else if (pingStatus == 2) {
            flag = YES;
        } else if (pingStatus == 3) {
            if ([[dictionary valueForKey:kKeyConnection] isKindOfClass:[NSDictionary class]]) {
                flag = YES;
            }
        } else if (pingStatus == 4) {
            if ([[dictionary valueForKey:kKeyToFavourite] boolValue]) {
                flag = YES;
            }
        } else if (pingStatus == 5) {
            if ([[dictionary valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
                flag = YES;
            }
        } else if (pingStatus == 6) {
            flag = NO;
        }
    }

    if (flag) {
        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        // Show chat window
        SRChatViewController *chatCon = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:server inObjectInfo:userDict];
        [APP_DELEGATE hideActivityIndicator];
        badgeBtn.hidden = YES;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatCon animated:YES];
        self.hidesBottomBarWhenPushed = NO;
        badgeBtn.hidden = NO;
    } else {
//        (APP_DELEGATE).topBarView.hidden = NO;
        (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
        [SRModalClass showAlert:@"Sorry, this user has disabled chat so they cannot receive any messages."];
        [SwipeableTableViewCell closeAllCells];
    }

}
// ----------------------------------------------------------------------------------------------------------------------
// favouriteUserAction:

- (void)favouriteUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    favTag = btnSender.tag;
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE showActivityIndicator];
    NSDictionary *userDict = usersListArr[favTag];
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassfavourites];
    NSDictionary *params = @{kKeyFavUserID: [userDict valueForKey:kKeyId], kKeyUserID: server.loggedInUserInfo[kKeyId]};
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:NO inMethodType:kPOST];
}

- (void)invisibleUserAction:(UIButton *)btnSender {
    (APP_DELEGATE).isSwipedCell = NO;
    invisibleTag = btnSender.tag;
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE showActivityIndicator];
    NSDictionary *userDict = usersListArr[invisibleTag];
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassInvisible];
    NSDictionary *params = @{kKeyInvisibleUserID: [userDict valueForKey:kKeyId], kKeyUserID: server.loggedInUserInfo[kKeyId]};
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:NO inMethodType:kPOST];
}

- (void)actionOnProfileCellBtn:(UIButton *)sender {
    // Get profile
    NSDictionary *userDict = usersListArr[sender.tag];

    if (![userDict[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
        server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:userDict];
        badgeBtn.hidden = YES;

        // Show user public profile
        SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
        [APP_DELEGATE hideActivityIndicator];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:tabCon animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }

}

- (void)compassTappedCaptured:(UIButton *)sender {
    NSMutableDictionary *userDict = usersListArr[sender.tag];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:userDict server:server];
    [APP_DELEGATE hideActivityIndicator];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

- (void)degreeBtnSelected:(UIButton *)sender {
    // Get profile
    NSDictionary *userDict = usersListArr[sender.tag];
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:userDict];
    badgeBtn.hidden = YES;

    if (![userDict[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
        // Show user public profile
        SRUserTabViewController *tabCon;
        if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            tabCon = [[SRUserTabViewController alloc] initWithNibName:@"SRUserTabViewController" bundle:nil profileData:nil server:server];
        } else {
            tabCon = [[SRUserTabViewController alloc] initWithNibName:@"DisabledDegreeSeperation" bundle:nil profileData:nil server:server];
        }

        [APP_DELEGATE hideActivityIndicator];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:tabCon animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }
}

#pragma mark - Tap Gesture

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]]) {
        //change it to your condition
        return NO;
    }
    return YES;
}

// here we must use override method -didSelectRow atIndexPath
- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)sender {
    SwipeableTableViewCell *cell = (SwipeableTableViewCell *) sender.view;
    if (CGPointEqualToPoint(cell.scrollView.contentOffset, CGPointZero)) {
        NSDictionary *userDict = usersListArr[cell.tag];
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:userDict];
        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];

        if (![userDict[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
            self.tabBarController.tabBar.hidden = YES;
            // Show user public profile
            SRUserTabViewController *tabCon;
            if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
                tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
            } else {
                tabCon = [[SRUserTabViewController alloc] initWithNibName:@"DisabledDegreeSeperation" bundle:nil profileData:nil server:server];
            }

            [APP_DELEGATE hideActivityIndicator];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:tabCon animated:NO];
            self.hidesBottomBarWhenPushed = NO;
        }
    }
}

#pragma mark - TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (usersListArr.count == 0) {
        if (self.isLoadingRadarUser) {
            self.indicatorView.frame = CGRectMake((_objtableView.frame.size.width - 44)/2, (_objtableView.frame.size.height - 44)/2, 44, 44);
            self.indicatorView.hidden = NO;
            tableView.backgroundView = self.indicatorView;
            [tableView bringSubviewToFront:self.lblNoData];
        } else {
            self.lblNoData.hidden = NO;
            self.lblNoData = [[UILabel alloc] initWithFrame:_objtableView.frame];
            self.lblNoData.text = NSLocalizedString(@"lbl.nodata.txt", @"");
            self.lblNoData.textAlignment = NSTextAlignmentCenter;
            self.lblNoData.textColor = [UIColor orangeColor];
            tableView.backgroundView = self.lblNoData;
            [tableView bringSubviewToFront:self.lblNoData];
        }
        return 0;

    } else {
        self.lblNoData.hidden = YES;
        tableView.backgroundView = nil;
        return usersListArr.count;
    }
    return [usersListArr count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"connectionCell";
    NSDictionary *userDict = usersListArr[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];
    
    // For the purposes of this demo, just return a random cell.
    SwipeableTableViewCell *cell;
    if (!cell) {
        cell = [[SwipeableTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //Store UserId and IndexPath
        cell.currentSwipedCellUserId = [userDict valueForKey:kKeyId];
        cell.currentSwipedCellIndexPath = indexPath;
        
        // Tap Gesture
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        singleTap.delegate = self;
        [cell addGestureRecognizer:singleTap];
        cell.tag = indexPath.section;
        singleTap.cancelsTouchesInView = NO;
        
        //Block button
        if ([userDict[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
            if ([userDict[kKeyDegree] integerValue] == 1) {
                UIButton *blockButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideLeft];
                blockButton.backgroundColor = [UIColor orangeColor];
                [blockButton setImage:[UIImage imageNamed:@"ic_cancel"] forState:UIControlStateNormal];
                [blockButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 25.0, 20.0, 15.0)];
                [blockButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -40.0, 5.0, 5.0)];
                [blockButton setTitle:[NSString stringWithFormat:@"Block %@", userDict[kKeyFirstName]] forState:UIControlStateNormal];
                blockButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
                [blockButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                blockButton.tag = indexPath.section;
                [blockButton addTarget:self action:@selector(blockUserAction:) forControlEvents:UIControlEventTouchUpInside];
                
                //
                //////////Delete user
                UIButton *deletButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideLeft];
                deletButton.backgroundColor = [UIColor redColor];
                [deletButton setImage:[UIImage imageNamed:@"close-icon.png"] forState:UIControlStateNormal];
                [deletButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 30.0, 20.0, 15.0)];
                [deletButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -40.0, 5.0, 5.0)];
                [deletButton setTitle:[NSString stringWithFormat:@"Delete %@", userDict[kKeyFirstName]] forState:UIControlStateNormal];
                deletButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
                [deletButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                deletButton.tag = indexPath.section;
                [deletButton addTarget:self action:@selector(deleteUserAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                
            }
        }
        
        //Ping button
        UIButton *pingsButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideRight];
        pingsButton.backgroundColor = [UIColor orangeColor];
        [pingsButton setImage:[UIImage imageNamed:@"mainmenu-pings.png"] forState:UIControlStateNormal];
        [pingsButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 20.0, 15.0, 15.0)];
        [pingsButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -45.0, 5.0, 5.0)];
        //		[pingsButton setTitle:[NSString stringWithFormat:@"Ping %@", [userDict objectForKey:kKeyFirstName]] forState:UIControlStateNormal];
        [pingsButton setTitle:[NSString stringWithFormat:@"Chat %@", userDict[kKeyFirstName]] forState:UIControlStateNormal];
        
        pingsButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        [pingsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        pingsButton.tag = indexPath.section;
        [pingsButton addTarget:self action:@selector(pingUserAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //Favourite button
        UIButton *favButton = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideRight];
        favButton.backgroundColor = [UIColor orangeColor];
        [favButton setImage:[UIImage imageNamed:@"favorite-white.png"] forState:UIControlStateNormal];
        [favButton setImageEdgeInsets:UIEdgeInsetsMake(5.0, 20.0, 15.0, 15.0)];
        [favButton setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -45.0, 5.0, 5.0)];
        if ([userDict[kKeyIsFavourite] boolValue] == true) {
            [favButton setTitle:@"Unfavorite" forState:UIControlStateNormal];
        } else {
            [favButton setTitle:@"Favorite" forState:UIControlStateNormal];
        }
        favButton.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        [favButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        favButton.tag = indexPath.section;
        [favButton addTarget:self action:@selector(favouriteUserAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //Invisible button
        UIButton *invisibleBtn = [cell createButtonWithWidth:100 onSide:SwipeableTableViewCellSideRight];
        invisibleBtn.backgroundColor = [UIColor orangeColor];
        [invisibleBtn setImage:[UIImage imageNamed:@"invisible-white.png"] forState:UIControlStateNormal];
        [invisibleBtn setImageEdgeInsets:UIEdgeInsetsMake(5.0, 20.0, 15.0, 15.0)];
        [invisibleBtn setTitleEdgeInsets:UIEdgeInsetsMake(63.0, -45.0, 5.0, 5.0)];
        
        if ([userDict[kKeyIsInvisible] boolValue] == true) {
            [invisibleBtn setTitle:@"Visible" forState:UIControlStateNormal];
        } else {
            [invisibleBtn setTitle:@"Invisible" forState:UIControlStateNormal];
        }
        
        invisibleBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
        [invisibleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        invisibleBtn.tag = indexPath.section;
        [invisibleBtn addTarget:self action:@selector(invisibleUserAction:) forControlEvents:UIControlEventTouchUpInside];
        
        // Assign values
        SRConnectionCellView *contentView = (SRConnectionCellView *) cell.scrollViewContentView;
        contentView.btnChat.hidden = TRUE;
        contentView.btnGesture.hidden = TRUE;
        contentView.userInfo = userDict;
        [contentView updateStatus];
        if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            if (degree == 1) {
                
//                NSString *lName = [[[NSString stringWithFormat:@"%@", userDict[kKeyLastName]] substringToIndex:1] capitalizedString];
                NSString *lName = [NSString stringWithFormat:@"%@", userDict[kKeyLastName]];
                contentView.lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], lName];

//                contentView.lblName.text = [NSString stringWithFormat:@"%@ %@.", userDict[kKeyFirstName], lName];
            } else if ([[userDict[kKeySetting] objectForKey:kkeyHideLastName] boolValue] == YES) {
                contentView.lblName.text = [NSString stringWithFormat:@"%@", userDict[kKeyFirstName]];
            } else {
                NSString *lName = [NSString stringWithFormat:@"%@", userDict[kKeyLastName]];
//                contentView.lblName.text = [NSString stringWithFormat:@"%@ %@.", userDict[kKeyFirstName], lName];
                contentView.lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], lName];

            }
        } else if ([[userDict[kKeySetting] objectForKey:kkeyHideLastName] boolValue] == YES) {
            contentView.lblName.text = [NSString stringWithFormat:@"%@", userDict[kKeyFirstName]];
        } else {
//            NSString *lName = [[[NSString stringWithFormat:@"%@", userDict[kKeyLastName]] substringToIndex:1] capitalizedString];
            NSString *lName = [NSString stringWithFormat:@"%@", userDict[kKeyLastName]];

            contentView.lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], lName];
        }
        
        CGSize yourLabelSize = [contentView.lblName.text sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:15]}];
        CGRect frame1;
        frame1 = contentView.lblName.frame;
        frame1.size.width = yourLabelSize.width;
        contentView.lblName.frame = frame1;
        
        //        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        //
        //            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
        //                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
        //                if ([imageName length] > 0) {
        //                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
        //
        //                    [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        //                } else
        //                    contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        //            } else
        //                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        //        } else {
        //            contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        //        }
        
        
        if ([userDict[kKeySetting][@"profile_photo"]intValue] == 5 ) {
            contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else if ([userDict[kKeySetting][@"profile_photo"]intValue] == 4 ) {
            contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            if ([userDict[kKeyIsFavourite]intValue] == 1 ) {
                
                NSString* strProfileURL = [self getProfileImageURL:userDict];
                if ([strProfileURL length] > 0){
                    [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
                }else{
                    contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
            }
        } else {
            NSString* strProfileURL = [self getProfileImageURL:userDict];
            if ([strProfileURL length] > 0){
                [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
            }else{
                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            }
        }
        if ([userDict[kKeyOccupation] length] > 0) {
            contentView.lblProfession.text = userDict[kKeyOccupation];
        } else {
            contentView.lblProfession.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");
        }
        //Set Shadow
        if ([contentView.lblProfession.text isEqualToString:@""]) {
            contentView.lblName.frame = CGRectMake(contentView.lblName.frame.origin.x, contentView.lblName.frame.origin.y + 15, contentView.lblName.frame.size.width, contentView.lblName.frame.size.height);
        }
        contentView.favImg.layer.shadowOffset = CGSizeMake(0, 1);
        contentView.favImg.layer.shadowRadius = 1.0;
        contentView.favImg.layer.shadowOpacity = 1;
        contentView.favImg.layer.masksToBounds = NO;
        if ([userDict[kKeyIsFavourite] boolValue] == true) {
            contentView.favImg.hidden = FALSE;
        } else {
            contentView.favImg.hidden = TRUE;
        }
        //Invisible
        if ([userDict[kKeyIsInvisible] boolValue] == true) {
            contentView.btnInvisible.hidden = FALSE;
            [contentView.btnInvisible addTarget:self action:@selector(invisibleUserAction:) forControlEvents:UIControlEventTouchUpInside];
            contentView.btnInvisible.tag = indexPath.section;
            contentView.blackOverlay.hidden = FALSE;
        } else {
            contentView.btnInvisible.hidden = TRUE;
            contentView.blackOverlay.hidden = TRUE;
        }
        
        // Location
        if ([userDict[kKeyLocation] objectForKey:kKeyLiveAddress] != nil && [[userDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 1) {
            contentView.lblAddress.text = [userDict[kKeyLocation] objectForKey:kKeyLiveAddress];
            [contentView.lblAddress sizeToFit];
            
            if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                if ([userDict[kKeyDistance] floatValue] > 805) {
                    contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@ %@", [userDict[kKeyDistance] floatValue], distanceUnit, @"approx"];
                } else {
                    contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@", [userDict[kKeyDistance] floatValue], distanceUnit];
                }
            } else {
                float distance = [userDict[kKeyDistance] floatValue];
                
                if ([userDict[kKeyDistance] floatValue] > 500) {
                    contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@ %@", distance, distanceUnit, @"approx"];
                } else {
                    contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@", distance, distanceUnit];
                }
            }
        } else {
            contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
            contentView.lblDistance.textAlignment = NSTextAlignmentCenter;
            contentView.lblDistance.text = @"     N/A";
        }
        if ([userDict[kKeyFamily] isKindOfClass:[NSDictionary class]]) {
            // NSDictionary *familyDict = [userDict objectForKey:kKeyFamily];
            contentView.lblDating.hidden = YES;
            contentView.imgDating.hidden = YES;
            contentView.lblFamily.hidden = YES;
        }
        // Dating
        else if ([userDict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *settingDict = userDict[kKeySetting];
            contentView.lblFamily.hidden = YES;
            
            if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
                contentView.lblDating.hidden = NO;
                contentView.imgDating.hidden = NO;
                contentView.imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
                contentView.lblDating.textColor = [UIColor whiteColor];
            } else {
                contentView.imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
                contentView.lblDating.hidden = YES;
                contentView.imgDating.hidden = YES;
            }
        } else {
            contentView.lblDating.hidden = YES;
            contentView.imgDating.hidden = YES;
            contentView.lblFamily.hidden = YES;
            contentView.imgDating.image = [UIImage imageNamed:@"ic_dating_white.png"];
        }
        
        // Degree
        [contentView.btnDegree addTarget:self action:@selector(degreeBtnSelected:) forControlEvents:UIControlEventTouchUpInside];
        contentView.btnDegree.tag = indexPath.section;
        
        contentView.btnDegree.layer.cornerRadius = contentView.btnDegree.frame.size.width / 2.0;
        contentView.btnDegree.layer.masksToBounds = YES;
        if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            [contentView.btnDegree setTitle:[NSString stringWithFormat:@" %ld°", (long) degree] forState:UIControlStateNormal];
        } else {
            [contentView.btnDegree setTitle:@" 6°" forState:UIControlStateNormal];
        }
        
        if (userDict[kKeyLocation] && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            //Compass location
            CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                server.myLocation.coordinate.longitude};
            
            NSDictionary *userLocDict = userDict[kKeyLocation];
            CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};
            
            [contentView.btnCompass addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
            contentView.btnCompass.tag = indexPath.section;
            
            NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
            if (directionValue == kKeyDirectionNorth) {
                contentView.lblCompassDirection.text = @"N";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionEast) {
                contentView.lblCompassDirection.text = @"E";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionSouth) {
                contentView.lblCompassDirection.text = @"S";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionWest) {
                contentView.lblCompassDirection.text = @"W";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionNorthEast) {
                contentView.lblCompassDirection.text = @"NE";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionNorthWest) {
                contentView.lblCompassDirection.text = @"NW";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionSouthEast) {
                contentView.lblCompassDirection.text = @"SE";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
            } else if (directionValue == kKeyDirectionSoutnWest) {
                contentView.lblCompassDirection.text = @"SW";
                [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
            }
        }
        contentView.btnProfileView.tag = indexPath.section;
        
        // Adjust green dot label appearence
        contentView.lblOnline.hidden = YES;
        CGRect frame = contentView.lblOnline.frame;
        
        
        // Adjust green dot label appearence
        contentView.lblOnline.layer.cornerRadius = contentView.lblOnline.frame.size.width / 2;
        contentView.lblOnline.layer.masksToBounds = YES;
        contentView.lblOnline.clipsToBounds = YES;
        
        if ([userDict[kKeyStatus] isEqualToString:@"1"]) {
            contentView.lblOnline.hidden = NO;
            contentView.lblName.frame = CGRectMake(frame.origin.x + 15, contentView.lblName.frame.origin.y, contentView.lblName.frame.size.width + 10, contentView.lblName.frame.size.height);
        } else {
            contentView.lblOnline.hidden = YES;
            contentView.lblName.frame = CGRectMake(frame.origin.x, contentView.lblName.frame.origin.y, contentView.lblName.frame.size.width + 10, contentView.lblName.frame.size.height);
        }
        CGRect btnInvisibleFrame;
        btnInvisibleFrame = contentView.btnInvisible.frame;
        btnInvisibleFrame.origin.x = contentView.lblName.frame.size.width + contentView.lblName.frame.origin.x + 8;
        
        if (btnInvisibleFrame.origin.x > SCREEN_WIDTH) {
            btnInvisibleFrame.origin.x = SCREEN_WIDTH - 35;
        }
        contentView.btnInvisible.frame = btnInvisibleFrame;
        
        contentView.lblAddress.frame = CGRectMake(frame.origin.x, contentView.lblAddress.frame.origin.y, contentView.lblAddress.frame.size.width + 10, contentView.lblAddress.frame.size.height);
        contentView.lblProfession.frame = CGRectMake(frame.origin.x, contentView.lblProfession.frame.origin.y, contentView.lblProfession.frame.size.width + 10, contentView.lblProfession.frame.size.height);
        contentView.lblFamily.frame = CGRectMake(frame.origin.x, contentView.lblFamily.frame.origin.y, contentView.lblFamily.frame.size.width, contentView.lblFamily.frame.size.height);
        contentView.imgDating.frame = CGRectMake(frame.origin.x, contentView.imgDating.frame.origin.y, contentView.imgDating.frame.size.width, contentView.imgDating.frame.size.height);
        contentView.lblDating.frame = CGRectMake(frame.origin.x + contentView.imgDating.frame.size.width + 4, contentView.lblDating.frame.origin.y, contentView.lblDating.frame.size.width, contentView.lblDating.frame.size.height);
        
        
        BOOL isSwippedCell = NO;
        if ((APP_DELEGATE).swipedCellUserId != nil && (APP_DELEGATE).swipedCellIndexPath != nil) {
            if ([indexPath isEqual:(APP_DELEGATE).swipedCellIndexPath] && [[userDict valueForKey:kKeyId] isEqualToString:(APP_DELEGATE).swipedCellUserId]) {
                isSwippedCell = YES;
            }
        }
        
        if (isSwippedCell) {
            if ((APP_DELEGATE).slideToRight) {
                [cell slideToRight];
            } else {
                [cell slidetoLeft];
            }
        }
    }
    return cell;
}

#pragma mark TableView Delegates Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }
    return headerView;
}

#pragma mark Notification Method

- (void)refreshUsers {
    [[SRAPIManager sharedInstance] updateToken:(APP_DELEGATE).server.token];
    [[SRAPIManager sharedInstance] getConnectionsWithStatus:2 completion:^(id _Nonnull result, NSError *_Nonnull error) {
        if (error == nil) {
          //  NSLog(@"Loaded list of connections");
        } else {
          //  NSLog(@"Failed to load my connections");
        }
    }];
}

- (void)userRadarStartLoading {
  //  NSLog(@"-------> userRadarStartLoading");
    self.isLoadingRadarUser = YES;
    if (usersListArr.count <= 0) {
        //Show loading
        self.indicatorView.hidden = NO;
        //If table show No Data and app reload, need change state to loading animation
        [self.objtableView reloadData];
    } else {
        // Hide loading
        self.indicatorView.hidden = YES;
    }
    
}

- (void)getRadarUsersList:(NSNotification *)inNotify {
    if ([inNotify isKindOfClass:[NSArray class]]) {
        return;
    }
    
    //Call api get radar user success
    self.isLoadingRadarUser = NO;
    
    usersListArr = [inNotify object];
    sourceListArr = [inNotify object];

    NSSortDescriptor *sortDescriptorX = [NSSortDescriptor sortDescriptorWithKey:kKeyFirstName ascending:YES];
    NSSortDescriptor *sortDescriptorY = [NSSortDescriptor sortDescriptorWithKey:kKeyLastName ascending:YES];
    NSArray *finishedSort = @[sortDescriptorX, sortDescriptorY];
    NSArray *returnedArray = [usersListArr sortedArrayUsingDescriptors:finishedSort];
    // Get sorted array
    usersListArr = [[NSMutableArray alloc] initWithArray:returnedArray];
    sourceListArr = [[NSMutableArray alloc] initWithArray:returnedArray];

//    if (isPresentListView) {
        [self reloadDataInList];
//    }
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
}

- (void)updatedLocation:(NSNotification *)inNotify {
}

- (void)imagesGotDownloaded:(NSNotification *)inNotify {
    // Remove all objects
    dotsUserPicsArr = [inNotify object];

    [self.objtableView reloadData];
}

- (void)addUserInFavouriteSucceed:(NSNotification *)inNotify {
    (APP_DELEGATE).swipedCellUserId = nil;
    (APP_DELEGATE).swipedCellIndexPath = nil;
    [SwipeableTableViewCell closeAllCells];
//    [SRModalClass showAlert:inNotify.object];

    if (usersListArr.count > 0 && favTag <= usersListArr.count) {
        NSMutableDictionary *favDict = usersListArr[favTag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:favTag];
        SwipeableTableViewCell *cell = [_objtableView cellForRowAtIndexPath:indexPath];
        SRConnectionCellView *contentView = (SRConnectionCellView *) cell.scrollViewContentView;
        if ([inNotify.object isEqualToString:@"favourite false"]) {
            [favDict setValue:@false forKey:kKeyIsFavourite];
            contentView.favImg.hidden = TRUE;
            [SRModalClass showAlertWithTitle:@"Successfully remove from favorite" alertTitle:@"Success"];
            
        } else {
            [favDict setValue:@true forKey:kKeyIsFavourite];
            contentView.favImg.hidden = FALSE;
            [SRModalClass showAlertWithTitle:@"Successfully add to favorite" alertTitle:@"Success"];
        }

        usersListArr[favTag] = favDict;
        NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:favTag];
        if (sections.count <= self.objtableView.numberOfSections) {
            [self.objtableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        }

        NSUInteger indexOfOldDataObject = [(APP_DELEGATE).topBarView.groupUsers indexOfObjectPassingTest:^BOOL(NSDictionary *dictObj, NSUInteger idx, BOOL *_Nonnull stop) {

            if ([dictObj[kKeyId] isEqualToString:favDict[kKeyId]]) {
                return YES;
                *stop = YES;
            } else {
                return NO;
            }
        }];

        if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).topBarView.groupUsers.count) {
            (APP_DELEGATE).topBarView.groupUsers[indexOfOldDataObject] = favDict;
        }

        //Replace radar array here
        UINavigationController *navController = [self.tabBarController viewControllers][1];
        SRDiscoveryRadarViewController *radar = [navController.viewControllers firstObject];
        NSUInteger indexOfSourceArr = [radar.sourceArr indexOfObjectPassingTest:^BOOL(NSDictionary *dictObj, NSUInteger idx, BOOL *_Nonnull stop) {

            if ([dictObj[kKeyId] isEqualToString:favDict[kKeyId]]) {
                return YES;
                *stop = YES;
            } else {
                return NO;
            }
        }];

        if (indexOfSourceArr != NSNotFound && radar.sourceArr && radar.sourceArr.count && indexOfSourceArr < radar.sourceArr.count) {
            radar.sourceArr[indexOfSourceArr] = favDict;
        }
    }
    [APP_DELEGATE hideActivityIndicator];
}

- (void)addUserInFavouriteFailed:(NSNotification *)inNotify {
    (APP_DELEGATE).swipedCellUserId = nil;
    (APP_DELEGATE).swipedCellIndexPath = nil;
    [SRModalClass showAlert:inNotify.object];
    [APP_DELEGATE hideActivityIndicator];
}

- (void)addUserInvisibleSucceed:(NSNotification *)inNotify {
    (APP_DELEGATE).swipedCellUserId = nil;
    (APP_DELEGATE).swipedCellIndexPath = nil;
    [SwipeableTableViewCell closeAllCells];
    [APP_DELEGATE hideActivityIndicator];

    if (usersListArr.count > 0 && invisibleTag <= usersListArr.count) {
        NSMutableDictionary *invisibleDict = usersListArr[invisibleTag];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:invisibleTag];
        SwipeableTableViewCell *cell = [_objtableView cellForRowAtIndexPath:indexPath];
        SRConnectionCellView *contentView = (SRConnectionCellView *) cell.scrollViewContentView;
        if ([inNotify.object isEqualToString:@"invisible false"]) {
            [invisibleDict setValue:@false forKey:kKeyIsInvisible];
            contentView.btnInvisible.hidden = TRUE;
            contentView.blackOverlay.hidden = TRUE;
        } else {
            [invisibleDict setValue:@true forKey:kKeyIsInvisible];
            contentView.btnInvisible.hidden = FALSE;
            contentView.blackOverlay.hidden = FALSE;
        }
        usersListArr[invisibleTag] = invisibleDict;

        NSIndexSet *sections = [[NSIndexSet alloc] initWithIndex:invisibleTag];
        [self.objtableView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)addUserInvisibleFailed:(NSNotification *)inNotify {
    (APP_DELEGATE).swipedCellUserId = nil;
    (APP_DELEGATE).swipedCellIndexPath = nil;
    [SRModalClass showAlert:inNotify.object];
    [APP_DELEGATE hideActivityIndicator];
}

-(NSString*) getProfileImageURL:(NSDictionary*) userDict{
    
    if  ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary self]])  {
        NSDictionary *profileDict = userDict[kKeyProfileImage];
        if (profileDict[kKeyImageName] != nil){
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                
                return imageUrl;
            }
        }
    }
    
    return @"";
}

- (void)deletBlockConnectionFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];
    [self.objtableView reloadData];
}
- (void)deletBlockConnectionSucceed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[inNotify object]];
    [self.objtableView reloadData];
}


@end
