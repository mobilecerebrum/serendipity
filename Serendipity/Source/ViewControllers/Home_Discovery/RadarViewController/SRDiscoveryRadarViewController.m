#include <sys/sysctl.h>
//#import "SRModalClass.h"
#import "SRUserTabViewController.h"
#import "SRDiscoveryRadarViewController.h"
#import "CalloutAnnotationView.h"
#import "SRPageControlViewController.h"
#import "UIImage+animatedGIF.h"
#import "SRAppDelegate.h"
#import "SRMapViewController.h"
#import "SRProfileTabViewController.h"
#import "SRGroupTabViewController.h"
#import "SREventTabViewController.h"
#import "SRDropPinViewController.h"
#import "SRMyConnectionsViewController.h"
#import "SRTrackingViewController.h"
#import <CoreLocation/CLAvailability.h>
@import TSLocationManager;
@import TSBackgroundFetch;

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)
#define kKeyModifiedFlag @"flag"

@interface radarDistanceClass : NSObject {
    
}

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *distance;
@property(nonatomic, strong) NSString *dataId;
@property(strong, nonatomic) CLLocation *myLocation;
@property(nonatomic, strong) NSDictionary *userDict;

+ (radarDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation;

@end

@implementation radarDistanceClass

+ (radarDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation {

    radarDistanceClass *obj = [[radarDistanceClass alloc] init];
    obj.dataId = userDict[kKeyId];
    NSDictionary *locationDict = userDict[kKeyLocation];
    obj.lat = locationDict[kKeyLattitude];
    obj.longitude = locationDict[kKeyRadarLong];
    obj.distance = userDict[kKeyDistance];
    obj.myLocation = myLocation;
    obj.userDict = userDict;

    return obj;
}

@end

@interface SRDiscoveryRadarViewController () {
    CGFloat threshold;
    BOOL shouldDragX;
    CLLocationManager *compassManager;
}

@property(nonatomic, strong) NSOperationQueue *distanceQueue;
@property(nonatomic, strong) NSMutableArray *didEnterRegionArray;

@end


@implementation SRDiscoveryRadarViewController
@synthesize sourceArr;

#pragma mark
#pragma mark - Other Methods
#pragma mark


- (UIImage *)makeRoundedImage:(UIImage *)image
                       radius:(float)radius; {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = 100 / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Return
    return roundedImage;
}

// -------------------------------------------------------------------------------------------------
// makeServerCallToUpdateLocation

- (void)makeServerCallToUpdateLocation:(CLLocation *)inLocation {
    addTextInSignificantTxt(@"makeServerCallToUpdateLocation");
    _oldLocation = server.myLocation;
    // Update the location
    if ((_oldLocation.coordinate.latitude != inLocation.coordinate.latitude) || (_oldLocation.coordinate.longitude != inLocation.coordinate.longitude)) {
        NSString *offset_lat = @"", *offset_long = @"", *offset_address = @"";
        addTextInSignificantTxt(@"Proceed to location update");
        addTextInSignificantTxt([NSString stringWithFormat:@"New location data : newlat %f, newlong %f", inLocation.coordinate.latitude, inLocation.coordinate.longitude]);
        server.myLocation = inLocation;
        //Get Address from location
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
            addTextInSignificantTxt(@"In Background");
            updatedLocation = @"";
            NSString *urlString = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateLocation, server.loggedInUserInfo[kKeyId]];
            NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
            NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];

            if (![[server.loggedInUserInfo[kKeySetting] objectForKey:kkeyDistanceAccuracy] isEqualToString:@"0"]) {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setValue:latitude forKey:kKeyLattitude];
                [dict setValue:longitude forKey:kKeyLongitude];
                [dict setValue:[server.loggedInUserInfo[kKeySetting] objectForKey:kkeyDistanceAccuracy] forKey:kkeyDistanceAccuracy];
                dict = [[SRModalClass addLocationOffset:[dict mutableCopy]] mutableCopy];
                offset_lat = [dict valueForKey:kKeyLattitude];
                offset_long = [dict valueForKey:kKeyLongitude];
            } else {
                offset_lat = @"0";
                offset_long = @"0";
            }
            double sped = server.myLocation.speed * 2.23694;
            NSString *speed = [NSString stringWithFormat:@"%f", sped];
          //  NSLog(@"ℹ️ User's speed %@", speed);
            NSString *direction = [NSString stringWithFormat:@"%f", server.myLocation.course];
            addTextInSignificantTxt([NSString stringWithFormat:@"Direction of user : %@", [NSString stringWithFormat:@"%f", server.myLocation.course]]);
            NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
            int version = [infoDict[@"CFBundleVersion"] intValue];
            NSString *iOSVersion = [[self getModel] stringByAppendingString:[UIDevice currentDevice].systemVersion];
            addTextInSignificantTxt([NSString stringWithFormat:@"iOSVersion : %@", iOSVersion]);
            NSDictionary *params = @{kKeyLattitude: latitude, kKeyLongitude: longitude, kKeyLiveAddress: updatedLocation, kKeySpeed: speed, kKeyDirection: direction, kKeyoffsetAddreass: offset_address, kKeyoffsetLong: offset_long, kKeyoffsetLat: offset_lat, kKeyBuildVersion: [NSNumber numberWithInt:version], KkeyDeviceVersion: iOSVersion};
            addTextInSignificantTxt(@"makeAsychronousRequest");
            [server makeAsychronousRequest:urlString inParams:params isIndicatorRequired:NO inMethodType:kPUT];
        } else {
            [self getAddressFromLatLon:server.myLocation];
        }
    } else {
        addTextInSignificantTxt([NSString stringWithFormat:@"old and new location data : oldLatitude %f, oldLong %f, newlat %f, newlong %f", _oldLocation.coordinate.latitude, _oldLocation.coordinate.longitude, inLocation.coordinate.latitude, inLocation.coordinate.longitude]);
    }
}

- (void)stopTimer {
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    (APP_DELEGATE).isReqInProcess = FALSE;
    (APP_DELEGATE).isTabClicked = FALSE;
}

- (void)updateTimerSetting {
    if ((APP_DELEGATE).isOnMap && (APP_DELEGATE).isTrackUserSelected) {
        [self updateTimerSettingsWithDelay:5];
    } else {
        [self updateTimerSettingsWithDelay:10];
    }
}

- (void)updateTimerSettingsWithDelay:(int)delay {
    [self.updateTimer invalidate];
    self.updateTimer = nil;
    int delayTime = delay;
    isTimerOn = YES;
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:delayTime
                                                        target:self
                                                      selector:@selector(makeServerCallToGetUsersList)
                                                      userInfo:nil
                                                       repeats:YES];
}

// -----------------------------------------------------------------------------------------------
- (void)makeServerCallToGetUsersList {
    if ([server.loggedInUserInfo valueForKey:kKeyId]) {
        if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateBackground) {
            _isGetUsersListCallInProgress = YES;

            if (_locationUpdateDate) {

                NSDate *updateDate = [NSDate date];

                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSDateComponents *components = [calendar components:NSCalendarUnitSecond fromDate:_locationUpdateDate
                                                             toDate:updateDate options:0];
                _secondDifference = components.second;

                _locationUpdateDate = [NSDate date];
            } else {
                _locationUpdateDate = [NSDate date];
                _secondDifference = 0;
            }
            NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
            NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
            NSString *urlStr;
            NSString *distance;
            //if (![latitude isEqualToString:@"0.000000"] && ![longitude isEqualToString:@"0.000000"])
            {
                distance = [NSString stringWithFormat:@"%.fmi", 30000.0];
                if (isTimerOn == NO || (APP_DELEGATE).timer == NO) {
                    urlStr = [NSString stringWithFormat:@"%@?distance=%@&lat=%@&lon=%@&type=%@&degree=%@", kKeyClassGetRadarUsers, distance, latitude, longitude, @"ALL", @"6"];
                } else {
                    urlStr = [NSString stringWithFormat:@"%@?distance=%@&lat=%@&lon=%@&type=%@&degree=%@", kKeyClassGetRadarUsers, distance, latitude, longitude, @"CONNECTION", @"1"];
                }
                (APP_DELEGATE).isReqInProcess = true;
                [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserStartLoading object:nil];
            }
        }
    } else {
      //  NSLog(@"log info not found");
    }
}

// --------------------------------------------------------------------------------
// getAddressFromLatLon:
- (void)getAddressFromLatLon:(CLLocation *)bestLocation {
    //Call Google API for correct distance and address from two locations
    if (server.myLocation != nil) {
        updatedLocation = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f&lon=%f", kNominatimServer, kAddressApi, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        request.timeoutInterval = 60;
        [request setHTTPMethod:@"GET"];
        NSError *error = nil;
        NSURLResponse *response = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&response
                                                         error:&error];
        if (data) {
            if (!error) {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                Boolean isAddressPresent = true;
                for (NSString *keyStr in json) {
                    if ([keyStr isEqualToString:@"error"]) {
                        isAddressPresent = false;
                    }
                }
                if (isAddressPresent) {
                    NSString *offset_lat = @"", *offset_long = @"", *offset_address = @"";
                  //  NSLog(@"Reverse Geocode Address : %@", [json valueForKey:@"display_name"]);
                    addTextInSignificantTxt([NSString stringWithFormat:@"Reverse Geocode Address : %@", [json valueForKey:@"display_name"]]);
                    updatedLocation = [json valueForKey:@"display_name"];
                    NSString *urlString = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateLocation, server.loggedInUserInfo[kKeyId]];
                    NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
                    NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
                    if (![[server.loggedInUserInfo[kKeySetting] objectForKey:kkeyDistanceAccuracy] isEqualToString:@"0"]) {
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        [dict setValue:latitude forKey:kKeyLattitude];
                        [dict setValue:longitude forKey:kKeyLongitude];
                        [dict setValue:[server.loggedInUserInfo[kKeySetting] objectForKey:kkeyDistanceAccuracy] forKey:kkeyDistanceAccuracy];
                        dict = [[SRModalClass addLocationOffset:[dict mutableCopy]] mutableCopy];
                        offset_lat = [dict valueForKey:kKeyLattitude];
                        offset_long = [dict valueForKey:kKeyLongitude];
                    } else {
                        offset_lat = @"0";
                        offset_long = @"0";
                    }
                    //Add User speed and heading (direction)
                    double sped = server.myLocation.speed * 2.23694;
                    NSString *speed = [NSString stringWithFormat:@"%f", sped];
                   // NSLog(@"ℹ️ User's speed %@", speed);
                    NSString *direction = [NSString stringWithFormat:@"%f", server.myLocation.course];
                    addTextInSignificantTxt([NSString stringWithFormat:@"Direction of user : %@", [NSString stringWithFormat:@"%f", server.myLocation.course]]);
                    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
                    NSString *version = infoDict[@"CFBundleVersion"];
                    NSString *iOSVersion = [[self getModel] stringByAppendingString:[UIDevice currentDevice].systemVersion];
                    NSDictionary *params = @{kKeyLattitude: latitude, kKeyLongitude: longitude, kKeyLiveAddress: updatedLocation, kKeySpeed: speed, kKeyDirection: direction, kKeyoffsetAddreass: offset_address, kKeyoffsetLong: offset_long, kKeyoffsetLat: offset_lat, kKeyBuildVersion: version, KkeyDeviceVersion: iOSVersion};
                    [server makeAsychronousRequest:urlString inParams:params isIndicatorRequired:NO inMethodType:kPUT];
                    addressStr = updatedLocation;
                    addTextInSignificantTxt([NSString stringWithFormat:@"Call update location with lat long"]);
                    NSDictionary *locDict = @{kKeyLiveAddress: updatedLocation};
                    [server.loggedInUserInfo setValue:locDict forKey:kKeyLocation];
                    // Post the notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarLocationUpdated object:nil];
                } else {
                   // NSLog(@"Reverse Geocode error : %@", error.localizedDescription);
                    updatedLocation = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                    addTextInSignificantTxt([NSString stringWithFormat:@"Reverse Geocode error : %@", error.localizedDescription]);
                }
            }
        }
    }
}

- (NSString *)getModel {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *model = malloc(size);
    sysctlbyname("hw.machine", model, &size, NULL, 0);
    NSString *deviceModel = [NSString stringWithCString:model encoding:NSUTF8StringEncoding];
    free(model);
    if ([deviceModel isEqualToString:@"iPod1,1"] || [deviceModel isEqualToString:@"iPod2,1"] || [deviceModel isEqualToString:@"iPod3,1"] || [deviceModel isEqualToString:@"iPod4,1"] || [deviceModel isEqualToString:@"iPod5,1"] || [deviceModel isEqualToString:@"iPod7,1"]) {
        deviceModel = @"iPod Touch";
    } else if ([deviceModel isEqualToString:@"iPhone1,1"]) {
        deviceModel = @"iPhone";
    } else if ([deviceModel isEqualToString:@"iPhone1,2"]) {
        deviceModel = @"iPhone 3G";
    } else if ([deviceModel isEqualToString:@"iPhone2,1"]) {
        deviceModel = @"iPhone 3GS";
    } else if ([deviceModel isEqualToString:@"iPhone3,1"] || [deviceModel isEqualToString:@"iPhone3,2"] || [deviceModel isEqualToString:@"iPhone3,3"]) {
        deviceModel = @"iPhone 4";
    } else if ([deviceModel isEqualToString:@"iPhone4,1"]) {
        deviceModel = @"iPhone4S";
    } else if ([deviceModel isEqualToString:@"iPhone5,1"] || [deviceModel isEqualToString:@"iPhone5,2"]) {
        deviceModel = @"iPhone 5";
    } else if ([deviceModel isEqualToString:@"iPhone5,3"] || [deviceModel isEqualToString:@"iPhone5,4"]) {
        deviceModel = @"iPhone 5C";
    } else if ([deviceModel isEqualToString:@"iPhone6,1"] || [deviceModel isEqualToString:@"iPhone6,2"]) {
        deviceModel = @"iPhone 5S";
    } else if ([deviceModel isEqualToString:@"iPhone7,2"]) {
        deviceModel = @"iPhone 6";
    } else if ([deviceModel isEqualToString:@"iPhone7,1"]) {
        deviceModel = @"iPhone 6Plus";
    } else if ([deviceModel isEqualToString:@"iPhone8,1"]) {
        deviceModel = @"iPhone 6S";
    } else if ([deviceModel isEqualToString:@"iPhone8,2"]) {
        deviceModel = @"iPhone 6SPlus";
    } else if ([deviceModel isEqualToString:@"iPhone8,4"]) {
        deviceModel = @"iPhone SE";
    } else if ([deviceModel isEqualToString:@"iPhone9,1"] || [deviceModel isEqualToString:@"iPhone9,3"]) {
        deviceModel = @"iPhone 7";
    } else if ([deviceModel isEqualToString:@"iPhone9,2"] || [deviceModel isEqualToString:@"iPhone9,4"]) {
        deviceModel = @"iPhone 7Plus";
    } else if ([deviceModel isEqualToString:@"iPhone10,1"] || [deviceModel isEqualToString:@"iPhone10,4"]) {
        deviceModel = @"iPhone 8";
    } else if ([deviceModel isEqualToString:@"iPhone10,5"]) {
        deviceModel = @"iPhone 8Plus";
    } else if ([deviceModel isEqualToString:@"iPhone10,3"] || [deviceModel isEqualToString:@"iPhone10,6"]) {
        deviceModel = @"iPhone X";
    }
    return deviceModel;
}

- (void)updateLocation {
    //Call update location notification here after getting updated location
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateLocation, server.loggedInUserInfo[kKeyId]];
    NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
    NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
    //Add User speed and heading (direction)
    NSString *speed = @"0";

    NSString *direction = [self getDirectionString:(float) server.myLocation.course];
    //[self getDirection:_oldLocation.coordinate toCoordinate:server.myLocation.coordinate];
    NSDictionary *params = @{kKeyLattitude: latitude, kKeyLongitude: longitude, kKeyLiveAddress: updatedLocation, kKeySpeed: speed, kKeyDirection: direction};
    [server makeAsychronousRequest:urlString inParams:params isIndicatorRequired:NO inMethodType:kPUT];
    addressStr = updatedLocation;
    addTextInSignificantTxt([NSString stringWithFormat:@"Call update location with different address"]);
    NSDictionary *locDict = @{kKeyLiveAddress: updatedLocation};
    [server.loggedInUserInfo setValue:locDict forKey:kKeyLocation];
    // Post the notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarLocationUpdated object:nil];
    updateRemaining = NO;
}

#pragma mark - Radar Methods

- (NSString *)getDirection:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc {
    float lat1 = (float) degreesToRadians(fromLoc.latitude);
    float lot2 = (float) degreesToRadians(toLoc.latitude);
    float diff = (float) (toLoc.longitude - fromLoc.longitude);
    float londiff = (float) degreesToRadians(diff);
    float y = (float) (sin(londiff) * cos(lot2));
    float x = (float) (cos(lat1) * sin(lot2) - sin(lat1) * cos(lot2) * cos(londiff));
    float resultDegree = (float) (radiandsToDegrees(atan2(y, x)) + 360);
    float degree = fmodf(resultDegree, 360.0);
    float directionValue = roundf(degree / 45);

    if (directionValue < 0) {
        directionValue = directionValue + 8;
    }
    NSArray *arrStr = @[@"N", @"NE", @"E", @"SE", @"S", @"SW", @"W", @"NW", @"N"];
    return [arrStr objectAtIndex:directionValue];
}

- (NSString *)getDirectionString:(float)degree {
    NSInteger degreeint = (NSInteger) roundf((degree / 45));
    NSArray *arrStr = @[@"N", @"NE", @"E", @"SE", @"S", @"SW", @"W", @"NW", @"N"];
    return arrStr[degreeint];
}

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc {
    float fLat = (float) degreesToRadians(fromLoc.latitude);
    float fLng = (float) degreesToRadians(fromLoc.longitude);
    float tLat = (float) degreesToRadians(toLoc.latitude);
    float tLng = (float) degreesToRadians(toLoc.longitude);

    float degree = (float) radiandsToDegrees(atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng)));

    if (degree >= 0) {
        return -degree;
    } else {
        return -(360 + degree);
    }

    return degree;
}

// -----------------------------------------------------------------------------------------------
// rotateArcsToHeading:

- (void)rotateArcsToHeading:(CGFloat)angle {
    // rotate the circle to heading degree
    radarCircleView.transform = CGAffineTransformMakeRotation(angle);

    // rotate all dots to opposite angle to keep the profile image straight up
    for (SRRadarDotView *dot in dotsArr) {
        dot.transform = CGAffineTransformMakeRotation(-angle);
    }
}

// ---------------------------------------------------------------------------------------
// setDisplayForRadar


- (void)setDisplayForRadar {
    // Spin the view of focus
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];

    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusNotDetermined) {
        return;
    }
    
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spin.duration = 1.5;
    spin.toValue = [NSNumber numberWithFloat:M_PI];
    spin.cumulative = YES;
    spin.removedOnCompletion = NO;
    spin.repeatCount = MAXFLOAT;
    [radarFocusView.layer addAnimation:spin forKey:@"spinRadarView"];
    // (APP_DELEGATE).lastLocation=[[CLLocation alloc]initWithLatitude:18.50950782424136  longitude:73.92428722232579];;
    //server.myLocation=(APP_DELEGATE).lastLocation;
    // START BACKGROUND GEOLOCATION
//    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", kSerendipityServer, kKeyClassUpdateLocation, server.loggedInUserInfo[kKeyId]];

    [[SLocationService sharedInstance] initLocationTracker:server];
//    TSLocationManager *bgGeo = [[SLocationService sharedInstance] getLocationManager];
//
//
//
//    // Add "location" event listener
//    void (^success)(TSLocation *) = ^void(TSLocation *tsLocation) {
//        addTextInSignificantTxt([NSString stringWithFormat:@"Location call back method executed successfully"]);
//        (APP_DELEGATE).lastLocation = tsLocation.location;
//        server.myLocation = (APP_DELEGATE).lastLocation;
//    };
//    void (^failure)(NSError *) = ^void(NSError *error) {
//        switch ([error code]) {
//            case kCLErrorNetwork: // general, network-related error
//            {
//                addTextInSignificantTxt([NSString stringWithFormat:@"Network Connection Problem : %@", error]);
//            }
//                break;
//            case kCLErrorDenied: {
//                addTextInSignificantTxt([NSString stringWithFormat:@"user has denied to use current Location : %@", error]);
//            }
//                break;
//            default: {
//                addTextInSignificantTxt([NSString stringWithFormat:@"unknown network error: %@", error]);
//            }
//                break;
//        }
//        NSLog(@"*** event location error: %@", error);
//    };
//    [bgGeo onLocation:success failure:failure];
//
//    // Add motionchange event listener
//    void (^callback)(TSLocation *) = ^void(TSLocation *tsLocation) {
//        NSDictionary *params = @{
//                @"isMoving": @(tsLocation.isMoving),
//                @"location": [tsLocation toDictionary]
//        };
//        NSLog(@"*** event motionchange: %@, %@", tsLocation, params);
//    };
//    [bgGeo onMotionChange:callback];
//
//    void (^onHeartbeat)(TSHeartbeatEvent *) = ^void(TSHeartbeatEvent *event) {
//        NSLog(@"- onHeartbeat: %@", event);
//    };
//    if ([MFMailComposeViewController canSendMail]) {
//        [bgGeo emailLog:@"serendipityawaits@googlegroups.com" success:^{
//            NSLog(@" - Successfully sent device log to sunil1@gmail.com");
//        }       failure:^(NSString *error) {
//            NSLog(@" - Email log failure: %@", error);
//        }];
//    }
//    void (^onHttp)(TSHttpEvent *) =^void(TSHttpEvent *event) {
//        NSData *data = [event.responseText dataUsingEncoding:NSUTF8StringEncoding];
//        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        NSLog(@"%@", [json objectForKey:@"success"]);
//        if ([[json objectForKey:@"success"] boolValue] == 0 && [json objectForKey:@"success"] != nil) {
//            (APP_DELEGATE).lblLocationText = NSLocalizedString(@"lbl.location_not_available.txt", @"");
//            [APP_DELEGATE hideActivityIndicator];
//        } else {
//            NSDictionary *dictData = [json objectForKey:@"response"];
//            NSDictionary *dictLoc = dictData[@"location"];
//            if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyBroadcastLocation] boolValue] == YES) {
//                NSDate *now = [NSDate date];
//                NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//                NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
//                NSInteger hour = [dateComponents hour];
//                NSInteger minutes = [dateComponents minute];
//                if ([[server.loggedInUserInfo valueForKey:kKeySetting] objectForKey:kkeySmartBatteryUsage]) {
//                    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeySmartBatteryUsage] boolValue] == YES && hour < 6) {
//                        int differenceInHour = 6 - (int) hour;
//                        differenceInHour = (3600 * differenceInHour) - (60 * (int) minutes);
//                        [(APP_DELEGATE).locationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:differenceInHour];
//                    }
//                }
//            }
//            if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
//                //App is in foreground. Act on it.
//                updatedLocation = dictLoc[kKeyLiveAddress];
//                NSDictionary *locDict = @{kKeyLiveAddress: updatedLocation};
//                [server.loggedInUserInfo setValue:locDict forKey:kKeyLocation];
//                if ([updatedLocation isEqualToString:@""]) {
//                    (APP_DELEGATE).lblLocationText = NSLocalizedString(@"lbl.location_not_available.txt", @"");
//                }
//                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarLocationUpdated object:nil];
//            }
//        }
//        NSLog(@"- onHttp: %@", event);
//    };
//    [bgGeo onHttp:onHttp];
//    [bgGeo startSchedule];
//    [bgGeo onHeartbeat:onHeartbeat];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        //Start tracking.Calling #start here would normally be executed from a UI action rather than automatically being called here.
//        [bgGeo start];
//    });
}
// -----------------------------------------------------------------------------------------------
// displayUsersDotsOnRadar

- (void)displayUsersDotsOnRadar {
    // empty the existing dots from radar
    for (SRRadarDotView *dot in dotsArr) {
        [dot removeFromSuperview];
    }
    [dotsArr removeAllObjects];

    // This method should be called after successful return of JSON array from your server-side service
    [self renderUsersOnRadar:nearbyUsers];
}

// -----------------------------------------------------------------------------------------------
// renderUsersOnRadar:

- (void)renderUsersOnRadar:(NSArray *)users {
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
            server.myLocation.coordinate.longitude};

    // The last user in the nearbyUsers list is the farthest
    float maxDistance = 0;
    if (![[[users lastObject] valueForKey:kKeyDistance] isKindOfClass:[NSNull class]]) {
        [[[users lastObject] valueForKey:kKeyDistance] floatValue];
    }

    // Remove all objects from array
    [dotsUserPicsArr removeAllObjects];

    // Add users dots
    for (NSDictionary *user in users) {
        SRRadarDotView *dot = [[SRRadarDotView alloc] initWithFrame:CGRectMake(0, 0, 12.0, 12.0)];
        dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
        dot.userProfile = user;

        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:user];

        // If connected or family member show image with black dot
        if ([[updatedDict valueForKey:kKeyDegree] integerValue] == 1 || [[user valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-dot-30px.png"];
        } else {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];
        }
        NSDictionary *userLocDict = user[kKeyLocation];
        if ([userLocDict isKindOfClass:[NSDictionary class]]) {
            if (([userLocDict valueForKey:kKeyLattitude] && ![[userLocDict valueForKey:kKeyLattitude] isEqual:[NSNull null]]) && ([userLocDict valueForKey:kKeyRadarLong] && ![[userLocDict valueForKey:kKeyRadarLong] isEqual:[NSNull null]])) {
                CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};
                float bearing = [self getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
                dot.bearing = @(bearing);

                float d = [[user valueForKey:kKeyDistance] floatValue];

                float distance;
                //Here check with distance from center
                if (maxDistance != 0) {
                    if (37 + d >= 130) {
                        distance = 130;
                    } else {
                        distance = (37 + d);
                    }
                } else {
                    distance = (float) (double) 37;
                }
                dot.distance = @(distance);
                dot.initialDistance = @(distance); // relative distance
                dot.userDistance = @(d);
                dot.zoomEnabled = NO;
                dot.userInteractionEnabled = NO;

                float left = (float) (148 + distance * sin(degreesToRadians(-bearing)));
                float top = (float) (148 - distance * cos(degreesToRadians(-bearing)));

                [self rotateDot:dot fromBearing:currentDeviceBearing toBearing:bearing atDistance:distance];

                dot.frame = CGRectMake(left, top, 12.0, 12.0);
                dot.initialFrame = dot.frame;

                [radarCircleView addSubview:dot];
                [dotsArr addObject:dot];

                dot.transform = CGAffineTransformMakeRotation(-headingAngle);
            }
        }
    }
    // Start timer to detect collision with radar line and blink
    [detectCollisionTimer invalidate];
    detectCollisionTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                            target:self selector:@selector(detectCollisions1:) userInfo:nil repeats:YES];
}

#pragma mark
#pragma mark - Radar&Dot Collisions Method
#pragma mark

- (void)detectCollisions1:(NSTimer *)theTimer {
    float radarLineRotation = (float) radiandsToDegrees([[radarFocusView.layer.presentationLayer valueForKeyPath:@"transform.rotation.z"] floatValue]);

    if (radarLineRotation >= 0) {
        radarLineRotation += 90;
    } else {
        if (radarLineRotation > -90) {
            radarLineRotation = 90 + radarLineRotation;
        } else
            radarLineRotation = 270 + (radarLineRotation + 180);
    }

    for (int i = 0; i < [dotsArr count]; i++) {
        SRRadarDotView *dot = dotsArr[i];
        float dotBearing = [dot.bearing floatValue];
        if (dotBearing < 0) {
            dotBearing = -dotBearing;
        }
        dotBearing = dotBearing - currentDeviceBearing;
        if (dotBearing < 0) {
            dotBearing = dotBearing + 360;
        }

        // Now get diffrence between dotbearing and radarline
        float diffrence;
        if (dotBearing > radarLineRotation) {
            diffrence = dotBearing - radarLineRotation;
        } else
            diffrence = radarLineRotation - dotBearing;

        if (diffrence <= 5) {
            [self pulse:dot];
        }
    }
}

- (void)pulse:(SRRadarDotView *)dot {
    // Dot images
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.duration = 1.0;
    pulse.toValue = @1.0F;
    pulse.autoreverses = NO;
    dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    [dot.layer addAnimation:pulse forKey:@"pulse"];


    if (dot.userProfile[kKeyProfileImage] != nil && [dot.userProfile[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        CGRect frame = dot.initialFrame;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.frame = frame;

        frame = dot.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.imgView.frame = frame;

        dot.imgView.contentMode = UIViewContentModeScaleToFill;
        dot.imgView.layer.cornerRadius = dot.imgView.frame.size.width / 2.0;
        dot.imgView.clipsToBounds = YES;
        dot.isImageApplied = YES;

        NSDictionary *profileDict = dot.userProfile[kKeyProfileImage];
        NSString *imageName;
        if (profileDict[kKeyImageName] != [NSNull null]) {
            imageName = profileDict[kKeyImageName];
        }

        if (![imageName isKindOfClass:[NSNull class]]) {
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                [dot.imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else {
                dot.imgView.image = [UIImage imageNamed:@"profile_menu.png"];
            }
        }

        [self performSelector:@selector(removeImageSetDefault:) withObject:dot afterDelay:0.5];
    }
}

- (void)removeImageSetDefault:(SRRadarDotView *)inDotView {
    if (inDotView.isImageApplied) {
        // Remove the animation
        [inDotView.layer removeAllAnimations];

        CGRect frame = inDotView.initialFrame;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.frame = frame;

        frame = inDotView.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.imgView.frame = frame;

        inDotView.imgView.contentMode = UIViewContentModeScaleAspectFill;
        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:inDotView.userProfile];

        if ([[updatedDict valueForKey:kKeyDegree] integerValue] == 1 || [[updatedDict valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-dot-30px.png"];
        } else
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];

        inDotView.isImageApplied = NO;
        inDotView.imgView.clipsToBounds = NO;
    }
}

#pragma mark - Radar Dot Translate Methods

- (void)rotateDot:(SRRadarDotView *)dot fromBearing:(CGFloat)fromDegrees toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CGMutablePathRef path = CGPathCreateMutable();

    CGPathAddArc(path, nil, 150, 150, distance, degreesToRadians(fromDegrees), degreesToRadians(degrees), YES);

    CAKeyframeAnimation *theAnimation;

    // Animation object for the key path
    theAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    theAnimation.path = path;
    CGPathRelease(path);

    // Set the animation properties
    theAnimation.duration = 3;
    theAnimation.removedOnCompletion = NO;
    theAnimation.repeatCount = 0;
    theAnimation.autoreverses = NO;
    theAnimation.fillMode = kCAFillModeBackwards;
    theAnimation.cumulative = YES;

    CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);
    dot.layer.position = newPosition;
    [dot.layer addAnimation:theAnimation forKey:@"rotateDot"];
}

// -----------------------------------------------------------------------------------------------
// translateDot:

- (void)translateDot:(SRRadarDotView *)dot toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];

    [animation setFromValue:[NSValue valueWithCGPoint:[[dot.layer.presentationLayer valueForKey:@"position"] CGPointValue]]];

    //CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);

    float left = (float) (148 + distance * sin(degreesToRadians(-degrees)));
    float top = (float) (148 - distance * cos(degreesToRadians(-degrees)));
    CGPoint newPosition = CGPointMake(left, top);
    [animation setToValue:[NSValue valueWithCGPoint:newPosition]];

    [animation setDuration:0.3f];
    animation.fillMode = kCAFillModeBackwards;
    animation.autoreverses = NO;
    animation.repeatCount = 0;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];

    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [alphaAnimation setDuration:0.5f];
    alphaAnimation.fillMode = kCAFillModeBackwards;
    alphaAnimation.autoreverses = NO;
    alphaAnimation.repeatCount = 0;
    alphaAnimation.removedOnCompletion = NO;
    alphaAnimation.cumulative = YES;
    alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    /*if (distance > 130) {
	    [alphaAnimation setToValue:[NSNumber numberWithFloat:0.0f]];
     }
     else {
	    [alphaAnimation setToValue:[NSNumber numberWithFloat:1.0f]];
     }*/

    //[dot.layer addAnimation:alphaAnimation forKey:@"alphaDot"];
    [dot.layer addAnimation:animation forKey:@"translateDot"];
}

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRDiscoveryRadarViewController" bundle:nil server:inServer className:kClassRadar];

    if (self) {
        // Initialization
        server = inServer;
        dotsArr = [NSMutableArray array];
        dotsUserPicsArr = [NSMutableArray new];
        (APP_DELEGATE).loggedinUserID = server.loggedInUserInfo[kKeyId];
        userDataDict = [[NSMutableDictionary alloc] init];

        sourceArr = [[NSMutableArray alloc] init];
        oldSourceArr = [[NSMutableArray alloc] init];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailsSucceed:)
                              name:kKeyNotificationCompleteSignUPSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailsFailed:)
                              name:kKeyNotificationCompleteSignUPFailed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(EventnotifyInNotificationSucceed:)
                              name:kKeyNotificationEventInOutSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(EventnotifyOutNotificationFailed::)
                              name:kKeyNotificationEventInOutFailed
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(getRadarUsersSucceed:)
                              name:kKeyNotificationGetRadarUsersSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getRadarUsersFailed:)
                              name:kKeyNotificationGetRadarUsersFailed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(makeServerCallToGetUsersList)
                              name:kKeyNotificationUpdateRadarList
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateTimerSetting)
                              name:kKeyNotificationUpdateTimer
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(stopTimer)
                              name:kKeyNotificationStopUpdateTimer
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(setDisplayForRadar)
                              name:kKeyNotificationLocationInit
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(filterTheRadarList:)
                              name:kKeyNotificationFilterApplied
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(filterUserListOnMap:)
                              name:kKeyNotificationMapRefreshOnSlider
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(batteryUsageSliderUpdate:)
                              name:kKeyNotificationBatteryUsageUpdate
                            object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(locationUpdatedSucced:)
                              name:kKeyNotificationUpdateLocationSuccess
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(locationUpdatedFailed:)
                              name:kKeyNotificationUpdateLocationFailed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(StartmonitoringLocation)
                              name:kKeyNotificationUpdateLocationInBackground
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(StartmonitoringLocation)
                              name:kKeyNotificationrestatrmonitoringLocationInBackground
                            object:nil];
        //        Tracking In
        [defaultCenter addObserver:self
                          selector:@selector(notifyInTrackingNotificationSucceed:)
                              name:kKeyNotificationNotifyInTrackingNotificationSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(notifyInTrackingNotificationFailed:)
                              name:kKeyNotificationNotifyInTrackingNotificationFailed
                            object:nil];

        //        Tracking Out
        [defaultCenter addObserver:self selector:@selector(notifyOutTrackingNotificationSucceed:) name:kKeyNotificationNotifyOutTrackingNotificationSucceed object:nil];
        [defaultCenter addObserver:self selector:@selector(notifyOutTrackingNotificationFailed:) name:kKeyNotificationNotifyOutTrackingNotificationFailed object:nil];


        [defaultCenter addObserver:self
                          selector:@selector(blockConnectionSucceed:)
                              name:kKeyNotificationAddConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(blockConnectionFailed:)
                              name:kKeyNotificationAddConnectionFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(blockConnectionSucceed:)
                              name:kKeyNotificationUpdateConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(blockConnectionFailed:)
                              name:kKeyNotificationUpdateConnectionFailed object:nil];

    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    addTextInSignificantTxt(@"radar loaded");
    [super viewDidLoad];
    isTimerOn = NO;
    
    threshold = 10;
    
    _distanceQueue = [[NSOperationQueue alloc] init];
    _didEnterRegionArray = [[NSMutableArray alloc] init];

    _matrixRequestArray = [[NSMutableArray alloc] init];
    _matrixResponseArray = [[NSMutableArray alloc] init];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkRadarTutorial) name:@"locationTutorialDone" object:nil];


    if (![APP_DELEGATE isLocationTutorialDone]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [APP_DELEGATE showTutorial:self];
        });
    }
    //jonish sprint 2
//    else if (@available(iOS 13.0, *)){
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//           [APP_DELEGATE showTutorial:self];
//        });
//    }
    //jonish sprint 2
    else {
        [self checkRadarTutorial];
    }
    compassManager = [[CLLocationManager alloc] init];
    compassManager.delegate = self;
    
    self.btnIncreasedMiles.hidden = NO;
    self.btnReduceMiles.hidden = NO;
    self.milesSetminusImg.hidden = NO;
    self.milesSetPlusImg.hidden = NO;
    self.lblMiles.hidden = YES;

    // set Miles Data on Miles Label
    //Prepare Slider Data
    // customize slider 1
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
       // NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
       // distancevalue = [strArray[0] floatValue];
        distancevalue = kKeyMaximumRadarKm;
    }

    latestDistance = (float) distancevalue;
    settingDistance = (float) distancevalue;

    self.slider1.maximumValue = (float) distancevalue;
    self.slider1.minimumValue = kKeyMinimumRadarMile;
    self.slider1.value = (float) distancevalue;
    self.slider1.popUpViewColor = [UIColor colorWithRed:1 green:0.588 blue:0 alpha:1];


    // Prepare radar view
    radarCircleView = [[SRRadarCircleView alloc] initWithFrame:CGRectMake(0, 0, self.radarViewContainer.bounds.size.width, self.radarViewContainer.bounds.size.height)];
    radarCircleView.points = CGPointMake(self.radarProfileImgView.frame.origin.x, self.radarProfileImgView.frame.origin.y);
    radarCircleView.layer.contentsScale = [UIScreen mainScreen].scale;
    self.radarViewContainer.layer.contentsScale = [UIScreen mainScreen].scale;
//    [self.northLineView removeFromSuperview];
//    [radarCircleView addSubview:self.northLineView];
    [self.radarViewContainer addSubview:radarCircleView];

    radarFocusView = [[SRRadarFocusView alloc] initWithFrame:CGRectMake(18, 18, 265, 265)];
    radarFocusView.layer.contentsScale = [UIScreen mainScreen].scale;
    radarFocusView.alpha = 0.68;
    [self.radarViewContainer addSubview:radarFocusView];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDotTapped:)];
    [radarCircleView addGestureRecognizer:tapGestureRecognizer];

    // ScrollView
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;

    // Bring view to front
    [self.view bringSubviewToFront:self.radarProfileImgView];
    [self.view bringSubviewToFront:self.northLineView];

    // Set profile pic
    self.radarProfileImgView.layer.cornerRadius = self.radarProfileImgView.frame.size.width / 2;
    self.radarProfileImgView.layer.masksToBounds = YES;



    // Add View
    swipeContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];

    swipeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, self.view.frame.size.height)];
    swipeView.alpha = 0.9;
    swipeView.backgroundColor = [UIColor colorWithRed:115 / 255.0 green:70 / 255.0 blue:14 / 255.0 alpha:0.6];

    lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 120, 40)];
    lblDistance.font = [UIFont fontWithName:kFontHelveticaBold size:26.0];
    lblDistance.textColor = [UIColor whiteColor];

    lblNoPPl = [[UILabel alloc] initWithFrame:CGRectMake(0, lblDistance.frame.origin.y + 70, 140, 22)];
    lblNoPPl.textColor = [UIColor whiteColor];
    lblNoPPl.font = [UIFont fontWithName:kFontHelveticaMedium size:18.0];

    // Add the views to container view
    [swipeContainerView addSubview:swipeView];
    [swipeContainerView addSubview:lblDistance];
    [swipeContainerView addSubview:lblNoPPl];

    swipeContainerView.alpha = 0.7;
    swipeContainerView.hidden = YES;

    [self.view addSubview:swipeContainerView];

    // Pan gesture on progressContainerView
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    //[self.view addGestureRecognizer:panGesture];
    [self.view addGestureRecognizer:panGesture];

    //Add Tap Gesture on dotView
    UITapGestureRecognizer *viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapAction:)];
    self.view.userInteractionEnabled = YES;
    viewTapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:viewTapGesture];

    // Add long press guesture on + button
    UILongPressGestureRecognizer *longPressPlus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressTap:)];
    longPressPlus.minimumPressDuration = 1.0f;
    self.btnIncreasedMiles.tag = 1;
    [self.btnIncreasedMiles addGestureRecognizer:longPressPlus];

    // Add long press guesture on - button
    UILongPressGestureRecognizer *longPressMinus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressTap:)];
    longPressMinus.minimumPressDuration = 1.0f;
    self.btnReduceMiles.tag = 2;
    [self.btnReduceMiles addGestureRecognizer:longPressMinus];

    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [_radarRefreshOverlay setHidden:YES];
    // Add long press guesture to refresh radar centered ProfileImgView
    UILongPressGestureRecognizer *radarLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressRadar:)];
    radarLongPress.minimumPressDuration = 1.0f;
    [self.radarProfileImgView addGestureRecognizer:radarLongPress];
    [self.extraCircleView bringSubviewToFront:self.radarViewContainer];
    [self.radarViewContainer bringSubviewToFront:self.radarProfileImgView];
  //  [self.radarViewContainer addGestureRecognizer:radarLongPress];

    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(updateSettings) name:
            UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(entersBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];

    _isGetUsersListCallInProgress = NO;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kKeyRadarToolTipFlag] boolValue] != 0 || (APP_DELEGATE).isSignUpSuccess == NO) {
        [self setDisplayForRadar];
    }
    if (nearbyUsers.count > 0) {
        self.activityIndicator.hidden = YES;
    }
    
    [self getDelayTime];
    [self setmileslableData];
}
// -----------------------------------------------------------------------------------------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.tabBarController.tabBar.hidden = NO;
//    (APP_DELEGATE).topBarView.hidden = NO;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
        self.slider1.maximumValue = kKeyMaximumRadarMile;
    } else {
        distanceUnit = kkeyUnitKilometers;
        self.slider1.maximumValue = kKeyMaximumRadarKm;
    }
    if ((APP_DELEGATE).isMeasurementChangesForRadar == true) {
        if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
            latestDistance = kKeyMaximumRadarMile;
        } else {
            latestDistance = kKeyMaximumRadarKm;
        }
        [self.slider1 setValue:latestDistance];
        (APP_DELEGATE).isMeasurementChangesForRadar = false;
    }
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld people in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
    _oldLocation = server.myLocation;
    [compassManager startUpdatingHeading];
    
    if ([[SLocationService sharedInstance] server] == nil){
        [self setDisplayForRadar];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:NO];

    if ((APP_DELEGATE).isReqInProcess == false) {
        (APP_DELEGATE).isTabClicked = YES;
        (APP_DELEGATE).timer = NO;
        (APP_DELEGATE).isOnMap = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateTimer object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
        (APP_DELEGATE).timer = YES;
    }
    (APP_DELEGATE).isBackMenu = NO;
    [self loadNotificationController];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.scrollView setHidden:YES];
    [self.toolTipScrollViewImg setHidden:YES];
    //[compassManager stopUpdatingHeading];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationStopUpdateTimer object:nil];
}

- (void)checkRadarTutorial {
    // Show tooltip view
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kKeyRadarToolTipFlag] boolValue] == 0 || (APP_DELEGATE).isSignUpSuccess)
    {
        if ((APP_DELEGATE).isSignUpSuccess) {
            (APP_DELEGATE).isSignUpSuccess = NO;
        }
        (APP_DELEGATE).tooltipRadarViewFlag = YES;
        [APP_DELEGATE toolTipFlagSaveData];
        // Show tooltip

        NSArray *titleArray = @[NSLocalizedString(@"lbl.radar_tooltip.txt", @""),
                                NSLocalizedString(@"lbl.radar_tooltip1.txt", @""),
                                NSLocalizedString(@"lbl.radar_tooltip2.txt", @""),
                                NSLocalizedString(@"lbl.radar_tooltip3.txt", @""),
                                NSLocalizedString(@"lbl.radar_update.txt", @""),
                                NSLocalizedString(@"", @"")];

        NSArray *imgArray = @[@"Radar",
                              @"Change-Filters-top",
                              @"Change-Filters-bottom_sd",
                              @"Change-Radar-bottom",
                              @"Radar_sd",
                              @"Under-Menu_sd"];

        SRPageControlViewController *pageViewController = [[SRPageControlViewController alloc] initWithNibName:@"SRPageControlViewController" bundle:nil];
          pageViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        pageViewController.imageArr = imgArray;
        pageViewController.titleArr = titleArray;
        pageViewController.showTooltipOnView = @"SRDiscoveryRadarViewController";
        [self presentViewController:pageViewController animated:NO completion:nil];
    }
    
    if ([[SLocationService sharedInstance] server] == nil){
        [self setDisplayForRadar];
    }
}

- (void)loadNotificationController {
    if ((APP_DELEGATE).isFromNotification) {
        if ((APP_DELEGATE).notificationType == 1) {
            SRMyConnectionsViewController *objMap = [[SRMyConnectionsViewController alloc] initWithNibName:nil bundle:nil server:server];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objMap animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else if ((APP_DELEGATE).notificationType == 3) {
            SRGroupTabViewController *objGroupTab = [[SRGroupTabViewController alloc] initWithNibName:nil bundle:nil];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objGroupTab animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else if ((APP_DELEGATE).notificationType == 4) {
            SREventTabViewController *objGroupTab = [[SREventTabViewController alloc] initWithNibName:nil bundle:nil];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objGroupTab animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else if ((APP_DELEGATE).notificationType == 5 || (APP_DELEGATE).notificationType == 8) {
            SRTrackingViewController *objMap = [[SRTrackingViewController alloc] initWithNibName:nil bundle:nil profileData:(APP_DELEGATE).dictUserInfo server:server];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objMap animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else if ((APP_DELEGATE).notificationType == 9) {
            SRDropPinViewController *objMap = [[SRDropPinViewController alloc] initWithNibName:nil bundle:nil server:server];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objMap animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        } else if ((APP_DELEGATE).notificationType == 10) {
            SRPingsViewController *objChat = [[SRPingsViewController alloc] initWithNibName:nil bundle:nil server:server];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:objChat animated:YES];
            self.hidesBottomBarWhenPushed = YES;
        }
    }
}

- (void)StartmonitoringLocation {
    addTextInSignificantTxt(@"StartmonitoringLocation called");
    if ((APP_DELEGATE).locationManager) {
        addTextInSignificantTxt(@"(APP_DELEGATE).locationManager allocated already and now starting MonitoringLocationChanges");
        (APP_DELEGATE).locationManager.delegate = self;
        (APP_DELEGATE).locationManager.distanceFilter = 20;
        if ([APP_DELEGATE isLocationTutorialDone]) {
            [(APP_DELEGATE).locationManager requestAlwaysAuthorization];
            [(APP_DELEGATE).locationManager startUpdatingLocation];
        }
        (APP_DELEGATE).locationManager.pausesLocationUpdatesAutomatically = NO;

        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
            [(APP_DELEGATE).locationManager setAllowsBackgroundLocationUpdates:YES];
        }

        addTextInSignificantTxt(@"Start Updating location called");
    } else {
        addTextInSignificantTxt(@"(APP_DELEGATE).locationManager allocating and starting MonitorLocationChanges");
        (APP_DELEGATE).locationManager = [[CLLocationManager alloc] init];
        (APP_DELEGATE).locationManager.delegate = self;
        //Start location services to get the true heading.
        (APP_DELEGATE).locationManager.distanceFilter = 20;
        (APP_DELEGATE).locationManager.pausesLocationUpdatesAutomatically = NO;

        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0")) {
            if ([APP_DELEGATE isLocationTutorialDone]) {
                [(APP_DELEGATE).locationManager setAllowsBackgroundLocationUpdates:YES];
            }
        }
        if ([APP_DELEGATE isLocationTutorialDone]) {
            [(APP_DELEGATE).locationManager requestAlwaysAuthorization];
            [(APP_DELEGATE).locationManager startUpdatingLocation];
            [(APP_DELEGATE).locationManager startMonitoringSignificantLocationChanges];
        }
    }
}

- (void)entersBackground {
    addTextInSignificantTxt(@"Application goes in the background");
    [detectCollisionTimer invalidate];
    [self.updateTimer invalidate];

    detectCollisionTimer = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSettings) name:
            UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
}

- (void)updateSettings {

    [detectCollisionTimer invalidate];
    detectCollisionTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self
                                                          selector:@selector(detectCollisions1:) userInfo:nil repeats:YES];

    [self.updateTimer invalidate];

    int delayTime = 10;

    if (delayTime > 0) {
        isTimerOn = YES;
        self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:delayTime target:self
                                                          selector:@selector(makeServerCallToGetUsersList) userInfo:nil repeats:YES];
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(entersBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];
}

#pragma mark - Slider update

- (void)sliderWillDisplayPopUpView:(ASValueTrackingSlider *)slider; {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        // NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        // distancevalue = [strArray[0] floatValue];
        distancevalue = kKeyMaximumRadarKm;
    }

    if (slider.value >= distancevalue) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.max_Distance.text", @"")];
    } else {
        latestDistance = slider.value;
        [self calculateAndGetNewUsers];
    }

}

#pragma mark - IBAction Method

- (IBAction)toolTipGotItBtnAction:(id)sender {
    for (UIView *overlay in self.tabBarController.view.subviews) {
        [APP_DELEGATE hideActivityIndicator];
        if (overlay.tag == 1000000) {
            [overlay removeFromSuperview];
        }
    }
}

- (void)refreshRadarBtnClick {
    [_radarRefreshOverlay setHidden:YES];
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            //do your code
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
    [APP_DELEGATE showActivityIndicator];
    if (isTimerOn == YES) {
        isTimerOn = NO;
        [self makeServerCallToGetUsersList];
        isTimerOn = YES;
    } else {
        [self makeServerCallToGetUsersList];
    }
}

- (void)setmileslableData {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    [userdef setValue:[userdef valueForKey:kKeyMiles_Data] forKey:kKeyTempMiles_Data];
    CGFloat distancevalue = 0.0;
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        // NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        // distancevalue = [strArray[0] floatValue];
        distancevalue = kKeyMaximumRadarKm;
    }

    self.slider1.maximumValue = (float) distancevalue;
    self.slider1.value = self.slider1.value;
    latestDistance = (float) distancevalue;

    settingDistance = (float) distancevalue;

    NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
    NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

    NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
    NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:@"miles" attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
    [muAtrStr appendAttributedString:atrStr1];
    [muAtrStr appendAttributedString:atrStr2];

    [self.lblMiles setAttributedText:muAtrStr];
    self.lblMiles.layer.cornerRadius = self.lblMiles.frame.size.width / 2;
    self.lblMiles.layer.masksToBounds = YES;
}

- (IBAction)actionOnButtonClick:(id)sender {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    NSString *strMilesTempData = [userdef valueForKey:kKeyTempMiles_Data];

    if (sender == self.btnIncreasedMiles) {
        if (strMilesData == nil) {
            strMilesData = [NSString stringWithFormat:@"%f", kKeyMaximumRadarMile];
        }
        if (latestDistance >= [strMilesData floatValue] && [strMilesTempData integerValue] >= [strMilesData integerValue]) {
            [SRModalClass showAlert:NSLocalizedString(@"alert.max_Distance.text", @"")];
        } else {
            latestDistance += 30;
            if (latestDistance >= kKeyMaximumRadarMile) {
                latestDistance = kKeyMaximumRadarMile;
            }
            sliderX = (int) latestDistance;
            [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
            [userdef synchronize];
            [self calculateAndGetNewUsers];
            swipeContainerView.hidden = NO;
        }
    } else {
        if (latestDistance <= kKeyMinimumRadarMile) {
            [SRModalClass showAlert:NSLocalizedString(@"alert.min_Distance.text", @"")];
        } else {
            latestDistance -= 30;
            if (latestDistance <= 30) {
                latestDistance = kKeyMinimumRadarMile;
                [self.slider1 setValue:0.0 animated:YES];
            }
            sliderX = latestDistance;
            [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
            [userdef synchronize];
            // Call method
            [self calculateAndGetNewUsers];
            swipeContainerView.hidden = NO;
        }
    }
    [UIView animateWithDuration:0.0 delay:0.9 options:UIViewAnimationOptionCurveEaseOut animations:^{
                swipeContainerView.hidden = NO;
                sliderX = (int) (((self.slider1.value * self.slider1.frame.size.width) / self.slider1.maximumValue) + self.slider1.frame.origin.x);
                if (sliderX > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                    swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                } else {
                    swipeView.frame = CGRectMake(0, 0, sliderX, self.view.frame.size.height);
                }

                if (sliderX >= (SCREEN_WIDTH / 2)) {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
                } else {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
                }
                [self.view bringSubviewToFront:self.progressContainerView];
            }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(hideSwipeView) withObject:self afterDelay:0.4];
                     }];
}

- (void)hideSwipeView {
    swipeContainerView.hidden = YES;
}

- (void)calculateAndGetNewUsers {

    if (latestDistance > -1) {

        // Set value
        NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
        NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

        NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

        NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
        NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
        NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:distanceUnit attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:12]}];
        [muAtrStr appendAttributedString:atrStr1];
        [muAtrStr appendAttributedString:atrStr2];

        [self.lblMiles setAttributedText:muAtrStr];
        [lblDistance setAttributedText:muAtrStr];

        NSMutableArray *filteredArr = [[NSMutableArray alloc] init];
        [filteredArr removeAllObjects];

        if (filterDictionary.count > 0) {
            NSMutableArray *filteredTrackUsers = [[NSMutableArray alloc] init];
            if (filterDictionary[kKeyGroupTag]) {
                if ([filterDictionary[kKeyGroupType] isEqualToString:@"Selected Connections"]) {

                    NSUInteger userId = [filterDictionary[kKeyGroupTag] integerValue];
                    [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue] && ([[obj valueForKey:@"id"] integerValue] == userId)) {
                            [filteredTrackUsers addObject:obj];
                        }
                    }];
                    nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
                    [self displayUsersDotsOnRadar];

                    // Post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];

                } else if ([filterDictionary[kKeyGroupTag] integerValue] == 2) {
                    [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                            if ([[obj valueForKey:@"is_active_connection"] boolValue]) {
                                [filteredTrackUsers addObject:obj];
                            }
//                            [filteredTrackUsers addObject:obj];
                        }
                    }];
                    nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
                    [self displayUsersDotsOnRadar];

                    // Post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
                } else if (![filterDictionary[kKeyGroupTag] boolValue]) {
                    [filteredArr removeAllObjects];
                    if ([filterDictionary[kKeyGroupType] isEqualToString:@"All Connections"]) {

                        [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                            if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                                [filteredTrackUsers addObject:obj];
                            }
                        }];

                        nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
                        [self displayUsersDotsOnRadar];

                        // Post notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
                    }
                } else if ([filterDictionary[kKeyGroupType] isEqualToString:@"Family"]) {
                    [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && [[obj objectForKey:kKeyFamily] isKindOfClass:[NSDictionary class]] && [[obj objectForKey:@"is_tracking"] boolValue]) {
                            [filteredTrackUsers addObject:obj];
                        }
                    }];

                    nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
                    [self displayUsersDotsOnRadar];

                    // Post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
                }
            } else if ([grpNotifiArr count] > 0) {
                [filteredArr removeAllObjects];
                [filteredArr addObjectsFromArray:grpNotifiArr];
                nearbyUsers = [NSArray arrayWithArray:filteredArr];
                [self displayUsersDotsOnRadar];
                // Post notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
            } else {
                //group_notification
                if (latestDistance <= settingDistance) {

                    for (NSDictionary *dict in sourceArr) {
                        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
                        if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && [updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyIsFamilyMember] integerValue] == 1) {
                            [filteredArr addObject:dict];
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 2) {
                            [filteredArr addObject:dict];
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 3) {
                            //For All
                            [filteredArr addObject:dict];
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 4) {
                            //For Active
                            if ([[updatedDict valueForKey:@"is_active_connection"] boolValue]) {
                                [filteredArr addObject:dict];
                            }
                        } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 5) {
                            //For favourite
                            if ([[updatedDict valueForKey:kKeyIsFavourite] boolValue]) {
                                [filteredArr addObject:dict];
                            }
                        } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree <= 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {

                            [filteredArr addObject:dict];
                        } else if ((APP_DELEGATE).topBarView.selectedDegree == -1 && (APP_DELEGATE).topBarView.btnCommunity.selected) {
                            filterDictionary = nil;
                            [filteredArr addObject:dict];
                        } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && (APP_DELEGATE).topBarView.selectedDegree == 6 && ![dict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
                            [filteredArr addObject:dict];
                        }

                    }

                    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
                    [filteredArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance) {
                            [tempArr addObject:obj];
                        }
                    }];

                    nearbyUsers = [NSArray arrayWithArray:tempArr];
                    // Post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
                }
            }
        } else {
            for (NSDictionary *dict in sourceArr) {
                NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];

                if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && (APP_DELEGATE).topBarView.btnConnection.selected && ([(APP_DELEGATE).topBarView.lblConnections.text isEqualToString:NSLocalizedString(@"lbl.family.txt", @"")])) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && (APP_DELEGATE).topBarView.btnConnection.selected && ([(APP_DELEGATE).topBarView.lblConnections.text isEqualToString:NSLocalizedString(@"lbl.Connections.txt", @"")])) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && (APP_DELEGATE).topBarView.btnConnection.selected && ([(APP_DELEGATE).topBarView.lblConnections.text isEqualToString:NSLocalizedString(@"lbl.Favourites.txt", @"")])) {
                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree <= 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {
                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected) {

                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.btnGroup.selected) {

                    if ([grpNotifiArr count] > 0) {
                        [filteredArr removeAllObjects];
                        [filteredArr addObjectsFromArray:grpNotifiArr];

                    }
                } else {
                    [filteredArr removeAllObjects];
                    [filteredArr addObjectsFromArray:sourceArr];
                }
            }

            NSMutableArray *tempArr = [[NSMutableArray alloc] init];
            [filteredArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance) {
                    [tempArr addObject:obj];
                }
            }];

            nearbyUsers = [NSArray arrayWithArray:tempArr];
            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
        }
        self.lblInPpl.text = [NSString stringWithFormat:@"%ld people in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
        lblNoPPl.text = [NSString stringWithFormat:@"%ld people in", (unsigned long) nearbyUsers.count];
        self.slider1.value = latestDistance;
        if (nearbyUsers.count > 0) {
            self.activityIndicator.hidden = YES;
        } else {
            self.activityIndicator.hidden = YES;
        }

        // Filter users
        [self filterUsers];
    }
}

- (void)filterUsers {
    for (id d in dotsArr) {
        SRRadarDotView *dot = (SRRadarDotView *) d;
        NSDictionary *userProfile = dot.userProfile;

        if ([nearbyUsers containsObject:userProfile]) {
            if (settingDistance != latestDistance) {
                float maxDistance = settingDistance;
                float dotDistance = [dot.initialDistance floatValue];

                if (maxDistance != latestDistance) {
                    float modifiedUserDistance = (maxDistance * dotDistance) / latestDistance;
                    if (modifiedUserDistance > 130) {
                        modifiedUserDistance = 130;
                    } else if (modifiedUserDistance < 37) {
                        modifiedUserDistance = 37;
                    }
                    dot.distance = @(modifiedUserDistance);
                }
            } else {
                // The last user in the nearbyUsers list is the farthest
                float maxDistance = [[[nearbyUsers lastObject] valueForKey:kKeyDistance] floatValue];

                float d = [[userProfile valueForKey:kKeyDistance] floatValue];

                float distance;

                if (maxDistance != 0) {
                    if (37 + d >= 130) {
                        distance = 130;
                    } else {
                        distance = (37 + d);
                    }
                } else {
                    distance = (double) 37;
                }

                // Distance
                dot.distance = @(distance);
            }

            float degrees = [dot.bearing floatValue];
            float left = (float) (148 + [dot.distance floatValue] * sin(degreesToRadians(-degrees)));
            float top = (float) (148 - [dot.distance floatValue] * cos(degreesToRadians(-degrees)));

            dot.hidden = NO;
            [self translateDot:dot toBearing:[dot.bearing floatValue] atDistance:[dot.distance floatValue]];
            dot.frame = CGRectMake(left, top, 14, 14);
            dot.initialFrame = dot.frame;
            dot.imgView.frame = CGRectMake(0, 0, 14, 14);
        } else {
            dot.hidden = YES;
        }
    }
}

// -------------------------------------------------------------------------------------------------
- (void)addViewsOnScrollView:(NSArray *)inArr {
    // Remove dropdown view if present
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;

    CGRect frame;
    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }

    for (NSUInteger i = 0; i < [inArr count]; i++) {
        NSDictionary *userDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i == 0) {
            frame = CGRectMake(0, 11, 130, 200);
        } else {
            frame = CGRectMake(frame.origin.x + 126, 11, 130, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;

        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(8.0, 8.0)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = ((CalloutAnnotationView *) annotationView).userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        ((CalloutAnnotationView *) annotationView).userProfileImage.layer.mask = maskLayer;

//        if (userDict[kKeyProfileImage] != nil && [userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
//            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
//                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
//                if (![imageName isKindOfClass:[NSNull class]]) {
//                    if ([imageName length] > 0) {
//                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
//
//                        [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
//                    } else
//                        ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
//                }
//            } else {
//                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
//                ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
//            }
//        } else {
//            ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
//            ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
//        }
        
        if ([userDict[kKeySetting][@"profile_photo"]intValue] == 5 ) {
           ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else if ([userDict[kKeySetting][@"profile_photo"]intValue] == 4 ) {
           ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            if ([userDict[kKeyIsFavourite]intValue] == 1 ) {
                
                NSString* strProfileURL = [self getProfileImageURL:userDict];
                if ([strProfileURL length] > 0){
                    [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
                }else{
                    ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
            }
        } else {
            NSString* strProfileURL = [self getProfileImageURL:userDict];
            if ([strProfileURL length] > 0){
                [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:strProfileURL]];
            }else{
                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            }
        }
        //Favourite
        if ([userDict[kKeyIsFavourite] boolValue] == true) {
            annotationView.favUserImage.hidden = FALSE;
        } else {
            annotationView.favUserImage.hidden = TRUE;
        }
        //Invisible
        //Set image shadow
        [annotationView.invisibleUserImage.layer setShadowOffset:CGSizeMake(5, 5)];
        [annotationView.invisibleUserImage.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [annotationView.invisibleUserImage.layer setShadowOpacity:7.5];

        if ([userDict[kKeyIsInvisible] boolValue] == true) {
            annotationView.invisibleUserImage.hidden = FALSE;
        } else {
            annotationView.invisibleUserImage.hidden = TRUE;
        }

        annotationView.lblProfileName.text = [userDict valueForKey:kKeyFirstName];
        if ([[userDict valueForKey:kKeyOccupation] length] <= 0) {
            annotationView.lblOccupation.text = @"None";
        } else {
            annotationView.lblOccupation.text = userDict[kKeyOccupation];
        }

        // Location
        float distance = [userDict[kKeyDistance] floatValue];

        annotationView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@", distance, distanceUnit];

        if ([[userDict valueForKey:kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            annotationView.lblDegree.text = [NSString stringWithFormat:@" %ld°", (long) degree];
        } else {
            annotationView.lblDegree.text = @" 6°";
        }

        // Add Shadow to btnPing
        [annotationView.pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [annotationView.pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [annotationView.pingButton.layer setShadowOpacity:8.5];
        [annotationView.pingButton addTarget:self action:@selector(pingButtonAction:) forControlEvents:UIControlEventTouchUpInside];


        //Add Compass Button
        [annotationView.compassButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [annotationView.compassButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [annotationView.compassButton.layer setShadowOpacity:8.5];

        CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                server.myLocation.coordinate.longitude};

        NSDictionary *userLocDict = userDict[kKeyLocation];
        CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};

        [annotationView.compassButton addTarget:self action:@selector(compassButtonAction:) forControlEvents:UIControlEventTouchUpInside];


        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            annotationView.lblCompassDirection.text = @"N";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            annotationView.lblCompassDirection.text = @"E";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            annotationView.lblCompassDirection.text = @"S";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            annotationView.lblCompassDirection.text = @"W";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            annotationView.lblCompassDirection.text = @"NE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            annotationView.lblCompassDirection.text = @"NW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            annotationView.lblCompassDirection.text = @"SE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            annotationView.lblCompassDirection.text = @"SW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }


        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [annotationView.userProfileImage addGestureRecognizer:annotationTapGesture];
        annotationView.userDict = userDict;


        //if users count is one
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }


        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else
            self.scrollView.scrollEnabled = NO;

        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
        [self.view bringSubviewToFront:self.scrollView];
    }
}

-(NSString*) getProfileImageURL:(NSDictionary*) userDict{
    
    if  ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary self]])  {
        NSDictionary *profileDict = userDict[kKeyProfileImage];
        if (profileDict[kKeyImageName] != nil){
            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                
                return imageUrl;
            }
        }
    }
    
    return @"";
}

-(int)distanceStep:(int )target currentValue:(float)current {
    NSArray *targets = @[@5, @15, @25, @50, @100, @500, @1000, @1500, @2000, @2500, @5000, @7500, @10000, @12500];
    
    if (current == 0) {
        return 0;
    }
    BOOL isKm = NO;
    
    float stepValue = kKeyMaximumRadarMile / targets.count;
    if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
        targets = @[@5, @15, @25, @50, @100, @500, @1000, @1500, @2000, @2500, @5000, @7500, @10000, @12500, @15000, @17500, @20000];
        stepValue = kKeyMaximumRadarKm / targets.count;
        isKm = YES;
    }
    
    int index = (int)round(current/stepValue) - 1;
    int mIndex = (int)round(current/(kKeyMaximumRadarMile / targets.count)) - 1;
//    NSLog(@"\n\n\n");
//    NSLog(@"Step value Km --- %f", kKeyMaximumRadarKm / targets.count);
//    NSLog(@"Step value M --- %f", kKeyMaximumRadarMile / targets.count);
//    NSLog(@"Index Km --- %d of %d", index, (int)targets.count);
//    NSLog(@"Index M --- %d of %d", mIndex, (int)targets.count);
//    NSLog(@"Is Km --- %@", isKm ? @"YES":@"NO");
//    NSLog(@"Current --- %f\n\n", current);
    
    if (round(current/stepValue) == 0.000000) {
        return 0;
    }
    if (index >= targets.count) {
        index = (int)targets.count - 1;
    }
    return [targets[index] intValue];
}

#pragma mark
#pragma mark - Gesture Method
#pragma mark

// --------------------------------------------------------------------------------
// handlePan:
- (void)handlePan:(UIPanGestureRecognizer *)gesture {
    self.activityIndicator.hidden = NO;
    // Transform the view by the amount of the x translation
    // Calculate how far the user has dragged across the view
    UIView *selectedView = gesture.view;
    CGPoint center = selectedView.center;
    int xPos = [gesture locationInView:self.slider1].x;
    float distance = sqrt(pow((center.x - xPos), 2.0) + pow((center.y - 0), 2.0));
    
    
    
    CGFloat progress = [gesture locationInView:self.slider1].x / (self.slider1.bounds.size.width - 5);
    progress = MIN(1.0, MAX(0.0, progress));
   // NSLog(@"UISlider value %f", [gesture locationInView:self.slider1].x);
    NSInteger translateX = [gesture locationInView:self.slider1].x;
    
    CGPoint translate = [gesture locationInView:gesture.view];
    if (translateX < 0) {
        translateX = 0;
    }

     //translationInView:self.view];
    CGPoint translationInView = [gesture translationInView:self.slider1];
    swipeContainerView.hidden = NO;

    NSInteger translateViewX = [gesture locationInView:self.slider1].x;
    if (translateX < 0) {
        translateX = 0;
    }
    
    // -________________________
    
    if (gesture.state == UIGestureRecognizerStateCancelled ||
            gesture.state == UIGestureRecognizerStateFailed ||
            gesture.state == UIGestureRecognizerStateEnded) {
        // When Pan ends
        swipeContainerView.hidden = YES;
        self.progressContainerView.hidden = NO;
        isSwipedOnce = NO;
        swipeView.frame = CGRectMake(0, 0, 0, self.view.frame.size.height);
        lblDistance.frame = CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 100, 30);
        lblNoPPl.frame = CGRectMake(0, lblDistance.frame.origin.y + 18, 100, 30);

        if (gesture.state == UIGestureRecognizerStateEnded) {
            if (progress < 1) {
                latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;
                
                if (latestDistance <= kKeyMinimumRadarMile) {
                    latestDistance = kKeyMinimumRadarMile;
                }
                latestDistance = [self distanceStep:5000 currentValue:(int)latestDistance];
                
                
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                   if (latestDistance >= kKeyMaximumRadarMile) {
                       latestDistance = kKeyMaximumRadarMile;
                       _slider1.value = latestDistance;
                   }
                } else {
                   if (latestDistance >= kKeyMaximumRadarKm) {
                       latestDistance = kKeyMaximumRadarKm;
                       _slider1.value = latestDistance;
                   }
                }
                
                [self calculateAndGetNewUsers];
                //self.slider1.value= (translate.x * self.slider1.maximumValue)/SCREEN_WIDTH;
            } else {
              //  latestDistance = settingDistance;
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                        _slider1.value = latestDistance;
                    }
                } else {
                    if (latestDistance >= kKeyMaximumRadarKm) {
                        latestDistance = kKeyMaximumRadarKm;
                        _slider1.value = latestDistance;
                    }
                }
                // Show people
                [self calculateAndGetNewUsers];
            }


            sliderX = translate.x;
            if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
            } else
                swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);

            if (translate.x >= (SCREEN_WIDTH / 2)) {
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
            } else {
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
            }

        }
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        self.progressContainerView.hidden = YES;
        if (translationInView.x > 0) {
            
            // While changing state of slider to increasing value of slider
            [UIView animateWithDuration:3.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                             }
                             completion:^(BOOL finished) {
                                 if (translate.x <= self.slider1.frame.origin.x) {
                                     swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);

                                     lblDistance.frame = CGRectMake(self.slider1.frame.origin.x + 20, lblDistance.frame.origin.y, 120, 40);
                                     lblNoPPl.frame = CGRectMake(self.slider1.frame.origin.x + 20, lblDistance.frame.origin.y + 40, 140, 22);

                                 } else if (translate.x <= self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                                     if (progress < 1) {

                                         latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;

                                         if (latestDistance <= kKeyMinimumRadarMile) {
                                             latestDistance = kKeyMinimumRadarMile;
                                         }
                                         
                                         
                                         latestDistance = [self distanceStep:5000 currentValue:(int)latestDistance];
                                         
                                         if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                                             if (latestDistance >= kKeyMaximumRadarMile) {
                                                 latestDistance = kKeyMaximumRadarMile;
                                             }
                                         } else {
                                             if (latestDistance >= kKeyMaximumRadarKm) {
                                                 latestDistance = kKeyMaximumRadarKm;
                                                 _slider1.value = latestDistance;
                                             }
                                         }
                                         // Show people
                                         [self calculateAndGetNewUsers];
                                     } else {
                                         //latestDistance = settingDistance;
                                         if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                                             if (latestDistance >= kKeyMaximumRadarMile) {
                                                 latestDistance = kKeyMaximumRadarMile;
                                                 _slider1.value = latestDistance;
                                             }
                                         } else {
                                             if (latestDistance >= kKeyMaximumRadarKm) {
                                                 latestDistance = kKeyMaximumRadarKm;
                                                 _slider1.value = latestDistance;
                                             }
                                         }
                                         // Show people
                                         [self calculateAndGetNewUsers];
                                     }

                                     sliderX = translate.x;
                                     if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                                         swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                                     } else
                                         swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);
                                     if (translate.x >= (SCREEN_WIDTH / 2)) {
                                         lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                                         lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
                                     } else {
                                         lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                                         lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
                                     }
                                 }
                             }];
        } else {
            // While changing state of slider to decreasing value of slider
            if (progress < 1) {
                
                latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;
                
                if (latestDistance <= kKeyMinimumRadarMile) {
                    latestDistance = kKeyMinimumRadarMile;
                    _slider1.value = latestDistance;
                }
                
                latestDistance = [self distanceStep:5000 currentValue:(int)latestDistance];
                
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                        _slider1.value = latestDistance;
                    }
                } else {
                    if (latestDistance >= kKeyMaximumRadarKm) {
                        latestDistance = kKeyMaximumRadarKm;
                        _slider1.value = latestDistance;
                    }
                }
                
                
                // Show people
                [self calculateAndGetNewUsers];
            } else {
               // latestDistance = settingDistance;
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                        _slider1.value = latestDistance;
                    }
                } else {
                    if (latestDistance >= kKeyMaximumRadarKm) {
                        latestDistance = kKeyMaximumRadarKm;
                        _slider1.value = latestDistance;
                    }
                }
                // Show people
                [self calculateAndGetNewUsers];
            }
            // Set swiping view
            if (translate.x < self.slider1.frame.origin.x) {
                swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
            } else {
                if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                    swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                } else
                    swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);
                if (translate.x >= SCREEN_WIDTH / 2) {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
                } else {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
                }
            }
        }
    }
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        if (latestDistance >= kKeyMaximumRadarMile) {
            latestDistance = kKeyMaximumRadarMile;
            _slider1.value = latestDistance;
        }
    } else {
        if (latestDistance >= kKeyMaximumRadarKm) {
            latestDistance = kKeyMaximumRadarKm;
            _slider1.value = latestDistance;
        }
    }
}

//----------------------------------------------------------------
- (void)tapOnRefreshOverlay:(UITapGestureRecognizer *)recognizer {
    //Hide radar refresh overlay
    [_radarRefreshOverlay setHidden:YES];
    for (id subview in self.tabBarController.view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *refreshButton = subview;
            refreshButton.hidden = YES;
        }
    }
}

//----------------------------------------------------------------
- (void)longPressTap:(UILongPressGestureRecognizer *)recognizer {

    if (recognizer.state == UIGestureRecognizerStateEnded) {
        swipeContainerView.hidden = YES;
        return;
    }
    if (recognizer.view.tag == 1) {
        // Long press detected, start the timer
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
        CGFloat distancevalue = 0.0;
        if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
            distancevalue = kKeyMaximumRadarMile;
        } else {
            // NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
            // distancevalue = [strArray[0] floatValue];
            distancevalue = kKeyMaximumRadarKm;
        }
        if (![[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
            distancevalue = kKeyMaximumRadarMile;
        }
        self.slider1.maximumValue = distancevalue;
        latestDistance = distancevalue;
        [self.slider1 setValue:distancevalue animated:YES];

        [userdef setValue:[NSString stringWithFormat:@"%f", distancevalue] forKey:kKeyTempMiles_Data];
        [userdef synchronize];


        swipeContainerView.hidden = NO;
        [UIView animateWithDuration:0.0 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width + self.slider1.frame.origin.x, self.view.frame.size.height);
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 150, lblDistance.frame.origin.y, 120, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 150, lblDistance.frame.origin.y + 40, 140, 22);
                    [self.view bringSubviewToFront:self.progressContainerView];
                }
                         completion:^(BOOL finished) {
                         }];
    } else if (recognizer.view.tag == 2) {
        // Long press detected, start the timer
        [self.slider1 setValue:0.0 animated:YES];
        latestDistance = kKeyMinimumRadarMile;
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
        [userdef synchronize];

        swipeContainerView.hidden = NO;
        [UIView animateWithDuration:0.0
                              delay:0.2
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);

                             lblDistance.frame = CGRectMake(self.slider1.frame.origin.x + 20, lblDistance.frame.origin.y, 120, 40);
                             lblNoPPl.frame = CGRectMake(self.slider1.frame.origin.x + 20, lblDistance.frame.origin.y + 40, 140, 22);
                             [self.view bringSubviewToFront:self.progressContainerView];
                         }
                         completion:^(BOOL finished) {
                         }];
    }
    [self calculateAndGetNewUsers];
}

-(void)refreshControllerData {
    [UIView animateWithDuration:0.0 delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                     } completion:^(BOOL finished) {
                         [_radarRefreshOverlay setHidden:NO];
                         if (!_radarRefreshOverlay) {
                             // Add view to refresh radar
                             _radarRefreshOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                             _radarRefreshOverlay.backgroundColor = [UIColor blackColor];
                             _radarRefreshOverlay.alpha = 0.7;
                             _radarRefreshOverlay.userInteractionEnabled = TRUE;
                             _radarRefreshOverlay.tag = 100000;
                             // Add update lbl
                             UILabel *updateLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 70, SCREEN_HEIGHT - 170, 140, 20)];
                             [updateLbl setText:@"Update radar now"];
                             [updateLbl setTextColor:[UIColor whiteColor]];
                             [updateLbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:14]];
                             [updateLbl setTextAlignment:NSTextAlignmentCenter];
                             [_radarRefreshOverlay addSubview:updateLbl];
                             UITapGestureRecognizer *tapGestOnRefreshOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRefreshOverlay:)];
                             [_radarRefreshOverlay addGestureRecognizer:tapGestOnRefreshOverlay];
                             [self.tabBarController.view addSubview:_radarRefreshOverlay];
                         }
                     }];
    // add refresh btn
    UIButton *refreshBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, SCREEN_HEIGHT - 130, 60, 60)];
    refreshBtn.layer.cornerRadius = refreshBtn.frame.size.width / 2;
    refreshBtn.opaque = true;
    [refreshBtn setImage:[UIImage imageNamed:@"update-radar-white.png"] forState:UIControlStateNormal];
    [refreshBtn setBackgroundColor:[UIColor whiteColor]];
    [refreshBtn addTarget:self action:@selector(refreshRadarBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [refreshBtn setUserInteractionEnabled:TRUE];
    [self.tabBarController.view addSubview:refreshBtn];
    [self.tabBarController.view bringSubviewToFront:refreshBtn];
}

- (void)longPressRadar:(UILongPressGestureRecognizer *)recognizer {
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
  //  NSLog(@"%s", __PRETTY_FUNCTION__);
    addTextInSignificantTxt(@"didEnterRegion called");

    if (region.notifyOnExit) {

        if (![_didEnterRegionArray containsObject:region]) {
            [_didEnterRegionArray addObject:region];
        }
    } else {
        [manager stopMonitoringForRegion:region];
    }
    //Call update location notification here after getting updated location
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *time = [[NSDateFormatter alloc] init];

    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    [time setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    [time setAMSymbol:@"am"];
    [time setPMSymbol:@"pm"];
    NSString *timeString = [[[time stringFromDate:currentDate] stringByAppendingString:@" "] stringByAppendingString:localAbbreviation];
    NSString *dayTime = [NSString stringWithFormat:@"%@", timeString];

    NSDictionary *dict = @{[NSString stringWithFormat:@"%@", kKeyId]: [NSString stringWithFormat:@"%@", region.identifier]};
    NSArray *trackInIdArray = @[dict];
    NSData *json = [NSJSONSerialization dataWithJSONObject:trackInIdArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSMutableDictionary *trackDict = [@{@"in": jsonString, @"date": dayTime} mutableCopy];
    [server makeAsychronousRequest:kKeyClassNotifyIn inParams:trackDict isIndicatorRequired:NO inMethodType:kPOST];

}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
  //  NSLog(@"%s", __PRETTY_FUNCTION__);
    addTextInSignificantTxt(@"didExitRegion called");

    if ([_didEnterRegionArray containsObject:region]) {
        [_didEnterRegionArray removeObject:region];
        [(APP_DELEGATE).locationManager stopMonitoringForRegion:region];
    } else {
        [manager stopMonitoringForRegion:region];
    }

    NSDate *currentDate = [NSDate date];
    NSDateFormatter *time = [[NSDateFormatter alloc] init];

    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    [time setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    [time setAMSymbol:@"am"];
    [time setPMSymbol:@"pm"];
    NSString *timeString = [[[time stringFromDate:currentDate] stringByAppendingString:@" "] stringByAppendingString:localAbbreviation];
    NSString *dayTime = [NSString stringWithFormat:@"%@", timeString];

    NSDictionary *dict = @{[NSString stringWithFormat:@"%@", kKeyId]: [NSString stringWithFormat:@"%@", region.identifier]};
    NSArray *trackOutIdArray = @[dict];
    NSData *json = [NSJSONSerialization dataWithJSONObject:trackOutIdArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSMutableDictionary *trackDict = [@{@"out": jsonString, @"date": dayTime} mutableCopy];
    [server makeAsychronousRequest:kKeyClassNotifyOut inParams:trackDict isIndicatorRequired:NO inMethodType:kPOST];
 //   NSLog(@"exit from region is called");
}

// --------------------------------------------------------------------------------
CGFloat DegreesToRadians(CGFloat degrees) {
    return degrees * M_PI / 180;
};

CGFloat RadiansToDegrees(CGFloat radians) {
    return radians * 180 / M_PI;
};
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    (APP_DELEGATE).compassHeading = newHeading.trueHeading;
    trueHeading = newHeading.trueHeading;
    (APP_DELEGATE).locationManager.delegate = nil;
    CGPoint anchorPoint = CGPointMake(0, -23);
    (APP_DELEGATE).anchor = anchorPoint;
    float heading = newHeading.magneticHeading; //in degrees
    headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    (APP_DELEGATE).rotation = heading;
    currentDeviceBearing = heading;
    [self rotateArcsToHeading:headingAngle];
    //[[NSNotificationCenter defaultCenter]postNotificationName:kKeyNotificationRotateMap object:[NSNumber numberWithFloat:heading]];

    // Heading angle post to other views
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateHeadingValue object:@(heading)];
    
    // rotate north view
    self.northLineView.transform = CGAffineTransformMakeRotation(-DegreesToRadians(newHeading.trueHeading));
}

// ----------------   ----------------------------------------------------------------
// didChangeAuthorizationStatus:
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            if ([APP_DELEGATE isLocationTutorialDone]) {
                [(APP_DELEGATE).locationManager requestAlwaysAuthorization];
            }
        }
            break;

        case kCLAuthorizationStatusDenied: {
        }
            break;

        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [(APP_DELEGATE).locationManager startUpdatingLocation]; //Will update location immediately
        }
            break;

        default:
            break;
    }
}

#pragma mark - Notification Method

// --------------------------------------------------------------------------------
// batteryUsageSliderUpdate:
- (void)batteryUsageSliderUpdate:(NSNotification *)notify {
    if ((APP_DELEGATE).lastLocation && [[UIApplication sharedApplication] applicationState] != UIApplicationStateBackground) {
       // NSLog(@"batteryUsageSliderUpdate");
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
        int delayTime = [self getDelayTime];

        /*if(delayTime > 0)
        {
            //StartmonitoringLocation
            self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:delayTime
                                                                     target:self selector:@selector(StartmonitoringLocation)
                                                                   userInfo:nil repeats:YES];
        }*/
    }
}

- (void)filterUserListOnMap:(NSNotification *)inNotify {
    latestDistance = [[inNotify object] floatValue];
    [self calculateAndGetNewUsers];
}

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:
- (void)getProfileDetailsSucceed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// getProfileDetailsFailed:

- (void)getProfileDetailsFailed:(NSNotification *)inNotify {
}


- (int)getDelayTime {

    int delayTime = 60;

    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
    NSInteger hour = [dateComponents hour];
    NSInteger minutes = [dateComponents minute];

    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeySmartBatteryUsage] boolValue] == YES && hour < 6) {

        int differenceInHour = 6 - (int) hour;

        differenceInHour = (3600 * differenceInHour) - (60 * (int) minutes);

        delayTime = differenceInHour;
    } else {

        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

        if (![defaults objectForKey:kKeyLocationUpdateTime]) {
            [defaults setObject:@"136" forKey:kKeyLocationUpdateTime];
            [defaults synchronize];
        }

        int updateDuration = [[defaults objectForKey:kKeyLocationUpdateTime] intValue];
        //int delayTime = 60;
        if (updateDuration == 0) {
            delayTime = 0;
        } else if (updateDuration == 34) {
            delayTime = 60 * 60;
        } else if (updateDuration == 68) {
            delayTime = 30 * 60;
        } else if (updateDuration == 102) {
            delayTime = 5 * 60;
        } else if (updateDuration == 136) {
            delayTime = 60;
        } else if (updateDuration == 170) {
            delayTime = 10;
        }
    }
    return delayTime;
}

// --------------------------------------------------------------------------------
- (void)getRadarUsersSucceed:(NSNotification *)inNotify {
    // NSLog(@"%@",[inNotify valueForKey:@"object"]);
    
    //----->
    //TODO: - Get data success
    
    [APP_DELEGATE hideActivityIndicator];
    _isGetUsersListCallInProgress = NO;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = NO;
    NSMutableArray *arr = [NSMutableArray new];
    NSDictionary *dict = [inNotify userInfo];
    if ([[dict valueForKey:@"objectUrl"] localizedCaseInsensitiveContainsString:@"ALL"]) {
        if ((APP_DELEGATE).oldDataArray && (APP_DELEGATE).oldDataArray.count && sourceArr.count) {
         //   NSLog(@"%lu", (unsigned long) (APP_DELEGATE).oldDataArray.count);
            if ([[inNotify object] count] != (APP_DELEGATE).oldDataArray.count) {
                NSMutableIndexSet *discardedItems = [NSMutableIndexSet indexSet];
                for (NSDictionary *userDict in (APP_DELEGATE).oldDataArray) {
                    NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"id=%@", [userDict valueForKey:@"dataId"]];
                    NSArray *tempArr = [[inNotify object] filteredArrayUsingPredicate:predicateForDistanceObj];
                    int index = 0;
                    NSDictionary *removeDict = [NSDictionary new];
                    if (tempArr.count == 0) {
                        for (int i = 0; i < (APP_DELEGATE).oldDataArray.count; i++) {
                            NSDictionary *dict = (APP_DELEGATE).oldDataArray[i];
                            index = i;
                            if ([[dict valueForKey:@"dataId"] isEqualToString:[userDict valueForKey:@"dataId"]]) {
                                removeDict = userDict;
                                break;
                            }
                        }
                        [discardedItems addIndex:index];
                        for (int i = 0; i < sourceArr.count; i++) {
                            NSDictionary *dict = sourceArr[i];
                            index = i;
                            if ([[dict valueForKey:kKeyId] isEqualToString:[removeDict valueForKey:@"dataId"]]) {
                                [sourceArr removeObjectAtIndex:index];
                                break;
                            }
                        }
                    }
                }
                [(APP_DELEGATE).oldDataArray removeObjectsAtIndexes:discardedItems];
            } else {
                NSMutableArray *filteredArr = [[NSMutableArray alloc] init];
                NSMutableArray *isTrackedArr = [[NSMutableArray alloc] init];

                for (NSDictionary *dict in [inNotify object]) {
                    NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
                    if ([updatedDict[kKeyDegree] integerValue] == 1) {
                        [filteredArr addObject:dict];
                    }
                    if ([updatedDict[kKeyIsTracking] integerValue] == 1) {
                        [isTrackedArr addObject:dict];
                    }
                }
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:isTrackedArr];
                [defaults setObject:data forKey:@"trackUserArray"];
                [defaults synchronize];
                
                for (NSDictionary *userDict in filteredArr) {
                    NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"id=%@", [userDict valueForKey:@"id"]];
                    NSArray *tempArr = [sourceArr filteredArrayUsingPredicate:predicateForDistanceObj];
                    if (tempArr.count > 0) {
                        for (int i = 0; i < sourceArr.count; i++) {
                            NSDictionary *dict = sourceArr[i];
                            if ([[dict valueForKey:@"id"] isEqualToString:[userDict valueForKey:@"id"]]) {
                                [dict setValue:[userDict valueForKey:kKeyDegree] forKey:kKeyDegree];
                                sourceArr[i] = userDict;
                                break;
                            }
                        }
                        for (int i = 0; i < (APP_DELEGATE).oldDataArray.count; i++) {
                            NSDictionary *dict = (APP_DELEGATE).oldDataArray[i];
                            if ([[dict valueForKey:@"dataId"] isEqualToString:[userDict valueForKey:@"id"]]) {
                                NSMutableDictionary *dictUserDegree = [dict valueForKey:@"_userDict"];
                                [dictUserDegree setValue:[userDict valueForKey:kKeyDegree] forKey:kKeyDegree];
                                [dict setValue:dictUserDegree forKey:@"_userDict"];
                                (APP_DELEGATE).oldDataArray[i] = [radarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation];
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            [(APP_DELEGATE).oldDataArray removeAllObjects];

            [arr addObjectsFromArray:[inNotify object]];

            for (NSDictionary *userDict in [inNotify object]) {
                [(APP_DELEGATE).oldDataArray addObject:[radarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
            }
        }
    } else {
        NSMutableArray *filteredArr = [NSMutableArray new];
        for (NSDictionary *dict in sourceArr) {
            NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
            if ([updatedDict[kKeyDegree] integerValue] == 1) {
                [filteredArr addObject:dict];
            }
        }
        for (NSDictionary *userDict in filteredArr) {
            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"id=%@", [userDict valueForKey:@"id"]];
            NSArray *tempArr = [[inNotify object] filteredArrayUsingPredicate:predicateForDistanceObj];
            int index = 0;
            NSDictionary *removeDict = [[NSDictionary alloc] init];
            if (tempArr.count == 0) {
                for (int i = 0; i < (APP_DELEGATE).oldDataArray.count; i++) {
                    NSDictionary *dict = (APP_DELEGATE).oldDataArray[i];
                    index = i;
                    if ([[dict valueForKey:@"dataId"] isEqualToString:[userDict valueForKey:@"id"]]) {
                        [(APP_DELEGATE).oldDataArray removeObjectAtIndex:index];
                        removeDict = userDict;
                        break;
                    }
                }

                for (int i = 0; i < sourceArr.count; i++) {
                    NSDictionary *dict = sourceArr[i];
                    index = i;
                    if ([[dict valueForKey:kKeyId] isEqualToString:[removeDict valueForKey:@"id"]]) {
                        [sourceArr removeObjectAtIndex:index];
                        break;
                    }
                }
            } else {
                NSDictionary *obj = tempArr[0];
                for (int i = 0; i < [sourceArr count]; i++) {
                    NSDictionary *dict = sourceArr[i];
                    if ([[dict valueForKey:@"id"] isEqualToString:[userDict valueForKey:@"id"]]) {
                        [dict setValue:[userDict valueForKey:kKeyDegree] forKey:kKeyDegree];
                        sourceArr[i] = obj;
                        break;
                    }
                }
                for (int i = 0; i < (APP_DELEGATE).oldDataArray.count; i++) {
                    NSDictionary *dict = (APP_DELEGATE).oldDataArray[i];
                    if ([[dict valueForKey:@"dataId"] isEqualToString:[userDict valueForKey:@"id"]]) {
                        NSMutableDictionary *dictUserDegree = [dict valueForKey:@"_userDict"];
                        [dictUserDegree setValue:[userDict valueForKey:kKeyDegree] forKey:kKeyDegree];
                        [dict setValue:dictUserDegree forKey:@"_userDict"];
                        (APP_DELEGATE).oldDataArray[i] = [radarDistanceClass getObjectFromDict:obj myLocation:server.myLocation];
                        break;
                    }
                }
            }
        }
    }
    for (NSDictionary *userDict in [inNotify object]) {
        NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

        NSArray *tempArr = [(APP_DELEGATE).oldDataArray filteredArrayUsingPredicate:predicateForDistanceObj];

        if (tempArr && tempArr.count) {

            radarDistanceClass *distanceObj = [tempArr firstObject];

            NSDictionary *currentLocationDict = userDict[kKeyLocation];
            NSDictionary *previousLocationDict = distanceObj.userDict[kKeyLocation];

            BOOL isBLocationChanged = NO;
            BOOL isALocationChanged = NO;

            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *previousUpdateDate = [dateFormatter dateFromString:previousLocationDict[kKeyLastSeenDate]];
            NSDate *currentDate = [dateFormatter dateFromString:currentLocationDict[kKeyLastSeenDate]];

            if ([previousUpdateDate compare:currentDate] != NSOrderedSame && [currentLocationDict valueForKey:kKeyLattitude] != NULL && [currentLocationDict valueForKey:kKeyLongitude] != NULL) {
                CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:[[currentLocationDict valueForKey:kKeyLattitude] floatValue] longitude:[[currentLocationDict valueForKey:kKeyRadarLong] floatValue]];

                CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[previousLocationDict valueForKey:kKeyLattitude] floatValue] longitude:[[previousLocationDict valueForKey:kKeyRadarLong] floatValue]];

                CLLocationDistance userBdistance = [toLoc distanceFromLocation:fromLoc];

                if (userBdistance >= DISTANCEFILTER) {
                    isBLocationChanged = YES;

//                    NSLog(@"id: %@ location changed", userDict[kKeyId]);
//                    NSLog(@"Previous Lat: %@ Current Lat:%@", distanceObj.lat, currentLocationDict[kKeyLattitude]);
//                    NSLog(@"Previous Long: %@ Current Long:%@", distanceObj.longitude, currentLocationDict[kKeyRadarLong]);
                }
                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];

                if (distance >= DISTANCEFILTER) {
                    isALocationChanged = YES;
                }
                if (isBLocationChanged || isALocationChanged) {
                    [arr addObject:userDict];
                }
            } else {
                if ([sourceArr count] != 0) {
                    NSArray *tempArr = [[NSArray alloc] init];
                    NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"id=%@", userDict[kKeyId]];
                    tempArr = [sourceArr filteredArrayUsingPredicate:predicateForDistanceObj];
                    if (tempArr && tempArr.count) {
                        NSMutableDictionary *tempDict = tempArr[0];
                        if ([tempDict[kKeySetting] valueForKey:kkeyDistanceAccuracy] != [userDict[kKeySetting] valueForKey:kkeyDistanceAccuracy]) {
                            [arr addObject:userDict];
                        }
                    }
                }
            }
        } else {
            [arr addObject:userDict];
            [(APP_DELEGATE).oldDataArray addObject:[radarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
        }
    }
    if (arr && arr.count) {
        NSLog(@"arr count: %lu", (unsigned long) arr.count);

        for (NSDictionary *dict in arr) {
            __block NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
            NSDictionary *distanceDict = [self updateDistance:updatedDict];
            updatedDict = distanceDict;

            NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldDataArray indexOfObjectPassingTest:^BOOL(radarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([anotherObj.dataId isEqualToString:updatedDict[kKeyId]]) {
                    return YES;
                } else {
                    return NO;
                }
            }];
            if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldDataArray.count) {
                radarDistanceClass *obj = [radarDistanceClass getObjectFromDict:updatedDict myLocation:server.myLocation];

                (APP_DELEGATE).oldDataArray[indexOfOldDataObject] = obj;
            }
            if ([[updatedDict valueForKey:kKeyDistance] floatValue] <= latestDistance) {
                NSUInteger indexOfObject = [sourceArr indexOfObjectPassingTest:^BOOL(NSDictionary *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {

                    if ([anotherObj[kKeyId] isEqualToString:updatedDict[kKeyId]]) {
                        return YES;
                    } else {
                        return NO;
                    }
                }];

                if (indexOfObject != NSNotFound && sourceArr && sourceArr.count && indexOfObject < sourceArr.count) {
                    sourceArr[indexOfObject] = updatedDict;
                } else {
//                    float distance = [[updatedDict valueForKey:kKeyDistance] floatValue];
//                    if ([distanceUnit isEqualToString:kkeyUnitMiles]) {
//                        if (distance <= 500) {
//                            [updatedDict setValue:@"" forKey:kKeyDistance];
//                        }
//                    } else {
//                        if (distance <= 805) {
//                            [updatedDict setValue:@"" forKey:kKeyDistance];
//                        }
//                    }
                    [sourceArr addObject:updatedDict];
                }
            }
        }
        (APP_DELEGATE).oldSourceArray = sourceArr;
        
        //Need to update trackuserarr from sourcearr
        (APP_DELEGATE).trackUserArray = [[NSMutableArray alloc] init];
        [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                [(APP_DELEGATE).trackUserArray addObject:obj];
            }
        }];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:(APP_DELEGATE).trackUserArray];
        [defaults setObject:data forKey:@"trackUserArray"];
        [defaults synchronize];
        
        [self PlotUsers];
        //By Madhura
        [self callDistanceAPIForMatrixRequestArr];
    } else {
        (APP_DELEGATE).oldSourceArray = sourceArr;
        [self PlotUsers];
    }
}

// Draw users onto radar
- (void)PlotUsers {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = latestDistance;
        //kKeyMaximumRadarMile
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }

    latestDistance = distancevalue;
    settingDistance = distancevalue;
    sourceArr = (APP_DELEGATE).oldSourceArray;
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else {
        distanceUnit = kkeyUnitKilometers;
    }
    dispatch_async(dispatch_get_main_queue(), ^{

        if (latestDistance <= settingDistance) {
            [self calculateAndGetNewUsers];
            [self displayUsersDotsOnRadar];
        } else {
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        }
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];

        // Add Location Offset and then send data to Map
        NSMutableArray *mapLocationOffsetArray = [[NSMutableArray alloc] init];

        for (radarDistanceClass *obj in (APP_DELEGATE).oldDataArray) {
            [mapLocationOffsetArray addObject:obj.userDict];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapUserChanges object:mapLocationOffsetArray];
        if ((APP_DELEGATE).isTabClicked == YES && (APP_DELEGATE).isReqInProcess == true) {
            (APP_DELEGATE).isTabClicked = NO;
            (APP_DELEGATE).isReqInProcess = false;
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateTimer object:nil];
        }
    });
}
// --------------------------------------------------------------------------------
// getRadarUsersFailed:

- (void)getRadarUsersFailed:(NSNotification *)inNotify {

    _isGetUsersListCallInProgress = NO;
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:[inNotify object]];
}

// --------------------------------------------------------------------------------
// filterTheRadarList:

- (void)filterTheRadarList:(NSNotification *)inNotify {
    (APP_DELEGATE).swipedCellUserId = nil;
    (APP_DELEGATE).swipedCellIndexPath = nil;
    //Remove user card on filter button tap
    NSMutableArray *filteredArr = [NSMutableArray new];
    filterDictionary = [inNotify object];
    grpNotifiArr = [[NSMutableArray alloc] init];

    [grpNotifiArr removeAllObjects];
    [filteredArr removeAllObjects];

    // For Selected Group Notification and respective  Radar Change
    grpNotifiArr = [filterDictionary valueForKey:@"group_notification"];


    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
    [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
        if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
            [tempArr addObject:obj];
        }
    }];
    (APP_DELEGATE).trackUserArray = tempArr;


    //selected TrackUsers
    NSMutableArray *filteredTrackUsers = [[NSMutableArray alloc] init];
    if (filterDictionary[kKeyGroupTag]) {

        if ([filterDictionary[kKeyGroupType] isEqualToString:@"Selected Connections"]) {

            NSUInteger userId = [filterDictionary[kKeyGroupTag] integerValue];
            [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue] && ([[obj valueForKey:@"id"] integerValue] == userId)) {
                    [filteredTrackUsers addObject:obj];
                }
            }];
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSData *data = [defaults objectForKey:@"trackUserArray"];
//            NSMutableArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//            [defaults synchronize];
//            for (int i = 0; i < myArray.count; i++){
//                if ([[myArray[i] valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[myArray[i] objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[myArray[i] objectForKey:kKeyDegree] integerValue] == 1 && [[myArray[i] objectForKey:@"is_tracking"] boolValue] && ([[myArray[i] valueForKey:@"id"] integerValue] == userId)) {
//                    [filteredTrackUsers addObject:myArray[i]];
//                }
//            }
            nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
            [self displayUsersDotsOnRadar];

            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];

        } else if ([filterDictionary[kKeyGroupTag] integerValue] == 2) {
//            [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
//                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
//                    if ([[obj valueForKey:@"is_active_connection"] boolValue]) {
//                        [filteredTrackUsers addObject:obj];
//                    }
////                    [filteredTrackUsers addObject:obj];
//                }
//            }];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [defaults objectForKey:@"trackUserArray"];
            NSMutableArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [defaults synchronize];
            for (int i = 0; i < myArray.count; i++){
                if ([[myArray[i] valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[myArray[i] objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[myArray[i] objectForKey:kKeyDegree] integerValue] == 1 && [[myArray[i] objectForKey:@"is_tracking"] boolValue]) {
                    if ([[myArray[i] valueForKey:@"is_active_connection"] boolValue]) {
                        [filteredTrackUsers addObject:myArray[i]];
                    }
                }
            }
            nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
            [self displayUsersDotsOnRadar];

            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
        } else if (![filterDictionary[kKeyGroupTag] boolValue]) {
            [filteredArr removeAllObjects];
            if ([filterDictionary[kKeyGroupType] isEqualToString:@"All Connections"]) {
                [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                    if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && ![[obj objectForKey:kKeyDegree] isKindOfClass:[NSNull class]] && [[obj objectForKey:kKeyDegree] integerValue] == 1 && [[obj objectForKey:@"is_tracking"] boolValue]) {
                        [filteredTrackUsers addObject:obj];
                    }
                }];
                nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
                [self displayUsersDotsOnRadar];

                // Post notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
            }
        } else if ([filterDictionary[kKeyGroupType] isEqualToString:@"Family"]) {
            [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance && [[obj objectForKey:kKeyFamily] isKindOfClass:[NSDictionary class]] && [[obj objectForKey:@"is_tracking"] boolValue]) {
                    [filteredTrackUsers addObject:obj];
                }
            }];
            nearbyUsers = [NSArray arrayWithArray:filteredTrackUsers];
            [self displayUsersDotsOnRadar];
            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
        }
    } else if ([grpNotifiArr count] > 0) {
        [filteredArr removeAllObjects];
        [filteredArr addObjectsFromArray:grpNotifiArr];
        nearbyUsers = [NSArray arrayWithArray:filteredArr];
        [self displayUsersDotsOnRadar];
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
    } else if ([filteredTrackUsers count] > 0) {
        [filteredArr removeAllObjects];
        [filteredArr addObjectsFromArray:filteredTrackUsers];
        nearbyUsers = [NSArray arrayWithArray:filteredArr];
        [self displayUsersDotsOnRadar];
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
    } else {
        //group_notification
        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
        if (latestDistance <= settingDistance) {
            [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                if ([[obj valueForKey:kKeyDistance] floatValue] <= latestDistance) {
                    [tmpArray addObject:obj];
                }
            }];
            for (NSDictionary *dict in tmpArray) {
                NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];

                if ([updatedDict[kKeyFamily] isKindOfClass:[NSDictionary class]] && [updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyIsFamilyMember] integerValue] == 1) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && ([[filterDictionary valueForKey:kKeyConnection] integerValue] == 2)) {
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 3) {
                    //For All
                    [filteredArr addObject:dict];
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 4) {
                    //For Active
                    if ([[updatedDict valueForKey:@"is_active_connection"] boolValue]) {
                        [filteredArr addObject:dict];
                    }
                } else if ([updatedDict[kKeyDegree] integerValue] == 1 && [[filterDictionary valueForKey:kKeyConnection] integerValue] == 5) {
                    //For favourite
                    if ([[updatedDict valueForKey:kKeyIsFavourite] boolValue]) {
                        [filteredArr addObject:dict];
                    }
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && [updatedDict[kKeyDegree] integerValue] == (APP_DELEGATE).topBarView.selectedDegree && (APP_DELEGATE).topBarView.selectedDegree <= 6 && (APP_DELEGATE).topBarView.selectedDegree > 1) {

                    [filteredArr addObject:dict];
                } else if ((APP_DELEGATE).topBarView.selectedDegree == -1 && (APP_DELEGATE).topBarView.btnCommunity.selected) {
                    [filteredArr removeAllObjects];
                    [filteredArr addObjectsFromArray:sourceArr];
                } else if ((APP_DELEGATE).topBarView.btnCommunity.selected && (APP_DELEGATE).topBarView.selectedDegree == 6) {
                    if (![dict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
                        [filteredArr addObject:dict];
                    }
                }
            }
            nearbyUsers = [NSArray arrayWithArray:filteredArr];
            [self displayUsersDotsOnRadar];
            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
        } else {
            [self calculateAndGetNewUsers];
            [self displayUsersDotsOnRadar];
        }
    }
    // Show text for no people
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld people in %.f %@", (unsigned long) nearbyUsers.count, latestDistance, distanceUnit];
    if (nearbyUsers.count > 0) {
        self.activityIndicator.hidden = YES;
    }
}

- (void)NotifyUser:(NSNotification *)inNotify {
   // NSLog(@"%@", [[NSUserDefaults standardUserDefaults] arrayForKey:@"LocationIn"]);
    NSMutableArray *notifyInArr = [[NSMutableArray alloc] init];
    if ([[inNotify object] count] > 0) {
        NSMutableArray *trackInArray = [[NSMutableArray alloc] initWithArray:[[inNotify object] valueForKey:@"in"]];
        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
        if (trackInArray.count > 20) {
            [tmpArray removeAllObjects];
            for (int i = 0; i < 20; i++) {
                [tmpArray addObject:trackInArray[i]];
            }
            trackInArray = [[NSMutableArray alloc] initWithArray:tmpArray];
        }
        for (int i = 0; i < [trackInArray count]; i++) {
            NSDictionary *dict = trackInArray[i];
            if ([[[NSUserDefaults standardUserDefaults] arrayForKey:@"LocationIn"] count] == 0) {
                if ([[dict valueForKey:kKeyDistance] intValue] < [[dict valueForKey:kKeyRadius] intValue]) {
                    [dict setValue:@"Yes" forKey:@"Notified"];
                    NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:dict];
                    [notifyInArr addObject:myData];
                 //   NSLog(@"%@", notifyInArr);
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setObject:notifyInArr forKey:@"LocationIn"];
                    [userDefaults synchronize];
                   // NSLog(@"%@", [[NSUserDefaults standardUserDefaults] arrayForKey:@"LocationIn"]);
                    [self notifyInAPI:dict];
                }
            } else {
                bool FoundinTrackInObj = FALSE;
                notifyInArr = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"LocationIn"] mutableCopy];
                for (int j = 0; j < [notifyInArr count]; j++) {
                    NSData *storedData = [notifyInArr[j] mutableCopy];
                    NSDictionary *dictnotifyInArr = (NSDictionary *) [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
                    if ([dictnotifyInArr valueForKey:kKeyId] == [dict valueForKey:kKeyId]) {
                        FoundinTrackInObj = TRUE;
                        break;
                    }
                }
                if (FoundinTrackInObj == FALSE) {
                    if ([[dict valueForKey:kKeyDistance] intValue] < [[dict valueForKey:kKeyRadius] intValue]) {
                        [dict setValue:@"Yes" forKey:@"Notified"];
                        NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:dict];
                        [notifyInArr addObject:myData];
                       // NSLog(@"%@", notifyInArr);
                        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                        [userDefaults setObject:notifyInArr forKey:@"LocationIn"];
                        [userDefaults synchronize];
                        [self notifyInAPI:dict];
                    }
                }
            }
        }
        NSMutableIndexSet *deletedItems = [NSMutableIndexSet indexSet];
        notifyInArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LocationIn"] mutableCopy];
        for (int j = 0; j < [notifyInArr count]; j++) {
            bool found = FALSE;
            NSData *storedData = [notifyInArr[j] mutableCopy];
            NSDictionary *dictnotifyInArr = (NSDictionary *) [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
            for (int i = 0; i < [trackInArray count]; i++) {
                NSDictionary *dict = trackInArray[i];
                if ([dictnotifyInArr valueForKey:kKeyId] == [dict valueForKey:kKeyId]) {
                    found = TRUE;
                    break;
                }
            }
            if (found == FALSE) {
                [deletedItems addIndex:j];
            }
        }
        if ([deletedItems count] > 0) {
            [notifyInArr removeObjectsAtIndexes:deletedItems];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:notifyInArr forKey:@"LocationIn"];
            [userDefaults synchronize];
        }
    }
}

- (void)NotifyUserOut:inNotify {
    NSArray *arrNotifyObject = [[NSArray alloc] init];
    arrNotifyObject = [inNotify object];
    NSMutableArray *notifyOutArr = [[NSMutableArray alloc] init];
    if ([[inNotify object] count] > 0) {
        NSMutableArray *trackOutArray = [[NSMutableArray alloc] initWithArray:[[inNotify object] valueForKey:@"out"]];
        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
        if (trackOutArray.count > 20) {
            [tmpArray removeAllObjects];
            for (int i = 0; i < 20; i++) {
                [tmpArray addObject:trackOutArray[i]];
            }
            trackOutArray = [[NSMutableArray alloc] initWithArray:tmpArray];
        }
        for (int i = 0; i < [trackOutArray count]; i++) {
            NSDictionary *dict = trackOutArray[i];
            [dict setValue:@"Yes" forKey:@"Notified"];
            if ([[[NSUserDefaults standardUserDefaults] arrayForKey:@"LocationOut"] count] == 0) {
                if ([[dict valueForKey:kKeyDistance] intValue] > [[dict valueForKey:kKeyRadius] intValue] && [[dict valueForKey:kKeyDistance] intValue] < [[dict valueForKey:kKeyRadius] intValue] + 150) {
                    NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:dict];
                    [notifyOutArr addObject:myData];
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setValue:notifyOutArr forKey:@"LocationOut"];
                    [userDefaults synchronize];
                    [self notifyOutAPI:dict];
                }
            } else {
                bool FoundinTrackInObj = FALSE;
                notifyOutArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LocationOut"] mutableCopy];
                for (int i = 0; i < [notifyOutArr count]; i++) {
                    NSData *storedData = [notifyOutArr[i] mutableCopy];
                    NSDictionary *dictnotifyInArr = (NSDictionary *) [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
                    if ([dictnotifyInArr valueForKey:kKeyId] == [dict valueForKey:kKeyId]) {
                        FoundinTrackInObj = TRUE;
                        break;
                    }
                }
                if (FoundinTrackInObj == FALSE) {
                    if ([[dict valueForKey:kKeyDistance] intValue] > [[dict valueForKey:kKeyRadius] intValue] && [[dict valueForKey:kKeyDistance] intValue] < [[dict valueForKey:kKeyRadius] intValue] + 150) {
                        NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:dict];
                        [notifyOutArr addObject:myData];
                        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                        [userDefaults setValue:notifyOutArr forKey:@"LocationOut"];
                        [userDefaults synchronize];
                        [self notifyOutAPI:dict];
                    }
                }
            }
        }
        NSMutableIndexSet *deletedItems = [NSMutableIndexSet indexSet];
        notifyOutArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LocationOut"] mutableCopy];
        for (int j = 0; j < [notifyOutArr count]; j++) {
            bool found = FALSE;
            NSData *storedData = [notifyOutArr[j] mutableCopy];
            NSDictionary *dictnotifyInArr = (NSDictionary *) [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
            for (int i = 0; i < [trackOutArray count]; i++) {
                NSDictionary *dict = trackOutArray[i];
                if ([dictnotifyInArr valueForKey:kKeyId] == [dict valueForKey:kKeyId] && [[dictnotifyInArr valueForKey:@"Notified"] isEqualToString:@"Yes"]) {
                    found = TRUE;
                    break;
                }
            }
            if (found == FALSE) {
                [deletedItems addIndex:j];
            }
        }
        if ([deletedItems count] > 0) {
            [notifyOutArr removeObjectsAtIndexes:deletedItems];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:notifyOutArr forKey:@"LocationOut"];
            [userDefaults synchronize];
        }
    }
}

- (void)notifyInAPI:(NSDictionary *)dict {
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *time = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    [time setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    [time setAMSymbol:@"am"];
    [time setPMSymbol:@"pm"];
    NSString *timeString = [[[time stringFromDate:currentDate] stringByAppendingString:@" "] stringByAppendingString:localAbbreviation];
    NSString *dayTime = [NSString stringWithFormat:@"%@", timeString];

    NSDictionary *dictjson = @{[NSString stringWithFormat:@"%@", kKeyId]: [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]]};
    NSArray *trackInIdArray = @[dictjson];
    NSData *json = [NSJSONSerialization dataWithJSONObject:trackInIdArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSMutableDictionary *trackDict = [@{@"in": jsonString, @"date": dayTime} mutableCopy];
    [server makeAsychronousRequest:kKeyClassNotifyIn inParams:trackDict isIndicatorRequired:NO inMethodType:kPOST];
}

- (void)notifyOutAPI:(NSDictionary *)dict {
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *time = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZoneLocal = [NSTimeZone localTimeZone];
    NSString *localAbbreviation = [timeZoneLocal abbreviation];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:localAbbreviation];
    [time setTimeZone:utcTimeZone];
    [time setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
    [time setAMSymbol:@"am"];
    [time setPMSymbol:@"pm"];
    NSString *timeString = [[[time stringFromDate:currentDate] stringByAppendingString:@" "] stringByAppendingString:localAbbreviation];
    NSString *dayTime = [NSString stringWithFormat:@"%@", timeString];
    NSDictionary *dictjson = @{[NSString stringWithFormat:@"%@", kKeyId]: [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]]};
    NSArray *trackInIdArray = @[dictjson];
    NSData *json = [NSJSONSerialization dataWithJSONObject:trackInIdArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSMutableDictionary *trackDict = [@{@"out": jsonString, @"date": dayTime} mutableCopy];
    [server makeAsychronousRequest:kKeyClassNotifyOut inParams:trackDict isIndicatorRequired:NO inMethodType:kPOST];
}

- (NSString *)eventInOutStatus:(NSInteger)distance currentStatus:(NSInteger)status {
    if (distance <= 0.5 && status != 2)
        return @"2";
    else if (distance <= 3 && status != 2 && status != 1)
        return @"1";
    else if (distance > 1 && status <= 2)
        return @"3";
    else
        return @"0";

}

- (void)eventInOut:(NSArray *)arrEvents {
    for (int i = 0; i < [arrEvents count]; i++) {
        NSDictionary *dictEvent = arrEvents[i];
        NSMutableArray *notifyEventArr = [[NSMutableArray alloc] init];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:[dictEvent valueForKey:kKeyUserID] forKey:kKeyUserID];
        [dict setValue:[dictEvent valueForKey:kKeyId] forKey:kKeyEventID];
        [dict setValue:[dictEvent valueForKey:kKeyDistance] forKey:kKeyDistance];
        NSString *status = @"";
        if ([[[NSUserDefaults standardUserDefaults] arrayForKey:@"EventInOut"] count] == 0)
            status = [self eventInOutStatus:[[dictEvent valueForKey:kKeyDistance] integerValue] currentStatus:0];
        else {
            bool found = FALSE;
            notifyEventArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"EventInOut"] mutableCopy];
            for (int j = 0; j < [notifyEventArr count]; j++) {
                NSData *storedData = [notifyEventArr[j] mutableCopy];
                NSDictionary *dictnotifyInArr = (NSDictionary *) [NSKeyedUnarchiver unarchiveObjectWithData:storedData];
                if ([dictnotifyInArr valueForKey:kKeyId] == [dictEvent valueForKey:kKeyId]) {
                    found = true;
                    status = [self eventInOutStatus:[[dictEvent valueForKey:kKeyDistance] integerValue] currentStatus:[[dictnotifyInArr valueForKey:kKeyStatus] integerValue]];
                    break;
                }
            }
            if (found == FALSE)
                status = [self eventInOutStatus:[[dictEvent valueForKey:kKeyDistance] integerValue] currentStatus:0];
        }
        [dict setValue:status forKey:kKeyStatus];
        NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:dict];
        [notifyEventArr addObject:myData];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:notifyEventArr forKey:@"EventInOut"];
        [userDefaults synchronize];
        [self eventNotifyInOutAPI:dict];
    }
}

- (void)eventNotifyInOutAPI:(NSDictionary *)dict {
    NSArray *trackInIdArray = @[dict];
    NSData *json = [NSJSONSerialization dataWithJSONObject:trackInIdArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSMutableDictionary *trackDict = [@{@"notify": jsonString} mutableCopy];
    [server makeAsychronousRequest:kKeyClassEventNotify inParams:trackDict isIndicatorRequired:NO inMethodType:kPOST];
}

// --------------------------------------------------------------------------------
- (void)locationUpdatedSucced:(NSNotification *)inNotify {
    addTextInSignificantTxt(@"locationUpdatedSucced");
    addTextInSignificantTxt([NSString stringWithFormat:@"%@", [inNotify object]]);
    [self NotifyUser:inNotify];
    [self NotifyUserOut:inNotify];
    if ([[inNotify object] objectForKey:@"events"] != nil) {
        NSArray *arrEventData = [[NSMutableArray alloc] initWithArray:[[inNotify object] valueForKey:@"events"]];
        [self eventInOut:arrEventData];
    }
    if ([[inNotify object] count] > 0) {
        NSMutableArray *trackInArray = [[NSMutableArray alloc] initWithArray:[[inNotify object] valueForKey:@"in"]];
        NSMutableArray *trackOutArray = [[NSMutableArray alloc] initWithArray:[[inNotify object] valueForKey:@"out"]];

        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
        if (trackInArray.count > 20) {
            [tmpArray removeAllObjects];
            for (int i = 0; i < 20; i++) {
                [tmpArray addObject:trackInArray[i]];
            }
            trackInArray = [[NSMutableArray alloc] initWithArray:tmpArray];
        }
        if (trackOutArray.count > 20) {
            [tmpArray removeAllObjects];
            for (int i = 0; i < 20; i++) {
                [tmpArray addObject:trackOutArray[i]];
            }
            trackOutArray = [[NSMutableArray alloc] initWithArray:tmpArray];
        }
        NSMutableSet *set1 = [NSMutableSet setWithArray:trackInArray];
        NSMutableSet *set2 = [NSMutableSet setWithArray:trackOutArray];
        [set1 intersectSet:set2]; //this will give you only the obejcts that are in both sets
        NSMutableArray *result = [[NSMutableArray alloc] initWithArray:[set1 allObjects]];
        [trackInArray removeObjectsInArray:result];
        [trackOutArray removeObjectsInArray:result];
        if (trackInArray.count || trackOutArray.count || result.count) {
            NSMutableArray *keepArray = [[NSMutableArray alloc] init];
            NSMutableArray *monitorArray = [[NSMutableArray alloc] initWithArray:[[(APP_DELEGATE).locationManager monitoredRegions] allObjects]];
            //Remove duplicate region from trackIn Array
            for (CLRegion *monitored in [(APP_DELEGATE).locationManager monitoredRegions]) {
                for (int i = 0; i < trackInArray.count; i++) {
                    NSDictionary *dict = trackInArray[i];
                    if ([[dict valueForKey:kKeyId] isEqualToString:monitored.identifier]) {
                        if (monitored.notifyOnEntry) {
                            [trackInArray removeObjectAtIndex:i];
                            if (![keepArray containsObject:monitored]) {
                                [keepArray addObject:monitored];
                            }
                        }
                    }
                }
                //Remove duplicate region from trackOut Array
                for (int i = 0; i < trackOutArray.count; i++) {
                    NSDictionary *dict = trackOutArray[i];
                    if ([[dict valueForKey:kKeyId] isEqualToString:monitored.identifier]) {
                        if (monitored.notifyOnExit) {
                            [trackOutArray removeObjectAtIndex:i];
                            if (![keepArray containsObject:monitored]) {
                                [keepArray addObject:monitored];
                            }
                        }
                    }
                }
                //Remove duplicate region from trackOut Array
                for (int i = 0; i < result.count; i++) {
                    NSDictionary *dict = result[i];
                    if ([[dict valueForKey:kKeyId] isEqualToString:monitored.identifier]) {

                        if (monitored.notifyOnEntry && monitored.notifyOnExit) {
                            [result removeObjectAtIndex:i];

                            if (![keepArray containsObject:monitored]) {

                                [keepArray addObject:monitored];
                            }
                        }
                    }
                }
            }
            [monitorArray removeObjectsInArray:keepArray];
            for (CLRegion *region in monitorArray) {
                if (![_didEnterRegionArray containsObject:region]) {
                    [(APP_DELEGATE).locationManager stopMonitoringForRegion:region];
                }
            }
        }
# warning limit to track 20 locations
        //Set Region for out locations
        for (NSDictionary *dict in trackOutArray) {
            CLLocationCoordinate2D cordinate = CLLocationCoordinate2DMake([[dict valueForKey:kKeyLattitude] floatValue], [[dict valueForKey:kKeyRadarLong] floatValue]);
            CLRegion *region = [[CLCircularRegion alloc] initWithCenter:cordinate radius:[[dict valueForKey:kKeyRadius] floatValue] identifier:[dict valueForKey:kKeyId]];
            region.notifyOnEntry = NO;
            region.notifyOnExit = YES;
            if ([CLLocationManager isMonitoringAvailableForClass:[CLRegion class]]) {
                [(APP_DELEGATE).locationManager startMonitoringForRegion:region];
            }
        }
        //Set Region for in locations
        for (NSDictionary *dict in trackInArray) {
            CLLocationCoordinate2D cordinate = CLLocationCoordinate2DMake([[dict valueForKey:kKeyLattitude] floatValue], [[dict valueForKey:kKeyRadarLong] floatValue]);
            CLRegion *region = [[CLCircularRegion alloc] initWithCenter:cordinate
                                                                 radius:[[dict valueForKey:kKeyRadius] floatValue]
                                                             identifier:[dict valueForKey:kKeyId]];
            region.notifyOnEntry = YES;
            region.notifyOnExit = NO;
            if ([CLLocationManager isMonitoringAvailableForClass:[CLRegion class]]) {
                [(APP_DELEGATE).locationManager startMonitoringForRegion:region];
            }
        }
        for (NSDictionary *dict in result) {
            CLLocationCoordinate2D cordinate = CLLocationCoordinate2DMake([[dict valueForKey:kKeyLattitude] floatValue], [[dict valueForKey:kKeyRadarLong] floatValue]);
            CLRegion *region = [[CLCircularRegion alloc] initWithCenter:cordinate
                                                                 radius:[[dict valueForKey:kKeyRadius] floatValue]
                                                             identifier:[dict valueForKey:kKeyId]];
            region.notifyOnEntry = YES;
            region.notifyOnExit = YES;
            if ([CLLocationManager isMonitoringAvailableForClass:[CLRegion class]]) {
                [(APP_DELEGATE).locationManager startMonitoringForRegion:region];
            }
        }
    }
}

// --------------------------------------------------------------------------------
// locationUpdatedFailed:
- (void)locationUpdatedFailed:(NSNotification *)inNotify {
    addTextInSignificantTxt(@"locationUpdatedFailed");
    addTextInSignificantTxt([NSString stringWithFormat:@"%@", [inNotify object]]);
}


- (void)EventnotifyInNotificationSucceed:(NSNotification *)inNotify {
   // NSLog(@"Success");
}

// --------------------------------------------------------------------------------
// notifyInTrackingNotificationFailed:
- (void)EventnotifyOutNotificationFailed:(NSNotification *)inNotify {
}



// --------------------------------------------------------------------------------
// notifyInTrackingNotificationSucceed:
- (void)notifyInTrackingNotificationSucceed:(NSNotification *)inNotify {
   // NSLog(@"Success");
}

// --------------------------------------------------------------------------------
// notifyInTrackingNotificationFailed:
- (void)notifyInTrackingNotificationFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// notifyOutTrackingNotificationSucceed:
- (void)notifyOutTrackingNotificationSucceed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// notifyOutTrackingNotificationFailed:
- (void)notifyOutTrackingNotificationFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// blockConnectionSucceed:
- (void)blockConnectionSucceed:(NSNotification *)inNotify {
    [(APP_DELEGATE) hideActivityIndicator];
    (APP_DELEGATE).isSwipedCell = NO;

    NSDictionary *userInfo = [inNotify userInfo];

    NSMutableArray *mutatedArr = [NSMutableArray arrayWithArray:sourceArr];
    NSDictionary *postInfo = userInfo[kKeyRequestedParams];

    BOOL isFound = NO;

    for (NSUInteger i = 0; i < [sourceArr count] && !isFound; i++) {
        NSDictionary *userDict = sourceArr[i];
        if ([userDict[kKeyId] isEqualToString:postInfo[kKeyConnectionID]]) {
            isFound = YES;
            [sourceArr removeObject:userDict];

            for (int i = 0; i < [(APP_DELEGATE).oldDataArray count]; i++) {
                radarDistanceClass *distanceClass = (APP_DELEGATE).oldDataArray[i];
                if ([distanceClass.dataId isEqualToString:userDict[kKeyId]]) {
                    //NSLog(@"distanceClass.dataId:%@ and [userDict objectForKey:kKeyId]:%@",distanceClass.dataId,[userDict objectForKey:kKeyId]);

                    [(APP_DELEGATE).oldDataArray removeObjectAtIndex:i];
                }
            }
        }
    }
    if (isFound) {
        if (latestDistance <= settingDistance) {
            [self calculateAndGetNewUsers];
            [self displayUsersDotsOnRadar];
        } else {
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        }
        // Post notification to List
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];
        // Add Location Offset and then send data to Map
        /*NSMutableArray *mapLocationOffsetArray = [[NSMutableArray alloc] init];
         for (radarDistanceClass *obj in (APP_DELEGATE).oldDataArray)
         {
         NSDictionary *userDict = [SRModalClass addLocationOffset:obj.userDict];
         [mapLocationOffsetArray addObject:userDict];
         }*/
        // Post notification to Map
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapUserChanges object:(APP_DELEGATE).oldDataArray];
        //By Madhura
        [self callDistanceAPIForMatrixRequestArr];
    }
}

// --------------------------------------------------------------------------------
// blockConnectionFailed:
- (void)blockConnectionFailed:(NSNotification *)inNotify {
    (APP_DELEGATE).isSwipedCell = NO;
    [SRModalClass showAlert:inNotify.object];
    [APP_DELEGATE hideActivityIndicator];
}

#pragma mark
#pragma mark - tapGesture Method
#pragma mark

- (void)onDotTapped:(UITapGestureRecognizer *)recognizer {
    UIView *circleView = recognizer.view;
    CGPoint point = [recognizer locationInView:circleView];
    // The for loop is to find out multiple dots in vicinity
    // you may define a NSMutableArray before the for loop and
    // get the group of dots together
    NSMutableArray *tappedUsers = [NSMutableArray array];
    for (SRRadarDotView *d in dotsArr) {
        if ([d.layer.presentationLayer hitTest:point] != nil) {
            // you can get the list of tapped user(s if more than one users are close enough)
            // use this variable outside of for loop to get list of users
            [tappedUsers addObject:d.userProfile];
        }
    }
    // use tappedUsers variable according to your app logic
    if (tappedUsers.count > 0) {
        [self addViewsOnScrollView:tappedUsers];
    } else {
        [self.scrollView setHidden:YES];
        [self.toolTipScrollViewImg setHidden:YES];
    }
}

// --------------------------------------------------------------------------------
// pingButtonAction
- (void)pingButtonAction:(UIButton *)sender {
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    BOOL flag = NO;//Check first if you allow to ping or not
    if ([[dictionary valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[dictionary valueForKey:kKeySetting]];
        NSUInteger pingStatus = [[setting valueForKey:kKeyallowPings] integerValue];
        if (pingStatus == 1) {
            flag = YES;
        } else if (pingStatus == 2) {
            flag = YES;
        } else if (pingStatus == 3) {
            if ([[dictionary valueForKey:kKeyConnection] isKindOfClass:[NSDictionary class]]) {
                flag = YES;
            }
        } else if (pingStatus == 4) {
            if ([[dictionary valueForKey:kKeyToFavourite] boolValue]) {
                flag = YES;
            }
        } else if (pingStatus == 5) {
            if ([[dictionary valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
                flag = YES;
            }
        } else if (pingStatus == 6) {
            flag = NO;
        }
    }

    if (flag) {
        if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];
        [APP_DELEGATE hideActivityIndicator];
        // Set chat view
        SRChatViewController *objSRChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:server.friendProfileInfo];

        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRChatView animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    } else {
        [SRModalClass showAlert:@"Sorry, this user has disabled chat so they cannot receive any messages."];
    }
}

// --------------------------------------------------------------------------------
// compassButtonAction
- (void)compassButtonAction:(UIButton *)sender {
    [APP_DELEGATE hideActivityIndicator];
    // Get profile
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    NSDictionary *dataDict = [SRModalClass removeNullValuesFromDict:calloutView.userDict];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dataDict];
    SRMapViewController *compassView = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:dictionary server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:compassView animated:YES];
    self.hidesBottomBarWhenPushed = NO;

}

// --------------------------------------------------------------------------------
// annotationTapAction:
- (void)annotationTapAction:(UITapGestureRecognizer *)sender {
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];
        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];
    if (![dictionary[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
        // Show user public profile
        SRUserTabViewController *tabCon;
        if ([dictionary[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
        } else
            tabCon = [[SRUserTabViewController alloc] initWithNibName:@"DisabledDegreeSeperation" bundle:nil profileData:nil server:server];
        [APP_DELEGATE hideActivityIndicator];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:tabCon animated:YES];
        self.hidesBottomBarWhenPushed = NO;
    }
}

// --------------------------------------------------------------------------------
// viewTapAction:
- (void)viewTapAction:(UITapGestureRecognizer *)sender {
    [self.scrollView setHidden:YES];
    [self.toolTipScrollViewImg setHidden:YES];
}

//
// --------------------------------------------------------------------------------
// getDBPath:
- (NSDictionary *)updateDistance:(NSDictionary *)dict {
    BOOL shouldCallMatrixAPI = NO;
    __block NSDictionary *updatedDict = [[NSMutableDictionary alloc] initWithDictionary:dict];

    float distance = [[updatedDict valueForKey:kKeyDistance] floatValue];

    if ([distanceUnit isEqualToString:kkeyUnitMiles]) {
        [updatedDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 500) {
            shouldCallMatrixAPI = YES;
        }
    } else {
        [updatedDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 805) {
            shouldCallMatrixAPI = YES;
        }
    }
    if (shouldCallMatrixAPI) {
        [_matrixRequestArray addObject:updatedDict];
    }
    return updatedDict;
}

#pragma mark GoogleMap Distance Matrics Methods

- (void)callDistanceAPIForMatrixRequestArr {

    NSMutableArray *requestsArray = [[NSMutableArray alloc] init];
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    __block NSString *checkURL = @"";
    NSMutableArray *arrLocation = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSDictionary *userDict in _matrixRequestArray) {
        if (!isNullObject([userDict valueForKey:kKeyLocation])) {
            arrLocation[i] = userDict;
            CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            request.timeoutInterval = 180;

            [requestsArray addObject:request];
            i++;
        }
    }
    if (requestsArray.count) {
        __block NSInteger currentRequestIndex = 0;

        __weak typeof(self) weakSelf = self;

        _handler = ^void(NSURLResponse *response, NSData *data, NSError *error) {

            if (!error) {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

                if ([weakSelf IsValidResponse:data]) {
                    dict[@"data"] = data; //add graphhopper response

                    if (currentRequestIndex < weakSelf.matrixRequestArray.count) {
                        dict[@"userDict"] = weakSelf.matrixRequestArray[currentRequestIndex];
                        [weakSelf.matrixResponseArray addObject:dict];
                    }
                    currentRequestIndex++;
                    if (currentRequestIndex < [requestsArray count]) {
                        NSURLRequest *requ = requestsArray[currentRequestIndex];
                        NSString *requestPath = [[requ URL] absoluteString];
                        checkURL = requestPath;
                        [NSURLConnection sendAsynchronousRequest:requestsArray[currentRequestIndex]
                                                           queue:[NSOperationQueue mainQueue]
                                               completionHandler:weakSelf.handler];
                    } else {
                        [weakSelf MatrixAPICallCompleted];
                    }
                }
            }
        };
        NSURLRequest *requ = requestsArray[0];
        NSString *requestPath = [[requ URL] absoluteString];
        checkURL = requestPath;
        [NSURLConnection sendAsynchronousRequest:requestsArray[0]
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:weakSelf.handler];
    }
}

- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }
}

- (void)MatrixAPICallCompleted {
    [_matrixRequestArray removeAllObjects];
    if (_matrixResponseArray && _matrixResponseArray.count) {
        BOOL shouldReloadData = NO;
        for (NSDictionary *dict in _matrixResponseArray) {
            NSData *data = dict[@"data"];
            Boolean isjsonValid = false;
            NSDictionary *userDict = dict[@"userDict"];
            double convertDist = 0.0;
            if (data.length > 0) {
                NSError *error;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
               // NSLog(@"\n\nMATRIX API RESPONSE:%@ \n\n", json);
                if ([json[@"paths"] count]) {
                    isjsonValid = true;
                    NSDictionary *dictOSM = [json valueForKey:@"paths"];
                    NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                    NSString *distance = arrdistance[0];
                    convertDist = [distance doubleValue];
                    if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                        //meters to killometer
                        convertDist = convertDist / 1000;
                    } else {
                        //meters to miles
                        convertDist = (convertDist * 0.000621371192);
                    }
                } else {
                   // NSLog(@"Matrix JSON not ok");
                    NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldDataArray indexOfObjectPassingTest:^BOOL(radarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {

                        if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                            return YES;
                        } else {
                            return NO;
                        }
                    }];
                    if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldDataArray.count) {
                        [(APP_DELEGATE).oldDataArray removeObjectAtIndex:indexOfOldDataObject];
                    }
                }
            }
            if (isjsonValid) {
                shouldReloadData = YES;
                [userDict setValue:[NSString stringWithFormat:@"%.2f", convertDist] forKey:kKeyDistance];
                NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldDataArray indexOfObjectPassingTest:^BOOL(radarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {

                    if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                        return YES;
                    } else {
                        return NO;
                    }
                }];

                if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldDataArray.count) {
                    radarDistanceClass *obj = [[radarDistanceClass alloc] init];
                    obj.dataId = userDict[kKeyId];
                    NSDictionary *locationDict = userDict[kKeyLocation];
                    obj.lat = locationDict[kKeyLattitude];
                    obj.longitude = locationDict[kKeyRadarLong];
                    obj.distance = [NSString stringWithFormat:@"%.2fOSM", convertDist];
                    obj.myLocation = server.myLocation;
                    obj.userDict = userDict;

                    (APP_DELEGATE).oldDataArray[indexOfOldDataObject] = obj;
                }
                __block NSDictionary *sourceUserDict = userDict;

                NSUInteger indexOfUserObject = [sourceArr indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *_Nonnull stop) {

                    if ([obj[kKeyId] isEqualToString:sourceUserDict[kKeyId]]) {
                        return YES;
                        *stop = YES;
                    } else {
                        return NO;
                    }
                }];

                if (indexOfUserObject != NSNotFound && indexOfUserObject < sourceArr.count) {

                    sourceArr[indexOfUserObject] = sourceUserDict;
                }
            }
        }
        if (shouldReloadData) {
            dispatch_async(dispatch_get_main_queue(), ^{

                if (latestDistance <= settingDistance) {
                    [self calculateAndGetNewUsers];
                    [self displayUsersDotsOnRadar];
                } else {
                    [self displayUsersDotsOnRadar];
                    [self calculateAndGetNewUsers];
                }
                // Post notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarUserChanges object:nearbyUsers];

                // Add Location Offset and then send data to Map
                NSMutableArray *mapLocationOffsetArray = [[NSMutableArray alloc] init];

                for (radarDistanceClass *obj in (APP_DELEGATE).oldDataArray) {
                    [mapLocationOffsetArray addObject:obj.userDict];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapUserChanges object:mapLocationOffsetArray];
            });
        } else {
           // NSLog(@"All Matrix API failed");
        }
        [_matrixResponseArray removeAllObjects];
    }
}

#pragma mark - ReverseGeocode Methods

- (void)getAddressFromReverseGeocode:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude completionBlock:(void (^)(NSString *address, NSError *error))completionBlock {

    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:toLoc completionHandler:^(NSArray *placemarks, NSError *error) {
      //  NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks lastObject];
            NSArray *lines = placemark.addressDictionary[@"FormattedAddressLines"];
            NSString *addressString = [lines componentsJoinedByString:@", "];
          //  NSLog(@"Address: %@", addressString);
            if (!isNullObject(addressString)) {
                completionBlock(addressString, nil);
            } else {
                completionBlock(nil, getNSErrorObjectWithDescription(@"Address not found", 400));
            }
        } else {
            completionBlock(nil, error);
        }
    }];
}
@end
