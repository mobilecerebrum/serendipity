#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

#import "SRProfileImageView.h"
#import "SRExtraRadarCircle.h"
#import "SRRadarCircleView.h"
#import "SRRadarDotView.h"
#import "SRRadarFocusView.h"
#import "SRHomeParentViewController.h"
#import "ASValueTrackingSlider.h"
#import "SRChatViewController.h"
#import "LocationShareModel.h"
@import TSLocationManager;
@import TSBackgroundFetch;

@interface SRDiscoveryRadarViewController : SRHomeParentViewController <CLLocationManagerDelegate> {
	// Instance variable
	float currentDeviceBearing;
	float latestDistance;
    float settingDistance;

    float headingAngle,trueHeading;
    
	NSTimer *detectCollisionTimer;

	NSArray *nearbyUsers;
	
    NSMutableArray *oldSourceArr;
	NSMutableArray *dotsArr;
    NSMutableArray *dotsUserPicsArr;
    NSMutableArray *grpNotifiArr;
    
	NSMutableDictionary *userDataDict;
    NSDictionary *filterDictionary;

	// View
	SRRadarCircleView *radarCircleView;
	SRRadarFocusView *radarFocusView;
    NSArray *groupUsers;
    
	UIImageView *swipeView;
	UIView *swipeContainerView;
	UILabel *lblDistance;
	UILabel *lblNoPPl;

	CGPoint touchLocation;
    int sliderX,headingcount,buildNo;
	// Flag
	BOOL isOnce;
    BOOL isSwipedOnce;
    NSArray *_sliders;
    NSString *distanceUnit, *addressStr;

    NSString *updatedLocation;
    TSLocationManager *bgGeo;
    BOOL isPresentView,isBackgroundMode,defferingUpdates;
    BOOL isTimerOn,shouldUpdateToServer,updateRemaining;
    UIBackgroundTaskIdentifier backgroundTask,backgroundUpdatetask;
    NSTimer *timer;
}
@property(nonatomic,strong)CMMotionActivityManager *motionHandler;
@property (strong,nonatomic) LocationShareModel * shareModel;

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet SRProfileImageView *bckProfileImg;
@property (nonatomic, strong) IBOutlet UIView *radarViewContainer;
@property (nonatomic, strong) IBOutlet UIView *northLineView;
@property (nonatomic, strong) UIView *radarRefreshOverlay;
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider1;
@property (nonatomic, strong) IBOutlet SRProfileImageView *radarProfileImgView;
@property (nonatomic, strong) IBOutlet SRExtraRadarCircle *extraCircleView;
@property (nonatomic, strong) IBOutlet UIView *progressContainerView;
@property (nonatomic, strong) IBOutlet UILabel *lblMiles;
@property (nonatomic, strong) IBOutlet UILabel *lblInPpl;

@property (nonatomic, strong) IBOutlet UIButton *btnReduceMiles;
@property (nonatomic, strong) IBOutlet UIButton *btnIncreasedMiles;
@property (weak, nonatomic) IBOutlet UIImageView *milesSetPlusImg;
@property (weak, nonatomic) IBOutlet UIImageView *milesSetminusImg;

// Properties
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic,strong)CLLocation *oldLocation;
@property (nonatomic,strong) NSDate *locationUpdateDate;
@property (nonatomic,strong) NSTimer *timerForLocationUpdate;

@property (nonatomic,strong) NSTimer *updateTimer, *updateLocationTimer;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) __block NSMutableArray *sourceArr;

@property NSInteger secondDifference;

@property BOOL isGetUsersListCallInProgress;

//+ (NSString *)getModel;

- (void)detectCollisions1:(NSTimer *)theTimer;

//For Google Matrix API call and response
@property (nonatomic,strong) __block NSMutableArray *matrixRequestArray;
@property (nonatomic,strong) __block NSMutableArray *matrixResponseArray;
@property (nonatomic,copy) void (^handler)(NSURLResponse *response, NSData *data, NSError *error);

#pragma mark - Init Method

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark - IBAction Method

- (IBAction)actionOnButtonClick:(id)sender;
-(void) PlotUsers;


@end
