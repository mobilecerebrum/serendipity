//
//  InviteNewContactCell.h
//  Serendipity
//
//  Created by Hitesh Surani on 25/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InviteNewContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMark;
@property (weak, nonatomic) IBOutlet UIImageView *imgSerendipityUser;

@end

NS_ASSUME_NONNULL_END
