//
//  InviteNewContactCell.m
//  Serendipity
//
//  Created by Hitesh Surani on 25/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "InviteNewContactCell.h"

@implementation InviteNewContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
