//
//  SocialContactListViewController.m
//  Serendipity
//
//  Created by Hitesh Surani on 20/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "SocialContactListViewController.h"
#import "SRModalClass.h"
#import "InviteNewContactCell.h"
#import "SRModalClass.h"

@interface SocialContactListViewController ()

@end

@implementation SocialContactListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     
    if (_typeOfScreen == GmailContact){
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.socialcontact.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }else if (_typeOfScreen == YahooContact){
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.yahoosocialcontact.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }else if (_typeOfScreen == Outlook){
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.outlook.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }
    [_tableView registerNib:[UINib nibWithNibName:@"InviteNewContactCell" bundle:nil] forCellReuseIdentifier:@"InviteNewContactCell"];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:5];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *rightDoneButton = [SRModalClass setRightNavBarItem:@"Done" barImage:nil forViewNavCon:self offset:10];
    
    [rightDoneButton addTarget:self action:@selector(actionOnDone:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_btnSelectAll addTarget:self action:@selector(tapSelectAll:) forControlEvents:UIControlEventTouchUpInside];
    /*--------------------- Register Notifications ------------------------------*/
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(getSocialConatctUsersSucceed:)
                          name:kKeyNotificationUserSocialContactGetSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getSocialConatctUsersFailed:)
                          name:kKeyNotificationUserSocialContactGetFailed object:nil];
    
    
    [defaultCenter addObserver:self selector:@selector(getConatctConnectionRequestUserSuccess:) name:kKeyNotificationContactSendConnectionRequestSucceed object:nil];
    
    
    [defaultCenter addObserver:self selector:@selector(getConatctConnectionRequestUsersFailed:) name:kKeyNotificationContactSendConnectionRequestFailed object:nil];
    
    
    
}


- (SocialContactListViewController*)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SocialContactListViewController" bundle:nil];
    if (self) {
        // Custom initialization
        server = inServer;
        SocialContact = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getSocialContactApi];
}


-(void)getSocialContactApi{
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyoffset] = @0;
    params[kKeyNumRecords] = @100;
    if (_typeOfScreen == GmailContact){
        params[kKeyContactType] = kKeyGmailValue;
    }else if (_typeOfScreen == YahooContact){
         params[kKeyContactType] = kKeyYahooValue;
    }else if (_typeOfScreen == PhoneContact){
         params[kKeyContactType] = kKeyContacts;
    }else if (_typeOfScreen == Outlook){
         params[kKeyContactType] = kKeyOutlookValue;
    }
    [server makeAsychronousRequest:kKeyClassUserContactsGet inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}




#pragma mark Action Methods
- (void)actionOnBack:(UIButton *)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}
#pragma mark Action Methods
- (void)actionOnDone:(UIButton *)sender {
    NSMutableArray * arrSelected = [[NSMutableArray alloc] init];
    for (int i = 0; i < [SocialContact count] ; i++){
        NSMutableDictionary * contactDict = [SocialContact objectAtIndex:i];
        if ([contactDict[kKeyIsSelected] isEqual:@1]){
           //
            [arrSelected addObject:contactDict[kKeyId]];
        }
    }
    
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyContactID] = arrSelected;
    
    [server makeAsychronousRequest:kKeyClassContactSendConnectionRequest inParams:params isIndicatorRequired:YES inMethodType:kPOST];
    
    
}
-(void)tapOnCheckMark:(UIButton *)sender
{
    NSMutableDictionary * contactDict = [SocialContact objectAtIndex:sender.tag];
    if ([contactDict[kKeyIsSelected] isEqual:@0]){
     contactDict[kKeyIsSelected] =  @1;
     [sender setSelected:true];
    }else{
        contactDict[kKeyIsSelected] = @0;
        [sender setSelected:false];
    }
    NSInteger temp = 0;
    for (int i = 0; i < [SocialContact count]; i++){
         NSMutableDictionary * checkContactDict = [SocialContact objectAtIndex:i];
        if ([checkContactDict[kKeyIsSelected] isEqual:@1]){
            temp ++;
        }
    }
    if ([SocialContact count] == temp){
        [_btnSelectAll setSelected:true];
    }else{
        [_btnSelectAll setSelected:false];
    }
    
}

-(void)tapSelectAll:(UIButton *)sender{
    [sender setSelected: !sender.isSelected];
    for (int i = 0; i < [SocialContact count]; i++)
       {
           NSMutableDictionary  *contactDict = [SocialContact objectAtIndex:i];
           if (sender.isSelected == true){
            contactDict[kKeyIsSelected] = @1;
           }else{
             contactDict[kKeyIsSelected] = @0;
           }
           [SocialContact replaceObjectAtIndex:i withObject:contactDict];
       }
       [self.tableView reloadData];
}

//--------------------------------------- numberOfRowsInSection:---------------------------------------//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return SocialContact.count;
}


//------------------------------------ cellForRowAtIndexPath: --------------------------------------//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InviteNewContactCell *cell = (InviteNewContactCell* )[tableView dequeueReusableCellWithIdentifier:@"InviteNewContactCell" forIndexPath:indexPath];
    NSDictionary * contactDict = SocialContact[indexPath.row];
    if (contactDict[@"full_name"] != nil){
        cell.lblUserName.text = contactDict[@"full_name"];
    }
    if ([contactDict[@"is_registered"]  isEqual: @0]){
        [cell.imgSerendipityUser setHidden:true];
    }else{
        [cell.imgSerendipityUser setHidden:false];
    }
    if ([contactDict[kKeyIsSelected] isEqual:@0]){
        [cell.btnCheckMark setSelected:false];
    }else{
        [cell.btnCheckMark setSelected:true];
    }
    
    cell.btnCheckMark.tag = indexPath.row;
    [cell.btnCheckMark addTarget:self action:@selector(tapOnCheckMark:) forControlEvents:UIControlEventTouchUpInside];
    // Return
    return cell;
}
//------------------------------- heightForRowAtIndexPath -------------------------------------//

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


#pragma mark
#pragma mark - Notification Methods
#pragma mark
// --------------------------------------------------------------------------------
// getDeletedUsersSucceed:

- (void)getSocialConatctUsersSucceed:(NSNotification *)inNotify {
    NSDictionary *contactSuccess = [inNotify object];
    self->SocialContact = contactSuccess[@"list"];
    
    for (int i = 0; i < [SocialContact count]; i++)
    {
       // NSLog(@"%d",i);
        NSMutableDictionary  *contactDict = [SocialContact objectAtIndex:i];
        contactDict[kKeyIsSelected] = @1;
        [_btnSelectAll setSelected:true];
        [SocialContact replaceObjectAtIndex:i withObject:contactDict];
    }
    //self->totalRecord = contactSuccess[@"total"];
    // Reload table
    [self.tableView reloadData];
}
- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
//// --------------------------------------------------------------------------------
// getDeleteUsersFailed:

- (void)getSocialConatctUsersFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

- (void)getConatctConnectionRequestUserSuccess:(NSNotification *)inNotify {
    
    NSString* message = [inNotify object];
    [self showAlertController:message alertTitle:@"Serrendipity"];
}
- (void)getConatctConnectionRequestUsersFailed:(NSNotification *)inNotify {
   [APP_DELEGATE hideActivityIndicator];
}
-(void)showAlertController:(NSString *)message alertTitle :(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        [self.navigationController popToViewController:self.delegate animated:NO];
                                    }];
    
        
    
        [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
