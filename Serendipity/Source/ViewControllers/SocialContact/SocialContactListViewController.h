//
//  SocialContactListViewController.h
//  Serendipity
//
//  Created by Hitesh Surani on 20/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum {
    PhoneContact,
    GmailContact,
    YahooContact,
    Outlook
} ContactListType;


@interface SocialContactListViewController : UIViewController
{
    SRServerConnection *server;
    NSMutableArray *SocialContact;
    NSInteger *totalRecord;

}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak) id delegate;
@property   ContactListType typeOfScreen;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll;

#pragma mark
#pragma mark Init Method
#pragma mark

- (SocialContactListViewController*)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *) dict server:(SRServerConnection *)inServer;



@end

NS_ASSUME_NONNULL_END
