//
//  ParentViewController.h
//  Serendipity
//
//  Created by Hitesh Surani on 02/04/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface ParentViewController : UIViewController

-(CGFloat)getBottomSafeArea;
-(CGFloat)getTopSafeArea;
-(void) hideShowTopBar;
-(void) hsHideShowTopBar;

@end

NS_ASSUME_NONNULL_END
