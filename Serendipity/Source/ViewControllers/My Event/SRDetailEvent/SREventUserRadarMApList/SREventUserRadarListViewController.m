#import "SRModalClass.h"
#import "SRUserTabViewController.h"
#import "SREventUserRadarListViewController.h"
#import "CalloutAnnotationView.h"
#import "SRProfileTabViewController.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

@implementation SREventUserRadarListViewController
#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
            inUserArr:(NSArray *)userArr {
    // Call super
    self = [super initWithNibName:@"SREventUserRadarListViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
        nearbyUsers = userArr;

        dotsArr = [NSMutableArray array];
        dotsUserPicsArr = [[NSMutableArray alloc] init];

        userDataDict = [[NSMutableDictionary alloc] init];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(updateHeadingAngle:)
                              name:kKeyNotificationUpdateHeadingValue
                            object:nil];
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    // ScrollView
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;

    // customize slider 1
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }

    latestDistance = distancevalue;
    settingDistance = distancevalue;

    NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
    NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

    NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
    NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:@"miles" attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
    [muAtrStr appendAttributedString:atrStr1];
    [muAtrStr appendAttributedString:atrStr2];

    // Prepare radar view
    radarCircleView = [[SRRadarCircleView alloc] initWithFrame:CGRectMake(0, 0, self.radarViewContainer.bounds.size.width, self.radarViewContainer.bounds.size.height)];
    radarCircleView.points = CGPointMake(self.radarProfileImgView.frame.origin.x, self.radarProfileImgView.frame.origin.y);
    radarCircleView.layer.contentsScale = [UIScreen mainScreen].scale;
    self.radarViewContainer.layer.contentsScale = [UIScreen mainScreen].scale;
    [self.northLineView removeFromSuperview];
    [radarCircleView addSubview:self.northLineView];
    [self.radarViewContainer addSubview:radarCircleView];

    radarFocusView = [[SRRadarFocusView alloc] initWithFrame:CGRectMake(18, 18, 265, 265)];
    radarFocusView.layer.contentsScale = [UIScreen mainScreen].scale;
    radarFocusView.alpha = 0.68;
    [self.radarViewContainer addSubview:radarFocusView];

    // Bring view to front
    [self.view bringSubviewToFront:self.radarProfileImgView];
    [self.view bringSubviewToFront:self.northLineView];

    // Set profile pic
    self.radarProfileImgView.layer.cornerRadius = self.radarProfileImgView.frame.size.width / 2;
    self.radarProfileImgView.layer.masksToBounds = YES;

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDotTapped:)];
    [radarCircleView addGestureRecognizer:tapGestureRecognizer];

    // Add View
    swipeContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];

    swipeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, self.view.frame.size.height)];
    swipeView.alpha = 0.6;
    swipeView.backgroundColor = [UIColor colorWithRed:115 / 255.0 green:70 / 255.0 blue:14 / 255.0 alpha:0.6];

    lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 100, 18)];
    lblDistance.font = [UIFont fontWithName:kFontHelveticaBold size:12.0];
    lblDistance.textColor = [UIColor whiteColor];

    lblNoPPl = [[UILabel alloc] initWithFrame:CGRectMake(0, lblDistance.frame.origin.y, 100, 18)];
    lblNoPPl.textColor = [UIColor whiteColor];
    lblNoPPl.font = [UIFont fontWithName:kFontHelveticaMedium size:12.0];

    // Add the views to container view
    [swipeContainerView addSubview:swipeView];
    [swipeContainerView addSubview:lblDistance];
    [swipeContainerView addSubview:lblNoPPl];

    swipeContainerView.alpha = 0.7;
    swipeContainerView.hidden = YES;

    [self.view addSubview:swipeContainerView];

    //Add Tap Gesture on dotView
    UITapGestureRecognizer *viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapAction:)];
    self.view.userInteractionEnabled = YES;
    viewTapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:viewTapGesture];
}

// -----------------------------------------------------------------------------------------------
// viewDidAppear

- (void)viewDidAppear:(BOOL)animated {
    // Call super
    [super viewDidAppear:NO];

    // Event image
    if (server.eventDetailInfo[kKeyImageObject] != nil) {
        self.bckProfileImg.image = server.eventDetailInfo[kKeyImageObject];
    } else {
        if ([server.eventDetailInfo[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            // Get result
            UIImage *img = nil;

            if ([server.eventDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [server.eventDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
//                        img = [UIImage imageWithData:
//                               [NSData dataWithContentsOfURL:
//                                [NSURL URLWithString:imageUrl]]];

                    [self.bckProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"group-orange-50px.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (img) {
                                self.bckProfileImg.image = img;
                            }
                        });

//                            self.bckProfileImg.image = img;
                    }];


                }
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        if (img) {
//                            self.bckProfileImg.image = img;
//                        }
//                    });
            }
        } else {
            self.bckProfileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
        }
    }


    [self setDisplayForRadar];
}

#pragma mark
#pragma mark - Load Method
#pragma mark

// --------------------------------------------------------------------------------
// loadUsersInRadar:

- (void)loadUsersInRadar:(NSArray *)inArr {
    nearbyUsers = inArr;

    // Call the radar set up methods after some delay
    if (!isOnce) {
        isOnce = YES;
        [self displayUsersDotsOnRadar];
    }
}

#pragma mark
#pragma mark - Other Methods
#pragma mark
// -------------------------------------------------------------------------------------------------
// addViewsOnScrollView:

- (void)addViewsOnScrollView:(NSArray *)inArr {

    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }

    for (NSUInteger i = 0; i < [inArr count]; i++) {
        CGRect frame = CGRectMake(0, 11, 130, 200);;
        NSDictionary *userDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i > 0) {
            frame = CGRectMake(frame.origin.x + 126, 11, 130, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;

        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds
                                         byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(8.0, 8.0)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = ((CalloutAnnotationView *) annotationView).userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        ((CalloutAnnotationView *) annotationView).userProfileImage.layer.mask = maskLayer;

        if (userDict[kKeyProfileImage] != nil && [userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                }
            } else
                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else {
            ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
        }

        ((CalloutAnnotationView *) annotationView).lblProfileName.text = [userDict valueForKey:kKeyFirstName];
        if ([[userDict valueForKey:kKeyOccupation] length] <= 0) {
            ((CalloutAnnotationView *) annotationView).lblOccupation.text = @"None";
        } else {
            ((CalloutAnnotationView *) annotationView).lblOccupation.text = userDict[kKeyOccupation];
        }
        //Favourite
        if ([userDict[kKeyIsFavourite] boolValue] == true) {
            ((CalloutAnnotationView *) annotationView).favUserImage.hidden = FALSE;
        } else
            ((CalloutAnnotationView *) annotationView).favUserImage.hidden = TRUE;

        //Invisible
        if ([userDict[kKeyIsInvisible] boolValue] == true) {
            ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = FALSE;
        } else
            ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = TRUE;

        if (!isNullObject([userDict valueForKey:kKeyDistance])) {
            ((CalloutAnnotationView *) annotationView).lblDistance.text = [NSString stringWithFormat:@"%.1f %@", [[userDict valueForKey:kKeyDistance] floatValue], distanceUnit];

        } else {
            ((CalloutAnnotationView *) annotationView).lblDistance.text = [NSString stringWithFormat:@"0 miles"];
        }


        if ([[userDict valueForKey:kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            ((CalloutAnnotationView *) annotationView).lblDegree.text = [NSString stringWithFormat:@" %ld°", (long) degree];
        } else {
            ((CalloutAnnotationView *) annotationView).lblDegree.text = @" 0°";
        }

        // Add Shadow to btnPing
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOpacity:8.5];
        [((CalloutAnnotationView *) annotationView).pingButton addTarget:self action:@selector(pingButtonAction:) forControlEvents:UIControlEventTouchUpInside];

        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [((CalloutAnnotationView *) annotationView).userProfileImage addGestureRecognizer:annotationTapGesture];
        ((CalloutAnnotationView *) annotationView).userDict = userDict;

        //if users count is one
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }

        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else
            self.scrollView.scrollEnabled = NO;

        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
    }
}

- (void)pingButtonAction:(UIButton *)sender {

    //
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];

    BOOL flag = YES;//Check first if you allow to ping or not

    if ([[dictionary valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[dictionary valueForKey:kKeySetting]];

        if (![[setting valueForKey:kKeyallowPings] boolValue])
            flag = NO;
    }

    if (flag) {
        if ([calloutView.userDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyMediaImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }

        dictionary = (NSMutableDictionary *) [SRModalClass removeNullValuesFromDict:dictionary];
        // Set chat view
        SRChatViewController *objSRChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:dictionary];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
        [((UIViewController *) self.delegate).navigationController pushViewController:objSRChatView animated:YES];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
    }
}

// --------------------------------------------------------------------------------
// annotationTapAction:

- (void)annotationTapAction:(UITapGestureRecognizer *)sender {
    //badgeBtn.hidden = YES;
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    if ([calloutView.userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];

    // Show user public profile
    if ([[server.loggedInUserInfo valueForKey:kKeyId] isEqualToString:[dictionary valueForKey:kKeyId]]) {
        SRProfileTabViewController *profileTabCon = [[SRProfileTabViewController alloc] initWithNibName:nil bundle:nil];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
        [((UIViewController *) self.delegate).navigationController pushViewController:profileTabCon animated:YES];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
    } else {
        SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
        [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
        ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
    }


}


// -------------------------------------------------------------------------------
// makeRoundedImage:

- (UIImage *)makeRoundedImage:(UIImage *)image
                       radius:(float)radius; {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = 100 / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Return
    return roundedImage;
}


#pragma mark
#pragma mark - Radar Methods
#pragma mark

// -----------------------------------------------------------------------------------------------
// getHeadingForDirectionFromCoordinate:

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc {
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);

    float degree = radiandsToDegrees(atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng)));

    if (degree >= 0) {
        return -degree;
    } else {
        return -(360 + degree);
    }

    return degree;
}

//                                   -----------------------------------------------------------------------------------------------
// rotateArcsToHeading:

- (void)rotateArcsToHeading:(CGFloat)angle {
    // rotate the circle to heading degree
    radarCircleView.transform = CGAffineTransformMakeRotation(angle);

    // rotate all dots to opposite angle to keep the profile image straight up
    for (SRRadarDotView *dot in dotsArr) {
        dot.transform = CGAffineTransformMakeRotation(-angle);
    }
}

// ---------------------------------------------------------------------------------------
// setDisplayForRadar

- (void)setDisplayForRadar {
    // Spin the view of focus
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spin.duration = 1.5;
    spin.toValue = [NSNumber numberWithFloat:M_PI];
    spin.cumulative = YES;
    spin.removedOnCompletion = NO;
    spin.repeatCount = MAXFLOAT;
    [radarFocusView.layer addAnimation:spin forKey:@"spinRadarView"];
}

// -----------------------------------------------------------------------------------------------
// displayUsersDotsOnRadar

- (void)displayUsersDotsOnRadar {
    // empty the existing dots from radar
    for (SRRadarDotView *dot in dotsArr) {
        [dot removeFromSuperview];
    }
    [dotsArr removeAllObjects];

    // This method should be called after successful return of JSON array from your server-side service
    [self renderUsersOnRadar:nearbyUsers];
}

// -----------------------------------------------------------------------------------------------
// renderUsersOnRadar:

- (void)renderUsersOnRadar:(NSArray *)users {
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
            server.myLocation.coordinate.longitude};

    // The last user in the nearbyUsers list is the farthest
    float maxDistance = [[[users lastObject] valueForKey:kKeyDistance] floatValue];

    // Remove all objects from array
    [dotsUserPicsArr removeAllObjects];

    // Add users dots
    for (NSDictionary *user in users) {
        SRRadarDotView *dot = [[SRRadarDotView alloc] initWithFrame:CGRectMake(0, 0, 12.0, 12.0)];
        dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
        dot.userProfile = user;

        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:user];

        // If connected or family member show image with black dot
        if ([[updatedDict valueForKey:kKeyDegree] integerValue] == 1 || [[user valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-dot-30px.png"];
        } else {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];
        }

        NSDictionary *userLocDict;
        if ([user[kKeyLocation] isKindOfClass:[NSDictionary class]] && user[kKeyLocation] != nil) {
            userLocDict = user[kKeyLocation];
        }

        CLLocationCoordinate2D userLoc;
        float lat = 0.0;
        float lon = 0.0;
        if ([userLocDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            if ([userLocDict valueForKey:kKeyLattitude] != nil)
                lat = [[userLocDict valueForKey:kKeyLattitude] floatValue];

            if ([userLocDict valueForKey:kKeyRadarLong] != nil)
                lon = [[userLocDict valueForKey:kKeyRadarLong] floatValue];
        }
        userLoc.latitude = lat;
        userLoc.longitude = lon;

        float bearing = [self getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        dot.bearing = @(bearing);

        float d = 0.0;
        if ([user valueForKey:kKeyDistance] != [NSNull null]) {
            d = [[user valueForKey:kKeyDistance] floatValue];
        }

        float distance;

        if (maxDistance != 0) {
            if (37 + d >= 130) {
                distance = 130;
            } else {
                distance = (37 + d);
            }
        } else {
            distance = (double) 37;
        }
        dot.distance = @(distance);
        dot.initialDistance = @(distance); // relative distance
        dot.userDistance = @(d);
        dot.zoomEnabled = NO;
        dot.userInteractionEnabled = NO;

        float left = (float) (148 + distance * sin(degreesToRadians(-bearing)));
        float top = (float) (148 - distance * cos(degreesToRadians(-bearing)));

        [self rotateDot:dot fromBearing:currentDeviceBearing toBearing:bearing atDistance:distance];

        dot.frame = CGRectMake(left, top, 12.0, 12.0);
        dot.initialFrame = dot.frame;

        [radarCircleView addSubview:dot];
        [dotsArr addObject:dot];

        dot.transform = CGAffineTransformMakeRotation(-headingAngle);
    }
    // Start timer to detect collision with radar line and blink
    detectCollisionTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(detectCollisions:) userInfo:nil
                                                           repeats:YES];
}

#pragma mark
#pragma mark - Radar&Dot Collisions Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// detectCollisions:

- (void)detectCollisions:(NSTimer *)theTimer {
    float radarLineRotation = radiandsToDegrees([[radarFocusView.layer.presentationLayer valueForKeyPath:@"transform.rotation.z"] floatValue]);

    if (radarLineRotation >= 0) {
        radarLineRotation += 90;
    } else {
        if (radarLineRotation > -90) {
            radarLineRotation = 90 + radarLineRotation;
        } else
            radarLineRotation = 270 + (radarLineRotation + 180);
    }

    for (int i = 0; i < [dotsArr count]; i++) {
        SRRadarDotView *dot = dotsArr[i];
        float dotBearing = [dot.bearing floatValue];
        if (dotBearing < 0) {
            dotBearing = -dotBearing;
        }
        dotBearing = dotBearing - currentDeviceBearing;
        if (dotBearing < 0) {
            dotBearing = dotBearing + 360;
        }

        // Now get diffrence between dotbearing and radarline
        float diffrence;
        if (dotBearing > radarLineRotation) {
            diffrence = dotBearing - radarLineRotation;
        } else
            diffrence = radarLineRotation - dotBearing;

        if (diffrence <= 5) {
            [self pulse:dot];
        }
    }
}

// -----------------------------------------------------------------------------------------------
// pulse:

- (void)pulse:(SRRadarDotView *)dot {

    // Dot images
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.duration = 1.0;
    pulse.toValue = @1.0F;
    pulse.autoreverses = NO;
    dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    [dot.layer addAnimation:pulse forKey:@"pulse"];

    if (dot.userProfile[kKeyProfileImage] != nil && [dot.userProfile[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        CGRect frame = dot.initialFrame;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.frame = frame;

        frame = dot.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.imgView.frame = frame;

        dot.imgView.contentMode = UIViewContentModeScaleToFill;
        dot.imgView.layer.cornerRadius = dot.imgView.frame.size.width / 2.0;
        dot.imgView.clipsToBounds = YES;
        dot.isImageApplied = YES;

        NSDictionary *profileDict = dot.userProfile[kKeyProfileImage];
        NSString *imageName = profileDict[kKeyImageName];
        if ([imageName length] > 0) {
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

            [dot.imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        } else
            dot.imgView.image = [UIImage imageNamed:@"profile_menu.png"];
        [self performSelector:@selector(removeImageSetDefault:) withObject:dot afterDelay:0.5];
    }
}

// -----------------------------------------------------------------------------------------------
// removeImageSetDefault:

- (void)removeImageSetDefault:(SRRadarDotView *)inDotView {
    if (inDotView.isImageApplied) {
        // Remove the animation
        [inDotView.layer removeAllAnimations];

        CGRect frame = inDotView.initialFrame;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.frame = frame;

        frame = inDotView.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.imgView.frame = frame;

        inDotView.imgView.contentMode = UIViewContentModeScaleAspectFill;
        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:inDotView.userProfile];

        if ([[updatedDict valueForKey:kKeyDegree] integerValue] == 1 || [[updatedDict valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-dot-30px.png"];
        } else
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];

        inDotView.isImageApplied = NO;
        inDotView.imgView.clipsToBounds = NO;
    }
}

#pragma mark
#pragma mark - Radar Dot Translate Methods
#pragma mark

// -----------------------------------------------------------------------------------------------
// rotateDot: fromBearing: atDistance:

- (void)rotateDot:(SRRadarDotView *)dot fromBearing:(CGFloat)fromDegrees toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CGMutablePathRef path = CGPathCreateMutable();

    CGPathAddArc(path, nil, 150, 150, distance, degreesToRadians(fromDegrees), degreesToRadians(degrees), YES);

    CAKeyframeAnimation *theAnimation;

    // Animation object for the key path
    theAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    theAnimation.path = path;
    CGPathRelease(path);

    // Set the animation properties
    theAnimation.duration = 3;
    theAnimation.removedOnCompletion = NO;
    theAnimation.repeatCount = 0;
    theAnimation.autoreverses = NO;
    theAnimation.fillMode = kCAFillModeBackwards;
    theAnimation.cumulative = YES;

    CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);
    dot.layer.position = newPosition;
    [dot.layer addAnimation:theAnimation forKey:@"rotateDot"];
}

// -----------------------------------------------------------------------------------------------
// translateDot:

- (void)translateDot:(SRRadarDotView *)dot toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];

    [animation setFromValue:[NSValue valueWithCGPoint:[[dot.layer.presentationLayer valueForKey:@"position"] CGPointValue]]];

    //CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);

    float left = (float) (148 + distance * sin(degreesToRadians(-degrees)));
    float top = (float) (148 - distance * cos(degreesToRadians(-degrees)));
    CGPoint newPosition = CGPointMake(left, top);
    [animation setToValue:[NSValue valueWithCGPoint:newPosition]];

    [animation setDuration:0.3f];
    animation.fillMode = kCAFillModeBackwards;
    animation.autoreverses = NO;
    animation.repeatCount = 0;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];

    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [alphaAnimation setDuration:0.5f];
    alphaAnimation.fillMode = kCAFillModeBackwards;
    alphaAnimation.autoreverses = NO;
    alphaAnimation.repeatCount = 0;
    alphaAnimation.removedOnCompletion = NO;
    alphaAnimation.cumulative = YES;
    alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    /*if (distance > 130) {
        [alphaAnimation setToValue:[NSNumber numberWithFloat:0.0f]];
       }
       else {
        [alphaAnimation setToValue:[NSNumber numberWithFloat:1.0f]];
       }*/

    //[dot.layer addAnimation:alphaAnimation forKey:@"alphaDot"];
    [dot.layer addAnimation:animation forKey:@"translateDot"];
}


#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// updateHeadingAngle:

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    currentDeviceBearing = heading;
    [self rotateArcsToHeading:headingAngle];
}

#pragma mark
#pragma mark - tapGesture Method
#pragma mark

- (void)onDotTapped:(UITapGestureRecognizer *)recognizer {
    UIView *circleView = recognizer.view;
    CGPoint point = [recognizer locationInView:circleView];
    // The for loop is to find out multiple dots in vicinity
    // you may define a NSMutableArray before the for loop and
    // get the group of dots together
    NSMutableArray *tappedUsers = [NSMutableArray array];
    for (SRRadarDotView *d in dotsArr) {

        if ([d.layer.presentationLayer hitTest:point] != nil) {
            // you can get the list of tapped user(s if more than one users are close enough)
            // use this variable outside of for loop to get list of users
            [tappedUsers addObject:d.userProfile];

        }
    }
    // use tappedUsers variable according to your app logic
    if (tappedUsers.count > 0) {
        [self addViewsOnScrollView:tappedUsers];
    } else {
        [self.scrollView setHidden:YES];
        [self.toolTipScrollViewImg setHidden:YES];
    }
}
// --------------------------------------------------------------------------------
// viewTapAction:

- (void)viewTapAction:(UITapGestureRecognizer *)sender {
    [self.toolTipScrollViewImg setHidden:YES];
    [self.scrollView setHidden:YES];
}

@end
