#import "SRModalClass.h"
#import "SwipeableTableViewCell.h"
#import "SRConnectionCellView.h"
#import "SRUserTabViewController.h"
#import "SREventUserListView.h"
#import "SRMapViewController.h"
#import "SRProfileTabViewController.h"

@interface EventMemberDistanceClass : NSObject

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *distance;
@property(nonatomic, strong) NSString *dataId;
@property(strong, nonatomic) CLLocation *myLocation;

@end

@implementation EventMemberDistanceClass


@end

@interface SREventUserListView ()

@property(nonatomic, strong) NSOperationQueue *eventMemberDistanceQueue;

@end

@implementation SREventUserListView


#pragma mark private methods

- (void)callDistanceAPI:(NSDictionary *)userDict {
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:_server.myLocation.coordinate.latitude longitude:_server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];


    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];
//    NSLog(@"urlStr::%@", urlStr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = rows[0];
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];

                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       double convertDist = [distanceWithoutCommas doubleValue];
                                       if ([self->distanceUnit isEqualToString:kkeyUnitKilometers]) {

                                           //meters to killometer
                                           convertDist = convertDist / 1000;
                                       } else {
                                           //meters to miles
                                           convertDist = (convertDist * 0.000621371192);
                                       }
                                       EventMemberDistanceClass *obj = [[EventMemberDistanceClass alloc] init];
                                       obj.dataId = userDict[kKeyId];
                                       NSDictionary *locationDict = userDict[kKeyLocation];
                                       obj.lat = locationDict[kKeyLattitude];
                                       obj.longitude = locationDict[kKeyRadarLong];
                                       obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, self->distanceUnit];
                                       obj.myLocation = self->_server.myLocation;

                                       //Live Address
                                       NSArray *destinationArray = json[@"destination_addresses"];

                         //              NSLog(@"Address : %@", [NSString stringWithFormat:@"in %@", destinationArray[0]]);

                                       NSUInteger indexOfUserObject = [self->usersListArr indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {


                                           if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                               return YES;
                                           } else {

                                               return NO;
                                           }

                                       }];

                            //           NSLog(@"%lu", (unsigned long) indexOfUserObject);
                                       NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                       NSArray *tempArr = [self->eventTempDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                       obj.address = [NSString stringWithFormat:@"in %@", destinationArray[0]];
                                       if (convertDist <= 0) {
                                           obj.address = @"";
                                       }

                                       if (tempArr && tempArr.count) {

                                           NSUInteger indexOfObject = [self->eventTempDataArray indexOfObjectPassingTest:^BOOL(EventMemberDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                                               if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                                   return YES;
                                               } else {

                                                   return NO;
                                               }

                                           }];


                                           self->eventTempDataArray[indexOfObject] = obj;

                                       } else {

                                           [self->eventTempDataArray addObject:obj];

                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^{

                                           if (indexOfUserObject != NSNotFound && indexOfUserObject < self->usersListArr.count) {
                                               [self->_objTblView reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                           }
                                       });
                                   }

                               }
                           }];

}


- (void)getEventMembersDistance {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (!eventTempDataArray.count) {
        [arr addObjectsFromArray:usersListArr];
    }

    for (int i = 0; i < arr.count; i++) {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];


        if (_server.myLocation != nil && [userDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            NSBlockOperation *op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled) {

                    [self callDistanceAPI:userDict];
                } else {

                 //   NSLog(@"Operation Not canceled");
                }
            }];
            [_eventMemberDistanceQueue addOperation:op];

        }

    }

}


#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SREventUserListView" owner:self options:nil];
        self = nibArray[0];

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(imagesGotDownloaded:)
                              name:kKeyNotificationEventUserRadarImageDownloaded
                            object:nil];
    }

    // Return
    return self;
}

//-----------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Load Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// loadUsers:

- (void)loadUsers:(NSArray *)inArr {
    eventTempDataArray = [[NSMutableArray alloc] init];
    _eventMemberDistanceQueue = [[NSOperationQueue alloc] init];
    //Get and Set distance measurement unit
    if ([[[_server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    usersListArr = inArr;

    // Group image
    if (self.server.groupDetailInfo[kKeyImageObject] != nil) {
        self.profileImg.image = self.server.groupDetailInfo[kKeyImageObject];
    } else {
        if ([self.server.groupDetailInfo[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            // Get result
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                UIImage *img = nil;

                if ([self.server.groupDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                    NSString *imageName = [self.server.groupDetailInfo[kKeyMediaImage] objectForKey:kKeyImageName];
                    if ([imageName length] > 0) {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                        img = [UIImage imageWithData:
                                [NSData dataWithContentsOfURL:
                                        [NSURL URLWithString:imageUrl]]];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (img) {
                            self.profileImg.image = img;
                        }
                    });
                }
            });
        } else {
            self.profileImg.image = [UIImage imageNamed:@"group-orange-50px.png"];
        }
    }

    // reload data
    [self.objTblView reloadData];

}

#pragma mark
#pragma mark Action Methods
#pragma mark
//
// ---------------------------------------------------------------------------------------
// actionOnProfileCellBtn:

- (void)actionOnProfileCellBtn:(UIButton *)sender {
    // Get profile
    NSDictionary *userDict = usersListArr[sender.tag];
    self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:userDict];

    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
    [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
}

//
// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTappedCaptured:(UIButton *)sender {
    NSMutableDictionary *userDict = usersListArr[sender.tag];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:userDict server:self.server];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
    [((UIViewController *) self.delegate).navigationController pushViewController:dropPin animated:YES];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
}

// ---------------------------------------------------------------------------------------
// degreeBtnSelected:
- (void)degreeBtnSelected:(UIButton *)sender {
    // Get profile
    NSDictionary *userDict = usersListArr[sender.tag];
    self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:userDict];


    // Show user public profile
    SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:@"SRUserTabViewController" bundle:nil profileData:nil server:self.server];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
    [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
    ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
}


#pragma mark
#pragma mark Tap Gesture
#pragma mark

// ---------------------------------------------------------------------------------------
// gestureRecognizer:
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]]) {      //change it to your condition
        return NO;
    }
    return YES;
}

// ---------------------------------------------------------------------------------------
// singleTapGestureCaptured:

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)sender {
    SwipeableTableViewCell *cell = (SwipeableTableViewCell *) sender.view;
    if (CGPointEqualToPoint(cell.scrollView.contentOffset, CGPointZero)) {
        // Get profile
        NSDictionary *userDict = usersListArr[cell.tag];
        // Get profile
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:userDict];
        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDict[kKeyProfileImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }
        self.server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:dictionary];

        // Show user public profile
        if ([[_server.loggedInUserInfo valueForKey:kKeyId] isEqualToString:[dictionary valueForKey:kKeyId]]) {
            SRProfileTabViewController *profileTabCon = [[SRProfileTabViewController alloc] initWithNibName:nil bundle:nil];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
            [((UIViewController *) self.delegate).navigationController pushViewController:profileTabCon animated:YES];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
        } else {
            SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = YES;
            [((UIViewController *) self.delegate).navigationController pushViewController:tabCon animated:YES];
            ((UIViewController *) self.delegate).hidesBottomBarWhenPushed = NO;
        }
    } else
        [cell.scrollView setContentOffset:CGPointZero animated:YES];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [usersListArr count];
}

// ----------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// ----------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"connectionCell";
    NSDictionary *userDict = usersListArr[indexPath.section];
    userDict = [SRModalClass removeNullValuesFromDict:userDict];

    // For the purposes of this demo, just return a random cell.
    SwipeableTableViewCell *cell;
    if (!cell) {
        cell = [[SwipeableTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.backgroundColor = [UIColor clearColor];

        // Tap Gesture
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        singleTap.delegate = self;
        [cell addGestureRecognizer:singleTap];
        cell.tag = indexPath.section;
        singleTap.cancelsTouchesInView = NO;

        // Assign values
        SRConnectionCellView *contentView = (SRConnectionCellView *) cell.scrollViewContentView;
        contentView.lblName.text = [NSString stringWithFormat:@"%@ %@", userDict[kKeyFirstName], userDict[kKeyLastName]];

        if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            if ([userDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyProfileImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [contentView.imgProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            contentView.imgProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];

        // Occupation
        if ([userDict[kKeyOccupation] length] > 0)
            contentView.lblProfession.text = userDict[kKeyOccupation];
        else
            contentView.lblProfession.text = NSLocalizedString(@"lbl.occupation_not_available.txt", @"");

        contentView.lblProfession.lineBreakMode = NSLineBreakByTruncatingTail;
        if ([contentView.lblProfession.text isEqualToString:@""]) {
            contentView.lblName.frame = CGRectMake(contentView.lblName.frame.origin.x, contentView.lblName.frame.origin.y + 15, contentView.lblName.frame.size.width, contentView.lblName.frame.size.height);
        }
        //Favourite
        contentView.favImg.hidden = TRUE;
        //Invisible
        contentView.btnInvisible.hidden = TRUE;
        contentView.blackOverlay.hidden = TRUE;

//        // Location
//		if ([[userDict objectForKey:kKeyAddress]length] > 0)
//			contentView.lblAddress.text = [NSString stringWithFormat:@"in %@", [userDict objectForKey:kKeyAddress]];
//		else
//			contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");

        //For Family show (You can t date Family  member so hide date icon)

//        if ([[userDict objectForKey:kKeyFamily]isKindOfClass:[NSDictionary class]]) {
//            // NSDictionary *familyDict = [userDict objectForKey:kKeyFamily];
//            contentView.lblDating.hidden = YES;
//            contentView.imgDating.hidden = YES;
//            contentView.lblFamily.hidden = NO;
//        }else
        // Dating
        if ([userDict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *settingDict = userDict[kKeySetting];
            contentView.lblFamily.hidden = YES;

            if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
                contentView.lblDating.hidden = NO;
                contentView.imgDating.hidden = NO;
                contentView.imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
            } else {
                contentView.lblDating.hidden = YES;
                contentView.imgDating.hidden = YES;
            }
        } else {
            contentView.lblDating.hidden = YES;
            contentView.imgDating.hidden = YES;
            contentView.lblFamily.hidden = YES;
        }

        // Degree
        [contentView.btnDegree addTarget:self action:@selector(degreeBtnSelected:) forControlEvents:UIControlEventTouchUpInside];
        contentView.btnDegree.tag = indexPath.section;
        contentView.btnDegree.layer.cornerRadius = contentView.btnDegree.frame.size.width / 2.0;
        contentView.btnDegree.layer.masksToBounds = YES;
        if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userDict[kKeyDegree] integerValue];
            [contentView.btnDegree setTitle:[NSString stringWithFormat:@" %ld°", (long) degree] forState:UIControlStateNormal];
        } else {
            [contentView.btnDegree setTitle:@" 6°" forState:UIControlStateNormal];
        }
        //TODO:
        CLLocationCoordinate2D myLoc = {self.server.myLocation.coordinate.latitude,
                self.server.myLocation.coordinate.longitude};

        NSDictionary *userLocDict;
        if ([userDict[kKeyLocation] isKindOfClass:[NSDictionary class]] && userDict[kKeyLocation] != nil) {
            userLocDict = userDict[kKeyLocation];
        }

        CLLocationCoordinate2D userLoc;
        float lat = 0.0;
        if ([userLocDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            if ([userLocDict valueForKey:kKeyLattitude] != nil)
                lat = [[userLocDict valueForKey:kKeyLattitude] floatValue];
        }

        float lon = 0.0;
        if ([userLocDict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            if ([userLocDict valueForKey:kKeyRadarLong] != nil)
                lon = [[userLocDict valueForKey:kKeyRadarLong] floatValue];
        }


        userLoc.latitude = lat;
        userLoc.longitude = lon;

        [contentView.btnCompass addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
        contentView.btnCompass.tag = indexPath.section;

        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            contentView.lblCompassDirection.text = @"N";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            contentView.lblCompassDirection.text = @"E";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            contentView.lblCompassDirection.text = @"S";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            contentView.lblCompassDirection.text = @"W";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            contentView.lblCompassDirection.text = @"NE";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            contentView.lblCompassDirection.text = @"NW";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            contentView.lblCompassDirection.text = @"SE";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            contentView.lblCompassDirection.text = @"SW";
            [contentView.btnCompass setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }

//        double distance = 0.0;
//        if ([userDict isKindOfClass:[NSDictionary class]] && [userDict valueForKey:kKeyDistance] !=[NSNull null]) {
//            distance = [[userDict valueForKey:kKeyDistance]doubleValue];
//        }
//
//		contentView.lblDistance.text = [NSString stringWithFormat:@"%.1f %@", distance, distanceUnit];
        contentView.btnProfileView.tag = indexPath.section;

        // Adjust green dot label appearence
        contentView.lblOnline.hidden = YES;


//        // Location
//        NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@",[userDict objectForKey:kKeyId]];
//        NSArray *tempArr = [eventTempDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
//        
//        if(tempArr && tempArr.count)
//        {
//            EventMemberDistanceClass *obj = [tempArr firstObject];
//            
//            if(![[NSString stringWithFormat:@"%@",obj.distance] isEqualToString:@""])
//            {
//                contentView.lblDistance.text = obj.distance;
//                
//            }
//            
//            if(![[NSString stringWithFormat:@"%@",obj.address] isEqualToString:@""])
//            {
//                
//                contentView.lblAddress.text = obj.address;
//                [contentView.lblAddress sizeToFit];
//                [contentView.btnCompass setUserInteractionEnabled:YES];
//                
//            }
//            else{
//                
//                contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
//            }
//            
//        }
//        else{
//            
//            contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
//        }

        // Location
        if (!isNullObject([userDict[kKeyLocation] objectForKey:kKeyLiveAddress])) {
            contentView.lblAddress.text = [userDict[kKeyLocation] objectForKey:kKeyLiveAddress];
        } else
            contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        [contentView.lblAddress sizeToFit];

        contentView.lblDistance.text = [NSString stringWithFormat:@"%.2f"@" %@", [userDict[kKeyDistance] floatValue], distanceUnit];


        if ([contentView.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {
            contentView.lblDistance.text = [NSString stringWithFormat:@"0.0 %@", distanceUnit];
        }
        if ([contentView.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {
            contentView.lblDistance.text = [NSString stringWithFormat:@"0.0 %@", distanceUnit];
        }

    }

    // Return
    return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 2.0;
    if (section == 0) {
        height = 0.0;
    }

    // Return
    return height;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }

    // Return
    return headerView;
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// imagesGotDownloaded:

- (void)imagesGotDownloaded:(NSNotification *)inNotify {
    // Remove all objects
    dotsUserPicsArr = [inNotify object];

    // Reload table view
    [self.objTblView reloadData];
}


@end
