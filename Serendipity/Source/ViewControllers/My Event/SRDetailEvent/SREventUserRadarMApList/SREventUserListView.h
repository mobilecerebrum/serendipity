#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "SRServerConnection.h"

@interface SREventUserListView : UIView <UIGestureRecognizerDelegate>
{
    // Instance variable
    NSArray *usersListArr;
    NSArray *dotsUserPicsArr;
    NSMutableArray *eventTempDataArray;
    
    float headingAngle;
    NSString *distanceUnit;
}

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *profileImg;
@property (nonatomic, strong) IBOutlet UITableView *objTblView;
@property (nonatomic, strong) SRServerConnection *server;

// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (void)loadUsers:(NSArray *)inArr;

@end
