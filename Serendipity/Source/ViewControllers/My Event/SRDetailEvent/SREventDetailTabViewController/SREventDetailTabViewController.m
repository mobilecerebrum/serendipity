//
//  SREventDetailTabViewController.m
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SREventDetailTabViewController.h"
#import "SRModalClass.h"
#import "SREventInfoViewController.h"
#import "SRPingsViewController.h"
#import "SREventDiscoveryViewController.h"
#import "SRNewEventViewController.h"
#import "SRChatViewController.h"

@interface SREventDetailTabViewController ()

@end

@implementation SREventDetailTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    // Call super
    self = [super initWithNibName:@"SREventDetailTabViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(ShowGroupSucceed:)
                              name:kKeyNotificationCreateGroupSucceed object:nil];
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Set delegate
    [self setDelegate:self];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    // Check if user has privillage to edit event
    NSDictionary *eventDict = (APP_DELEGATE).server.eventDetailInfo;
    NSArray *members = eventDict[kKeyEvent_Attendees];

    BOOL isAdmin = NO;
    for (NSUInteger i = 0; i < [members count] && !isAdmin; i++) {
        NSDictionary *dict = members[i];
        if ([dict[kKeyIsAdmin] isEqualToString:@"1"] && [eventDict[kKeyCreatedBy] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
            isAdmin = YES;
        }
    }

    rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"edit_profile_white.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(editEventBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    NSMutableArray *memberIdsArr = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < [members count]; i++) {
        NSDictionary *dict = members[i];
        [memberIdsArr addObject:dict[kKeyUserID]];
        if ([memberIdsArr containsObject:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
            if ([dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] && [dict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                if ([[eventDict valueForKey:kKeyIs_Public] boolValue]) {
                    rightButton.hidden = NO;
                } else if ([dict[kKeyIsAdmin] isEqualToString:@"1"]) {
                    rightButton.hidden = NO;
                } else
                    rightButton.hidden = YES;
            }
        } else
            rightButton.hidden = YES;
    }
    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];

    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;

    [[UITabBarItem appearance]                      setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f],
            NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance]              setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f],
            NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];

    if ([[eventDict valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
        // Set SREventdetail tab
        SREventInfoViewController *eventTab = [[SREventInfoViewController alloc] initWithNibName:nil bundle:nil eventInfo:(APP_DELEGATE).server.eventDetailInfo server:(APP_DELEGATE).server];
        UIImage *tabImage = [UIImage imageNamed:@"tabbar-event.png"];
        UITabBarItem *objUserTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.eventInfoTab.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-event-active.png"]];
        [objUserTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        eventTab.tabBarItem = objUserTabItem;
        self.viewControllers = @[eventTab];

    } else {
        // Set SREventdetail tab
        SREventInfoViewController *eventTab = [[SREventInfoViewController alloc] initWithNibName:nil bundle:nil eventInfo:(APP_DELEGATE).server.eventDetailInfo server:(APP_DELEGATE).server];
        UIImage *tabImage = [UIImage imageNamed:@"tabbar-event.png"];
        UITabBarItem *objUserTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.eventInfoTab.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-event-active.png"]];
        [objUserTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        eventTab.tabBarItem = objUserTabItem;

        //  Set SREventDiscovery tab
        SREventDiscoveryViewController *discoveryTab = [[SREventDiscoveryViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
        tabImage = [UIImage imageNamed:@"tabbar-discovery.png"];
        UITabBarItem *objSRSOPTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userDiscovery.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-discovery-active.png"]];
        [objSRSOPTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        discoveryTab.tabBarItem = objSRSOPTabItem;

#warning added _to event id for detect only eventgroup chat also changes event attendee to group member name
        NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithDictionary:(APP_DELEGATE).server.eventDetailInfo];
        [eventInfo setValue:[NSString stringWithFormat:@"%@_", [eventInfo valueForKey:kKeyId]] forKey:kKeyId];
        [eventInfo setValue:[NSArray arrayWithArray:[eventInfo valueForKey:kKeyEvent_Attendees]] forKey:kKeyGroupMembers];
        [eventInfo removeObjectForKey:kKeyEvent_Attendees];

        // Set Pings tab

        SRChatViewController *objSRPingsView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:eventInfo];
        tabImage = [UIImage imageNamed:@"tabbar-ping.png"];
        UITabBarItem *SRPingsViewTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userPing.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-ping-active.png"]];
        [SRPingsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        objSRPingsView.tabBarItem = SRPingsViewTabItem;

        self.viewControllers = @[eventTab, discoveryTab, objSRPingsView];
        self.selectedIndex = 0;
        self.tabBar.unselectedItemTintColor = appTintColor;
        //Set Enable or Disable Chat option
        NSArray *eventMembersDict = [(APP_DELEGATE).server.eventDetailInfo valueForKey:kKeyEvent_Attendees];
        BOOL isMember = NO;
        if ([[(APP_DELEGATE).server.eventDetailInfo valueForKey:kKeyUserID] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:kKeyId]]) {
            isMember = YES;
        } else {
            for (NSDictionary *dict in eventMembersDict) {

                if ([[dict valueForKey:kKeyUserID] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:kKeyId]]) {
                    isMember = YES;
                }
            }
        }
        if (!isMember) {
            [[[self tabBar] items][2] setEnabled:FALSE];
        }
        if ([[eventInfo valueForKey:@"is_converted"]  isEqual: @"0"]) {
            [[[self tabBar] items][2] setEnabled:FALSE];
        }

    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    // Title
    if ((APP_DELEGATE).server.eventDetailInfo != nil) {
        NSString *fullName = [NSString stringWithFormat:@"%@", [(APP_DELEGATE).server.eventDetailInfo valueForKey:kKeyName]];
        [SRModalClass setNavTitle:fullName forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

// ---------------------------------------------------------------------------------------
// plusBtnAction:

- (void)editEventBtnAction:(id)sender {

    SRNewEventViewController *objSRNewEventView = [[SRNewEventViewController alloc] initWithNibName:@"SRNewEventViewController" bundle:nil dict:(APP_DELEGATE).server.eventDetailInfo server:(APP_DELEGATE).server];
    objSRNewEventView.delegate = self;
    [self.navigationController pushViewController:objSRNewEventView animated:NO];

}

#pragma mark
#pragma mark TabBarController Delegate
#pragma mark

// ----------------------------------------------------------------------------
// didSelectViewController:

- (void)tabBarController:(UITabBarController *)inTabBarController didSelectViewController:(UIViewController *)viewController {
    if (inTabBarController.selectedIndex == 1 || inTabBarController.selectedIndex == 2) {
        if ([SRModalClass isEventExist:(APP_DELEGATE).server.eventDetailInfo[kKeyId]]) {
            rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"calendar-check_25.png"] forViewNavCon:self offset:-23];
        } else {
            rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"calendar-plus_25.png"] forViewNavCon:self offset:-23];
        }
        rightButton.userInteractionEnabled = NO;
    } else {
        NSDictionary *eventDict = (APP_DELEGATE).server.eventDetailInfo;
        NSArray *members = eventDict[kKeyEvent_Attendees];

        BOOL isAdmin = NO;
        for (NSUInteger i = 0; i < [members count] && !isAdmin; i++) {
            NSDictionary *dict = members[i];
            if ([dict[kKeyIsAdmin] isEqualToString:@"1"] && [eventDict[kKeyCreatedBy] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
                isAdmin = YES;
            }
        }


        [rightButton setImage:[UIImage imageNamed:@"edit_profile_white.png"] forState:UIControlStateNormal];
        for (NSUInteger i = 0; i < [members count]; i++) {
            NSDictionary *dict = members[i];
            if ([dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] && [dict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                if ([[eventDict valueForKey:kKeyIs_Public] boolValue]) {
                    rightButton.hidden = NO;
                } else if ([dict[kKeyIsAdmin] isEqualToString:@"1"]) {
                    rightButton.hidden = NO;
                } else
                    rightButton.hidden = YES;
            } else
                rightButton.hidden = YES;
        }
    }
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// ShowGroupSucceed:

- (void)ShowGroupSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    id object = [inNotify object];
    if ([object isKindOfClass:[NSString class]]) {
        [self.navigationController popViewControllerAnimated:NO];
    }
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
}


@end
