//
//  SREventDetailTabViewController.h
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SREventDetailTabViewController : UITabBarController<UITabBarControllerDelegate>
{
    // Instance
    UIButton *rightButton;
}

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil;
@end
