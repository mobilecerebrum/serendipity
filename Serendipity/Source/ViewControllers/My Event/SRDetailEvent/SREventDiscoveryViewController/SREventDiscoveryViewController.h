//
//  SREventDiscoveryViewController.h
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHContextMenuView.h"
#import "SRServerConnection.h"
#import "SREventUserRadarListViewController.h"
#import "SREventUserListView.h"
#import "SREventUserMapView.h"
@interface SREventDiscoveryViewController : ParentViewController<CLLocationManagerDelegate,GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>
{
    // Instance Variable
    NSMutableArray *eventUsers;
    
    SREventUserRadarListViewController *radarCon;
    SREventUserListView *listView;
    SREventUserMapView *mapView;
    
    // Flag
    BOOL isOnce;
    
    // Server
    SRServerConnection *server;
    NSString *distanceUnit;
}

// Properties
@property (nonatomic, strong) IBOutlet UIButton *btnAnimate;

//For Google Matrix API call and response
@property (nonatomic,strong) __block NSMutableArray *matrixRequestArray;
@property (nonatomic,strong) __block NSMutableArray *matrixResponseArray;
@property (nonatomic,copy) void (^handler)(NSURLResponse *response, NSData *data, NSError *error);


#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)btnAnimateClick:(id)sender;

@end
