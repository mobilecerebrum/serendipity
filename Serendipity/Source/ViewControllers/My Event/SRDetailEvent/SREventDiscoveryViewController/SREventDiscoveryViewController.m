//
//  SREventDiscoveryViewController.m
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SREventDiscoveryViewController.h"
#import "SRGroupTabViewController.h"
#import "SRModalClass.h"
#import "RadarDistanceClass.h"

@interface SREventDiscoveryViewController ()

@end

@implementation SREventDiscoveryViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SREventDiscoveryViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        eventUsers = [[NSMutableArray alloc] init];
    }

    //return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    // Call Super
    [super viewDidLoad];

    _matrixRequestArray = [[NSMutableArray alloc] init];
    _matrixResponseArray = [[NSMutableArray alloc] init];

    // Set Up For Animation Button on RadarDiscovery
    GHContextMenuView *overlay = [[GHContextMenuView alloc] init];
    overlay.dataSource = self;
    overlay.delegate = self;
    overlay.selectedIndex = 0;

    // To present overlay on menu btn click
    UITapGestureRecognizer *_tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:overlay action:@selector(tapBtnDetected:)];
    [self.btnAnimate addGestureRecognizer:_tapGestureRecognizer];
}

// -----------------------------------------------------------------------------------------------
// viewDidAppear

- (void)viewDidAppear:(BOOL)animated {
    // Call super
    [super viewDidAppear:NO];

    if (!isOnce) {
        isOnce = YES;

        // Views
        radarCon = [[SREventUserRadarListViewController alloc] initWithNibName:nil bundle:nil server:server inUserArr:eventUsers];
        radarCon.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        radarCon.delegate = self;
        [self.view addSubview:radarCon.view];
        [self.view bringSubviewToFront:self.btnAnimate];

        // Filter the users and alloc the views
        for (NSDictionary *userDict in server.eventDetailInfo[kKeyEvent_Attendees]) {
            NSDictionary *userDetails;
            if (userDict[kKeyFirstName] != nil) {
                userDetails = userDict;
            } else if (userDict[kKeyUser] != nil) {
                userDetails = userDict[kKeyUser];
            } else {
                NSString *userId = userDict[kKeyUserID];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
                NSArray *filterArr = [server.eventUsersArr filteredArrayUsingPredicate:predicate];
                if ([filterArr count] > 0) {
                    userDetails = filterArr[0];
                } else {
                    userDetails = [NSMutableDictionary dictionary];
                }
            }
            // Add dict to mutable array
            [eventUsers addObject:userDetails];
        }

        // Sorting
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        NSArray *sortedArr = [eventUsers sortedArrayUsingDescriptors:sortDescriptors];

        [eventUsers removeAllObjects];
        [eventUsers addObjectsFromArray:sortedArr];

        [self getSavedDistance];

//        // Load radar users
//        [radarCon loadUsersInRadar:eventUsers];
//        
//        listView = [[SREventUserListView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        listView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        [self.view addSubview:listView];
//        listView.server = server;
//        listView.delegate = self;
//        [listView loadUsers:eventUsers];
//        listView.hidden = YES;
//        
//        mapView = [[SREventUserMapView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        [self.view addSubview:mapView];
//        mapView.server = server;
//        mapView.delegate = self;
//        [mapView loadUsers:eventUsers];
//        mapView.hidden = YES;
    }

    [self.view bringSubviewToFront:self.btnAnimate];

}

#pragma mark - Distance Calculation Methods

- (void)getSavedDistance {

    NSMutableArray *arr = [[NSMutableArray alloc] init];

    if ((APP_DELEGATE).oldEventUserArray && (APP_DELEGATE).oldEventUserArray.count && eventUsers.count) {

        for (NSDictionary *userDict in eventUsers) {

            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

            NSArray *tempArr = [(APP_DELEGATE).oldEventUserArray filteredArrayUsingPredicate:predicateForDistanceObj];

            if (tempArr && tempArr.count) {

                RadarDistanceClass *distanceObj = [tempArr firstObject];

                NSDictionary *currentLocationDict = userDict[kKeyLocation];
                NSDictionary *previousLocationDict = distanceObj.userDict[kKeyLocation];

                BOOL isBLocationChanged = NO;
                BOOL isALocationChanged = NO;

                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *previousUpdateDate = [dateFormatter dateFromString:previousLocationDict[kKeyLastSeenDate]];
                NSDate *currentDate = [dateFormatter dateFromString:currentLocationDict[kKeyLastSeenDate]];

                if ([previousUpdateDate compare:currentDate] != NSOrderedSame) {
                    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:[[currentLocationDict valueForKey:kKeyLattitude] floatValue] longitude:[[currentLocationDict valueForKey:kKeyRadarLong] floatValue]];

                    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[previousLocationDict valueForKey:kKeyLattitude] floatValue] longitude:[[previousLocationDict valueForKey:kKeyRadarLong] floatValue]];

                    CLLocationDistance userBdistance = [toLoc distanceFromLocation:fromLoc];

                    if (userBdistance >= DISTANCEFILTER) {

                        isBLocationChanged = YES;

//                        NSLog(@"id: %@ location changed", userDict[kKeyId]);
//                        NSLog(@"Previous Lat: %@ Current Lat:%@", distanceObj.lat, currentLocationDict[kKeyLattitude]);
//                        NSLog(@"Previous Long: %@ Current Long:%@", distanceObj.longitude, currentLocationDict[kKeyRadarLong]);
                    }
                }

                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];

                if (distance >= DISTANCEFILTER) {

                    isALocationChanged = YES;

//                    NSLog(@"My Location changed");
//                    NSLog(@"Previous Lat: %f Current Lat:%f", distanceObj.myLocation.coordinate.latitude, (APP_DELEGATE).lastLocation.coordinate.latitude);
//                    NSLog(@"Previous Long: %f Current Long:%f", distanceObj.myLocation.coordinate.longitude, (APP_DELEGATE).lastLocation.coordinate.longitude);
                }
                if (isBLocationChanged || isALocationChanged) {
                    [arr addObject:userDict];
                }
            } else {

                [arr addObject:userDict];
                [(APP_DELEGATE).oldEventUserArray addObject:[RadarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
            }
        }
    } else {

#warning Each time event user will get change so don't remove all the objects, change logic of removing

        [arr addObjectsFromArray:eventUsers];

        for (NSDictionary *userDict in eventUsers) {

            if ((APP_DELEGATE).oldEventUserArray && (APP_DELEGATE).oldEventUserArray.count) {

                NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldEventUserArray indexOfObjectPassingTest:^BOOL(RadarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {
                    if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                        return YES;
                        *stop = YES;
                    } else {
                        return NO;
                    }
                }];

                if (indexOfOldDataObject != NSNotFound && (APP_DELEGATE).oldEventUserArray && (APP_DELEGATE).oldEventUserArray.count && indexOfOldDataObject < (APP_DELEGATE).oldEventUserArray.count) {
                    RadarDistanceClass *obj = [RadarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation];

                    (APP_DELEGATE).oldEventUserArray[indexOfOldDataObject] = obj;

                } else {

                    [(APP_DELEGATE).oldEventUserArray addObject:[RadarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];

                }

            } else {

                [(APP_DELEGATE).oldEventUserArray addObject:[RadarDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];

            }
        }

    }

    if (arr && arr.count) {

      //  NSLog(@"arr count: %lu", (unsigned long) arr.count);

        for (NSDictionary *dict in arr) {

            __block NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
            NSDictionary *distanceDict = [self updateDistance:updatedDict];

          //  NSLog(@"id:%d", [distanceDict[kKeyId] intValue]);

            updatedDict = distanceDict;

            NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldEventUserArray indexOfObjectPassingTest:^BOOL(RadarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                if ([anotherObj.dataId isEqualToString:updatedDict[kKeyId]]) {
                    return YES;
                } else {

                    return NO;
                }

            }];

            if (indexOfOldDataObject != NSNotFound && (APP_DELEGATE).oldEventUserArray && (APP_DELEGATE).oldEventUserArray.count && indexOfOldDataObject < (APP_DELEGATE).oldEventUserArray.count) {
                RadarDistanceClass *obj = [RadarDistanceClass getObjectFromDict:updatedDict myLocation:server.myLocation];

                (APP_DELEGATE).oldEventUserArray[indexOfOldDataObject] = obj;

            }
            NSUInteger indexOfObject = [eventUsers indexOfObjectPassingTest:^BOOL(NSDictionary *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {

                if ([anotherObj[kKeyId] isEqualToString:updatedDict[kKeyId]]) {
                    return YES;
                } else {

                    return NO;
                }

            }];

            if (indexOfObject != NSNotFound && eventUsers && eventUsers.count && indexOfObject < eventUsers.count) {

                eventUsers[indexOfObject] = updatedDict;
            } else {

                [eventUsers addObject:updatedDict];
            }
        }
        // Load radar users
        [radarCon loadUsersInRadar:eventUsers];

        listView = [[SREventUserListView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        listView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:listView];
        listView.server = server;
        listView.delegate = self;
        [listView loadUsers:eventUsers];
        listView.hidden = YES;

        mapView = [[SREventUserMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:mapView];
        mapView.server = server;
        mapView.delegate = self;
        [mapView loadUsers:eventUsers];
        mapView.hidden = YES;

        if (_matrixRequestArray && _matrixRequestArray.count) {
            //By Madhura
            [self callDistanceAPIForMatrixRequestArr];

        }
    } else {

        // Load radar users
        [radarCon loadUsersInRadar:eventUsers];

        listView = [[SREventUserListView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        listView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:listView];
        listView.server = server;
        listView.delegate = self;
        [listView loadUsers:eventUsers];
        listView.hidden = YES;

        mapView = [[SREventUserMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:mapView];
        mapView.server = server;
        mapView.delegate = self;
        [mapView loadUsers:eventUsers];
        mapView.hidden = YES;

    }
}


- (NSDictionary *)updateDistance:(NSDictionary *)dict {
    __block NSDictionary *updatedDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    float distance = [[updatedDict valueForKey:kKeyDistance] floatValue];

    BOOL shouldCallMatrixAPI = NO;


    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;


    if ([distanceUnit isEqualToString:kkeyUnitMiles]) {
        [updatedDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 20) {

            shouldCallMatrixAPI = YES;

        }
    } else {
        [updatedDict setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 32) {

            shouldCallMatrixAPI = YES;
        }
    }

    if (shouldCallMatrixAPI) {

        [_matrixRequestArray addObject:updatedDict];

    }

    return updatedDict;

}

- (void)callDistanceAPIForMatrixRequestArr {

    NSMutableArray *requestsArray = [[NSMutableArray alloc] init];
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    __block NSString *checkURL = @"";
    NSMutableArray *arrLocation = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSDictionary *userDict in _matrixRequestArray) {
        if (!isNullObject([userDict valueForKey:kKeyLocation])) {
            arrLocation[i] = userDict;
            CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude] floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong] floatValue]];
            NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
            //        NSLog(@"urlStr::%@",urlStr);
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            request.timeoutInterval = 180;

            [requestsArray addObject:request];
            i++;
        }

    }

    if (requestsArray.count) {
        __block NSInteger currentRequestIndex = 0;

        __weak typeof(self) weakSelf = self;

        _handler = ^void(NSURLResponse *response, NSData *data, NSError *error) {

            if (!error) {

                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

                if ([weakSelf IsValidResponse:data]) {
                    dict[@"data"] = data; //add graphhopper response

                    if (currentRequestIndex < weakSelf.matrixRequestArray.count) {
                        dict[@"userDict"] = weakSelf.matrixRequestArray[currentRequestIndex];
                        [weakSelf.matrixResponseArray addObject:dict];
                    }
                    currentRequestIndex++;
                    if (currentRequestIndex < [requestsArray count]) {
                        NSURLRequest *requ = requestsArray[currentRequestIndex];
                        NSString *requestPath = [[requ URL] absoluteString];
                        checkURL = requestPath;
                        [NSURLConnection sendAsynchronousRequest:requestsArray[currentRequestIndex]
                                                           queue:[NSOperationQueue mainQueue]
                                               completionHandler:weakSelf.handler];
                    } else {
                        [weakSelf MatrixAPICallCompleted];
                    }
                }
            }
        };
        NSURLRequest *requ = requestsArray[0];
        NSString *requestPath = [[requ URL] absoluteString];
        checkURL = requestPath;
        [NSURLConnection sendAsynchronousRequest:requestsArray[0]
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:weakSelf.handler];
    }


}


- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }

}

- (void)MatrixAPICallCompleted {

    [_matrixRequestArray removeAllObjects];
    if (_matrixResponseArray && _matrixResponseArray.count) {
        BOOL shouldReloadData = NO;

        for (NSDictionary *dict in _matrixResponseArray) {

            NSData *data = dict[@"data"];
            NSDictionary *userDict = dict[@"userDict"];
            Boolean isjsonValid = false;
            double convertDist = 0.0;
            NSError *error;
            if (data.length > 0) {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                NSLog(@"\n\nMATRIX API RESPONSE For userid: %@ :%@ \n\n", userDict[kKeyId], json);
                if ([json[@"paths"] count]) {
                    isjsonValid = true;
                    //parse data
                    NSDictionary *dictOSM = [json valueForKey:@"paths"];
                    NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                    NSString *distance = arrdistance[0];
                    convertDist = [distance doubleValue];

                    if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                        //meters to killometer
                        convertDist = convertDist / 1000;
                    } else {
                        //meters to miles
                        convertDist = (convertDist * 0.000621371192);
                    }
                } else {

                  //  NSLog(@"Matrix JSON not ok");

                    NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldEventUserArray indexOfObjectPassingTest:^BOOL(RadarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                        if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                            return YES;
                        } else {

                            return NO;
                        }

                    }];

                    if (indexOfOldDataObject != NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldEventUserArray.count) {
                        [(APP_DELEGATE).oldEventUserArray removeObjectAtIndex:indexOfOldDataObject];
                    }
                }
            }
            if (isjsonValid) {
                shouldReloadData = YES;

                [userDict setValue:[NSString stringWithFormat:@"%.2f", convertDist] forKey:kKeyDistance];

                NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldEventUserArray indexOfObjectPassingTest:^BOOL(RadarDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {


                    if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                        return YES;
                    } else {

                        return NO;
                    }

                }];

                if (indexOfOldDataObject != NSNotFound && (APP_DELEGATE).oldEventUserArray && (APP_DELEGATE).oldEventUserArray.count && indexOfOldDataObject < (APP_DELEGATE).oldEventUserArray.count) {
                    RadarDistanceClass *obj = [[RadarDistanceClass alloc] init];
                    obj.dataId = userDict[kKeyId];
                    NSDictionary *locationDict = userDict[kKeyLocation];
                    obj.lat = locationDict[kKeyLattitude];
                    obj.longitude = locationDict[kKeyRadarLong];
                    obj.distance = [NSString stringWithFormat:@"%.2f", convertDist];
                    obj.myLocation = server.myLocation;
                    obj.userDict = userDict;
                    (APP_DELEGATE).oldEventUserArray[indexOfOldDataObject] = obj;
                }
                NSUInteger indexOfUserObject = [eventUsers indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL *_Nonnull stop) {

                    if ([obj[kKeyId] isEqualToString:userDict[kKeyId]]) {
                        return YES;
                        *stop = YES;

                    } else {

                        return NO;

                    }

                }];

                if (indexOfUserObject != NSNotFound && eventUsers && eventUsers.count && indexOfUserObject < eventUsers.count) {
                    eventUsers[indexOfUserObject] = userDict;
                }
            }
        }

        if (shouldReloadData) {
            dispatch_async(dispatch_get_main_queue(), ^{

                [radarCon loadUsersInRadar:eventUsers];
                [listView loadUsers:eventUsers];
//                [mapView loadUsers:eventUsers];

            });

        } else {

          //  NSLog(@"All Matrix API failed");
        }

        [_matrixResponseArray removeAllObjects];
    }

}


#pragma mark
#pragma mark IBAction Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// btnAnimateClick:

- (IBAction)btnAnimateClick:(id)sender {
    [self.btnAnimate setBackgroundColor:[UIColor clearColor]];
    [self.btnAnimate setImage:[UIImage imageNamed:@"close-black.png"] forState:UIControlStateNormal];
}

#pragma mark
#pragma mark GHContextMenu Delegate Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// numberOfMenuItems

- (NSInteger)numberOfMenuItems {
    return 3;
}

//------------------------------------------------------------------------------------------------------------------------
// imageForItemAtIndex:

- (UIImage *)imageForItemAtIndex:(NSInteger)index {
    NSString *imageName = nil;
    switch (index) {
        case 0:
            imageName = @"toogle-radar-white-50px.png";
            break;

        case 1:
            imageName = @"toogle-map-white.png";
            break;

        case 2:
            imageName = @"toogle-list-white-50px.png";
            break;

        default:
            break;
    }

    // Image name
    return [UIImage imageNamed:imageName];
}

//------------------------------------------------------------------------------------------------------------------------
// didSelectItemAtIndex:

- (void)didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point {
    switch (selectedIndex) {
        case 0: {
            [self.btnAnimate setImage:[UIImage imageNamed:@"toogle-radar-black-50px.png"] forState:UIControlStateNormal];
            listView.hidden = YES;
            mapView.hidden = YES;
            radarCon.view.hidden = NO;
            [self.view bringSubviewToFront:self.btnAnimate];

            break;
        }

        case 1: {
            listView.hidden = YES;
            mapView.hidden = NO;
            radarCon.view.hidden = YES;

            [self.btnAnimate.layer setShadowOffset:CGSizeMake(5, 5)];
            [self.btnAnimate.layer setShadowColor:[[UIColor blackColor] CGColor]];
            [self.btnAnimate.layer setShadowOpacity:8.5];
            [self.btnAnimate setImage:[UIImage imageNamed:@"toogle-map-black.png"] forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnAnimate];
            break;
        }

        case 2: {
            // Remove views
            listView.hidden = NO;
            mapView.hidden = YES;
            radarCon.view.hidden = YES;
            [self.btnAnimate setImage:[UIImage imageNamed:@"toogle-list-black-50px.png"] forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnAnimate];
            break;
        }

        default:
            break;
    }
}

@end
