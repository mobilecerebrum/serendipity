//
//  SREventMapViewController.m
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SREventMapViewController.h"
#import "SRModalClass.h"
#import "CalloutAnnotation.h"
#import "SRMapProfileView.h"
#import "SRGroupDetailTabViewController.h"
#import "SRMapViewController.h"

const double mapZoomLimit = 0.007812;

@interface SREventMapViewController ()

@end

@implementation SREventMapViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//-----------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"SREventMapViewController" bundle:nil];

    if (self) {
        // Custom initialization
        server = inServer;
        nearByUsersList = [[NSMutableArray alloc] init];
        userProfileViewArr = [[NSMutableArray alloc] init];
        myLocation = server.myLocation;
        latestDistance = kKeyMaximumRadarMile;

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getUpdateLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(showEventSucceed:)
                              name:kKeyNotificationEventRadarChanges object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateEventSliderValue:)
                              name:kKeyNotificationEventRadarRefreshOnSlider
                            object:nil];
    }

    //return
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark
#pragma mark Standard Method
#pragma mark

//-----------------------------------------------------------------------------------------------------------------------
// viewDidLoad:

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];
    // Centering Map to current Location
    self.mapView.delegate = self;


    self.slider1.maximumValue = kKeyMaximumRadarMile;
    self.slider1.minimumValue = kKeyMinimumRadarMile;
    self.slider1.value = latestDistance;

    self.slider1.popUpViewCornerRadius = 5.0;
    [self.slider1 setMaxFractionDigitsDisplayed:0];
    self.slider1.popUpViewColor = [UIColor colorWithRed:1 green:0.588 blue:0 alpha:1];
    self.slider1.font = [UIFont fontWithName:@"GillSans-Bold" size:22];
    self.slider1.textColor = [UIColor whiteColor];
    self.slider1.popUpViewWidthPaddingFactor = 0.5; //1.7
    [self.slider1 addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    self.slider1.dataSource = self;



    // ScrollView
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideScrollView:)];
    [self.scrollView addGestureRecognizer:tapGesture];

    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [pinch setDelegate:self];
    [pinch setDelaysTouchesBegan:YES];
    [self.mapView addGestureRecognizer:pinch];

    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;

    // Add leftview to search textfield
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    container.backgroundColor = [UIColor clearColor];

    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(25, 4, 20, 20)];
    searchIcon.image = [UIImage imageNamed:@"ic_search"];
    [container addSubview:searchIcon];

    self.txfSearchField.leftView = container;
    self.txfSearchField.leftViewMode = UITextFieldViewModeAlways;
    self.txfSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"eventList.view.txt.searchText.txt", "") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txfSearchField.layer.cornerRadius = 5;

    //Zoom in controls
    zoomInBtn.layer.borderWidth = 1.0;
    zoomInBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    zoomOutBtn.layer.borderWidth = 1.0;
    zoomOutBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];

    [self.locationToolBar setBackgroundImage:[UIImage new]
                          forToolbarPosition:UIToolbarPositionAny
                                  barMetrics:UIBarMetricsDefault];

    MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];

    if (SCREEN_WIDTH <= 375) {
        spacer.width = -16;
    } else
        spacer.width = -22; // for example shift right bar button to the right

//    self.locationToolBar.frame = CGRectMake(self.locationToolBar.frame.origin.x, self.locationToolBar.frame.origin.y, 32, 30);
    [self.locationToolBar.layer setCornerRadius:5.0];
    [self.locationToolBar.layer setMasksToBounds:YES];
    self.locationToolBar.items = @[buttonItem];

    [self.mapView setCenterCoordinate:myLocation.coordinate animated:YES];
    MKCoordinateSpan span = MKCoordinateSpanMake(0.5, 0.0);
    MKCoordinateRegion region = MKCoordinateRegionMake(MKCoordinateForMapPoint(MKMapPointForCoordinate(myLocation.coordinate)), span);
    [self.mapView setRegion:region animated:YES];

    if (pin == nil) {
        pin = [[REVClusterPin alloc] init];
        pin.coordinate = myLocation.coordinate;
    } else
        pin.coordinate = myLocation.coordinate;
    [self.mapView addAnnotation:pin];

    CLLocationCoordinate2D centerCoordinate = self.mapView.centerCoordinate;
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
    [self.mapView setCenterCoordinate:centerLocation.coordinate zoomLevel:0 animated:NO];
}
//-----------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    // Call super
//    (APP_DELEGATE).topBarView.hidden = YES;
    [super viewWillAppear:YES];
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        [self.mapView setCenterCoordinate:panoramaLastLoc zoomLevel:5 animated:YES];
    }
}
// -----------------------------------------------------------------------------------------------
// viewDidAppear

- (void)viewDidAppear:(BOOL)animated {
    // Call super
    [super viewDidAppear:NO];
    // Plot users on map
    [self plotUsersOnMapView];
}


//-----------------------------------------------------------------------------------------------------------------------
// viewWillDisappear:
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventMapRefreshOnSlider object:[NSString stringWithFormat:@"%f", self.slider1.value]];
}

#pragma mark
#pragma mark - Slider update
#pragma mark

//-----------------------------------------------------------------------------------------------------------------------
// sliderValueChange:
- (void)sliderValueChange:(UISlider *)Sender {
    isSlide = YES;

    mapZoomLevel = Sender.value / 166;
    mapZoomLevel = 18 - mapZoomLevel + 1;
    if (mapZoomLevel == 1) {
        mapZoomLevel = 0;
    }

    CLLocationCoordinate2D centerCoordinate = self.mapView.centerCoordinate;
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
    [self.mapView setCenterCoordinate:centerLocation.coordinate zoomLevel:mapZoomLevel animated:NO];
}

// slider has a custom NSNumberFormatter to display temperature in °C
// the dataSource method below returns custom NSStrings for specific values
- (NSString *)slider:(ASValueTrackingSlider *)slider stringForValue:(float)value; {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }

    NSString *str;
    NSInteger valuee = ((distancevalue / 100) * 50);
    NSInteger value1 = ((distancevalue / 100) * 60);
    NSInteger value2 = ((distancevalue / 100) * 70);
    NSInteger value3 = ((distancevalue / 100) * 80);
    NSInteger value4 = ((distancevalue / 100) * 90);
    NSInteger value5 = ((distancevalue / 100) * 95);
    NSInteger value6 = ((distancevalue / 100) * 100);


    if (slider.value <= valuee) {
        str = [[NSString stringWithFormat:@"%.f", slider.value] stringByAppendingString:@" miles"];
    } else if (slider.value > valuee && slider.value <= value1) {
        str = placemark.thoroughfare;
    } else if (slider.value > value1 && slider.value <= value2) {
        str = placemark.subLocality;
    } else if (slider.value > value2 && slider.value <= value3) {
        str = placemark.locality;
    } else if (slider.value > value3 && slider.value <= value4) {
        str = placemark.administrativeArea;
    } else if (slider.value > value4 && slider.value <= value5) {
        str = placemark.country;
    } else if (slider.value > value5 && slider.value <= value6) {
        str = @"World";

    }
    return str;
}

//-----------------------------------------------------------------------------------------------------------------------
// handlePinchGesture:
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)Sender {
    isSlide = NO;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    isSlide = NO;
    return YES;
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

//
// -----------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [UIView animateWithDuration:0.5f animations:^{
            self->streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
}

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
 //   NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
 //   NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.view];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        self->streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    self->panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[self->_mapView camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [self->_mapView setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        self->isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!(CGRectContainsPoint(self->streetViewBtn.frame, touchPoint))) {
                        if (!self->isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 1;
                            [panoAlert show];
                        }
                    }
                }
            }];
//        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapView addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [self.mapView convertPoint:point toCoordinateFromView:self.view];

    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        self->streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];

    dispatch_async(dispatch_get_main_queue(), ^(void) {

        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    self->panoramaLastLoc = tapPoint;
                    MKMapCamera *newCamera = [[_mapView camera] copy];
                    [newCamera setPitch:45.0];
                    [newCamera setHeading:90.0];
                    [newCamera setAltitude:500.0];
                    newCamera.centerCoordinate = tapPoint;
                    [self->_mapView setCamera:newCamera animated:YES];

                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        self->isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!self->isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 1;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}

// --------------------------------------------------------------------------------
// zoomInMap
- (IBAction)zoomInMap {
    isSlide = NO;
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta = MIN(region.span.latitudeDelta * 2.0, 180.0);
    region.span.longitudeDelta = MIN(region.span.longitudeDelta * 2.0, 180.0);
    [self.mapView setRegion:region animated:YES];
}

// --------------------------------------------------------------------------------
// zoomOutMap
- (IBAction)zoomOutMap {
    isSlide = NO;
    MKCoordinateRegion region = self.mapView.region;
    region.span.latitudeDelta /= 2.0;
    region.span.longitudeDelta /= 2.0;
    [self.mapView setRegion:region animated:YES];
}

- (IBAction)actionOnBtnClick:(UIButton *)inSender {
    if (inSender.tag == 0) {
        self.mapView.mapType = MKMapTypeSatellite;
        [btnStreet setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    } else {
        self.mapView.mapType = MKMapTypeStandard;
        [btnStreet setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

#pragma mark
#pragma mark Private Method
#pragma mark

// -------------------------------------------------------------------------------------------------
// hideScrollView:

- (void)hideScrollView:(UITapGestureRecognizer *)gesture {
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;
}

// -------------------------------------------------------------------------------------------------
// addViewsOnScrollView:

- (void)addViewsOnScrollView:(NSArray *)inArr {

    CGRect frame;
    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (NSUInteger i = 0; i < [inArr count]; i++) {
        // Get profile pic of users
        NSDictionary *userDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i == 0) {
            frame = CGRectMake(0, 11, 180, 200);
        } else {
            frame = CGRectMake(frame.origin.x + 136, 11, 140, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;

        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds
                                         byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                               cornerRadii:CGSizeMake(8.0, 8.0)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = ((CalloutAnnotationView *) annotationView).userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        ((CalloutAnnotationView *) annotationView).userProfileImage.layer.mask = maskLayer;

        if (userDict[kKeyMediaImage] != nil && [userDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {

            if ([userDict[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyMediaImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl;
                    if ([[userDict valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
                        imageUrl = [NSString stringWithFormat:@"%@", imageName];
                    } else
                        imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                    [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"events_avatar1.png"];

            } else {
                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"events_avatar1.png"];
                ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
            }
        } else {
            ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"events_avatar1.png"];
            ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
        }


        //Favourite
        ((CalloutAnnotationView *) annotationView).favUserImage.hidden = TRUE;

        //Invisible
        ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = TRUE;

        //Distance
        ((CalloutAnnotationView *) annotationView).lblDistance.hidden = TRUE;

        //Degree
        ((CalloutAnnotationView *) annotationView).lblDegree.hidden = TRUE;

        ((CalloutAnnotationView *) annotationView).lblProfileName.text = [userDict valueForKey:kKeyName];

        //Date & time
        NSString *dateString = [NSString stringWithFormat:@"%@ %@", [userDict valueForKey:kKeyEvent_Date], [userDict valueForKey:kKeyEvent_Time]];
        NSDate *eventDate = [NSDate getEventDateFromString:dateString];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        [formatter setDateFormat:@"MMM dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSString *monthString = [formatter stringFromDate:eventDate];
        [formatter setDateFormat:@"HH:mm"];
        NSString *timeStr = [formatter stringFromDate:eventDate];
        ((CalloutAnnotationView *) annotationView).lblOccupation.text = [NSString stringWithFormat:@"%@ %@ at %@", [userDict valueForKey:kKeyEvent_Type], monthString, timeStr];
        ((CalloutAnnotationView *) annotationView).lblOccupation.frame = CGRectMake(((CalloutAnnotationView *) annotationView).lblOccupation.frame.origin.x, ((CalloutAnnotationView *) annotationView).lblOccupation.frame.origin.y, ((CalloutAnnotationView *) annotationView).frame.size.width, ((CalloutAnnotationView *) annotationView).lblOccupation.frame.size.height);

        // Image for group type
        //As Dj showing all event will be addable to calender
        if (![SRModalClass isEventExist:userDict[kKeyId]]) {
            ((CalloutAnnotationView *) annotationView).imgViewGrp.backgroundColor = [UIColor orangeColor];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.image = [UIImage imageNamed:@"calendar-plus_25.png"];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapEventAddCalender = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addEventToCalender:)];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.tag = [[userDict valueForKey:kkeyNumbering] integerValue];
            [((CalloutAnnotationView *) annotationView).imgViewGrp addGestureRecognizer:tapEventAddCalender];

        } else {
            ((CalloutAnnotationView *) annotationView).imgViewGrp.image = [UIImage imageNamed:@"calendar-check_25.png"];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.backgroundColor = [UIColor blackColor];
        }
        [((CalloutAnnotationView *) annotationView).imgViewGrp setContentMode:UIViewContentModeCenter];
        ((CalloutAnnotationView *) annotationView).imgViewGrp.layer.cornerRadius = ((CalloutAnnotationView *) annotationView).imgViewGrp.frame.size.width / 2.0;


        if ([[userDict valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
            [((CalloutAnnotationView *) annotationView).pingButton setHidden:YES];
        } else {
            // Add Shadow to btnPing
            [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
            [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
            [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOpacity:8.5];
            [((CalloutAnnotationView *) annotationView).pingButton addTarget:self action:@selector(pingButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        }



        //Add Compass Button
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowOpacity:8.5];

        CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                server.myLocation.coordinate.longitude};

        NSDictionary *userLocDict = userDict[kKeyLocation];
        CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};

        [((CalloutAnnotationView *) annotationView).compassButton addTarget:self action:@selector(compassButtonAction:) forControlEvents:UIControlEventTouchUpInside];


        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            annotationView.lblCompassDirection.text = @"N";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            annotationView.lblCompassDirection.text = @"E";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            annotationView.lblCompassDirection.text = @"S";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            annotationView.lblCompassDirection.text = @"W";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            annotationView.lblCompassDirection.text = @"NE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            annotationView.lblCompassDirection.text = @"NW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            annotationView.lblCompassDirection.text = @"SE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            annotationView.lblCompassDirection.text = @"SW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }

        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(annotationTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [((CalloutAnnotationView *) annotationView).userProfileImage addGestureRecognizer:annotationTapGesture];
        ((CalloutAnnotationView *) annotationView).userDict = userDict;


        //if users count is one
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }
        //        // Add subview to scroll view
        //        [self.scrollView addSubview:annotationView];


        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else
            self.scrollView.scrollEnabled = NO;

        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
    }
}

// -------------------------------------------------------------------------------
// addEventToCalender:
- (void)addEventToCalender:(UITapGestureRecognizer *)tapRecognizer {
    UIImageView *imgView = (UIImageView *) tapRecognizer.view;
    [imgView setImage:[UIImage imageNamed:@"calendar-check_25.png"]];
    imgView.backgroundColor = [UIColor blackColor];
    imgView.userInteractionEnabled = NO;
    for (int i = 0; i < nearByUsersList.count; i++) {
        if (tapRecognizer.view.tag == [[nearByUsersList[i] valueForKey:kkeyNumbering] integerValue]) {
            [SRModalClass saveEventToCalender:nearByUsersList[i]];
        }
    }
}

// -------------------------------------------------------------------------------
// makeRoundedImage:

- (UIImage *)makeRoundedImage:(UIImage *)image
                       radius:(float)radius; {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = radius / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Return
    return roundedImage;
}

// -------------------------------------------------------------------------------
// plotUsersOnMapView:

- (void)plotUsersOnMapView {

    [userProfileViewArr removeAllObjects];

    NSMutableArray *pins = [NSMutableArray array];
    CLLocationCoordinate2D coordinate;
    if (searchArray.count > 0) {
        for (NSDictionary *dict in searchArray) {
            float lat = [dict[kKeyLattitude] floatValue];
            float lon = [dict[kKeyRadarLong] floatValue];
            coordinate.latitude = lat;
            coordinate.longitude = lon;


            CLLocationCoordinate2D newCoord = coordinate;
            pin = [[REVClusterPin alloc] init];
            pin.title = [[dict valueForKey:kKeyCombination] valueForKey:kKeyId];
            pin.coordinate = newCoord;
            [pins addObject:pin];
            //[self.mapView addAnnotation:pin];
        }
        [_mapView addAnnotations:pins];
    } else {
        //    for (NSDictionary *dict in nearByUsersList) {
        for (NSDictionary *dict in sourceArr) {
            float lat = [dict[kKeyLattitude] floatValue];
            float lon = [dict[kKeyRadarLong] floatValue];
            coordinate.latitude = lat;
            coordinate.longitude = lon;


            CLLocationCoordinate2D newCoord = coordinate;
            pin = [[REVClusterPin alloc] init];
            pin.title = [[dict valueForKey:kKeyCombination] valueForKey:kKeyId];
            pin.coordinate = newCoord;
            [pins addObject:pin];
            //[self.mapView addAnnotation:pin];
        }
        [_mapView addAnnotations:pins];
    }
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) pins.count, latestDistance, distanceUnit];
}


#pragma mark
#pragma mark
#pragma mark Action methods

// --------------------------------------------------------------------------------
// pingButtonAction:

- (void)pingButtonAction:(UIButton *)sender {


    //
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];

    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    if ([calloutView.userDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyMediaImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }

    server.eventDetailInfo = [SRModalClass removeNullValuesFromDict:dictionary];
    // Set chat view
    SRChatViewController *objSRPingsView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:(APP_DELEGATE).server.eventDetailInfo];
    [self.navigationController pushViewController:objSRPingsView animated:NO];
}

// --------------------------------------------------------------------------------
// compassButtonAction

- (void)compassButtonAction:(UIButton *)sender {

    // Get profile
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    NSDictionary *dataDict = [SRModalClass removeNullValuesFromDict:calloutView.userDict];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dataDict];
    SRMapViewController *compassView = [[SRMapViewController alloc] initWithNibName:@"FromEventListView" bundle:nil inDict:dictionary server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:compassView animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}


// --------------------------------------------------------------------------------
// annotationTapAction:

- (void)annotationTapAction:(UITapGestureRecognizer *)sender {

    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];
    // Get profile
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];
    CGPoint p = [sender locationInView:calloutView];

    if (CGRectContainsPoint(calloutView.imgViewGrp.frame, p) && ![SRModalClass isEventExist:dictionary[kKeyId]]) {

        [SRModalClass saveEventToCalender:dictionary];
        calloutView.imgViewGrp.image = [UIImage imageNamed:@"calendar-check.png"];
        calloutView.imgViewGrp.backgroundColor = [UIColor blackColor];

    } else {
        if ([calloutView.userDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [calloutView.userDict[kKeyMediaImage] objectForKey:kKeyImageName]];
            NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

            if ([dotsFilterArr count] > 0) {
                dictionary[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
            }
        }

        server.eventDetailInfo = [SRModalClass removeNullValuesFromDict:dictionary];
        SREventDetailTabViewController *objEventTab = [[SREventDetailTabViewController alloc] initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:objEventTab animated:YES];
    }

}


#pragma mark
#pragma mark MapView Delegates
#pragma mark

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated; {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    [geocoder reverseGeocodeLocation:loc
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       placemark = placemarks[0];
                   }];
}


//-------------------------------------------------------------------------------------------------------------
// Check Zoom Level:

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    MKZoomScale currentZoomScale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
    if (currentZoomScale > mapZoomLimit) {
        if (!isCheckZoomLvl) {
            isCheckZoomLvl = YES;

            for (id <MKAnnotation> annotation in self.mapView.annotations) {
                if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                } else {
                    [self.mapView removeAnnotation:annotation];
                }
            }
            for (UIView *view in userProfileViewArr) {
                [view removeFromSuperview];
            }
            isZoomIn = YES;
        }
    } else {
        if (isCheckZoomLvl) {
            isCheckZoomLvl = NO;

            for (id <MKAnnotation> annotation in self.mapView.annotations) {
                if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                } else {
                    [self.mapView removeAnnotation:annotation];
                }
            }
            isZoomIn = NO;
            for (UIView *view in userProfileViewArr) {
                [view removeFromSuperview];
            }

        }
    }
    if (!isSlide) {
        mapZoomLevel = [mapView zoomLevel];
        if (mapZoomLevel == 1) {
            mapZoomLevel = 0;
        }
        self.slider1.value = 3000 - (mapZoomLevel * 166);
    }

    //Get all annotations count in current visible rect
    NSSet *annSet = [mapView annotationsInMapRect:mapView.visibleMapRect];
    NSInteger numberOfPins = 0;

    for (id <MKAnnotation> annotationPin in annSet) {
        if ([annotationPin isKindOfClass:[REVClusterPin class]]) {
            REVClusterPin *clusteredPin = annotationPin;

            if (clusteredPin.nodeCount) {
                numberOfPins = numberOfPins + clusteredPin.nodeCount;
            } else if ([clusteredPin.title isKindOfClass:[NSArray class]]) {
                NSArray *titleArray = (NSArray *) clusteredPin.title;
                numberOfPins = numberOfPins + titleArray.count;
            } else {
                numberOfPins = numberOfPins + 1;
            }
        }
    }
    // Show text for no event
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (long) numberOfPins, self.slider1.value, distanceUnit];
    latestDistance = self.slider1.value;
}

//-----------------------------------------------------------------------------------------------------------------------
// viewForAnnotation:

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *AnnotationIdentifier = @"Annotation";
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];

    if (!pinView) {

        MKAnnotationView *customPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
        if (annotation == mapView.userLocation) {
            SRMapProfileView *myProfileView = [[SRMapProfileView alloc] init];
            UIImage *img = [SRModalClass imageFromUIView:myProfileView];
            customPinView.image = img;
            customPinView.userInteractionEnabled = FALSE;

            return customPinView;
        }
    }

    MKAnnotationView *annView;
    if ([annotation isKindOfClass:[REVClusterPin class]]) {
        pin = (REVClusterPin *) annotation;

        if ([pin nodeCount] > 0) {
            annView = (REVClusterAnnotationView *)
                    [mapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];

            if (!annView)
                annView = (REVClusterAnnotationView *) [[REVClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"cluster"];
            annView.image = [UIImage imageNamed:@"radar-pin-map-orange-60px.png"];
            [(REVClusterAnnotationView *) annView setClusterText:
                    [NSString stringWithFormat:@"%lu", (unsigned long) [pin nodeCount]]];

            annView.canShowCallout = NO;
        } else {
            annView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];

            if (!annView)
                annView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];

            if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
                // Set profile pic
                self.imgUserProfile.layer.masksToBounds = YES;
                SRMapProfileView *myProfileView = [[SRMapProfileView alloc] init];
                UIImage *img = [SRModalClass imageFromUIView:myProfileView];
                annView.image = img;
                annView.userInteractionEnabled = FALSE;
                //[annotationView addSubview:myProfileView];
            } else {

                for (NSDictionary *user in nearByUsersList) {
                    if (annotation.coordinate.latitude == [[user valueForKey:kKeyLattitude] floatValue] && annotation.coordinate.longitude == [[user valueForKey:kKeyRadarLong] floatValue]) {
//

                        if (!isZoomIn) {
                            if ([[user valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
                                annView.image = [UIImage imageNamed:@"radar-pin-black-30px.png"];
                            } else if ([SRModalClass isEventExist:user[kKeyId]]) {
                                annView.image = [UIImage imageNamed:@"radar-pin-white-20px.png"];
                            } else if ([user[kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                                annView.image = [UIImage imageNamed:@"radar-pin-white-dot-20px.png"];
                            } else
                                annView.image = [UIImage imageNamed:@"radar-pin-orange-15px.png"];
                        } else {
                            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
                            SRMapProfileView *userProfileView = nibArray[1];
                            userProfileView.userProfileImg.frame = CGRectMake(userProfileView.userProfileImg.frame.origin.x, userProfileView.userProfileImg.frame.origin.y - 2, 37, 37);
                            userProfileView.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
                            userProfileView.userProfileImg.layer.cornerRadius = userProfileView.userProfileImg.frame.size.width / 2.0;
                            userProfileView.userProfileImg.clipsToBounds = YES;
                            userProfileView.userProfileImg.layer.masksToBounds = YES;

                            if ([user[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {

                                if ([user[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
//
                                    NSString *imageName = [user[kKeyMediaImage] objectForKey:kKeyImageName];
                                    if ([imageName length] > 0) {
                                        NSString *imageUrl;
                                        if ([[user valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
                                            imageUrl = [NSString stringWithFormat:@"%@", imageName];
                                        } else
                                            imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                        [userProfileView.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                    } else

                                        userProfileView.userProfileImg.image = [UIImage imageNamed:@"events_avatar1.png"];
                                } else
                                    userProfileView.userProfileImg.image = [UIImage imageNamed:@"events_avatar1.png"];
                            } else
                                userProfileView.userProfileImg.image = [UIImage imageNamed:@"events_avatar1.png"];

                            // Add to view
                            [userProfileViewArr addObject:userProfileView];
                            UIImage *img = [SRModalClass imageFromUIView:userProfileView];
                            annView.image = img;
                        }
                    }
                }
            }
        }
    }
    // No callout
    [annView setCanShowCallout:NO];
    [annView setEnabled:YES];

    // Return
    return annView;
}

//-----------------------------------------------------------------------------------------------------------------
// didSelectAnnotationView:
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    MKZoomScale currentZoomScale = mapView.bounds.size.width / mapView.visibleMapRect.size.width;
    isSelectAnnotation = YES;

    if (currentZoomScale > mapZoomLimit) {
        NSMutableArray *combinationArr = [NSMutableArray array];
        for (NSDictionary *user in nearByUsersList) {

            if (view.annotation.coordinate.latitude == [[user valueForKey:kKeyLattitude] floatValue] && view.annotation.coordinate.longitude == [[user valueForKey:kKeyRadarLong] floatValue]) {
                [combinationArr addObject:user];
            }

        }
        if (combinationArr.count == 0) {
            if (searchArray.count > 0) {
                for (NSDictionary *user in searchArray) {

                    if (view.annotation.coordinate.latitude == [[user valueForKey:kKeyLattitude] floatValue] && view.annotation.coordinate.longitude == [[user valueForKey:kKeyRadarLong] floatValue]) {
                        [combinationArr addObject:user];
                    }
                }
            }
        }
        [self addViewsOnScrollView:combinationArr];
    } else {
        NSMutableArray *combinationArr = [NSMutableArray array];
        for (NSDictionary *user in sourceArr) {
            if (view.annotation.coordinate.latitude == [[user valueForKey:kKeyLattitude] floatValue] && view.annotation.coordinate.longitude == [[user valueForKey:kKeyRadarLong] floatValue]) {
                [combinationArr addObject:user];
            }

        }
        [self addViewsOnScrollView:combinationArr];
    }

}


//-----------------------------------------------------------------------------------------------------------------
// didDeselectAnnotationView:

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    //Hide ProfilesScrollview if unhide
    isSelectAnnotation = NO;

    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;
    selectedCombinatonArr = nil;

    if ([view.annotation isKindOfClass:[REVClusterPin class]]) {
        // Deselected the pin annotation.
        REVClusterPin *pinAnnotation = ((REVClusterPin *) view.annotation);
        [mapView removeAnnotation:pinAnnotation.calloutAnnotation];
        pinAnnotation.calloutAnnotation = nil;
    }
}

//-----------------------------------------------------------------------------------------------------------------
// didUpdateUserLocation:

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // Zoom to region containing the user location
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1000, 1000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

#pragma mark
#pragma mark Delegate Callout Method
#pragma mark

// --------------------------------------------------------------------------------
// calloutButtonClicked:

- (void)calloutButtonClicked:(NSString *)title {
}

#pragma mark
#pragma mark Notification Method
#pragma mark

- (void)updateEventSliderValue:(NSNotification *)inNotify {
    latestDistance = [[inNotify object] floatValue];
    mapZoomLevel = latestDistance / 166;
    mapZoomLevel = 18 - mapZoomLevel + 1;
    self.slider1.value = latestDistance;
    isSlide = YES;
    CLLocationCoordinate2D centerCoordinate = self.mapView.centerCoordinate;
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
    [self.mapView setCenterCoordinate:centerLocation.coordinate zoomLevel:mapZoomLevel animated:NO];
}

// --------------------------------------------------------------------------------
// ShowEventSucceed:

- (void)showEventSucceed:(NSNotification *)inNotify {

    sourceArr = [inNotify object];
    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
        } else {
            [self.mapView removeAnnotation:annotation];
        }
    }
    for (UIView *view in userProfileViewArr) {
        [view removeFromSuperview];
    }

    // Remove all objects
    // [nearByUsersList removeAllObjects];

    // Check if users are on same distance
    NSMutableArray *usersArr = [NSMutableArray arrayWithArray:[inNotify object]];
    if (usersArr.count > 0) {
        nearByUsersList = usersArr;
    }

    self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearByUsersList.count, latestDistance, distanceUnit];
    // Now plot users
    [self plotUsersOnMapView];

}


// --------------------------------------------------------------------------------
// getUpdateLocation:

- (void)getUpdateLocation:(NSNotification *)inNotify {
    myLocation = server.myLocation;


}

#pragma mark
#pragma mark TextField Delegate method
#pragma mark

// --------------------------------------------------------------------------------
//  textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    // Search
    [self searchEventForSearchText:textField.text];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self searchEventForSearchText:str];

    // Return
    return YES;
}



// --------------------------------------------------------------------------------
// textFieldDidEndEditing

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // Search
    [self searchEventForSearchText:nil];
    [textField resignFirstResponder];

    // Return
    return YES;
}
// --------------------------------------------------------------------------------
// searchEventsForSearchText:

- (void)searchEventForSearchText:(NSString *)searchText {
    if ([searchText length] > 0) {
        NSString *filter = @"%K CONTAINS[cd] %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyName, searchText];
        searchArray = [sourceArr filteredArrayUsingPredicate:predicate];

        NSArray *oldPins = self.mapView.annotations;
        for (REVClusterPin *annotation in oldPins) {

            if (annotation.coordinate.latitude == myLocation.coordinate.latitude && annotation.coordinate.longitude == myLocation.coordinate.longitude) {
            } else {
                [self.mapView removeAnnotation:annotation];
            }
        }
        [self plotUsersOnMapView];
    } else {
        searchArray = nil;
        nearByUsersList = [NSMutableArray arrayWithArray:sourceArr];
        [self plotUsersOnMapView];
    }
}

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txfSearchField resignFirstResponder];
}


@end
