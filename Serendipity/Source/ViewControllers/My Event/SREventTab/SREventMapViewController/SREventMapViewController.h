//
//  SREventMapViewController.h
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CalloutAnnotationView.h"
#import "SRProfileImageView.h"
#import "SRMapWindowView.h"
#import "NSDate+SRCustomDate.h"
#import "ASValueTrackingSlider.h"
#import "SREventDetailTabViewController.h"
#import "SRChatViewController.h"
#import "REVClusterMapView.h"
#import "REVClusterPin.h"
#import "REVClusterAnnotationView.h"
#import "MKMapView+ZoomLevel.h"
#import "SRPanoramaViewController.h"


@interface SREventMapViewController : ParentViewController<CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate, CalloutAnnotationViewDelegate,ASValueTrackingSliderDataSource,UIAlertViewDelegate>
{
    // Instance Variables
    NSArray *sourceArr;
    NSArray *searchArray;
    
    NSMutableArray *nearByUsersList;
    NSMutableArray *userProfileViewArr;
    NSArray *selectedCombinatonArr;
    NSArray *dotsUserPicsArr;
    
    CLLocation *myLocation;
    CLPlacemark *placemark;

    REVClusterPin *pin;
    //PinAnnotation *myPinAnnotation;
    
    // Flags
    BOOL isZoomIn;
    BOOL isSelectAnnotation;
    BOOL isSlide;
    BOOL isCheckZoomLvl;
    BOOL isCountAdd;
    
    // Server
    SRServerConnection *server;
    NSString *distanceUnit;
    NSInteger mapZoomLevel;
    float latestDistance;
    IBOutlet UIButton *btnSatelite;
    IBOutlet UIButton *btnStreet;
    IBOutlet UIButton *zoomInBtn,*zoomOutBtn,*streetViewBtn;
    CLLocationCoordinate2D panoramaLastLoc;
    BOOL isPanoramaLoaded;
}

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (strong, nonatomic) IBOutlet UIToolbar *locationToolBar;
@property (weak, nonatomic) IBOutlet REVClusterMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *layerView;
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *txfSearchField;
@property (strong, nonatomic) SRProfileImageView *imgUserProfile;
@property (weak, nonatomic)  IBOutlet UIButton *btnSatelite,*btnStreet;
// Slider view
@property (nonatomic, strong) IBOutlet UIView *progressContainerView;
@property (nonatomic, strong) IBOutlet UILabel *lblInPpl;
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider1;

// Other Properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;
@end


