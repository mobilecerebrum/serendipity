//
//  SREventRadarViewController.h
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <EventKit/EventKit.h>
#import "ASValueTrackingSlider.h"
#import "SRProfileImageView.h"
#import "SRExtraRadarCircle.h"
#import "SRRadarCircleView.h"
#import "SRRadarDotView.h"
#import "SRRadarFocusView.h"
#import "SRChatViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

@interface SREventRadarViewController : ParentViewController<CLLocationManagerDelegate,ASValueTrackingSliderDataSource,ASValueTrackingSliderDelegate,GMSAutocompleteTableDataSourceDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate>

{
    // Instance variable
    float currentDeviceBearing;
    float latestDistance;
    float settingDistance;
    
    float headingAngle;
    
//    NSTimer *detectCollisionTimer2;
    
    NSArray *nearbyEvents;
    NSArray *sourceArr;
    NSArray *eventUsers;
    
    NSMutableArray *dotsArr;
    NSMutableArray *dotsUserPicsArr;
    NSMutableArray *extEventArray;
    NSMutableDictionary *userDataDict;
    
    //Externla events array
    NSArray *extEventCatListArray;
    NSMutableArray *extEventVenueArray,*extEventCategoryArray,*extEventPerformerArray;
    BOOL isVenue;
    NSString *locationName;
    GMSPlace *placeDetail;
    
    //Picker
    UIPickerView *categoryPicker;
    NSString *selectedCategory;
    UIToolbar *toolBar;
    
    // View
    SRRadarCircleView *radarCircleView;
    SRRadarFocusView *radarFocusView;
    
    UIImageView *swipeView;
    UIView *swipeContainerView;
    UILabel *lblDistance;
    UILabel *lblNoPPl;
    
    CGPoint touchLocation;
    
    // Flag
    BOOL isOnce;
    BOOL isSwipedOnce;
    BOOL isPrivateEvent;
    
    // Server
    SRServerConnection *server;
    
    int pageNo;
    NSInteger sliderX;
    NSString *filterType,*distanceUnit;
    
    //Event Filters
    NSString *eventFilter;
    NSPredicate *eventPredicate;
    
    // Controllers
    UITableViewController *_resultsController;
    GMSAutocompleteTableDataSource *_tableDataSource;
    GMSAutocompleteFetcher* _fetcher;
}

// Properties
@property (weak, nonatomic) IBOutlet UIImageView *toolTipScrollViewImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet SRProfileImageView *bckProfileImg;
@property (nonatomic, strong) IBOutlet UIView *radarViewContainer;
@property (nonatomic, strong) IBOutlet UIView *northLineView;

@property (nonatomic, strong) IBOutlet SRProfileImageView *radarProfileImgView;
@property (nonatomic, strong) IBOutlet SRExtraRadarCircle *extraCircleView;
@property (nonatomic, strong) IBOutlet UIView *progressContainerView;
@property (nonatomic, strong) IBOutlet UILabel *lblInPpl;
@property (nonatomic, strong) IBOutlet UIButton *eventFilterBtn,*dateFilterBtn;
@property (nonatomic, strong) IBOutlet UISegmentedControl *filterSegment;
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *slider1;
@property (nonatomic, strong) UIView *radarRefreshOverlay;
@property (nonatomic, strong) IBOutlet UIButton *btnReduceMiles;
@property (nonatomic, strong) IBOutlet UIButton *btnIncreasedMiles;
@property (weak, nonatomic) IBOutlet UIImageView *milesSetPlusImg;
@property (weak, nonatomic) IBOutlet UIImageView *milesSetminusImg;


@property (strong, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UITextField *txfSearchField;
@property (weak, nonatomic) IBOutlet UITextField *txfFltredSearch;

// Properties
@property (weak) id delegate;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnButtonClick:(id)sender;
@property (strong, nonatomic) NSTimer *detectCollisionTimer2;
- (void)detectCollisions2:(NSTimer *)theTimer;
@end
