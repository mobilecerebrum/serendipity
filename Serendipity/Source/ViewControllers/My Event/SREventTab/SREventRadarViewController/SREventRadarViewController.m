//
//  SREventRadarViewController.m
//  Serendipity
//
//  Created by Dhanraj Bhandari on 14/01/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SREventRadarViewController.h"
#import "SRModalClass.h"
#import "SRUserTabViewController.h"
#import "SREventDetailTabViewController.h"
#import "NSDate+SRCustomDate.h"
#import "UIImageView+WebCache.h"
#import "CalloutAnnotationView.h"
#import "KxMenu.h"
#import "SRMapViewController.h"
#import "UISegmentedControl+Common.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

#define kKeyModifiedFlag @"flag"

@interface SREventRadarViewController ()

@property BOOL isExternalEventAPIInProgress;

@end

@implementation SREventRadarViewController

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SREventRadarViewController" bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
        dotsArr = [NSMutableArray array];
        dotsUserPicsArr = [[NSMutableArray alloc] init];

        userDataDict = [[NSMutableDictionary alloc] init];
        extEventVenueArray = [[NSMutableArray alloc] init];
        extEventPerformerArray = [[NSMutableArray alloc] init];
        extEventCategoryArray = [[NSMutableArray alloc] init];
        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getUpdateLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateHeadingAngle:)
                              name:kKeyNotificationUpdateHeadingValue
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getEventSucceed:)
                              name:kKeyNotificationGetEventSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getEventFailed:)
                              name:kKeyNotificationGetEventFailed object:nil];

        [defaultCenter addObserver:self
                          selector:@selector(makeServerCallToGetEventList)
                              name:kKeyNotificationNewEventAdded object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(filterEventUserListOnMap:)
                              name:kKeyNotificationEventMapRefreshOnSlider
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(refreshExternalList:)
                              name:kKeyNotificationCallExternalEvent
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(refreshExternalCatEventList:)
                              name:kKeyNotificationCallCategoryExternalEvent
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(refreshExternalVenueEventList:)
                              name:kKeyNotificationCallVenueExternalEvent
                            object:nil];
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];
    [self.filterSegment ensureiOS12Style];

    // Get external events
    pageNo = 1;
    // Get category list for external events
    [self getCategoryForExternalEvents:@"http://api.eventful.com/json/categories/list?app_key=R26GjGhPXtNsjCSz"];

    // Set the minimum range
    latestDistance = kKeyMaximumRadarMile;
    settingDistance = kKeyMaximumRadarMile;
    //Prepare Slider Data
    // customize slider 1
    self.slider1.maximumValue = latestDistance;
    self.slider1.minimumValue = kKeyMinimumRadarMile;
    self.slider1.value = latestDistance;

    self.slider1.popUpViewCornerRadius = 5.0;
    //[self.slider1 setPopUpViewCornerRadius:16.0];
    [self.slider1 setMaxFractionDigitsDisplayed:0];
    self.slider1.popUpViewColor = [UIColor colorWithRed:1 green:0.588 blue:0 alpha:1];
    self.slider1.font = [UIFont fontWithName:@"GillSans-Bold" size:22];
    self.slider1.textColor = [UIColor whiteColor];
    self.slider1.popUpViewWidthPaddingFactor = 1.7; //1.7
    self.slider1.delegate = self;

    NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
    NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

    NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
    NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:@"miles" attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
    [muAtrStr appendAttributedString:atrStr1];
    [muAtrStr appendAttributedString:atrStr2];


    // Prepare radar view
    radarCircleView = [[SRRadarCircleView alloc] initWithFrame:CGRectMake(0, 0, self.radarViewContainer.bounds.size.width, self.radarViewContainer.bounds.size.height)];
    radarCircleView.points = CGPointMake(self.radarProfileImgView.frame.origin.x, self.radarProfileImgView.frame.origin.y);
    radarCircleView.layer.contentsScale = [UIScreen mainScreen].scale;
    self.radarViewContainer.layer.contentsScale = [UIScreen mainScreen].scale;
    [self.northLineView removeFromSuperview];
    [radarCircleView addSubview:self.northLineView];
    [self.radarViewContainer addSubview:radarCircleView];

    radarFocusView = [[SRRadarFocusView alloc] initWithFrame:CGRectMake(18, 18, 265, 265)];
    radarFocusView.layer.contentsScale = [UIScreen mainScreen].scale;
    radarFocusView.alpha = 0.68;
    [self.radarViewContainer addSubview:radarFocusView];

    // Bring view to front
    [self.view bringSubviewToFront:self.radarProfileImgView];
    [self.view bringSubviewToFront:self.northLineView];

    // Set profile pic
    self.radarProfileImgView.layer.cornerRadius = self.radarProfileImgView.frame.size.width / 2;
    self.radarProfileImgView.layer.masksToBounds = YES;
    [_radarRefreshOverlay setHidden:YES];
    // Add long press guesture to refresh radar centered ProfileImgView
    UILongPressGestureRecognizer *radarLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressRadar:)];
    radarLongPress.minimumPressDuration = 1.0f;
    self.radarProfileImgView.userInteractionEnabled = YES;
    //    [self.extraCircleView addGestureRecognizer:radarLongPress];
    [self.radarProfileImgView addGestureRecognizer:radarLongPress];
    [self.extraCircleView bringSubviewToFront:self.radarViewContainer];
    [self.radarViewContainer bringSubviewToFront:self.radarProfileImgView];
    [self.radarProfileImgView bringSubviewToFront:self.radarProfileImgView.superview];

    // Add View
    // Add View
    swipeContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];

    swipeView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, self.view.frame.size.height)];
    swipeView.alpha = 0.9;
    swipeView.backgroundColor = [UIColor colorWithRed:115 / 255.0 green:70 / 255.0 blue:14 / 255.0 alpha:0.6];

    lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 120, 40)];
    lblDistance.font = [UIFont fontWithName:kFontHelveticaBold size:26.0];
    lblDistance.textColor = [UIColor whiteColor];

    lblNoPPl = [[UILabel alloc] initWithFrame:CGRectMake(0, lblDistance.frame.origin.y + 70, 140, 22)];
    lblNoPPl.textColor = [UIColor whiteColor];
    lblNoPPl.font = [UIFont fontWithName:kFontHelveticaMedium size:18.0];

    // Add the views to container view
    [swipeContainerView addSubview:swipeView];
    [swipeContainerView addSubview:lblDistance];
    [swipeContainerView addSubview:lblNoPPl];

    swipeContainerView.alpha = 0.7;
    swipeContainerView.hidden = YES;

    [self.view addSubview:swipeContainerView];

    // Pan gesture on progressContainerView
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.view addGestureRecognizer:panGesture];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDotTapped:)];
    [radarCircleView addGestureRecognizer:tapGestureRecognizer];

    //Add Tap Gesture on dotView
    UITapGestureRecognizer *viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapAction:)];
    viewTapGesture.delegate = self;
    self.view.userInteractionEnabled = YES;
    viewTapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:viewTapGesture];
    // Add long press guesture on + button
    UILongPressGestureRecognizer *longPressPlus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressTap:)];
    longPressPlus.minimumPressDuration = 1.0f;
    self.btnIncreasedMiles.tag = 1;
    [self.btnIncreasedMiles addGestureRecognizer:longPressPlus];

    // Add long press guesture on - button
    UILongPressGestureRecognizer *longPressMinus = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressTap:)];
    longPressMinus.minimumPressDuration = 1.0f;
    self.btnReduceMiles.tag = 2;
    [self.btnReduceMiles addGestureRecognizer:longPressMinus];

    // Add leftview to search textfield
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    container.backgroundColor = [UIColor clearColor];

    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(20, 6, 17, 17)];
    searchIcon.image = [UIImage imageNamed:@"ic_search"];
    [container addSubview:searchIcon];

    self.txfSearchField.leftView = container;
    self.txfSearchField.leftViewMode = UITextFieldViewModeAlways;
    self.txfSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Event Location" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txfSearchField.layer.cornerRadius = 5;

    //search by filter button
    UIView *container1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    container1.backgroundColor = [UIColor clearColor];

    UIImageView *searchIcon1 = [[UIImageView alloc] initWithFrame:CGRectMake(20, 6, 17, 17)];
    searchIcon1.image = [UIImage imageNamed:@"ic_search"];
    [container1 addSubview:searchIcon1];

    self.txfFltredSearch.leftView = container1;
    self.txfFltredSearch.leftViewMode = UITextFieldViewModeAlways;
    self.txfFltredSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Eventful Title or Keyword" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.txfFltredSearch.layer.cornerRadius = 5;


    // ScrollView
    self.scrollView.hidden = YES;
    self.toolTipScrollViewImg.hidden = YES;

    // Auto complete
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = _tableDataSource;
    _resultsController.tableView.dataSource = _tableDataSource;


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(entersBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];

    [[NSNotificationCenter defaultCenter]       addObserver:self selector:@selector(updateSettings) name:
            UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];


    //display current city in location textfield
    [self displayCurrentCity];
}

- (void)entersBackground {

    [_detectCollisionTimer2 invalidate];

    _detectCollisionTimer2 = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];

    [[NSNotificationCenter defaultCenter]       addObserver:self selector:@selector(updateSettings) name:
            UIApplicationDidBecomeActiveNotification object:[UIApplication sharedApplication]];
}


- (void)updateSettings {
    [_detectCollisionTimer2 invalidate];
    _detectCollisionTimer2 = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self
                                                            selector:@selector(detectCollisions2:)
                                                            userInfo:nil
                                                             repeats:YES];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(entersBackground) name:UIApplicationDidEnterBackgroundNotification object:[UIApplication sharedApplication]];


}

// -----------------------------------------------------------------------------------------------
// viewWillAppear
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    (APP_DELEGATE).topBarView.hidden = YES;
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;

    [self setmileslableData];

}
// -----------------------------------------------------------------------------------------------
// viewDidAppear

- (void)viewDidAppear:(BOOL)animated {
    // Call super
    [super viewDidAppear:NO];

    // Call the radar set up methods after some delay
    if (!isOnce) {
        isOnce = YES;
        [self setDisplayForRadar];
        [self makeServerCallToGetEventList];
    }
}

//-----------------------------------------------------------------------------------------------------------------------
// viewWillDisappear:
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.scrollView setHidden:YES];
    [self.toolTipScrollViewImg setHidden:YES];
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarRefreshOnSlider object:[NSString stringWithFormat:@"%f", latestDistance]];

}


#pragma mark
#pragma mark - Other Methods
#pragma mark

//
//--------------------------------------------------------------------------
// refreshRadarBtnClick
- (void)refreshRadarBtnClick {
    [APP_DELEGATE showActivityIndicator];
    [self makeServerCallToGetEventList];
}


// -------------------------------------------------------------------------------------------------
// addViewsOnScrollView:

- (void)addViewsOnScrollView:(NSArray *)inArr {

    CGRect frame;
    for (UIView *subview in[self.scrollView subviews]) {
        if ([subview isKindOfClass:[CalloutAnnotationView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (NSUInteger i = 0; i < [inArr count]; i++) {
        // Get profile pic of users
        NSDictionary *userDict = inArr[i];
        CalloutAnnotationView *annotationView = [[CalloutAnnotationView alloc] init];
        if (i == 0) {
            frame = CGRectMake(0, 11, 130, 200);
        } else {
            frame = CGRectMake(frame.origin.x + 126, 11, 130, 200);
        }
        annotationView.frame = frame;
        annotationView.toolTipImage.hidden = YES;

        // Profile image
        UIBezierPath *maskPath;
        maskPath = [UIBezierPath bezierPathWithRoundedRect:((CalloutAnnotationView *) annotationView).userProfileImage.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft) cornerRadii:CGSizeMake(8.0, 8.0)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = ((CalloutAnnotationView *) annotationView).userProfileImage.bounds;
        maskLayer.path = maskPath.CGPath;
        ((CalloutAnnotationView *) annotationView).userProfileImage.layer.mask = maskLayer;

        if (userDict[kKeyMediaImage] != nil && [userDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {

            if ([userDict[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [userDict[kKeyMediaImage] objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl;
                    if ([[userDict valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
                        imageUrl = [NSString stringWithFormat:@"%@", imageName];
                    } else
                        imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                    [((CalloutAnnotationView *) annotationView).userProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"events_avatar1.png"];

            } else {
                ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"events_avatar1.png"];
                ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
            }
        } else {
            ((CalloutAnnotationView *) annotationView).userProfileImage.image = [UIImage imageNamed:@"events_avatar1.png"];
            ((CalloutAnnotationView *) annotationView).userProfileImage.clipsToBounds = YES;
        }


        //Favourite
        ((CalloutAnnotationView *) annotationView).favUserImage.hidden = TRUE;

        //Invisible
        ((CalloutAnnotationView *) annotationView).invisibleUserImage.hidden = TRUE;

        //Distance
        ((CalloutAnnotationView *) annotationView).lblDistance.hidden = TRUE;

        //Degree
        ((CalloutAnnotationView *) annotationView).lblDegree.hidden = TRUE;

        ((CalloutAnnotationView *) annotationView).lblProfileName.text = [userDict valueForKey:kKeyName];

        //Date & time
        NSString *dateString = [NSString stringWithFormat:@"%@ %@", [userDict valueForKey:kKeyEvent_Date], [userDict valueForKey:kKeyEvent_Time]];
        NSDate *eventDate = [NSDate getEventDateFromString:dateString];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
        [formatter setDateFormat:@"MMM dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSString *monthString = [formatter stringFromDate:eventDate];
        [formatter setDateFormat:@"HH:mm"];
        NSString *timeStr = [formatter stringFromDate:eventDate];
        ((CalloutAnnotationView *) annotationView).lblOccupation.text = [NSString stringWithFormat:@"%@ %@ at %@", [userDict valueForKey:kKeyEvent_Type], monthString, timeStr];
        ((CalloutAnnotationView *) annotationView).lblOccupation.frame = CGRectMake(((CalloutAnnotationView *) annotationView).lblOccupation.frame.origin.x, ((CalloutAnnotationView *) annotationView).lblOccupation.frame.origin.y, ((CalloutAnnotationView *) annotationView).frame.size.width, ((CalloutAnnotationView *) annotationView).lblOccupation.frame.size.height);

        // Image for group type
        //As Dj showing all event will be addable to calender
        if (![SRModalClass isEventExist:userDict[kKeyId]]) {
            ((CalloutAnnotationView *) annotationView).imgViewGrp.backgroundColor = [UIColor orangeColor];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.image = [UIImage imageNamed:@"calendar-plus_25.png"];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapEventAddCalender = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addEventToCalender:)];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.tag = [[userDict valueForKey:kkeyNumbering] integerValue];
            [((CalloutAnnotationView *) annotationView).imgViewGrp addGestureRecognizer:tapEventAddCalender];
        } else {
            ((CalloutAnnotationView *) annotationView).imgViewGrp.image = [UIImage imageNamed:@"calendar-check_25.png"];
            ((CalloutAnnotationView *) annotationView).imgViewGrp.backgroundColor = [UIColor blackColor];
        }
        [((CalloutAnnotationView *) annotationView).imgViewGrp setContentMode:UIViewContentModeCenter];
        ((CalloutAnnotationView *) annotationView).imgViewGrp.layer.cornerRadius = ((CalloutAnnotationView *) annotationView).imgViewGrp.frame.size.width / 2.0;

        if ([[userDict valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
            [((CalloutAnnotationView *) annotationView).pingButton setHidden:YES];
        } else {
            // Add Shadow to btnPing
            [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
            [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
            [((CalloutAnnotationView *) annotationView).pingButton.layer setShadowOpacity:8.5];
            [((CalloutAnnotationView *) annotationView).pingButton addTarget:self action:@selector(actionOnPingBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        }

        //Add Compass Button
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowOffset:CGSizeMake(5, 5)];
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
        [((CalloutAnnotationView *) annotationView).compassButton.layer setShadowOpacity:8.5];

        CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
                server.myLocation.coordinate.longitude};

        NSDictionary *userLocDict = userDict[kKeyLocation];
        CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};

        [((CalloutAnnotationView *) annotationView).compassButton addTarget:self action:@selector(compassButtonAction:) forControlEvents:UIControlEventTouchUpInside];


        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            annotationView.lblCompassDirection.text = @"N";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            annotationView.lblCompassDirection.text = @"E";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            annotationView.lblCompassDirection.text = @"S";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            annotationView.lblCompassDirection.text = @"W";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            annotationView.lblCompassDirection.text = @"NE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            annotationView.lblCompassDirection.text = @"NW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            annotationView.lblCompassDirection.text = @"SE";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            annotationView.lblCompassDirection.text = @"SW";
            [annotationView.compassButton setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }

        // Add Tap Gesture on dotView
        UITapGestureRecognizer *annotationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(windowTapAction:)];
        annotationTapGesture.numberOfTapsRequired = 1;
        [((CalloutAnnotationView *) annotationView).userProfileImage addGestureRecognizer:annotationTapGesture];
        ((CalloutAnnotationView *) annotationView).userDict = userDict;


        //if users count is one
        if (inArr.count == 1) {
            CGRect frame = annotationView.frame;
            frame.origin.x = ((self.scrollView.frame.size.width / 2) - (annotationView.frame.size.width / 2));
            annotationView.frame = frame;
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = YES;
        } else {
            // Add subview to scroll view
            [self.scrollView addSubview:annotationView];
            self.toolTipScrollViewImg.hidden = NO;
        }
        if ((frame.origin.x + 130) > SCREEN_WIDTH) {
            self.scrollView.scrollEnabled = YES;
        } else
            self.scrollView.scrollEnabled = NO;

        // The content size
        self.scrollView.contentSize = CGSizeMake(frame.origin.x + frame.size.width + 15, 200);
        self.scrollView.hidden = NO;
    }
}

// -------------------------------------------------------------------------------
// addEventToCalender:
- (void)addEventToCalender:(UITapGestureRecognizer *)tapRecognizer {
    UIImageView *imgView = (UIImageView *) tapRecognizer.view;
    [imgView setImage:[UIImage imageNamed:@"calendar-check_25.png"]];
    imgView.backgroundColor = [UIColor blackColor];
    imgView.userInteractionEnabled = NO;
    for (int i = 0; i < nearbyEvents.count; i++) {
        if (tapRecognizer.view.tag == [[nearbyEvents[i] valueForKey:kkeyNumbering] integerValue]) {
            [SRModalClass saveEventToCalender:nearbyEvents[i]];
        }
    }
}
// -------------------------------------------------------------------------------
// makeRoundedImage:

- (UIImage *)makeRoundedImage:(UIImage *)image
                       radius:(float)radius; {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = 100 / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // Return
    return roundedImage;
}

// -------------------------------------------------------------------------------------------------
// makeServerCallToGetUsersList

- (void)makeServerCallToGetEventList {

    //[APP_DELEGATE showActivityIndicator];
    NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
    NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
    NSInteger distance = kKeyMaximumRadarMile;

    NSString *urlStr = [NSString stringWithFormat:@"%@distance=%@&lat=%@&lon=%@", kKeyClassEvent, [NSString stringWithFormat:@"%ldmi", (long) distance], latitude, longitude];
    [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    // Hide refresh overlay if exists
    for (UIView *overlay in self.tabBarController.view.subviews) {
        if (overlay.tag == 100000)
            [overlay setHidden:YES];
    }

}

#pragma mark Get User Current City

- (void)displayCurrentCity {
    // Performers
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    [geocoder reverseGeocodeLocation:loc
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       CLPlacemark *placemark = placemarks[0];

        self->locationName = placemark.subAdministrativeArea;
        [self->_txfSearchField setText:self->locationName];

                       //Filter if user current address get
        if (self->sourceArr.count > 0) {
                           [self searchEventForDateOrLoc:self->_txfSearchField.text];
                       }
//         }
                   }];

}

#pragma mark
#pragma mark - Radar Methods
#pragma mark

// -----------------------------------------------------------------------------------------------
// getHeadingForDirectionFromCoordinate:

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc {
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);

    float degree = radiandsToDegrees(atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng)));

    if (degree >= 0) {
        return -degree;
    } else {
        return -(360 + degree);
    }

    return degree;
}

//                                   -----------------------------------------------------------------------------------------------
// rotateArcsToHeading:

- (void)rotateArcsToHeading:(CGFloat)angle {
    // rotate the circle to heading degree
    radarCircleView.transform = CGAffineTransformMakeRotation(angle);

    // rotate all dots to opposite angle to keep the profile image straight up
    for (SRRadarDotView *dot in dotsArr) {
        dot.transform = CGAffineTransformMakeRotation(-angle);
    }
}

// ---------------------------------------------------------------------------------------
// setDisplayForRadar

- (void)setDisplayForRadar {
    // Spin the view of focus
    CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    spin.duration = 1.5;
    spin.toValue = [NSNumber numberWithFloat:M_PI];
    spin.cumulative = YES;
    spin.removedOnCompletion = NO;
    spin.repeatCount = MAXFLOAT;
    [radarFocusView.layer addAnimation:spin forKey:@"spinRadarView"];
}

// -----------------------------------------------------------------------------------------------
// displayUsersDotsOnRadar

- (void)displayUsersDotsOnRadar {
    // empty the existing dots from radar
    [APP_DELEGATE hideActivityIndicator];
    for (SRRadarDotView *dot in dotsArr) {
        [dot removeFromSuperview];
    }
    [dotsArr removeAllObjects];

    // This method should be called after successful return of JSON array from your server-side service
    [self renderUsersOnRadar:nearbyEvents];
}

// -----------------------------------------------------------------------------------------------
// renderUsersOnRadar:

- (void)renderUsersOnRadar:(NSArray *)users {
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
            server.myLocation.coordinate.longitude};

    // The last user in the nearbyEvents list is the farthest
    float maxDistance = [[[users lastObject] valueForKey:kKeyDistance] floatValue];

    // Remove all objects from array
    [dotsUserPicsArr removeAllObjects];

    // Add users dots
    for (NSDictionary *user in users) {
        SRRadarDotView *dot = [[SRRadarDotView alloc] initWithFrame:CGRectMake(0, 0, 12.0, 12.0)];
        dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
        dot.userProfile = user;

        if ([user[kKeyExternalEvent] isEqualToString:@"1"]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-black-30px.png"];
        } else if ([SRModalClass isEventExist:user[kKeyId]]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-white-20px.png"];
        } else if ([user[kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-white-dot-20px.png"];
        } else
            dot.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];

        CLLocationCoordinate2D userLoc = {[[user valueForKey:kKeyLattitude] floatValue], [[user valueForKey:kKeyRadarLong] floatValue]};
        float bearing = [self getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        dot.bearing = @(bearing);

        float d = [[user valueForKey:kKeyDistance] floatValue];

        float distance;

        if (maxDistance != 0) {
            if (37 + d >= 130) {
                distance = 130;
            } else {
                distance = (37 + d);
            }
        } else {
            distance = (double) 37;
        }
        dot.distance = @(distance);
        dot.initialDistance = @(distance); // relative distance
        dot.userDistance = @(d);
        dot.zoomEnabled = NO;
        dot.userInteractionEnabled = NO;

        float left = (float) (148 + distance * sin(degreesToRadians(-bearing)));
        float top = (float) (148 - distance * cos(degreesToRadians(-bearing)));

        [self rotateDot:dot fromBearing:currentDeviceBearing toBearing:bearing atDistance:distance];

        dot.frame = CGRectMake(left, top, 12.0, 12.0);
        dot.initialFrame = dot.frame;
        [radarCircleView addSubview:dot];
        [dotsArr addObject:dot];

        dot.transform = CGAffineTransformMakeRotation(-headingAngle);
    }
    // Start timer to detect collision with radar line and blink
    [_detectCollisionTimer2 invalidate];
    _detectCollisionTimer2 = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self selector:@selector(detectCollisions2:)
                                                            userInfo:nil repeats:YES];
}

#pragma mark
#pragma mark - Radar&Dot Collisions Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// detectCollisions:

- (void)detectCollisions2:(NSTimer *)theTimer {
    float radarLineRotation = radiandsToDegrees([[radarFocusView.layer.presentationLayer valueForKeyPath:@"transform.rotation.z"] floatValue]);

    if (radarLineRotation >= 0) {
        radarLineRotation += 90;
    } else {
        if (radarLineRotation > -90) {
            radarLineRotation = 90 + radarLineRotation;
        } else
            radarLineRotation = 270 + (radarLineRotation + 180);
    }

    for (int i = 0; i < [dotsArr count]; i++) {
        SRRadarDotView *dot = dotsArr[i];
        float dotBearing = [dot.bearing floatValue];
        if (dotBearing < 0) {
            dotBearing = -dotBearing;
        }
        dotBearing = dotBearing - currentDeviceBearing;
        if (dotBearing < 0) {
            dotBearing = dotBearing + 360;
        }

        // Now get diffrence between dotbearing and radarline
        float diffrence;
        if (dotBearing > radarLineRotation) {
            diffrence = dotBearing - radarLineRotation;
        } else
            diffrence = radarLineRotation - dotBearing;

        if (diffrence <= 5) {
            [self pulse:dot];
        }
    }
}

// -----------------------------------------------------------------------------------------------
// pulse:

- (void)pulse:(SRRadarDotView *)dot {
    /*if ([dot.layer.animationKeys containsObject:@"pulse"] || dot.zoomEnabled) {
        // view is already animating. so return
        return;
     }*/

    // Dot images
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.duration = 1.0;
    pulse.toValue = @1.0F;
    pulse.autoreverses = NO;
    dot.layer.contentsScale = [UIScreen mainScreen].scale; // Retina
    [dot.layer addAnimation:pulse forKey:@"pulse"];


    if (dot.userProfile[kKeyMediaImage] != nil && [dot.userProfile[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        CGRect frame = dot.initialFrame;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.frame = frame;

        frame = dot.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 50;
        frame.size.height = 50;
        dot.imgView.frame = frame;

        dot.imgView.contentMode = UIViewContentModeScaleToFill;
        dot.imgView.layer.cornerRadius = dot.imgView.frame.size.width / 2.0;
        dot.imgView.clipsToBounds = YES;
        dot.isImageApplied = YES;

        NSDictionary *profileDict = dot.userProfile[kKeyMediaImage];
        NSString *imageName = profileDict[kKeyImageName];
        if ([imageName length] > 0) {
            NSString *imageUrl;
            if ([dot.userProfile[kKeyExternalEvent] isEqualToString:@"1"]) {
                imageUrl = [NSString stringWithFormat:@"%@", imageName];
            } else
                imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

            [dot.imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        } else
            dot.imgView.image = nil;
    }
    [self performSelector:@selector(removeImageSetDefault:) withObject:dot afterDelay:0.5];
}

// -----------------------------------------------------------------------------------------------
// removeImageSetDefault:

- (void)removeImageSetDefault:(SRRadarDotView *)inDotView {
    if (inDotView.isImageApplied) {
        // Remove the animation
        [inDotView.layer removeAllAnimations];

        CGRect frame = inDotView.initialFrame;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.frame = frame;

        frame = inDotView.imgView.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.width = 12;
        frame.size.height = 12;
        inDotView.imgView.frame = frame;

        inDotView.imgView.contentMode = UIViewContentModeScaleAspectFill;
        NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:inDotView.userProfile];


        if ([updatedDict[kKeyExternalEvent] isEqualToString:@"1"]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-black-30px.png"];
        } else if ([SRModalClass isEventExist:updatedDict[kKeyId]]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-white-20px.png"];
        } else if ([updatedDict[kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-white-dot-20px.png"];
        } else
            inDotView.imgView.image = [UIImage imageNamed:@"radar-pin-orange-30px.png"];

        inDotView.isImageApplied = NO;
        inDotView.imgView.clipsToBounds = NO;
    }
}

#pragma mark
#pragma mark - Radar Dot Translate Methods
#pragma mark

// -----------------------------------------------------------------------------------------------
// rotateDot: fromBearing: atDistance:

- (void)rotateDot:(SRRadarDotView *)dot fromBearing:(CGFloat)fromDegrees toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CGMutablePathRef path = CGPathCreateMutable();

    CGPathAddArc(path, nil, 150, 150, distance, degreesToRadians(fromDegrees), degreesToRadians(degrees), YES);

    CAKeyframeAnimation *theAnimation;

    // Animation object for the key path
    theAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    theAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    theAnimation.path = path;
    CGPathRelease(path);

    // Set the animation properties
    theAnimation.duration = 3;
    theAnimation.removedOnCompletion = NO;
    theAnimation.repeatCount = 0;
    theAnimation.autoreverses = NO;
    theAnimation.fillMode = kCAFillModeBackwards;
    theAnimation.cumulative = YES;

    CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);
    dot.layer.position = newPosition;
    [dot.layer addAnimation:theAnimation forKey:@"rotateDot"];
}

// -----------------------------------------------------------------------------------------------
// translateDot:

- (void)translateDot:(SRRadarDotView *)dot toBearing:(CGFloat)degrees atDistance:(CGFloat)distance {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];

    [animation setFromValue:[NSValue valueWithCGPoint:[[dot.layer.presentationLayer valueForKey:@"position"] CGPointValue]]];

    //CGPoint newPosition = CGPointMake(distance * cos(degreesToRadians(degrees)) + 148, distance * sin(degreesToRadians(degrees)) + 148);

    float left = (float) (148 + distance * sin(degreesToRadians(-degrees)));
    float top = (float) (148 - distance * cos(degreesToRadians(-degrees)));
    CGPoint newPosition = CGPointMake(left, top);
    [animation setToValue:[NSValue valueWithCGPoint:newPosition]];

    [animation setDuration:0.3f];
    animation.fillMode = kCAFillModeBackwards;
    animation.autoreverses = NO;
    animation.repeatCount = 0;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];

    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    [alphaAnimation setDuration:0.5f];
    alphaAnimation.fillMode = kCAFillModeBackwards;
    alphaAnimation.autoreverses = NO;
    alphaAnimation.repeatCount = 0;
    alphaAnimation.removedOnCompletion = NO;
    alphaAnimation.cumulative = YES;
    alphaAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [dot.layer addAnimation:animation forKey:@"translateDot"];
}


#pragma mark
#pragma mark - Slider update
#pragma mark
#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (void)setmileslableData {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }

    self.slider1.maximumValue = distancevalue;
    self.slider1.value = self.slider1.value;
    settingDistance = distancevalue;

    NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
    NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

    NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

    NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
    NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:@"miles" attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
    [muAtrStr appendAttributedString:atrStr1];
    [muAtrStr appendAttributedString:atrStr2];
}

- (void)updateSliderValuesToRadar:(NSString *)values {
    //NSInteger sliderValue = [values intValue];
}

// -----------------------------------------------------------------------------------------------
// sliderWillDisplayPopUpView

- (void)sliderWillDisplayPopUpView:(ASValueTrackingSlider *)slider; {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }

    if (slider.value >= distancevalue) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.max_Distance.text", @"")];
    } else {
        latestDistance = slider.value;
        [self calculateAndGetNewUsers];
    }
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark
// -----------------------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnClick:(id)sender {
    //BOOL isIncreased = NO;
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    NSString *strMilesTempData = [userdef valueForKey:kKeyTempMiles_Data];

    if (sender == self.btnIncreasedMiles) {
        if (strMilesData == nil) {
            strMilesData = [NSString stringWithFormat:@"%f", kKeyMaximumRadarMile];
        }
        if (latestDistance >= [strMilesData floatValue] && [strMilesTempData integerValue] >= [strMilesData integerValue]) {
            [SRModalClass showAlert:NSLocalizedString(@"alert.max_Distance.text", @"")];
        } else {
            latestDistance += 30;
            if (latestDistance >= kKeyMaximumRadarMile) {
                latestDistance = kKeyMaximumRadarMile;
            }
            sliderX = latestDistance;
            [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
            [userdef synchronize];
            [self calculateAndGetNewUsers];
            swipeContainerView.hidden = NO;
        }
    } else {
        if (latestDistance <= kKeyMinimumRadarMile) {
            [SRModalClass showAlert:NSLocalizedString(@"alert.min_Distance.text", @"")];
            // swipeContainerView.hidden = YES;
        } else {
            latestDistance -= 30;
            if (latestDistance <= 30) {
                latestDistance = kKeyMinimumRadarMile;
                [self.slider1 setValue:0.0 animated:YES];
            }
            sliderX = latestDistance;
            //             [self.slider1 setValue:latestDistance animated:YES];
            [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
            [userdef synchronize];
            // Call method
            [self calculateAndGetNewUsers];
            swipeContainerView.hidden = NO;
        }
    }

    [UIView animateWithDuration:0.0 delay:0.9 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self->swipeContainerView.hidden = NO;
        self->sliderX = ((self.slider1.value * self.slider1.frame.size.width) / self.slider1.maximumValue) + self.slider1.frame.origin.x;
        if (self->sliderX > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
            self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                } else
                    self->swipeView.frame = CGRectMake(0, 0, self->sliderX, self.view.frame.size.height);

        if (self->sliderX >= (SCREEN_WIDTH / 2)) {
            self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 140, self->lblDistance.frame.origin.y, 120, 40);
            self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 160, self->lblDistance.frame.origin.y + 40, 140, 22);
                } else {
                    self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y, 140, 40);
                    self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y + 40, 140, 22);
                }
                [self.view bringSubviewToFront:self.progressContainerView];
            }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(hideSwipeView) withObject:self afterDelay:0.4];

                     }];
}

// -----------------------------------------------------------------------------------------------
// hideSwipeView:
- (void)hideSwipeView {
    swipeContainerView.hidden = YES;
}
// -----------------------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnButtonClick:(UIButton *)sender {
    if (sender == self.eventFilterBtn) {
        if (self.filterSegment.isHidden) {
            self.filterSegment.hidden = NO;
        } else {
            (APP_DELEGATE).isEventFilterApplied = NO;
            self.filterSegment.hidden = YES;
            nearbyEvents = sourceArr;
            if (settingDistance == latestDistance) {
                [self displayUsersDotsOnRadar];
            } else {
                [self displayUsersDotsOnRadar];
                [self calculateAndGetNewUsers];
            }
            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];
            // Show text for no people
            self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];
        }
    } else if (sender == self.dateFilterBtn) {
        UIImage *image1 = [UIImage imageNamed:@"mainmenu-events.png"];
        if ([[self.dateFilterBtn currentImage] isEqual:image1]) {
            // Set datepicker as inputview
            [self.txfSearchField resignFirstResponder];
            if (self.txfSearchField.text.length > 0) {
                self.txfSearchField.text = @"";
            }
            [self.dateFilterBtn setImage:[UIImage imageNamed:@"location-white.png"] forState:UIControlStateNormal];
            self.txfSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Select date" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
            UIDatePicker *datePicker = [[UIDatePicker alloc] init];
            [datePicker setDate:[NSDate date]];
            datePicker.datePickerMode = UIDatePickerModeDate;
            [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
            [_txfSearchField setInputView:datePicker];
        } else {
            [self.txfSearchField resignFirstResponder];
            if (self.txfSearchField.text.length > 0) {
                self.txfSearchField.text = @"";
            }
            [self.dateFilterBtn setImage:[UIImage imageNamed:@"mainmenu-events.png"] forState:UIControlStateNormal];
            self.txfSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@" Event Location" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
            _txfSearchField.inputView = nil;
        }

    } else if (sender == self.btnClear) {
        _txfSearchField.text = @"";
    }
}

- (void)dateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker *) _txfSearchField.inputView;
    [picker setMinimumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];

    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _txfSearchField.text = [NSString stringWithFormat:@"%@", dateString];
    [self searchEventForDateOrLoc:dateString];
}

// -----------------------------------------------------------------------------------------------
// actionOnSegmentedControl:
- (IBAction)actionOnSegmentedControl:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 1) {

        //Set color to selected segment index
        for (int i=0; i<[sender.subviews count]; i++)
        {
            if ([[sender.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && [[sender.subviews objectAtIndex:i]isSelected])
            {
                [[sender.subviews objectAtIndex:i] setTintColor:[UIColor orangeColor]];
            }
            if ([[sender.subviews objectAtIndex:i] respondsToSelector:@selector(isSelected)] && ![[sender.subviews objectAtIndex:i] isSelected])
            {
                [[sender.subviews objectAtIndex:i] setTintColor:[sender tintColor]];
            }
        }
        sender.selectedSegmentIndex = UISegmentedControlNoSegment;
        isPrivateEvent = YES;

        NSArray *menuItems =
                @[
                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.you_invited_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.your_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.popular_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],
                ];

        [KxMenu showMenuInView:self.view
                      fromRect:sender.frame
                     menuItems:menuItems];
    } else {
        isPrivateEvent = NO;

        //Set color to selected segment index
//        for (int i = 0; i < [sender.subviews count]; i++) {
//            if ([sender.subviews[i] isSelected]) {
//                UIColor *tintcolor = [UIColor orangeColor];
//                [sender.subviews[i] setTintColor:tintcolor];
//            } else {
//                [sender.subviews[i] setTintColor:nil];
//            }
//        }
        sender.selectedSegmentIndex = UISegmentedControlNoSegment;

        NSArray *menuItems =
                @[
                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.you_invited_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.your_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.popular_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"header.title.external_event.txt", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.title.event_performer_filter.text", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.title.event_venue_filter.text", @"") image:nil target:self action:@selector(eventFilterApplied:)],


                        [KxMenuItem menuItem:NSLocalizedString(@"lbl.title.event_category_filter.text", @"") image:nil target:self action:@selector(eventFilterApplied:)],

                ];

        [KxMenu showMenuInView:self.view
                      fromRect:sender.frame
                     menuItems:menuItems];
    }
}

- (void)eventFilterApplied:(id)sender {
    (APP_DELEGATE).isEventFilterApplied = YES;
    KxMenuItem *clickedFilter = sender;
    NSMutableArray *filterArray = [NSMutableArray array];

    if (isPrivateEvent) {
        if ([clickedFilter.title isEqualToString:NSLocalizedString(@"header.title.you_invited_event.txt", @"")]) {
            //Groups you have been invited
            NSMutableArray *inviteEventArray = [NSMutableArray array];
            for (NSDictionary *dict in sourceArr) {
                NSArray *eventMembers = [dict valueForKey:kKeyEvent_Attendees];
                if ([eventMembers isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *userDict in eventMembers) {
                        if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && [[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"] && [[dict valueForKey:kKeyIs_Public] isEqualToString:@"0"]) {
                            [inviteEventArray addObject:dict];
                        }
                    }
                }
            }
            filterArray = inviteEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];

        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"header.title.your_event.txt", @"")]) {
            //Your groups
            NSMutableArray *myEventArray = [NSMutableArray array];
            for (NSDictionary *dict in sourceArr) {
                NSArray *eventMembers = [dict valueForKey:kKeyEvent_Attendees];
                if ([eventMembers isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *userDict in eventMembers) {
                        if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && [[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"] && [[dict valueForKey:kKeyIs_Public] isEqualToString:@"0"]) {
                            [myEventArray addObject:dict];
                        }
                    }
                }
            }

            filterArray = myEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"header.title.popular_event.txt", @"")]) {
            // Popular groups array
            NSMutableArray *popularEventArray = [NSMutableArray array];
            for (NSDictionary *dict in sourceArr) {
                if ([[dict valueForKey:kKeyEvent_Attendees] isKindOfClass:[NSArray class]]) {
                    if (([dict[kKeyEvent_Attendees] count] >= 25 && [[dict valueForKey:kKeyIs_Public] isEqualToString:@"0"])) {
                        [popularEventArray addObject:dict];
                    }
                }
            }
            filterArray = popularEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        }
    } else {
        if ([clickedFilter.title isEqualToString:NSLocalizedString(@"header.title.you_invited_event.txt", @"")]) {
            //events you have been invited
            NSMutableArray *inviteEventArray = [NSMutableArray array];
            for (NSDictionary *dict in sourceArr) {
                NSArray *eventMembers = [dict valueForKey:kKeyEvent_Attendees];
                if ([eventMembers isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *userDict in eventMembers) {
                        if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && [[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"0"] && [[dict valueForKey:kKeyIs_Public] isEqualToString:@"1"]) {
                            [inviteEventArray addObject:dict];
                        }
                    }
                }
            }
            filterArray = inviteEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];

        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"header.title.your_event.txt", @"")]) {
            //Your events
            NSMutableArray *myEventArray = [NSMutableArray array];

            for (NSDictionary *dict in sourceArr) {
                NSArray *eventMembers = [dict valueForKey:kKeyEvent_Attendees];
                if ([eventMembers isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *userDict in eventMembers) {
                        if ([[userDict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]] && [[userDict valueForKey:kKeyInvitationStatus] isEqualToString:@"1"] && [[dict valueForKey:kKeyIs_Public] isEqualToString:@"1"]) {
                            [myEventArray addObject:dict];
                        }
                    }
                }
            }
            filterArray = myEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"header.title.popular_event.txt", @"")]) {
            // Popular groups array
            NSMutableArray *popularEventArray = [NSMutableArray array];
            for (NSDictionary *dict in sourceArr) {
                if ([[dict valueForKey:kKeyEvent_Attendees] isKindOfClass:[NSArray class]] && [[dict valueForKey:kKeyIs_Public] isEqualToString:@"1"]) {
                    if (([dict[kKeyEvent_Attendees] count] >= 25 && [dict[kKeyIs_Public] intValue] == 1)) {
                        [popularEventArray addObject:dict];
                    }
                }
            }
            filterArray = popularEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@ "header.title.external_event.txt", @"")]) {
            // Popular groups array
            NSMutableArray *externalEventArray = [NSMutableArray array];
            for (NSDictionary *dict in sourceArr) {
                if (([dict[kKeyExternalEvent] isEqualToString:@"1"])) {
                    [externalEventArray addObject:dict];
                }
            }
            filterArray = externalEventArray;
            nearbyEvents = filterArray;
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.title.event_performer_filter.text", @"")]) {
            // Performers
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            CLLocation *loc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
            [geocoder reverseGeocodeLocation:loc
                           completionHandler:^(NSArray *placemarks, NSError *error) {
                               CLPlacemark *placemark = placemarks[0];

                self->locationName = [[placemark.locality stringByAppendingString:@","] stringByAppendingString:placemark.subAdministrativeArea];
                               [APP_DELEGATE showActivityIndicator];
                if (self->_txfFltredSearch.text.length > 0) {
                    self->locationName = self->placeDetail.name;
                    NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/events/search?app_key=R26GjGhPXtNsjCSz&keywords=%@&location=%@&date=Future", self->_txfFltredSearch.text, self->locationName];
                                   [self getFilteredPerformersEvents:externalEventUrl];
                               } else {
                                   NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/events/search?app_key=R26GjGhPXtNsjCSz&keywords=nil&location=%@&date=Future", self->locationName];
                                   [self getFilteredPerformersEvents:externalEventUrl];
                               }
                           }];
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.title.event_venue_filter.text", @"")] || [clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.title.event_category_filter.text", @"")]) {
            // Show category picker
            categoryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.tabBarController.tabBar.frame.origin.y - 160, SCREEN_WIDTH, 160)];
            categoryPicker.backgroundColor = [UIColor whiteColor];
            categoryPicker.dataSource = self;
            categoryPicker.delegate = self;

            categoryPicker.showsSelectionIndicator = YES;
            UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                    initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                           target:self action:@selector(pickCategory:)];
            UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
                    initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone
                           target:self action:@selector(cancelPickCategory:)];
            UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            toolBar = [[UIToolbar alloc] initWithFrame:
                    CGRectMake(0, categoryPicker.frame.origin.y - 40, SCREEN_WIDTH, 40)];
            toolBar.barTintColor = [UIColor orangeColor];
            toolBar.tintColor = [UIColor whiteColor];
            toolBar.translucent = NO;
            NSArray *toolbarItems = @[doneButton, flexibleItem, cancelButton];
            [toolBar setItems:toolbarItems];
            if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.title.event_venue_filter.text", @"")]) {
                isVenue = YES;
            } else
                isVenue = NO;
            [self.view addSubview:categoryPicker];
            [self.view addSubview:toolBar];
        } else if ([clickedFilter.title isEqualToString:NSLocalizedString(@"lbl.title.event_user_filter.text", @"")]) {
            // Users

        }
    }
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:filterArray];
    // Show text for no people
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) filterArray.count, latestDistance, distanceUnit];
}

- (void)cancelPickCategory:(id)sender {
    [categoryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
}

- (void)pickCategory:(id)sender {
    [categoryPicker removeFromSuperview];
    [toolBar removeFromSuperview];
    if (server.myLocation.coordinate.latitude != 0) {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        [geocoder reverseGeocodeLocation:loc
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           CLPlacemark *placemark = placemarks[0];
            if (self->_txfSearchField.text.length > 0) {
                self->locationName = self->placeDetail.name;
                           } else
                               self->locationName = [[placemark.locality stringByAppendingString:@","] stringByAppendingString:placemark.subAdministrativeArea];
                           [APP_DELEGATE showActivityIndicator];
            if (self->isVenue) {
                NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/venues/search?app_key=R26GjGhPXtNsjCSz&keywords=%@&location=%@", self->selectedCategory, self->locationName];
                               [self getFilteredVenueEvents:externalEventUrl];
                           } else {
                               NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/events/search?app_key=R26GjGhPXtNsjCSz&keywords=%@&location=%@&date=Future", self->selectedCategory, self->locationName];
                               [self getFilteredCategoryEvents:externalEventUrl];
                           }
                       }];
    }
}

// --------------------------------------------------------------------------------
// Action on Pin button click
- (IBAction)actionOnPingBtnClick:(id)sender {
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    // Get profile
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];


    if ([userDataDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDataDict[kKeyMediaImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            eventInfo[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    server.eventDetailInfo = [SRModalClass removeNullValuesFromDict:eventInfo];


    // Set chat view
    SRChatViewController *objSRPingsView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:(APP_DELEGATE).server.eventDetailInfo];
    [self.navigationController pushViewController:objSRPingsView animated:NO];
}

// --------------------------------------------------------------------------------
// compassButtonAction

- (void)compassButtonAction:(UIButton *)sender {

    // Get profile
    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender superview];
    NSDictionary *dataDict = [SRModalClass removeNullValuesFromDict:calloutView.userDict];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:dataDict];
    SRMapViewController *compassView = [[SRMapViewController alloc] initWithNibName:@"FromEventListView" bundle:nil inDict:dictionary server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:compassView animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}


// --------------------------------------------------------------------------------
// calculateAndGetNewUsers

- (void)calculateAndGetNewUsers {
    if (latestDistance > -1) {
        // Set value
        NSString *strDecimal = [NSString stringWithFormat:@"%.1f", latestDistance];
        NSArray *decimalNoArr = [strDecimal componentsSeparatedByString:@"."];

        NSString *str = [NSString stringWithFormat:@"%@.", decimalNoArr[0]];

        NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:str];
        NSAttributedString *atrStr1 = [[NSAttributedString alloc] initWithString:decimalNoArr[1] attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaBold size:10]}];
        NSAttributedString *atrStr2 = [[NSAttributedString alloc] initWithString:distanceUnit attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10]}];
        [muAtrStr appendAttributedString:atrStr1];
        [muAtrStr appendAttributedString:atrStr2];

        //[self.lblMiles setAttributedText:muAtrStr];
        [lblDistance setAttributedText:muAtrStr];

        // Get new array for the distance

        NSPredicate *predicate1 = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary<NSString *, id> *_Nullable bindings) {

            BOOL status = NO;

            if ([[evaluatedObject objectForKey:kKeyDistance] floatValue] <= self->latestDistance) {
                status = YES;
            }

            return status;
        }];

        if (!(APP_DELEGATE).isEventFilterApplied) {
            nearbyEvents = [sourceArr filteredArrayUsingPredicate:predicate1];
        }

        self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];
        lblNoPPl.text = [NSString stringWithFormat:@"%ld event in", (unsigned long) nearbyEvents.count];
        self.slider1.value = latestDistance;
        // Post notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];

        // Filter users
        for (id d in dotsArr) {
            SRRadarDotView *dot = (SRRadarDotView *) d;
            NSDictionary *userProfile = dot.userProfile;

            if ([nearbyEvents containsObject:userProfile]) {
                if (settingDistance != latestDistance) {
                    float maxDistance = settingDistance;
                    float dotDistance = [dot.initialDistance floatValue];

                    if (maxDistance != latestDistance) {
                        float modifiedUserDistance = (maxDistance * dotDistance) / latestDistance;
                        if (modifiedUserDistance > 130) {
                            modifiedUserDistance = 130;
                        } else if (modifiedUserDistance < 37) {
                            modifiedUserDistance = 37;
                        }
                        dot.distance = @(modifiedUserDistance);
                    }
                } else {
                    // The last user in the nearbyEvents list is the farthest
                    float maxDistance = [[[nearbyEvents lastObject] valueForKey:kKeyDistance] floatValue];

                    float d = [[userProfile valueForKey:kKeyDistance] floatValue];

                    float distance;

                    if (maxDistance != 0) {
                        if (37 + d >= 130) {
                            distance = 130;
                        } else {
                            distance = (37 + d);
                        }
                    } else {
                        distance = (double) 37;
                    }

                    // Distance
                    dot.distance = @(distance);
                }

                float degrees = [dot.bearing floatValue];
                float left = (float) (148 + [dot.distance floatValue] * sin(degreesToRadians(-degrees)));
                float top = (float) (148 - [dot.distance floatValue] * cos(degreesToRadians(-degrees)));

                dot.hidden = NO;
                [self translateDot:dot toBearing:[dot.bearing floatValue] atDistance:[dot.distance floatValue]];
                dot.frame = CGRectMake(left, top, 14, 14);
                dot.initialFrame = dot.frame;
                dot.imgView.frame = CGRectMake(0, 0, 14, 14);
            } else {
                dot.hidden = YES;
            }
        }
    }
}

// --------------------------------------------------------------------------------
// pinchDistance:

- (void)pinchDistance:(UIPinchGestureRecognizer *)gesture {
}

// --------------------------------------------------------------------------------
// handlePan:

// --------------------------------------------------------------------------------
// handlePan:
- (void)handlePan:(UIPanGestureRecognizer *)gesture {
    // Transform the view by the amount of the x translation
    // Calculate how far the user has dragged across the view
    CGFloat progress = [gesture locationInView:self.slider1].x / (self.slider1.bounds.size.width - 5 * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
   // NSLog(@"%f", [gesture locationInView:self.slider1].x);
    CGPoint translate = [gesture locationInView:gesture.view]; //translationInView:self.view];
    CGPoint translationInView = [gesture translationInView:self.slider1];
    swipeContainerView.hidden = NO;

    NSInteger translateX = [gesture locationInView:self.slider1].x;
    if (translateX < 0) {
        translateX = 0;
    }

    if (gesture.state == UIGestureRecognizerStateCancelled ||
            gesture.state == UIGestureRecognizerStateFailed ||
            gesture.state == UIGestureRecognizerStateEnded) {
        // When Pan ends
        swipeContainerView.hidden = YES;
        self.progressContainerView.hidden = NO;
        isSwipedOnce = NO;
        swipeView.frame = CGRectMake(0, 0, 0, self.view.frame.size.height);
        lblDistance.frame = CGRectMake(0, self.radarProfileImgView.frame.origin.y + self.radarProfileImgView.frame.size.height + 100, 100, 30);
        lblNoPPl.frame = CGRectMake(0, lblDistance.frame.origin.y + 18, 100, 30);

        if (gesture.state == UIGestureRecognizerStateEnded) {
            if (progress < 1) {
                latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;
                if (latestDistance <= kKeyMinimumRadarMile) {
                    latestDistance = kKeyMinimumRadarMile;
                } else {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                    }
                    // Show people
                }
                [self calculateAndGetNewUsers];
                //                self.slider1.value= (translate.x * self.slider1.maximumValue)/SCREEN_WIDTH;
            } else {
                latestDistance = settingDistance;
                // Show people
                [self calculateAndGetNewUsers];
            }


            sliderX = translate.x;
            if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
            } else
                swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);

            if (translate.x >= (SCREEN_WIDTH / 2)) {
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
            } else {
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
            }

        }
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        self.progressContainerView.hidden = YES;
        if (translationInView.x > 0) {
            // While changing state of slider to increasing value of slider
            [UIView animateWithDuration:3.0
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                             }
                             completion:^(BOOL finished) {
                                 if (translate.x <= self.slider1.frame.origin.x) {
                                     self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);

                                     self->lblDistance.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y, 120, 40);
                                     self->lblNoPPl.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y + 40, 140, 22);

                                 } else if (translate.x <= self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                                     if (progress < 1) {

                                         self->latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;

                                         if (self->latestDistance <= kKeyMinimumRadarMile) {
                                             self->latestDistance = kKeyMinimumRadarMile;
                                         } else {
                                             if (self->latestDistance >= kKeyMaximumRadarMile) {
                                                 self->latestDistance = kKeyMaximumRadarMile;
                                             }
                                         }
                                         // Show people
                                         [self calculateAndGetNewUsers];
                                     } else {
                                         self->latestDistance = self->settingDistance;
                                         // Show people
                                         [self calculateAndGetNewUsers];
                                     }

                                     self->sliderX = translate.x;
                                     if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                                         self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                                     } else
                                         self->swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);
                                     if (translate.x >= (SCREEN_WIDTH / 2)) {
                                         self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 140, self->lblDistance.frame.origin.y, 120, 40);
                                         self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 160, self->lblDistance.frame.origin.y + 40, 140, 22);
                                     } else {
                                         self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y, 140, 40);
                                         self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width, self->lblDistance.frame.origin.y + 40, 140, 22);
                                     }
                                 }
                             }];
        } else {
            // While changing state of slider to decreasing value of slider
            if (progress < 1) {
                latestDistance = (translateX * self.slider1.maximumValue) / self.slider1.frame.size.width;
                if (latestDistance <= kKeyMinimumRadarMile) {
                    latestDistance = kKeyMinimumRadarMile;
                } else {
                    if (latestDistance >= kKeyMaximumRadarMile) {
                        latestDistance = kKeyMaximumRadarMile;
                    }
                }
                // Show people
                [self calculateAndGetNewUsers];
            } else {
                latestDistance = settingDistance;
                // Show people
                [self calculateAndGetNewUsers];
            }
            // Set swiping view
            if (translate.x < self.slider1.frame.origin.x) {
                swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);
                lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
            } else {
                if (translate.x > self.slider1.frame.origin.x + self.slider1.frame.size.width) {
                    swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width, self.view.frame.size.height);
                } else
                    swipeView.frame = CGRectMake(0, 0, translate.x, self.view.frame.size.height);
                if (translate.x >= SCREEN_WIDTH / 2) {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 140, lblDistance.frame.origin.y, 120, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width - 160, lblDistance.frame.origin.y + 40, 140, 22);
                } else {
                    lblDistance.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y, 140, 40);
                    lblNoPPl.frame = CGRectMake(swipeView.frame.origin.x + swipeView.frame.size.width, lblDistance.frame.origin.y + 40, 140, 22);
                }
            }
        }
    }
}

#pragma mark
#pragma mark - tapGesture Method
#pragma mark

//
//----------------------------------------------------------------
//longPressTap:
- (void)longPressTap:(UILongPressGestureRecognizer *)recognizer {

    if (recognizer.state == UIGestureRecognizerStateEnded) {
        swipeContainerView.hidden = YES;
        return;
    }
    if (recognizer.view.tag == 1) {
        // Long press detected, start the timer
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
        CGFloat distancevalue = 0.0;
        if (strMilesData == nil) {
            distancevalue = kKeyMaximumRadarMile;
        } else {
            NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
            distancevalue = [strArray[0] floatValue];
        }
        self.slider1.maximumValue = distancevalue;
        latestDistance = distancevalue;
        [self.slider1 setValue:distancevalue animated:YES];

        [userdef setValue:[NSString stringWithFormat:@"%f", distancevalue] forKey:kKeyTempMiles_Data];
        [userdef synchronize];


        swipeContainerView.hidden = NO;
        [UIView animateWithDuration:0.0 delay:0.2 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.size.width + self.slider1.frame.origin.x, self.view.frame.size.height);
            self->lblDistance.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 150, self->lblDistance.frame.origin.y, 120, 40);
            self->lblNoPPl.frame = CGRectMake(self->swipeView.frame.origin.x + self->swipeView.frame.size.width - 150, self->lblDistance.frame.origin.y + 40, 140, 22);
                    [self.view bringSubviewToFront:self.progressContainerView];
                }
                         completion:^(BOOL finished) {
                         }];
    } else if (recognizer.view.tag == 2) {
        // Long press detected, start the timer
        [self.slider1 setValue:0.0 animated:YES];
        latestDistance = kKeyMinimumRadarMile;
        NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
        [userdef setValue:[NSString stringWithFormat:@"%f", latestDistance] forKey:kKeyTempMiles_Data];
        [userdef synchronize];

        swipeContainerView.hidden = NO;
        [UIView animateWithDuration:0.0
                              delay:0.2
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
            self->swipeView.frame = CGRectMake(0, 0, self.slider1.frame.origin.x, self.view.frame.size.height);

            self->lblDistance.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y, 120, 40);
            self->lblNoPPl.frame = CGRectMake(self.slider1.frame.origin.x + 20, self->lblDistance.frame.origin.y + 40, 140, 22);
                             [self.view bringSubviewToFront:self.progressContainerView];
                         }
                         completion:^(BOOL finished) {
                         }];
    }
    [self calculateAndGetNewUsers];
}

//
//----------------------------------------------------------------
//tapOnRefreshOverlay:
- (void)tapOnRefreshOverlay:(UITapGestureRecognizer *)recognizer {
    //Hide radar refresh overlay
    [_radarRefreshOverlay setHidden:YES];
}

//
//---------------------------------------------------------------
//longPressRadar:
- (void)longPressRadar:(UILongPressGestureRecognizer *)recognizer {

    [UIView animateWithDuration:0.0 delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{

                     } completion:^(BOOL finished) {

                         [self->_radarRefreshOverlay setHidden:NO];
                         if (!self->_radarRefreshOverlay) {
                    // Add view to refresh radar
                             self->_radarRefreshOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
                             self->_radarRefreshOverlay.backgroundColor = [UIColor blackColor];
                             self->_radarRefreshOverlay.alpha = 0.7;
                             self->_radarRefreshOverlay.userInteractionEnabled = TRUE;
                    self->_radarRefreshOverlay.tag = 100000;

                    // Add update lbl
                    UILabel *updateLbl = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 70, SCREEN_HEIGHT - 235, 140, 20)];
                    [updateLbl setText:@"Update radar now"];
                    [updateLbl setTextColor:[UIColor whiteColor]];
                    [updateLbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:14]];
                    [updateLbl setTextAlignment:NSTextAlignmentCenter];
                             [self->_radarRefreshOverlay addSubview:updateLbl];

                    // add refresh btn
                    UIButton *refreshBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, SCREEN_HEIGHT - 195, 60, 60)];
                    refreshBtn.layer.cornerRadius = refreshBtn.frame.size.width / 2;
                    [refreshBtn setImage:[UIImage imageNamed:@"update-radar-white.png"] forState:UIControlStateNormal];
                    [refreshBtn setBackgroundColor:[UIColor whiteColor]];
                    [refreshBtn addTarget:self action:@selector(refreshRadarBtnClick) forControlEvents:UIControlEventTouchUpInside];
                    [refreshBtn setUserInteractionEnabled:TRUE];

                             [self->_radarRefreshOverlay addSubview:refreshBtn];

                    UITapGestureRecognizer *tapGestOnRefreshOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRefreshOverlay:)];
                             [self->_radarRefreshOverlay addGestureRecognizer:tapGestOnRefreshOverlay];

                             [self.tabBarController.view addSubview:self->_radarRefreshOverlay];
                    [self.view bringSubviewToFront:refreshBtn];
                }

                //[self.view bringSubviewToFront:_radarRefreshOverlay];
            }];
}

- (void)onDotTapped:(UITapGestureRecognizer *)recognizer {
    UIView *circleView = recognizer.view;
    CGPoint point = [recognizer locationInView:circleView];
    // The for loop is to find out multiple dots in vicinity
    // you may define a NSMutableArray before the for loop and
    // get the group of dots together
    NSMutableArray *tappedUsers = [NSMutableArray array];
    for (SRRadarDotView *d in dotsArr) {

        if ([d.layer.presentationLayer hitTest:point] != nil) {
            // you can get the list of tapped user(s if more than one users are close enough)
            // use this variable outside of for loop to get list of users
            [tappedUsers addObject:d.userProfile];

        }
    }
    // use tappedUsers variable according to your app logic
    if (tappedUsers.count > 0) {
        [self addViewsOnScrollView:tappedUsers];
    } else {
        [self.scrollView setHidden:YES];
        [self.toolTipScrollViewImg setHidden:YES];
    }
}

// --------------------------------------------------------------------------------
// windowTapGesture:

- (void)windowTapAction:(UITapGestureRecognizer *)sender {

    CalloutAnnotationView *calloutView = (CalloutAnnotationView *) [sender.view superview];
    // Get profile
    NSMutableDictionary *eventInfo = [NSMutableDictionary dictionaryWithDictionary:calloutView.userDict];

    if ([userDataDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [userDataDict[kKeyMediaImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsUserPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            eventInfo[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }
    server.eventDetailInfo = [SRModalClass removeNullValuesFromDict:eventInfo];

    SREventDetailTabViewController *objGroupTab = [[SREventDetailTabViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:objGroupTab animated:YES];
}

// --------------------------------------------------------------------------------
// viewTapAction:

- (void)viewTapAction:(UITapGestureRecognizer *)sender {
    [self.toolTipScrollViewImg setHidden:YES];
    [self.scrollView setHidden:YES];
}

#pragma mark
#pragma mark External events API Method
#pragma mark

// --------------------------------------------------------------------------------
//  Method parses the JSON Data Received
- (id)parseJSON:(NSData *)data {
    id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    return jsonData;
}

// --------------------------------------------------------------------------------
// getFilteredVenueEvents
- (void)getFilteredVenueEvents:(NSString *)eventUrl {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:eventUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                id jsonObj = [self parseJSON:data];
                NSDictionary *dictionary = [[NSDictionary alloc] initWithDictionary:jsonObj];
                if ([dictionary valueForKey:kKeyExtEventvenuesKey]) {
                    // Venues external events

                    if ([dictionary valueForKey:kKeyExtEventvenuesKey] == [NSNull null]) {
                        [SRModalClass showAlert:@"No Data"];
                        [APP_DELEGATE hideActivityIndicator];
                    } else {
                        NSDictionary *venuesDict = [[NSDictionary alloc] initWithDictionary:[dictionary valueForKey:kKeyExtEventvenuesKey]];
                        NSMutableArray *externalVenueArray;
                        if ([[venuesDict valueForKey:kKeyExtEventvenue] isKindOfClass:[NSArray class]] && [venuesDict valueForKey:kKeyExtEventvenue] != nil) {
                            externalVenueArray = [[NSMutableArray alloc] initWithArray:[venuesDict valueForKey:kKeyExtEventvenue]];
                        } else if ([[venuesDict valueForKey:kKeyExtEventvenue] isKindOfClass:[NSDictionary class]] && [venuesDict valueForKey:kKeyExtEventvenue] != nil) {
                            NSDictionary *eventD = [[NSDictionary alloc] initWithDictionary:[venuesDict valueForKey:kKeyExtEventvenue]];
                            externalVenueArray = [@[eventD] mutableCopy];
                        }

                        for (int i = 0; i < externalVenueArray.count; i++) {
                            NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventID] forKey:kKeyId];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyName] forKey:kKeyName];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventVname] forKey:kKeyEvent_Type];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyLattitude];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyRadarLong];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventLongitude] forKey:kKeyLongitude];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventAddress] forKey:kKeyAddress];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventDescription] forKey:kKeyDescription];
                            [eventDict setValue:[SRModalClass returnDateFromDate:[externalVenueArray[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Date];
                            [eventDict setValue:[SRModalClass returnTimeFromDate:[externalVenueArray[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Time];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventCreated] forKey:kKeyEventCreated];
                            [eventDict setValue:[externalVenueArray[i] valueForKey:kKeyExtEventModified] forKey:kKeyEventUpdated];
                            [eventDict setValue:@"1" forKey:kKeyExternalEvent];
                            [eventDict setValue:kKeyExtEventvenue forKey:kKeyExternalEventType];


                            CLLocation *eventLocation = [[CLLocation alloc] initWithLatitude:[[externalVenueArray[i] valueForKey:kKeyExtEventLatitude] floatValue] longitude:[[externalVenueArray[i] valueForKey:kKeyExtEventLongitude] floatValue]];

                            CLLocationDistance distanceInMeters = [self->server.myLocation distanceFromLocation:eventLocation];
                            CGFloat distanceInMiles = distanceInMeters / 1609;
                            [eventDict setValue:[NSString stringWithFormat:@"%f", distanceInMiles] forKey:kKeyDistance];

                            if ([[externalVenueArray[i] valueForKey:kKeyExtEventImage] isKindOfClass:[NSDictionary class]] && [[externalVenueArray[i] valueForKey:kKeyExtEventImage] objectForKey:kKeyExtEventImageMedium] != nil) {
                                //Get image dictionary
                                NSMutableDictionary *imageDict = [[NSMutableDictionary alloc] initWithDictionary:[externalVenueArray[i] valueForKey:kKeyExtEventImage]];

                                //Get medium size image dictionary
                                NSMutableDictionary *mediumImageDict = [[NSMutableDictionary alloc] initWithDictionary:[imageDict valueForKey:kKeyExtEventImageMedium]];

                                // Get medium image url
                                NSString *imgUrl = [mediumImageDict valueForKey:kKeyExtEventImageUrl];

                                //Set url to imgDict
                                NSMutableDictionary *imgDict = [[NSMutableDictionary alloc] init];
                                [imgDict setValue:imgUrl forKey:kKeyImageName];

                                // Set image dict to event dictionary for  image
                                [eventDict setValue:imgDict forKey:kKeyMediaImage];
                            }

                            BOOL flag = NO;
                            for (NSDictionary *dict in self->extEventVenueArray) {
                                if ([[eventDict valueForKey:kKeyExtEventID] isEqualToString:[dict valueForKey:kKeyExtEventID]]) {
                                    flag = YES;
                                    break;
                                }
                            }
                            if (!flag) {
                                [self->extEventVenueArray addObject:eventDict];
                            }
                        }

                        self->nearbyEvents = self->extEventVenueArray;
                        if (self->settingDistance == self->latestDistance) {
                            [self displayUsersDotsOnRadar];
                        } else {
                            [self displayUsersDotsOnRadar];
                            [self calculateAndGetNewUsers];
                        }

                        // Show text for no people
                        int noCounter = 1;
                        for (NSDictionary *dict in self->nearbyEvents) {
                            [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
                            noCounter++;
                        }
                        self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) self->nearbyEvents.count, self->latestDistance, self->distanceUnit];
                        // Post notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:self->nearbyEvents];
                    }
                }
            }
        });

    }];
}

// --------------------------------------------------------------------------------
// getFilteredPerformersEvents
- (void)getFilteredPerformersEvents:(NSString *)eventUrl {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:eventUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                id jsonObj = [self parseJSON:data];
                NSDictionary *dictionary = [[NSDictionary alloc] initWithDictionary:jsonObj];
                if ([dictionary valueForKey:kKeyEvents]) {
                    // Venues external events

                    if ([dictionary valueForKey:kKeyEvents] == [NSNull null]) {
                        [SRModalClass showAlert:@"No Data"];
                        [APP_DELEGATE hideActivityIndicator];
                    } else {
                        NSDictionary *venuesDict = [[NSDictionary alloc] initWithDictionary:[dictionary valueForKey:kKeyEvents]];
                        NSMutableArray *externalPerformerArray;
                        if ([[venuesDict valueForKey:kKeyExtEvent] isKindOfClass:[NSArray class]] && [venuesDict valueForKey:kKeyExtEvent] != nil) {
                            externalPerformerArray = [[NSMutableArray alloc] initWithArray:[venuesDict valueForKey:kKeyExtEvent]];
                        } else if ([[venuesDict valueForKey:kKeyExtEvent] isKindOfClass:[NSDictionary class]] && [venuesDict valueForKey:kKeyExtEvent] != nil) {
                            NSDictionary *eventD = [[NSDictionary alloc] initWithDictionary:[venuesDict valueForKey:kKeyExtEvent]];
                            externalPerformerArray = [@[eventD] mutableCopy];
                        }

                        for (int i = 0; i < externalPerformerArray.count; i++) {
                            NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventID] forKey:kKeyId];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventTitle] forKey:kKeyName];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventVname] forKey:kKeyEvent_Type];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyLattitude];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyRadarLong];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventLongitude] forKey:kKeyLongitude];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventAddress] forKey:kKeyAddress];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventDescription] forKey:kKeyDescription];
                            [eventDict setValue:[SRModalClass returnDateFromDate:[externalPerformerArray[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Date];
                            [eventDict setValue:[SRModalClass returnTimeFromDate:[externalPerformerArray[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Time];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventCreated] forKey:kKeyEventCreated];
                            [eventDict setValue:[externalPerformerArray[i] valueForKey:kKeyExtEventModified] forKey:kKeyEventUpdated];
                            [eventDict setValue:@"1" forKey:kKeyExternalEvent];


                            CLLocation *eventLocation = [[CLLocation alloc] initWithLatitude:[[externalPerformerArray[i] valueForKey:kKeyExtEventLatitude] floatValue] longitude:[[externalPerformerArray[i] valueForKey:kKeyExtEventLongitude] floatValue]];

                            CLLocationDistance distanceInMeters = [self->server.myLocation distanceFromLocation:eventLocation];
                            CGFloat distanceInMiles = distanceInMeters / 1609;
                            [eventDict setValue:[NSString stringWithFormat:@"%f", distanceInMiles] forKey:kKeyDistance];

                            if ([[externalPerformerArray[i] valueForKey:kKeyExtEventImage] isKindOfClass:[NSDictionary class]] && [[externalPerformerArray[i] valueForKey:kKeyExtEventImage] objectForKey:kKeyExtEventImageMedium] != nil) {
                                //Get image dictionary
                                NSMutableDictionary *imageDict = [[NSMutableDictionary alloc] initWithDictionary:[externalPerformerArray[i] valueForKey:kKeyExtEventImage]];

                                //Get medium size image dictionary
                                NSMutableDictionary *mediumImageDict = [[NSMutableDictionary alloc] initWithDictionary:[imageDict valueForKey:kKeyExtEventImageMedium]];

                                // Get medium image url
                                NSString *imgUrl = [mediumImageDict valueForKey:kKeyExtEventImageUrl];

                                //Set url to imgDict
                                NSMutableDictionary *imgDict = [[NSMutableDictionary alloc] init];
                                [imgDict setValue:imgUrl forKey:kKeyImageName];

                                // Set image dict to event dictionary for  image
                                [eventDict setValue:imgDict forKey:kKeyMediaImage];
                            }

                            BOOL flag = NO;
                            for (NSDictionary *dict in self->extEventPerformerArray) {
                                if ([[eventDict valueForKey:kKeyExtEventID] isEqualToString:[dict valueForKey:kKeyExtEventID]]) {
                                    flag = YES;
                                    break;
                                }
                            }
                            if (!flag) {
                                [self->extEventPerformerArray addObject:eventDict];
                            }
                        }

                        self->nearbyEvents = self->extEventPerformerArray;
                        if (self->settingDistance == self->latestDistance) {
                            [self displayUsersDotsOnRadar];
                        } else {
                            [self displayUsersDotsOnRadar];
                            [self calculateAndGetNewUsers];
                        }

                        // Show text for no people
                        int noCounter = 1;
                        for (NSDictionary *dict in self->nearbyEvents) {
                            [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
                            noCounter++;
                        }
                        self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) self->nearbyEvents.count, self->latestDistance, self->distanceUnit];
                        // Post notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:self->nearbyEvents];
                    }
                }
            }
        });

    }];
}

// --------------------------------------------------------------------------------
// getFilteredCategoryEvents
- (void)getFilteredCategoryEvents:(NSString *)eventUrl {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:eventUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                id jsonObj = [self parseJSON:data];
                NSDictionary *dictionary = [[NSDictionary alloc] initWithDictionary:jsonObj];
                if ([dictionary valueForKey:kKeyEvents]) {
                    // Venues external events

                    if ([dictionary valueForKey:kKeyEvents] == [NSNull null]) {
                        [SRModalClass showAlert:@"No Data"];
                        [APP_DELEGATE hideActivityIndicator];
                    } else {
                        NSDictionary *venuesDict = [[NSDictionary alloc] initWithDictionary:[dictionary valueForKey:kKeyEvents]];
                        NSMutableArray *externalCatEventArray;
                        if ([[venuesDict valueForKey:kKeyExtEvent] isKindOfClass:[NSArray class]] && [venuesDict valueForKey:kKeyExtEvent] != nil) {
                            externalCatEventArray = [[NSMutableArray alloc] initWithArray:[venuesDict valueForKey:kKeyExtEvent]];
                        } else if ([[venuesDict valueForKey:kKeyExtEvent] isKindOfClass:[NSDictionary class]] && [venuesDict valueForKey:kKeyExtEvent] != nil) {
                            NSDictionary *eventD = [[NSDictionary alloc] initWithDictionary:[venuesDict valueForKey:kKeyExtEvent]];
                            externalCatEventArray = [@[eventD] mutableCopy];
                        }

                        for (int i = 0; i < externalCatEventArray.count; i++) {
                            NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventID] forKey:kKeyId];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventTitle] forKey:kKeyName];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventVname] forKey:kKeyEvent_Type];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyLattitude];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyRadarLong];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventLongitude] forKey:kKeyLongitude];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventAddress] forKey:kKeyAddress];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventDescription] forKey:kKeyDescription];
                            [eventDict setValue:[SRModalClass returnDateFromDate:[externalCatEventArray[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Date];
                            [eventDict setValue:[SRModalClass returnTimeFromDate:[externalCatEventArray[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Time];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventCreated] forKey:kKeyEventCreated];
                            [eventDict setValue:[externalCatEventArray[i] valueForKey:kKeyExtEventModified] forKey:kKeyEventUpdated];
                            [eventDict setValue:@"1" forKey:kKeyExternalEvent];
                            [eventDict setValue:kKeyExtEvent forKey:kKeyExternalEventType];


                            CLLocation *eventLocation = [[CLLocation alloc] initWithLatitude:[[externalCatEventArray[i] valueForKey:kKeyExtEventLatitude] floatValue] longitude:[[externalCatEventArray[i] valueForKey:kKeyExtEventLongitude] floatValue]];

                            CLLocationDistance distanceInMeters = [self->server.myLocation distanceFromLocation:eventLocation];
                            CGFloat distanceInMiles = distanceInMeters / 1609;
                            [eventDict setValue:[NSString stringWithFormat:@"%f", distanceInMiles] forKey:kKeyDistance];

                            if ([[externalCatEventArray[i] valueForKey:kKeyExtEventImage] isKindOfClass:[NSDictionary class]] && [[externalCatEventArray[i] valueForKey:kKeyExtEventImage] objectForKey:kKeyExtEventImageMedium] != nil) {
                                //Get image dictionary
                                NSMutableDictionary *imageDict = [[NSMutableDictionary alloc] initWithDictionary:[externalCatEventArray[i] valueForKey:kKeyExtEventImage]];

                                //Get medium size image dictionary
                                NSMutableDictionary *mediumImageDict = [[NSMutableDictionary alloc] initWithDictionary:[imageDict valueForKey:kKeyExtEventImageMedium]];

                                // Get medium image url
                                NSString *imgUrl = [mediumImageDict valueForKey:kKeyExtEventImageUrl];

                                //Set url to imgDict
                                NSMutableDictionary *imgDict = [[NSMutableDictionary alloc] init];
                                [imgDict setValue:imgUrl forKey:kKeyImageName];

                                // Set image dict to event dictionary for  image
                                [eventDict setValue:imgDict forKey:kKeyMediaImage];
                            }

                            BOOL flag = NO;
                            for (NSDictionary *dict in self->extEventCategoryArray) {
                                if ([[eventDict valueForKey:kKeyExtEventID] isEqualToString:[dict valueForKey:kKeyExtEventID]]) {
                                    flag = YES;
                                    break;
                                }
                            }
                            if (!flag) {
                                [self->extEventCategoryArray addObject:eventDict];
                            }
                        }

                        self->nearbyEvents = self->extEventCategoryArray;
                        if (self->settingDistance == self->latestDistance) {
                            [self displayUsersDotsOnRadar];
                        } else {
                            [self displayUsersDotsOnRadar];
                            [self calculateAndGetNewUsers];
                        }

                        // Show text for no people
                        int noCounter = 1;
                        for (NSDictionary *dict in self->nearbyEvents) {
                            [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
                            noCounter++;
                        }
                        self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) self->nearbyEvents.count, self->latestDistance, self->distanceUnit];
                        // Post notification
                        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:self->nearbyEvents];
                    }
                }
            }
        });

    }];
}

// --------------------------------------------------------------------------------
// getFilteredExternalEvents
- (void)getCategoryForExternalEvents:(NSString *)eventUrl {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:eventUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                id jsonObj = [self parseJSON:data];
                NSDictionary *dictionary = [[NSDictionary alloc] initWithDictionary:jsonObj];
                if ([dictionary valueForKey:kKeyExtEventCategory]) {
                    self->extEventCatListArray = [[NSArray alloc] initWithArray:[dictionary valueForKey:kKeyExtEventCategory]];
                }
            }
        });

    }];
}

//
// --------------------------------------------------------------------------------
// Get external events API call:
- (void)getExternalEventsList {
    extEventArray = [[NSMutableArray alloc] initWithArray:sourceArr];
    if (server.myLocation.coordinate.latitude != 0) {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        [geocoder reverseGeocodeLocation:loc
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           CLPlacemark *placemark = placemarks[0];
                           //            NSString *locationName = placemark.locality;
            if (self->_txfSearchField.text.length > 0) {
                if (self->placeDetail.name.length > 0) {
                    self->locationName = self->placeDetail.name;
                               } else {
                                   self->locationName = self->_txfSearchField.text;
                               }

                           } else
                               //TODO: - crash when locality nil(Change location apple and vn)
                               self->locationName = [[placemark.locality stringByAppendingString:@","] stringByAppendingString:placemark.subAdministrativeArea];

            NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/events/search?app_key=R26GjGhPXtNsjCSz&location=%@&date=Future&sort_order=date&page_number=%d", self->locationName, self->pageNo];

                           NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:externalEventUrl] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
                           [request setHTTPMethod:@"GET"];

                           [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {

                               dispatch_async(dispatch_get_main_queue(), ^{
                                   if (data) {

                                       id jsonObj = [self parseJSON:data];


                                       //Parse result
                                       NSDictionary *dictionary = [[NSDictionary alloc] initWithDictionary:jsonObj];
                                       if ([dictionary isKindOfClass:[NSDictionary class]] && [dictionary[kKeyEvents] isKindOfClass:[NSDictionary class]]) {

                                           NSDictionary *eventDictionary = [[NSDictionary alloc] initWithDictionary:dictionary[kKeyEvents]];

                                           NSMutableArray *eventListArr;
                                           if ([[eventDictionary valueForKey:kKeyExtEvent] isKindOfClass:[NSArray class]] && [eventDictionary valueForKey:kKeyExtEvent] != nil) {
                                               eventListArr = [[NSMutableArray alloc] initWithArray:[eventDictionary valueForKey:kKeyExtEvent]];
                                           } else if ([[eventDictionary valueForKey:kKeyExtEvent] isKindOfClass:[NSDictionary class]] && [eventDictionary valueForKey:kKeyExtEvent] != nil) {

                                               NSDictionary *eventD = [[NSDictionary alloc] initWithDictionary:[eventDictionary valueForKey:kKeyExtEvent]];
                                               eventListArr = [@[eventD] mutableCopy];
                                           }


                                           for (int i = 0; i < eventListArr.count; i++) {
                                               NSMutableDictionary *eventDict = [[NSMutableDictionary alloc] init];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventID] forKey:kKeyId];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventTitle] forKey:kKeyName];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventVname] forKey:kKeyEvent_Type];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventLatitude] forKey:kKeyLattitude];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventLongitude] forKey:kKeyLongitude];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventLongitude] forKey:kKeyRadarLong];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventAddress] forKey:kKeyAddress];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventDescription] forKey:kKeyDescription];
                                               [eventDict setValue:[SRModalClass returnDateFromDate:[eventListArr[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Date];
                                               [eventDict setValue:[SRModalClass returnTimeFromDate:[eventListArr[i] valueForKey:kKeyExtEventTime]] forKey:kKeyEvent_Time];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventCreated] forKey:kKeyEventCreated];
                                               [eventDict setValue:[eventListArr[i] valueForKey:kKeyExtEventModified] forKey:kKeyEventUpdated];
                                               [eventDict setValue:@"1" forKey:kKeyExternalEvent];


                                               CLLocation *eventLocation = [[CLLocation alloc] initWithLatitude:[[eventListArr[i] valueForKey:kKeyExtEventLatitude] floatValue] longitude:[[eventListArr[i] valueForKey:kKeyExtEventLongitude] floatValue]];

                                               CLLocationDistance distanceInMeters = [self->server.myLocation distanceFromLocation:eventLocation];
                                               CGFloat distanceInMiles = distanceInMeters / 1609;
                                               [eventDict setValue:[NSString stringWithFormat:@"%f", distanceInMiles] forKey:kKeyDistance];


                                               if ([[eventListArr[i] valueForKey:kKeyExtEventImage] isKindOfClass:[NSDictionary class]] && [[eventListArr[i] valueForKey:kKeyExtEventImage] objectForKey:kKeyExtEventImageMedium] != nil) {
                                                   //Get image dictionary
                                                   NSMutableDictionary *imageDict = [[NSMutableDictionary alloc] initWithDictionary:[eventListArr[i] valueForKey:kKeyExtEventImage]];

                                                   //Get medium size image dictionary
                                                   NSMutableDictionary *mediumImageDict = [[NSMutableDictionary alloc] initWithDictionary:[imageDict valueForKey:kKeyExtEventImageMedium]];

                                                   // Get medium image url
                                                   NSString *imgUrl = [mediumImageDict valueForKey:kKeyExtEventImageUrl];

                                                   //Set url to imgDict
                                                   NSMutableDictionary *imgDict = [[NSMutableDictionary alloc] init];
                                                   [imgDict setValue:imgUrl forKey:kKeyImageName];

                                                   // Set image dict to event dictionary for  image
                                                   [eventDict setValue:imgDict forKey:kKeyMediaImage];
                                               }

                                               BOOL flag = NO;
                                               for (NSDictionary *dict in self->extEventArray) {
                                                   if ([[eventDict valueForKey:kKeyExtEventID] isEqualToString:[dict valueForKey:kKeyExtEventID]]) {
                                                       flag = YES;
                                                       break;
                                                   }
                                               }
                                               if (!flag) {
                                                   [self->extEventArray addObject:eventDict];
                                               }
                                           }
                                       }
                                   } else
                                       self->pageNo = 1;

                                   self->sourceArr = [NSArray arrayWithArray:self->extEventArray];


                                   if (self->settingDistance == self->latestDistance) {
                                       self->nearbyEvents = self->sourceArr;
                                       [self displayUsersDotsOnRadar];
                                   } else {
                                       [self displayUsersDotsOnRadar];
                                       [self calculateAndGetNewUsers];
                                   }
                                   // Show text for no people
                                   int noCounter = 1;
                                   for (NSDictionary *dict in self->nearbyEvents) {
                                       [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
                                       noCounter++;
                                   }
                                   self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) self->nearbyEvents.count, self->latestDistance, self->distanceUnit];
                                   // Post notification
                                   [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:self->nearbyEvents];
                               });

                           }];

                       }];
    }
    [APP_DELEGATE hideActivityIndicator];
}
// --------------------------------------------------------------------------------
// refreshExternalList:

- (void)refreshExternalList:(NSNotification *)inNotify {
    pageNo++;
    [self getExternalEventsList];
}

// --------------------------------------------------------------------------------
// refreshExternalCatEventList:

- (void)refreshExternalCatEventList:(NSNotification *)inNotify {
    pageNo++;
    NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/events/search?app_key=R26GjGhPXtNsjCSz&keywords=%@&location=%@&date=Future", selectedCategory, locationName];
    [self getFilteredCategoryEvents:externalEventUrl];
}
// --------------------------------------------------------------------------------
// refreshExternalVenueEventList:

- (void)refreshExternalVenueEventList:(NSNotification *)inNotify {
    pageNo++;
    NSString *externalEventUrl = [NSString stringWithFormat:@"http://api.eventful.com/json/venues/search?app_key=R26GjGhPXtNsjCSz&keywords=%@&location=%@", selectedCategory, locationName];
    [self getFilteredVenueEvents:externalEventUrl];
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// filterUserListOnMap:
- (void)filterEventUserListOnMap:(NSNotification *)inNotify {
    latestDistance = [[inNotify object] floatValue];
    self.slider1.value = latestDistance;
    [self calculateAndGetNewUsers];
}
// --------------------------------------------------------------------------------
// getUpdateLocation:

- (void)getUpdateLocation:(NSNotification *)inNotify {
    // Update list
    [self makeServerCallToGetEventList];

    //display current city in location textfield
    [self displayCurrentCity];
}

// --------------------------------------------------------------------------------
// updateHeadingAngle:

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    currentDeviceBearing = heading;
    [self rotateArcsToHeading:headingAngle];
}

// --------------------------------------------------------------------------------
// ShowEventSucceed:

- (void)getEventSucceed:(NSNotification *)inNotify {
    //[_radarRefreshOverlay setHidden:YES];
    [APP_DELEGATE hideActivityIndicator];
    NSDictionary *result = [inNotify object];
    if (result) {
        // Display the dots
        NSDictionary *dictionary = [inNotify object];
        if ([dictionary isKindOfClass:[NSDictionary class]] && dictionary[kKeyEvents] != nil) {
            NSArray *eventListArr = dictionary[kKeyEvents];

            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
            NSArray *sortDescriptors = @[sortDescriptor];
            eventListArr = [eventListArr sortedArrayUsingDescriptors:sortDescriptors];

            eventUsers = dictionary[kKeyGroupUsers];
            server.eventUsersArr = eventUsers;

            if (![sourceArr isEqual:eventListArr]) {
                sourceArr = eventListArr;
                if (extEventArray.count > 0) {
                    [sourceArr arrayByAddingObjectsFromArray:extEventArray];
                    // sourceArr=[NSArray arrayWithArray:extEventArray];
                }
                //NSArray *joeAndJenny = [sourceArr filteredArrayUsingPredicate:[NSPredicate        nearbyEvents = filteredArr;
                if (settingDistance == latestDistance) {
                    nearbyEvents = sourceArr;
                    [self displayUsersDotsOnRadar];
                } else {
                    [self displayUsersDotsOnRadar];
                    [self calculateAndGetNewUsers];
                }
                // Show text for no people
                self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];

                // Post notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];
            }
        } else if ([dictionary isKindOfClass:[NSDictionary class]]) {
            // Check whether edit or create
            NSDictionary *postInfo = [inNotify userInfo];
            NSDictionary *requestedParam = postInfo[kKeyRequestedParams];

            NSMutableArray *mutatedArr = [NSMutableArray array];
            [mutatedArr addObjectsFromArray:sourceArr];

            if (requestedParam[@"_method"] != nil) {
                // Find the dict and replace it
                BOOL found = NO;
                for (NSUInteger i = 0; i < [mutatedArr count] && !found; i++) {
                    NSDictionary *dict = mutatedArr[i];
                    if ([dictionary[kKeyId] isEqualToString:dict[kKeyId]]) {
                        found = YES;
                        mutatedArr[i] = dictionary;
                    }
                }
            } else {
                [mutatedArr addObject:dictionary];
            }

            // Sorting
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
            NSArray *sortDescriptors = @[sortDescriptor];
            sourceArr = [mutatedArr sortedArrayUsingDescriptors:sortDescriptors];
            //If external events are here
            if (extEventArray.count > 0) {
                sourceArr = [NSArray arrayWithArray:extEventArray];
            }
            if (settingDistance == latestDistance) {
                nearbyEvents = sourceArr;
                [self displayUsersDotsOnRadar];
            } else {
                [self displayUsersDotsOnRadar];
                [self calculateAndGetNewUsers];
            }
            // Show text for no people
            self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];
            // Post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];
        } else {
            // Else deleted
            NSDictionary *postInfo = [inNotify userInfo];
            NSString *objectUrl = postInfo[kKeyObjectUrl];
            NSArray *componentsArr = [objectUrl componentsSeparatedByString:@"/"];
            if ([componentsArr count] > 1) {
                NSString *groupId = componentsArr[1];

                NSMutableArray *mutatedArr = [NSMutableArray array];
                [mutatedArr addObjectsFromArray:sourceArr];

                BOOL found = NO;
                for (NSUInteger i = 0; i < [mutatedArr count] && !found; i++) {
                    NSDictionary *dict = mutatedArr[i];
                    if ([groupId isEqualToString:dict[kKeyId]]) {
                        found = YES;
                        [mutatedArr removeObject:dict];
                    }
                }

                // Sorting
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
                NSArray *sortDescriptors = @[sortDescriptor];
                sourceArr = [mutatedArr sortedArrayUsingDescriptors:sortDescriptors];

                //If external events are here
                if (extEventArray.count > 0) {
                    sourceArr = [NSArray arrayWithArray:extEventArray];
                }
                if (settingDistance == latestDistance) {
                    nearbyEvents = sourceArr;
                    [self displayUsersDotsOnRadar];
                } else {
                    [self displayUsersDotsOnRadar];
                    [self calculateAndGetNewUsers];
                }
                // Show text for no people
                self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];
                // Post notification
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];
            }
        }
    }


    // Get external events
    [self getExternalEventsList];


    //Filter if user current address get
    if (_txfSearchField.text.length > 0) {
        [self searchEventForDateOrLoc:_txfSearchField.text];
    }
}

// --------------------------------------------------------------------------------
// ShowEventSucceed:

- (void)getEventFailed:(NSNotification *)inNotify {
    // Get external events
    [APP_DELEGATE hideActivityIndicator];
    [self getExternalEventsList];
}

// --------------------------------------------------------------------------------
// ShowGroupFailed:

- (void)ShowGroupFailed:(NSNotification *)inNotify {
}

#pragma mark
#pragma mark GMSAutocompleteTableDataSource Delegate method
#pragma mark
// --------------------------------------------------------------------------------
// didAutocompleteWithPlace:

- (void) tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    placeDetail = place;
    _txfSearchField.text = place.name;
    [_txfSearchField resignFirstResponder];
    [self searchEventForDateOrLoc:place.name];
}

// --------------------------------------------------------------------------------
// didFailAutocompleteWithError:

- (void)     tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [_txfSearchField resignFirstResponder];
    _txfSearchField.text = @"";
    [SRModalClass showAlert:@"Sorry, we are not able to properly map user locations at the moment. Please close and reopen the app."];
}

// --------------------------------------------------------------------------------
// didUpdateAutocompletePredictionsForTableDataSource:

- (void)didUpdateAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:_resultsController.tableView]) {

        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    return YES;
}

#pragma mark
#pragma mark - Picker View Data source
#pragma mark

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [extEventCatListArray count];
}


#pragma mark
#pragma mark- Picker View Delegate
#pragma mark

- (void)                pickerView:(UIPickerView *)pickerView didSelectRow:
        (NSInteger)row inComponent:(NSInteger)component {
    selectedCategory = [extEventCatListArray[row] valueForKey:kKeyId];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *lbl = (UILabel *) view;
    // Customise Font
    if (lbl == nil) {
        //label size
        CGRect frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30);

        lbl = [[UILabel alloc] initWithFrame:frame];
        [lbl setTextAlignment:NSTextAlignmentCenter];
        [lbl setBackgroundColor:[UIColor clearColor]];
        //here you can play with fonts
        [lbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:15.0]];

    }
    //picker view array is the datasource
    [lbl setText:[extEventCatListArray[row] valueForKey:kKeyId]];

    return lbl;
}

#pragma mark
#pragma mark TextField Delegate method
#pragma mark

// --------------------------------------------------------------------------------
//  textFieldDidBeginEditing:
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField isEqual:_txfSearchField] && _txfSearchField.inputView == nil) {

        // Add overlay view
        [self addChildViewController:_resultsController];
        _resultsController.view.frame = CGRectMake(0, 50, SCREEN_WIDTH, 150);
        _resultsController.view.alpha = 0.0f;
        [self.view addSubview:_resultsController.view];
        [UIView animateWithDuration:0.5
                         animations:^{
            self->_resultsController.view.alpha = 1.0f;
                         }];
        [_resultsController didMoveToParentViewController:self];
    }
}

// --------------------------------------------------------------------------------
//  textFieldDidEndEditing:
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField isEqual:_txfSearchField]) {
        // Remove  overlay view from view
        [_resultsController willMoveToParentViewController:nil];
        [UIView animateWithDuration:0.5
                         animations:^{
            self->_resultsController.view.alpha = 0.0f;
                         }
                         completion:^(BOOL finished) {
            [self->_resultsController.view removeFromSuperview];
            [self->_resultsController removeFromParentViewController];
                         }];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // Return
    return YES;
}
// --------------------------------------------------------------------------------
//  textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    // Search
    [self searchEventForSearchText:textField.text];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([textField isEqual:_txfSearchField]) {
        [_tableDataSource sourceTextHasChanged:_txfSearchField.text];
    } else {
        NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [self searchEventForSearchText:str];
    }

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidEndEditing

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // Search
    [self searchEventForDateOrLoc:nil];
    [self searchEventForSearchText:nil];
    [textField resignFirstResponder];

    // Return
    return YES;
}

#pragma mark
#pragma mark Search methods
#pragma mark
// --------------------------------------------------------------------------------
// searchEventsForSearchText:

- (void)searchEventForDateOrLoc:(NSString *)searchText {
    if ([searchText length] > 0) {
        NSString *filter = @"%K CONTAINS[cd] %@ OR %K CONTAINS[cd] %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyAddress, searchText, kKeyEvent_Date, searchText];
        nearbyEvents = [sourceArr filteredArrayUsingPredicate:predicate];
        [self displayUsersDotsOnRadar];
        (APP_DELEGATE).isEventFilterApplied = YES;
    } else {
        if (settingDistance == latestDistance) {
            nearbyEvents = sourceArr;
            [self displayUsersDotsOnRadar];
        } else {
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        }
        (APP_DELEGATE).isEventFilterApplied = NO;
    }
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];

}

// --------------------------------------------------------------------------------
// searchEventsForSearchText:

- (void)searchEventForSearchText:(NSString *)searchText {

    if (searchText.length > 0) {

        if ([_txfSearchField.text length] > 0 && [_txfSearchField.text length] > 0) {
            eventFilter = @"%K CONTAINS[cd] %@ AND %K CONTAINS[cd] %@";
            eventPredicate = [NSPredicate predicateWithFormat:eventFilter, kKeyAddress, _txfSearchField.text, kKeyName, searchText];
        }
        if ([_txfSearchField.text length] > 0) {
            eventFilter = @"%K CONTAINS[cd] %@";
            eventPredicate = [NSPredicate predicateWithFormat:eventFilter, kKeyAddress, _txfSearchField.text];
        } else {
            eventFilter = @"%K CONTAINS[cd] %@";
            eventPredicate = [NSPredicate predicateWithFormat:eventFilter, kKeyName, searchText];
        }

        nearbyEvents = [sourceArr filteredArrayUsingPredicate:eventPredicate];
        [self displayUsersDotsOnRadar];
        (APP_DELEGATE).isEventFilterApplied = YES;
    } else {
        if (settingDistance == latestDistance) {
            nearbyEvents = sourceArr;
            [self displayUsersDotsOnRadar];
        } else {
            [self displayUsersDotsOnRadar];
            [self calculateAndGetNewUsers];
        }
        (APP_DELEGATE).isEventFilterApplied = NO;
    }
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationEventRadarChanges object:nearbyEvents];
    self.lblInPpl.text = [NSString stringWithFormat:@"%ld event in %.f %@", (unsigned long) nearbyEvents.count, latestDistance, distanceUnit];
}

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txfSearchField resignFirstResponder];
    [self.txfFltredSearch resignFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
