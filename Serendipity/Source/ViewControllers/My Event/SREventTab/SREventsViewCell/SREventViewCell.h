#import <UIKit/UIKit.h>

@interface SREventViewCell : UITableViewCell
{
    //Instance variables
    
}
//Properties
@property (weak, nonatomic) IBOutlet UIImageView *imgBGCell;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePhoto;
@property (weak, nonatomic) IBOutlet UIImageView *rejectEventShadow;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblConvertedEventMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblMembers;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lblareGoing;
@property (weak, nonatomic) IBOutlet UIImageView *imgGroups;
@property (weak, nonatomic) IBOutlet UIButton *imgDirectionBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDay;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;
@end
