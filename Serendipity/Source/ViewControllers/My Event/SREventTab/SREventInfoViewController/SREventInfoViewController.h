#import <UIKit/UIKit.h>
#import "NSDate+SRCustomDate.h"

@interface SREventInfoViewController : ParentViewController<UIActionSheetDelegate>
{
    //Server
    SRServerConnection *server;
    
    //Instance Variables
    NSMutableArray *scrollViewImages;
    UIActionSheet *objActionSheet;
    
    NSMutableArray  *cacheImgArr;    
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imgBGPhoto;
@property (weak, nonatomic) IBOutlet UIView *viewProfileImageBG;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewEventInfo;
@property (weak, nonatomic) IBOutlet UIView *viewEventInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UILabel *lblEventDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblEventAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblEventDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnReadMore;
@property (weak, nonatomic) IBOutlet UILabel *lblGoingWith,*lblNotConfirmed,*lblUnableToAttend;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewGoingWith,*scrollviewNotConfirmed,*scrollviewUnableToAttend;
@property (weak, nonatomic) IBOutlet UILabel *lblEventType;
@property (weak, nonatomic) IBOutlet UILabel *lblEventIs;
@property (weak, nonatomic) IBOutlet UILabel *lblSetReminder;
@property (weak, nonatomic) IBOutlet UIButton *btnEventType;
@property (weak, nonatomic) IBOutlet UIButton *btnEventIS;
@property (weak, nonatomic) IBOutlet UIButton *btnSetReminder;


//Other Properties
@property(strong,nonatomic)NSMutableDictionary *eventDetails;
@property (weak) id delegate;

- (IBAction)btnReadMoreAction:(id)sender;
- (IBAction)btnEventTypeAction:(id)sender;
- (IBAction)btnEventISAction:(id)sender;
- (IBAction)btnSetReminderAction:(id)sender;

#pragma mark
#pragma mark Init Method
#pragma mark


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil eventInfo:(NSDictionary *)eventData
               server:(SRServerConnection *)inServer;


@end
