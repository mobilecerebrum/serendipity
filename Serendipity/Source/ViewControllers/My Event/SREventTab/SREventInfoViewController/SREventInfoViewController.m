#define kInterestAddButtonTag 800

#import "SRModalClass.h"
#import "SRNewEventViewController.h"

#import "SREventInfoViewController.h"

@interface SREventInfoViewController () {
    BOOL isReadMore;
}

@end

@implementation SREventInfoViewController
@synthesize eventDetails;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil eventInfo:(NSDictionary *)eventData
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SREventInfoViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        eventDetails = [NSMutableDictionary dictionaryWithDictionary:eventData];
        cacheImgArr = [[NSMutableArray alloc] init];
    }
    //return
    return self;
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {

    //Call Super
    [super viewDidLoad];
    isReadMore = FALSE;
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;


    //NavigationBar left and right button text and actions
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-man-pencil.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(EditEventBtnAction:) forControlEvents:UIControlEventTouchUpInside];


    self.btnReadMore.hidden = YES;

    // Adjust Event Picture appearence
    self.imgProfileImage.layer.cornerRadius = self.imgProfileImage.frame.size.width / 2;
    self.imgProfileImage.layer.masksToBounds = YES;
    self.imgProfileImage.clipsToBounds = YES;


    //Add tap gesture on scrollview for end editing.
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.scrollviewEventInfo addGestureRecognizer:singleTap];
}

//------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    // Update scroll view
    for (UIView *subview in[self.scrollviewGoingWith subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (UIView *subview in[self.scrollviewNotConfirmed subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    for (UIView *subview in[self.scrollviewUnableToAttend subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }

    // Display the group info
    [self displayEventInfo];
    [self setScrollContentSize];
}

#pragma mark
#pragma mark Private Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// displayEventInfo

- (void)displayEventInfo {

    //Get the updated Event data if user edited
    eventDetails = [NSMutableDictionary dictionaryWithDictionary:(APP_DELEGATE).server.eventDetailInfo];

    // Title
    [SRModalClass setNavTitle:[NSString stringWithFormat:@"%@", eventDetails[kKeyName]] forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    // Event image
    if (eventDetails[kKeyImageObject] != nil) {
        self.imgBGPhoto.image = eventDetails[kKeyImageObject];
        self.imgProfileImage.image = eventDetails[kKeyImageObject];
    } else {
        if ([eventDetails[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
            // Get result
            if ([eventDetails[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                NSString *imageName = [eventDetails[kKeyMediaImage] objectForKey:kKeyImageName];

                NSString *imageUrl;
                if ([imageName length] > 0) {
                    if ([eventDetails[kKeyExternalEvent] isEqualToString:@"1"]) {
                        imageUrl = [NSString stringWithFormat:@"%@", imageName];
                    } else
                        imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                    [self.imgBGPhoto sd_setImageWithURL:[NSURL URLWithString:imageUrl]];


                    if ([eventDetails[kKeyExternalEvent] isEqualToString:@"1"]) {
                        imageUrl = [NSString stringWithFormat:@"%@", imageName];
                    } else
                        imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                    [self.imgProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else {
                    self.imgBGPhoto.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    self.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
            }
        } else {
            self.imgBGPhoto.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            self.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        }
    }

    // Event name
    self.lblEventName.text = eventDetails[kKeyName];

    NSDate *eventDate = [NSDate getEventDateFromString:[NSString stringWithFormat:@"%@ %@", [eventDetails valueForKey:kKeyEvent_Date], [eventDetails valueForKey:kKeyEvent_Time]]];
    // Event Type, date, time
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [formatter setTimeZone:utcTimeZone];
    [formatter setDateFormat:@"MMM dd,HH:mm"];
    NSString *timeStr = [formatter stringFromDate:eventDate];

    NSString *type;
    if (timeStr.length > 0) {
        type = [NSString stringWithFormat:@"%@.%@", [eventDetails valueForKey:kKeyEvent_Type], timeStr];
    } else
        type = [NSString stringWithFormat:@"%@", [eventDetails valueForKey:kKeyEvent_Type]];
    self.lblEventDetails.text = type;


    // Event address

//    Get current Address From Google API
    if ([eventDetails[kKeyAddress] length] > 0) {
        self.lblEventAddress.text = [NSString stringWithFormat:@"%@", eventDetails[kKeyAddress]];
    } else
        self.lblEventAddress.text = @"";

    // Event descriptions
    // Display Description
    if ([eventDetails[kKeyDescription] length] > 0) {
        if ([eventDetails[kKeyDescription] length] > 310) {
            NSString *str = eventDetails[kKeyDescription];
            NSString *subStr = [str substringWithRange:NSMakeRange(0, 310)];
            subStr = [NSString stringWithFormat:@"%@...", subStr];
            self.lblEventDesc.text = subStr;
            self.btnReadMore.hidden = NO;
        } else {
            self.lblEventDesc.text = eventDetails[kKeyDescription];
            self.btnReadMore.hidden = YES;
        }
    } else {
        self.lblEventDesc.text = @" ";
        self.btnReadMore.hidden = YES;
    }


    if ([[eventDetails valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
        self.lblGoingWith.hidden = TRUE;
        self.scrollviewGoingWith.hidden = TRUE;
        self.lblEventType.hidden = TRUE;
        self.lblEventIs.hidden = TRUE;
        self.btnEventIS.hidden = TRUE;
        self.btnEventType.hidden = TRUE;
    } else {
        // Event Members
        // Add members to scroll view
        NSMutableArray *eventUserArr = [[NSMutableArray alloc] initWithArray:eventDetails[kKeyEvent_Attendees]];

        // Get event members information in array (Array created here because we want show alphabetically sorted users)
        NSMutableArray *membersArr = [[NSMutableArray alloc] init];
        for (int i = 0; i < eventUserArr.count; i++) {
            NSDictionary *userDetails;
            if ([eventUserArr[i] objectForKey:kKeyFirstName] != nil) {
                userDetails = eventUserArr[i];
            }
//            else if ([[eventUserArr objectAtIndex:i] objectForKey:kKeyUser] != nil) {
//                userDetails = [[eventUserArr objectAtIndex:i] objectForKey:kKeyUser];
//            }
            else {
                NSString *userId = [eventUserArr[i] objectForKey:kKeyUserID];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
                NSArray *filterArr = [server.eventUsersArr filteredArrayUsingPredicate:predicate];
                if ([filterArr count] > 0) {
                    userDetails = filterArr[0];
                    [userDetails setValue:[eventUserArr[i] objectForKey:kKeyInvitationStatus] forKey:kKeyInvitationStatus];
                    [userDetails setValue:[eventUserArr[i] objectForKey:kKeyIsAdmin] forKey:kKeyIsAdmin];
                }
            }
            if (userDetails.allKeys.count > 0) {
                [membersArr addObject:userDetails];
            }
        }

        // Get sorted data in member array
        membersArr = [SRModalClass sortMembersArray:membersArr];
        CGRect latestFrame = CGRectZero;
        CGRect latestFrame1 = CGRectZero;
        CGRect latestFrame2 = CGRectZero;
        UIView *subView;
        for (NSDictionary *userDict in membersArr) {
            // Add view and
            if ([userDict[kKeyInvitationStatus] isEqualToString:@"0"]) {
                // Add view and
                if (CGRectIsEmpty(latestFrame1)) {
                    latestFrame1 = CGRectMake(0, 0, 70, self.scrollviewNotConfirmed.frame.size.height);
                } else
                    latestFrame1 = CGRectMake(latestFrame1.origin.x + 74, 0, 70, self.scrollviewNotConfirmed.frame.size.height);
                subView = [[UIView alloc] initWithFrame:latestFrame1];
            } else if ([userDict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                // Add to scroll view
                if (CGRectIsEmpty(latestFrame)) {
                    latestFrame = CGRectMake(0, 0, 70, self.scrollviewGoingWith.frame.size.height);
                } else
                    latestFrame = CGRectMake(latestFrame.origin.x + 74, 0, 70, self.scrollviewGoingWith.frame.size.height);
                subView = [[UIView alloc] initWithFrame:latestFrame];
            } else {
                // Add view and
                if (CGRectIsEmpty(latestFrame2)) {
                    latestFrame2 = CGRectMake(0, 0, 70, self.scrollviewUnableToAttend.frame.size.height);
                } else
                    latestFrame2 = CGRectMake(latestFrame2.origin.x + 74, 0, 70, self.scrollviewUnableToAttend.frame.size.height);
                subView = [[UIView alloc] initWithFrame:latestFrame2];
            }
            // Add views

            subView.backgroundColor = [UIColor clearColor];

            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 2, 50, 50)];
            imgView.contentMode = UIViewContentModeScaleToFill;
            imgView.layer.cornerRadius = imgView.frame.size.width / 2.0;
            imgView.layer.masksToBounds = YES;
            imgView.image = [UIImage imageNamed:@"profile_menu.png"];

            // Apply image

            if ([userDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {

                NSDictionary *profileDict = userDict[kKeyProfileImage];
                NSString *imageName = profileDict[kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                    [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else
                    imgView.image = [UIImage imageNamed:@"profile_menu.png"];
            } else
                imgView.image = [UIImage imageNamed:@"profile_menu.png"];

            // Name
            UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 52, 70, 18)];
            if ([userDict[kKeyDegree] isKindOfClass:[NSNumber class]] && [userDict[kKeyDegree] integerValue] == 1) {
                lblName.textColor = [UIColor whiteColor];
            } else
                lblName.textColor = [UIColor orangeColor];

            lblName.backgroundColor = [UIColor clearColor];
            lblName.textAlignment = NSTextAlignmentCenter;
            lblName.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
            if ([userDict[kKeyId] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                lblName.text = @"Me";
            } else {

                lblName.text = [NSString stringWithFormat:@"%@ %@.", userDict[kKeyFirstName], [userDict[kKeyLastName] length] > 0 ? [userDict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)] : @""];
            }

            // Label if admin
            UILabel *lblAdmin = [[UILabel alloc] initWithFrame:CGRectMake(0, 51 + 18, 70, 18)];
            if ([userDict[kKeyIsAdmin] isEqualToString:@"1"]) {
                lblAdmin.textColor = [UIColor whiteColor];
                lblAdmin.backgroundColor = [UIColor clearColor];
                lblAdmin.textAlignment = NSTextAlignmentCenter;
                lblAdmin.font = [UIFont fontWithName:kFontHelveticaMedium size:10];
                lblAdmin.text = @"(Admin)";

            }
            // Add views
            [subView addSubview:imgView];
            [subView addSubview:lblName];
            [subView addSubview:lblAdmin];


            if ([userDict[kKeyInvitationStatus] isEqualToString:@"0"]) {
                // Add to scroll view
                [self.scrollviewNotConfirmed addSubview:subView];
            } else if ([userDict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                // Add to scroll view
                [self.scrollviewGoingWith addSubview:subView];
            } else {
                // Add to scroll view
                [self.scrollviewUnableToAttend addSubview:subView];
            }


            if ((latestFrame.origin.x + 100) > SCREEN_WIDTH) {
                self.scrollviewGoingWith.contentSize = CGSizeMake(latestFrame.origin.x + 100, 0);
                self.scrollviewGoingWith.scrollEnabled = YES;
                self.scrollviewGoingWith.backgroundColor = [UIColor clearColor];
            }

            if ((latestFrame1.origin.x + 100) > SCREEN_WIDTH) {
                self.scrollviewNotConfirmed.contentSize = CGSizeMake(latestFrame1.origin.x + 100, 0);
                self.scrollviewNotConfirmed.scrollEnabled = YES;
                self.scrollviewNotConfirmed.backgroundColor = [UIColor clearColor];
            }

            if ((latestFrame2.origin.x + 100) > SCREEN_WIDTH) {
                self.scrollviewUnableToAttend.contentSize = CGSizeMake(latestFrame2.origin.x + 100, 0);
                self.scrollviewUnableToAttend.scrollEnabled = YES;
                self.scrollviewUnableToAttend.backgroundColor = [UIColor clearColor];
            }

        }


        // Update Empty scrollView height
        NSUInteger subViewCount = self.scrollviewGoingWith.subviews.count;
        CGRect rect;
        if (subViewCount < 1) {
            subViewCount = 0;
            rect = self.scrollviewGoingWith.frame;
            rect.size.height = 10;
            self.scrollviewGoingWith.frame = rect;
            self.lblGoingWith.text = [NSString stringWithFormat:@"%@ (%lu users)", @"You are going with:", (unsigned long) subViewCount];
        } else {
            rect = self.scrollviewGoingWith.frame;
            rect.size.height = 90;
            self.scrollviewGoingWith.frame = rect;
            self.lblGoingWith.text = [NSString stringWithFormat:@"%@ (%lu users)", @"You are going with:", (unsigned long) subViewCount];
        }
        subViewCount = self.scrollviewNotConfirmed.subviews.count;
        if (subViewCount < 1) {
            subViewCount = 0;

            rect = self.lblNotConfirmed.frame;
            rect.origin.y = self.scrollviewGoingWith.frame.origin.y + self.scrollviewGoingWith.frame.size.height + 10;
            self.lblNotConfirmed.frame = rect;

            rect = self.scrollviewNotConfirmed.frame;
            rect.size.height = 10;
            rect.origin.y = self.lblNotConfirmed.frame.origin.y + 20;
            self.scrollviewNotConfirmed.frame = rect;
            if (SCREEN_WIDTH <= 320) {
                [self.lblNotConfirmed setFont:[UIFont systemFontOfSize:12.0]];
            }
            self.lblNotConfirmed.text = [NSString stringWithFormat:@"%@ (%lu users)", @"Invited users who have not confirmed:", (unsigned long) subViewCount];
        } else {
            //        rect = self.scrollviewNotConfirmed.frame;
            //        rect.size.height = 90;
            rect = self.lblNotConfirmed.frame;
            rect.origin.y = self.scrollviewGoingWith.frame.origin.y + self.scrollviewGoingWith.frame.size.height + 10;
            self.lblNotConfirmed.frame = rect;

            rect = self.scrollviewNotConfirmed.frame;
            rect.size.height = 90;
            rect.origin.y = self.lblNotConfirmed.frame.origin.y + 20;
            self.scrollviewNotConfirmed.frame = rect;
            if (SCREEN_WIDTH <= 320) {
                [self.lblNotConfirmed setFont:[UIFont systemFontOfSize:12.0]];
            }
            self.lblNotConfirmed.text = [NSString stringWithFormat:@"%@ (%lu users)", @"Invited users who have not confirmed:", (unsigned long) subViewCount];
        }
        subViewCount = self.scrollviewUnableToAttend.subviews.count;
        if (subViewCount < 1) {
            subViewCount = 0;

            rect = self.lblUnableToAttend.frame;
            rect.origin.y = self.scrollviewNotConfirmed.frame.origin.y + self.scrollviewNotConfirmed.frame.size.height + 10;
            self.lblUnableToAttend.frame = rect;

            rect = self.scrollviewUnableToAttend.frame;
            rect.size.height = 10;
            rect.origin.y = self.lblUnableToAttend.frame.origin.y + 20;
            self.scrollviewUnableToAttend.frame = rect;
            self.lblUnableToAttend.text = [NSString stringWithFormat:@"%@ (%lu users)", @"Users unable to attend:", (unsigned long) subViewCount];
        } else {
            //        rect = self.scrollviewUnableToAttend.frame;
            //        rect.size.height = 90;
            rect = self.lblUnableToAttend.frame;
            rect.origin.y = self.scrollviewNotConfirmed.frame.origin.y + self.scrollviewNotConfirmed.frame.size.height + 10;
            self.lblUnableToAttend.frame = rect;

            rect = self.scrollviewNotConfirmed.frame;
            rect.size.height = 90;
            rect.origin.y = self.lblUnableToAttend.frame.origin.y + 20;
            self.scrollviewUnableToAttend.frame = rect;
            self.lblUnableToAttend.text = [NSString stringWithFormat:@"%@ (%lu users)", @"Users unable to attend:", (unsigned long) subViewCount];
        }


        rect = self.btnEventType.frame;
        rect.origin.y = self.scrollviewUnableToAttend.frame.origin.y + self.scrollviewUnableToAttend.frame.size.height + 30;
        self.btnEventType.frame = rect;

        rect = self.lblEventType.frame;
        rect.origin.y = self.btnEventType.frame.origin.y + self.btnEventType.frame.size.height + 30;
        self.lblEventType.frame = rect;

        rect = self.btnEventIS.frame;
        rect.origin.y = self.btnEventType.frame.origin.y + self.btnEventType.frame.size.height + 30;
        self.btnEventIS.frame = rect;

        rect = self.lblEventIs.frame;
        rect.origin.y = self.btnEventType.frame.origin.y + self.btnEventType.frame.size.height + 30;
        self.lblEventIs.frame = rect;

        rect = self.btnSetReminder.frame;
        rect.origin.y = self.scrollviewUnableToAttend.frame.origin.y + self.scrollviewUnableToAttend.frame.size.height + 30;
        self.btnSetReminder.frame = rect;

        rect = self.lblSetReminder.frame;
        rect.origin.y = self.btnEventIS.frame.origin.y + self.btnEventIS.frame.size.height + 30;
        self.lblSetReminder.frame = rect;









        // Define the group type
        if ([eventDetails[kKeyIs_Public] isEqualToString:@"1"])
            self.lblEventType.text = @"Public(anyone can invite)";
        else
            self.lblEventType.text = @"Private";

        // Define group on/off
        if ([eventDetails[kKeyGroup_is] isEqualToString:@"1"])
            self.lblEventIs.text = @"On";
    }


    if ([SRModalClass isEventExist:eventDetails[kKeyId]]) {
        int reminderTime = [SRModalClass getStoredReminder:eventDetails[kKeyId]];

        if (reminderTime == 0) {
            self.lblSetReminder.text = @"";

        } else if (reminderTime == 30) {
            self.lblSetReminder.text = [NSLocalizedString(@"30 Minutes.txt", "") stringByAppendingString:@" before"];

        } else if (reminderTime == 45) {
            self.lblSetReminder.text = [NSLocalizedString(@"45 Minutes.txt", "") stringByAppendingString:@" before"];

        } else if (reminderTime == 60) {
            self.lblSetReminder.text = [NSLocalizedString(@"1 Hour.txt", "") stringByAppendingString:@" before"];

        } else if (reminderTime == 120) {
            self.lblSetReminder.text = [NSLocalizedString(@"2 Hour.txt", "") stringByAppendingString:@" before"];

        } else if (reminderTime == 240) {
            self.lblSetReminder.text = [NSLocalizedString(@"4 Hour.txt", "") stringByAppendingString:@" before"];

        } else if (reminderTime == 1440) {
            self.lblSetReminder.text = [NSLocalizedString(@"1 Day.txt", "") stringByAppendingString:@" before"];

        } else //NSLocalizedString(@"1 Hour.txt", "")
        {
            self.lblSetReminder.text = @"";
        }
    } else {
        self.lblSetReminder.text = @"";
    }

    self.scrollviewEventInfo.contentSize = CGSizeMake(0, self.viewEventInfo.frame.origin.y + self.viewEventInfo.frame.size.height + 200);
    self.scrollviewEventInfo.scrollEnabled = YES;
    self.scrollviewEventInfo.backgroundColor = [UIColor clearColor];

}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

// ---------------------------------------------------------------------------------------
// EditEventBtnAction:

- (void)EditEventBtnAction:(id)sender {

    NSMutableArray *eventUserArr = [[NSMutableArray alloc] initWithArray:eventDetails[kKeyEvent_Attendees]];
    for (int i = 0; i < eventUserArr.count; i++) {
        if ([[server.loggedInUserInfo valueForKey:kKeyId] isEqualToString:[eventUserArr[i] valueForKey:kKeyUserID]] && [[eventUserArr[i] valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
            [SRModalClass showAlert:@"You have not accepted event invitation so you cannot edit event information."];
        } else if ([[server.loggedInUserInfo valueForKey:kKeyId] isEqualToString:[eventUserArr[i] valueForKey:kKeyUserID]]) {
            if ([[eventDetails valueForKey:kKeyIs_Public] isEqualToString:@"0"] && [[eventUserArr[i] valueForKey:kKeyIsAdmin] isEqualToString:@"0"]) {
                [SRModalClass showAlert:@"Only admins can edit event information."];
            } else if ([[eventUserArr[i] valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
                [SRModalClass showAlert:@"You have not accepted event invitation so you cannot edit event information."];
            } else {
                SRNewEventViewController *objEditEvent = [[SRNewEventViewController alloc] initWithNibName:@"SRNewEventViewController" bundle:nil dict:(APP_DELEGATE).server.eventDetailInfo server:(APP_DELEGATE).server];
                objEditEvent.delegate = self;

                objEditEvent.dataDict = eventDetails;
                [self.navigationController pushViewController:objEditEvent animated:NO];
            }
        }
    }
}


// ---------------------------------------------------------------------------------------
// btnReadMoreAction:

- (IBAction)btnReadMoreAction:(id)sender {
    if ([self.btnReadMore.titleLabel.text isEqualToString:@" << "]) {
        isReadMore = FALSE;
        NSString *str = eventDetails[kKeyDescription];
        NSString *subStr = [str substringWithRange:NSMakeRange(0, 310)];
        subStr = [NSString stringWithFormat:@"%@...", subStr];
        self.lblEventDesc.text = subStr;

        [self.btnReadMore setTitle:NSLocalizedString(@"btn.read_more.txt", @"") forState:UIControlStateNormal];
    } else {
        isReadMore = TRUE;
        NSString *str = eventDetails[kKeyDescription];
        self.lblEventDesc.text = str;
        [self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
    }

    // Set Scroll Size
    [self setScrollContentSize];
}

// ---------------------------------------------------------------------------------------
// btnEventTypeAction:

- (IBAction)btnEventTypeAction:(id)sender {
}

// ---------------------------------------------------------------------------------------
// btnEventISAction:

- (IBAction)btnEventISAction:(id)sender {
}

// ---------------------------------------------------------------------------------------
// btnSetReminderAction:

- (IBAction)btnSetReminderAction:(id)sender {
    NSDate *eventDate = [NSDate getEventDateFromString:[NSString stringWithFormat:@"%@ %@", [eventDetails valueForKey:kKeyEvent_Date], [eventDetails valueForKey:kKeyEvent_Time]]];

    //Check here date is passed or not
    if (![NSDate isEndDateIsSmallerThanCurrent:eventDate]) {
        // Present action sheet
        objActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel.button.title.text", "") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"30 Minutes.txt", ""), NSLocalizedString(@"45 Minutes.txt", ""), NSLocalizedString(@"1 Hour.txt", ""), NSLocalizedString(@"2 Hour.txt", ""), NSLocalizedString(@"4 Hour.txt", ""), NSLocalizedString(@"1 Day.txt", ""), nil];

        // Show SetReminder ActionSheet
        [objActionSheet showFromRect:[sender frame] inView:self.view animated:YES];
        [objActionSheet setBackgroundColor:[UIColor orangeColor]];
    } else {
        [SRModalClass showAlert:@"Sorry, this event already occurred so it cannot be added to your calendar."];
    }

}

#pragma mark
#pragma mark ActionSheet Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// clickedButtonAtIndex:

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 6) {

    } else {
        NSMutableDictionary *eventInfo;
        eventInfo = [NSMutableDictionary dictionaryWithDictionary:eventDetails];

        if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"30 Minutes.txt", "")]) {
            [eventInfo setValue:@"30" forKey:kKeyReminder];

        } else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"45 Minutes.txt", "")]) {
            [eventInfo setValue:@"45" forKey:kKeyReminder];
        } else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"1 Hour.txt", "")]) {
            [eventInfo setValue:@"60" forKey:kKeyReminder];
        } else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"2 Hour.txt", "")]) {
            [eventInfo setValue:@"120" forKey:kKeyReminder];
        } else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"4 Hour.txt", "")]) {
            [eventInfo setValue:@"240" forKey:kKeyReminder];
        } else if ([[popup buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"1 Day.txt", "")]) {
            [eventInfo setValue:@"1440" forKey:kKeyReminder];
        } else {
            [eventInfo setValue:@"0" forKey:kKeyReminder];
        }
        [SRModalClass updateReminderForEvent:eventInfo];

        self.lblSetReminder.text = [[objActionSheet buttonTitleAtIndex:buttonIndex] stringByAppendingString:@" before"];
    }
}

// --------------------------------------------------------------------------------
// willPresentActionSheet:

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *subview in actionSheet.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *) subview;
            [button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            NSString *buttonText = button.titleLabel.text;
            if ([buttonText isEqualToString:NSLocalizedString(@"cancel.button.title.text", nil)]) {
                [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            }
        }
    }

}

//code for add images in scrollview programmatically
#pragma mark
#pragma mark Scroll Customization Method
#pragma mark

// --------------------------------------------------------------------------------
// addDashedBorderWithColor:

- (CAShapeLayer *)addDashedBorderWithColor:(CGFloat)lineNo {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    CGSize frameSize = CGSizeMake(65, 65);
    CGRect shapeRect = CGRectMake(0.0f, 0.0f, frameSize.width, frameSize.height);
    [shapeLayer setBounds:shapeRect];
    [shapeLayer setPosition:CGPointMake(frameSize.width / 2, frameSize.height / 2)];

    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[[UIColor whiteColor] CGColor]];
    [shapeLayer setLineWidth:lineNo];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineDashPattern:
            @[@10,
                    @2]];

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:shapeRect cornerRadius:frameSize.width / 2];
    [shapeLayer setPath:path.CGPath];

    // Return
    return shapeLayer;
}

// --------------------------------------------------------------------------------
// addItemsToScrollView:

- (void)addItemsToScrollView:(BOOL)isProfile {
    // Remove all objects from scroll view
    NSArray *imgsArr;
    imgsArr = scrollViewImages;

    CGRect latestFrame = CGRectZero;

    // Add images to scroll view accordingly
    for (NSUInteger i = 0; i < [imgsArr count]; i++) {
        UIView *subView = [[UIView alloc] init];
        if (i == 0) {
            subView.frame = CGRectMake(5, 0, 65, 70);
        } else
            subView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 4, 0, 65, 70);
        latestFrame = subView.frame;
        subView.backgroundColor = [UIColor clearColor];

        // Add image view
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 46, -2, 20, 20)];

        imgView.image = [UIImage new];
        imgView.layer.cornerRadius = imgView.frame.size.width / 2;
        imgView.clipsToBounds = YES;

        CAShapeLayer *layer = [self addDashedBorderWithColor:2];
        [imgView.layer addSublayer:layer];

        UILabel *lblImageName = [[UILabel alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 20, 64, 60, 15)];
        lblImageName.textColor = [UIColor whiteColor];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaRegular size:11.0];
        lblImageName.text = [NSString stringWithFormat:@"image%ld", (unsigned long) i];
        [subView addSubview:lblImageName];
        [btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        btnDelete.tag = kInterestAddButtonTag + i;

        [btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];

        [subView addSubview:imgView];
        [subView addSubview:btnDelete];

        // Add to the scroll view according to flag
        [self.scrollviewGoingWith addSubview:subView];
    }

    // Check if lastest frame is not empty
    if (CGRectEqualToRect(latestFrame, CGRectZero)) {
        latestFrame = CGRectMake(5, 0, 65, 70);
    } else
        latestFrame = CGRectMake(latestFrame.origin.x + 70, 0, 65, 70);

    UIView *btnSuperView = [[UIView alloc] init];
    btnSuperView.frame = latestFrame;

    // Add to scroll view
    UIButton *btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
    [btnAdd addTarget:self action:@selector(actionOnBtnAdd:) forControlEvents:UIControlEventTouchUpInside];
    [btnSuperView addSubview:btnAdd];

    btnAdd.tag = kInterestAddButtonTag;
    [btnAdd.layer addSublayer:[self addDashedBorderWithColor:1]];
    [btnAdd setImage:[UIImage imageNamed:@"miles-set-plus.png"] forState:UIControlStateNormal];
    [self.scrollviewGoingWith addSubview:btnSuperView];


    if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {

        self.scrollviewGoingWith.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
        self.scrollviewGoingWith.scrollEnabled = YES;
        //self.scrollViewAddMember.backgroundColor = [UIColor clearColor];

    }
}

// -----------------------------------------------------------------------------------------------
// actionOnBtnDelete:

- (void)actionOnBtnDelete:(UIButton *)inSender {

    for (UIView *subview in[self.scrollviewGoingWith subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    // Remove object at index
    NSInteger i = inSender.tag / kInterestAddButtonTag;
    [scrollViewImages removeObjectAtIndex:i];

    [self addItemsToScrollView:NO];

}

// -----------------------------------------------------------------------------------------------
// actionOnBtnAdd:

- (void)actionOnBtnAdd:(UIButton *)inSender {

    for (UIView *subview in[self.scrollviewGoingWith subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    // Add object at index
    NSInteger i = inSender.tag / kInterestAddButtonTag;
    i++;
    NSString *string = [NSString stringWithFormat:@"%ld", (long) i];
    [scrollViewImages addObject:string];
    [self addItemsToScrollView:NO];

}

//---------------------------------------------------------------------------------------------------------------------------------
// setScrollContentSize

- (void)setScrollContentSize {
    int y;
    CGRect frame = self.lblEventName.frame;
    CGRect textRect = [self.lblEventName.text boundingRectWithSize:CGSizeMake(self.lblEventName.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lblEventName.font} context:nil];
    frame.size.height = textRect.size.height;
    self.lblEventName.frame = frame;

    // height for about event Details
    y = self.lblEventName.frame.origin.y + self.lblEventName.frame.size.height;
    if ([[eventDetails valueForKey:kKeyEvent_Type] length] > 0) {
        frame.size.height = textRect.size.height;
        self.lblEventDetails.frame = frame;
        frame = self.lblEventDetails.frame;
        frame.origin.y = y + 5;
        self.lblEventDetails.frame = frame;

        //height for Profile Description Details

        textRect = [self.lblEventDetails.text boundingRectWithSize:CGSizeMake(self.lblEventDetails.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lblEventDetails.font} context:nil];

        frame.size.height = textRect.size.height;
        self.lblEventDetails.frame = frame;

        y = self.lblEventDetails.frame.origin.y + self.lblEventDetails.frame.size.height;
    }

    if ([[eventDetails valueForKey:kKeyAddress] length] > 0) {
        frame.size.height = textRect.size.height;
        self.lblEventAddress.frame = frame;
        frame = self.lblEventAddress.frame;
        frame.origin.y = y + 5;
        self.lblEventAddress.frame = frame;

        //height for Profile Description Details

        textRect = [self.lblEventAddress.text boundingRectWithSize:CGSizeMake(self.lblEventAddress.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lblEventAddress.font} context:nil];

        frame.size.height = textRect.size.height;
        self.lblEventAddress.frame = frame;

        y = self.lblEventAddress.frame.origin.y + self.lblEventAddress.frame.size.height;
    }

    if ([eventDetails[kKeyDescription] length] > 0) {
        frame.size.height = textRect.size.height;
        self.lblEventDesc.frame = frame;
        frame = self.lblEventDesc.frame;
        frame.origin.y = y + 5;
        self.lblEventDesc.frame = frame;

        //height for Profile Description Details

        textRect = [self.lblEventDesc.text boundingRectWithSize:CGSizeMake(self.lblEventDesc.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lblEventDesc.font} context:nil];

        frame.size.height = textRect.size.height;
        self.lblEventDesc.frame = frame;
        self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x, self.lblEventDesc.frame.origin.y + self.lblEventDesc.frame.size.height, self.btnReadMore.frame.size.width, self.btnReadMore.frame.size.height);

        y = y + self.lblEventDesc.frame.size.height + self.btnReadMore.frame.size.height;

    }

    if ([[eventDetails valueForKey:kKeyExternalEvent] isEqualToString:@"1"]) {
        self.lblEventType.hidden = TRUE;
        self.lblEventIs.hidden = TRUE;
        self.btnEventType.hidden = TRUE;
        self.btnEventIS.hidden = TRUE;
        self.lblGoingWith.hidden = TRUE;
        self.scrollviewGoingWith.hidden = TRUE;
        self.lblNotConfirmed.hidden = TRUE;
        self.scrollviewNotConfirmed.hidden = TRUE;
        self.lblUnableToAttend.hidden = TRUE;
        self.scrollviewUnableToAttend.hidden = TRUE;

        //set frame for set reminder lbl
        frame = self.lblSetReminder.frame;
        frame.origin.y = y + 10;
        self.lblSetReminder.frame = frame;

        //set frame for set reminder btn
        frame = self.btnSetReminder.frame;
        frame.origin.y = y + 10;
        self.btnSetReminder.frame = frame;

        y = y + self.btnSetReminder.frame.size.height + 20;
        if (isReadMore) {
            y = y + 180;
        }

    } else {
        frame = self.lblGoingWith.frame;
        frame.origin.y = y + 15;
        self.lblGoingWith.frame = frame;

        y = y + self.lblGoingWith.frame.size.height;
        frame = self.scrollviewGoingWith.frame;
        frame.origin.y = y + 25;
        self.scrollviewGoingWith.frame = frame;

        y = y + self.scrollviewGoingWith.frame.size.height;
        frame = self.lblNotConfirmed.frame;
        frame.origin.y = y + 15;
        self.lblNotConfirmed.frame = frame;

        y = y + self.lblNotConfirmed.frame.size.height;
        frame = self.scrollviewNotConfirmed.frame;
        frame.origin.y = y + 25;
        self.scrollviewNotConfirmed.frame = frame;

        y = y + self.scrollviewNotConfirmed.frame.size.height;
        frame = self.lblUnableToAttend.frame;
        frame.origin.y = y + 15;
        self.lblUnableToAttend.frame = frame;

        y = y + self.lblUnableToAttend.frame.size.height;
        frame = self.scrollviewUnableToAttend.frame;
        frame.origin.y = y + 25;
        self.scrollviewUnableToAttend.frame = frame;

        y = y + self.scrollviewUnableToAttend.frame.size.height;
        frame = self.lblEventType.frame;
        frame.origin.y = y + 25;
        self.lblEventType.frame = frame;

        frame = self.btnEventType.frame;
        frame.origin.y = y + 25;
        self.btnEventType.frame = frame;

        y = y + self.lblEventType.frame.size.height;
        frame = self.lblEventIs.frame;
        frame.origin.y = y + 25;
        self.lblEventIs.frame = frame;

        frame = self.btnEventIS.frame;
        frame.origin.y = y + 25;
        self.btnEventIS.frame = frame;

        y = y + self.lblEventIs.frame.size.height;
        frame = self.lblSetReminder.frame;
        frame.origin.y = y + 25;
        self.lblSetReminder.frame = frame;

        frame = self.btnSetReminder.frame;
        frame.origin.y = y + 25;
        self.btnSetReminder.frame = frame;
        y = y + self.btnSetReminder.frame.size.height + 30;
    }


    self.viewEventInfo.frame = CGRectMake(self.viewEventInfo.frame.origin.x, self.viewEventInfo.frame.origin.y, self.viewEventInfo.frame.size.width, y);

    self.scrollviewEventInfo.contentSize = CGSizeMake(0, y + self.viewEventInfo.frame.origin.y + 44);
    self.scrollviewEventInfo.scrollEnabled = YES;
}


#pragma mark
#pragma mark Tap Gesture
#pragma mark

// ---------------------------------------------------------------------------------------
// singleTapGestureCaptured:
- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture {
    self.scrollviewEventInfo.contentOffset = CGPointMake(0, 0);
    [self.view endEditing:YES];
}
@end
