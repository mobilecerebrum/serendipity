
#import "SRNewEventViewController.h"
#import "SRModalClass.h"
#import "SRLocationSetViewController.h"
#import "SRMyConnectionsViewController.h"
#import "SRMemberViewController.h"
#import "CommonFunction.h"
#import "UITextView+Placeholder.h"

#define kLeftBtnTag 600
#define kRightSaveBtnTag 601
#define kRightEditBtnTag 602

#define kKeySelectedObj @"selecetdObj"

@interface SRNewEventViewController () {
    NSMutableArray *membersArr;
    CLLocationCoordinate2D locationCoordinate;
    NSData *imgData;
}
@end

@implementation SRNewEventViewController
@synthesize dataDict;
#pragma mark
#pragma mark Init Method
#pragma mark

//-----------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
                 dict:(NSDictionary *)eventDict
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SRNewEventViewController" bundle:nibBundleOrNil];
    if (self) {

        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(createEventSucceed:)
                              name:kKeyNotificationCreateEventSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(createEventFailed:)
                              name:kKeyNotificationCreateEventFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(inviteUserSucceed:)
                              name:kKeyNotificationInviteUserSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(inviteUserFailed:)
                              name:kKeyNotificationInviteUserFailed object:nil];
        // Custom initialization
        server = inServer;

        self.dataDict = [NSMutableDictionary dictionaryWithDictionary:eventDict];

        adminArr = [[NSMutableArray alloc] init];
        membersArr = [[NSMutableArray alloc] init];
        addPeopleImgArr = [[NSMutableArray alloc] init];

    }
    //return

    return self;
}
// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//-----------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call Super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    self.datePicker.minimumDate = [NSDate date];
    //    // Set radio buttons
    [self.onRadioBtn setSelected:TRUE];
    [self.publicRadioBtn setSelected:TRUE];
    [self DisplayEventInfo];

    // Add subview for add new connection
    self.viewAddPic.layer.cornerRadius = 5;
    self.viewAddPic.layer.masksToBounds = YES;
    [self.view addSubview:self.viewAddPic];
    self.viewAddPic.hidden = YES;

//    if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {
        // Date picker toolbar button space
//        UIBarButtonItem *fixedItemSpaceWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//        fixedItemSpaceWidth.width = 280.0f; // or whatever you want
//        self.pickerToolBar.items = @[self.cancelBarBtn, fixedItemSpaceWidth, self.doneBarBtn];
//    }
    _txtviewEventDesc.placeholder = @"Event Description";
    
    self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtName.placeholder attributes:@{NSForegroundColorAttributeName:UIColor.blackColor}];
    self.txtPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtPhoneNumber.placeholder attributes:@{NSForegroundColorAttributeName:UIColor.blackColor}];

}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.txtviewEventDesc.makePlaceHolderCenter = true;
}

- (void)backBtnAction:(id)sender {
    self.pickerView.hidden = YES;
    [self.view endEditing:YES];
    [self.navigationController popToViewController:self.delegate animated:YES];
}

// ---------------------------------------------------------------------------------------
// EditEventBtnAction:

- (void)EditEventBtnAction:(id)sender {

    [self.navigationController popToViewController:self.delegate animated:NO];
}

#pragma mark
#pragma mark Private Method
#pragma mark

// -----------------------------------------------------------------------------------------------------
// Display Edit event data

- (void)DisplayEventInfo {
//    self.lblAddPic.hidden = YES;
    if ([dataDict valueForKey:kKeyName]) {
        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.editEvent.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];


        self.txtEventName.text = [dataDict valueForKey:kKeyName];

        self.txtviewEventDesc.text = [dataDict valueForKey:kKeyDescription];
        //Get Address from Location Coordinates
        self.txtSetLocation.text = [dataDict valueForKey:kKeyAddress];
        self.txtEventType.text = [dataDict valueForKey:kKeyEvent_Type];
        //Get formatted date
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc] init];
        NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        [formatterDate setTimeZone:utcTimeZone];
        [formatterDate setDateFormat:@"yyyy-MM-dd"];
        NSDate *eveDate = [formatterDate dateFromString:[dataDict valueForKey:kKeyEvent_Date]];
        self.txtDate.text = [SRModalClass returnStringInMMDDYYYY:eveDate];

        [formatterDate setDateFormat:@"HH:mm:ss"];
        eveDate = [formatterDate dateFromString:[dataDict valueForKey:kKeyEvent_Time]];
        [formatterDate setDateFormat:@"HH:mm"];
        self.txtTime.text = [formatterDate stringFromDate:eveDate];

//        self.lblAddPic.hidden = NO;
        [self.btnAddPic setImage:[UIImage imageNamed:@"image-edit.png"] forState:UIControlStateNormal];

        if ([[dataDict valueForKey:kKeyIs_Public] isEqualToString:@"1"]) {
            [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.publicRadioBtn setSelected:TRUE];
            [self.privteRadioBtn setSelected:FALSE];
        } else {
            [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.publicRadioBtn setSelected:FALSE];
            [self.privteRadioBtn setSelected:TRUE];
        }

        if ([[dataDict valueForKey:kKeyEvent_Status] isEqualToString:@"1"]) {
            [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.onRadioBtn setSelected:TRUE];
            [self.offRadioBtn setSelected:FALSE];
        } else {
            [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
            [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
            [self.offRadioBtn setSelected:TRUE];
            [self.onRadioBtn setSelected:FALSE];
        }

        //For Event image
        //default Image
        self.imgAddPic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        self.imgBGScreen.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        // Event image
        if (dataDict[kKeyImageObject] != nil) {
            self.imgAddPic.image = dataDict[kKeyImageObject];
            self.imgBGScreen.image = dataDict[kKeyImageObject];
        } else {
            if ([dataDict[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
                // Get result
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    UIImage *img = nil;

                    if ([dataDict[kKeyMediaImage] objectForKey:kKeyImageName] != nil) {
                        NSString *imageName = [dataDict[kKeyMediaImage] objectForKey:kKeyImageName];
                        if ([imageName length] > 0) {
                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                            img = [UIImage imageWithData:
                                    [NSData dataWithContentsOfURL:
                                            [NSURL URLWithString:imageUrl]]];
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (img) {
                                self.imgAddPic.image = img;
                                self.imgBGScreen.image = img;
                            }
                        });
                    }
                });
            } else {
                self.imgAddPic.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                self.imgBGScreen.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            }
        }
        // Filter the users and alloc the views
        for (NSDictionary *userDict in server.eventDetailInfo[kKeyEvent_Attendees]) {
            NSMutableDictionary *userDetails = [NSMutableDictionary dictionary];
            if (userDict[kKeyFirstName] != nil) {
                [userDetails addEntriesFromDictionary:userDict];
            } else if (userDict[kKeyUser] != nil) {
                userDetails = userDict[kKeyUser];
            } else {
                NSString *userId = userDict[kKeyUserID];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
                NSArray *filterArr = [server.eventUsersArr filteredArrayUsingPredicate:predicate];
                if ([filterArr count] > 0) {
                    userDetails = filterArr[0];
                }
            }

            // Add dict to mutable array
            userDetails[kKeySelectedObj] = @"selected";
            [membersArr addObject:userDetails];
        }

        NSDictionary *eventDict = (APP_DELEGATE).server.eventDetailInfo;
        NSArray *members = eventDict[kKeyEvent_Attendees];

        canEdit = NO;
        for (NSUInteger i = 0; i < [members count] && !canEdit; i++) {
            NSDictionary *dict = members[i];
            if ([dict[kKeyUserID] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] && [dict[kKeyInvitationStatus] isEqualToString:@"1"]) {
                if ([[eventDict valueForKey:kKeyIs_Public] boolValue]) {
                    canEdit = YES;
                } else if ([dict[kKeyIsAdmin] isEqualToString:@"1"]) {
                    canEdit = YES;
                } else
                    canEdit = NO;
            }
        }


        if (!canEdit) {
            self.txtEventName.enabled = NO;
            self.btnLocation.userInteractionEnabled = NO;
            self.txtEventType.enabled = NO;
            self.btnDate.enabled = NO;
            self.btnTime.enabled = NO;
            self.txtviewEventDesc.userInteractionEnabled = NO;
            self.publicRadioBtn.enabled = NO;
            self.privteRadioBtn.enabled = NO;
            self.onRadioBtn.enabled = NO;
            self.offRadioBtn.enabled = NO;
            self.btnAddPic.enabled = NO;
            self.txtSetLocation.userInteractionEnabled = NO;
        }
        // Add memebers
        [self addImagesInScrollView];
    } else {
        canEdit = YES;

        // Date Picker
        [self.datePicker setMinimumDate:[NSDate date]];

        [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.createNewEvent.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    }

    //Navigation bar buttons
    if (canEdit) {
        UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"edit_profile_white.png"] forViewNavCon:self offset:-23];
        rightButton.tag = kRightSaveBtnTag;
        [rightButton addTarget:self action:@selector(actionOnButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setTag:kLeftBtnTag];

    //AddPic Background View Rounded Appearance.
    self.viewAddPicBG.layer.cornerRadius = self.viewAddPicBG.frame.size.width / 2;
    self.viewAddPicBG.layer.masksToBounds = YES;
    self.viewAddPicBG.clipsToBounds = YES;

    self.txtviewEventDesc.clipsToBounds = YES;
    self.txtviewEventDesc.layer.cornerRadius = 5.0f;


    self.pickerView.hidden = YES;

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    singleTap.cancelsTouchesInView = YES;
    [self.scrollviewEventDetails addGestureRecognizer:singleTap];
}

// -----------------------------------------------------------------------------------------------------
// getLocationFromAddressString:

- (void)getHSLocationFromAddressString:(NSString *)addressStr CompletionHandler:(void (^)(CLLocationCoordinate2D location))completionHandler {

    
//    double latitude = 0, longitude = 0;
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    //    NSString *esc_addr = [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"%@?format=json&addressdetails=1&q=%@", kNominatimServer, esc_addr];

    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:req]];

//    NSError *error = nil;
    double __block latitude = 0, longitude = 0;

    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
             NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
              if (result) {

                  if ([result containsString:@"\"lat\":"]) {
                      NSRange firstRange = [result rangeOfString:@"\"lat\":"];
                      NSRange finalRange = NSMakeRange(firstRange.location + firstRange.length + 1, 6);
                      NSString *lat = [result substringWithRange:finalRange];
                      latitude = [lat doubleValue];
        //              NSLog(@"contain %@", lat);
                  }
                  if ([result containsString:@"\"lon\":"]) {
                      NSRange firstRange = [result rangeOfString:@"\"lon\":"];
                      NSRange finalRange = NSMakeRange(firstRange.location + firstRange.length + 1, 6);
                      NSString *lon = [result substringWithRange:finalRange];
                      longitude = [lon doubleValue];
        //              NSLog(@"contain %@", lon);
                  }
                  
                  CLLocationCoordinate2D center;
                  center.latitude = latitude;
                  center.longitude = longitude;
                  completionHandler(center);
        }
    }]resume];
//    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
//
//    if([responseCode statusCode] != 200){
//        NSLog(@"Error getting %@, HTTP status code %i", url, [responseCode statusCode]);
//        return nil;
//    }
//
//    return [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
    
}

- (CLLocationCoordinate2D)getLocationFromAddressString:(NSString *)addressStr {
    double latitude = 0, longitude = 0;
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSString *esc_addr = [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"%@?format=json&addressdetails=1&q=%@", kNominatimServer, esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {

        if ([result containsString:@"\"lat\":"]) {
            NSRange firstRange = [result rangeOfString:@"\"lat\":"];
            NSRange finalRange = NSMakeRange(firstRange.location + firstRange.length + 1, 6);
            NSString *lat = [result substringWithRange:finalRange];
            latitude = [lat doubleValue];
     //       NSLog(@"contain %@", lat);
        }
        if ([result containsString:@"\"lon\":"]) {
            NSRange firstRange = [result rangeOfString:@"\"lon\":"];
            NSRange finalRange = NSMakeRange(firstRange.location + firstRange.length + 1, 6);
            NSString *lon = [result substringWithRange:finalRange];
            longitude = [lon doubleValue];
  //          NSLog(@"contain %@", lon);
        }

//        NSScanner *scanner = [NSScanner scannerWithString:result];
//        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
//            [scanner scanDouble:&latitude];
//            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
//                [scanner scanDouble:&longitude];
//            }
//        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

// ------------------------------------------------------------------------------------------------------
// addMembersInScrollView:

- (void)addImagesInScrollView {
    CGRect latestFrame = CGRectZero;
    NSArray *eventMemberArr = [[NSArray alloc] initWithArray:self.dataDict[kKeyEvent_Attendees]];
    NSMutableArray *adminArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < eventMemberArr.count; i++) {
        if ([[eventMemberArr[i] valueForKey:kKeyIsAdmin] isEqualToString:@"1"]) {
            [adminArray addObject:[eventMemberArr[i] valueForKey:kKeyUserID]];
        }
    }

    //Remove old view from scrollview
    for (UIView *oldView in self.scrollInvitePeople.subviews) {
        [oldView removeFromSuperview];
    }


    // Add images to scroll view accordingly
    for (NSUInteger i = 0; i < [membersArr count]; i++) {
        NSDictionary *dict = membersArr[i];
        UIView *subView = [[UIView alloc] init];
        if (i == 0) {
            subView.frame = CGRectMake(5, 0, 70, self.scrollInvitePeople.frame.size.height);
        } else
            subView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 4, 0, 70, self.scrollInvitePeople.frame.size.height);
        latestFrame = subView.frame;
        subView.backgroundColor = [UIColor clearColor];

        // Add image view
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        imgView.layer.cornerRadius = imgView.frame.size.width / 2.0;
        imgView.layer.masksToBounds = YES;
        imgView.image = [UIImage imageNamed:@"profile_menu.png"];

        UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 46, -2, 20, 20)];


        if ([dict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *profileDict = dict[kKeyProfileImage];

            NSString *imageName = profileDict[kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                [imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                imgView.image = [UIImage imageNamed:@"profile_menu.png"];

        } else
            imgView.image = [UIImage imageNamed:@"profile_menu.png"];


        [btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        btnDelete.tag = i;
        [btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];

        [subView addSubview:imgView];
        [subView addSubview:btnDelete];

        // Name
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, 67, 70, 18)];
        lblName.textAlignment = NSTextAlignmentCenter;
        lblName.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
        lblName.textColor = [UIColor whiteColor];
        NSString *lastName = [dict[kKeyLastName] substringToIndex:1];
        lblName.text = [NSString stringWithFormat:@"%@ %@.", dict[kKeyFirstName], lastName];
        [subView addSubview:lblName];

        // Button made admin
        UIButton *btnAddAdmin = [[UIButton alloc] initWithFrame:CGRectMake(0, 67 + 15, 70, 30)];
        if ([adminArray containsObject:[dict valueForKey:kKeyId]]) {
            // Already admin of group
            [btnAddAdmin setTitle:@"Admin" forState:UIControlStateNormal];
            [btnAddAdmin setTitleColor:[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];
            [adminArr addObject:dict];
        } else {
            [btnAddAdmin setTitle:@"Make Admin" forState:UIControlStateNormal];
            [btnAddAdmin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        [btnAddAdmin addTarget:self action:@selector(actionOnMakeAdmin:) forControlEvents:UIControlEventTouchUpInside];
        btnAddAdmin.titleLabel.textAlignment = NSTextAlignmentCenter;
        btnAddAdmin.titleLabel.font = [UIFont fontWithName:kFontHelveticaBold size:10.0];
        btnAddAdmin.tag = i;
        [subView addSubview:btnAddAdmin];

        // Add to the scroll view
        [self.scrollInvitePeople addSubview:subView];
        if (!canEdit) {
            subView.userInteractionEnabled = NO;
        }
    }
    // Check if lastest frame is not empty
    // Add to scroll view check if user cam add group members
    if ([dataDict[kKeyCreatedBy] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]] || canEdit || [dataDict count] == 0) {
        if (CGRectEqualToRect(latestFrame, CGRectZero)) {
            latestFrame = CGRectMake(5, 0, 70, self.scrollInvitePeople.frame.size.height);
        } else
            latestFrame = CGRectMake(latestFrame.origin.x + 70, 0, 70, self.scrollInvitePeople.frame.size.height);

        UIView *btnSuperView = [[UIView alloc] init];
        btnSuperView.frame = latestFrame;


        UIButton *btnAdd = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        [btnAdd addTarget:self action:@selector(btnInvitePeopleAction:) forControlEvents:UIControlEventTouchUpInside];
        [btnSuperView addSubview:btnAdd];

        [btnAdd.layer addSublayer:[self addDashedBorderWithColor:1]];
        [btnAdd setImage:[UIImage imageNamed:@"miles-set-plus.png"] forState:UIControlStateNormal];
        [self.scrollInvitePeople addSubview:btnSuperView];
    }

    if ((latestFrame.origin.x + 30) > SCREEN_WIDTH) {
        self.scrollInvitePeople.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
        self.scrollInvitePeople.scrollEnabled = YES;
        self.scrollInvitePeople.backgroundColor = [UIColor clearColor];
    }
}


#pragma mark
#pragma mark Country Picker Delegates Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// pickCountry:
- (void)pickCountry:(NSDictionary *)countryDict {
    if ([countryDict count] != 0) {
        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [countryDict valueForKey:kKeyPhoneCode]];
        }
        [self.txtCountryName setText:[countryDict valueForKey:kkeyCountryName]];

        // Set search icon to textfield
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(40, 0, 45, 30)];
        container.backgroundColor = [UIColor clearColor];

//        self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 45, 30)];
//        self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
//        self.lblPhoneCode.textColor = [UIColor blackColor];

        if (countryDict && [countryDict valueForKey:kKeyPhoneCode]) {
            [self.lblPhoneCode setText:[@"+" stringByAppendingString:[countryDict valueForKey:kKeyPhoneCode]]];

        }

//        [container addSubview:self.lblPhoneCode];

//        self.txtPhoneNumber.leftView = container;
//        self.txtPhoneNumber.leftViewMode = UITextFieldViewModeAlways;
    }
}

-(void) prefillCountryCode{
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale.
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSArray *countryArray = server.countryArr;

    for (NSDictionary *dict in countryArray) {
        if ([[dict valueForKey:@"iso"] isEqualToString:countryCode]) {
            countryMasterId = [NSString stringWithFormat:@"%@", [dict valueForKey:kKeyId]];

            [self.txtCountryName setText:[dict valueForKey:kkeyCountryName]];

            UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 74, 30)];

            container.backgroundColor = [UIColor clearColor];

            self.lblPhoneCode = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, 74, 30)];
            self.lblPhoneCode.font = [UIFont fontWithName:kFontHelveticaMedium size:14.0];
            self.lblPhoneCode.textAlignment = NSTextAlignmentCenter;
            self.lblPhoneCode.textColor = [UIColor blackColor];

            if (dict && [dict valueForKey:kKeyPhoneCode]) {
                [self.lblPhoneCode setText:[@"+" stringByAppendingString:[NSString stringWithFormat:@"%@", [dict valueForKey:kKeyPhoneCode]]]];
            }

            [container addSubview:self.lblPhoneCode];
            self.txtPhoneNumber.textAlignment = NSTextAlignmentLeft;

            self.txtPhoneNumber.leftView = container;
            self.txtPhoneNumber.leftViewMode = UITextFieldViewModeAlways;

        }
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark
// ----------------------------------------------------------------------------------------------
// actionOnCountryBtn

- (IBAction)actionOnCountryBtn {
    SRCountryPickerViewController *countryPicker = [[SRCountryPickerViewController alloc] initWithNibName:@"SRCountryPickerViewController" bundle:nil server:server];
    countryPicker.delegate = self;
    [self.navigationController pushViewController:countryPicker animated:YES];
}

// ----------------------------------------------------------------------------------------------------------
// actionOnMakeAdmin:

- (void)actionOnMakeAdmin:(UIButton *)sender {
    NSInteger tag = sender.tag;
    NSDictionary *userDict = membersArr[tag];

    if ([[sender titleForState:UIControlStateNormal] isEqualToString:@"Admin"]) {
        [sender setTitle:@"Make Admin" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        // Remove from admin array
        [adminArr removeObject:userDict];
    } else {
        [sender setTitle:@"Admin" forState:UIControlStateNormal];
        [sender setTitleColor:[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0.0 alpha:1.0] forState:UIControlStateNormal];

        // Add to admin array
        [adminArr addObject:userDict];
    }

}

// ----------------------------------------------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnButtonClick:(id)sender {

    // End editing
    [self.view endEditing:YES];

    self.pickerView.hidden = NO;
    //Action on Time
    if (sender == self.btnTime) {
        [self.datePicker setDatePickerMode:UIDatePickerModeTime];
        isDate = NO;
        if (isDate) {
            [self.datePicker removeTarget:self action:@selector(updateTimeInTextField:) forControlEvents:UIControlEventValueChanged];
            [self.datePicker addTarget:self action:@selector(updateDateInTextField:) forControlEvents:UIControlEventValueChanged];
        } else {
            [self.datePicker removeTarget:self action:@selector(updateDateInTextField:) forControlEvents:UIControlEventValueChanged];

            [self.datePicker addTarget:self action:@selector(updateTimeInTextField:) forControlEvents:UIControlEventValueChanged];
        }

        if ([[self.txtTime text] length] == 0) {
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"HH:mm a"];
//            
//            self.txtTime.text = [formatter stringFromDate:[NSDate date]];
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
//            [dateFormatter setTimeZone:utcTimeZone];
//            [dateFormatter setDateFormat:@"HH:mm a"];
//            dobDate = [dateFormatter stringFromDate:[NSDate date]];
        } else {
            NSString *txtFldDateStr = self.txtTime.text;
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
            [formatter setTimeZone:utcTimeZone];
            [formatter setDateFormat:@"HH:mm"];
            NSDate *date = [formatter dateFromString:txtFldDateStr];
            [self.datePicker setDate:date];
        }
    }
        // Action on date
    else if (sender == self.btnDate) {

        isDate = YES;
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
        if (isDate) {
            [self.datePicker removeTarget:self action:@selector(updateTimeInTextField:) forControlEvents:UIControlEventValueChanged];
            [self.datePicker addTarget:self action:@selector(updateDateInTextField:) forControlEvents:UIControlEventValueChanged];
        } else {
            [self.datePicker removeTarget:self action:@selector(updateDateInTextField:) forControlEvents:UIControlEventValueChanged];
            [self.datePicker addTarget:self action:@selector(updateTimeInTextField:) forControlEvents:UIControlEventValueChanged];
        }

        if ([[self.txtDate text] length] == 0) {

//            self.txtDate.text = [SRModalClass returnStringInMMDDYYYY:[NSDate date]];
//            dobDate = [SRModalClass returnStringInMMDDYYYY:[NSDate date]];
        } else {
            NSString *txtFldDateStr = self.txtDate.text;
            NSDate *date = [SRModalClass returnDateInMMDDYYYY:txtFldDateStr];
            [self.datePicker setDate:date];
        }
    } else if (sender == self.doneBarBtn) {
        self.pickerView.hidden = YES;
        [self.view endEditing:YES];
        [self.scrollviewEventDetails setContentOffset:CGPointZero animated:YES];
        if (isDate) {
            [self updateDateInTextField:self.datePicker];
        } else {
            [self updateTimeInTextField:self.datePicker];
        }
    } else if (sender == self.cancelBarBtn) {
        [self.scrollviewEventDetails setContentOffset:CGPointZero animated:YES];
        self.pickerView.hidden = YES;

        if (isDate) {
            self.txtDate.text = self.txtDate.text.length > 0 ? self.txtDate.text : @"";
        } else {
            self.txtTime.text = self.txtTime.text.length > 0 ? self.txtTime.text : @"";
        }

    } else if ([(UIButton *) sender tag] == kRightSaveBtnTag) {
        self.pickerView.hidden = YES;
        [self.view endEditing:YES];

        NSString *errMsg = nil;
        if ([self.txtEventName.text length] == 0) {
            errMsg = NSLocalizedString(@"empty.eventname.text", @"");
        }
        if ([self.txtSetLocation.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.eventlocation.text", @"");
        }
        if ([self.txtEventType.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.eventtype.text", @"");
        }
        if ([self.txtDate.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.eventdate.text", @"");
        }
        if ([self.txtTime.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.eventtime.text", @"");
        }
        if ([self.txtviewEventDesc.text length] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.eventdesc.text", @"");
        }

        // TODO: Event member notify
        if ([membersArr count] == 0 && errMsg == nil) {
            errMsg = NSLocalizedString(@"empty.eventlmember.text", @"");
        }
        if (errMsg) {
            [SRModalClass showAlert:errMsg];
        } else {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            params[kKeyName] = self.txtEventName.text;
            if ([self.txtviewEventDesc.text length] > 0)
                params[kKeyDescription] = self.txtviewEventDesc.text;
            params[kKeyAddress] = self.txtSetLocation.text;

            if (locationCoordinate.latitude < 1 && locationCoordinate.longitude <= 1) {
                
//                [self getHSLocationFromAddressString:self.txtSetLocation.text CompletionHandler:^(CLLocationCoordinate2D location) {
//                    NSLog(@"%f",location.latitude);
//                }];
                
                locationCoordinate = [self getLocationFromAddressString:self.txtSetLocation.text];
                params[kKeyLattitude] = [NSString stringWithFormat:@"%f", locationCoordinate.latitude];
                params[kKeyRadarLong] = [NSString stringWithFormat:@"%f", locationCoordinate.longitude];
            } else {
                params[kKeyLattitude] = [NSString stringWithFormat:@"%f", locationCoordinate.latitude];
                params[kKeyRadarLong] = [NSString stringWithFormat:@"%f", locationCoordinate.longitude];
            }

            if ([self.txtEventType.text length] > 0)
                params[kKeyEvent_Type] = self.txtEventType.text;

            if ([self.txtDate.text length] > 0)
                params[kKeyEvent_Date] = self.txtDate.text;

            if ([self.txtTime.text length] > 0)
                params[kKeyEvent_Time] = self.txtTime.text;

            if ([self.publicRadioBtn isSelected]) {
                params[kKeyIs_Public] = @"1";
            } else {
                params[kKeyIs_Public] = @"0";
            }

            if ([self.onRadioBtn isSelected]) {
                params[kKeyEvent_Status] = @"1";
            } else {
                params[kKeyEvent_Status] = @"0";
            }

            if (imgData == nil) {
                ///imgData = UIImagePNGRepresentation(self.imgAddPic.image);
                imgData = UIImageJPEGRepresentation(self.imgAddPic.image, 1.0);
            }
            if (isBtnAddFromGroupClicked) {
                (APP_DELEGATE).selectedGroupId = [(APP_DELEGATE).selectedGroupId substringToIndex:[(APP_DELEGATE).selectedGroupId length] - 1];
                params[kKeySelectedGroupId] = (APP_DELEGATE).selectedGroupId;
                isBtnAddFromGroupClicked = false;
                (APP_DELEGATE).selectedGroupId = @"";
            }
            // Add Event Member Records
            NSArray *sortedArr = ([membersArr valueForKeyPath:@"@distinctUnionOfObjects.id"]);
            NSString *joinedMembersID = [sortedArr componentsJoinedByString:@","];
            params[kKeyAttendees] = joinedMembersID;

            // Add Event admin Record
            NSArray *sortedAdminArr = ([adminArr valueForKeyPath:@"@distinctUnionOfObjects.id"]);
            NSString *joinedAdminUserId = [sortedAdminArr componentsJoinedByString:@","];
            params[kKeyEvent_Admin_Ids] = joinedAdminUserId;

            // Check for New Groups or Existing Group
            if ([dataDict count] > 0) {
                //For Update Existing Group
                params[@"_method"] = kPUT;
                NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassCreateEvent, dataDict[kKeyId]];
                [server asychronousRequestWithData:params imageData:imgData forKey:kKeyMediaImage toClassType:urlStr inMethodType:kPOST];
            } else {
                [server asychronousRequestWithData:params imageData:imgData forKey:kKeyMediaImage toClassType:kKeyClassCreateEvent inMethodType:kPOST];
            }
        }
    } else {
        self.pickerView.hidden = YES;
        [self.view endEditing:YES];
        [self.navigationController popToViewController:self.delegate animated:YES];
    }
}
// -----------------------------------------------------------------------------------------------------
// updateDateInTextField

- (void)updateDateInTextField:(UIDatePicker *)picker {
    NSString *formattedDate = [SRModalClass returnStringInMMDDYYYY:picker.date];
    self.txtDate.text = formattedDate;
}

// -----------------------------------------------------------------------------------------------------
// updateTimeInTextField

- (void)updateTimeInTextField:(UIDatePicker *)picker {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *formattedDate = [dateFormatter stringFromDate:picker.date];
    self.txtTime.text = formattedDate;
}

// ----------------------------------------------------------------------------------------------------------
// btnAddPicAction:

- (IBAction)btnAddPicAction:(id)sender {

    [self.view endEditing:YES];

    UIActionSheet *actionSheet = nil;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // Present action sheet
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self cancelButtonTitle:NSLocalizedString (@"cancel.button.title.text", "") destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), NSLocalizedString(@"camera.text", ""), nil];
    } else {
        // Present action sheet
        actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                  delegate:self cancelButtonTitle:NSLocalizedString (@"cancel.button.title.text", "") destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"set.from.photo.library.text", ""), nil];
    }

    // Show sheet
    [actionSheet showFromRect:[sender frame] inView:self.view animated:YES];


}

// ----------------------------------------------------------------------------------------------------------
// btnLocationAction:

- (IBAction)btnLocationAction {
    // Location from map
    SRLocationSetViewController *objLocationView = [[SRLocationSetViewController alloc] initWithNibName:nil bundle:nil server:server];
    objLocationView.delegate = self;
    objLocationView.getLocation = YES;
    objLocationView.address = self.txtSetLocation.text;
    [self.navigationController pushViewController:objLocationView animated:YES];
}



// ----------------------------------------------------------------------------------------------------------
// btnInvitePeopleAction:

- (IBAction)btnInvitePeopleAction:(id)sender {
    [self prefillCountryCode];
    self.viewAddPic.hidden = NO;
    [self.viewAddPic setCenter:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2)];
}

// ----------------------------------------------------------------------------------------------------------
// btnAddFromConnAction:
- (IBAction)btnAddFromConnAction:(id)sender {
    SRMemberViewController *membersCon = [[SRMemberViewController alloc] initWithNibName:nil bundle:nil server:server addedMembers:membersArr];

    membersCon.delegate = self;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:membersCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;
    self.viewAddPic.hidden = YES;
}

// ----------------------------------------------------------------------------------------------------------
// btnAddFromGroupsAction:
- (IBAction)btnAddFromGroupsAction:(id)sender {
    isBtnAddFromGroupClicked = true;
    (APP_DELEGATE).selectedGroupId = [[NSString alloc] init];
    SRMemberViewController *membersCon = [[SRMemberViewController alloc] initWithNibName:@"AddFromGroups" bundle:nil server:server addedMembers:nil];

    membersCon.delegate = self;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:membersCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;
    self.viewAddPic.hidden = YES;
}


// ----------------------------------------------------------------------------------------------------------
// btnAddNewContactAction:
- (IBAction)btnAddNewContactAction:(id)sender {
    if (self.txtPhoneNumber.text.length == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
    } else {
        // Call login api
        NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:self.txtPhoneNumber.text];
        if (mobile_Number.length == 10) {

            [self.txtPhoneNumber resignFirstResponder];
            mobile_Number = [countryMasterId stringByAppendingString:mobile_Number];
            // Call server api to get verify code for password
            [APP_DELEGATE showActivityIndicator];
            // Call api
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            params[kKeyMobileNumber] = mobile_Number;
            params[kKeyMessage] = [NSString stringWithFormat:@"Hi,%@ your are invited in %@ event via Serendipity app. To see download Serendipity app from:http://serendipity.app", self.txtName.text, self.txtEventName.text];

            NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassSendSMS];
            [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
        } else {
            [SRModalClass showAlert:NSLocalizedString(@"empty.phoneNo.text", @"")];
        }
    }
}

// ----------------------------------------------------------------------------------------------------------
// actionOnRadioButton (Group type):

- (IBAction)actionOnGroupTypeBtn:(UIButton *)inSender {
    if (inSender == self.publicRadioBtn) {
        [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.publicRadioBtn setSelected:TRUE];
        [self.privteRadioBtn setSelected:FALSE];
    } else {
        [self.privteRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.publicRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.publicRadioBtn setSelected:FALSE];
        [self.privteRadioBtn setSelected:TRUE];
    }
}
// ----------------------------------------------------------------------------------------------------------
// actionOnRadioButton (Group is):

- (IBAction)actionOnGroupIsBtn:(UIButton *)inSender {
    if (inSender == self.onRadioBtn) {
        [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.onRadioBtn setSelected:TRUE];
        [self.offRadioBtn setSelected:FALSE];
    } else {
        [self.offRadioBtn setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [self.onRadioBtn setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [self.offRadioBtn setSelected:TRUE];
        [self.onRadioBtn setSelected:FALSE];
    }
}

// ---------------------------------------------------------------------------------------
// btnCloseImageAction:

- (IBAction)btnCloseImageAction:(id)sender {

}

#pragma mark
#pragma mark - Message Composer delegate
#pragma mark

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark Location Controller Deelegate
#pragma mark

//// ---------------------------------------------------------------------------------------
//// dataFromSRLocationViewController
//
//- (void)dataFromSRLocationViewController:(NSString *)data {
//    self.txtSetLocation.text = data;
//}
// -----------------------------------------------------------------------------------------------------
// locationAddressWithLatLong:

- (void)locationAddressWithLatLong:(NSString *)data centerCoordinate:(CLLocationCoordinate2D)inCenter {
    locationCoordinate = inCenter;
    self.txtSetLocation.text = data;
}

#pragma mark
#pragma mark Textfield delegatss
#pragma mark

// ---------------------------------------------------------------------------------------
// textFieldShouldReturn:
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [textField resignFirstResponder];
    return YES;
}

// ---------------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.txtSetLocation) {
        [textField resignFirstResponder];
        [self btnLocationAction];
    }
    if (textField == self.txtName || textField == self.txtCountryName || textField == self.txtPhoneNumber) {
        CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 15);
        [self.scrollViewAddPic setContentOffset:scrollPoint animated:YES];
    } else {
        if (SCREEN_HEIGHT <= 480.0) {
            CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 15);
            [self.scrollviewEventDetails setContentOffset:scrollPoint animated:YES];

        } else {

            self.self.scrollviewEventDetails.contentOffset = CGPointMake(0, 0);
        }
    }
}

// ---------------------------------------------------------------------------------------
// textFieldDidEndEditing:

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollviewEventDetails setContentOffset:CGPointZero animated:YES];
    [self.scrollViewAddPic setContentOffset:CGPointZero animated:YES];
}

#pragma mark -
#pragma mark UITextViewDelegate methods

// ---------------------------------------------------------------------------------------
// shouldChangeTextInRange:

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {

    if (range.length == 0) {
        if ([text isEqualToString:@"\n"]) {
            [textView resignFirstResponder];
            return NO;
        }

    }
    return YES;
}
// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (![textField isEqual:self.txtPhoneNumber]) {
        return YES;
    }
    if ([textField isEqual:self.txtPhoneNumber]) {
        BOOL isValid = YES;
        NSString *errorMsg = nil;
        NSString *textStrToCheck = [NSString stringWithFormat:@"%@%@", textField.text, string];

        if ([textStrToCheck length] > 14) {
            errorMsg = NSLocalizedString(@"telephone.no.lenght.error", @"");
        } else if (errorMsg == nil) {
            BOOL flag = [SRModalClass numberValidation:string];

            textField.text = [SRModalClass formatPhoneNumber:self.txtPhoneNumber.text deleteLastChar:NO];

            if (!flag) {
                errorMsg = NSLocalizedString(@"invalid.char.alert.text", "");
            }
        }
        // Set flag and show alert
        if (errorMsg) {
            isValid = NO;
            [SRModalClass showAlert:errorMsg];
        }

        // Return
        return isValid;
    }
    return NO;
}
// ---------------------------------------------------------------------------------------
// textViewShouldBeginEditing:

- (void)textViewShouldBeginEditing:(UITextView *)textView; {
    self.pickerView.hidden = YES;

    if (SCREEN_HEIGHT <= 480.0) {
        CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y - 25);
        [self.scrollviewEventDetails setContentOffset:scrollPoint animated:YES];
    } else {
        CGPoint scrollPoint = CGPointMake(0, 75);
        [self.scrollviewEventDetails setContentOffset:scrollPoint animated:YES];
    }


}

// ---------------------------------------------------------------------------------------
// textViewDidEndEditing:

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.scrollviewEventDetails setContentOffset:CGPointZero animated:YES];
}

#pragma mark
#pragma mark Touch View Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

    self.viewAddPic.hidden = YES;
    self.scrollviewEventDetails.contentOffset = CGPointMake(0, 0);
    self.pickerView.hidden = YES;

    UITouch *touch = [[event allTouches] anyObject];

    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark
#pragma mark Tap Gesture
#pragma mark

// ---------------------------------------------------------------------------------------
// singleTapGestureCaptured:
- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture {
    self.scrollviewEventDetails.contentOffset = CGPointMake(0, 0);
    self.pickerView.hidden = YES;
    self.viewAddPic.hidden = YES;
    [self.view endEditing:YES];
}


#pragma mark
#pragma mark ActionSheet Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// clickedButtonAtIndex:

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Instantiate image picker and set delegate
    UIImagePickerController *imagePickerCon = [[UIImagePickerController alloc] init];
    imagePickerCon.delegate = self;
    imagePickerCon.allowsEditing = YES;
    [imagePickerCon setModalPresentationStyle:UIModalPresentationFullScreen];
    
    if (buttonIndex == 0) {
        imagePickerCon.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

        CATransition *transition = [CATransition animation];
        transition.duration = 1;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self presentViewController:imagePickerCon animated:NO completion:nil];
    } else if (buttonIndex == 1 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePickerCon.sourceType = UIImagePickerControllerSourceTypeCamera;

        CATransition *transition = [CATransition animation];
        transition.duration = 1;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];

        [self presentViewController:imagePickerCon animated:NO completion:nil];
    }
}

#pragma mark
#pragma mark Image Picker Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// didFinishPickingMediaWithInfo:

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // Selection of image done set it to button
    UIImage *selectedImage = [info valueForKey:UIImagePickerControllerEditedImage];
    imgData = UIImageJPEGRepresentation(selectedImage, 1.0);
    self.imgAddPic.image = selectedImage;
    self.imgBGScreen.image = selectedImage;



    // Dismiss picker
    [picker dismissViewControllerAnimated:NO completion:nil];
}

// --------------------------------------------------------------------------------
// imagePickerControllerDidCancel:

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark
#pragma mark Member Controller Delegate
#pragma mark

//-----------------------------------------------------------------------------------------------------
// addedMembersFromGroups:
- (void)addedMembersFromGroups:(NSMutableArray *)inArr {
    //Check if memeber Arr already contains member
    NSMutableArray *membersIdArr = [NSMutableArray array];
    for (int i = 0; i < membersArr.count; i++) {
        [membersIdArr addObject:[membersArr[i] valueForKey:kKeyId]];
    }
    // Insert group member into member array
    for (int i = 0; i < inArr.count; i++) {
        NSMutableDictionary *userDetails;
        NSString *userId = [inArr[i] objectForKey:kKeyUserID];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
        NSArray *filterArr = [server.groupUsersArr filteredArrayUsingPredicate:predicate];
        if ([filterArr count] > 0) {
            userDetails = filterArr[0];
            [userDetails setValue:@"selected" forKey:@"selecetdObj"];
            [userDetails removeObjectForKey:kKeyDistance];
            if (![membersIdArr containsObject:[userDetails valueForKey:kKeyId]]) {
                [membersIdArr addObject:[userDetails valueForKey:kKeyId]];
                [membersArr addObject:userDetails];
            }
        }
    }

    //Remove myDict from membersArr(to avoid duplicates members)
    for (int i = 0; i < membersArr.count; i++) {
        NSDictionary *myDict = membersArr[i];
        if ([[myDict valueForKey:kKeyId] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
            [membersArr removeObjectAtIndex:i];
        }
    }

    // Add objects to scroll view
    [self addImagesInScrollView];
}

// -----------------------------------------------------------------------------------------------------
// addedMembersList:

- (void)addedMembersList:(NSArray *)inArr {
    // Remove old objects add new
    if (dataDict != nil) {
        NSMutableArray *filteredArr = [NSMutableArray array];
        for (NSDictionary *inDict in membersArr) {
            if ([inDict[kKeyDegree] isKindOfClass:[NSNumber class]] && [inDict[kKeyDegree] integerValue] != 1) {
                [filteredArr addObject:inDict];
            } else if ([inDict[kKeyDegree] isKindOfClass:[NSNull class]]) {
                [filteredArr addObject:inDict];
            }
        }

        //[membersArr removeAllObjects];
        for (int i = 0; i < inArr.count; i++) {
            NSMutableDictionary *dict = inArr[i];
            [dict removeObjectForKey:kKeyIsInvisible];
            [dict removeObjectForKey:kKeyIsFavourite];
            [dict removeObjectForKey:kKeyDistance];
            [dict setValue:@"1" forKey:kKeyDegree];

            if (![membersArr containsObject:dict]) {
                [membersArr addObject:dict];
            }
        }
        for (int i = 0; i < filteredArr.count; i++) {
            if (![membersArr containsObject:filteredArr[i]]) {
                [membersArr addObjectsFromArray:filteredArr];
            }
        }

    } else {
        //[membersArr removeAllObjects];
        for (int i = 0; i < inArr.count; i++) {
            if (![membersArr containsObject:inArr[i]]) {
                [membersArr addObjectsFromArray:inArr];
            }
        }

    }

    // Update scroll view
    for (UIView *subview in[self.scrollInvitePeople subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }

    // Add objects to scroll view
    [self addImagesInScrollView];
}


//code for add images in scrollview programmatically

#pragma mark
#pragma mark Scroll Customization Method
#pragma mark

// --------------------------------------------------------------------------------
// addDashedBorderWithColor:

- (CAShapeLayer *)addDashedBorderWithColor:(CGFloat)lineNo {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    CGSize frameSize = CGSizeMake(65, 65);
    CGRect shapeRect = CGRectMake(0.0f, 0.0f, frameSize.width, frameSize.height);
    [shapeLayer setBounds:shapeRect];
    [shapeLayer setPosition:CGPointMake(frameSize.width / 2, frameSize.height / 2)];

    [shapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [shapeLayer setStrokeColor:[[UIColor whiteColor] CGColor]];
    [shapeLayer setLineWidth:lineNo];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineDashPattern:
            @[@10,
                    @2]];

    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:shapeRect cornerRadius:frameSize.width / 2];
    [shapeLayer setPath:path.CGPath];

    // Return
    return shapeLayer;
}

// --------------------------------------------------------------------------------
// addItemsToScrollView:

- (void)addItemsToScrollView:(BOOL)isProfile {
    // Remove all objects from scroll view
    NSArray *imgsArr;
    imgsArr = addPeopleImgArr;

    CGRect latestFrame = CGRectZero;

    // Add images to scroll view accordingly
    for (NSUInteger i = 0; i < [imgsArr count]; i++) {
        UIView *subView = [[UIView alloc] init];
        if (i == 0) {
            subView.frame = CGRectMake(5, 0, 65, 70);
        } else
            subView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 4, 0, 65, 70);
        latestFrame = subView.frame;
        subView.backgroundColor = [UIColor clearColor];

        // Add image view
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
        imgView.contentMode = UIViewContentModeScaleToFill;
        UIButton *btnDelete = [[UIButton alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 46, -2, 20, 20)];

        imgView.image = addPeopleImgArr[i];
        imgView.layer.cornerRadius = imgView.frame.size.width / 2;
        imgView.clipsToBounds = YES;

        CAShapeLayer *layer = [self addDashedBorderWithColor:2];
        [imgView.layer addSublayer:layer];

        UILabel *lblImageName = [[UILabel alloc] initWithFrame:CGRectMake(imgView.frame.origin.x + 20, 64, 60, 15)];
        lblImageName.textColor = [UIColor whiteColor];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaRegular size:11.0];
        lblImageName.text = [NSString stringWithFormat:@"image%ld", (unsigned long) i];
        [subView addSubview:lblImageName];
        [btnDelete setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
        btnDelete.tag = kInterestAddButtonTag + i;

        [btnDelete addTarget:self action:@selector(actionOnBtnDelete:) forControlEvents:UIControlEventTouchUpInside];

        [subView addSubview:imgView];
        [subView addSubview:btnDelete];

        // Add to the scroll view according to flag
        [self.scrollInvitePeople addSubview:subView];
    }

    // Check if lastest frame is not empty
    if (CGRectEqualToRect(latestFrame, CGRectZero)) {
        latestFrame = CGRectMake(5, 0, 65, 65);
    } else
        latestFrame = CGRectMake(latestFrame.origin.x + 70, 0, 65, 65);

    UIView *btnSuperView = [[UIView alloc] init];
    btnSuperView.frame = latestFrame;

    // Add to scroll view
    //UIButton *btnAdd = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 65, 65)];
    self.btnInvitePeople.frame = latestFrame;
    self.btnInvitePeople.tag = kInterestAddButtonTag;


    if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {

        self.scrollInvitePeople.contentSize = CGSizeMake(latestFrame.origin.x + 70, 0);
        self.scrollInvitePeople.scrollEnabled = YES;
    }
}

// ----------------------------------------------------------------------------------------------------------
// actionOnBtnDelete:

- (void)actionOnBtnDelete:(UIButton *)inSender {
    NSInteger tag = inSender.tag;

    // Remove the member
    NSDictionary *dict = membersArr[tag];
    NSString *idStr = [dict valueForKey:kKeyId];
    NSMutableArray *eventMemberIdArray = membersArr;
    for (NSDictionary *dict in eventMemberIdArray) {
        if ([idStr isEqualToString:[dict valueForKey:kKeyUserID]]) {
            idStr = [dict valueForKey:kKeyId];
        }
    }

    // Remove object at index
    [membersArr removeObjectAtIndex:tag];

    // Update scroll view
    for (UIView *subview in[self.scrollInvitePeople subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    // Add objects to scroll view
    [self addImagesInScrollView];
}

// -----------------------------------------------------------------------------------------------
// actionOnBtnAdd:

- (void)actionOnBtnAdd:(UIButton *)inSender {

    for (UIView *subview in[self.scrollInvitePeople subviews]) {
        if ([subview isKindOfClass:[UIView class]]) {
            [subview removeFromSuperview];
        }
    }
    // Add object at index
    NSInteger i = inSender.tag / kInterestAddButtonTag;
    i++;
    NSString *string = [NSString stringWithFormat:@"%ld", (long) i];
    [addPeopleImgArr addObject:string];
    [self addItemsToScrollView:NO];

}


#pragma mark
#pragma mark Location Controller Deelegate
#pragma mark

// -----------------------------------------------------------------------------------------------------
// dataFromSRLocationViewController

- (void)dataFromSRConnectionViewController:(NSMutableArray *)data {
    addPeopleImgArr = data;
    [self addItemsToScrollView:YES];
}


#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// CreateEventSucceed:

- (void)createEventSucceed:(NSNotification *)inNotify {

    NSArray *result = [inNotify object];


    // Event created successfully
    if (dataDict.count > 0) {
        (APP_DELEGATE).server.eventDetailInfo = [inNotify object];
    }

    //Add event to calender
    [SRModalClass saveEventToCalender:(APP_DELEGATE).server.eventDetailInfo];

    // Create event group for xmpp chat
    NSDictionary *groupDict = [inNotify object];

    NSMutableArray *groupList = [NSMutableArray array];
    //Member array == [groupDict objectForKey:kKeyEvent_Attendees]

    for (NSDictionary *userdict in membersArr) {
        // NSDictionary *userDetails = [userdict objectForKey:kKeyUser];

        NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionary];
        [mutatedDict setValue:[NSString stringWithFormat:@"%@", userdict[kKeyId]] forKey:kKeyId];
        [mutatedDict setValue:[NSString stringWithFormat:@"%@", userdict[kKeyFirstName]] forKey:kKeyFirstName];
        [mutatedDict setValue:[NSString stringWithFormat:@"%@", userdict[kKeyLastName]] forKey:kKeyLastName];
        if ([[userdict valueForKey:kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSString *imagename = [[userdict valueForKey:kKeyProfileImage] valueForKey:kKeyImageName];

            if (imagename.length > 0) {
                [mutatedDict setValue:imagename forKey:kKeyProfileImage];
            } else {
                [mutatedDict setValue:@"" forKey:kKeyProfileImage];
            }

        } else {
            [mutatedDict setValue:@"" forKey:kKeyProfileImage];
        }

        [groupList addObject:mutatedDict];
    }
    if (result) {
        [self.navigationController popViewControllerAnimated:YES];
    }

    // Post notification for call new events from server
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewEventAdded object:nil];

}
// --------------------------------------------------------------------------------
// CreateEventFailed:

- (void)createEventFailed:(NSNotification *)inNotify {
    [SRModalClass showAlert:[NSString stringWithFormat:@"%@", [inNotify object]]];
}
// --------------------------------------------------------------------------------
// inviteUserSucceed:

- (void)inviteUserSucceed:(NSNotification *)inNotify {
    [self.viewAddPic setHidden:YES];
}
// --------------------------------------------------------------------------------
// CreateEventFailed:

- (void)inviteUserFailed:(NSNotification *)inNotify {
    [self.viewAddPic setHidden:YES];
}


@end
