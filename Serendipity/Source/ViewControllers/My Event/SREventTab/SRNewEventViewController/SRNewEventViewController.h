#define kInterestAddButtonTag 800
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SRCountryPickerViewController.h"

@interface SRNewEventViewController : ParentViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIActionSheetDelegate,MFMessageComposeViewControllerDelegate,CountryPickerDelegate>
{
        NSMutableArray *addPeopleImgArr;
    
        NSString *dobDate,*countryMasterId;
        BOOL isDate;
        BOOL canEdit, isBtnAddFromGroupClicked;
    
        //Server
        SRServerConnection *server;
        NSMutableArray *adminArr;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgBGScreen;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddPicBGColor;
@property (weak, nonatomic) IBOutlet UIImageView *imgEventDetailsBGColor;
@property (weak, nonatomic) IBOutlet UIView *viewAddPicBG;
@property (weak, nonatomic) IBOutlet UILabel *lblAddPic;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddPic;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewEventDetails;
@property (weak, nonatomic) IBOutlet UITextField *txtEventName;
@property (weak, nonatomic) IBOutlet UITextField *txtEventType;
@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UITextField *txtTime;
@property (strong, nonatomic) IBOutlet UIButton *btnTime;
@property (weak, nonatomic) IBOutlet UITextField *txtSetLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtviewEventDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UIView *viewEventDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblEventType;
@property (weak, nonatomic) IBOutlet UILabel *lblPublic;
//@property (weak, nonatomic) IBOutlet UISwitch *switchEventType;
@property (weak, nonatomic) IBOutlet UIButton *publicRadioBtn,*privteRadioBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivate;
@property (weak, nonatomic) IBOutlet UILabel *lblEvent_is;
@property (weak, nonatomic) IBOutlet UILabel *lblOff;
//@property (weak, nonatomic) IBOutlet UISwitch *switchEvent_is;

@property (weak, nonatomic) IBOutlet UIButton *onRadioBtn,*offRadioBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblOn;
@property (weak, nonatomic) IBOutlet UILabel *lblInvitePeople;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollInvitePeople;
@property (weak, nonatomic) IBOutlet UIButton *btnInvitePeople;

@property (strong, nonatomic) IBOutlet UIView *viewAddPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAddPicBG;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewAddPic;
@property (weak, nonatomic) IBOutlet UILabel *lblAddMember;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFromConn,*btnAddFromGroups;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryName;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewContact;
@property (weak, nonatomic) IBOutlet UIButton *countryPicBtn;
@property (strong, nonatomic)UILabel *lblPhoneCode;

// Date Picker View
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneBarBtn;

//Other Properties
@property (strong, nonatomic)NSMutableDictionary *dataDict;

- (IBAction)btnAddPicAction:(id)sender;
- (IBAction)btnLocationAction;
//- (IBAction)switchEventTypeAction:(id)sender;
//- (IBAction)switchEventIsAction:(id)sender;
//- (IBAction)btnCloseImageAction:(id)sender;
- (IBAction)btnInvitePeopleAction:(id)sender;

- (IBAction)btnAddFromConnAction:(id)sender;
- (IBAction)btnAddNewContactAction:(id)sender;

#pragma mark
#pragma mark Init Method
#pragma mark


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
                 dict:(NSDictionary *)eventDict
               server:(SRServerConnection *)inServer;

@property(weak)id delegate;

@end
