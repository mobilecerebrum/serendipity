
#import "SRModalClass.h"
#import "SREventsViewController.h"
#import "SRDiscoveryMapViewController.h"
#import "SREventRadarViewController.h"
#import "SRNewEventViewController.h"
#import "SREventMapViewController.h"

#import "SREventTabViewController.h"

@implementation SREventTabViewController

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
            className:(NSString *)inClassName {
    // Call super
    self = [super initWithNibName:nibNameOrNil bundle:nil];

    if (self) {
        // Initialization
        server = inServer;
        subClass = inClassName;
    }
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(FetchNotification:)
                          name:kKeyNotificationFetchNotification object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(hideNotificationView:)
                          name:kkeynotificationHideNotificationView object:nil];
    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - Standard Overrides Method
#pragma mark

// ---------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];

    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;


    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];

    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f], NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f], NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];


    // Set EventMap tab
    SREventMapViewController *objEventMap = [[SREventMapViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-map.png"];
    UITabBarItem *objEventMapTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.map.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-map-active.png"]];
    [objEventMapTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objEventMap.tabBarItem = objEventMapTabItem;

    //  Set EventRadar tab
    SREventRadarViewController *objEventRadar = [[SREventRadarViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-radar.png"];
    UITabBarItem *objEventRadarTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.radar.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-radar-active.png"]];
    [objEventRadarTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objEventRadar.tabBarItem = objEventRadarTabItem;


    //Set EventList tab
    SREventsViewController *objEventList = [[SREventsViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-list.png"];
    UITabBarItem *objEventListTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.list.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-list-active.png"]];
    [objEventListTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objEventList.tabBarItem = objEventListTabItem;

    self.viewControllers = @[objEventMap, objEventRadar, objEventList];
    self.selectedIndex = 1;
    self.tabBar.unselectedItemTintColor = appTintColor;

    // Set navigation bar Appearance

    // Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.events.view", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
    [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];


    // Add view to the tabbar top
    notificationView = [[SRNotificationView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    notificationView.notifyViewFor = kKeyEvent;
    [self.view addSubview:notificationView];
    [self.view bringSubviewToFront:notificationView];

    notificationView.notificationArr = (APP_DELEGATE).server.notificationArr;

    __block NSArray *filterArr;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"4"];
        filterArr = [(APP_DELEGATE).server.notificationArr filteredArrayUsingPredicate:predicate];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Set Title to Notification Button
            if ([filterArr count] == 0) {
                notificationView.hidden = YES;
            } else {
                notificationView.hidden = NO;
                notificationView.notificationArr = filterArr;
                [notificationView.tblView reloadData];

                NSString *str = [NSString stringWithFormat:@"You have %ld new notifications", (unsigned long) [filterArr count]];
                [notificationView.btnNotify setTitle:str forState:UIControlStateNormal];
            }
        });
    });

}

// ---------------------------------------------------------------------------------------
// viewWillDisappear

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (notificationView.isOpen) {
        // Update notification count in menu view
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdatedNotificationCount object:nil userInfo:nil];
        });
    }
    // Dealloc all register notifications
//    [[NSNotificationCenter defaultCenter]removeObserver:self];
//    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
//    [defaultCenter removeObserver:self name:kKeyNotificationGetEventSucceed object:nil];
//    [defaultCenter removeObserver:self name:kKeyNotificationGetEventFailed object:nil];
//    
//    for (UIViewController *viewControllerObj in self.viewControllers) {
//        
//        [defaultCenter removeObserver:viewControllerObj];
//    }

}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    if (notificationView.isOpen) {
        // Update notifications status
        for (NSUInteger i = 0; i < [notificationView.notificationArr count]; i++) {
            NSDictionary *notifyDict = notificationView.notificationArr[i];
            NSDictionary *param = @{kKeyStatus: @1};
            NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
            [(APP_DELEGATE).server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
        }
    }
    if ((APP_DELEGATE).isFromNotification) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
        (APP_DELEGATE).isFromNotification = NO;
        (APP_DELEGATE).isNotificationFromClosed = FALSE;

    } else {
        [APP_DELEGATE hideActivityIndicator];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

// ---------------------------------------------------------------------------------------
// plusBtnAction:

- (void)plusBtnAction:(id)sender {

    SRNewEventViewController *objSRNewEventView = [[SRNewEventViewController alloc] initWithNibName:@"SRNewEventViewController" bundle:nil dict:nil server:(APP_DELEGATE).server];
    objSRNewEventView.delegate = self;
    [self.navigationController pushViewController:objSRNewEventView animated:NO];
}

// --------------------------------------------------------------------------------
// hideNotificationView:

- (void)hideNotificationView:(NSNotification *)inNotify {
    [notificationView setHidden:YES];
}

- (void)FetchNotification:(NSNotification *)inNotify {
    int index = 0;
    NSDictionary *notifyDict;
    NSMutableArray *arrNotifcationData = [[NSMutableArray alloc] init];
    arrNotifcationData = [notificationView.notificationArr mutableCopy];
    for (int i = 1; i <= [arrNotifcationData count]; i++) {
        if ([arrNotifcationData[i - 1] objectForKey:kKeyReference] != nil) {
            NSArray *groupData = [arrNotifcationData[i - 1] objectForKey:kKeyReference];
            if ([(APP_DELEGATE).selectedGroupId isEqualToString:[groupData valueForKey:kKeyId]]) {
                notifyDict = arrNotifcationData[i - 1];
                index = i;
                break;
            }
        }
    }
    if (index > 0) {
        [arrNotifcationData removeObjectAtIndex:index - 1];
    }
    if ([arrNotifcationData count] > 0) {
        notificationView.notificationArr = [arrNotifcationData mutableCopy];
        [notificationView.tblView reloadData];
        NSString *str = [NSString stringWithFormat:@"You have %ld new notifications", (unsigned long) [notificationView.notificationArr count]];
        [notificationView.btnNotify setTitle:str forState:UIControlStateNormal];
        NSDictionary *param = @{kKeyStatus: @1};
        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
        [(APP_DELEGATE).server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
    } else {
        notificationView.hidden = YES;
    }
}
@end
