
#import <UIKit/UIKit.h>
#import "SRNotificationView.h"

@interface SREventTabViewController : UITabBarController
{
    // Instance Variable
    SRNotificationView *notificationView;
    NSArray *notificationArr;
    
    //Other Properties
    NSString *subClass;
    
    //Server
    SRServerConnection *server;
    
}

// Other Properties

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
            className:(NSString *)inClassName;
@end
