
#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

#import "SRProfileImageView.h"
#import "NSDate+SRCustomDate.h"
#import "SREventViewCell.h"

@interface SREventsViewController : ParentViewController
{
       
    // Server
    SRServerConnection *server;
    NSInteger currentIndex;
    NSMutableDictionary *eventData;
    // Overlay
    UIImageView *overlayImgView;
    NSString *distanceUnit;
    NSMutableArray *eventList;
    NSDictionary *dictPram_CometChat;

}
@property (weak, nonatomic) IBOutlet SRProfileImageView *imgBGPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgBGColor;
@property (weak, nonatomic) IBOutlet UITableView *objTableView;
@property (weak, nonatomic) IBOutlet UITextField *txfSearchField;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) UILabel *footerLabel;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (weak) id delegate;
@property (nonatomic,strong) NSMutableArray *oldEventDataArray;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

-(Boolean)IsValidResponse:(NSData *)json;

@end
