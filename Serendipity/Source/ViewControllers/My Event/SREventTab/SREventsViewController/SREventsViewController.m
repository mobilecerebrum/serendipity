
#import "SRModalClass.h"
#import "SREventViewCell.h"
#import "SREventsViewController.h"
#import "SRNewEventViewController.h"
#import "SREventInfoViewController.h"
#import "SREventDetailTabViewController.h"
#import "SRMapViewController.h"
#import "CommonFunction.h"

@interface eventDistanceClass : NSObject

@property(nonatomic, strong) NSString *lat;
@property(nonatomic, strong) NSString *longitude;
@property(nonatomic, strong) NSString *eventAddress;
@property(nonatomic, strong) NSString *eventDistance;
@property(nonatomic, strong) NSString *dataId;

@end

@implementation eventDistanceClass


@end


@interface SREventsViewController () {
    NSArray *yourEventListArr, *popularEventListArr, *externalEventListArr, *youInvitedEventListArr, *eventGoingInAreaArr;
    NSMutableArray *sourceExternalEventArr, *sourceYourEventArr, *sourcePopularEventArr, *sourceYouInviteEventArr, *sourceEventGoingAreaArr;
    NSMutableArray *userList;
    NSArray *dotsEventPicsArr;
    float settingDistance;
    BOOL shouldStopLoadMoreExternalEvents;

}
@property(nonatomic, strong) NSOperationQueue *distanceQueue;
@end

@implementation SREventsViewController

- (Boolean)IsValidResponse:(NSData *)data {
    Boolean isOSMValid = false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json) {
        if ([keyStr isEqualToString:@"paths"]) {
            isOSMValid = true;
        }
    }
    if (isOSMValid || ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"])) {
        return true;
    } else {
        return false;
    }

}


- (void)callDistanceAPI:(NSDictionary *)userDict {
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict valueForKey:kKeyLattitude] floatValue] longitude:[[userDict valueForKey:kKeyRadarLong] floatValue]];


    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@", fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude, kKeyGoogleMap];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   double convertDist = 0.0;
                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"]) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = rows[0];
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];

                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       convertDist = [distanceWithoutCommas doubleValue];
                                       if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                           //meters to killometer
                                           convertDist = convertDist / 1000;
                                       } else {
                                           //meters to miles
                                           convertDist = (convertDist * 0.000621371192);
                                       }
                                       eventDistanceClass *obj = [[eventDistanceClass alloc] init];
                                       obj.dataId = userDict[kKeyId];
                                       NSDictionary *locationDict = userDict[kKeyLocation];
                                       obj.lat = locationDict[kKeyLattitude];
                                       obj.longitude = locationDict[kKeyRadarLong];
                                       obj.eventDistance = [NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit];
                                       //Live Address
                                       NSArray *destinationArray = json[@"destination_addresses"];
                                       obj.eventAddress = [NSString stringWithFormat:@"in %@", destinationArray[0]];

                                       NSInteger section = 0;
                                       NSMutableArray *tempArra1 = [[NSMutableArray alloc] init];

                                       NSMutableArray *eventAttendiesArray = userDict[kKeyEvent_Attendees];
                                       NSMutableArray *eventAttendiesIdsArray = [NSMutableArray array];
                                       for (int i = 0; i < [eventAttendiesArray count]; i++) {
                                           [eventAttendiesIdsArray addObject:[eventAttendiesArray[i] valueForKey:kKeyUserID]];
                                       }
                                       if ([dict[kKeyExternalEvent] isEqualToString:@"1"]) {
                                           [tempArra1 addObjectsFromArray:[externalEventListArr mutableCopy]];
                                           section = 4;
                                       } else if (![eventAttendiesIdsArray containsObject:[server.loggedInUserInfo valueForKey:kKeyId]]) {
                                           [tempArra1 addObjectsFromArray:[eventGoingInAreaArr mutableCopy]];
                                           section = 0;
                                       } else if ([dict[kKeyUserID] isEqualToString:server.loggedInUserInfo[kKeyId]] && [eventAttendiesArray count] < 25) {
                                           [tempArra1 addObjectsFromArray:[yourEventListArr mutableCopy]];
                                           section = 2;
                                       } else if ([eventAttendiesArray count] >= 25) {
                                           for (int i = 0; i < [eventAttendiesArray count]; i++) {
                                               if ([[eventAttendiesArray[i] valueForKey:kKeyInvitationStatus] isEqualToString:@"0"] && [[eventAttendiesArray[i] valueForKey:kKeyUserID] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                                                   if (![youInvitedEventListArr containsObject:dict]) {
                                                       [tempArra1 addObjectsFromArray:youInvitedEventListArr];
                                                       section = 1;
                                                   }
                                               } else if ([[eventAttendiesArray[i] valueForKey:kKeyInvitationStatus] isEqualToString:@"1"] && [[eventAttendiesArray[i] valueForKey:kKeyUserID] isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                                                   if (![popularEventListArr containsObject:dict]) {
                                                       [tempArra1 addObjectsFromArray:popularEventListArr];
                                                       section = 3;
                                                   }
                                               }
                                           }
                                       } else {
                                           if (![youInvitedEventListArr containsObject:dict]) {
                                               [tempArra1 addObjectsFromArray:youInvitedEventListArr];
                                               section = 1;
                                           }
                                       }

                                       NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                       NSArray *tempArr = [_oldEventDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                       obj.eventAddress = [NSString stringWithFormat:@"in %@", destinationArray[0]];

                                       if (tempArr && tempArr.count) {
                                           NSUInteger indexOfObject = [_oldEventDataArray indexOfObjectPassingTest:^BOOL(eventDistanceClass *anotherObj, NSUInteger idx, BOOL *_Nonnull stop) {
                                               if ([anotherObj.dataId isEqualToString:userDict[kKeyId]]) {
                                                   return YES;
                                               } else {
                                                   return NO;
                                               }
                                           }];
                                           _oldEventDataArray[indexOfObject] = obj;
                                       } else {
                                           [_oldEventDataArray addObject:obj];
                                       }


                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           NSUInteger row = [tempArra1 indexOfObjectPassingTest:^BOOL(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                                               if ([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]]) {
                                                   if (row != NSNotFound && row < tempArra1.count) {
                                                       NSIndexPath *rowToReload = [NSIndexPath indexPathForRow:row inSection:section];
                                                       NSArray *rowsToReload = @[rowToReload];
                                                       [_objTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
                                                   }
                                                   return YES;
                                               } else {
                                                   return NO;
                                               }
                                           }];

                                       });
                                   }
                               }
                           }];
}


- (void)getDistance {

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if (!((APP_DELEGATE).oldEventUserArray.count)) {
        [arr addObjectsFromArray:eventList];
    } else {
        for (NSDictionary *userDict in eventList) {

            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];

            NSArray *tempArr = [_oldEventDataArray filteredArrayUsingPredicate:predicateForDistanceObj];

            if (tempArr && tempArr.count) {

                eventDistanceClass *distanceObj = [tempArr firstObject];

                if ([distanceObj.lat isEqualToString:userDict[kKeyLattitude]] && [distanceObj.longitude isEqualToString:userDict[kKeyRadarLong]]) {
                    // Don't Do anything
                } else {
                    [arr addObject:userDict];
                }
            } else {
                [arr addObject:userDict];
            }
        }
    }


    for (int i = 0; i < arr.count; i++) {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];


        if (server.myLocation != nil && userDict[kKeyLattitude] != nil && [userDict valueForKey:kKeyRadarLong] != nil) {

            NSBlockOperation *op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled) {


                    [self callDistanceAPI:userDict];

                } else {

      //              NSLog(@"Operation Not canceled");
                }
            }];
            [_distanceQueue addOperation:op];
        }
    }
}

#pragma mark
#pragma mark Init Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SREventsViewController" bundle:nibBundleOrNil];
    if (self) {


        // Register Notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getEventSucceed:)
                              name:kKeyNotificationEventRadarChanges object:nil];
        [defaultCenter addObserver:self selector:@selector(EventConvertedSucceed:) name:KKeyNotificationEventConverted object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectEventSucceed:)
                              name:kKeyNotificationAcceptRejectEventSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(AcceptRejectEventFailed:)
                              name:kKeyNotificationAcceptRejectEventFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(RequestToJoinEventSucceed:)
                              name:kKeyNotificationRequestToJoinEventSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(RequestToJoinFailed:)
                              name:kKeyNotificationRequestToJoinEventFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(DeleteEventSucceed:)
                              name:kKeyNotificationDeleteEventSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(DeleteEventFailed:)
                              name:kKeyNotificationDeleteEventFailed object:nil];
        // Custom initialization
        server = inServer;
        userList = [[NSMutableArray alloc] init];
        sourceYourEventArr = [[NSMutableArray alloc] init];
        sourcePopularEventArr = [[NSMutableArray alloc] init];
        yourEventListArr = [[NSMutableArray alloc] init];
        popularEventListArr = [[NSMutableArray alloc] init];
        externalEventListArr = [[NSMutableArray alloc] init];

        sourceEventGoingAreaArr = [[NSMutableArray alloc] init];
        sourceYourEventArr = [[NSMutableArray alloc] init];
        sourcePopularEventArr = [[NSMutableArray alloc] init];
        sourceExternalEventArr = [[NSMutableArray alloc] init];
        sourceYouInviteEventArr = [[NSMutableArray alloc] init];
    }
    //return
    return self;
}
// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {

    //Call Super
    [super viewDidLoad];

    _oldEventDataArray = [[NSMutableArray alloc] init];
    _distanceQueue = [[NSOperationQueue alloc] init];


    // Overlay image view
    overlayImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.objTableView.frame.origin.y, self.view.frame.size.width, self.objTableView.frame.size.height)];
    overlayImgView.alpha = 0.4;
    overlayImgView.userInteractionEnabled = YES;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = overlayImgView.bounds;
        gradient.colors = @[(id) [[UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0] CGColor], (id) [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.8] CGColor]];
        [overlayImgView.layer insertSublayer:gradient atIndex:0];
    });


    // customize slider 1
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    NSString *strMilesData = [userdef valueForKey:kKeyMiles_Data];
    CGFloat distancevalue = 0.0;
    if (strMilesData == nil) {
        distancevalue = kKeyMaximumRadarMile;
    } else {
        NSArray *strArray = [strMilesData componentsSeparatedByString:@" "];
        distancevalue = [strArray[0] floatValue];
    }

    settingDistance = distancevalue;

    // Get profile image
    [SRModalClass setUserImage:server.loggedInUserInfo inSize:nil applyToView:self.imgBGPhoto];

    //Set Tableview ContentInset
    self.objTableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);


    //Text field search left view
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
    container.backgroundColor = [UIColor clearColor];

    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(25, 2, 26, 27)];
    searchIcon.image = [UIImage imageNamed:@"ic_search"];
    [container addSubview:searchIcon];

    self.txfSearchField.leftView = container;
    self.txfSearchField.leftViewMode = UITextFieldViewModeAlways;
    self.txfSearchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"eventList.view.txt.searchText.txt", "") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];

    [self getDistance];
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// -------------------------------------------------------------------------------------------------
// makeServerCallToGetEventsList

- (void)makeServerCallToGetEventListView {

    if (server.myLocation.coordinate.latitude != 0) {
        NSString *latitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.latitude];
        NSString *longitude = [NSString localizedStringWithFormat:@"%f", server.myLocation.coordinate.longitude];
        NSString *urlStr = [NSString stringWithFormat:@"%@distance=%@&lat=%@&lon=%@", kKeyClassEvent, [NSString stringWithFormat:@"%.fmi", settingDistance], latitude, longitude];
        [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];

    }

}

// --------------------------------------------------------------------------------
// searchEventsForSearchText:

- (void)searchEventForSearchText:(NSString *)searchText {
    if ([searchText length] > 0) {
        NSString *filter = @"%K CONTAINS[cd] %@";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:filter, kKeyName, searchText];
        youInvitedEventListArr = [sourceYouInviteEventArr filteredArrayUsingPredicate:predicate];
        yourEventListArr = [sourceYourEventArr filteredArrayUsingPredicate:predicate];
        popularEventListArr = [sourcePopularEventArr filteredArrayUsingPredicate:predicate];
        externalEventListArr = [sourceExternalEventArr filteredArrayUsingPredicate:predicate];
        eventGoingInAreaArr = [sourceEventGoingAreaArr filteredArrayUsingPredicate:predicate];
        [self.objTableView reloadData];
    } else {
        youInvitedEventListArr = sourceYouInviteEventArr;
        yourEventListArr = sourceYourEventArr;
        popularEventListArr = sourcePopularEventArr;
        externalEventListArr = sourceExternalEventArr;
        eventGoingInAreaArr = sourceEventGoingAreaArr;
        [self.objTableView reloadData];
    }
}

//---------------------------------------------------------------------------------------------------------
- (void)addEventToCalender:(UITapGestureRecognizer *)tapGes {
    if (tapGes.state == UIGestureRecognizerStateEnded) {
        SREventViewCell *clickedCell = (SREventViewCell *) [[[tapGes view] superview] superview];
        if (clickedCell) {
            NSIndexPath *indexPath = [self.objTableView indexPathForCell:clickedCell];
            if (indexPath.section == 0) {
                [SRModalClass showAlert:NSLocalizedString(@"alert.save_event_goingInarea.txt", @"")];
            } else if (indexPath.section == 1) {
                NSString *loggedInUser = server.loggedInUserInfo[kKeyId];
                NSArray *eventMemberArr = [youInvitedEventListArr[indexPath.row] objectForKey:kKeyEvent_Attendees];
                for (int i = 0; i < eventMemberArr.count; i++) {
                    if ([[eventMemberArr[i] valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
                        if ([[eventMemberArr[i] valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
                            [SRModalClass showAlert:NSLocalizedString(@"alert.save_event.txt", @"")];
                        } else {
                            [SRModalClass saveEventToCalender:youInvitedEventListArr[indexPath.row]];
                            clickedCell.lblareGoing.text = [NSString stringWithFormat:@"you are going +%lu...", (unsigned long) [eventMemberArr count]];

                            // Change Calender image to saved
                            clickedCell.imgGroups.backgroundColor = [UIColor blackColor];
                            clickedCell.imgGroups.contentMode = UIViewContentModeCenter;
                            [clickedCell.imgGroups setImage:[UIImage imageNamed:@"calendar-check_25.png"]];

                            // Update you are attending event
                            clickedCell.lblareGoing.frame = CGRectMake(clickedCell.lblareGoing.frame.origin.x, clickedCell.lblareGoing.frame.origin.y, clickedCell.lblareGoing.frame.size.width + 50, clickedCell.lblareGoing.frame.size.height);
                            clickedCell.lblareGoing.textColor = [UIColor orangeColor];
                            clickedCell.lblareGoing.layer.borderColor = [[UIColor clearColor] CGColor];

                        }
                    }
                }
            } else if (indexPath.section == 2) {
                [SRModalClass saveEventToCalender:yourEventListArr[indexPath.row]];
                clickedCell.lblareGoing.text = [NSString stringWithFormat:@"you are going +%lu...", [[yourEventListArr[indexPath.row] objectForKey:kKeyEvent_Attendees] count]];
            } else if (indexPath.section == 3) {
                [SRModalClass saveEventToCalender:popularEventListArr[indexPath.row]];
                clickedCell.lblareGoing.text = [NSString stringWithFormat:@"you are going +%lu...", [[popularEventListArr[indexPath.row] objectForKey:kKeyEvent_Attendees] count]];
            } else {
                [SRModalClass saveEventToCalender:externalEventListArr[indexPath.row]];
                clickedCell.lblareGoing.text = [NSString stringWithFormat:@"you are going +%lu...", [[externalEventListArr[indexPath.row] objectForKey:kKeyEvent_Attendees] count]];
            }

            if (indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4) {
                // Change Calender image to saved
                clickedCell.imgGroups.backgroundColor = [UIColor blackColor];
                clickedCell.imgGroups.contentMode = UIViewContentModeCenter;
                [clickedCell.imgGroups setImage:[UIImage imageNamed:@"calendar-check_25.png"]];

                // Update you are attending event
                clickedCell.lblareGoing.frame = CGRectMake(clickedCell.lblareGoing.frame.origin.x, clickedCell.lblareGoing.frame.origin.y, clickedCell.lblareGoing.frame.size.width + 50, clickedCell.lblareGoing.frame.size.height);
                clickedCell.lblareGoing.textColor = [UIColor orangeColor];
                clickedCell.lblareGoing.layer.borderColor = [[UIColor clearColor] CGColor];

            }
        }
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    (APP_DELEGATE).topBarView.hidden = YES;

    shouldStopLoadMoreExternalEvents = NO;

    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return eventGoingInAreaArr.count;
    } else if (section == 1) {
        return youInvitedEventListArr.count;
    } else if (section == 2) {
        return yourEventListArr.count;
    } else if (section == 3) {
        return popularEventListArr.count;
    } else
        return externalEventListArr.count;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    SREventViewCell *cell;

    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SREventViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            cell = (SREventViewCell *) nibObjects[0];
        }
    }
    //Reset Content
    cell.lblProfileName.text = @"";
    cell.lblMembers.text = @"";
    cell.lblAddress.text = @"";
    cell.lblDateTime.text = @"";
    cell.lblDay.text = @"";
    cell.lblDistanceLabel.text = @"";
    cell.lblareGoing.text = @"";

    // Adjust green dot label appearence
    cell.imgGroups.layer.cornerRadius = cell.imgGroups.frame.size.width / 2;
    cell.imgGroups.layer.masksToBounds = YES;
    cell.imgGroups.clipsToBounds = YES;

    NSDictionary *eventDetails;

    if (indexPath.section == 0) {
        eventDetails = eventGoingInAreaArr[indexPath.row];
    } else if (indexPath.section == 1) {
        eventDetails = youInvitedEventListArr[indexPath.row];
    } else if (indexPath.section == 2) {
        eventDetails = yourEventListArr[indexPath.row];
    } else if (indexPath.section == 3) {
        eventDetails = popularEventListArr[indexPath.row];
    } else if (indexPath.section == 4) {
        eventDetails = externalEventListArr[indexPath.row];
    }
    // For eventMember image using sd image cache
    id mediaDict = eventDetails[kKeyMediaImage];
    if ([mediaDict isKindOfClass:[NSDictionary class]] && [mediaDict objectForKey:kKeyImageName] != nil) {
        NSString *imageName = [mediaDict objectForKey:kKeyImageName];
        if ([imageName length] > 0) {

            NSString *imageUrl;
            if (indexPath.section == 4) {
                imageUrl = [NSString stringWithFormat:@"%@", imageName];
            } else
                imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

            [cell.imgProfilePhoto sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"events_avatar1.png"]];
        } else {
            cell.imgProfilePhoto.image = [UIImage imageNamed:@"events_avatar1.png"];
        }
    } else
        cell.imgProfilePhoto.image = [UIImage imageNamed:@"events_avatar1.png"];

    UITapGestureRecognizer *addEventTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addEventToCalender:)];
    addEventTapGes.numberOfTapsRequired = 1;
    cell.imgGroups.userInteractionEnabled = YES;

    [cell.imgGroups addGestureRecognizer:addEventTapGes];

    if ([SRModalClass isEventExist:eventDetails[kKeyId]]) {
        [cell.imgGroups removeGestureRecognizer:addEventTapGes];
        cell.imgGroups.backgroundColor = [UIColor blackColor];
        [cell.imgGroups setImage:[UIImage imageNamed:@"calendar-check_25.png"]];
    } else {
        cell.imgGroups.backgroundColor = [UIColor orangeColor];
        [cell.imgGroups setImage:[UIImage imageNamed:@"calendar-plus_25.png"]];
    }
    [cell.imgGroups setContentMode:UIViewContentModeCenter];

    cell.lblProfileName.text = [[[eventDetails valueForKey:kkeyNumbering] stringByAppendingString:@". "] stringByAppendingString:[NSString stringWithFormat:@"%@", [eventDetails valueForKey:kKeyName]]];

    cell.lblMembers.text = [NSString stringWithFormat:@"%@", [eventDetails valueForKey:kKeyEvent_Type]];

    if ([eventDetails valueForKey:kKeyAddress] != [NSNull null]) {
        cell.lblAddress.text = [NSString stringWithFormat:@"%@", [eventDetails valueForKey:kKeyAddress]];
    } else
        cell.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");

    NSString *dateString = [NSString stringWithFormat:@"%@ %@", [eventDetails valueForKey:kKeyEvent_Date], [eventDetails valueForKey:kKeyEvent_Time]];

    NSDate *eventDate = [NSDate getEventDateFromString:dateString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    [formatter setDateFormat:@"MMM dd"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSString *monthString = [formatter stringFromDate:eventDate];
    [formatter setDateFormat:@"HH:mm"];
    NSString *timeStr = [formatter stringFromDate:eventDate];

    cell.lblDateTime.text = [NSString stringWithFormat:@"%@ at %@", monthString, timeStr];

    [formatter setDateFormat:@"yyyy-MM-dd"];

    if ([[formatter stringFromDate:eventDate] isEqualToString:[formatter stringFromDate:[NSDate date]]]) {
        cell.lblDay.text = @"Today";
    } else {
        cell.lblDay.text = [SRModalClass timeLeftSinceDate:eventDate];
    }
    //  cell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.0f %@",[[eventDetails valueForKey:kKeyDistance] floatValue] ,distanceUnit];

    if ([SRModalClass isEventExist:eventDetails[kKeyId]]) {
        cell.lblareGoing.text = [NSString stringWithFormat:@"you are going +%lu...", [eventDetails[kKeyEvent_Attendees] count]];
    } else {
        cell.lblareGoing.frame = CGRectMake(cell.lblareGoing.frame.origin.x, cell.lblareGoing.frame.origin.y, cell.lblareGoing.frame.size.width - 60, cell.lblareGoing.frame.size.height);
        cell.lblareGoing.text = NSLocalizedString(@"Decline", @"");
        cell.lblareGoing.textAlignment = NSTextAlignmentCenter;
        cell.lblareGoing.textColor = [UIColor redColor];
        cell.lblareGoing.layer.borderWidth = 0.5;
        cell.lblareGoing.layer.borderColor = [[UIColor redColor] CGColor];
        cell.lblareGoing.layer.cornerRadius = 5;
    }

    // If event is rejected
    [cell.rejectEventShadow setHidden:TRUE];
    NSString *loggedInUser = server.loggedInUserInfo[kKeyId];
    for (int i = 0; i < [eventDetails[kKeyEvent_Attendees] count]; i++) {
        if ([[[eventDetails[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
            if ([[[eventDetails[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyInvitationStatus] isEqualToString:@"2"]) {
                [cell.rejectEventShadow setHidden:FALSE];

                // Show decline btn
                //                cell.lblareGoing.frame=CGRectMake(cell.lblareGoing.frame.origin.x, cell.lblareGoing.frame.origin.y, cell.lblareGoing.frame.size.width-60, cell.lblareGoing.frame.size.height);
                cell.lblareGoing.text = NSLocalizedString(@"Decline", @"");
                cell.lblareGoing.textAlignment = NSTextAlignmentCenter;
                cell.lblareGoing.textColor = [UIColor redColor];
                cell.lblareGoing.layer.borderWidth = 0.5;
                cell.lblareGoing.layer.borderColor = [[UIColor redColor] CGColor];
                cell.lblareGoing.layer.cornerRadius = 5;

                // Set calendar iamge
                cell.imgGroups.backgroundColor = [UIColor orangeColor];
                [cell.imgGroups setImage:[UIImage imageNamed:@"calendar-plus_25.png"]];
                [cell setUserInteractionEnabled:FALSE];
            }
        }
    }
    //TODO: Set direction Image
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
            server.myLocation.coordinate.longitude};

    CLLocationCoordinate2D userLoc = {[[eventDetails valueForKey:kKeyLattitude] floatValue], [[eventDetails valueForKey:kKeyRadarLong] floatValue]};

    [cell.imgDirectionBtn addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
    cell.imgDirectionBtn.tag = indexPath.section;

    NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
    if (directionValue == kKeyDirectionNorth) {
        cell.lblCompassDirection.text = @"N";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionEast) {
        cell.lblCompassDirection.text = @"E";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionSouth) {
        cell.lblCompassDirection.text = @"S";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionWest) {
        cell.lblCompassDirection.text = @"W";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionNorthEast) {
        cell.lblCompassDirection.text = @"NE";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionNorthWest) {
        cell.lblCompassDirection.text = @"NW";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionSouthEast) {
        cell.lblCompassDirection.text = @"SE";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
    } else if (directionValue == kKeyDirectionSoutnWest) {
        cell.lblCompassDirection.text = @"SW";
        [cell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
    }


    if (server.myLocation != nil && [eventDetails isKindOfClass:[NSDictionary class]]) {
        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[eventDetails valueForKey:kKeyLattitude] floatValue] longitude:[[eventDetails valueForKey:kKeyRadarLong] floatValue]];
        NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude, toLoc.coordinate.latitude, toLoc.coordinate.longitude];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if (!error) {
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                       if ([self IsValidResponse:data]) {
                                           NSDictionary *dictOSM = [json valueForKey:@"paths"];
                                           NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                                           NSString *distance = arrdistance[0];
                                           double convertDist = [distance doubleValue];
                                           if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                               //meters to killometer
                                               convertDist = convertDist / 1000;
                                           } else {
                                               //meters to miles
                                               convertDist = (convertDist * 0.000621371192);
                                           }
                                           //Correct distance
                                           cell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit];
                                       }
                                   } else {
                                       cell.lblDistanceLabel.text = [NSString stringWithFormat:@"%@ %@", [eventDetails valueForKey:kKeyDistance], distanceUnit];
                                   }
                               }];
    } else {
        cell.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
        cell.lblDistanceLabel.text = [NSString stringWithFormat:@"0.0 %@", distanceUnit];
    }



    //By Madhura
//    NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@",[eventDetails objectForKey:kKeyId]];
//    NSArray *tempArr = [_oldEventDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
//    
//    if(tempArr && tempArr.count)
//    {
//        eventDistanceClass *obj = [tempArr firstObject];
//        
//        if(![[NSString stringWithFormat:@"%@",obj.eventDistance] isEqualToString:@""])
//        {
//            cell.lblDistanceLabel.text= obj.eventDistance;
//        }
//    }
//    else
//    {
////        contentView.lblAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
//    }
//    
//    if ([cell.lblAddress.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")])
//    {
//        cell.lblDistanceLabel.text = [NSString stringWithFormat:@"0.0 %@",distanceUnit];
//    }
    if ([[eventDetails valueForKey:kKeyIs_Converted] isEqualToString:@"1"] && indexPath.section == 2 && [eventDetails[kKeyUserID] isEqualToString:loggedInUser])
        cell.lblConvertedEventMsg.hidden = NO;

    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    //add seperator line
    UIView *separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 2)];/// change size as you need.
    separatorLineView.backgroundColor = [UIColor colorWithRed:170.0 green:170.0 blue:170.0 alpha:0.5];// you can also put image here
    [cell.contentView addSubview:separatorLineView];
    // Return
    return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 95;
}

// ----------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

// ----------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];


    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 6, tableView.bounds.size.width - 90, 18)];

    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:kFontHelveticaMedium size:15];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];

    if (section == 0) {
        [headerView setBackgroundColor:[UIColor colorWithRed:170.0 green:170.0 blue:170.0 alpha:0.5]];

        label.text = NSLocalizedString(@"header.title.event_going_in_area.txt", @"");
    } else if (section == 1) {
        [headerView setBackgroundColor:[UIColor colorWithRed:170.0 green:170.0 blue:170.0 alpha:0.5]];

        label.text = NSLocalizedString(@"header.title.you_invited_event.txt", @"");
    } else if (section == 2) {
        [headerView setBackgroundColor:[UIColor colorWithRed:170.0 green:170.0 blue:170.0 alpha:0.5]];

        label.text = NSLocalizedString(@"header.title.your_event.txt", @"");
    } else if (section == 3) {
        [headerView setBackgroundColor:[UIColor colorWithRed:139.0 green:76.0 blue:91.0 alpha:0.5]];
        label.text = NSLocalizedString(@"header.title.popular_event.txt", @"");
    } else {
        [headerView setBackgroundColor:[UIColor colorWithRed:139.0 green:76.0 blue:91.0 alpha:0.5]];
        label.text = NSLocalizedString(@"header.title.external_event.txt", @"");
    }
    return headerView;
}

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSMutableDictionary *eventDetails;

    if (indexPath.section == 0) {
        eventDetails = [eventGoingInAreaArr[indexPath.row] mutableCopy];
    } else if (indexPath.section == 1) {
        eventDetails = [youInvitedEventListArr[indexPath.row] mutableCopy];
    } else if (indexPath.section == 2) {
        eventDetails = [yourEventListArr[indexPath.row] mutableCopy];
    } else if (indexPath.section == 3) {
        eventDetails = [popularEventListArr[indexPath.row] mutableCopy];
    } else {
        eventDetails = [externalEventListArr[indexPath.row] mutableCopy];
    }


    if ([eventDetails[kKeyMediaImage] isKindOfClass:[NSDictionary class]]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyImageName, [eventDetails[kKeyMediaImage] objectForKey:kKeyImageName]];
        NSArray *dotsFilterArr = [dotsEventPicsArr filteredArrayUsingPredicate:predicate];

        if ([dotsFilterArr count] > 0) {
            eventDetails[kKeyImageObject] = [dotsFilterArr[0] objectForKey:kKeyImageObject];
        }
    }

    server.eventDetailInfo = [SRModalClass removeNullValuesFromDict:eventDetails];
    SREventDetailTabViewController *objGroupTab = [[SREventDetailTabViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:objGroupTab animated:YES];
    // Search settings
    [self.view endEditing:YES];
    self.objTableView.hidden = NO;
    self.txfSearchField.text = @"";


}

//
//--------------------------------------------------
//willDisplayCell:
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((APP_DELEGATE).isEventFilterApplied == NO) {
        if (externalEventListArr.count > 1) {
            if (indexPath.row == externalEventListArr.count - 2) {
                // Post notification
                if ([[externalEventListArr[indexPath.row] valueForKey:kKeyExternalEventType] isEqualToString:kKeyExtEvent]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationCallCategoryExternalEvent object:nil];
                } else if ([[externalEventListArr[indexPath.row] valueForKey:kKeyExternalEventType] isEqualToString:kKeyExtEventvenue]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationCallVenueExternalEvent object:nil];
                } else
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationCallExternalEvent object:nil];
                [self setupTableViewFooter];
            }
        }
    }
}

#pragma mark
#pragma mark TableView Swipable button Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------------------
// canEditRowAtIndexPath:
// ----------------------------------------------------------------------------------------------------------------------
// canEditRowAtIndexPath:
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    //Obviously, if this returns no, the edit option won't even populate
    if (!(indexPath.section == 4)) {
        return YES;
    }
    return NO;
}

// ----------------------------------------------------------------------------------------------------------------------
// commitEditingStyle:
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //Nothing gets called here if you invoke `tableView:editActionsForRowAtIndexPath:` according to Apple docs so just leave this method blank
}

// ----------------------------------------------------------------------------------------------------------------------
// editActionsForRowAtIndexPath:
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *buttonArr;
    currentIndex = indexPath.row;
    NSString *loggedInUser = server.loggedInUserInfo[kKeyId];
    if (indexPath.section == 0) {
        NSString *titleJoin;
        eventData = eventGoingInAreaArr[indexPath.row];
        if ([[eventData valueForKey:kKeyIs_Public] isEqualToString:@"1"]) {
            titleJoin = @"Join";
        } else
            titleJoin = @"Request to join";
        UITableViewRowAction *accept = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:titleJoin handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            NSString *imageName = @"";
            if ([eventData[@"image"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dictImage = eventData[@"image"];
                imageName = dictImage[@"file"];
            }
            dictPram_CometChat = @{kKeyGroupID: eventData[@"chat_room_id"], @"groupName": eventData[@"name"], @"groupPwd": @"", @
                                                                                                                                "grouppic": imageName};
            NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassEventAttendees];
            NSDictionary *paramDict = @{kKeyEventID: eventData[kKeyId], kKeyUserID: [server.loggedInUserInfo valueForKey:kKeyId]};
            [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPOST];
        }];
        accept.backgroundColor = [UIColor orangeColor];
        buttonArr = @[accept];
    }
    if (indexPath.section == 1) {
        eventData = youInvitedEventListArr[indexPath.row];

        for (int i = 0; i < [eventData[kKeyEvent_Attendees] count]; i++) {
            if ([[[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID] isEqualToString:loggedInUser] && [[[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyInvitationStatus] isEqualToString:@"0"]) {
                (APP_DELEGATE).selectedGroupId = [[NSString alloc] init];
                (APP_DELEGATE).selectedGroupId = eventData[kKeyId];
                UITableViewRowAction *accept = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Accept" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                    NSString *imageName = @"";
                    if ([eventData[@"image"] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dictImage = eventData[@"image"];
                        imageName = dictImage[@"file"];
                    }
                    dictPram_CometChat = @{kKeyGroupID: eventData[@"chat_room_id"], @"groupName": eventData[@"name"], @"groupPwd": @"", @
                                                                                                                                        "grouppic": imageName};
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassEventAttendees, [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyId]];
                    NSDictionary *paramDict = @{kKeyEventID: eventData[kKeyId], kKeyUserID: [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID], kKeyStatus: @"1"};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];

                }];
                accept.backgroundColor = [UIColor orangeColor];


                UITableViewRowAction *reject = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Reject" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassEventAttendees, [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyId]];
                    NSDictionary *paramDict = @{kKeyEventID: eventData[kKeyId], kKeyUserID: [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID], kKeyStatus: @"2"};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                }];
                reject.backgroundColor = [UIColor orangeColor];
                buttonArr = @[accept, reject];
            } else if ([[[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID] isEqualToString:loggedInUser] && [[[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyInvitationStatus] isEqualToString:@"1"]) {
                UITableViewRowAction *reject = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Leave" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassEventAttendees, [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyId]];
                    NSDictionary *paramDict = @{kKeyEventID: eventData[kKeyId], kKeyUserID: [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID], kKeyStatus: @"2"};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                }];
                reject.backgroundColor = [UIColor orangeColor];
                buttonArr = @[reject];
            }
        }
    }
    if (indexPath.section == 2) {
        eventData = yourEventListArr[indexPath.row];
        if ([eventData[kKeyUserID] isEqualToString:loggedInUser]) {

            UITableViewRowAction *accept = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                // Delete Event
                [[[UIAlertView alloc] initWithTitle:eventData[kKeyName] message:@"Are you sure you want to delete this event?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
            }];
            accept.backgroundColor = [UIColor redColor];
            if (![[eventData valueForKey:kKeyIs_Converted] isEqualToString:@"1"]) {
                UITableViewRowAction *reject = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Turn Into Group" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassCreateEvent, eventData[kKeyId]];
                    NSString *isConverted = @"1";
                    NSDictionary *paramDict = @{kKeyName: eventData[kKeyName], kKeyDescription: eventData[kKeyDescription], kKeyAddress: eventData[kKeyAddress], kKeyLattitude: eventData[kKeyLattitude], kKeyRadarLong: eventData[kKeyRadarLong], kKeyEvent_Date: eventData[kKeyEvent_Date], kKeyEvent_Time: eventData[kKeyEvent_Time], kKeyEvent_Type: eventData[kKeyEvent_Type], kKeyIs_Public: eventData[kKeyIs_Public], kKeyEvent_Status: eventData[kKeyEvent_Status], kKeyIs_Converted: isConverted};
                    [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                }];
                reject.backgroundColor = [UIColor orangeColor];
                buttonArr = @[reject, accept];
            } else {
                buttonArr = @[accept];
            }
        } else {
            UITableViewRowAction *accept = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Leave" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                for (int i = 0; i < [eventData[kKeyEvent_Attendees] count]; i++) {
                    if ([[[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
                        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassEventAttendees, [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyId]];
                        NSDictionary *paramDict = @{kKeyEventID: eventData[kKeyId], kKeyUserID: [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID], kKeyStatus: @"2"};
                        [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                    }
                }
            }];
            accept.backgroundColor = [UIColor orangeColor];
            buttonArr = @[accept];
        }
    }
    if (indexPath.section == 3) {
        eventData = popularEventListArr[indexPath.row];
        if ([eventData[kKeyUserID] isEqualToString:loggedInUser]) {
            UITableViewRowAction *accept = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                // Delete Event
                [[[UIAlertView alloc] initWithTitle:eventData[kKeyName] message:@"Are you sure you want to delete this event?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
            }];
            accept.backgroundColor = [UIColor redColor];
            buttonArr = @[accept];
        } else {
            UITableViewRowAction *accept = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Reject" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                for (int i = 0; i < [eventData[kKeyEvent_Attendees] count]; i++) {
                    if ([[[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID] isEqualToString:loggedInUser]) {
                        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassEventAttendees, [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyId]];
                        NSDictionary *paramDict = @{kKeyEventID: eventData[kKeyId], kKeyUserID: [[eventData[kKeyEvent_Attendees] objectAtIndex:i] valueForKey:kKeyUserID], kKeyStatus: @"2"};
                        [server makeAsychronousRequest:urlStr inParams:paramDict isIndicatorRequired:YES inMethodType:kPUT];
                    }
                }
            }];
            accept.backgroundColor = [UIColor orangeColor];
            buttonArr = @[accept];
        }
    }
    return buttonArr;
}

#pragma mark
#pragma mark AlertView Delegates
#pragma mark

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassCreateEvent, eventData[kKeyId]];
        [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
    }
}

#pragma mark
#pragma mark Action methods
#pragma mark

// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTappedCaptured:(UIButton *)sender {
    //NSLog(@"Compass tapped:%ld",sender.tag);
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.objTableView]; // maintable --> replace your tableview name
    NSIndexPath *clickedButtonIndexPath = [self.objTableView indexPathForRowAtPoint:touchPoint];


    NSMutableDictionary *eventDict;
    if (clickedButtonIndexPath.section == 0) {
        eventDict = eventGoingInAreaArr[clickedButtonIndexPath.row];
    } else if (clickedButtonIndexPath.section == 1) {
        eventDict = youInvitedEventListArr[clickedButtonIndexPath.row];
    } else if (clickedButtonIndexPath.section == 2) {
        eventDict = yourEventListArr[clickedButtonIndexPath.row];
    } else if (clickedButtonIndexPath.section == 3) {
        eventDict = popularEventListArr[clickedButtonIndexPath.row];
    } else if (clickedButtonIndexPath.section == 4) {
        eventDict = externalEventListArr[clickedButtonIndexPath.row];
    }


    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:@"FromEventListView" bundle:nil inDict:eventDict server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;

}

#pragma mark
#pragma mark Set footerview
#pragma mark

//
//--------------------------------------------------
//setupTableViewFooter
- (void)setupTableViewFooter {
    // set up label
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.navigationController.navigationBar.frame.size.height)];
    footerView.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.font = [UIFont fontWithName:kFontHelveticaBold size:14];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    label.textColor = [UIColor darkGrayColor];
    label.text = @"Loading";

    self.footerLabel = label;
    CGSize labelSize = [self.footerLabel sizeThatFits:footerView.frame.size];
    [footerView addSubview:self.footerLabel];

    // set up activity indicator
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.hidesWhenStopped = YES;
    activityIndicatorView.color = [UIColor blackColor];

    self.activityIndicator = activityIndicatorView;
    [self.activityIndicator startAnimating];

    [footerView addSubview:self.activityIndicator];

    CGRect footerFrame = footerView.frame;
    label.frame = CGRectMake((footerFrame.size.width - labelSize.width - 4 - activityIndicatorView.frame.size.width) / 2, (footerFrame.size.height - labelSize.height) / 2, (footerFrame.size.width - labelSize.width - 4 - activityIndicatorView.frame.size.width), labelSize.height);
    self.activityIndicator.frame = CGRectMake(label.frame.origin.x + labelSize.width + 4, (footerFrame.size.height - activityIndicatorView.frame.size.height) / 2, activityIndicatorView.frame.size.width, activityIndicatorView.frame.size.height);

    self.objTableView.tableFooterView = footerView;
}

#pragma mark
#pragma mark TextField Delegate method
#pragma mark

// --------------------------------------------------------------------------------
//  textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    // Search
    [self searchEventForSearchText:textField.text];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// shouldChangeCharactersInRange:

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *str = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self searchEventForSearchText:str];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldShouldBeginEditing:

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Add overlay view
    [self.view addSubview:overlayImgView];
    self.objTableView.scrollEnabled = NO;

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Add overlay view
    [self.view addSubview:overlayImgView];
    self.objTableView.scrollEnabled = NO;
}

// --------------------------------------------------------------------------------
// textFieldShouldEndEditing:

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // Remove  overlay view from view
    [overlayImgView removeFromSuperview];
    self.objTableView.scrollEnabled = YES;

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// textFieldDidEndEditing

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // Search
    [self searchEventForSearchText:nil];

    [overlayImgView removeFromSuperview];
    [textField resignFirstResponder];

    // Return
    return YES;
}

// --------------------------------------------------------------------------------
// touchesBegan:

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.txfSearchField resignFirstResponder];
    [overlayImgView removeFromSuperview];
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// ShowEventSucceed:

- (void)getEventSucceed:(NSNotification *)inNotify {

    NSArray *eventsList = [inNotify object];
    [userList removeAllObjects];
    userList = [NSMutableArray arrayWithArray:server.eventUsersArr];
    NSMutableArray *externalMutatedArr = [NSMutableArray array];
    NSMutableArray *popularMutatedArr = [NSMutableArray array];
    NSMutableArray *yourMutatedArr = [NSMutableArray array];
    NSMutableArray *youInviteMutatedArr = [NSMutableArray array];
    NSMutableArray *eventGoingMutatedArr = [NSMutableArray array];

    for (NSDictionary *dict in eventsList) {
        NSMutableArray *eventAttendiesArray = dict[kKeyEvent_Attendees];
        NSMutableArray *eventAttendiesIdsArray = [NSMutableArray array];
        for (int i = 0; i < [eventAttendiesArray count]; i++) {
            [eventAttendiesIdsArray addObject:[eventAttendiesArray[i] valueForKey:kKeyUserID]];
        }

        if ([dict[kKeyExternalEvent] isEqualToString:@"1"]) {
            [externalMutatedArr addObject:dict];
        } else if (![eventAttendiesIdsArray containsObject:[server.loggedInUserInfo valueForKey:kKeyId]]) {
            [eventGoingMutatedArr addObject:dict];
        } else if ([[dict valueForKey:kKeyUserID] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
            [yourMutatedArr addObject:dict];
        } else if ([eventAttendiesArray count] >= 25) {
            for (int i = 0; i < [eventAttendiesArray count]; i++) {
                NSDictionary *eventAttendiesDict = eventAttendiesArray[i];
                NSString *invitationStatus = [eventAttendiesDict valueForKey:kKeyInvitationStatus];
                NSString *userId = [eventAttendiesDict valueForKey:kKeyUserID];

                if ([invitationStatus isEqualToString:@"0"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![youInviteMutatedArr containsObject:dict]) {
                        [youInviteMutatedArr addObject:dict];
                    }
                } else if ([invitationStatus isEqualToString:@"1"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![popularMutatedArr containsObject:dict]) {
                        [popularMutatedArr addObject:dict];
                    }
                }
            }
        } else if ([eventAttendiesArray count] >= 25) {
            for (int i = 0; i < [eventAttendiesArray count]; i++) {
                NSDictionary *eventAttendiesDict = eventAttendiesArray[i];
                NSString *invitationStatus = [eventAttendiesDict valueForKey:kKeyInvitationStatus];
                NSString *userId = [eventAttendiesDict valueForKey:kKeyUserID];

                if ([invitationStatus isEqualToString:@"0"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![youInviteMutatedArr containsObject:dict]) {
                        [youInviteMutatedArr addObject:dict];
                    }
                } else if ([invitationStatus isEqualToString:@"1"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![popularMutatedArr containsObject:dict]) {
                        [popularMutatedArr addObject:dict];
                    }
                }
            }
        } else if ([eventAttendiesArray count] >= 25) {
            for (int i = 0; i < [eventAttendiesArray count]; i++) {
                NSDictionary *eventAttendiesDict = eventAttendiesArray[i];
                NSString *invitationStatus = [eventAttendiesDict valueForKey:kKeyInvitationStatus];
                NSString *userId = [eventAttendiesDict valueForKey:kKeyUserID];

                if ([invitationStatus isEqualToString:@"0"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![youInviteMutatedArr containsObject:dict]) {
                        [youInviteMutatedArr addObject:dict];
                    }
                } else if ([invitationStatus isEqualToString:@"1"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![popularMutatedArr containsObject:dict]) {
                        [popularMutatedArr addObject:dict];
                    }
                }
            }
        } else if ([eventAttendiesArray count] < 25) {
            for (int i = 0; i < [eventAttendiesArray count]; i++) {
                NSDictionary *eventAttendiesDict = eventAttendiesArray[i];
                NSString *invitationStatus = [eventAttendiesDict valueForKey:kKeyInvitationStatus];
                NSString *userId = [eventAttendiesDict valueForKey:kKeyUserID];

                if ([invitationStatus isEqualToString:@"0"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![youInviteMutatedArr containsObject:dict]) {
                        [youInviteMutatedArr addObject:dict];
                    }
                } else if ([invitationStatus isEqualToString:@"1"] && [userId isEqualToString:server.loggedInUserInfo[kKeyId]]) {
                    if (![yourMutatedArr containsObject:dict]) {
                        [yourMutatedArr addObject:dict];
                    }
                }
            }
        } else {
            if (![youInviteMutatedArr containsObject:dict]) {
                [youInviteMutatedArr addObject:dict];
            }
        }
    }
    sourceEventGoingAreaArr = eventGoingMutatedArr;
    sourceYourEventArr = yourMutatedArr;
    sourcePopularEventArr = popularMutatedArr;
    sourceExternalEventArr = externalMutatedArr;
    sourceYouInviteEventArr = youInviteMutatedArr;

    int noCounter = 1;
    for (NSDictionary *dict in sourceEventGoingAreaArr) {
        [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
        noCounter++;
    }
    for (NSDictionary *dict in sourceYouInviteEventArr) {
        [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
        noCounter++;
    }
    for (NSDictionary *dict in sourceYourEventArr) {
        [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
        noCounter++;
    }
    for (NSDictionary *dict in sourcePopularEventArr) {
        [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
        noCounter++;
    }
    for (NSDictionary *dict in sourceExternalEventArr) {
        [dict setValue:[NSString stringWithFormat:@"%d", noCounter] forKey:kkeyNumbering];
        noCounter++;
    }
    eventGoingInAreaArr = [sourceEventGoingAreaArr mutableCopy];
    yourEventListArr = [sourceYourEventArr mutableCopy];
    popularEventListArr = [sourcePopularEventArr mutableCopy];
    externalEventListArr = [sourceExternalEventArr mutableCopy];
    youInvitedEventListArr = [sourceYouInviteEventArr mutableCopy];

    // Reload table
    [self.objTableView reloadData];

    [_distanceQueue cancelAllOperations];
    [self getDistance];
}
// --------------------------------------------------------------------------------
// ShowEventSucceed:

- (void)getEventFailed:(NSNotification *)inNotify {
    // NSDictionary *result =[inNotify object];

}

- (void)EventConvertedSucceed:(NSNotification *)inNotify {
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewEventAdded object:nil];
    [self.objTableView reloadData];
}

// --------------------------------------------------------------------------------
// AcceptRejectEventSucceed:
- (void)AcceptRejectEventSucceed:(NSNotification *)inNotify {
    // Post notification for call new events from server
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewEventAdded object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFetchNotification object:nil];
    NSDictionary *dict = [[NSDictionary alloc] init];
    dict = [inNotify userInfo];
}

// --------------------------------------------------------------------------------
// AcceptRejectEventFailed:
- (void)AcceptRejectEventFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// DeleteEventSucceed:
- (void)DeleteEventSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    // Post notification for call new events from server
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewEventAdded object:nil];
}

// --------------------------------------------------------------------------------
// DeleteEventFailed:
- (void)DeleteEventFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}
// --------------------------------------------------------------------------------
// RequestToJoinEventSucceed:

- (void)RequestToJoinEventSucceed:(NSNotification *)inNotify {
    // Post notification for call new events from server
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationNewEventAdded object:nil];
}
// --------------------------------------------------------------------------------
// RequestToJoinFailed:

- (void)RequestToJoinFailed:(NSNotification *)inNotify {
    NSLog(@"Request to Join Failed");
}
@end
