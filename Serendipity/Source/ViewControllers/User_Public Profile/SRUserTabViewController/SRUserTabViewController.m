#import "SRModalClass.h"
#import "SRUserViewController.h"
#import "SRPingsViewController.h"
#import "SRDegreeSeperationViewController.h"
#import "SRUserTabViewController.h"

@interface SRUserTabViewController ()

@property(nonatomic, strong) SRUserViewController *objUser;

@end

@implementation SRUserTabViewController

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    if ([nibNameOrNil isEqualToString:@"SRUserTabViewController"]) {
        self.showDegreeSeperation = kKeyIsFromDegreeSeperation;
    } else if ([nibNameOrNil isEqualToString:@"DisabledDegreeSeperation"]) {
        self.showDegreeSeperation = kKeyIsFromZeroDegreeSeperation;
    } else if ([nibNameOrNil isEqualToString:@"fromChatView"]) {
        fromChatView = YES;
    }

    self = [super initWithNibName:@"SRUserTabViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
        
    }

    // Return
    return self;
}

#pragma mark
#pragma mark Standard Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

//    (APP_DELEGATE).topBarView.hidden = YES;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;

    // Set delegate
    [self setDelegate:self];

    // Register Notifications
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(addUserSucceed:)
                          name:kKeyNotificationAddConnectionSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(addUserFailed:)
                          name:kKeyNotificationAddConnectionFailed object:nil];

    // Title
//	NSString *fullName = [NSString stringWithFormat:@"%@ %@", [(APP_DELEGATE).server.friendProfileInfo objectForKey:kKeyFirstName], [(APP_DELEGATE).server.friendProfileInfo objectForKey:kKeyLastName]];
    NSString *fullName;
    NSDictionary *userDictt = (APP_DELEGATE).server.friendProfileInfo;

    if ((APP_DELEGATE).server.friendProfileInfo[kKeySetting]) {
        if ([(APP_DELEGATE).server.friendProfileInfo[kKeySetting] objectForKey:kkeyHideLastName]) {
            if ([[(APP_DELEGATE).server.friendProfileInfo[kKeySetting] objectForKey:kkeyHideLastName] boolValue] == YES) {
                fullName = [NSString stringWithFormat:@"%@", (APP_DELEGATE).server.friendProfileInfo[kKeyFirstName]];
            } else {
                fullName = [NSString stringWithFormat:@"%@ %@", (APP_DELEGATE).server.friendProfileInfo[kKeyFirstName], (APP_DELEGATE).server.friendProfileInfo[kKeyLastName]];
            }
        }
    }

    
    

    [SRModalClass setNavTitle:fullName forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

//	UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
//	[leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];


    if ([(APP_DELEGATE).server.friendProfileInfo[kKeyConnection] isKindOfClass:[NSDictionary class]]) {
        if ([[(APP_DELEGATE).server.friendProfileInfo[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"1"] || [[(APP_DELEGATE).server.friendProfileInfo[kKeyConnection] objectForKey:kKeyConnectionStatus] isEqualToString:@"2"]) {
            UIImage *image1 = [[UIImage imageNamed:@"phone.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            secondRightButton = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(makePhoneCall:)];

            self.navigationItem.rightBarButtonItems = @[secondRightButton];
        } else {
            UIImage *image = [[UIImage imageNamed:@"menubar-man-plus.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            rightButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(addProfileBtnAction:)];

            UIImage *image1 = [[UIImage imageNamed:@"phone.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            secondRightButton = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(makePhoneCall:)];

            self.navigationItem.rightBarButtonItems = @[rightButton, secondRightButton];
        }
    } else {
        UIImage *image = [[UIImage imageNamed:@"menubar-man-plus.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        rightButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(addProfileBtnAction:)];

        UIImage *image1 = [[UIImage imageNamed:@"phone.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        secondRightButton = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(makePhoneCall:)];

        self.navigationItem.rightBarButtonItems = @[rightButton, secondRightButton];
    }


    if ((APP_DELEGATE).server.friendProfileInfo[kKeyMessageStatus]) {
        UIImage *image1 = [[UIImage imageNamed:@"phone.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        secondRightButton = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(makePhoneCall:)];

        self.navigationItem.rightBarButtonItems = @[secondRightButton];
    }


    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];

    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f], NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f], NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];

    // Set Profile tab
    _objUser = [[SRUserViewController alloc] initWithNibName:nil bundle:nil profileData:(APP_DELEGATE).server.friendProfileInfo server:(APP_DELEGATE).server];
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-user-female.png"];
    UITabBarItem *objUserTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userProfile.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-user-female-active.png"]];
    [objUserTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    _objUser.tabBarItem = objUserTabItem;

    //  Set Degree of Seperation tab
    SRDegreeSeperationViewController *degreeSepCon = [[SRDegreeSeperationViewController alloc] initWithNibName:nil bundle:nil profileData:(APP_DELEGATE).server.friendProfileInfo server:(APP_DELEGATE).server];
    tabImage = [UIImage imageNamed:@"tabbar-degree.png"];
    UITabBarItem *objSRSOPTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.degreeOfSeperation.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-degree-active.png"]];
    [objSRSOPTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    degreeSepCon.tabBarItem = objSRSOPTabItem;
    self.delegate = self;

    // Set Pings tab
    SRChatViewController *objSRPingsView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:(APP_DELEGATE).server inObjectInfo:(APP_DELEGATE).server.friendProfileInfo];
    // objSRPingsView.delegate = self;
    objSRPingsView.tabBarHeight = self.tabBar.frame.size.height;
    tabImage = [UIImage imageNamed:@"tabbar-ping.png"];
    UITabBarItem *SRPingsViewTabItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"tab.userPing.text", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-ping-active.png"]];
    [SRPingsViewTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    objSRPingsView.tabBarItem = SRPingsViewTabItem;

    if (fromChatView) {
        self.viewControllers = @[_objUser];
        [SRModalClass hideTabBar:(APP_DELEGATE).tabBarController];
    } else
        self.viewControllers = @[_objUser, degreeSepCon, objSRPingsView];


    self.tabBar.unselectedItemTintColor = appTintColor;

    //
    if (!fromChatView) {
        if ([self.showDegreeSeperation isEqualToString:kKeyIsFromDegreeSeperation]) {
            self.selectedIndex = 1;
        } else if ([self.showDegreeSeperation isEqualToString:kKeyIsFromZeroDegreeSeperation]) {
            [[[self tabBar] items][1] setEnabled:FALSE];
        } else
            self.selectedIndex = 0;


        BOOL flag = NO;

        NSDictionary *userDict = (APP_DELEGATE).server.friendProfileInfo;

        if ([[userDict valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *setting = [NSDictionary dictionaryWithDictionary:[userDict valueForKey:kKeySetting]];

            //            if (![[setting valueForKey:kKeyallowPings]boolValue])
            //                flag=NO;
            NSUInteger pingStatus = [[setting valueForKey:kKeyallowPings] integerValue];
            if (pingStatus == 1) {
                flag = YES;
            } else if (pingStatus == 2) {
                if ([[userDict valueForKey:kKeyConnection] isKindOfClass:[NSDictionary class]]) {
                    if ([[[userDict valueForKey:kKeyConnection] valueForKey:kKeyConnectionis_imported] boolValue]) {
                        flag = YES;
                    }
                }
            } else if (pingStatus == 3) {
                if ([[userDict valueForKey:kKeyConnection] isKindOfClass:[NSDictionary class]]) {
                    flag = YES;
                }
            } else if (pingStatus == 4) {
                if ([[userDict valueForKey:kKeyToFavourite] boolValue]) {
                    flag = YES;
                }
            } else if (pingStatus == 5) {
                if ([[userDict valueForKey:kKeyFamily] isKindOfClass:[NSDictionary class]]) {
                    flag = YES;
                }
            } else if (pingStatus == 6) {
                flag = NO;
            }
        }
        if (flag) {
            [[[self tabBar] items][2] setEnabled:TRUE];
        } else {
            [[[self tabBar] items][2] setEnabled:FALSE];
        }

    }
    //




}

- (BOOL)hidesBottomBarWhenPushed {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];

}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if (viewController == tabBarController.viewControllers[1]) {
        if ([self.showDegreeSeperation isEqualToString:kKeyIsFromZeroDegreeSeperation]) {
            return NO;
        } else
            return YES;
    } else if (viewController == tabBarController.viewControllers[2]) {
        if ([(APP_DELEGATE).server.friendProfileInfo[kKeyId] isEqualToString:(APP_DELEGATE).server.loggedInUserInfo[kKeyId]]) {
            return NO;
        } else {
            //[APP_DELEGATE showActivityIndicator];
            //[self.navigationItem setRightBarButtonItems:nil animated:NO];
            return YES;
        }
    } else {
        return YES;
    }

}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];

 //   NSLog(@"Tabbar controller dealloc");

}

#pragma mark
#pragma mark Action Methodss
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

// ---------------------------------------------------------------------------------------
// plusBtnAction:

- (void)addProfileBtnAction:(id)sender {
    NSDictionary *userDict = (APP_DELEGATE).server.friendProfileInfo;
    NSDictionary *params = @{kKeyUserID: (APP_DELEGATE).server.loggedInUserInfo[kKeyId], kKeyConnectionID: userDict[kKeyId]};
    [(APP_DELEGATE).server makeAsychronousRequest:kKeyClassAddConnection inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

- (void)makePhoneCall:(id)sender {
    NSDictionary *userDict = (APP_DELEGATE).server.friendProfileInfo;
    if (![[userDict valueForKey:kKeyMobileNumber] isKindOfClass:[NSNull class]] && [userDict valueForKey:kKeyMobileNumber]) {
        NSString *phoneNumber = [userDict valueForKey:kKeyMobileNumber];
        NSURL *phoneUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];

        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {            
            [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];

        } else {
            [SRModalClass showAlert:@"Your device doesn't support this feature."];
        }
    } else {
        [SRModalClass showAlert:@"Mobile number is not correct."];
    }
}

#pragma mark -
#pragma mark TabBarController Delegate
#pragma mark -

// ----------------------------------------------------------------------------
// didSelectViewController:

- (void)tabBarController:(UITabBarController *)inTabBarController didSelectViewController:(UIViewController *)viewController {

}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// addUserSucceed:

- (void)addUserSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.requestsent.text", @"")];
    //rightButton.hidden = YES;
    UIImage *image1 = [[UIImage imageNamed:@"phone.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    secondRightButton = [[UIBarButtonItem alloc] initWithImage:image1 style:UIBarButtonItemStylePlain target:self action:@selector(makePhoneCall:)];
    self.navigationItem.rightBarButtonItems = @[secondRightButton];

    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateRadarList object:nil];
}

// --------------------------------------------------------------------------------
// addUserFailed:

- (void)addUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

@end
