#import <UIKit/UIKit.h>
#import "SRChatViewController.h"

@interface SRUserTabViewController : UITabBarController<UITabBarControllerDelegate>
{
    //Instance Variables
    UIBarButtonItem *rightButton,*secondRightButton;
    UIButton *phoneCall;
    BOOL fromChatView;
}
@property (strong,nonatomic) NSString *showDegreeSeperation;

@property (nonatomic,strong) NSMutableArray *familyListArr;


#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer ;
@end
