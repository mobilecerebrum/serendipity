//
//  SRUserTrackCell.m
//  Serendipity
//
//  Created by Sunil Dhokare on 28/07/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRUserTrackCell.h"

@implementation SRUserTrackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
