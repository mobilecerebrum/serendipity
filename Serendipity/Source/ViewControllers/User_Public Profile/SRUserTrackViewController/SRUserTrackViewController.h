//
//  SRUserTrackViewController.h
//  Serendipity
//
//  Created by Sunil Dhokare on 28/07/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface SRUserTrackViewController : UIViewController
{
    // Server
    SRServerConnection *server;
    
    // Instance variable
    NSArray * trackArray;
    NSMutableArray *yestardayData;
    NSMutableArray *last7DayData;
    NSMutableArray *last15DayData;
    // Instance variable
    NSMutableDictionary *userProfileInfo;
    NSMutableDictionary *trackDataDict;
    NSArray *time1;
    NSArray *time2;
    NSArray *time3;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *bgProfileImgView;
@property (weak, nonatomic) IBOutlet UIImageView *overlayImgView;
@property (weak, nonatomic) IBOutlet UITableView *objTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
- (IBAction)valueChanged:(id)sender;



// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil profileData:(NSDictionary *)inProfileDict server:(SRServerConnection *)inServer;
@end
