//
//  SRUserTrackViewController.m
//  Serendipity
//
//  Created by Sunil Dhokare on 28/07/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRUserTrackViewController.h"
#import "SRModalClass.h"
#import "SRUserTrackCell.h"

@implementation SRUserTrackViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil profileData:(NSDictionary *)inProfileDict server:(SRServerConnection *)inServer {
    //Call Super
    self = [super initWithNibName:@"SRUserTrackViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        yestardayData = [[NSMutableArray alloc] init];
        last7DayData = [[NSMutableArray alloc] init];
        last15DayData = [[NSMutableArray alloc] init];
        userProfileInfo = [[NSMutableDictionary alloc]initWithDictionary:inProfileDict];
        trackDataDict = [[NSMutableDictionary alloc] init];

        
        
    }
    
    //return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [SRModalClass setNavTitle:@"My Track" forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame= self.segmentControl.frame;
    [self.segmentControl setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 40)];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f]};
    if (SCREEN_WIDTH <= 320) {
        attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:14.0f]};
    }
    [self.segmentControl setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];

    
    // Profile image
    self.bgProfileImgView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
   
    if ([[userProfileInfo objectForKey:kKeyProfileImage]isKindOfClass:[NSDictionary class]])
    {
        if ([[userProfileInfo objectForKey:kKeyProfileImage]objectForKey:kKeyImageName] != nil)
        {
            NSString *imageName = [[userProfileInfo objectForKey:kKeyProfileImage]objectForKey:kKeyImageName];
            if ([imageName length] > 0)
            {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityServer, kKeyUserProfileImage, imageName];;
                
                [self.bgProfileImgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            }
        }
    }
   
   
    NSArray *city = [[NSArray alloc] initWithObjects:@"Kashi Mira Road, Near Laxmi Baug, Thane", @"Swami Vivekanand Road, Near Station, Borivali West",@"New Link Road, Borivali West",@"S V Road, Dahisar East",@"Shiv Vallabh Road, Western Express Highway, Dahisar East",@"Western Express Highway, Mira Village, Mira Road, Mira Bhayandar",@"Navghar Rd, Chandan nagar, Bhayandar East",@"Mahavidhyalay Marg, Navghar Gaon, Bhayandar East",@"Kanakia Rd, Mira Road East",@"Goddev Phatak Rd, Vikas Industrial Estate, Bhayandar East",@"RSC Road Number 41, Mumbai",@"Uran Road, Sector 19A, Nerul", nil];
   time1 = [[NSArray alloc] initWithObjects:@"11:05 am",@"11:50 am",@"12:02 am",@"12:40 pm",@"2:02 pm",@"2:20 pm",@"2:55 pm",@"3:15 pm",@"4:25 pm",@"5:30 pm",@"5:55 pm",@"6:00 pm", nil];
    
    for (int i = 0; i < city.count; i++) {
        trackDataDict = [[NSMutableDictionary alloc]init];
        [trackDataDict setObject:[city objectAtIndex:i] forKey:kKeyLocation];
//        [trackDataDict setObject:[time objectAtIndex:i] forKey:kKeyTime];
        [last15DayData addObject:trackDataDict];
    }
    city = [[NSArray alloc] initWithObjects:@"SN Road, Near Court, Mulund West", @"Lokmanya Tilak Road & Kesar Baug Ln, Hanuman Chowk, Mulund East",@"Veer Savarkar Road, Gavanpada, Mulund East",@"LBS Marg, Mulund West",@"Lal Bahadur Shastri Road, Rajiv Gandhi Nagar, Bhandup West",@"Station Road, C.G.S. Colony, Kanjurmarg East",@"V.S. Road, C.G.S. Colony, Bhandup East",@" Link Road, Kanjurmarg (East)",@"Jawahar Rd, Ghatkopar, Saibaba Nagar, Pant Nagar, Ghatkopar East",@"Ramakrishna Chemburkar Marg, Postal Colony, Chembur",@"Trombay Road, Chembur",@"Swamy Vallabhdas Road, Chunabhatti, Sion", nil];
    time2 = [[NSArray alloc] initWithObjects:@"9:09 am",@"10:00 am",@"10:59 am",@"11:30 pm",@"12:05 pm",@"1:05 pm",@"1:50 pm",@"2:20 pm",@"3:00 pm",@"4:10 pm",@"5:25 pm",@"5:55 pm", nil];
    
    for (NSInteger i = city.count - 1; i >= 0; i--) {
        trackDataDict = [[NSMutableDictionary alloc]init];
        [trackDataDict setObject:[city objectAtIndex:i] forKey:kKeyLocation];
//        [trackDataDict setObject:[time objectAtIndex:i] forKey:kKeyTime];
        [last7DayData addObject:trackDataDict];
    }
    
    city = [[NSArray alloc] initWithObjects:@"Chhatrapati Shivaji International Airport",@"Kirol Road, Off LBS Road, Kurla West",@"Vihar Road, Powai",@"90 ft Road, Kandivili (East)",@"Dr Annie Besant Road, Worli",@"IA Project Road, Andheri",@"S.K.Bole Marg, Prabhadevi",@"Dr Meisheri Road, Central Railway Godown, Umerkhadi",@"Acharya Donde Marg, Parel",@"Kalbadevi Road, Dhobi Talao, Kalbadev",@"Kirol Street, Jugaldas Mody Marg",@"Dr Baba Saheb Ambedkar Marg, Byculla East", nil];
    time3 = [[NSArray alloc] initWithObjects:@"10:00 am",@"11:05 am",@"11:45 am",@"12:15 pm",@"1:05 pm",@"2:00 pm",@"3:10 pm",@"4:18 pm",@"5:35 pm",@"6:02 pm",@"6:40 pm",@"7:10 pm", nil];
    for (int i = 0; i < city.count; i++) {
        trackDataDict = [[NSMutableDictionary alloc]init];
        [trackDataDict setObject:[city objectAtIndex:i] forKey:kKeyLocation];
//        [trackDataDict setObject:[time objectAtIndex:i] forKey:kKeyTime];
        [yestardayData addObject:trackDataDict];
    }
    trackArray = yestardayData;
    
    //Shuffle array data
    NSMutableArray *shuffleArray  = [[NSMutableArray alloc]initWithArray:trackArray];
    NSUInteger count = [shuffleArray count];
    if (count < 1) return;
    for (NSUInteger i = 0; i < count - 1; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [shuffleArray exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    trackArray = shuffleArray;
    
    [self.objTableView reloadData];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Action Methods
// ---------------------------------------------------------------------------------------
// valueChanged:
- (IBAction)valueChanged:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        trackArray = yestardayData;
    }
    else if (sender.selectedSegmentIndex == 1)
    {
        trackArray = last7DayData;
    }
    else if (sender.selectedSegmentIndex == 2)
    {
        trackArray = last15DayData;
    }
    
    //Shuffle array data
    NSMutableArray *shuffleArray  = [[NSMutableArray alloc]initWithArray:trackArray];
    NSUInteger count = [shuffleArray count];
    if (count < 1) return;
    for (NSUInteger i = 0; i < count - 1; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [shuffleArray exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    trackArray = shuffleArray;
   
    
    [self.objTableView reloadData];
}

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// ---------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return trackArray.count;
}

// ---------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    SRUserTrackCell *cell;
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRUserTrackCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            cell = (SRUserTrackCell *)[nibObjects objectAtIndex:0];
        }
    }
    // Selection style
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dataDict = [trackArray objectAtIndex:indexPath.row];
    cell.lblAddress.text = [dataDict valueForKey:kKeyLocation];
    [cell.lblAddress adjustsFontSizeToFitWidth];
    if (self.segmentControl.selectedSegmentIndex == 0) {
        cell.lblTime.text = [time1 objectAtIndex:indexPath.row];
    }
    else if (self.segmentControl.selectedSegmentIndex == 1)
    {
         cell.lblTime.text = [time2 objectAtIndex:indexPath.row];
    }
    else
    {
         cell.lblTime.text = [time3 objectAtIndex:indexPath.row];
    }
    
    
    // Return
    return cell;
}

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
// ----------------------------------------------------------------------------------
// heightForFooterInSection:
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 30.0f;
}

// ----------------------------------------------------------------------------------
// viewForFooterInSection:
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    footerView.frame = CGRectMake(SCREEN_WIDTH/2, 5, 60, 4);
    footerView.backgroundColor = [UIColor blackColor];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    imgView.alpha = 0.8;
    [imgView setBackgroundColor:[UIColor colorWithRed:102.0 / 255.0f green:77 / 255.0f blue:66 / 255.0f alpha:0.8]];
    [footerView addSubview:imgView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, tableView.bounds.size.width, 18)];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:kFontHelveticaMedium size:15];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"Today's total distance : 3000 miles";
    [footerView addSubview:label];
    
    return footerView;
}
@end
