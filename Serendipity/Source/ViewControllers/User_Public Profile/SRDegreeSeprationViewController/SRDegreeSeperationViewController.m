#import "SRModalClass.h"
#import "SRMapWindowView.h"
#import "SRDegreeBtnView.h"
#import "SRUserTabViewController.h"
#import "SRDegreeSeperationViewController.h"
#import "UIView+Animation.h"


#define kKeyTappedImage @"tappedImg"
#define kKeyAddedView @"addedView"
#define kKeySubViewTag @"subviewTag"

@interface SRDegreeSeperationViewController ()
{
    NSMutableArray *degreeUserList; //This is Use for change degreeview accrding user select extended degree user
    BOOL        isDegreeUserSelected;
}

@end

@implementation SRDegreeSeperationViewController
#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRDegreeSeperationViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        profileDict = [[NSMutableDictionary alloc] initWithDictionary:inProfileDict];
        extendedListArr = [[NSMutableArray alloc]init];
        tappedImgView = [[SRCustomImageView alloc] init];
        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailsSucceed:)
                              name:kKeyNotificationGetUserProfileSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailsFailed:)
                              name:kKeyNotificationGetUserProfileSucceed
                            object:nil];
        
//        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassUpdateUserPhoneNo, [inProfileDict objectForKey:kKeyId]];
//        [server makeAsychronousRequest:urlStr
//                              inParams:nil
//                   isIndicatorRequired:NO
//                          inMethodType:kGET];
        
        [SRAPIManager.sharedInstance getUserWithId:[inProfileDict objectForKey:kKeyId] completion:^(id  _Nonnull result, NSError * _Nonnull error) {
            if (!error) {
                NSMutableDictionary *profileInfo = [[result objectForKey:@"response"] mutableCopy];
                [self updateUserInfo:profileInfo];
            }
        }];
        
    }
    
    // Return
    return self;
}

#pragma mark
#pragma mark Standard Overrides Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call Super
    
    [super viewDidLoad];
    [self.indicatorView stopAnimating];
    // Display degree of sepreation on label
    self.lblDegree.layer.cornerRadius = self.lblDegree.frame.size.width / 2.0;
    self.lblDegree.layer.borderColor = [UIColor blackColor].CGColor;
    self.lblDegree.layer.borderWidth = 5.0;
    self.lblDegree.backgroundColor = [UIColor orangeColor];
    self.lblDegree.clipsToBounds=YES;
    self.lblDegree.layer.masksToBounds=YES;
    degreeUserList = [[NSMutableArray alloc]init];
    isDegreeUserSelected=NO;
    
    //
    [self.btnDegree setBackgroundColor:[UIColor clearColor]];
    self.btnDegree.layer.cornerRadius=self.btnDegree.frame.size.width/2;
    
    if ([[profileDict objectForKey:kKeyDegree] isKindOfClass:[NSNumber class]]) {
        NSInteger degree = [[profileDict objectForKey:kKeyDegree]integerValue];
        self.lblDegree.text = [NSString stringWithFormat:@" %ld°", (long)degree];
    }
    else {
        self.lblDegree.text = @" 0°";
    }
    
    //Creating and Adding a blurring overlay view
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.imgViewBckScroll.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.imgViewBckScroll.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.imgViewBckScroll addSubview:blurEffectView];
    }
    else {
        self.imgViewBckScroll.backgroundColor = [UIColor colorWithRed:85.0 green:85.0 blue:85.0 alpha:0.7f];
    }
    // Enable paging
    self.btnReset.layer.borderWidth = 0.0;
    self.scrollView.pagingEnabled = YES;
    self.userProfileImgView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    isUserSelect=false;
}

// --------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setUpDegreesCombination];
}


#pragma mark
#pragma mark Private Method
#pragma mark
-(void)setUpDegreesCombination
{
    [self.indicatorView stopAnimating];
    // Display the combination
    if ([[profileDict objectForKey:kKeyDegree]isKindOfClass:[NSNumber class]] && [[profileDict objectForKey:kKeyDegree]integerValue] > 1) {
        self.lblNoPpl.hidden = YES;
        
        selectedDegree=[[profileDict objectForKey:kKeyDegree] integerValue];
        userDegree=selectedDegree;
        
        [self displayCombination:[[profileDict objectForKey:kKeyDegree] stringValue]];
    }
    else {
        self.lblNoPpl.hidden = NO;
        if ([[profileDict objectForKey:kKeyDegree]isKindOfClass:[NSNumber class]] && [[profileDict objectForKey:kKeyDegree]integerValue] == 1) {
            
            selectedDegree=[[profileDict objectForKey:kKeyDegree] integerValue];
            userDegree=selectedDegree;
            self.lblNoPpl.text = [NSString stringWithFormat:@"You are directly connected to %@", [profileDict objectForKey:kKeyFirstName]];
        }
        else if ([[profileDict objectForKey:kKeyDegree]isKindOfClass:[NSString class]]) {
            
            selectedDegree=0;
            userDegree=selectedDegree;
            self.lblNoPpl.text = [NSString stringWithFormat:@"There are no degree connection available between you and %@", [profileDict objectForKey:kKeyFirstName]];
            [self.lblNoPpl sizeToFit];
        }
    }
    //Set up  curv menu
    [self setUpCurvMenu];
    //Setup Profile image
    id mediaDict = [profileDict objectForKey:kKeyProfileImage];
    if ([mediaDict isKindOfClass:[NSDictionary class]] && [mediaDict objectForKey:kKeyImageName] != nil) {
        // Get result
        if ([mediaDict objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [mediaDict objectForKey:kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                [self.userProfileImgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            }
        }
    }
}


-(IBAction)btnResetClicked:(id)sender
{
    isUserSelect=false;
    [self displayCombination:[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]];
}
// --------------------------------------------------------------------------------------------------------------------
// displayCombination

- (void)displayCombination:(NSString *)degreeValue {
    NSDictionary *combinationDict = [profileDict objectForKey:kKeyDegreeCombination];
    //Commited because getting crash here when we try to remove null vale of array
    if ([combinationDict isKindOfClass:[NSArray class]]) {
        if (combinationDict.count == 0) {
            self.lblNoPpl.hidden = NO;
            self.lblNoPpl.text = [NSString stringWithFormat:@"There are no degree connection available between you and %@", [profileDict objectForKey:kKeyFirstName]];
        }
        return;
    }
    combinationArr = [combinationDict objectForKey:degreeValue];
    self.lblNoPpl.hidden = YES;
    [self.scrollView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (obj != self.lblNoPpl) {
            [obj removeFromSuperview];
        }
    }];
    
    if(!isDegreeUserSelected)
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width , 0);
    if(combinationArr.count>=1 &&selectedDegree==1)
    {
        self.lblNoPpl.hidden=NO;
        self.lblNoPpl.text = [NSString stringWithFormat:@"You are directly connected to %@", [profileDict objectForKey:kKeyFirstName]];
        
    }else if(combinationArr.count==0)
    {
        self.lblNoPpl.hidden=NO;
        self.lblNoPpl.text = [NSString stringWithFormat:@"There are no degree connection available between you and %@", [profileDict objectForKey:kKeyFirstName]];
        [self.lblNoPpl sizeToFit];
    }
    else
    {
        self.lblNoPpl.hidden=YES;
        self.tblUser = [[UITableView alloc] init];
        if (selectedDegree==2)
        {
            _tblUser.frame=CGRectMake(self.scrollView.frame.origin.x,100, self.scrollView.frame.size.width,85*selectedDegree );
        }
        else if(selectedDegree==3)
        {
            _tblUser.frame=CGRectMake(self.scrollView.frame.origin.x,70, self.scrollView.frame.size.width,85*selectedDegree );
        }
        else if(selectedDegree==4)
        {
            _tblUser.frame=CGRectMake(self.scrollView.frame.origin.x,35, self.scrollView.frame.size.width,85*selectedDegree );
        }
        else
        {
            _tblUser.frame=self.scrollView.bounds;
        }
        self.tblUser.separatorStyle=UITableViewCellSeparatorStyleNone;
        self.tblUser.backgroundColor=[UIColor clearColor];
        [self.scrollView addSubview:self.tblUser];
        self.tblUser.delegate=self;
        self.tblUser.dataSource=self;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (selectedDegree==2)
    {
        return 1;
    }
    else if (selectedDegree==3)
    {
        return 2;
    }
    else if (selectedDegree==4)
    {
        return 3;
    }
    else if (selectedDegree==5)
    {
        return 4;
    }
    else if (selectedDegree==6)
    {
        return 5;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

// ----------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    // Reuse and create cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [layout setSectionInset:UIEdgeInsetsMake(0, 10, 0, 10)];
        CGRect frame;
        frame=CGRectMake(cell.frame.origin.x-1, cell.frame.origin.y-30, self.scrollView.frame.size.width+5, 185);
        UICollectionView *  collectionView=[[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
        collectionView.backgroundColor=[UIColor clearColor];
        collectionView.backgroundView = nil;
        collectionView.layer.cornerRadius = 8.0;
        collectionView.layer.masksToBounds = YES;
        [collectionView setDataSource:self];
        [collectionView setDelegate:self];
        collectionView.alwaysBounceVertical = NO;
        collectionView.tag=indexPath.row;
        [self UserCollectionArray:indexPath.row];
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        cell.backgroundColor=[UIColor clearColor];
        [cell addSubview:collectionView];
    }
    return cell;
}

-(void)showNewDegreeConnection:(UITapGestureRecognizer *)gesture
{
    isUserSelect=false;
    long userId=gesture.view.tag ;
    _collectionView=(UICollectionView *)gesture.view.superview;
    NSMutableArray *secUserList=[[NSMutableArray alloc]init];
    for (NSUInteger i = 0; i < [combinationArr count]; i++)
    {
        NSArray *objectsArr = [combinationArr objectAtIndex:i];
        
        if ([objectsArr count]>0)
        {
            if (![secUserList containsObject:[objectsArr objectAtIndex:gesture.view.superview.tag]])
            {
                [secUserList addObject:[objectsArr objectAtIndex:gesture.view.superview.tag]];
            }
        }
    }
    degreeUserList=secUserList;
    int index=0;
    for (int i = 0 ; i < degreeUserList.count; i++)
    {
        if ([[NSNumber numberWithInteger:userId] isEqualToNumber:[degreeUserList objectAtIndex:i]])
        {
            index=i;
            break;
        }
    }
    if ([degreeUserList count]>1)
    {
        for (int i = 0 ; i < degreeUserList.count; i++)
        {
            if (![[NSNumber numberWithInteger:userId] isEqualToNumber:[degreeUserList objectAtIndex:i]])
            {
                NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
                UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath: path];
                NSIndexPath *pathindex = [NSIndexPath indexPathForRow:index inSection:0];
                UICollectionViewCell *cellobj = [_collectionView cellForItemAtIndexPath: pathindex];
                for (UIView *i in cell.contentView.subviews)
                {
                    if([i isKindOfClass:[UILabel class]])
                    {
                        UILabel *lbl=(UILabel *)i;
                        lbl.hidden=YES;
                    }
                }
                [UIView animateWithDuration:2 animations:^{
                    [cell setFrame:CGRectMake(cell.frame.origin.x,cell.frame.origin.y+80, cell.frame.size.width, cell.frame.size.height)];
                    for (UIView *i in cell.contentView.subviews)
                    {
                        if([i isKindOfClass:[UIImageView class]])
                        {
                            UIImageView *img=(UIImageView *)i;
                            img.alpha=0.0;
                        }
                    }
                }];
                NSMutableArray *finalArr=[[NSMutableArray alloc]init];
                NSArray *arr = [_collectionView visibleCells];
                [self UserCollectionArray:_collectionView.tag];
                for (int i=0;i<[CollectionViewArr count];i++)
                {
                    int userid=[[CollectionViewArr objectAtIndex:i] intValue];
                    for (int j=0;j<[arr count];j++)
                    {
                        UICollectionViewCell *cellobj2 = [arr objectAtIndex:j];
                        if (userid==(int)cellobj2.tag)
                        {
                            [finalArr addObject:cellobj2];
                        }
                    }
                }
                int loc=(int)[finalArr count];
                UICollectionViewCell *cellobjframe=[finalArr objectAtIndex:loc/2];
                [UIView animateWithDuration:2 animations:^{
                    if ([self->CollectionViewArr count]<=5)
                    {
                        
                        [cellobj setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width/2)-(cellobj.frame.size.width/2), cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                    }
                    else
                    {
                        if(([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone && UIScreen.mainScreen.nativeBounds.size.height == 2436))
                        {
                            if (index<=5)
                            {
                                [cellobj setFrame:CGRectMake(155, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                            else
                            {
                                
                                [cellobj setFrame:CGRectMake(cellobjframe.frame.origin.x-40, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                        }
                        else if (SCREEN_HEIGHT==736)
                        {
                            if (index<=5)
                            {
                                [cellobj setFrame:CGRectMake(175, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                            else
                            {
                                [cellobj setFrame:CGRectMake(cellobjframe.frame.origin.x-30, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                        }
                        else if (SCREEN_HEIGHT==667)
                        {
                            if (index<=5)
                            {
                                [cellobj setFrame:CGRectMake(155, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                            else
                            {
                                
                                [cellobj setFrame:CGRectMake(cellobjframe.frame.origin.x-40, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                        }
                        else if (SCREEN_HEIGHT==568)
                        {
                            if (index<=4)
                            {
                                [cellobj setFrame:CGRectMake(130, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                            else
                            {
                                
                              //  NSLog(@"%f",cellobjframe.frame.origin.x);
                                [cellobj setFrame:CGRectMake(cellobjframe.frame.origin.x, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }
                        }
                    }
                }];
                for (UIView *i in cellobj.contentView.subviews)
                {
                    if([i isKindOfClass:[UIImageView class]])
                    {
                        UIImageView *image=(UIImageView *)i;
                        image.layer.borderWidth = 2;
                        image.layer.borderColor = [UIColor orangeColor].CGColor;
                    }
                }
                [_collectionView bringSubviewToFront:cellobj];
            }
        }
    }
    _collectionView.scrollEnabled=FALSE;
    selectedId=userId;
    SelectedCollectionTag=_collectionView.tag;
    if (SelectedCollectionTag!=0)
    {
        int tag=(int)_collectionView.tag ;
        int i=tag-1;
        [self loadCollectionView:i];
    }
}
// ----------------------------------------------------------------------------------------------------
// actionOnExtentedScrollView:

-(void)loadCollectionView:(int)i
{
    __block int index=i;
    isUserSelect=true;
    nextCollectionTag=i;
    [self UserCollectionArray:nextCollectionTag];
    NSIndexPath *pathindex = [NSIndexPath indexPathForRow:nextCollectionTag inSection:0];
    UITableViewCell *cell=[_tblUser cellForRowAtIndexPath:pathindex];
    for (UIView *i in cell.subviews)
    {
        if([i isKindOfClass:[UICollectionView class]])
        {
            UICollectionView *objCollection = (UICollectionView *)i;
            NSMutableArray *secUserList=[[NSMutableArray alloc]init];
            NSMutableArray *RelocateItemList=[[NSMutableArray alloc]init];
            for (NSUInteger i = 0; i < [combinationArr count]; i++)
            {
                NSArray *objectsArr = [combinationArr objectAtIndex:i];
                
                if ([objectsArr count]>0)
                {
                    if (![RelocateItemList containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                    {
                        [RelocateItemList addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                    }
                    if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                    {
                        if (![secUserList containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                        {
                            [secUserList addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                        }
                    }
                }
            }
            int position=0;
       //     NSLog(@"%lu",(unsigned long)CollectionViewArr.count);
            if ([secUserList count]>=1)
            {
                for (int j = 0 ; j < secUserList.count; j++)
                {
                    for (int i = 0 ; i < RelocateItemList.count; i++)
                    {
                        if ([[secUserList objectAtIndex:j] isEqualToNumber:[RelocateItemList objectAtIndex:i]])
                        {
                            position=i;
                            break;
                        }
                    }
                    NSIndexPath *path = [NSIndexPath indexPathForRow:position inSection:0];
                    UICollectionViewCell *cell = [objCollection cellForItemAtIndexPath: path];
                    for (UIView *i in cell.contentView.subviews)
                    {
                        if([i isKindOfClass:[UILabel class]])
                        {
                            UILabel *lbl=(UILabel *)i;
                            lbl.hidden=YES;
                        }
                    }
                    [UIView animateWithDuration:2 animations:^{
                        [cell setFrame:CGRectMake(cell.frame.origin.x,cell.frame.origin.y+80, cell.frame.size.width, cell.frame.size.height)];
                        for (UIView *i in cell.contentView.subviews)
                        {
                            if([i isKindOfClass:[UIImageView class]])
                            {
                                UIImageView *img=(UIImageView *)i;
                                img.alpha=0.0;
                            }
                        }
                    }];
                }
            }
            
            if ([CollectionViewArr count]<5 && [CollectionViewArr count]!=[RelocateItemList count])
            {
                if ([CollectionViewArr count]==1)
                {
                    for (int i = 0 ; i < RelocateItemList.count; i++)
                    {
                        if ([[CollectionViewArr objectAtIndex:0] isEqualToNumber:[RelocateItemList objectAtIndex:i]])
                        {
                            position=i;
                            break;
                        }
                    }
                    NSIndexPath *pathindex = [NSIndexPath indexPathForRow:position inSection:0];
                    UICollectionViewCell *cellobj = [objCollection cellForItemAtIndexPath: pathindex];
      //              NSLog(@"%lu",(unsigned long)[objCollection.visibleCells count]);
                    if ([objCollection.visibleCells containsObject:cellobj])
                    {
         //               NSLog(@"%lu",(unsigned long)cellobj.frame.origin.y);
                        if (cellobj.frame.origin.y>40)
                        {
                            [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                            for (UIView *i in cellobj.contentView.subviews)
                            {
                                if([i isKindOfClass:[UIImageView class]])
                                {
                                    UIImageView *img=(UIImageView *)i;
                                    img.alpha=1.0;
                                }
                                if([i isKindOfClass:[UILabel class]])
                                {
                                    UILabel *lbl=(UILabel *)i;
                                    lbl.hidden=NO;
                                }
                            }
                        }
                        else
                        {
                            [UIView animateWithDuration:3 animations:^{
                                [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-10, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                            }];
                        }
                    }
                }
                if ([CollectionViewArr count]==2)
                {
                    for (int j=0;j<[CollectionViewArr count];j++)
                    {
                        for (int i = 0 ; i < RelocateItemList.count; i++)
                        {
                            if ([[CollectionViewArr objectAtIndex:j] isEqualToNumber:[RelocateItemList objectAtIndex:i]])
                            {
                                position=i;
                                break;
                            }
                        }
                        NSIndexPath *pathindex = [NSIndexPath indexPathForRow:position inSection:0];
                        UICollectionViewCell *cellobj = [objCollection cellForItemAtIndexPath: pathindex];
                        if (j==0)
                        {
                            if ([objCollection.visibleCells containsObject:cellobj])
                            {
                                if (cellobj.frame.origin.y>40)
                                {
                                    [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    for (UIView *i in cellobj.contentView.subviews)
                                    {
                                        if([i isKindOfClass:[UIImageView class]])
                                        {
                                            UIImageView *img=(UIImageView *)i;
                                            img.alpha=1.0;
                                        }
                                        if([i isKindOfClass:[UILabel class]])
                                        {
                                            UILabel *lbl=(UILabel *)i;
                                            lbl.hidden=NO;
                                        }
                                    }
                                }
                                else
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                        }
                        else
                        {
                            if ([objCollection.visibleCells containsObject:cellobj])
                            {
                                if (cellobj.frame.origin.y>40)
                                {
                                    [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x+60,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    for (UIView *i in cellobj.contentView.subviews)
                                    {
                                        if([i isKindOfClass:[UIImageView class]])
                                        {
                                            UIImageView *img=(UIImageView *)i;
                                            img.alpha=1.0;
                                        }
                                        if([i isKindOfClass:[UILabel class]])
                                        {
                                            UILabel *lbl=(UILabel *)i;
                                            lbl.hidden=NO;
                                        }
                                    }
                                }
                                else
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x+60, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                            
                        }
                    }
                }
                if ([CollectionViewArr count]==3)
                {
                    for (int j=0;j<[CollectionViewArr count];j++)
                    {
                        for (int i = 0 ; i < RelocateItemList.count; i++)
                        {
                            if ([[CollectionViewArr objectAtIndex:j] isEqualToNumber:[RelocateItemList objectAtIndex:i]])
                            {
                                position=i;
                                break;
                            }
                        }
                        NSIndexPath *pathindex = [NSIndexPath indexPathForRow:position inSection:0];
                        UICollectionViewCell *cellobj = [objCollection cellForItemAtIndexPath: pathindex];
                        if (j==0)
                        {
                            if (cellobj.frame.origin.y>40)
                            {
                                [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-60,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                for (UIView *i in cellobj.contentView.subviews)
                                {
                                    if([i isKindOfClass:[UIImageView class]])
                                    {
                                        UIImageView *img=(UIImageView *)i;
                                        img.alpha=1.0;
                                    }
                                    if([i isKindOfClass:[UILabel class]])
                                    {
                                        UILabel *lbl=(UILabel *)i;
                                        lbl.hidden=NO;
                                    }
                                }
                            }
                            else
                            {
                                if ([objCollection.visibleCells containsObject:cellobj])
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-60, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                            
                        }
                        else if (j==1)
                        {
                            if (cellobj.frame.origin.y>40)
                            {
                                [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                for (UIView *i in cellobj.contentView.subviews)
                                {
                                    if([i isKindOfClass:[UIImageView class]])
                                    {
                                        UIImageView *img=(UIImageView *)i;
                                        img.alpha=1.0;
                                    }
                                    if([i isKindOfClass:[UILabel class]])
                                    {
                                        UILabel *lbl=(UILabel *)i;
                                        lbl.hidden=NO;
                                    }
                                }
                            }
                            else
                            {
                                if ([objCollection.visibleCells containsObject:cellobj])
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                        }
                        else
                        {
                            if (cellobj.frame.origin.y>40)
                            {
                                [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x+60,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                for (UIView *i in cellobj.contentView.subviews)
                                {
                                    if([i isKindOfClass:[UIImageView class]])
                                    {
                                        UIImageView *img=(UIImageView *)i;
                                        img.alpha=1.0;
                                    }
                                    if([i isKindOfClass:[UILabel class]])
                                    {
                                        UILabel *lbl=(UILabel *)i;
                                        lbl.hidden=NO;
                                    }
                                }
                            }
                            else
                            {
                                if ([objCollection.visibleCells containsObject:cellobj])
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x+60, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                                
                            }
                        }
                    }
                }
                if ([CollectionViewArr count]==4)
                {
                    for (int j=0;j<[CollectionViewArr count];j++)
                    {
                        for (int i = 0 ; i < RelocateItemList.count; i++)
                        {
                            if ([[CollectionViewArr objectAtIndex:j] isEqualToNumber:[RelocateItemList objectAtIndex:i]])
                            {
                                position=i;
                                break;
                            }
                        }
                        NSIndexPath *pathindex = [NSIndexPath indexPathForRow:position inSection:0];
                        UICollectionViewCell *cellobj = [objCollection cellForItemAtIndexPath: pathindex];
                        if (j==0)
                        {
                            if (cellobj.frame.origin.y>40)
                            {
                                [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-120,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                for (UIView *i in cellobj.contentView.subviews)
                                {
                                    if([i isKindOfClass:[UIImageView class]])
                                    {
                                        UIImageView *img=(UIImageView *)i;
                                        img.alpha=1.0;
                                    }
                                    if([i isKindOfClass:[UILabel class]])
                                    {
                                        UILabel *lbl=(UILabel *)i;
                                        lbl.hidden=NO;
                                    }
                                }
                            }
                            else
                            {
                                if ([objCollection.visibleCells containsObject:cellobj])
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-120, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                        }
                        else if (j==1)
                        {
                            if ([objCollection.visibleCells containsObject:cellobj])
                            {
                                if (cellobj.frame.origin.y>40)
                                {
                                    [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-60,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    for (UIView *i in cellobj.contentView.subviews)
                                    {
                                        if([i isKindOfClass:[UIImageView class]])
                                        {
                                            UIImageView *img=(UIImageView *)i;
                                            img.alpha=1.0;
                                        }
                                        if([i isKindOfClass:[UILabel class]])
                                        {
                                            UILabel *lbl=(UILabel *)i;
                                            lbl.hidden=NO;
                                        }
                                    }
                                }
                                else
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x-60, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                        }
                        else if (j==2)
                        {
                            if ([objCollection.visibleCells containsObject:cellobj])
                            {
                                if (cellobj.frame.origin.y>40)
                                {
                                    [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    for (UIView *i in cellobj.contentView.subviews)
                                    {
                                        if([i isKindOfClass:[UIImageView class]])
                                        {
                                            UIImageView *img=(UIImageView *)i;
                                            img.alpha=1.0;
                                        }
                                        if([i isKindOfClass:[UILabel class]])
                                        {
                                            UILabel *lbl=(UILabel *)i;
                                            lbl.hidden=NO;
                                        }
                                    }
                                }
                                else
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                        }
                        else
                        {
                            if ([objCollection.visibleCells containsObject:cellobj])
                            {
                                if (cellobj.frame.origin.y>40)
                                {
                                    [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x+60,cellobj.frame.origin.y-80, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    for (UIView *i in cellobj.contentView.subviews)
                                    {
                                        if([i isKindOfClass:[UIImageView class]])
                                        {
                                            UIImageView *img=(UIImageView *)i;
                                            img.alpha=1.0;
                                        }
                                        if([i isKindOfClass:[UILabel class]])
                                        {
                                            UILabel *lbl=(UILabel *)i;
                                            lbl.hidden=NO;
                                        }
                                    }
                                }
                                else
                                {
                                    [UIView animateWithDuration:5 animations:^{
                                        [cellobj setFrame:CGRectMake(self.lblDegree.frame.origin.x+60, cellobj.frame.origin.y, cellobj.frame.size.width, cellobj.frame.size.height)];
                                    }];
                                }
                            }
                        }
                    }
                }
                objCollection.scrollEnabled=FALSE;
            }
            if (index>0)
            {
                index--;
                [self loadCollectionView:index];
            }
            else
                isUserSelect=false;
        }
    }
}

- (void)actionOnExtentedScrollView:(UITapGestureRecognizer *)gesture {
    // Add in the array
    //This is predicate for hide only current extended view
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %ld && %K == %ld", kKeySubViewTag, gesture.view.tag, kKeyDegreeCombination, gesture.view.superview.tag];
    //This is predicate for hide all exteded view
    //  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %ld", kKeyDegreeCombination, gesture.view.tag];
    NSArray *filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
    if ([filteredArr count] > 0) {
        NSDictionary *dict = [filteredArr objectAtIndex:0];
        UIImageView *imgView = [dict objectForKey:kKeyTappedImage];
        imgView.hidden = NO;
        UIView *enlargeView = [dict objectForKey:kKeyAddedView];
        enlargeView.hidden = YES;
    }
    
    isDegreeUserSelected=NO;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionToGetDegreeOfUser:

- (void)actionToGetDegreeOfUser:(UITapGestureRecognizer *)gesture {
    //#error work here
    
    //Here first hide all degree view
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %ld", kKeyDegreeCombination, tappedImgView.superview.tag];
    NSArray *filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
    
    // Close other views
    for (NSUInteger a = 0; a < [filteredArr count]; a++) {
        NSDictionary *dict = [filteredArr objectAtIndex:a];
        UIImageView *imgView = [dict objectForKey:kKeyTappedImage];
        imgView.hidden = NO;
        UIView *addedSupView = [dict objectForKey:kKeyAddedView];
        addedSupView.hidden = YES;
    }
    SRCustomImageView *imgView = (SRCustomImageView *)gesture.view;
    //NSDictionary *userDict = imgView.object;
    
    //First select tapped user and store in tappedImgView
    tappedImgView = (SRCustomImageView *)gesture.view;
    
    
    if (imgView.isContainDegree)
    {
        NSDictionary *combinationDict = [profileDict objectForKey:kKeyDegreeCombination];
        //Commited because getting crash here when we try to remove null vale of array
        //combinationDict = [SRModalClass removeNullValuesFromDict:combinationDict];
        
        NSArray *combinationArr = [combinationDict objectForKey:[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree ]];
        NSArray *selectedCombination =[NSArray arrayWithArray:[combinationArr objectAtIndex:gesture.view.superview.tag]];
        
        int index =(int)[selectedCombination indexOfObject:[NSNumber numberWithInteger:imgView.repeatUserId]];
        
        [degreeUserList removeAllObjects];
        //Enumrate array for find same user present to next next index
        [combinationArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSArray *tempObj=(NSArray*)obj;
            long int indxUserId=[[tempObj objectAtIndex:index] integerValue];
            long int searchId=imgView.repeatUserId;
            
            if(indxUserId ==searchId)
            {
                NSDictionary *matchUserDic =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[[tempObj objectAtIndex:index-1] integerValue]],kKeyId,[NSString stringWithFormat:@"%lu",(unsigned long)idx],kKeyIndex, nil];
                
                if (![[degreeUserList valueForKey:kKeyId] containsObject:[NSNumber numberWithInteger:[[tempObj objectAtIndex:index-1] integerValue]]]) {
                    
                    [degreeUserList addObject:matchUserDic];
                }
            }
        }];
        
        // Add the scroll view
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [layout setSectionInset:UIEdgeInsetsMake(0, 10, 0, 10)];
        CGRect frame;
        if (SCREEN_WIDTH == 414) {
            frame=CGRectMake(97, tappedImgView.frame.origin.y, 220, 64);
        }
        else if (SCREEN_WIDTH == 375){
            frame=CGRectMake(77, tappedImgView.frame.origin.y, 220, 64);
        }
        else
            frame=CGRectMake(50, tappedImgView.frame.origin.y, 220, 64);
        _collectionView=[[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
        _collectionView.layer.cornerRadius = 8.0;
        _collectionView.layer.masksToBounds = YES;
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        _collectionView.alwaysBounceVertical = NO;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        //[self.scrollView addSubview:_collectionView];
        //#error work from here
        if (degreeUserList.count>1)
        {
            // check here  scrollview already in list or not
//            NSLog(@"%ld",tappedImgView.superview.tag);
//            NSLog(@"%ld",tappedImgView.tag);
            predicate = [NSPredicate predicateWithFormat:@"%K == %ld AND %K == %ld", kKeyDegreeCombination, tappedImgView.superview.tag,kKeySubViewTag, tappedImgView.tag];
            filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
            
            if ([filteredArr count] == 0)
            {
                // Get the array of combination first
                NSArray *objectsArr = degreeUserList;
                CGRect innerFrame = CGRectZero;
                for (NSUInteger j = 0; j < [objectsArr count]; j++) {
                    NSNumber *userID = [(NSDictionary *)[objectsArr objectAtIndex:j] valueForKey:kKeyId];
                    if ([objectsArr count] < 4) {
                        if (j == 0) {
                            NSInteger count = 0;
                            if ([objectsArr count] > 1) {
                                count = 4 * (objectsArr.count - 1);
                            }
                            CGFloat value = objectsArr.count * 60;
                            value = value + count;
                            value = value / 2;
                            CGFloat x = (_collectionView.frame.size.width / 2) - value;
                            innerFrame = CGRectMake(x, 2, 60, 60);
                        }
                        else
                            innerFrame.origin.x = innerFrame.origin.x + innerFrame.size.width + 4;
                    }
                    else {
                        if (j == 0) {
                            innerFrame = CGRectMake(8, 2, 60, 60);
                        }
                        else
                            innerFrame.origin.x = innerFrame.origin.x + innerFrame.size.width + 4;
                    }
                    
                    SRCustomImageView *imageView = [[SRCustomImageView alloc]initWithFrame:innerFrame];
                    imageView.contentMode = UIViewContentModeScaleAspectFill;
                    imageView.layer.cornerRadius = imageView.frame.size.width / 2.0;
                    imageView.layer.masksToBounds = YES;
                    imageView.clipsToBounds = YES;
                    imageView.backgroundColor = [UIColor clearColor];
                    imageView.userInteractionEnabled = YES;
                    imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    imageView.isExtended = YES;
                    imageView.tag =userID.integerValue;
                    
                    // Add tapGesture On ImageView
                    
                    UITapGestureRecognizer *tapGestureOnImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showNewDegreeConnection:)];
                    [imageView addGestureRecognizer:tapGestureOnImage];
                    
                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionOnExtentedScrollView:)];
                    [_collectionView addGestureRecognizer:tapGesture];
                    
                    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
                    longPressGesture.minimumPressDuration = 0.2f;
                    [imageView addGestureRecognizer:longPressGesture];
                    
                    NSArray *degreeUsers = [profileDict objectForKey:kKeyDegreeUsers];
                    NSString *userIdStr = [NSString stringWithFormat:@"%@", userID];
                    predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userIdStr];
                    NSArray *fileterdArr = [degreeUsers filteredArrayUsingPredicate:predicate];
                    
                    if ([fileterdArr count] > 0) {
                        NSDictionary *userDict = [fileterdArr objectAtIndex:0];
                        imageView.tag = [[userDict objectForKey:kKeyId]integerValue];
                        [imageView setDictionaryValue:userDict];
                        
                        id mediaDict = [userDict objectForKey:kKeyProfileImage];
                        
                        if ([mediaDict isKindOfClass:[NSDictionary class]] && [mediaDict objectForKey:kKeyImageName] != nil) {
                            // Get result
                            
                            if ([mediaDict objectForKey:kKeyImageName] != nil) {
                                NSString *imageName = [mediaDict objectForKey:kKeyImageName];
                                
                                if ([imageName length] > 0) {
                                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                                    [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                                }else
                                    imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                            }else
                                imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                            
                        }
                        else
                            imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    }
                    
                    // Add to scroll view
                    //[_collectionView addSubview:imageView];
                    if ([objectsArr count] >= 4) {
                        _collectionView.contentSize = CGSizeMake(innerFrame.size.width + innerFrame.origin.x + 5, 0);
                    }
                }
                
                // Add to superview
                _collectionView.backgroundColor = [UIColor colorWithRed:48 / 255.0 green:46 / 255.0 blue:44 / 255.0 alpha:1.0];
                _collectionView.tag = tappedImgView.tag;
                
                [tappedImgView.superview addSubview:_collectionView];
                
                // Add in the extendedlist array
                predicate = [NSPredicate predicateWithFormat:@"%K == %ld AND %K == %ld", kKeyDegreeCombination, tappedImgView.superview.tag,kKeySubViewTag, tappedImgView.tag];
                filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
                if ([filteredArr count] == 0) {
                    tappedImgView.hidden = YES;
                    NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionary];
                    [mutatedDict setObject:[NSNumber numberWithInteger:tappedImgView.superview.tag] forKey:kKeyDegreeCombination];
                    [mutatedDict setObject:tappedImgView forKey:kKeyTappedImage];
                    [mutatedDict setObject:_collectionView forKey:kKeyAddedView];
                    [mutatedDict setObject:[NSNumber numberWithInteger:tappedImgView.tag] forKey:kKeySubViewTag];
                    [extendedListArr addObject:mutatedDict];
                }
                else {
                    NSMutableDictionary *dict = [filteredArr objectAtIndex:0];
                    if ([[dict objectForKey:kKeyDegreeCombination]integerValue] == tappedImgView.superview.tag && [[dict objectForKey:kKeySubViewTag]integerValue] != tappedImgView.tag) {
                        NSMutableDictionary *mutatedDict = [NSMutableDictionary dictionary];
                        [mutatedDict setObject:[NSNumber numberWithInteger:tappedImgView.superview.tag] forKey:kKeyDegreeCombination];
                        [mutatedDict setObject:tappedImgView forKey:kKeyTappedImage];
                        [mutatedDict setObject:_collectionView forKey:kKeyAddedView];
                        [mutatedDict setObject:[NSNumber numberWithInteger:tappedImgView.tag] forKey:kKeySubViewTag];
                        [extendedListArr addObject:mutatedDict];
                    }
                }
                
            }else
            {
                NSDictionary *dict = [filteredArr objectAtIndex:0];
                UIImageView *imgView = [dict objectForKey:kKeyTappedImage];
                imgView.hidden = YES;
                UIView *enlargeView = [dict objectForKey:kKeyAddedView];
                enlargeView.hidden = NO;
                
            }
        }
    }
    return;
    
    // Which user image is tapped display degree relation between him and me
    predicate = [NSPredicate predicateWithFormat:@"%K = %ld && %K = %ld", kKeySubViewTag, gesture.view.tag, kKeyDegreeCombination, gesture.view.superview.tag];
    filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
    if ([filteredArr count] > 0) {
        NSDictionary *dict = [filteredArr objectAtIndex:0];
        UIImageView *imgView = [dict objectForKey:kKeyTappedImage];
        imgView.hidden = YES;
        UIView *enlargeView = [dict objectForKey:kKeyAddedView];
        enlargeView.hidden = NO;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %ld", kKeyDegreeCombination, tappedImgView.superview.tag];
        NSArray *filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
        
        // Close other views
        for (NSUInteger a = 0; a < [filteredArr count]; a++) {
            NSDictionary *dict = [filteredArr objectAtIndex:a];
            UIImageView *imgView = [dict objectForKey:kKeyTappedImage];
            
            if (imgView.tag != gesture.view.tag) {
                imgView.hidden = NO;
                UIView *addedSupView = [dict objectForKey:kKeyAddedView];
                addedSupView.hidden = YES;
            }
        }
    }
    else {
        tappedImgView = (SRCustomImageView *)gesture.view;
        // NSDictionary *dict = tappedImgView.object;
        
        self.indicatorView.hidden = NO;
        [self.indicatorView startAnimating];
        [self.view bringSubviewToFront:self.indicatorView];
        self.view.userInteractionEnabled = NO;
    }
    
    // Call profile display method
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [_collectionView visibleCells]) {
        NSIndexPath *indexPath = [_collectionView indexPathForCell:cell];
       // NSLog(@"%@",indexPath);
    }
}
// ---------------------------------------------------------------------------------------------------------------------------------
// handleLongPressGestures:

- (void)handleLongPressGestures:(UIGestureRecognizer *)gesture
{
    UICollectionViewCell *cell=(UICollectionViewCell *)gesture.view;
    for (UIView *subview in cell.contentView.subviews)
    {
        if ([subview isKindOfClass:[SRCustomImageView class]])
        {
            SRCustomImageView *imgView = (SRCustomImageView *)subview;
            NSDictionary *userDict = imgView.object;
            
            chatDict=[[NSMutableDictionary alloc]initWithDictionary:userDict];
            
            NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRMapWindowView" owner:self options:nil];
            if ([nibObjects count] > 0)
            {
                SRMapWindowView *profileView = (SRMapWindowView *)[nibObjects objectAtIndex:1];
                profileView.bgImage.layer.cornerRadius = 8.0;
                profileView.bgImage.layer.masksToBounds = YES;
                
                [profileView.pingButton.layer setShadowOffset:CGSizeMake(5, 5)];
                [profileView.pingButton.layer setShadowColor:[[UIColor blackColor] CGColor]];
                [profileView.pingButton.layer setShadowOpacity:8.5];
                [profileView.pingButton addTarget:self action:@selector(openChatView) forControlEvents:UIControlEventTouchUpInside];
                profileView.userProfileImage.object = userDict;
                
                // Profile image
                UIBezierPath *maskPath;
                maskPath = [UIBezierPath bezierPathWithRoundedRect:profileView.userProfileImage.bounds
                                                 byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                                       cornerRadii:CGSizeMake(8.0, 8.0)];
                
                CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
                maskLayer.frame = profileView.userProfileImage.bounds;
                maskLayer.path = maskPath.CGPath;
                profileView.userProfileImage.layer.mask = maskLayer;
                profileView.userProfileImage.image = imgView.image;
                
                // Name
                profileView.lblProfileName.text = [NSString stringWithFormat:@"%@ %@.", [userDict objectForKey:kKeyFirstName], [[userDict objectForKey:kKeyLastName] substringWithRange:NSMakeRange(0, 1)]];
                
                // Occupation
                profileView.lblOccupation.text = [userDict objectForKey:kKeyOccupation];
                
                // Distance
                double distance = [[userDict objectForKey:kKeyDistance]doubleValue];
                profileView.lblDistance.text = [NSString stringWithFormat:@"%.1f", distance];
                
                // Dating image
                if ([[userDict objectForKey:kKeySetting]isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *settingDict = [userDict objectForKey:kKeySetting];
                    if ([[settingDict objectForKey:kKeyOpenForDating]isEqualToString:@"1"]) {
                        profileView.datingImgView.image = [UIImage imageNamed:@"dating-orange-40px.png"];
                    }
                    else {
                        profileView.datingImgView.image = [UIImage imageNamed:@"dating-black-30px.png"];
                    }
                }
                else {
                    profileView.datingImgView.image = [UIImage imageNamed:@"dating-black-30px.png"];
                }
                
                // Degree
                profileView.lblDegree.text = [NSString stringWithFormat:@"%@", [[userDict objectForKey:kKeyDegree] stringValue]];
                profileView.lblDegree.layer.cornerRadius = profileView.lblDegree.frame.size.width / 2;
                
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionOnProfileImage:)];
                [profileView addGestureRecognizer:tapGesture];
                
                // Add view on the center
                CGRect frame = profileView.frame;
                if (profileDetailView == nil) {
                    //self.scrollView.frame.size.width, self.scrollView.frame.size.height
                    profileDetailView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,self.scrollView.frame.size.width,self.scrollView.frame.size.height)];
                    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
                        profileDetailView.backgroundColor = [UIColor clearColor];
                        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
                        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                        blurEffectView.frame = profileDetailView.bounds;
                        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                        [profileDetailView addSubview:blurEffectView];
                    }
                    else {
                        [profileDetailView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                    }
                    
                    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionToHideProfileView:)];
                    [profileDetailView addGestureRecognizer:tapGesture];
                }
                [self.scrollView addSubview:profileDetailView];
                //                   if (imgView.isExtended)
                //                       [imgView.superview.superview addSubview:profileDetailView];
                //                   else
                //                       [imgView.superview addSubview:profileDetailView];
                
                frame.origin.x = self.scrollView.frame.size.width / 2 - 65;
                frame.origin.y = self.scrollView.frame.size.height / 2 - 100;
                profileView.frame = frame;
                [profileDetailView addSubview:profileView];
                [profileDetailView bringSubviewToFront:profileView];
            }
        }
    }
    isDegreeUserSelected=NO;
}
// ---------------------------------------------------------------------------------------------------------------------------------
// openChatView:
-(void)openChatView
{
    // Remove form superview
    [profileDetailView removeFromSuperview];
    
    SRChatViewController *chatCon = [[SRChatViewController alloc]initWithNibName:nil bundle:nil server:server inObjectInfo:chatDict];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:chatCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;
}
// ---------------------------------------------------------------------------------------------------------------------------------
// actionToHideProfileView:

- (void)actionToHideProfileView:(UITapGestureRecognizer *)gesture {
    // Remove form superview
    [profileDetailView removeFromSuperview];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnProfileImage:

- (void)actionOnProfileImage:(UITapGestureRecognizer *)gesture {
    
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnScrollTap:

- (void)actionOnScrollTap:(UITapGestureRecognizer *)gesture {
    // Hide all degree relation between him and me
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %ld", kKeyDegreeCombination, gesture.view.tag];
    NSArray *filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
    
    // Close other views
    for (NSUInteger a = 0; a < [filteredArr count]; a++) {
        NSDictionary *dict = [filteredArr objectAtIndex:a];
        UIImageView *imgView = [dict objectForKey:kKeyTappedImage];
        imgView.hidden = NO;
        UIView *addedSupView = [dict objectForKey:kKeyAddedView];
        addedSupView.hidden = YES;
    }
}

-(void)setUpCurvMenu
{
    // Set Up For Animation Button on RadarDiscovery
    overlay = [[GHContextMenuView alloc] init];
    overlay.isDegreeView=YES;
    overlay.userDrgree=selectedDegree;
    overlay.dataSource = self;
    overlay.delegate = self;
    overlay.selectedIndex = 0;
    [overlay setUpDegreeView];
    [overlay setUpViewRadius:120];
    UITapGestureRecognizer *_longPressRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:overlay action:@selector(tapBtnDetected:)];
    [self.btnDegree addGestureRecognizer:_longPressRecognizer];
    return;
    if (!degreeBtnView)
    {
        degreeBtnView = [[SRDegreeBtnView alloc]init];
        degreeBtnView.frame = CGRectMake(self.lblDegree.center.x-(320/2), self.lblDegree.frame.origin.y-120.0f, 320, 120.0f);
        degreeBtnView.delegate=self;
        // degreeBtnView.backgroundColor=[UIColor colorWithRed:0.3 green:0.4 blue:0.3 alpha:0.3];
        [degreeBtnView setUpView];
        [self.view addSubview:degreeBtnView];
        buttonArray = [[NSArray alloc] initWithObjects:degreeBtnView.degreeBtn1, degreeBtnView.degreeBtn2, degreeBtnView.degreeBtn3, degreeBtnView.degreeBtn4, degreeBtnView.degreeBtn5, degreeBtnView.degreeBtn6, nil];
        degreeBtnView.hidden=YES;
    }
    [self.view bringSubviewToFront:self.lblDegree];
    [self.view bringSubviewToFront:self.btnDegree];
}

-(void)setUpCurvShadow
{
    //    CGMutablePathRef arc = CGPathCreateMutable();
    //    CGPathMoveToPoint(arc, NULL,
    //                      startPoint.x, startPoint.y);
    //    CGPathAddArc(arc, NULL,
    //                 centerPoint.x, centerPoint.y,
    //                 radius,
    //                 startAngle,
    //                 endAngle,
    //                 YES);
    //    CGFloat lineWidth = 10.0;
    //    CGPathRef strokedArc =
    //    CGPathCreateCopyByStrokingPath(arc, NULL,
    //                                   lineWidth,
    //                                   kCGLineCapButt,
    //                                   kCGLineJoinMiter, // the default
    //                                   10);
    //    CGContextRef c = UIGraphicsGetCurrentContext();
    //    CGContextAddPath(c, strokedArc);
    //    CGContextSetFillColorWithColor(c, [UIColor lightGrayColor].CGColor);
    //    CGContextSetStrokeColorWithColor(c, [UIColor blackColor].CGColor);
    //    CGContextDrawPath(c, kCGPathFillStroke);
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   // NSLog(@"Scrolling Up");
}
-(void) longPressGestureRecognised:(UILongPressGestureRecognizer *)sender
{
    //UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = sender.state;
    
    CGPoint location = [sender locationInView: _collectionView];
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint: location];
    
    static UIView *snapshot = nil;
    static NSIndexPath *sourceIndexPath = nil;
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath: indexPath];
                
                // Take a snapshot of the selected item using helper method.
                snapshot = [self customSnapshotFromView: cell];
                
                // Add the snapshot as subview, centered at cell's centre.
                __block CGPoint centre = cell.center;
                snapshot.center = centre;
                snapshot.alpha = 0.0;
                [_collectionView addSubview: snapshot];
                [UIView animateWithDuration: 0.25 animations:^{
                    
                    // Offset for gesture location.
                    centre.y = location.y;
                    snapshot.center = centre;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    
                    // Fade out.
                    cell.alpha = 0.0;
                    
                } completion: ^(BOOL finished) {
                    cell.hidden = YES;
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint centre = snapshot.center;
            centre.y = location.y;
            //            snapshot.center = centre;
            
            //To set snapshotview frame when touch point changes  (according to move touch point in collectionview change snapshotview frame)
            CGPoint newCoord = [sender locationInView:_collectionView];
            float dX = newCoord.x - 30;
            float dY = newCoord.y - 30;
            snapshot.frame = CGRectMake(dX, dY, snapshot.frame.size.width, snapshot.frame.size.height);
            
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual: sourceIndexPath]) {
                // Update data source.
                [degreeUserList exchangeObjectAtIndex: indexPath.item withObjectAtIndex:sourceIndexPath.item];
                
                // Move the items.
                [_collectionView moveItemAtIndexPath: sourceIndexPath toIndexPath: indexPath];
                
                // And update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            NSMutableArray *visibleCellArray = [[NSMutableArray alloc] init];
            NSArray *visibleCells = [_collectionView indexPathsForVisibleItems];
            
            //Sorted IndexPath
            NSArray *sortedIndexPaths = [visibleCells sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                NSIndexPath *path1 = (NSIndexPath *)obj1;
                NSIndexPath *path2 = (NSIndexPath *)obj2;
                return [path1 compare:path2];
            }];
            for (NSIndexPath *indexPath in sortedIndexPaths) {
                [visibleCellArray addObject:[degreeUserList objectAtIndex:indexPath.row]];
            }
            
            //Get index of which user connection We have to change
            NSPredicate *predicate;
            if (visibleCellArray.count > 0) {
                NSString *userId= [[visibleCellArray objectAtIndex:1] valueForKey:kKeyId];
                predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userId];
            }
            else
            {
                predicate = [NSPredicate predicateWithFormat:@"%K == %ld", kKeyId, sender.view.tag];
            }
            
            
            NSArray *filteredArr = [degreeUserList filteredArrayUsingPredicate:predicate];
            if ([filteredArr count] > 0) {
                NSDictionary *selectUserDict = [filteredArr objectAtIndex:0];
                
                for (int i = 0 ; i < visibleCellArray.count; i++) {
                    if (![[selectUserDict valueForKey:kKeyId] isEqualToNumber:[[visibleCellArray objectAtIndex:i]valueForKey:kKeyId]]) {
                        NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:0];
                        UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath: path];
                        //[cell drainAway:3];
                        if (i==0)
                        {
                            CGRect basketTopFrame = cell.frame;
                            basketTopFrame.origin.x = 80;
                            [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{ cell.frame = basketTopFrame; } completion:^(BOOL finished){ }];
                        }
                        else
                        {
                            CGRect napkinBottomFrame = cell.frame;
                            napkinBottomFrame.origin.x = 80;
                            [UIView animateWithDuration:1.0 delay:0.0 options: UIViewAnimationOptionCurveEaseOut animations:^{ cell.frame = napkinBottomFrame; } completion:^(BOOL finished){/*done*/}];
                        }
                    }
                }
                NSDictionary *currentViewDic;
                //Get all combination of selected degree
                NSMutableDictionary *combinationDict = [profileDict objectForKey:kKeyDegreeCombination];
                
                NSMutableArray *combinationArr = [combinationDict objectForKey:[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]];
                
                //Get which combination  to exchange from our extend userlist
                predicate = [NSPredicate predicateWithFormat:@"%K == %ld && %K == %ld", kKeySubViewTag, tappedImgView.tag, kKeyDegreeCombination, sender.view.superview.superview.tag];
                
                filteredArr = [extendedListArr filteredArrayUsingPredicate:predicate];
                
                if ([filteredArr count] > 0)
                {
                    currentViewDic =[filteredArr objectAtIndex:0];
                    //Exchange position of user to show degree
                    if ([[selectUserDict valueForKey:kKeyIndex] integerValue] <= combinationArr.count && [[currentViewDic valueForKey:kKeyDegreeCombination] integerValue] <= combinationArr.count)
                    {
                        [combinationArr exchangeObjectAtIndex:[[selectUserDict valueForKey:kKeyIndex] integerValue] withObjectAtIndex:[[currentViewDic valueForKey:kKeyDegreeCombination] integerValue]];
                        
                        [combinationDict setObject:combinationArr forKey:[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]];
                        [profileDict setObject:combinationDict forKey:kKeyDegreeCombination];  //Set new degree combination
                        
                        isDegreeUserSelected=YES;           //User avoid scrollview inition position set
                        [extendedListArr removeAllObjects]; //Remove all store view to avoid mismatch hide view
                        //Call to show new combition
                        // Delay execution of my block for 10 seconds.
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            
                            [self displayCombination:[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]];
                        });
                        
                    }
                    
                }
            }
            
        }
            
        default: {
            // Clean up.
            UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath: sourceIndexPath];
            cell.hidden = NO;
            cell.alpha = 0.0;
            [UIView animateWithDuration: 0.25 animations: ^{
                
                //                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                
                // Undo fade out.
                cell.alpha = 1.0;
                
            }completion: ^(BOOL finished) {
                
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
                
            }];
            break;
        }
    }
}


-(UIView *) customSnapshotFromView:(UIView *)inputView
{
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage: image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}

/*
 - (void)handleLongPress:(UILongPressGestureRecognizer *)gr{
 switch(gr.state){
 case UIGestureRecognizerStateBegan:
 {
 NSIndexPath *selectedIndexPath = [_collectionView indexPathForItemAtPoint:[gr locationInView:_collectionView]];
 
 if(selectedIndexPath == nil) break;
 [_collectionView beginInteractiveMovementForItemAtIndexPath:selectedIndexPath];
 break;
 }
 case UIGestureRecognizerStateChanged:
 {
 [_collectionView updateInteractiveMovementTargetPosition:[gr locationInView:gr.view]];
 break;
 }
 case UIGestureRecognizerStateEnded:
 {
 [_collectionView endInteractiveMovement];
 break;
 }
 default:
 {
 [_collectionView cancelInteractiveMovement];
 break;
 }
 }
 }*/
#pragma mark
#pragma mark collectionView delegate Methods
#pragma mark
-(NSMutableArray *)UserCollectionArray:(NSInteger)index
{
    CollectionViewArr=[[NSMutableArray alloc]init];
    if (isUserSelect)
    {
        for (NSUInteger i = 0; i < [combinationArr count]; i++)
        {
            NSArray *objectsArr = [combinationArr objectAtIndex:i];
            if (SelectedCollectionTag==1)
            {
                if ([objectsArr count]>0)
                {
                    if ([[objectsArr objectAtIndex:SelectedCollectionTag]isEqualToNumber:[NSNumber numberWithInteger:selectedId]])
                    {
                        if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                        {
                            [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                        }
                    }
                }
            }
            else if (SelectedCollectionTag==2)
            {
                if ([objectsArr count]>0)
                {
                    if (nextCollectionTag==1)
                    {
                        if ([[objectsArr objectAtIndex:SelectedCollectionTag]isEqualToNumber:[NSNumber numberWithInteger:selectedId]])
                        {
                            if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                            {
                                [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                            }
                        }
                    }
                    else
                    {
                        for (int i=0;i<[CollectionArr0 count];i++)
                        {
                            int userid=[[CollectionArr0 objectAtIndex:i] intValue];
                            if ([[objectsArr objectAtIndex:nextCollectionTag+1]isEqualToNumber:[NSNumber numberWithInteger:userid]])
                            {
                                if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                                {
                                    [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                                }
                            }
                        }
                    }
                }
            }
            else if (SelectedCollectionTag==3)
            {
                if ([objectsArr count]>0)
                {
                    if (nextCollectionTag==2)
                    {
                        if ([[objectsArr objectAtIndex:SelectedCollectionTag]isEqualToNumber:[NSNumber numberWithInteger:selectedId]])
                        {
                            
                            if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                            {
                                [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                            }
                        }
                    }
                    else
                    {
                        for (int i=0;i<[CollectionArr0 count];i++)
                        {
                            int userid=[[CollectionArr0 objectAtIndex:i] intValue];
                            if ([[objectsArr objectAtIndex:nextCollectionTag+1]isEqualToNumber:[NSNumber numberWithInteger:userid]])
                            {
                                if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                                {
                                    [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                                }
                            }
                        }
                    }
                }
            }
            else if (SelectedCollectionTag==4)
            {
                if ([objectsArr count]>0)
                {
                    if (nextCollectionTag==3)
                    {
                        if ([[objectsArr objectAtIndex:SelectedCollectionTag]isEqualToNumber:[NSNumber numberWithInteger:selectedId]])
                        {
                            if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                            {
                                [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                            }
                        }
                    }
                    else
                    {
                        for (int i=0;i<[CollectionArr0 count];i++)
                        {
                            int userid=[[CollectionArr0 objectAtIndex:i] intValue];
                            if ([[objectsArr objectAtIndex:nextCollectionTag+1]isEqualToNumber:[NSNumber numberWithInteger:userid]])
                            {
                                if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:nextCollectionTag]])
                                {
                                    [CollectionViewArr addObject:[objectsArr objectAtIndex:nextCollectionTag]];
                                }
                            }
                        }
                    }
                }
            }
        }
        CollectionArr0=[[NSMutableArray alloc] init];
        CollectionArr0=CollectionViewArr;
    }
    else
    {
        for (NSUInteger i = 0; i < [combinationArr count]; i++)
        {
            NSArray *objectsArr = [combinationArr objectAtIndex:i];
            if ([objectsArr count]>0)
            {
                if (![CollectionViewArr containsObject:[objectsArr objectAtIndex:index]])
                {
                    [CollectionViewArr addObject:[objectsArr objectAtIndex:index]];
                }
            }
        }
    }
    return CollectionViewArr;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [CollectionViewArr count];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if ([CollectionViewArr count]<6)
    {
        NSInteger viewWidth = self.scrollView.frame.size.width;
        NSInteger totalCellWidth = 65 * [CollectionViewArr count];
        NSInteger totalSpacingWidth = [CollectionViewArr count];
        NSInteger leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2;
        NSInteger rightInset = leftInset;
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset);
    }
    else
        return UIEdgeInsetsMake(0,0,0,0);
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"cellIdentifier";
    UICollectionViewCell *cell;
    if (cell == nil)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        for(UIView *view in cell.contentView.subviews)
        {
            if ([view isKindOfClass:[UIImageView class]]) {
                [view removeFromSuperview];
            } else if ([view isKindOfClass:[UILabel class]]) {
                [view removeFromSuperview];
            }
        }
        SRCustomImageView *imageView = [[SRCustomImageView alloc]init];
        imageView.frame=CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y, 55, 55);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.layer.cornerRadius = imageView.frame.size.width / 2.0;
        imageView.layer.masksToBounds = YES;
        imageView.clipsToBounds = YES;
        imageView.backgroundColor = [UIColor clearColor];
        imageView.userInteractionEnabled = YES;
        imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        cell.layer.masksToBounds = YES;
        cell.clipsToBounds = YES;
        if (!isUserSelect)
        {
            [self UserCollectionArray:collectionView.tag];
        }
        if ([CollectionViewArr count]<6)
        {
            collectionView.scrollEnabled=NO;
        }
    //    NSLog(@"%lu",indexPath.row);
        if (indexPath.row<[CollectionViewArr count])
        {
            NSNumber *userID = [CollectionViewArr objectAtIndex:indexPath.row ];
            imageView.tag =userID.integerValue;
            cell.tag =userID.integerValue;
            NSArray *degreeUsers = [profileDict objectForKey:kKeyDegreeUsers];
            NSString *userIdStr = [NSString stringWithFormat:@"%@", userID];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userIdStr];
            NSArray *fileterdArr = [degreeUsers filteredArrayUsingPredicate:predicate];
            NSString *title;
            if ([fileterdArr count] > 0)
            {
                NSDictionary *userDict = [fileterdArr objectAtIndex:0];
                imageView.tag = [[userDict objectForKey:kKeyId]integerValue];
                [imageView setDictionaryValue:userDict];
                title = [userDict valueForKey:@"first_name"];
                id mediaDict = [userDict objectForKey:kKeyProfileImage];
                if ([mediaDict isKindOfClass:[NSDictionary class]] && [mediaDict objectForKey:kKeyImageName] != nil)
                {
                    // Get result
                    if ([mediaDict objectForKey:kKeyImageName] != nil)
                    {
                        NSString *imageName = [mediaDict objectForKey:kKeyImageName];
                        if ([imageName length] > 0)
                        {
                            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                            [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                        }else
                            imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                    }else
                        imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
                else
                    imageView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            }
            UITapGestureRecognizer *tapGestureOnImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showNewDegreeConnection:)];
            [cell addGestureRecognizer:tapGestureOnImage];// Add to scroll view
            UILongPressGestureRecognizer *longPress=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
            longPress.minimumPressDuration = 0.2f;
            [cell addGestureRecognizer:longPress];
            [cell.contentView addSubview:imageView];
            
            [cell.contentView addSubview:[self cellTitle:title indexPath:indexPath]];
        }
    }
    return cell;
}

-(UILabel *)cellTitle:(NSString *)name indexPath:(NSIndexPath *)indexPath
{
    UILabel *title = [[UILabel alloc] init];
    title.textColor = [UIColor whiteColor];
    title.text = name;
    float widthIs = [title.text boundingRectWithSize:title.frame.size options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{ NSFontAttributeName:title.font } context:nil].size.width;
    if (name.length<=5)
    {
        title.frame=CGRectMake(10,55, widthIs, 20);
        title.textAlignment=NSTextAlignmentCenter;
    }
    else if (name.length>5 &&name.length<=8 )
    {
        title.frame=CGRectMake(7,55, widthIs, 20);
    }
    else
    {
        title.frame=CGRectMake(0,55, widthIs, 20);
    }
    title.backgroundColor = [UIColor clearColor];
    [title setFont:[UIFont systemFontOfSize:10]];
    return title;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(60, 105);
}
-(void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    UICollectionViewLayoutAttributes *attributes = [cv layoutAttributesForItemAtIndexPath:indexPath];
    
    CGRect cellRect = attributes.frame;
    
    CGRect cellFrameInSuperview = [cv convertRect:cellRect toView:[cv superview]];
    
   // NSLog(@"%f",cellFrameInSuperview.origin.x);
}
#pragma mark
#pragma mark Action Methods
#pragma mark

// --------------------------------------------------------------------------------
// degreeBtnAction:

- (IBAction)degreeBtnAction:(id)sender
{
    
    self.lblDegree.text=degreeBtnView.isHidden?[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]:@"";
    self.lblDegree.backgroundColor =self.lblDegree.text.length!=0?[UIColor orangeColor] :[UIColor whiteColor];
    
    
    return;
    // COMMENT ACTION BY ASHWINI (This is the action defined for degreebtnclick by Pragati but commented from first)
    
    degreeBtnView .hidden =degreeBtnView.isHidden? NO :YES;
    
    [self.btnDegree setImage:degreeBtnView.isHidden?nil:[UIImage imageNamed:@"tabbar-logo-active.png"] forState:UIControlStateNormal];
    self.btnDegree.contentMode=UIViewContentModeScaleAspectFit;
    
    self.lblDegree.text=degreeBtnView.isHidden?[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]:@"";
    self.lblDegree.backgroundColor =degreeBtnView.isHidden?[UIColor orangeColor] :[UIColor whiteColor];
    
    //Change button apearance
    [self changeAppearance];        // Change button apearance
    
}
-(void)changeAppearance
{
    for (int i=1; i<=buttonArray.count; i++)
    {
        UIButton *dBtn = (UIButton *)[buttonArray objectAtIndex:i-1];
        if (selectedDegree>0 &&i>=userDegree)
        {
            if (i==selectedDegree)
            {
                dBtn.backgroundColor=[UIColor whiteColor];
                
            }else
            {
                dBtn.backgroundColor=[UIColor orangeColor];
            }
        }else
        {
            dBtn.backgroundColor=[UIColor lightGrayColor];
        }
    }
}
#pragma mark
#pragma mark Degree selection vew delegate
#pragma mark
-(void)selectedDegreeButton:(UIButton *)degreeBtn withDegree:(NSUInteger)degree
{
    
    if (degree) {
        
        if (degree<userDegree)
        {
            return;
        }
        self.lblNoPpl.hidden = YES;
        
        selectedDegree=degree;
        isUserSelect=false;
        [self displayCombination:[NSString stringWithFormat:@"%lu",(unsigned long)degree]];
        
        [self changeAppearance];
        
    }else
    {
        [self degreeBtnAction:nil];
    }
}

#pragma mark
#pragma mark GHContextMenu Delegate Method
#pragma mark

//------------------------------------------------------------------------------------------------------------------------
// numberOfMenuItems

- (NSInteger)numberOfMenuItems {
    return 6;
}


//------------------------------------------------------------------------------------------------------------
// labelForItemAtIndex: withSelectedIndex:
- (UILabel*) labelForItemAtIndex:(NSInteger) index
{
    
    UILabel *lbl=[[UILabel alloc]init];
    lbl.frame=CGRectMake(0, 0, 40, 40);
    lbl.font=[UIFont systemFontOfSize:16];
    
    switch (index) {
        case 0:
            lbl.text=[NSString stringWithFormat:@"%ld%@",index+1,@"\u00B0"];
            break;
            
        case 1:
            lbl.text=[NSString stringWithFormat:@"%ld%@", index+1,@"\u00B0"];
            break;
            
        case 2:
            lbl.text=[NSString stringWithFormat:@"%ld%@", index+1,@"\u00B0"];
            break;
        case 3:
            lbl.text=[NSString stringWithFormat:@"%ld%@", index+1,@"\u00B0"];
            break;
            
        case 4:
            lbl.text=[NSString stringWithFormat:@"%ld%@", index+1,@"\u00B0"];
            break;
            
        case 5:
            lbl.text=[NSString stringWithFormat:@"%ld%@", index+1,@"\u00B0"];
            break;
            
        default:
            break;
    }
    
    // Image name
    return lbl;
}
//------------------------------------------------------------------------------------------------------------------------
// didSelectItemAtIndex:

- (void)didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point {
    
    self.lblDegree.backgroundColor =[UIColor orangeColor];
    
    self.lblDegree.text=[NSString stringWithFormat:@" %lu%@",selectedIndex==-1?0:(unsigned long)selectedIndex+1, @"\u00B0"];
    
    selectedDegree=(selectedIndex==-1 &&userDegree==0)?0:selectedIndex+1;
    isUserSelect=false;
    [self displayCombination:[NSString stringWithFormat:@"%lu",(unsigned long)selectedDegree]];
    
    [self.btnDegree setImage:nil forState:UIControlStateNormal];
    [self.view bringSubviewToFront:self.btnDegree];
    
    return;
    
    switch (selectedIndex) {
        case 0:
        {
            
            [self.btnDegree setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnDegree];
            
            break;
        }
            
        case 1:
        {
            [self.btnDegree setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnDegree];
            
            break;
        }
            
        case 2:
        {
            [self.btnDegree setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnDegree];
            
            break;
        }
        case 3:
        {
            [self.btnDegree setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnDegree];
            
            break;
        }
            
        case 4:
        {
            [self.btnDegree setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnDegree];
            
            break;
        }
            
        case 5:
        {
            [self.btnDegree setImage:nil forState:UIControlStateNormal];
            [self.view bringSubviewToFront:self.btnDegree];
            
            break;
        }
            
        default:
            break;
    }
}

-(void)updateUserInfo:(NSMutableDictionary *)inProfile {
    [self.indicatorView stopAnimating];
    inProfile[kKeyDegree] =  [profileDict objectForKey:kKeyDegree];
    if (![[inProfile objectForKey:kKeyId] isEqualToString:[profileDict objectForKey:kKeyId]]) {
        self.view.userInteractionEnabled = YES;
    } else {
        if ([[profileDict objectForKey:kKeyDegree] isKindOfClass:[NSNumber class]]) {
            [profileDict addEntriesFromDictionary:[SRModalClass removeNullValuesFromDict:inProfile]];
            // [self setUpDegreesCombination];
            [self displayCombination:[[profileDict objectForKey:kKeyDegree] stringValue]];
            
        }
    }
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:

- (void)getProfileDetailsSucceed:(NSNotification *)inNotify {
    NSMutableDictionary *inProfile = [inNotify object];
    [self updateUserInfo:inProfile];
}

// --------------------------------------------------------------------------------
// getProfileDetailsFailed:

- (void)getProfileDetailsFailed:(NSNotification *)inNotify
{
    [SRModalClass showAlert:@"Request time out"];
}

@end
