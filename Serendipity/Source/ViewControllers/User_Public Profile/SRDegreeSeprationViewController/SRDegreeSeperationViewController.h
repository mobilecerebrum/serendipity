#import <UIKit/UIKit.h>
#import "SRCustomImageView.h"
#import "SRDegreeBtnView.h"
#import "SRProfileImageView.h"
#import "GHContextMenuView.h"
#import "UIImageView+WebCache.h"


@interface SRDegreeSeperationViewController : ParentViewController<GHContextOverlayViewDataSource, GHContextOverlayViewDelegate,SRDegreeChooseDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    // Instance Variables
    NSMutableDictionary *profileDict;
    NSMutableArray *extendedListArr, *CollectionViewArr,*CollectionArr0,*CollectionArr1,*CollectionArr2;
    SRCustomImageView *tappedImgView;
    UIView *profileDetailView;
    UICollectionView *_collectionView;
    // Server
    SRServerConnection *server;
    SRDegreeBtnView *degreeBtnView;
    NSArray *buttonArray,*combinationArr;
    NSTimer *myTimer;
    
    NSMutableDictionary *chatDict;
    
    NSUInteger         selectedDegree;
    NSUInteger         userDegree,selectedId, SelectedCollectionTag,nextCollectionTag;
    
    //Custom view for userDegree
    GHContextMenuView *overlay;
    BOOL isUserSelect;
}

// Prperties
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBckScroll;
@property (strong, nonatomic) IBOutlet SRProfileImageView *profileImgView;
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImgView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *lblDegree;
@property (strong, nonatomic) IBOutlet UIButton *btnDegree,*btnReset;
@property (strong, nonatomic) IBOutlet UILabel *lblNoPpl;
@property(strong,nonatomic)IBOutlet UITableView *tblUser;
// Other Prperties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark Action Method
#pragma mark

- (IBAction)degreeBtnAction:(id)sender;
-(IBAction)btnResetClicked:(id)sender;
@end
