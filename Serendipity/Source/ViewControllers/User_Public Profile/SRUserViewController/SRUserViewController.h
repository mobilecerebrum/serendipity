#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>

@interface SRUserViewController : ParentViewController<MFMailComposeViewControllerDelegate>
{
	// Instance variable
    NSMutableDictionary *userProfileInfo;
    
	UILabel *lblCountOfImages;
	UIButton *btnProfileDetail;

	// Server
	SRServerConnection *server;
    MFMailComposeViewController *reportController;
    
    // For family
//    NSMutableArray *familyListArr;
    
    BOOL isOnce;
}

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *imgViewProfile;

@property (nonatomic, strong) IBOutlet UIScrollView *objBackGroundScrollview;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileHeadName;
@property (nonatomic, strong) IBOutlet UILabel *lblDegree;

@property (nonatomic, strong) IBOutlet UILabel *lblProfileAddress;
@property (nonatomic, strong) IBOutlet UILabel *lblProfileDesc;
@property (nonatomic, strong) IBOutlet UIImageView *imgDating;
@property (nonatomic, strong) IBOutlet UILabel *lblDatingTitle;
@property (nonatomic, strong) IBOutlet UIButton *btnAlbum;
@property (nonatomic, strong) IBOutlet UIButton *btnReadMore;
@property (nonatomic, strong) IBOutlet UIButton *btnCompass;
@property (nonatomic, strong) IBOutlet UIScrollView *profileDetailsScrollview;
@property (nonatomic, strong) IBOutlet UIView *objDatingView;
@property (nonatomic, strong) IBOutlet UIView *objFamilyView;
@property (nonatomic, strong) IBOutlet UIView *objConnectionView;
@property (nonatomic, strong) IBOutlet UIScrollView *familyPhotosScrollview;
@property (nonatomic, strong) IBOutlet UIScrollView *connectionPhotosScrollview;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewBckScroll;
@property (nonatomic, strong) IBOutlet UILabel *lblFamilyCount;
@property (nonatomic, strong) IBOutlet UILabel *lblConnectionCount;
@property (nonatomic, strong) IBOutlet UIView *objInterestsView;
@property (nonatomic, strong) IBOutlet UIScrollView *interestsPhotosScrollview;
@property (nonatomic, strong) IBOutlet UILabel *lblInterestsCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;
@property (nonatomic,strong) NSMutableArray *familyListArr;

//Other Property
@property (weak) id delegate;

@property (nonatomic,strong) NSString *lblStr;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)btnAlbumClick:(id)sender;
- (IBAction)btnReadMoreClick:(id)sender;

@end
