#import "SRModalClass.h"
#import "SRUserViewController.h"
#import "SRMapViewController.h"
#import "SRUserTabViewController.h"
#import "SRProfileTabViewController.h"

@implementation SRUserViewController
#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRUserViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        userProfileInfo = [[NSMutableDictionary alloc] initWithDictionary:inProfileDict];
        server = inServer;

        // Register notification
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailSucceed:)
                              name:kKeyNotificationGetUserProfileSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailFailed:)
                              name:kKeyNotificationGetUserProfileFailed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updateHeadingAngle:)
                              name:kKeyNotificationUpdateHeadingValue
                            object:nil];
    }

    // Return
    return self;
}

// -----------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Tap guesture on profile image
    UITapGestureRecognizer *tapProfileImg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnAlbumClick:)];

    [self.imgViewProfile setUserInteractionEnabled:TRUE];
    [self.imgViewProfile addGestureRecognizer:tapProfileImg];

    // Family List
    self.lblFamilyCount.hidden = TRUE;

    SRUserTabViewController *v = (SRUserTabViewController *) self.tabBarController;
    v.familyListArr = [[NSMutableArray alloc] init];
    _familyListArr = [[NSMutableArray alloc] init];

    // Hide connectin & interest scroll
    self.objConnectionView.hidden = TRUE;
    self.objInterestsView.hidden = TRUE;

    // Set image view gredient
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.imgViewBckScroll.alpha = 0.95;
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.imgViewBckScroll.frame.size.height);
        gradient.colors = @[(id) [[UIColor blackColor] CGColor],
                (id) [[UIColor whiteColor] CGColor]];
        [self.imgViewBckScroll.layer insertSublayer:gradient atIndex:0];
    });

    // Hide the scroll at first
    self.objBackGroundScrollview.hidden = YES;
    self.familyPhotosScrollview.scrollEnabled = NO;
    self.connectionPhotosScrollview.scrollEnabled = NO;
    self.interestsPhotosScrollview.scrollEnabled = NO;

    //set Constant text to labels
    self.lblFamilyCount.text = NSLocalizedString(@"lbl.family.txt", @"");
    self.lblConnectionCount.text = NSLocalizedString(@"lbl.connections.txt", @"");
    self.lblInterestsCount.text = NSLocalizedString(@"lbl.interests.txt", @"");


    //Add Shadow to btnAlbum
    [self.btnAlbum.layer setShadowOffset:CGSizeMake(5, 5)];
    [self.btnAlbum.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [self.btnAlbum.layer setShadowOpacity:8.5];

    // Call Api
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassRegisterUser, userProfileInfo[kKeyId]];
    // [server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:YES inMethodType:kGET];
 //   [server makeSynchronousRequest:urlStr inParams:nil inMethodType:kGET isIndicatorRequired:YES];

    CGRect frameimg = CGRectMake(5, 5, 18, 18);
    UIButton *leftButton = [[UIButton alloc] initWithFrame:frameimg];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"menubar-back.png"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backBtnAction:)
         forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *leftBarBtn = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.tabBarController.navigationItem.leftBarButtonItem = leftBarBtn;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self updatedProfileInfo:self->userProfileInfo];
    });
    
    
    [self rotationCompass:-((APP_DELEGATE).compassHeading * M_PI / 180)];
}

// ------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    // Call Super
    [super viewWillAppear:NO];

    // Display details
    if (!isOnce) {
        [self displayDetails];
        isOnce = YES;
    }
}

#pragma mark
#pragma mark Private Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// setScrollContentSize

- (void)setScrollContentSize {

    // height for Profile Address Details
    int y;
    CGRect frame = self.lblProfileAddress.frame;
    CGRect textRect = [self.lblProfileAddress.text boundingRectWithSize:CGSizeMake(self.profileDetailsScrollview.frame.size.width - 16, CGFLOAT_MAX)
                                                                options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lblProfileAddress.font} context:nil];

    frame.size.height = textRect.size.height;
    frame.size.width = self.profileDetailsScrollview.frame.size.width - 16;
    self.lblProfileAddress.frame = frame;

    [self.lblProfileAddress sizeToFit];

    // height for about user Details
    y = self.lblProfileAddress.frame.origin.y + self.lblProfileAddress.frame.size.height;
    if ([userProfileInfo[kKeyAboutUser] length] > 0) {
        frame = self.lblProfileDesc.frame;
        frame.origin.y = y + 5;
        self.lblProfileDesc.frame = frame;

        //height for Profile Description Details

        textRect = [self.lblProfileDesc.text boundingRectWithSize:CGSizeMake(self.lblProfileDesc.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.lblProfileDesc.font} context:nil];

        frame.size.height = textRect.size.height;
        frame.size.width = self.profileDetailsScrollview.frame.size.width - 16;
        self.lblProfileDesc.frame = frame;

        self.btnReadMore.frame = CGRectMake(self.btnReadMore.frame.origin.x, self.lblProfileDesc.frame.origin.y + self.lblProfileDesc.frame.size.height, self.btnReadMore.frame.size.width, self.btnReadMore.frame.size.height);

        y = y + self.lblProfileDesc.frame.size.height + self.btnReadMore.frame.size.height;
    }

    // height for dating view
    if ([userProfileInfo[kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *settingDict = userProfileInfo[kKeySetting];
        if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
            [self.objDatingView setHidden:FALSE];
            frame = self.objDatingView.frame;
            frame.origin.y = y + 8;
            self.objDatingView.frame = frame;

            y = y + self.objDatingView.frame.size.height;
        } else {
            [self.objDatingView setHidden:TRUE];
        }

    }

    // height for family scroll
    frame = self.objFamilyView.frame;
    frame.origin.y = y + 12;
    self.objFamilyView.frame = frame;
    y = y + self.objFamilyView.frame.size.height + 10;

    //Set contect size for profile scroll using family scroll as connection and interest scroll is hidden
    self.profileDetailsScrollview.contentSize = CGSizeMake(self.profileDetailsScrollview.frame.size.width, y + 50);

    //Disable scrolling in iphone 6s plus
    if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {

        self.profileDetailsScrollview.scrollEnabled = NO;
    } else
        self.profileDetailsScrollview.scrollEnabled = YES;

}

- (IBAction)reportUser:(id)sender {
    
    reportController = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        NSString *textToShare = @"Report to user";
        [reportController setMessageBody:[NSString stringWithFormat:@"%@ %@", textToShare, [userProfileInfo objectForKey:kKeyId]] isHTML:NO];
        reportController.delegate = self;
        [reportController setCcRecipients:@[@"support@serendipity.app"]];
        reportController.mailComposeDelegate = self;
        [reportController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:reportController animated:YES completion:nil];
    }
    
}


//---------------------------------------------------------------------------------------------------------------------------------
// displayDetails

- (void)displayDetails {
    NSDictionary *dict = userProfileInfo;

    // Profile image
    if (userProfileInfo[kKeyImageObject] != nil) {
        self.imgViewProfile.image = (UIImage *) userProfileInfo[kKeyImageObject];
    }
    if ([userProfileInfo[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        if ([userProfileInfo[kKeyProfileImage] objectForKey:kKeyImageName] != nil) {
            NSString *imageName = [userProfileInfo[kKeyProfileImage] objectForKey:kKeyImageName];
            if ([imageName length] > 0) {
                NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];;

                [self.imgViewProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            } else
                self.imgViewProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
        } else
            self.imgViewProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];
    } else
        self.imgViewProfile.image = [UIImage imageNamed:@"deault_profile_bck.png"];

    // Name
//	self.lblProfileHeadName.text = [NSString stringWithFormat:@"%@ %@", [dict valueForKey:kKeyFirstName], [dict valueForKey:kKeyLastName]];


    if ([[dict[kKeySetting] objectForKey:kkeyHideLastName] boolValue] == YES) {
        self.lblProfileHeadName.text = [NSString stringWithFormat:@"%@", dict[kKeyFirstName]];
    } else
        self.lblProfileHeadName.text = [NSString stringWithFormat:@"%@ %@", dict[kKeyFirstName], dict[kKeyLastName]];

    // Display the values
    _lblStr = @"";
    if ([dict[kKeyDOB] length] > 0 && [[dict[kKeySetting] objectForKey:kKeyShowAge] boolValue] == YES) {
        NSString *birthDate = dict[kKeyDOB];
        NSDate *todayDate = [NSDate date];
        NSDate *dobDate = [SRModalClass returnDateInYYYYMMDD:birthDate];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear
                                                   fromDate:dobDate
                                                     toDate:todayDate
                                                    options:0];

        _lblStr = [NSString stringWithFormat:@"%ld", (long) components.year];
    }
    // Occupation
    if ([dict[kKeyOccupation] length] > 0) {
        if ([_lblStr length] > 0) {
            _lblStr = [NSString stringWithFormat:@"%@, %@", _lblStr, dict[kKeyOccupation]];
        } else {
            _lblStr = dict[kKeyOccupation];
        }
    }

    if (_lblStr.length) {
        _lblStr = [_lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        _lblStr = [_lblStr stringByReplacingOccurrencesOfString:@",," withString:@","];
    }

    //Live Address
    BOOL isMutatedStrSet = NO;
    NSString *address = @"";
    if ([dict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
        address = [dict[kKeyLocation] objectForKey:kKeyLiveAddress];
    }
    if (address.length > 0) {
        if ([_lblStr length] > 0) {
            NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:_lblStr];
            NSString *str = [NSString stringWithFormat:@" in %@", address];
            NSAttributedString *atrStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaItalicMedium size:12]}];
            [muAtrStr appendAttributedString:atrStr];
            [self.lblProfileAddress setAttributedText:muAtrStr];
            isMutatedStrSet = YES;
        } else {
            _lblStr = address;
            _lblStr = [_lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
    } else {
        if ([_lblStr length] > 0 && [address length] > 0) {
            NSMutableAttributedString *muAtrStr = [[NSMutableAttributedString alloc] initWithString:_lblStr];
            NSString *str = [NSString stringWithFormat:@" in %@", [dict[kKeyLocation] valueForKey:kKeyLiveAddress]];
            NSAttributedString *atrStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaItalicMedium size:12]}];
            [muAtrStr appendAttributedString:atrStr];
            [self.lblProfileAddress setAttributedText:muAtrStr];
            // _lblStr = self.lblProfileAddress.text;
            isMutatedStrSet = YES;
        } else if ([address length] > 0) {
            _lblStr = [dict[kKeyLocation] valueForKey:kKeyLiveAddress];
            _lblStr = [_lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        } else if ([dict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
            if ([[dict[kKeyLocation] valueForKey:kKeyLattitude] length] > 0 && [[dict[kKeyLocation] valueForKey:kKeyLattitude] length] > 0) {
                //Get Address from GoogleAPI
                [self getCurrentAddress];
            }
        }
    }

    // Check whether lbl string has some thing to display
    if ([_lblStr length] > 0 && !isMutatedStrSet) {
        self.lblProfileAddress.text = _lblStr;
    } else if ([_lblStr length] == 0) {
        self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
    }

    // Display Description
    NSString *str = dict[kKeyAboutUser];
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    if (goodValue == nil){
        goodValue = str;
    }
    [dict setValue:goodValue forKey:kKeyAboutUser];
    [userProfileInfo setValue:goodValue forKey:kKeyAboutUser];
    
    if ([dict[kKeyAboutUser] length] > 0) {
        if ([dict[kKeyAboutUser] length] > 135) {
            NSString *str = [NSString stringWithFormat:@"%@\n%@", dict[kKeyAboutUser], dict[kKeyUserWebUrl]];
            NSString *subStr = [str substringWithRange:NSMakeRange(0, 135)];
            subStr = [NSString stringWithFormat:@"%@...", subStr];
            
            self.lblProfileDesc.text = subStr;
            self.btnReadMore.hidden = NO;
        } else {
            self.lblProfileDesc.text = dict[kKeyAboutUser];
            self.lblProfileDesc.text = [NSString stringWithFormat:@"%@\n%@", self.lblProfileDesc.text, dict[kKeyUserWebUrl]];
            self.btnReadMore.hidden = YES;
        }
    } else {
        self.lblProfileDesc.text = @" ";
        self.btnReadMore.hidden = YES;
    }

    // Open dating set image accrodingly
    if ([dict[kKeySetting] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *settingDict = dict[kKeySetting];
        if ([settingDict[kKeyOpenForDating] isEqualToString:@"1"]) {
            self.imgDating.image = [UIImage imageNamed:@"dating-orange-40px.png"];
        } else {
            // If not open for dating hide image and lbl
            self.imgDating.hidden = TRUE;
            self.lblDatingTitle.hidden = TRUE;
        }
    } else {
        // If setting dictionary is null
        self.imgDating.hidden = TRUE;
        self.lblDatingTitle.hidden = TRUE;
    }

    // Degree Relation
    self.lblDegree.layer.cornerRadius = self.lblDegree.frame.size.width / 2.0;
    self.lblDegree.layer.masksToBounds = YES;

    if (dict[kKeyIsFromConnection] != nil) {
        self.lblDegree.text = @" 1°";
    } else {
        if ([userProfileInfo[kKeyDegree] isKindOfClass:[NSNumber class]]) {
            NSInteger degree = [userProfileInfo[kKeyDegree] integerValue];
            self.lblDegree.text = [NSString stringWithFormat:@" %ld°", (long) degree];
        } else {
            self.lblDegree.text = @" 6°";
        }
    }

    [self.familyPhotosScrollview setHidden:NO];
    [self.connectionPhotosScrollview setHidden:YES];
    [self.interestsPhotosScrollview setHidden:YES];

    //Compass location
    CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude,
            server.myLocation.coordinate.longitude};

    NSDictionary *userLocDict;
    if ([dict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
        userLocDict = dict[kKeyLocation];
        CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};


        [self.btnCompass addTarget:self action:@selector(compassTapCaptured:) forControlEvents:UIControlEventTouchUpInside];

        NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
        if (directionValue == kKeyDirectionNorth) {
            self.lblCompassDirection.text = @"N";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionEast) {
            self.lblCompassDirection.text = @"E";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouth) {
            self.lblCompassDirection.text = @"S";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionWest) {
            self.lblCompassDirection.text = @"W";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthEast) {
            self.lblCompassDirection.text = @"NE";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionNorthWest) {
            self.lblCompassDirection.text = @"NW";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSouthEast) {
            self.lblCompassDirection.text = @"SE";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
        } else if (directionValue == kKeyDirectionSoutnWest) {
            self.lblCompassDirection.text = @"SW";
            [self.btnCompass setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
        }
    }

    [self setScrollContentSize];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [reportController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark Action Methods
#pragma mark
// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)backBtnAction:(id)sender {
    [APP_DELEGATE hideActivityIndicator];
    if ([self.tabBarController.selectedViewController isKindOfClass:[SRUserViewController class]]) {
        if (self.objBackGroundScrollview.isHidden) {
            [self.tabBarController.navigationController popViewControllerAnimated:YES];
        } else {
            self.objBackGroundScrollview.hidden = YES;
            self.profileDetailsScrollview.hidden = NO;
            self.imgViewBckScroll.hidden = NO;
            self.btnAlbum.hidden = NO;
        }
    } else if ([self.tabBarController.selectedViewController isKindOfClass:[SRChatViewController class]] && (APP_DELEGATE).isPreviewOpen) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationBackClicked
                                                            object:nil];
    } else
        [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

// ------------------------------------------------------------------------------------------------------
// familyImageTapDetected:
- (void)familyImageTapDetected:(UIGestureRecognizer *)tapRecognizer {
    UIView *viewTiedWithRecognizer = tapRecognizer.view;

    // Get profile
    NSDictionary *userDict = _familyListArr[viewTiedWithRecognizer.tag];
    NSMutableDictionary *mutatedDict = [[NSMutableDictionary alloc] init];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyId] forKey:kKeyId];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyFirstName] forKey:kKeyFirstName];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyLastName] forKey:kKeyLastName];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyAboutUser] forKey:kKeyAboutUser];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyMobileNumber] forKey:kKeyMobileNumber];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyAddress] forKey:kKeyAddress];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyDOB] forKey:kKeyDOB];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyOccupation] forKey:kKeyOccupation];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyProfileImage] forKey:kKeyProfileImage];
    [mutatedDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeySetting] forKey:kKeySetting];
    mutatedDict[kKeyIsFromConnection] = @1;
    // For showind direct connection in degree of seperation view
    [mutatedDict setValue:@1 forKey:kKeyDegree];

    //Connection Dictionary
    NSMutableDictionary *connDict = [[NSMutableDictionary alloc] init];
    [connDict setValue:@"2" forKey:kKeyConnectionStatus];
    [connDict setValue:[[userDict valueForKey:kKeyRelative] valueForKey:kKeyId] forKey:kKeyConnectionID];
    [mutatedDict setValue:connDict forKey:kKeyConnection];

    server.friendProfileInfo = [SRModalClass removeNullValuesFromDict:mutatedDict];
    // Show user public profile
    if ([[[userDict valueForKey:kKeyRelative] valueForKey:kKeyId] isEqualToString:[server.loggedInUserInfo valueForKey:kKeyId]]) {
        SRProfileTabViewController *profileTabCon = [[SRProfileTabViewController alloc] initWithNibName:nil bundle:nil];
        self.hidesBottomBarWhenPushed = YES;
        [self.tabBarController.navigationController pushViewController:profileTabCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else {
        SRUserTabViewController *tabCon = [[SRUserTabViewController alloc] initWithNibName:nil bundle:nil];
        self.hidesBottomBarWhenPushed = YES;
        [self.tabBarController.navigationController pushViewController:tabCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    }
}

// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTapCaptured:(UIButton *)sender {
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] initWithDictionary:userProfileInfo];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:nil bundle:nil inDict:userDict server:server];
//    (APP_DELEGATE).topBarView.hidden = YES;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

// ------------------------------------------------------------------------------------------------------
// btnReadMoreClick:

- (IBAction)btnReadMoreClick:(id)sender {
    if ([self.btnReadMore.titleLabel.text isEqualToString:@" << "]) {
        NSString *str = [NSString stringWithFormat:@"%@\n%@", userProfileInfo[kKeyAboutUser], userProfileInfo[kKeyUserWebUrl]];
        NSString *subStr = [str substringWithRange:NSMakeRange(0, 135)];
        subStr = [NSString stringWithFormat:@"%@...", subStr];
        self.lblProfileDesc.text = subStr;

        [self.btnReadMore setTitle:NSLocalizedString(@"btn.read_more.txt", @"") forState:UIControlStateNormal];
    } else {
        NSString *str = [NSString stringWithFormat:@"%@\n%@", userProfileInfo[kKeyAboutUser], userProfileInfo[kKeyUserWebUrl]];
        self.lblProfileDesc.text = str;
        [self.btnReadMore setTitle:@" << " forState:UIControlStateNormal];
    }

    // Set Scroll Size
    [self setScrollContentSize];
}

// ------------------------------------------------------------------------------------------------------
// btnAlbumClick:

- (IBAction)btnAlbumClick:(id)sender {
    NSArray *mediaArr = userProfileInfo[kKeyMedia];
    if ([mediaArr count] > 0) {
        lblCountOfImages.hidden = NO;
        btnProfileDetail.hidden = NO;

        self.btnAlbum.hidden = YES;

        self.imgViewBckScroll.hidden = YES;
        self.profileDetailsScrollview.hidden = YES;

        self.objBackGroundScrollview.hidden = NO;
        self.objBackGroundScrollview.scrollEnabled = YES;
    } else {
        [SRModalClass showAlert:NSLocalizedString(@"alert.no.albums.text", @"")];
    }
}

// ------------------------------------------------------------------------------------------------------
// contactBtnClicked:

- (void)contactBtnClicked:(UIButton *)sender {
    UIView *view = [sender superview];
    for (UIView *subView in[view subviews]) {
        if ([subView isKindOfClass:[UILabel class]] || [subView isKindOfClass:[UIButton class]]) {
            subView.hidden = YES;
            if ([subView isKindOfClass:[UILabel class]]) {
                lblCountOfImages = (UILabel *) subView;
            } else {
                btnProfileDetail = (UIButton *) subView;
            }
        }
    }
    self.btnAlbum.hidden = NO;
    self.imgViewBckScroll.hidden = NO;
    self.profileDetailsScrollview.hidden = NO;
    self.profileDetailsScrollview.scrollEnabled = YES;

    self.objBackGroundScrollview.hidden = YES;
    self.objBackGroundScrollview.scrollEnabled = NO;
}

// ------------------------------------------------------------------------------------------------------
// addImagesInScrollView:

- (void)addImagesInScrollView:(BOOL)isFamily {
    for (UIView *v in [_familyPhotosScrollview subviews]) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
        if ([v isKindOfClass:[UILabel class]]) {
            [v removeFromSuperview];
        }

    }
    // Frame at starting point
    CGRect latestFrame = CGRectZero;
    // Adding images to scroll view
    for (int i = 0; i < [_familyListArr count]; i++) {
        self.lblFamilyCount.hidden = true;

        NSDictionary *relativeDict = [_familyListArr[i] objectForKey:kKeyRelative];

        UIImageView *objImageView = [[UIImageView alloc] init];
        if (i == 0) {
            [objImageView setFrame:CGRectMake(5, 0, 40, 40)];
        } else
            objImageView.frame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 25, 0, 40, 40);

        latestFrame = objImageView.frame;
        objImageView.image = [UIImage imageNamed:@"profile_thumbnail.png"];
        if ([relativeDict isKindOfClass:[NSDictionary class]] && relativeDict != nil) {
            NSDictionary *imgDict = [relativeDict valueForKey:kKeyProfileImage];
            if ([imgDict isKindOfClass:[NSDictionary class]] && imgDict != nil) {
                if (imgDict[kKeyImageName] != nil) {
                    NSString *imagename = [imgDict valueForKey:@"file"];
                    if ([imagename length] > 1) {

                        NSString *stringUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imagename];

                        [objImageView sd_setImageWithURL:[NSURL URLWithString:stringUrl] placeholderImage:[UIImage imageNamed:@"profile_thumbnail.png"]];
                    } else
                        objImageView.image = [UIImage imageNamed:@"profile_thumbnail.png"];
                } else
                    objImageView.image = [UIImage imageNamed:@"profile_thumbnail.png"];

            } else
                objImageView.image = [UIImage imageNamed:@"profile_thumbnail.png"];
        }
        objImageView.contentMode = UIViewContentModeScaleToFill;
        objImageView.layer.cornerRadius = objImageView.frame.size.width / 2;
        objImageView.clipsToBounds = YES;
        objImageView.tag = i;
        objImageView.userInteractionEnabled = TRUE;
        UITapGestureRecognizer *familyImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(familyImageTapDetected:)];
        [objImageView addGestureRecognizer:familyImageTap];

        UILabel *lblImageName = [[UILabel alloc] init];
        [lblImageName setFrame:CGRectMake(latestFrame.origin.x, latestFrame.origin.y + 40, 65, 18)];
        lblImageName.font = [UIFont fontWithName:kFontHelveticaMedium size:10];

        if ([relativeDict isKindOfClass:[NSDictionary class]]) {

            if (![[relativeDict valueForKey:kKeyFirstName] isKindOfClass:[NSNull class]] && [relativeDict valueForKey:kKeyFirstName] != nil && ![[relativeDict valueForKey:kKeyLastName] isKindOfClass:[NSNull class]] && [relativeDict valueForKey:kKeyLastName] != nil) {

                //NSString *lastname = [NSString stringWithFormat:@"%@",[relativeDict valueForKey:kKeyLastName]];

                NSString *lnameStr = [[relativeDict valueForKey:kKeyLastName] substringToIndex:1];

                lblImageName.text = [NSString stringWithFormat:@"%@ %@.", [relativeDict valueForKey:kKeyFirstName], lnameStr];
            } else
                lblImageName.text = @"";

        }

        lblImageName.textColor = [UIColor whiteColor];
        [self.familyPhotosScrollview addSubview:objImageView];
        [self.familyPhotosScrollview addSubview:lblImageName];
        self.familyPhotosScrollview.contentSize = CGSizeMake(150 * _familyListArr.count, 0);
        self.familyPhotosScrollview.scrollEnabled = YES;
        self.familyPhotosScrollview.userInteractionEnabled = YES;
        //lblImageName.text = str1;

        if (isFamily) {
            
            [self.familyPhotosScrollview addSubview:objImageView];
            [self.familyPhotosScrollview addSubview:lblImageName];
            
            if ((latestFrame.origin.x + 55) > SCREEN_WIDTH) {
                self.familyPhotosScrollview.contentSize = CGSizeMake(objImageView.frame.size.width * _familyListArr.count, 0);
                self.familyPhotosScrollview.scrollEnabled = YES;
                
            }
        }
        else {
            [self.connectionPhotosScrollview addSubview:lblImageName];
            [self.connectionPhotosScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.connectionPhotosScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.connectionPhotosScrollview.scrollEnabled = YES;
            }
            
            // Add images in Interests Scrollview
            [self.interestsPhotosScrollview addSubview:lblImageName];
            [self.interestsPhotosScrollview addSubview:objImageView];
            if ((latestFrame.origin.x + 45) > SCREEN_WIDTH) {
                self.interestsPhotosScrollview.contentSize = CGSizeMake(latestFrame.origin.x + 55, 0);
                self.interestsPhotosScrollview.scrollEnabled = YES;
            }
        }

    }
}

- (void)getCurrentAddress {
    self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
    //Call Google API for correct distance and address from two locations

    if (server.myLocation != nil && [userProfileInfo[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];

        NSString *urlStr = [NSString stringWithFormat:@"%@%@=%f&lon=%f", kNominatimServer, kAddressApi, fromLoc.coordinate.latitude, fromLoc.coordinate.longitude];
    //    NSLog(@"Nominatim URl=%@", urlStr);

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.timeoutInterval = 60;
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if (!error) {
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                       Boolean isAddressPresent = true;
                                       for (NSString *keyStr in json) {
                                           if ([keyStr isEqualToString:@"error"]) {
                                               isAddressPresent = false;
                                           }
                                       }
                                       if (isAddressPresent) {
                                           NSString *strAddress = [json valueForKey:@"display_name"];
                                           if ([userProfileInfo[kKeyDOB] length] > 0) {
                                               NSString *birthDate = userProfileInfo[kKeyDOB];
                                               NSDate *todayDate = [NSDate date];
                                               NSDate *dobDate = [SRModalClass returnDateInYYYYMMDD:birthDate];
                                               NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                                               NSDateComponents *components = [calendar components:NSCalendarUnitYear
                                                                                          fromDate:dobDate
                                                                                            toDate:todayDate
                                                                                           options:0];
                                               _lblStr = [NSString stringWithFormat:@"%ld", (long) components.year];
                                           }
                                           if ([userProfileInfo[kKeyOccupation] length] > 0) {
                                               if ([_lblStr length] > 0) {
                                                   _lblStr = [NSString stringWithFormat:@"%@, %@", _lblStr, userProfileInfo[kKeyOccupation]];
                                               } else {
                                                   _lblStr = userProfileInfo[kKeyOccupation];
                                               }
                                           }
                                           if (_lblStr.length) {
                                               _lblStr = [_lblStr stringByAppendingString:[NSString stringWithFormat:@", in %@", strAddress]];
                                           } else {
                                               _lblStr = [NSString stringWithFormat:@"in %@", strAddress];
                                           }
                                           if (_lblStr.length) {
                                               _lblStr = [_lblStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                               _lblStr = [_lblStr stringByReplacingOccurrencesOfString:@",," withString:@","];
                                           }
                                           self.lblProfileAddress.text = _lblStr;
                                           [self setScrollContentSize];
                                       } else {
                                           self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                                           [self setScrollContentSize];
                                       }
                                   } else {
                                       self.lblProfileAddress.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                                       [self setScrollContentSize];
                                   }
                               }];
    }
}

-(void)updatedProfileInfo:(NSDictionary*)inObj {
    [APP_DELEGATE hideActivityIndicator];
    [userProfileInfo addEntriesFromDictionary:[SRModalClass removeNullValuesFromDict:inObj]];
    
    // Remove all objects & add new
    [_familyListArr removeAllObjects];
    if ([userProfileInfo[kKeyFamily] isKindOfClass:[NSArray class]]) {
        [_familyListArr addObjectsFromArray:userProfileInfo[kKeyFamily]];
    }
    
       [self addImagesInScrollView:NO];

    // Display images in scroll view
    NSMutableArray *mediaArr = userProfileInfo[kKeyMedia];
    for (int i = 0; i < mediaArr.count; i++) {
        NSDictionary *mediaDict = mediaArr[i];
        
        CGRect frame;
        frame.origin.x = UIScreen.mainScreen.bounds.size.width * i;
        frame.origin.y = 0;
        frame.size = self.objBackGroundScrollview.frame.size;
        frame.size.width = UIScreen.mainScreen.bounds.size.width;
        
        UIView *subview = [[UIView alloc] initWithFrame:frame];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor blackColor];
        
        imageView.userInteractionEnabled = YES;
        if (mediaDict[kKeyImageName] != nil) {
            NSString *imageName = mediaDict[kKeyImageName];
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
            [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        } else
            imageView.image = [UIImage imageNamed:@"profile_thumbnail.png"];
        if (i%2 == 0) {
            subview.backgroundColor = [UIColor redColor];
        } else {
            subview.backgroundColor = [UIColor grayColor];
        }
        [subview addSubview:imageView];
        
        
        UILabel *countLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 150, 20)];
        countLbl.font = [UIFont fontWithName:kFontHelveticaMedium size:12];
        NSString *str1 = [NSString stringWithFormat:@"%d of %lu", i + 1, (unsigned long) mediaArr.count];
        countLbl.textColor = [UIColor whiteColor];
        countLbl.text = str1;
        [subview addSubview:countLbl];
        
        UIButton *ContactBtn = [[UIButton alloc] initWithFrame:CGRectMake(UIScreen.mainScreen.bounds.size.width - 50, 5, 25, 25)];
        [ContactBtn setBackgroundImage:[UIImage imageNamed:@"back-to-profile.png"] forState:UIControlStateNormal];
        [ContactBtn addTarget:self action:@selector(contactBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [subview addSubview:ContactBtn];
        
        if (i == 0) {
            lblCountOfImages = countLbl;
            lblCountOfImages.hidden = YES;
            btnProfileDetail = ContactBtn;
            btnProfileDetail.hidden = YES;
        }
        [self.objBackGroundScrollview addSubview:subview];
    }
    self.objBackGroundScrollview.contentSize = CGSizeMake(UIScreen.mainScreen.bounds.size.width * mediaArr.count, 0);
    self.objBackGroundScrollview.scrollEnabled = NO;
    
    
    [self.familyPhotosScrollview setHidden:NO];
    [self.connectionPhotosScrollview setHidden:NO];
    [self.interestsPhotosScrollview setHidden:NO];
}

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    float headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    [self rotationCompass:headingAngle];
}

- (void)rotationCompass:(CGFloat)headingAngle {
    // rotate the compass to heading degree
    self.btnCompass.transform = CGAffineTransformMakeRotation(headingAngle);
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:

- (void)getProfileDetailSucceed:(NSNotification *)inNotify {
    [self updatedProfileInfo:[inNotify object]];
}

// --------------------------------------------------------------------------------
// getProfileDetailsFailed:

- (void)getProfileDetailFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    // [SRModalClass showAlert:@"Request time out"];
}


@end
