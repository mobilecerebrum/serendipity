//
//  SRActivityItemProvider.h
//  Serendipity
//
//  Created by Madhura on 28/03/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRActivityItemProvider : UIActivityItemProvider<UIActivityItemSource>

@end
