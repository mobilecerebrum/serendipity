#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "ASValueTrackingSlider.h"
#import <GoogleSignIn/GoogleSignIn.h>

#import "LiveConnectClient.h"
#import "SRManagePersistentConnectionsViewController.h"

#define YAHOO_AUTHORIZATION_CODE @"YAHOO_AUTHORIZATION_CODE"
#define YahooConsumerkey @"dj0yJmk9TlNuVzVwbUxNQVdUJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWJk"
#define YahooConsumerSecret @"4935672cd4752ba2e45edf8206dc31f6ab3e15fc"
#define YahooAppId @"rdR8LD6q"
#define YahooCallBackUrl @"http://dev.serendipity.app/callback"

//#define LiveClientId @"9ab9985a-775f-47ca-818e-8b536baa8bdc" //@"0000000040188722"    //@"000000004813E69A"
//@"6a8d57df-8439-4124-aae9-f9b19f0d19e4"


@interface SRMenuViewController : ParentViewController  <GIDSignInDelegate,LiveAuthDelegate,LiveOperationDelegate>
{
	// Instance variable
	NSMutableArray *menuItemArray;
	NSMutableArray *menuIconArray,*myLocationArray;
    NSArray *notificationsList;
    NSMutableArray *contactList,*importContactArray;
    NSMutableArray *contactNameList;
    NSMutableArray *ContactNumberList;
    NSString *texttoshare;
	// Server
	SRServerConnection *server;
    BOOL isPhonebtnClicked;
    //slider
    ASValueTrackingSlider *slider;
    UILabel *minimalLbl,*veryLowLbl,*lowLbl,*medLbl,*highLbl,*realTimeLbl,*sliderLbl;
}

// Properties
@property (strong, nonatomic) IBOutlet SRProfileImageView *backGroundImage;
@property (strong, nonatomic) IBOutlet SRProfileImageView *profileImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileBackgroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *profileBackgroundColor;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UITableView *objtableView;
@property (strong, nonatomic) IBOutlet UIImageView *logoAppImage;
@property (strong, nonatomic) IBOutlet UILabel *versionDateLbl;
@property (strong, nonatomic) NSString *contactType;
//Yahoo Session

@property (weak) id<YahooSessionGetNotifyDelegate> delegate1;

//Live
@property (strong, nonatomic) LiveConnectClient *liveClient;
// Other properties
@property (weak) id delegate;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;

@end
