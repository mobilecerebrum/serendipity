#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "SRModalClass.h"
#import "SRHomeMenuCustomCell.h"
#import "SRProfileTabViewController.h"
#import "SRAccountSettingsViewController.h"
#import "SRPingsTabViewController.h"
#import "SRAchivementsViewController.h"
#import "SRServerConnection.h"
#import "SRMyConnectionsViewController.h"
#import "SRFamilyViewController.h"
#import "SREventsViewController.h"
#import "SRLearnViewController.h"
#import "SRMenuViewController.h"
#import "SREventTabViewController.h"
#import "SRGroupTabViewController.h"
#import "SRDropPinViewController.h"
#import "SRTerms&ConditionsViewController.h"
#import "SRPrivacyViewController.h"
#import "SRTrackingViewController.h"
#import "SRActivityItemProvider.h"
#import "SRBatterySettingsTableViewCell.h"
#import "DBManager.h"
#import <OAuthSwift/OAuthSwift-Swift.h>
#import "Serendipity-Bridging-Header.h"
#import <Serendipity-Swift.h>
#import "SocialContactListViewController.h"
#import "SRContactPermissionVC.h"
#import "SRlocationHistoryVC.h"


#define kBtnFacebookTag 1
#define kBtnWhatsAppTag 2
#define kBtnPhoneCallTag 3
#define kBtnLinkedInTag 4
#define kBtnYahooTag 5
#define kBtnGooglePlusTag 6
#define kBtnShareTag 7
#define kBtnMessageTag 8
#define kCellBtnFacebookTag 9
#define kCellBtnGoogleTag 10
#define kCellBtnLinkedInTag 11
#define kCellBtnTwitterTag 12
#define kSliderMinValue 0
#define kSliderMaxValue 170
#define kSliderInterval 34

@implementation SRMenuViewController

YahooSwift *yahoo;

#pragma mark
#pragma mark Private Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// displayData

- (void)displayData {
 //   NSLog(@"loggedinInfo:::%@", server.loggedInUserInfo);
    //Set ProfileName get from Server Data
    self.lblName.text = [server.loggedInUserInfo valueForKey:kKeyFirstName];
    [self updatedLocation:nil];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// updateTable:

- (void)updateTable {
    // Get count
    notificationsList = server.notificationArr;
    [self.objtableView reloadData];
}

- (void)updatedNotificationCount:(NSNotification *)inNotify {
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassGetNotification, [server.loggedInUserInfo objectForKey:kKeyId]];
    [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    /*
     [[SRAPIManager sharedInstance] getNotificationsForUser:server.loggedInUserInfo[kKeyId] completion:^(id _Nonnull result, NSError *_Nonnull error) {
     NSLog(@"Loaded notifications: \n %@", result);
     }];
     
     //[server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
     
     NSString *notificationInfo = [inNotify object];
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, notificationInfo];
     NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
     
     NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
     if(array.count>0 &&filterArr.count>0)
     [array removeObject:[filterArr objectAtIndex:0]];
     notificationsList = array;
     [self.objtableView reloadData];
     [self performSelector:@selector(updateTable) withObject:nil afterDelay:0.2];
     */
}

#pragma mark Init Method
// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRMenuViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        
        // Register notification
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailsSucceed:)
                              name:kKeyNotificationCompleteSignUPSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getProfileDetailsFailed:)
                              name:kKeyNotificationCompleteSignUPFailed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationSucceed:)
                              name:kKeyNotificationGetNotificationSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getNotificationFailed:)
                              name:kKeyNotificationGetNotificationFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(pingsNotificationCount:)
                              name:kKeyChatMessageRecieved object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(pingsNotificationCount:)
                              name:kKeyNotificationMessageStateUpdated object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateSucceed:)
                              name:kKeyNotificationSettingUpdateSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateFailed:)
                              name:kKeyNotificationSettingUpdateFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updatedLocation:)
                              name:kKeyNotificationRadarLocationUpdated
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(updatedNotificationCount:)
                              name:kKeyNotificationUpdatedNotificationCount
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(contactUploadSuccessNotifier:)
                              name:kKeyNotificationContactUploadSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(contactUploadFailedNotifier:)
                              name:kKeyNotificationContactUploadFailed object:nil];
        
        
        [defaultCenter addObserver:self
                          selector:@selector(getContactStoreSuccessNotifier:)
                              name:kKeyNotificationUserContactStoreSucceed
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getContactStoreFailureNotifier:)
                              name:kKeyNotificationUserContactStoreFailed object:nil];
        
        
        [defaultCenter addObserver:self
                          selector:@selector(refreshList:)
                              name:kKeyNotificationRefreshList
                            object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(yahooContactsImported:) name:@"YAHOO_CONTACTS_LOADED" object:nil];
    }
    return self;
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self name:kKeyNotificationUserContactStoreSucceed object:nil];
    [defaultCenter removeObserver:self name:kKeyNotificationUserContactStoreFailed object:nil];
}
- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Standard overrides

- (void)viewDidLoad{
    // Call super
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    
    //    NSString *numericVersion = [bundleInfo objectForKey:@"CFBundleNumericVersion"];
    NSString *shortVersion = bundleInfo[@"CFBundleShortVersionString"];
    if ([self GetBuildDate] != nil) {
        self.versionDateLbl.text = [[@"@V" stringByAppendingString:[shortVersion stringByAppendingString:@" "]] stringByAppendingString:[self GetBuildDate]];
    }
    
    // Set title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.menu", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    menuItemArray = [@[NSLocalizedString(@"lbl.broadcast_Loc.txt", @""), NSLocalizedString(@"lbl.tracking.txt", @""), NSLocalizedString(@"lbl.Pings.txt", @""), NSLocalizedString(@"lbl.Drop a pin.txt", @""), NSLocalizedString(@"lbl.privacy_setting.txt", @""), NSLocalizedString(@"lbl.Groups.txt", @""), NSLocalizedString(@"lbl.event.txt", @"'"), NSLocalizedString(@"lbl.Connections.txt", @""), NSLocalizedString(@"lbl.Contacts.txt", @""), NSLocalizedString(@"lbl.battery_drain.txt", @""), NSLocalizedString(@"lbl.Share with Friends.txt", @""), NSLocalizedString(@"lbl.Learn More.txt", @""), NSLocalizedString(@"lbl.Account Settings.txt", @""), NSLocalizedString(@"lbl.connect with friends.txt", @""), NSLocalizedString(@"lbl.Like_n_follow.txt", @""), NSLocalizedString(@"lbl.term_&_conditions.txt", @""), NSLocalizedString(@"lbl.policy.txt", @""),NSLocalizedString(@"lbl.send.location.logs.txt", @""),NSLocalizedString(@"lbl.location.history.logs.txt", @"")] mutableCopy];
    
    menuIconArray = [@[@"mainmenu-broadcast.png",
                       //@"mainmenu-battery.png",
                       @"mainmenu-tracking.png",
                       @"mainmenu-pings.png",
                       @"mainmenu-droppin.png",
                       @"mainmenu-privacy.png",
                       @"mainmenu-groups.png",
                       @"mainmenu-events.png",
                       @"mainmenu-connection.png",
                       @"tabbar-contact.png",
                       @"mainmenu-battery.png",
                       @"mainmenu-share.png",
                       @"mainmenu-learnmore.png",
                       @"mainmenu-setting.png",
                       @"imgview.eventIcon.img", @"", @"", @"", @"",@""] mutableCopy];
    
    self.objtableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.objtableView.separatorInset = UIEdgeInsetsMake(0, 8, 0, 0);
    
    // Tap Action On ProfileBackground Image
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileBackgroundImageTap:)];
    tapGesture.numberOfTapsRequired = 1;
    self.profileBackgroundImage.userInteractionEnabled = YES;
    [self.profileBackgroundImage addGestureRecognizer:tapGesture];
    
    // Set profile pic
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.layer.masksToBounds = YES;
    //Get google sign in to get gmail contact
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    //[signIn signInSilently];
    signIn.delegate = self;
    signIn.presentingViewController = self;
    
    //For google add contact scope
    NSString *contactsScope = @"https://www.googleapis.com/auth/contacts.readonly";
    NSArray *currentScopes = [GIDSignIn sharedInstance].scopes;
    [GIDSignIn sharedInstance].scopes = [currentScopes arrayByAddingObject:contactsScope];
    signIn.scopes = [currentScopes arrayByAddingObject:contactsScope];
    [self.backGroundImage blurImage];
    [[SRAPIManager sharedInstance] updateToken:server.token];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.tabBarController.tabBar setHidden:YES];
    // Display Data
    [self displayData];
    
    [self performSelector:@selector(updateTable) withObject:nil afterDelay:0.2];
    
    //    (APP_DELEGATE).topBarView.hidden = YES;
    (APP_DELEGATE).topBarView.clipsToBounds = YES;
    (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
    [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(getContactStoreSuccessNotifier:)
                          name:kKeyNotificationUserContactStoreSucceed
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getContactStoreFailureNotifier:)
                          name:kKeyNotificationUserContactStoreFailed object:nil];
}


- (void)refreshList:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.objtableView reloadData];
    });
}

#pragma mark Action Methods

- (NSString *)GetBuildDate {
    NSString *buildDate;
    
    // Get build date and time, format to 'yyMMddHHmm'
    NSString *dateStr = [NSString stringWithFormat:@"%@ %@", [NSString stringWithUTF8String:__DATE__], [NSString stringWithUTF8String:__TIME__]];
    
    // Convert to date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"LLL d yyyy HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // Set output format and convert to string
    [dateFormat setDateFormat:@"dd/MM/yy"];
    buildDate = [dateFormat stringFromDate:date];
    
    
    return buildDate;
}

- (void)backBtnAction:(id)sender {
    //    (APP_DELEGATE).topBarView.hidden = NO;
    // Set hidden NO because we set hidden YES when we click on community button of top bar view on radar view
    (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
    (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
    (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    (APP_DELEGATE).isBackMenu = YES;
    [self.navigationController popToViewController:self.delegate animated:NO];
}

- (void)profileBackgroundImageTap:(id)sender {
    SRProfileTabViewController *profileTabCon = [[SRProfileTabViewController alloc] initWithNibName:nil bundle:nil];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:profileTabCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;
}

#pragma mark TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuItemArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if (cell == nil) {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRHomeMenuCustomCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRHomeMenuCustomCell *customCell = (SRHomeMenuCustomCell *) nibObjects[0];
            cell = customCell;
        }
    }
    ((SRHomeMenuCustomCell *) cell).selectionStyle = UITableViewCellSelectionStyleNone;
    // Manage subviews hide and unhide
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemGoogleBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.cornerRadius = ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.frame.size.width / 2;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.masksToBounds = YES;
    ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.clipsToBounds = YES;
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        UISwitch *objSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 65, 4, 60, 25)];
        objSwitch.transform = CGAffineTransformMakeScale(1.0, 1.0);
        objSwitch.tintColor = [UIColor whiteColor];
        [objSwitch setOnTintColor:[UIColor orangeColor]];
        objSwitch.backgroundColor = [UIColor lightGrayColor];
        objSwitch.layer.cornerRadius = 16.0;
        
        if (indexPath.row == 0) {
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyBroadcastLocation] boolValue] == YES) {
                [objSwitch setOn:YES animated:NO];
            } else {
                [objSwitch setOn:NO animated:NO];
            }
        } else {
            if ([[server.loggedInUserInfo valueForKey:kKeySetting] objectForKey:kkeytracking_me]) {
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeytracking_me] boolValue] == YES) {
                    [objSwitch setOn:YES animated:NO];
                } else {
                    [objSwitch setOn:NO animated:NO];
                }
            }
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %@ AND (reference_type_id == %@ OR reference_type_id == %@)", @"0", @"6", @"7"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        if ([(APP_DELEGATE) getUnreadTrackingLocationCount] > 0) {
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                            NSInteger count = [(APP_DELEGATE) getUnreadTrackingLocationCount];
                            NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                            CGRect lblFrame = ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.frame;
                            lblFrame.origin.x = objSwitch.frame.origin.x - lblFrame.size.width - 5;
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.frame = lblFrame;
                        } else
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        CGRect lblFrame = ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.frame;
                        lblFrame.origin.x = objSwitch.frame.origin.x - lblFrame.size.width - 5;
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.frame = lblFrame;
                        NSInteger count = [filterArr count] + [(APP_DELEGATE) getUnreadTrackingLocationCount];
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
        }
        objSwitch.tag = indexPath.row;
        [objSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
        [((SRHomeMenuCustomCell *) cell) addSubview:objSwitch];
        
        
    } else if (indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8) {
        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.cornerRadius = ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.frame.size.width / 2;
        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.layer.masksToBounds = YES;
        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.clipsToBounds = YES;
        
        ((SRHomeMenuCustomCell *) cell).MenuIconImage.image = [UIImage imageNamed:@"mainmenu-droppin.png"];
        ((SRHomeMenuCustomCell *) cell).MenuIconImage.contentMode = UIViewContentModeScaleAspectFit;
        
        if (indexPath.row == 2) {
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"13"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) [filterArr count]];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
        }
        
        if (indexPath.row == 3) {
            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
            NSInteger count = [[DBManager getSharedInstance] getUnreadMsgCount];
            
            if (count == 0) {
                ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
            } else {
                ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
            }
        } else if (indexPath.row == 1) {
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == %@ AND (reference_type_id == %@ OR reference_type_id == %@)", @"0", @"6", @"7"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        if ([(APP_DELEGATE) getUnreadTrackingLocationCount] > 0) {
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                            NSInteger count = [(APP_DELEGATE) getUnreadTrackingLocationCount];
                            NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                        } else
                            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        NSInteger count = [filterArr count] + [(APP_DELEGATE) getUnreadTrackingLocationCount];
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) count];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
        }
        //        else if (indexPath.row == 6) {
        //            __block NSArray *filterArr;
        //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"2"];
        //                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
        //                dispatch_async(dispatch_get_main_queue(), ^{
        //                    // Set Title to Notification Button
        //                    if ([filterArr count] == 0) {
        //                        ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = YES;
        //                    }
        //                    else {
        //                        ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.hidden = NO;
        //                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long)[filterArr count]];
        //                        ((SRHomeMenuCustomCell *)cell).MenuItemCountLabel.text = str;
        //                    }
        //                });
        //            });
        //        }
        else if (indexPath.row == 5) {
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"3"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) [filterArr count]];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
        } else if (indexPath.row == 6) {
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"4"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) [filterArr count]];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
        } else if (indexPath.row == 7) {
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"1"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) [filterArr count]];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
            //jonish sprint 2
            UIButton *connBtnImage = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2, 11, 22, 22)];
            UIImage *btnImage = [UIImage imageNamed:@"invisible-black.png"];
            [connBtnImage setImage:btnImage forState:UIControlStateNormal];
            [connBtnImage addTarget:self action:@selector(filterInvisibleConnection:) forControlEvents:UIControlEventTouchUpInside];
            //jonish sprint 2
            //connBtnImage.image = [UIImage imageNamed:@"invisible-black.png"];
            [((SRHomeMenuCustomCell *) cell).contentView addSubview:connBtnImage];
        }else if (indexPath.row == 8) {
            ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
            UIButton *connImg = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2, 11, 22, 22)];
            [connImg setImage:[UIImage imageNamed:@"icons-8-available-updates-filled-100"] forState:UIControlStateNormal];
            [connImg addTarget:self action:@selector(actionOnRefresh:) forControlEvents:UIControlEventTouchUpInside];
            [((SRHomeMenuCustomCell *) cell).contentView addSubview:connImg];
            
            __block NSArray *filterArr;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"17"];
                filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Set Title to Notification Button
                    if ([filterArr count] == 0) {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = YES;
                    } else {
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.hidden = NO;
                        NSString *str = [NSString stringWithFormat:@"%lu", (unsigned long) [filterArr count]];
                        ((SRHomeMenuCustomCell *) cell).MenuItemCountLabel.text = str;
                    }
                });
            });
            
            
        }
        
        
    } else if (indexPath.row == 9) {
        
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRBatterySettingsTableViewCell" owner:self options:nil];
        if ([nibObjects count] > 0) {
            SRBatterySettingsTableViewCell *customCell = (SRBatterySettingsTableViewCell *) nibObjects[0];
            cell = customCell;
        }
        //        UIFont *lblFont;
        //        UIView *cellView;
        //        if (SCREEN_WIDTH == 320 ) {
        //            sliderLbl = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 190, 15, 190, 15)];
        //            sliderLbl.font = [UIFont fontWithName:kFontHelveticaBold size:10];
        //            lblFont = [UIFont fontWithName:kFontHelveticaRegular size:10];
        //            cellView = [[UIView alloc]initWithFrame:CGRectMake(40, 30, SCREEN_WIDTH - 40, 40)];
        //            slider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,11,SCREEN_WIDTH - 60, 8)];
        //        }
        //        else
        //        {
        //            sliderLbl = [[UILabel alloc]initWithFrame:CGRectMake(375 - 230, 15, 230, 15)];
        //            sliderLbl.font = [UIFont fontWithName:kFontHelveticaBold size:11];
        //            lblFont = [UIFont fontWithName:kFontHelveticaRegular size:11];
        //            cellView = [[UIView alloc]initWithFrame:CGRectMake(40, 30, 375 - 40, 40)];
        //            slider = [[ASValueTrackingSlider alloc]initWithFrame:CGRectMake(10,11,375 - 60, 8)];
        //        }
        //
        //        sliderLbl.textColor = [UIColor whiteColor];
        //        sliderLbl.textAlignment = NSTextAlignmentCenter;
        //        [sliderLbl adjustsFontSizeToFitWidth];
        //        [((SRHomeMenuCustomCell *)cell).contentView addSubview:sliderLbl];
        //
        //        cellView.backgroundColor = [UIColor clearColor];
        //        slider.maximumValue = kSliderMaxValue;
        //        slider.minimumValue = kSliderMinValue;
        //        slider.value = kSliderMinValue;
        //        slider.minimumTrackTintColor = [UIColor blackColor];
        //        slider.maximumTrackTintColor = [UIColor blackColor];
        //        slider.popUpViewColor = [UIColor clearColor];
        //        slider.backgroundColor = [UIColor blackColor];
        //        slider.textColor = [UIColor clearColor];
        //        [slider setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
        //        slider.continuous = NO;
        //        [slider addTarget:self action:@selector(setSliderRoundValue) forControlEvents:UIControlEventValueChanged];
        //        [cellView addSubview:slider];
        //        [cellView bringSubviewToFront:slider];
        //
        //
        //        int x = 10,y = 25;
        //        minimalLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 45, 15)];
        //        minimalLbl.textColor = [UIColor blackColor];
        //        minimalLbl.font = lblFont;
        //        minimalLbl.text=NSLocalizedString(@"lbl.minimal.txt", @"");
        //        minimalLbl.textAlignment = NSTextAlignmentLeft;
        //        [cellView addSubview:minimalLbl];
        //
        //        if (SCREEN_WIDTH == 320 ) {
        //            x = minimalLbl.frame.size.width + 5;
        //        }
        //        else
        //            x = minimalLbl.frame.size.width + 15;
        //        veryLowLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 47, 15)];
        //        veryLowLbl.textColor = [UIColor blackColor];
        //        veryLowLbl.font = lblFont;
        //        veryLowLbl.text=NSLocalizedString(@"lbl.very_low.txt", @"");
        //        veryLowLbl.textAlignment = NSTextAlignmentCenter;
        //        [cellView addSubview:veryLowLbl];
        //
        //        if (SCREEN_WIDTH == 320 ) {
        //            x = x + veryLowLbl.frame.size.width + 5;
        //        }
        //        else
        //            x = x + veryLowLbl.frame.size.width + 15;
        //        lowLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 35, 15)];
        //        lowLbl.textColor = [UIColor blackColor];
        //        lowLbl.font = lblFont;
        //        lowLbl.text=NSLocalizedString(@"lbl.low.txt", @"");
        //        lowLbl.textAlignment = NSTextAlignmentCenter;
        //        [cellView addSubview:lowLbl];
        //
        //        if (SCREEN_WIDTH == 320 ) {
        //            x = x + lowLbl.frame.size.width + 5;
        //        }
        //        else
        //            x = x + lowLbl.frame.size.width + 15;
        //        medLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 55, 15)];
        //        medLbl.textColor = [UIColor blackColor];
        //        medLbl.font = lblFont;
        //        medLbl.text=NSLocalizedString(@"lbl.medium.txt", @"");
        //        [cellView addSubview:medLbl];
        //
        //        if (SCREEN_WIDTH == 320 ) {
        //            x = x + medLbl.frame.size.width + 5;
        //        }
        //        else
        //            x = x + medLbl.frame.size.width + 15;
        //        highLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 28, 15)];
        //        highLbl.textColor = [UIColor blackColor];
        //        highLbl.font = lblFont;
        //        highLbl.text=NSLocalizedString(@"lbl.high.txt", @"");
        //        [cellView addSubview:highLbl];
        //
        //        if (SCREEN_WIDTH == 320 ) {
        //            x = x + highLbl.frame.size.width + 5;
        //        }
        //        else
        //            x = x + highLbl.frame.size.width + 15;
        //        realTimeLbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, 55, 15)];
        //        realTimeLbl.textColor = [UIColor blackColor];
        //        realTimeLbl.font = lblFont;
        //        realTimeLbl.text=NSLocalizedString(@"lbl.real_time.txt", @"");
        //        [cellView addSubview:realTimeLbl];
        //
        //        [((SRHomeMenuCustomCell *)cell).contentView addSubview:cellView];
        //        [((SRHomeMenuCustomCell *)cell).contentView  bringSubviewToFront:slider];
        //        [self.view bringSubviewToFront:slider];
        //        [slider bringSubviewToFront:slider.superview.superview];
        //
        //        //Get and Set LocationUpdateTimeInterval and BattaryUsage form NSUserDefaults
        //        NSUserDefaults *updatedTimeDict = [NSUserDefaults standardUserDefaults];
        //        int locationUpdateTime = [[updatedTimeDict objectForKey:kKeyLocationUpdateTime] intValue];
        //        if (locationUpdateTime > 0) {
        //            slider.value = locationUpdateTime;
        //        }
        //
        //        //Set Selected Slider Value Label
        //        if (slider.value == 0) {
        //            minimalLbl.textColor = [UIColor whiteColor];
        //            sliderLbl.text= NSLocalizedString(@"lbl.battery_usage_min.txt", @"");
        //        }
        //        else if (slider.value == 34)
        //        {
        //            veryLowLbl.textColor = [UIColor whiteColor];
        //            sliderLbl.text= NSLocalizedString(@"lbl.battery_usage_very_low.txt", @"");
        //        }
        //        else if (slider.value == 68)
        //        {
        //            lowLbl.textColor = [UIColor whiteColor];
        //            sliderLbl.text= NSLocalizedString(@"lbl.battery_usage_low.txt", @"");
        //        }
        //        else if (slider.value == 102)
        //        {
        //            medLbl.textColor = [UIColor whiteColor];
        //            sliderLbl.text= NSLocalizedString(@"lbl.battery_usage_medium.txt", @"");
        //        }
        //        else if (slider.value == 136)
        //        {
        //            highLbl.textColor = [UIColor whiteColor];
        //            sliderLbl.text= NSLocalizedString(@"lbl.battery_usage_max.txt", @"");
        //        }
        //        else
        //        {
        //            realTimeLbl.textColor = [UIColor whiteColor];
        //            sliderLbl.text= NSLocalizedString(@"lbl.battery_usage_real_time.txt", @"");
        //        }
    } else if (indexPath.row == 13) {
        ((SRHomeMenuCustomCell *) cell).menuItemLableWidth.constant = SCREEN_WIDTH - 100;
        ((SRHomeMenuCustomCell *) cell).MenuItemLabel.textColor = [UIColor whiteColor];
        ((SRHomeMenuCustomCell *) cell).MenuItemLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
        ((SRHomeMenuCustomCell *) cell).MenuItemLabel.textAlignment = NSTextAlignmentCenter;
        int x;
        if (SCREEN_WIDTH == 414) {
            x = 155;
        } else if (SCREEN_WIDTH == 375) {
            x = 115;
        } else
            x = 85;
        UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 34, SCREEN_WIDTH, 44)];
        cellView.backgroundColor = [UIColor blackColor];
        
        UIButton *YahooBtn = [[UIButton alloc] initWithFrame:CGRectMake(x, 6, 30, 30)];
        [YahooBtn setImage:[UIImage imageNamed:@"social-yahoo.png"] forState:UIControlStateNormal];
        [YahooBtn addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        YahooBtn.tag = kBtnYahooTag;
        [cellView addSubview:YahooBtn];
        
        x = x + YahooBtn.frame.size.width + 9;
        UIButton *GooglePlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(x, 6, 30, 30)];
        [GooglePlusBtn setImage:[UIImage imageNamed:@"social-googleplus.png"] forState:UIControlStateNormal];
        [GooglePlusBtn addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        GooglePlusBtn.tag = kBtnGooglePlusTag;
        [cellView addSubview:GooglePlusBtn];
        
        x = x + GooglePlusBtn.frame.size.width + 9;
        
        
        UIButton *MessageBtn = [[UIButton alloc] initWithFrame:CGRectMake(x, 6, 30, 30)];
        [MessageBtn setImage:[UIImage imageNamed:@"social-outlook.png"] forState:UIControlStateNormal];
        [MessageBtn addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        MessageBtn.tag = kBtnMessageTag;
        [cellView addSubview:MessageBtn];
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:cellView];
        
        // Calculate width for button
        UIFont *font = [UIFont fontWithName:kFontHelveticaMedium size:12];
        NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                         NSForegroundColorAttributeName: [UIColor blackColor]};
        NSString *text = NSLocalizedString(@"lbl.Connect via phone.txt", @"");
        CGSize textSize = [text sizeWithAttributes:userAttributes];
        
        UIButton *connectPhoneBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, 70, SCREEN_WIDTH - 40, 80)];
        
        [connectPhoneBtn.titleLabel setFont:[UIFont fontWithName:kFontHelveticaMedium size:12]];
        [connectPhoneBtn.titleLabel setNumberOfLines:5];
        [connectPhoneBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [connectPhoneBtn.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        
        [connectPhoneBtn setTitle:NSLocalizedString(@"lbl.Connect via phone.txt", @"") forState:UIControlStateNormal];
        [connectPhoneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [connectPhoneBtn addTarget:self action:@selector(actionOnConnectPhoneBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:connectPhoneBtn];
        
        UIImageView *phoneImg = [[UIImageView alloc] initWithFrame:CGRectMake(textSize.width + connectPhoneBtn.frame.origin.x + 5, 85, 23, 23)];
        [phoneImg setImage:[UIImage imageNamed:@"black-phone.png"]];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionOnConnectPhoneBtnClick:)];
        singleTap.numberOfTapsRequired = 1;
        [phoneImg setUserInteractionEnabled:YES];
        [phoneImg addGestureRecognizer:singleTap];
        [((SRHomeMenuCustomCell *) cell).contentView addSubview:phoneImg];
    } else if (indexPath.row == 14) {
        ((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN.hidden = NO;
        ((SRHomeMenuCustomCell *) cell).MenuItemGoogleBTN.hidden = YES;
        ((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN.hidden = NO;
        ((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN.hidden = NO;
        [((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        ((SRHomeMenuCustomCell *) cell).MenuItemFacebookBTN.tag = kCellBtnFacebookTag;
        /*[((SRHomeMenuCustomCell *)cell).MenuItemGoogleBTN addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
         ((SRHomeMenuCustomCell *)cell).MenuItemGoogleBTN.tag = kCellBtnGoogleTag;*/
        [((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        ((SRHomeMenuCustomCell *) cell).MenuItemLinkedInBTN.tag = kCellBtnLinkedInTag;
        [((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN addTarget:self action:@selector(actionOnShareCellBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        ((SRHomeMenuCustomCell *) cell).MenuItemTwitterBTN.tag = kCellBtnTwitterTag;
    }
    
    if (indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16) {
        [((SRHomeMenuCustomCell *) cell).MenuIconImage setHidden:YES];
        ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame = CGRectMake(((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame.origin.x - 30, ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame.origin.y, ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame.size.width, ((SRHomeMenuCustomCell *) cell).MenuItemLabel.frame.size.height);
    }
    ((SRHomeMenuCustomCell *) cell).MenuItemLabel.text = menuItemArray[indexPath.row];
    ((SRHomeMenuCustomCell *) cell).MenuIconImage.image = [UIImage imageNamed:menuIconArray[indexPath.row]];
    ((SRHomeMenuCustomCell *) cell).MenuIconImage.contentMode = UIViewContentModeScaleAspectFit;
    ((SRHomeMenuCustomCell *) cell).clipsToBounds = YES;
    ((SRHomeMenuCustomCell *) cell).backgroundColor = [UIColor clearColor];
    
    // Return
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 13) {
        return 150;
    } else if ((indexPath.row == 6) && [APP_DELEGATE isUserBelow18]) {
//    } else if ((indexPath.row == 5 || indexPath.row == 6) && [APP_DELEGATE isUserBelow18]) {
        return 0;
        
    } else if (indexPath.row == 9) {
        return 175;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        SRTrackingViewController *trackingVC = [[SRTrackingViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:trackingVC animated:YES];
    } else if (indexPath.row == 2) {
        //        SRPingsTabViewController *pingsCon = [[SRPingsTabViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:server];
        SRPingsViewController *objSRPingsView = [[SRPingsViewController alloc] initWithNibName:nil bundle:nil server:server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRPingsView animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 3) {
        SRDropPinViewController *dropPin = [[SRDropPinViewController alloc] initWithNibName:nil bundle:nil server:server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:dropPin animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 4) {
        SRPrivacyViewController *objSRPrivacyViewController = [[SRPrivacyViewController alloc] initWithNibName:nil bundle:nil server:server];
        objSRPrivacyViewController.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objSRPrivacyViewController animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 5) {
        SRGroupTabViewController *objGroupTab = [[SRGroupTabViewController alloc] initWithNibName:nil bundle:nil];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objGroupTab animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 6) {
        
        SREventTabViewController *eventTab = [[SREventTabViewController alloc] initWithNibName:nil bundle:nil server:server className:nil];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:eventTab animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 7) {
        SRMyConnectionsViewController *connectionCon = [[SRMyConnectionsViewController alloc] initWithNibName:nil bundle:nil server:server];
        connectionCon.delegate = self;
        connectionCon.isShowInvisibleConnection = false;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:connectionCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 8) {
        self.hidesBottomBarWhenPushed = YES;
//        UIStoryboard *locationStoryBoard = [UIStoryboard storyboardWithName:@"LocationFirstResponse" bundle:nil];
//        SRContactPermissionVC *locationViewController = [locationStoryBoard instantiateViewControllerWithIdentifier:@"SRContactPermissionVC"];
//        locationViewController.delegate = self;
//        //locationViewController.
//        [locationViewController setModalPresentationStyle:UIModalPresentationFullScreen];
//        [self.navigationController pushViewController:locationViewController animated:YES];
//        [self.window.rootViewController presentViewController:locationViewController animated:YES completion:nil];
        
//        if (container) {
//            [container presentViewController:locationViewController animated:YES completion:nil];
//        } else {
//            [self.window.rootViewController presentViewController:locationViewController animated:YES completion:nil];
//        }
        
        
        SRContactsViewController *objContacts = [[SRContactsViewController alloc] initWithNibName:@"fromMenuView" bundle:nil profileData:nil server:(APP_DELEGATE).server];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objContacts animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 10) {
        SRActivityItemProvider *obj = [[SRActivityItemProvider alloc] initWithPlaceholderItem:@""];
        NSArray *activityItems = @[obj];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityVC.excludedActivityTypes = @[UIActivityTypePrint];
            [self presentViewController:activityVC animated:TRUE completion:nil];
        });
    } else if (indexPath.row == 11) {
        SRLearnViewController *learnCon = [[SRLearnViewController alloc] initWithNibName:nil bundle:nil profileData:nil server:nil];
        learnCon.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:learnCon animated:NO];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 12) {
        SRAccountSettingsViewController *accountSetCon = [[SRAccountSettingsViewController alloc] initWithNibName:nil bundle:nil server:server];
        accountSetCon.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:accountSetCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 15) {
        SRTerms_ConditionsViewController *termsCon = [[SRTerms_ConditionsViewController alloc] initWithNibName:@"Terms & Policy1" bundle:nil server:server];
        termsCon.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:termsCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 16) {
        SRTerms_ConditionsViewController *termsCon = [[SRTerms_ConditionsViewController alloc] initWithNibName:@"Terms & Policy3" bundle:nil server:server];
        termsCon.delegate = self;
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:termsCon animated:YES];
        self.hidesBottomBarWhenPushed = YES;
    } else if (indexPath.row == 17) {
        //Email the location logs
        [[SLocationService sharedInstance] emailLocationLogs];
    }else if (indexPath.row == 18) {
//        SRlocationHistoryVC *locationHistory = [[SRlocationHistoryVC alloc] initWithNibName:@"SRlocationHistoryVC" bundle:nil server:server];
//        self.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:locationHistory animated:YES];
        self.hidesBottomBarWhenPushed = YES;
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"TrackingRequest" bundle:nil];
            SRlocationHistoryVC *controller = [s instantiateViewControllerWithIdentifier:@"SRlocationHistoryVC"];
        //controller->server = server;
            [self.navigationController pushViewController:controller animated:true];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.row == 10) {
    //        SRBatterySettingsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //        [cell updateUI];
    //    }
}

#pragma mark
#pragma mark Actions On CellBtnClick
#pragma mark

- (void)setSliderRoundValue {
    NSString *batterySetting;
    // get the nearest interval value
    for (int i = kSliderMinValue; i <= kSliderMaxValue; i = i + kSliderInterval) {
        
        if (slider.value >= i && slider.value < i + kSliderInterval) {
            if (slider.value - i < i + kSliderInterval - slider.value) {
                slider.value = i;
            } else {
                slider.value = i + kSliderInterval;
            }
            
            if (slider.value == 0) {
                minimalLbl.textColor = [UIColor whiteColor];
                veryLowLbl.textColor = [UIColor blackColor];
                lowLbl.textColor = [UIColor blackColor];
                medLbl.textColor = [UIColor blackColor];
                highLbl.textColor = [UIColor blackColor];
                realTimeLbl.textColor = [UIColor blackColor];
                sliderLbl.text = NSLocalizedString(@"lbl.battery_usage_min.txt", @"");
                batterySetting = NSLocalizedString(@"lbl.minimal.txt", @"");
            } else if (slider.value == 34) {
                minimalLbl.textColor = [UIColor blackColor];
                veryLowLbl.textColor = [UIColor whiteColor];
                lowLbl.textColor = [UIColor blackColor];
                medLbl.textColor = [UIColor blackColor];
                highLbl.textColor = [UIColor blackColor];
                realTimeLbl.textColor = [UIColor blackColor];
                sliderLbl.text = NSLocalizedString(@"lbl.battery_usage_very_low.txt", @"");
                batterySetting = NSLocalizedString(@"lbl.very_low.txt", @"");
            } else if (slider.value == 68) {
                minimalLbl.textColor = [UIColor blackColor];
                veryLowLbl.textColor = [UIColor blackColor];
                lowLbl.textColor = [UIColor whiteColor];
                medLbl.textColor = [UIColor blackColor];
                highLbl.textColor = [UIColor blackColor];
                realTimeLbl.textColor = [UIColor blackColor];
                sliderLbl.text = NSLocalizedString(@"lbl.battery_usage_low.txt", @"");
                batterySetting = NSLocalizedString(@"lbl.low.txt", @"");
            } else if (slider.value == 102) {
                minimalLbl.textColor = [UIColor blackColor];
                veryLowLbl.textColor = [UIColor blackColor];
                lowLbl.textColor = [UIColor blackColor];
                medLbl.textColor = [UIColor whiteColor];
                highLbl.textColor = [UIColor blackColor];
                realTimeLbl.textColor = [UIColor blackColor];
                sliderLbl.text = NSLocalizedString(@"lbl.battery_usage_medium.txt", @"");
                batterySetting = NSLocalizedString(@"lbl.medium.txt", @"");
            } else if (slider.value == 136) {
                minimalLbl.textColor = [UIColor blackColor];
                veryLowLbl.textColor = [UIColor blackColor];
                lowLbl.textColor = [UIColor blackColor];
                medLbl.textColor = [UIColor blackColor];
                highLbl.textColor = [UIColor whiteColor];
                realTimeLbl.textColor = [UIColor blackColor];
                sliderLbl.text = NSLocalizedString(@"lbl.battery_usage_max.txt", @"");
                batterySetting = NSLocalizedString(@"lbl.high.txt", @"");
            } else {
                minimalLbl.textColor = [UIColor blackColor];
                veryLowLbl.textColor = [UIColor blackColor];
                lowLbl.textColor = [UIColor blackColor];
                medLbl.textColor = [UIColor blackColor];
                highLbl.textColor = [UIColor blackColor];
                realTimeLbl.textColor = [UIColor whiteColor];
                sliderLbl.text = NSLocalizedString(@"lbl.battery_usage_real_time.txt", @"");
                batterySetting = NSLocalizedString(@"lbl.real_time.txt", @"");
            }
            // Set locationUpdateTime interval
            NSUserDefaults *updatedTimeDict = [NSUserDefaults standardUserDefaults];
            [updatedTimeDict setObject:[NSString stringWithFormat:@"%f", slider.value] forKey:kKeyLocationUpdateTime];
            [updatedTimeDict synchronize];
            
            
            // Update setting
            NSDictionary *payload = @{
                kKeyFieldName: kkeyBatteryUsage,
                kKeyFieldValue: batterySetting
            };
            NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
            [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
            
            break;
        }
    }
}

- (void)changeSwitch:(UISwitch *)sender {
    if ([sender tag] == 0) {
        NSString *brodcast_Loc;
        if (sender.isOn) {
            brodcast_Loc = @"1";
        } else {
            brodcast_Loc = @"0";
        }
        NSDictionary *payload = @{
            kKeyFieldName: kkeyBroadcastLocation,
            kKeyFieldValue: brodcast_Loc
        };
        
        NSString *url = [NSString stringWithFormat:@"%@", kKeyClassSetting];
        [server makeAsychronousRequest:url inParams:payload isIndicatorRequired:NO inMethodType:kPOST];
    } else if ([sender tag] == 1) {
        NSString *smart_battery;
        if (sender.isOn) {
            smart_battery = @"1";
            // [SRModalClass showAlert:@"Turning on saves battery by disabling connection location updates from Midnight to 6 am."];
        } else {
            smart_battery = @"0";
        }
        
        [[server.loggedInUserInfo objectForKey:kKeySetting] setValue:smart_battery forKey:kkeytracking_me];
        [[SRAPIManager sharedInstance] updateSettings:kkeytracking_me withValue:smart_battery];
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnConnectPhoneBtnClick
- (void)actionOnConnectPhoneBtnClick:(UIButton *)sender {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    isPhonebtnClicked = YES;
    [defaultCenter addObserver:self
                      selector:@selector(contactUploadSuccess:)
                          name:kKeyNotificationContactUploadSucceed
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(contactUploadFailed:)
                          name:kKeyNotificationContactUploadFailed object:nil];
    [self getAllContacts];
}

#pragma mark
#pragma mark UIViewcontroller delegate
#pragma mark

// ------------------------------------------------------------------------------------------------------------------
- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] pushViewController:viewController animated:YES];
}

#pragma mark
#pragma mark Google sign in Delegate
#pragma mark

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (!error) {
        [APP_DELEGATE showActivityIndicator];
        // get user contact
        [self getGmailcontact:user];
    }
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
}

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:^{
        [APP_DELEGATE hideActivityIndicator];
    }];
}

- (void)getGmailcontact:(GIDGoogleUser *)user {
    // [APP_DELEGATE hideActivityIndicator];
    NSString *urlStr;
    
    // Google Contacts feed
    //
    //    https://www.googleapis.com/oauth2/v2/userinfo
    urlStr = [NSString stringWithFormat:@"https://www.google.com/m8/feeds/contacts/default/full?access_token=%@&max-results=999&alt=json&v=3.0" , user.authentication.accessToken];
    
    //    https://www.google.com/m8/feeds/contacts/default/full/?access_token=' +
    //    authResult.access_token + "&alt=json&callback=?"
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = 60;
    [request setHTTPMethod:@"GET"];
    // [request setValue:@"3.0" forHTTPHeaderField:@"GData-Version"];
    // [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    
    //NSString *output = nil;
    NSError *error = nil;
    
    NSURLResponse *response = nil;
    
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    if (data) {
        // API fetch succeeded
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        
        if ([dict[@"feed"][@"entry"] count]) {
            NSArray *arrContact = dict[@"feed"][@"entry"];
            contactList = [[NSMutableArray alloc] init];
            [(APP_DELEGATE) showActivityIndicator];
            //parse data
            for (int i = 0; i < [arrContact count]; i++) {
                NSString *strFirstName = @"", *strLastName = @"", *strMobile = @"";
                NSDictionary *dictMobile;
                NSDictionary *dictContact = arrContact[i];
                NSDictionary *dictName = [dictContact valueForKey:@"gd$name"];
                if (dictName != nil) {
                    //gd$fullName
                    NSDictionary *dictFirstName = [dictName valueForKey:@"gd$givenName"];
                    if ([dictFirstName valueForKey:@"$t"]){
                        strFirstName = [dictFirstName valueForKey:@"$t"];
                    }
                    
                    NSDictionary *dictLastName = [dictName valueForKey:@"gd$familyName"];
                    if ([dictLastName valueForKey:@"$t"] != nil) {
                        strLastName = [dictLastName valueForKey:@"$t"];
                    }
                    
                }
                NSArray *arrMobile = [dictContact valueForKey:@"gd$phoneNumber"];
                if ([arrMobile count] > 0) {
                    dictMobile = arrMobile[0];
                    strMobile = [dictMobile valueForKey:@"uri"];
                    NSString *stringWithoutSpaces = [strMobile
                    stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    strMobile = [stringWithoutSpaces
                    stringByReplacingOccurrencesOfString:@"tel:" withString:@""];
                }
                
                //                NSArray *arrEmail = [dictContact valueForKey:@"gd$email"];
                //                if ([arrEmail count] > 0) {
                //                    dictEmail = arrEmail[0];
                //                    strEmail = [dictEmail valueForKey:@"address"];
                //                }
                if (![strMobile  isEqual: @""] && strMobile != nil){
                    NSMutableDictionary *dictUserData = [[NSMutableDictionary alloc] init];
                    [dictUserData setValue:strFirstName forKey:kKeyFirstName];
                    [dictUserData setValue:strLastName forKey:kKeyLastName];
                    //            [dictUserData setValue:strEmail forKey:kKeyEmail];
                    [dictUserData setValue:strMobile forKey:kKeyMobileNumber];
                    [contactList addObject:dictUserData];
                }
                
            }
            
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
            if (contactList.count > 0) {
                [dataDict setValue:contactList forKey:kKeyContacts];
                [dataDict setValue:kKeyGmailValue forKey:kKeyContacTyype];
                [server makeSynchronousRequest:kKeyClassUserContactsStore inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
            } else {
                [(APP_DELEGATE) hideActivityIndicator];
            }
            [GIDSignIn.sharedInstance signOut];
        }
    } else {
        // fetch failed
        [SRModalClass showAlert:@"None of your contacts are currently on Serendipity.  We will let you know when your contacts join.  (put Okay button below the text)"];
        //output = [error description];
    }
}

-(void)yahooContactsImported:(NSNotification*)notification {
    NSMutableArray *contacts = [notification object];
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    if (contacts != nil) {
        if (contacts.count > 0) {
            
            [dataDict setValue:contacts forKey:kKeyContacts];
            [dataDict setValue:kKeyYahooValue forKey:kKeyContacTyype];
            [server makeSynchronousRequest:kKeyClassUserContactsStore inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
        } else {
            [(APP_DELEGATE) hideActivityIndicator];
        }
    }
}


#pragma mark
#pragma mark Yahoo Delegate Method
#pragma mark
// -------------------------------------------------------------------------------------------------------------------------------------
// createYahooSession

-(void)didAuthorized:(NSDictionary *)dictResponse {
   // NSLog(@"Authorized");
}

- (void)createYahooSession {
    
    yahoo = [YahooSwift new];
    [yahoo createSessionWithConsumerKey:YahooConsumerkey secretKey:YahooConsumerSecret callbackUrl:YahooCallBackUrl authorizeUrl:@"https://api.login.yahoo.com/oauth2/request_auth" viewController:self];

}




#pragma mark- get notification from yahoo

- (void)didReceiveAuthorization {
    [self createYahooSession];
}



#pragma mark
#pragma mark- Hotmail live Delegate Method
#pragma mark

// -------------------------------------------------------------------------------------------------
// getHotmailContacts

- (void)getHotmailContacts {
    [APP_DELEGATE hideActivityIndicator];
    [APP_DELEGATE hsshowActivityIndicator];
    
    HSOutlookSignIn *outlook = [HSOutlookSignIn shared];
    [outlook callGraphAPIWithVc:self block:^(BOOL status, NSArray<NSDictionary<NSString *,id> *> * contacts, NSString * message) {
        [outlook signOut];

        if(status && contacts.count > 0){
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
            [dataDict setValue:contacts forKey:kKeyContacts];
            [dataDict setValue:kKeyOutlookValue forKey:kKeyContacTyype];
            [server makeSynchronousRequest:kKeyClassUserContactsStore inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
        }else{
            [APP_DELEGATE hshideActivityIndicator];
            if (![message isEqualToString:@"Cancel"]){
                [SRModalClass showAlert:message];
            }
            [(APP_DELEGATE) hideActivityIndicator];
        }
    }];
}

// ----------------------------------------------------------------------------------------------------
// loginWithScopes:

- (void)loginWithScopes:(NSArray *)scopes {
    @try {
        [self.liveClient login:self
                        scopes:scopes
                      delegate:self
                     userState:@"login"];
    }
    @catch (id ex) {
        
    }
}

// ------------------------------------------------------------------------------------------------------
// getLivemailContacts

- (void)getLivemailContacts {
    if (self.liveClient.session) {
        [self.liveClient getWithPath:@"me/contacts"
                            delegate:self
                           userState:@"get"];
    } else {
        [APP_DELEGATE hideActivityIndicator];
    }
    
}

#pragma mark LiveAuthDelegate

- (void)authCompleted:(LiveConnectSessionStatus)status session:(LiveConnectSession *)session
            userState:(id)userState {
    
    [APP_DELEGATE showActivityIndicator];
    [self getLivemailContacts];
}

// ------------------------------------------------------------------------------------------------------
// authFailed:

- (void)authFailed:(NSError *)error
         userState:(id)userState {
    
    [APP_DELEGATE hideActivityIndicator];
}

#pragma mark LiveOperationDelegate

// -------------------------------------------------------------------------------------------------------
// liveOperationSucceeded

- (void)liveOperationSucceeded:(LiveOperation *)operation {
    [APP_DELEGATE hideActivityIndicator];
    
    _contactType = @"hotmail";
    
    if ([operation.result[@"data"] count]) {
    } else {
        [SRModalClass showAlert:@"None of your contacts are currently on Serendipity.  We will let you know when your contacts join.  (put Okay button below the text)"];
    }
}

// -------------------------------------------------------------------------------------------------------------------------------------
// liveOperationFailed

- (void)liveOperationFailed:(NSError *)error operation:(LiveOperation *)operation {
    [APP_DELEGATE hideActivityIndicator];
    // [SRModalClass showAlert:@"Error in connection"];
}


#pragma mark
#pragma mark Actions On ShareCellBtnClick
#pragma mark
// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnShareCellBtnClick

- (void)actionOnShareCellBtnClick:(UIButton *)btnSender {
    [APP_DELEGATE showActivityIndicator];
    if (btnSender.tag == kBtnYahooTag) {
        [self createYahooSession];
    } else if (btnSender.tag == kBtnGooglePlusTag) {
        [[GIDSignIn sharedInstance] signIn];
    }else if (btnSender.tag == kBtnMessageTag) {
        [self getHotmailContacts];
    } else if (btnSender.tag == kCellBtnFacebookTag) {
        
        NSURL *facebookURL = [NSURL URLWithString:@"https://www.facebook.com/serendipityawaits1/"];
        if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
            
            [[UIApplication sharedApplication] openURL:facebookURL
                                               options:@{}
                                     completionHandler:nil];
            
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"]
                                               options:@{}
                                     completionHandler:nil];
            
        }
    } else if (btnSender.tag == kCellBtnGoogleTag) {
        
        NSURL *googlePlusURL = [NSURL URLWithString:@"https://plus.google.com/u/0/117679139997961181842?hl"];
        if ([[UIApplication sharedApplication] canOpenURL:googlePlusURL]) {
            
            [[UIApplication sharedApplication] openURL:googlePlusURL options:@{} completionHandler:nil];
            
        } else {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"] options:@{} completionHandler:nil];
        }
    } else if (btnSender.tag == kCellBtnLinkedInTag) {
        
        NSURL *linkedInURL = [NSURL URLWithString:@"https://www.linkedin.com/company/10124782"];
        if ([[UIApplication sharedApplication] canOpenURL:linkedInURL]) {
            [[UIApplication sharedApplication] openURL:linkedInURL options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"] options:@{} completionHandler:nil];
            
        }
    } else if (btnSender.tag == kCellBtnTwitterTag) {
        
        NSURL *twitterURL = [NSURL URLWithString:@"https://twitter.com/serendipity1on1"];
        if ([[UIApplication sharedApplication] canOpenURL:twitterURL]) {
            
            [[UIApplication sharedApplication] openURL:twitterURL options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"] options:@{} completionHandler:nil];
        }
    }
}

- (void)getAllContacts {
    //ABAddressBookRef m_addressbook = ABAddressBookCreate();
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    // ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    } else {
        // We are on iOS 5 or Older
        accessGranted = YES;
        [self getContactsWithAddressBook:addressBook];
    }
    if (accessGranted) {
        [self getContactsWithAddressBook:addressBook];
    }
    
    // Send contact list to server
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    if (contactList.count > 0) {
        ///kKeyClassUserContactsStore
        [dataDict setValue:contactList forKey:kKeyGroupUsers];
        [dataDict setValue:kKeyContacts forKey:kKeyType];
        [server makeSynchronousRequest:kKeyClassUploadContacts inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
        // [server makeAsychronousRequest:kKeyClassUploadContacts inParams:dataDict isIndicatorRequired:YES inMethodType:kPOST];
    }
}

//-----------------------------------------------------------------------------------------------------------------------
// getContactsWithAddressBook:

- (void)getContactsWithAddressBook:(ABAddressBookRef)addressBook {
    contactList = [[NSMutableArray alloc] init];
    
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    
    for (int i = 0; i < nPeople; i++) {
        NSMutableDictionary *dOfPerson = [NSMutableDictionary dictionary];
        NSMutableDictionary *listPerson = [NSMutableDictionary dictionary];
        
        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople, i);
        
        //For username and surname
        ABMultiValueRef phones = (__bridge ABMultiValueRef) ((__bridge NSString *) ABRecordCopyValue(ref, kABPersonPhoneProperty));
        
        
        NSString *firstNameStr = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonFirstNameProperty) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        
        if (firstNameStr.length > 0) {
            dOfPerson[kKeyFirstName] = firstNameStr;
        } else
            dOfPerson[kKeyFirstName] = @"";
        
        NSString *lastNameStr = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonLastNameProperty) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonLastNameProperty);
        
        if (lastNameStr.length > 0) {
            dOfPerson[kKeyLastName] = lastNameStr;
        } else {
            dOfPerson[kKeyLastName] = @"";
        }
        
        //For Email ids
        ABMutableMultiValueRef eMail = ABRecordCopyValue(ref, kABPersonEmailProperty);
        if (ABMultiValueGetCount(eMail) > 0) {
            dOfPerson[kKeyEmail] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(eMail, 0);
            
            [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(eMail, 0) forKey:kKeyEmail];
        }
        CFRelease(eMail);
        
        //For Phone number List
        NSString *mobileLabel;
        for (CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
            mobileLabel = (__bridge_transfer NSString *) ABMultiValueCopyLabelAtIndex(phones, i);
            if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneMobileLabel]) {
                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);
                
                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
            } else if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneIPhoneLabel]) {
                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);
                
                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
            }
        }
        CFRelease(phones);
        
        
        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil) {
            
            NSString *mobileNo = [SRModalClass RemoveSpecialCharacters:[dOfPerson valueForKey:kKeyMobileNumber]];
            if (mobileNo.length > 10) {
                mobileNo = [mobileNo substringWithRange:NSMakeRange(mobileNo.length - 10, 10)];
            }
            [dOfPerson setValue:mobileNo forKey:kKeyMobileNumber];
        }
        
        if ([dOfPerson valueForKey:kKeyEmail] == nil) {
            [dOfPerson setValue:@"" forKey:kKeyEmail];
        }
        // If contact number is not nil then only add it in array
        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil) {
            [contactList addObject:dOfPerson];
        }
        
    }
    CFRelease(allPeople);
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getProfileDetailsSucceed:

- (void)getProfileDetailsSucceed:(NSNotification *)inNotify {
    // Display Data
    [self displayData];
}

// --------------------------------------------------------------------------------
// getProfileDetailsFailed:

- (void)getProfileDetailsFailed:(NSNotification *)inNotify {
}

// ----------------------------------------------------------------------------------
// getNotificationSucceed:

- (void)getNotificationSucceed:(NSNotification *)inNotify {
    notificationsList = server.notificationArr;
    
    // Reload table
    [self.objtableView reloadData];
}

// --------------------------------------------------------------------------------
// getNotificationFailed:

- (void)getNotificationFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// pingsNotificationCount:

- (void)pingsNotificationCount:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// contactUploadSuccess:
- (void)contactUploadSuccess:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    isPhonebtnClicked = NO;
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self name:kKeyNotificationContactUploadSucceed object:nil];
    [defaultCenter removeObserver:self name:kKeyNotificationContactUploadFailed object:nil];
    [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import.text", @"")];
}

// --------------------------------------------------------------------------------
// contactUploadFailed:
- (void)contactUploadFailed:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import_fail.txt", @"")];
}
// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:


- (void)settingUpdateSucceed:(NSNotification *)inNotify {
    
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSString *broadCastLoc = [response valueForKey:kkeyBroadcastLocation];
    NSString *smart_battery = [response valueForKey:kkeySmartBatteryUsage];
    NSString *battery_Usage = [response valueForKey:kkeyBatteryUsage];
    NSDictionary *requestParam = [[inNotify userInfo] valueForKey:kKeyRequestedParams];
    if ([[requestParam valueForKey:@"field_name"] isEqualToString:kkeyBatteryUsage]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationBatteryUsageUpdate object:nil userInfo:nil];
    }
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:broadCastLoc forKey:kkeyBroadcastLocation];
    [userDef setValue:smart_battery forKey:kkeySmartBatteryUsage];
    NSMutableDictionary *dictSetting;
    dictSetting = [[server.loggedInUserInfo valueForKey:kKeySetting] mutableCopy];
    if (dictSetting == nil)
        dictSetting = [[NSMutableDictionary alloc] init];
    [dictSetting setValue:broadCastLoc forKey:kkeyBroadcastLocation];
    [dictSetting setValue:smart_battery forKey:kkeySmartBatteryUsage];
    [dictSetting setValue:battery_Usage forKey:kkeyBatteryUsage];
    server.loggedInUserInfo[kKeySetting] = dictSetting;
    [userDef synchronize];
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {
    
}

// --------------------------------------------------------------------------------
// updatedLocation:

- (void)updatedLocation:(NSNotification *)inNotify {
    if ([[server.loggedInUserInfo valueForKey:kKeyAddress] length] != 0) {
        NSString *lblStr = @"----";
        if ([server.loggedInUserInfo[kKeyDOB] length] > 0) {
            NSString *birthDate = server.loggedInUserInfo[kKeyDOB];
            NSDate *todayDate = [NSDate date];
            
            NSDate *dobDate = [SRModalClass returnDateInYYYYMMDD:birthDate];
            birthDate = [SRModalClass returnStringInMMDDYYYY:dobDate];
            dobDate = [SRModalClass returnDateInMMDDYYYY:birthDate];
            
            int time = [todayDate timeIntervalSinceDate:dobDate];
            int allDays = (((time / 60) / 60) / 24);
            int days = allDays % 365;
            int years = (allDays - days) / 365;
            
            lblStr = [NSString stringWithFormat:@"%d, %@", years, [server.loggedInUserInfo valueForKey:kKeyAddress]];
        } else {
            lblStr = [NSString stringWithFormat:@"%@", [server.loggedInUserInfo valueForKey:kKeyAddress]];
        }
        self.lblAddress.text = lblStr;
    } else if ([[server.loggedInUserInfo valueForKey:kKeyLocation] isKindOfClass:[NSDictionary class]]) {
        NSString *lblStr = @"----";
        NSString *locationText = [[server.loggedInUserInfo valueForKey:kKeyLocation] valueForKey:kKeyLiveAddress];
        if ([locationText isEqual:[NSNull null]]) {
            locationText = @"";
        }
        if ([locationText length] != 0) {
            if ([server.loggedInUserInfo[kKeyDOB] length] > 0) {
                NSString *birthDate = server.loggedInUserInfo[kKeyDOB];
                NSDate *todayDate = [NSDate date];
                
                NSDate *dobDate = [SRModalClass returnDateInYYYYMMDD:birthDate];
                birthDate = [SRModalClass returnStringInMMDDYYYY:dobDate];
                dobDate = [SRModalClass returnDateInMMDDYYYY:birthDate];
                
                int time = [todayDate timeIntervalSinceDate:dobDate];
                int allDays = (((time / 60) / 60) / 24);
                int days = allDays % 365;
                int years = (allDays - days) / 365;
                
                lblStr = [NSString stringWithFormat:@"%d, %@", years, [[server.loggedInUserInfo valueForKey:kKeyLocation] valueForKey:kKeyLiveAddress]];
            } else {
                lblStr = [NSString stringWithFormat:@"%@", [[server.loggedInUserInfo valueForKey:kKeyLocation] valueForKey:kKeyLiveAddress]];
            }
            self.lblAddress.text = lblStr;
        } else {
            self.lblAddress.text = (APP_DELEGATE).lblLocationText;
        }
    } else {
        self.lblAddress.text = (APP_DELEGATE).lblLocationText;
    }
}

- (void)contactUploadSuccessNotifier:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    NSArray *importContactArr = [notification object];
    
    if (importContactArr.count > 0) {
        importContactArray = [[NSMutableArray alloc] initWithArray:importContactArr];
        server.importContactArr = importContactArray;
        if (importContactArray.count > 0) {
            for (int i = 0; i < importContactArray.count; i++) {
                NSDictionary *dict = importContactArray[i];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@ || %K == %@", kKeyMobileNumber, [dict valueForKey:kKeyMobileNumber], kKeyEmail, [dict valueForKey:kKeyEmail]];
                NSArray *filteredArr = [ContactNumberList filteredArrayUsingPredicate:predicate];
                if (filteredArr.count > 0) {
                    int index = (int) [ContactNumberList indexOfObject:[filteredArr firstObject]];
                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyName] forKey:kKeyName];
                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyFirstName] forKey:kKeyFirstName];
                    [dict setValue:[ContactNumberList[index] valueForKey:kKeyLastName] forKey:kKeyLastName];
                    ContactNumberList[index] = dict;
                }
            }
        }
        server.contactList = ContactNumberList;
    }
    if (!isPhonebtnClicked)
        [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import.text", @"")];
}



-(void)getContactStoreSuccessNotifier:(NSNotification *)notification {
    // [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import.text", @"")];
    [self showAlertController:NSLocalizedString(@"alert.contact_import.text", @"") alertTitle:@"Contact Sync" notify:notification];
    
}

-(void)showAlertController:(NSString *)message alertTitle :(NSString*)title notify :(NSNotification *)notification{
    [APP_DELEGATE hshideActivityIndicator];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
        [self MoveToSocialContact: notification];
        
    }];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)MoveToSocialContact:(NSNotification *)notification {
    SocialContactListViewController *objSocialContactListView =  [[SocialContactListViewController alloc] initWithNibName:nil bundle:nil inDict:nil server:server];
    objSocialContactListView.delegate = self;
    self.hidesBottomBarWhenPushed = YES;
    if ([notification.userInfo[@"requestedParams"][@"contact_type"]  isEqual: @1]){
        objSocialContactListView.typeOfScreen = GmailContact;
    }else if ([notification.userInfo[@"requestedParams"][@"contact_type"]  isEqual: @2]){
        objSocialContactListView.typeOfScreen = YahooContact;
    }else if ([notification.userInfo[@"requestedParams"][@"contact_type"]  isEqual: @0]){
        objSocialContactListView.typeOfScreen = PhoneContact;
    }else if ([notification.userInfo[@"requestedParams"][@"contact_type"]  isEqual: @3]){
        objSocialContactListView.typeOfScreen = Outlook;
    }
    
    [self.navigationController pushViewController:objSocialContactListView animated:YES];
    self.hidesBottomBarWhenPushed = YES;
}

-(void)getContactStoreFailureNotifier:(NSNotification *)notification {
    [APP_DELEGATE hshideActivityIndicator];
    [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import_fail.txt", @"")];
}


- (void)contactUploadFailedNotifier:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    if (!isPhonebtnClicked)
        [SRModalClass showAlert:NSLocalizedString(@"alert.contact_import_fail.txt", @"")];
}
- (void)actionOnRefresh:(UIButton *)btnSender {
     self.hidesBottomBarWhenPushed = YES;
     UIStoryboard *locationStoryBoard = [UIStoryboard storyboardWithName:@"LocationFirstResponse" bundle:nil];
     SRContactPermissionVC *locationViewController = [locationStoryBoard instantiateViewControllerWithIdentifier:@"SRContactPermissionVC"];
     locationViewController.delegate = self;
     locationViewController.isForMenu = @"yes";
     [locationViewController setModalPresentationStyle:UIModalPresentationFullScreen];
     [self.navigationController pushViewController:locationViewController animated:YES];
//     [self.window.rootViewController presentViewController:locationViewController animated:YES completion:nil];
}
//jonish sprint 2
- (void)filterInvisibleConnection:(UIButton *)btnSender {
    
    SRMyConnectionsViewController *connectionCon = [[SRMyConnectionsViewController alloc] initWithNibName:nil bundle:nil server:server];
    connectionCon.delegate = self;
    connectionCon.isShowInvisibleConnection = true;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:connectionCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;
}
//jonish sprint 2

@end
