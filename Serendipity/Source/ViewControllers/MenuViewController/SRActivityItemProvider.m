//
//  SRActivityItemProvider.m
//  Serendipity
//
//  Created by Madhura on 28/03/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import "SRActivityItemProvider.h"

@implementation SRActivityItemProvider

- (id)activityViewController:(UIActivityViewController *)activityViewController
         itemForActivityType:(NSString *)activityType {
    if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
        NSString *strFrst = @"Hi, please join Serendipity,where you can:";
        NSString *strSec = @"• Keep track of your friends & family anywhere in the world in real time!";
        NSString *strThird = @"• Chat with your friends & get free video calling.";
        NSString *strFourth = @"Say goodbye to missed connections & lost family and friends. Join today at";
        NSString *strfifth = @"http://www.SerendipityAwaits.com";
        NSString *texttoshare = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@", @"Hi,", strFrst, strSec, strThird, strFourth, strfifth];
        return texttoshare;
    } else {
        NSString *strFrst = @"I would like you to join Serendipity, an app where you do so many cool things such as:";
        NSString *strSec = @"• Keep track of your friends and family anywhere in the world in real time!";
        NSString *strThird = @"• View how you're connected to others through our six degrees of separation functionality.";
        NSString *strFourth = @"• Maintain your privacy which allows to keep your location a secret at anytime.";
        NSString *strFifth = @"Say goodbye to missed connections and lost family and friends. Join today at";
        NSString *strSix = @"http://www.SerendipityAwaits.com and make sure your friends and family are safe at all times!";
        NSString *texttoshare = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@", @"Hello,", strFrst, strSec, strThird, strFourth, strFifth, strSix];
        return texttoshare;
    }
    return nil;
}
@end
