#import "SRModalClass.h"
#import "SRLearnViewController.h"

@implementation SRLearnViewController

#pragma mark
#pragma mark Private Method
#pragma mark

#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRLearnViewController" bundle:nibBundleOrNil];

    if (self) {
        // Custom initialization
    }

    // Return
    return self;
}

#pragma mark
#pragma mark Standard overrides
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    // Set title
    [SRModalClass setNavTitle:NSLocalizedString(@"lbl.Learn More.txt", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    // Prepare video play
    [APP_DELEGATE showActivityIndicator];
//    NSURL *targetURL = [NSURL URLWithString:@"http://serendipityawaits.com/mobile/source.html"];
    
    NSURL *targetURL = [NSURL URLWithString:@"http://serendipity.app/mobile/source.html"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    self.videoWebView.navigationDelegate = self;
    [self.videoWebView loadRequest:request];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#

#pragma mark -
#pragma mark - WKWebview Delegate Methods

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
  //  NSLog(@"Failed: %@", error);
    if ([error code] == NSURLErrorCancelled) {
        return;
    }
    [[[UIAlertView alloc] initWithTitle:@"Can't find server" message:@"Check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];

}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [APP_DELEGATE hideActivityIndicator];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 20; // set as you want
}

#

#pragma mark -
#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    self.videoWebView.scrollView.maximumZoomScale = 20; // set similar to previous.
}


#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// backBtnAction:

- (void)backBtnAction:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

@end
