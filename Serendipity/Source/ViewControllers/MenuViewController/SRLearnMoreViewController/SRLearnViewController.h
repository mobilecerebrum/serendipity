//
//  SRLearnViewController.h
//  Serendipity
//
//  Created by Sunil Dhokare on 05/06/1937 SAKA.
//  Copyright (c) 1937 SAKA Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRLearnViewController : ParentViewController<WKNavigationDelegate,UIScrollViewDelegate>
{
    //

}
@property(weak)id delegate;
// Other properties
@property (strong, nonatomic) IBOutlet WKWebView *videoWebView;
@property (strong, nonatomic) AVPlayer *videoPlayer;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          profileData:(NSDictionary *)inProfileDict
               server:(SRServerConnection *)inServer;
@end
