//
//  PhoneConatactInviteStep1VC.h
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhoneConatactInviteStep1VC : UIViewController
{
       SRServerConnection *server;
       NSMutableArray *serendiptyContact;
       NSInteger totalRecord;
       NSMutableArray *contactList, *importContactArray;
       NSMutableArray *ContactNumberList;
}
@property (weak, nonatomic) IBOutlet UIView *viewSelectAll;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll;
@property (weak, nonatomic) IBOutlet UIButton *btnSendConnectionRequest;

- (id)initWithNibName:(NSString *)nibNameOrNil
bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer;

@end

NS_ASSUME_NONNULL_END
