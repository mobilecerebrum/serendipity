//
//  PhoneConatactInviteStep2VC.h
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhoneConatactInviteStep2VC : UIViewController<UITextFieldDelegate>
{
       SRServerConnection *server;
       NSMutableArray *serendiptyContact;
       NSInteger *totalRecord;
       int inviteCount;
       int currentInviteIndex;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (weak, nonatomic) IBOutlet UIView *viewSearchContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil
bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer;
@end

NS_ASSUME_NONNULL_END
