//
//  PhoneConatactInviteStep2VC.m
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "PhoneConatactInviteStep2VC.h"
#import "SRModalClass.h"
#import "SRNewContactInviteCell.h"
#import "SRModalClass.h"



@interface PhoneConatactInviteStep2VC ()

@end

@implementation PhoneConatactInviteStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getSocialContactApi];
    _txtSearch.delegate = self;
    inviteCount = 0;
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    
    [defaultCenter addObserver:self
                      selector:@selector(getSocialConatctUsersSucceed:)
                          name:kKeyNotificationUserSocialContactGetSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getSocialConatctUsersFailed:)
                          name:kKeyNotificationUserSocialContactGetFailed object:nil];
    
    
    [defaultCenter addObserver:self
                      selector:@selector(inviteUserSucceed:)
                          name:kKeyNotificationInviteUserSucceed object:nil];
    
    [defaultCenter addObserver:self selector:@selector(inviteUserFailed:) name:kKeyNotificationInviteUserFailed object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SRNewContactInviteCell" bundle:nil] forCellReuseIdentifier:@"SRNewContactInviteCell"];
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:5];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableFooterView = nil;
    //    UIButton *rightDoneButton = [SRModalClass setRightNavBarItem:@"Next" barImage:nil forViewNavCon:self offset:10];
    //
    //    [rightDoneButton addTarget:self action:@selector(actionOnDone:) forControlEvents:UIControlEventTouchUpInside];
    //
    
    UIBarButtonItem *skipbtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"Skip"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:@selector(actionOnSKip:)];
    
    [skipbtn setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *next = [[UIBarButtonItem alloc]
                             initWithTitle:@"Next"
                             style:UIBarButtonItemStyleBordered
                             target:self
                             action:@selector(actionOnDone:)];
    [next setTintColor:Rgb2UIColor(255, 149, 0)];
    self.navigationItem.rightBarButtonItems = @[next,skipbtn];
    
    
    
    self.viewSearchContainer.layer.cornerRadius = 5.0;
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    self.txtSearch.attributedPlaceholder = str;
    
    [_btnClose addTarget:self action:@selector(actionOnClose:) forControlEvents:UIControlEventTouchUpInside];
    
    
}
- (void)actionOnSKip:(UIButton *)sender {
    [self showAlertController:@"Serendipity is much more useful when you can share your location with your contacts and they can see your location.\nAre you sure you don\'t want to invite your phone contacts to join you on Serendipity?" alertTitle:@"Alert"];
}
- (void)actionOnClose:(UIButton *)sender {
    if ([_txtSearch.text length] != @0){
        self.txtSearch.text = @"";
        [serendiptyContact removeAllObjects];
        [self getSocialContactApi];
    }
   
}


#pragma mark Action Methods
- (void)actionOnBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)actionOnDone:(UIButton *)sender {
    
    if (inviteCount == 0){
        [self showAlertController:@"Serendipity is much more useful when you can share your location with your contacts and they can see your location.\nAre you sure you don\'t want to invite your phone contacts to join you on Serendipity?" alertTitle:@"Alert"];
    }else{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
               [userDefault setValue:@"yes" forKey:kKeyIsImportPhoneContact];
               [userDefault synchronize];
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            
        [APP_DELEGATE setInitialController];
        }];
    }
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"PhoneConatactInviteStep2VC" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        serendiptyContact = [[NSMutableArray alloc] init];
    }
    return self;
}

//--------------------------------------- numberOfRowsInSection:---------------------------------------//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return serendiptyContact.count;
}


//------------------------------------ cellForRowAtIndexPath: --------------------------------------//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SRNewContactInviteCell *cell = (SRNewContactInviteCell* )[tableView dequeueReusableCellWithIdentifier:@"SRNewContactInviteCell" forIndexPath:indexPath];
    NSDictionary * contactDict = serendiptyContact[indexPath.row];
    if (contactDict[@"full_name"] != nil){
        cell.lblContactName.text = contactDict[@"full_name"];
    }
    if ([contactDict[@"is_invited"]  isEqual: @1]){
        [cell.btnInvite setBackgroundColor:[UIColor.whiteColor colorWithAlphaComponent:0.8]];
        [cell.btnInvite setUserInteractionEnabled:NO];
        [cell.btnInvite setTitle:@"Invited" forState:UIControlStateNormal];
    }else{
        [cell.btnInvite setBackgroundColor:[Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:1]];
        [cell.btnInvite setUserInteractionEnabled:YES];
        [cell.btnInvite setTitle:@"Invite" forState:UIControlStateNormal];
    }
    
    
    cell.btnInvite.tag = indexPath.row;
    [cell.btnInvite addTarget:self action:@selector(actionOnInvitebtn:) forControlEvents:UIControlEventTouchUpInside];
    
    //    if ([contactDict[@"is_registered"]  isEqual: @0]){
    //        [cell.imgSerendipityUser setHidden:true];
    //    }else{
    //        [cell.imgSerendipityUser setHidden:false];
    //    }
    //    if ([contactDict[kKeyIsSelected] isEqual:@0]){
    //        [cell.btnCheckMark setSelected:false];
    //    }else{
    //        [cell.btnCheckMark setSelected:true];
    //    }
    
    return cell;
}
#pragma mark Action Methods
- (void)actionOnInvitebtn:(UIButton *)sender {
    currentInviteIndex = sender.tag;
    
    [APP_DELEGATE showActivityIndicator];
    NSString *mobile_Number = [SRModalClass RemoveSpecialCharacters:serendiptyContact[sender.tag][@"mobile_number"]];
    // Call api
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[kKeyMobileNumber] = mobile_Number;
    params[@"is_invite"] = @1;
    params[kKeyMessage] = @"Hello, I would like you to join Serendipity, an app where you do so many cool things such as:\n• Keep track of your friends and family anywhere in the world in real time!\n• View how you\'re connected to others through our six degrees of separation functionality.\n• Maintain your privacy which allows to keep your location a secret at anytime.\nSay goodbye to missed connections and lost family and friends. Join today at http://www.serendipity.app and make sure your friends and family are safe at all times!";
    
    NSString *urlStr = [NSString stringWithFormat:@"%@", kKeyClassSendSMS];
    [server makeAsychronousRequest:urlStr inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}

-(void)tapOnCheckMark:(UIButton *)sender{
    
}

//------------------------------- heightForRowAtIndexPath -------------------------------------//

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


-(void)getSocialContactApi{
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyoffset] = @0;
    params[kKeyNumRecords] = @999;
    params[kKeyContactType] = kKeyContactValue;
    params[@"is_registered"] = @0;
    params[@"q"] = _txtSearch.text;
    
    [server makeAsychronousRequest:kKeyClassUserContactsGet inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}
#pragma mark
// --------------------------------------------------------------------------------
// getDeletedUsersSucceed:

- (void)getSocialConatctUsersSucceed:(NSNotification *)inNotify {
    NSDictionary *contactSuccess = [inNotify object];
    self->serendiptyContact = contactSuccess[@"list"];
    
    if (serendiptyContact.count == 0){
              
              UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, _tableView.bounds.size.height)];
              noDataLabel.text             = @"Contacts not found";
              noDataLabel.textColor        = [UIColor whiteColor];
              noDataLabel.textAlignment    = NSTextAlignmentCenter;
              _tableView.backgroundView = noDataLabel;
              _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
          }else{
              _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
              _tableView.backgroundView = nil;
          }
    
    [self.tableView reloadData];
}

//// --------------------------------------------------------------------------------
// getDeleteUsersFailed:

- (void)getSocialConatctUsersFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}
- (void)inviteUserSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    if ([inNotify.userInfo[@"response"][@"status"][@"code"]  isEqual: @290] || [inNotify.userInfo[@"response"][@"status"][@"code"] isEqual: @200]){
        inviteCount = inviteCount + 1;
        serendiptyContact[currentInviteIndex][@"is_invited"] = @1;
        [SRModalClass showAlert:@"Your invitation has been sent successfully"];
        [self.tableView reloadData];
    }else{
        [SRModalClass showAlert:@"Invalid mobile number"];
    }
     
}

- (void)inviteUserFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [serendiptyContact removeAllObjects];
    [self getSocialContactApi];
    [_txtSearch resignFirstResponder];
    return true;
}


-(void)showAlertController:(NSString *)message alertTitle :(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes I'm Sure"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
            [self showPopUpContactRefresh];
                                    }];
    UIAlertAction* noBtn = [UIAlertAction
    actionWithTitle:@"No"
    style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {
        
    }];
    [alert addAction:yesButton];
    [alert addAction:noBtn];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showPopUpContactRefresh{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"If you wish to import your phone contacts later, just go to the Main Menu and click on    next to the word Contacts.\n\n"];

    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = [UIImage imageNamed:@"icons-8-available-updates-filled-100"];
    textAttachment.bounds = CGRectMake(0.0,-8,textAttachment.image.size.width,textAttachment.image.size.height);
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    [attributedString replaceCharactersInRange:NSMakeRange(88, 1) withAttributedString:attrStringWithImage];
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0, SCREEN_WIDTH-40, 300)];
//    [label setAttributedText:attributedString];
//    [label setNumberOfLines:0];
//    [label setTextAlignment: NSTextAlignmentCenter];
//    [label sizeToFit];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"If you wish to invite your phone contacts later, just go to the Main Menu and click on Contacts then click the Invite text next to the user you would like to join you on Serendipity." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    alert.tag = 1;
    //[alert setValue:label forKey:@"accessoryView"];
    [alert show];
    
}
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setValue:@"yes" forKey:kKeyIsImportPhoneContact];
        [userDefault synchronize];
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
          [APP_DELEGATE setInitialController];
        }];
    }
}

    



@end
