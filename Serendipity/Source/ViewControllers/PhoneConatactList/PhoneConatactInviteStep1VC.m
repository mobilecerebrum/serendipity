//
//  PhoneConatactInviteStep1VC.m
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "PhoneConatactInviteStep1VC.h"
#import "SRModalClass.h"
#import "SRSerendipityConatactCell.h"
#import "PhoneConatactInviteStep2VC.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <AddressBookUI/AddressBookUI.h>
#import "UIScrollView+InfiniteScroll.h"
@interface PhoneConatactInviteStep1VC ()

@end

@implementation PhoneConatactInviteStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
   // NSLog(@"%@",[userDefault valueForKey:kKeyIsImportPhoneContact]);
    
    totalRecord = 0;
    [self btnActionGetAllContacts];
    server = (APP_DELEGATE).server;
    
    // Register notification
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(getContactStoreSuccessNotifier:)
                          name:kKeyNotificationUserContactStoreSucceed
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(getContactStoreFailureNotifier:)
                          name:kKeyNotificationUserContactStoreFailed object:nil];
    
    [defaultCenter addObserver:self
                         selector:@selector(getSocialConatctUsersSucceed:)
                             name:kKeyNotificationUserSocialContactGetSucceed object:nil];
    [defaultCenter addObserver:self
                         selector:@selector(getSocialConatctUsersFailed:)
                             name:kKeyNotificationUserSocialContactGetFailed object:nil];
    [defaultCenter addObserver:self selector:@selector(getConatctConnectionRequestUserSuccess:) name:kKeyNotificationContactSendConnectionRequestSucceed object:nil];
       
    [defaultCenter addObserver:self selector:@selector(getConatctConnectionRequestUsersFailed:) name:kKeyNotificationContactSendConnectionRequestFailed object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SRSerendipityConatactCell" bundle:nil] forCellReuseIdentifier:@"SRSerendipityConatactCell"];
    self.tableView.tableFooterView = nil;

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:5];
    ;
       [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
       
       
       UIButton *rightDoneButton = [SRModalClass setRightNavBarItem:@"Next" barImage:nil forViewNavCon:self offset:10];
    [rightDoneButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
       [rightDoneButton addTarget:self action:@selector(actionOnDone:) forControlEvents:UIControlEventTouchUpInside];
    
       [_btnSelectAll addTarget:self action:@selector(tapSelectAll:) forControlEvents:UIControlEventTouchUpInside];
    self.btnSendConnectionRequest.layer.cornerRadius = 5;
    [_btnSendConnectionRequest addTarget:self action:@selector(actionOnSendConnectionRequest:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
//    // Add infinite scroll handler
//    __weak typeof(self) weakSelf = self;
//
//    [self.tableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
//        [weakSelf getSocialContactApi];
//    }];
//
//    [self.tableView setShouldShowInfiniteScrollHandler:^BOOL(UIScrollView * _Nonnull scrollView) {
//        return [self->serendiptyContact count] != totalRecord;
//    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil inDict:(NSMutableDictionary *)dict
               server:(SRServerConnection *)inServer {
    // Call Super
    self = [super initWithNibName:@"PhoneConatactInviteStep1VC" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        serendiptyContact = [[NSMutableArray alloc] init];
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self getSocialContactApi];
}


#pragma mark Action Methods
- (void)actionOnBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tapSelectAll:(UIButton *)sender {
    [sender setSelected: !sender.isSelected];
    if (sender.isSelected == true){
        _btnSendConnectionRequest.enabled =  YES;
        _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:1];;
      
    }else{
     _btnSendConnectionRequest.enabled =  NO;
     _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:0.6];
    }
    for (int i = 0; i < [serendiptyContact count]; i++)
       {
           NSMutableDictionary  *contactDict = [serendiptyContact objectAtIndex:i];
           if (sender.isSelected == true){
            contactDict[kKeyIsSelected] = @1;
           }else{
             contactDict[kKeyIsSelected] = @0;
           }
           [serendiptyContact replaceObjectAtIndex:i withObject:contactDict];
       }
       [self.tableView reloadData];
}
- (IBAction)btnSkipPress:(UIButton *)sender {
    [self showAlertController:@"Serendipity is much more useful when you can share your location with your contacts and they can see your location.  Are you sure you don't want to connect with your phone contacts?" alertTitle:@"Alert"];
}

- (void)actionOnDone:(UIButton *)sender {
    
    [self showAlertController:@"Serendipity is much more useful when you can share your location with your contacts and they can see your location.  Are you sure you don't want to connect with your phone contacts?" alertTitle:@"Alert"];
}
- (void)actionOnSendConnectionRequest:(UIButton *)sender {
    
    
    
    NSMutableArray * arrSelected = [[NSMutableArray alloc] init];
    for (int i = 0; i < [serendiptyContact count] ; i++){
        NSMutableDictionary * contactDict = [serendiptyContact objectAtIndex:i];
        if ([contactDict[kKeyIsSelected] isEqual:@1]){
            [arrSelected addObject:contactDict[kKeyId]];
        }
    }
    
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyContactID] = arrSelected;
    
    [server makeAsychronousRequest:kKeyClassContactSendConnectionRequest inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}
-(void)tapOnCheckMark:(UIButton *)sender
{
    NSMutableDictionary * contactDict = [serendiptyContact objectAtIndex:sender.tag];
    if ([contactDict[kKeyIsSelected] isEqual:@0]){
     contactDict[kKeyIsSelected] =  @1;
     [sender setSelected:true];
    }else{
        contactDict[kKeyIsSelected] = @0;
        [sender setSelected:false];
    }
    NSInteger temp = 0;
    for (int i = 0; i < [serendiptyContact count]; i++){
         NSMutableDictionary * checkContactDict = [serendiptyContact objectAtIndex:i];
        if ([checkContactDict[kKeyIsSelected] isEqual:@1]){
            temp ++;
        }
    }
    if ([serendiptyContact count] == temp){
        [_btnSelectAll setSelected:true];
    }else{
        [_btnSelectAll setSelected:false];
    }
    if (temp == 0){
        _btnSendConnectionRequest.enabled =  NO;
        _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:0.6];
        
    }else{
        _btnSendConnectionRequest.enabled =  YES;
        _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:1];
    }
    
}

//--------------------------------------- numberOfRowsInSection:---------------------------------------//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return serendiptyContact.count;
}


//------------------------------------ cellForRowAtIndexPath: --------------------------------------//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SRSerendipityConatactCell *cell = (SRSerendipityConatactCell* )[tableView dequeueReusableCellWithIdentifier:@"SRSerendipityConatactCell" forIndexPath:indexPath];
    NSDictionary * contactDict = serendiptyContact[indexPath.row];
    if (contactDict[@"full_name"] != nil){
        cell.lblUserName.text = contactDict[@"full_name"];
    }
    if ([contactDict[@"is_registered"]  isEqual: @0]){
        [cell.imgSerendipityUser setHidden:true];
    }else{
        [cell.imgSerendipityUser setHidden:false];
    }
    if ([contactDict[kKeyIsSelected] isEqual:@0]){
        [cell.btnCheckMark setSelected:false];
    }else{
        [cell.btnCheckMark setSelected:true];
    }
    
    cell.btnCheckMark.tag = indexPath.row;
    [cell.btnCheckMark addTarget:self action:@selector(tapOnCheckMark:) forControlEvents:UIControlEventTouchUpInside];
    // Return
    return cell;
}
//------------------------------- heightForRowAtIndexPath -------------------------------------//

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(void)showAlertController:(NSString *)message alertTitle :(NSString*)title{
    UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
    
    
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes I'm Sure"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
            [self showPopUpContactRefresh];
                                    }];
    UIAlertAction* noBtn = [UIAlertAction
    actionWithTitle:@"No"
    style:UIAlertActionStyleCancel
    handler:^(UIAlertAction * action) {
        
    }];
    [alert addAction:yesButton];
    [alert addAction:noBtn];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showPopUpContactRefresh{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"If you wish to import your phone contacts later, just go to the Main Menu and click on     next to the word Contacts.\n\n"];

    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = [UIImage imageNamed:@"icons-8-available-updates-filled-100"];
    textAttachment.bounds = CGRectMake(0.0,-8,textAttachment.image.size.width,textAttachment.image.size.height);
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:textAttachment];
    [attributedString replaceCharactersInRange:NSMakeRange(88, 1) withAttributedString:attrStringWithImage];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0, SCREEN_WIDTH-40, 300)];
    [label setAttributedText:attributedString];
    [label setNumberOfLines:0];
    [label setTextAlignment: NSTextAlignmentCenter];
    [label sizeToFit];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    alert.tag = 1;
    [alert setValue:label forKey:@"accessoryView"];
    [alert show];
}

//
// -----------------------------------------------------------------------------------
// clickedButtonAtIndex:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        PhoneConatactInviteStep2VC *objPhoneConatactInviteStep2View = [[PhoneConatactInviteStep2VC alloc] initWithNibName:nil bundle:nil inDict:nil server:server];
//           objPhoneConatactInviteStep2View.delegate = self;
           self.hidesBottomBarWhenPushed = YES;
           [self.navigationController pushViewController:objPhoneConatactInviteStep2View animated:YES];
           self.hidesBottomBarWhenPushed = YES;
    }
}

- (void)btnActionGetAllContacts {
    //ABAddressBookRef m_addressbook = ABAddressBookCreate();

    ABAddressBookRef addressBook = ABAddressBookCreate();
    // ABAddressBookRef addressBook = ABAddressBookCreate();

    __block BOOL accessGranted = NO;

    if (&ABAddressBookRequestAccessWithCompletion != NULL) {
        // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });

        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    } else {
        // We are on iOS 5 or Older
        accessGranted = YES;
//        [self getContactsWithAddressBook:addressBook];
        [self getAllContacts];
    }
    if (accessGranted) {
        [self getAllContacts];
//        [self getContactsWithAddressBook:addressBook];
    }

    // Send contact list to server
    // WHY WE NEED IT?
//    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
//    if (contactList.count > 0) {
//        [dataDict setValue:contactList forKey:kKeyGroupUsers];
//        [dataDict setValue:kKeyContacts forKey:kKeyType];
//        [server makeAsychronousRequest:kKeyClassUploadContacts inParams:dataDict isIndicatorRequired:YES inMethodType:kPOST];
//    }
}

//- (void)getContactsWithAddressBook:(ABAddressBookRef)addressBook {
//    contactList = [[NSMutableArray alloc] init];
//    ContactNumberList = [[NSMutableArray alloc] init];
//    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
//    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
//
//    for (int i = 0; i < nPeople; i++) {
//        NSMutableDictionary *dOfPerson = [NSMutableDictionary dictionary];
//        NSMutableDictionary *listPerson = [NSMutableDictionary dictionary];
//
//        ABRecordRef ref = CFArrayGetValueAtIndex(allPeople, i);
//
//        //For username and surname
//        NSString *firstNameStr = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonFirstNameProperty) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonFirstNameProperty);
//
//        if (firstNameStr.length > 0) {
//            dOfPerson[kKeyFirstName] = firstNameStr;
//        } else {
//            dOfPerson[kKeyFirstName] = @"";
//        }
//
//        NSString *lastNameStr = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonLastNameProperty) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonLastNameProperty);
//
//        if (lastNameStr.length > 0) {
//            dOfPerson[kKeyLastName] = lastNameStr;
//        } else
//            dOfPerson[kKeyLastName] = @"";
//
//        NSString *countryCode = (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonAddressCountryCodeKey) == nil ? @"" : (__bridge_transfer NSString *) ABRecordCopyValue(ref, kABPersonAddressCountryCodeKey);
//
//        if (countryCode.length > 0) {
//            dOfPerson[kKeyCountryMasterId] = countryCode;
//        } else
//            dOfPerson[kKeyCountryMasterId] = @"";
//
//        NSString *fullNameStr = [[[NSString stringWithFormat:@"%@", firstNameStr] stringByAppendingString:@" "] stringByAppendingString:[NSString stringWithFormat:@"%@", lastNameStr]];
//
//
//        //For Email ids
//        ABMutableMultiValueRef eMail = ABRecordCopyValue(ref, kABPersonEmailProperty);
//        if (ABMultiValueGetCount(eMail) > 0) {
//            dOfPerson[kKeyEmail] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(eMail, 0);
//
//            [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(eMail, 0) forKey:kKeyEmail];
//        }
//        CFRelease(eMail);
//
//        //For Phone number List
//        ABMultiValueRef phones = (__bridge ABMultiValueRef) ((__bridge NSString *) ABRecordCopyValue(ref, kABPersonPhoneProperty));
//
//        NSString *mobileLabel;
//        NSLog(@"Phone of %@ is %@", firstNameStr, phones);
//        for (CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
//            mobileLabel = (__bridge_transfer NSString *) ABMultiValueCopyLabelAtIndex(phones, i);
//            if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneMobileLabel]) {
//                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);
//
//                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
//            } else if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneIPhoneLabel]) {
//                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);
//
//                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
//            } else if ([mobileLabel isEqualToString:(NSString *) kABPersonPhoneMainLabel]) {
//                dOfPerson[kKeyMobileNumber] = (__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i);
//
//                [listPerson setValue:(__bridge_transfer NSString *) ABMultiValueCopyValueAtIndex(phones, i) forKey:kKeyMobileNumber];
//            }
//
//        }
//        CFRelease(phones);
//        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil) {
//            NSString *mobileNo = [SRModalClass RemoveSpecialCharacters:[dOfPerson valueForKey:kKeyMobileNumber]];
//            [dOfPerson setValue:countryCode forKey:kKeyCountryMasterId];
//            [dOfPerson setValue:[NSString stringWithFormat:@"%@",mobileNo] forKey:kKeyMobileNumber];
//        }
//
//        if ([dOfPerson valueForKey:kKeyEmail] == nil) {
//            [dOfPerson setValue:@"" forKey:kKeyEmail];
//        }
//
////        // Add contact number in array only when if mobile number is not empty
////        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil) {
////            [contactList addObject:dOfPerson];
////        }
//
//
//        if ([dOfPerson valueForKey:kKeyMobileNumber] != nil && ![[dOfPerson valueForKey:kKeyMobileNumber]  isEqual: @""]){
//            // Create array of user dictionaries
//            [listPerson setValue:firstNameStr forKey:kKeyFirstName];
//            [listPerson setValue:lastNameStr forKey:kKeyLastName];
//            [listPerson setValue:fullNameStr forKey:kKeyName];
//            [listPerson setValue:[dOfPerson valueForKey:kKeyMobileNumber] forKey:kKeyMobileNumber];
//            if ([[dOfPerson valueForKey:kKeyMobileNumber] stringValue].length >= 7) {
//                [ContactNumberList addObject:listPerson];
//            }
////            [listPerson setValue:[dOfPerson valueForKey:kKeyCountryMasterId] forKey:kKeyCountryMasterId];
//           // [listPerson setValue:[dOfPerson valueForKey:kKeyEmail] forKey:kKeyEmail];
//
//        }
//
//    }
//    server.contactList = ContactNumberList;
//    [self SendMobileNumberForSync];
//    CFRelease(allPeople);
//}

- (void) getAllContacts {
    CNContactStore *store = [[CNContactStore alloc] init];
    contactList = [[NSMutableArray alloc] init];
    ContactNumberList = [[NSMutableArray alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactMiddleNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey];
            CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
            NSError *error;
            [store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop) {
                if (error) {
                    NSLog(@"error fetching contacts %@", error);
                } else {
                    // copy data to my custom Contact class.
                    NSMutableDictionary *dOfPerson = [NSMutableDictionary dictionary];
                    if (contact.givenName.length > 0) {
                        dOfPerson[kKeyFirstName] = contact.givenName;
                    } else {
                        dOfPerson[kKeyFirstName] = @"";
                    }
                    if (contact.familyName.length > 0) {
                        dOfPerson[kKeyLastName] = contact.familyName;
                    } else {
                        dOfPerson[kKeyLastName] = @"";
                    }
                    if (contact.emailAddresses.count > 0) {
                        for (CNLabeledValue<NSString *> *phone in contact.emailAddresses) {
                            if (phone.value.length > 0) {
                                dOfPerson[kKeyEmail] = phone.value;
                                break;
                            }
                        }
                    } else {
                        dOfPerson[kKeyEmail] = @"";
                    }
                    if (contact.phoneNumbers.count > 0) {
                        for (CNLabeledValue<CNPhoneNumber *> *phone in contact.phoneNumbers) {
                            if (phone.value.stringValue.length > 0) {
                                dOfPerson[kKeyMobileNumber] = phone.value.stringValue;
                                break;
                            }
                        }
                    } else {
                        dOfPerson[kKeyMobileNumber] = @"-1";
                    }
                    //jonish sprint 2
                    if ([dOfPerson valueForKey:kKeyMobileNumber] != NULL) {
                        [ContactNumberList addObject:dOfPerson];
                    }
                    //jonish sprint 2
                }
            }];
            //call API here
            server.contactList = ContactNumberList;
            
            [self SendMobileNumberForSync];
        }
    }];
}


-(void)SendMobileNumberForSync{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    if (server.contactList != nil) {
        if (server.contactList.count > 0) {
            [(APP_DELEGATE) showActivityIndicator];
            [dataDict setValue:server.contactList forKey:kKeyContacts];
            [dataDict setValue:kKeyContactValue forKey:kKeyContacTyype];
            [server makeSynchronousRequest:kKeyClassUserContactsStore inParams:dataDict inMethodType:kPOST isIndicatorRequired:YES];
        } else {
            [(APP_DELEGATE) hideActivityIndicator];
        }
    }
}

-(void)getContactStoreSuccessNotifier:(NSNotification *)notification {
    //Neha
//     NSMutableArray* arrContacts = notification.userInfo[@"requestedParams"][@"contacts"];
//     serendiptyContact = arrContacts;
//    [self.tableView reloadData];
    //Contacts should be imported from here.
    
    [(APP_DELEGATE) hideActivityIndicator];
    [self getSocialContactApi];
    
}

-(void)getContactStoreFailureNotifier:(NSNotification *)notification {
    [APP_DELEGATE hideActivityIndicator];
    if (serendiptyContact.count == 0){
        _btnSendConnectionRequest.enabled =  NO;
        _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:0.6];
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, _tableView.bounds.size.height)];
        noDataLabel.text             = @"Contacts not found";
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        _tableView.backgroundView = noDataLabel;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_viewSelectAll setHidden:YES];
        //self.tableView no
    }else{
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundView = nil;
        [_viewSelectAll setHidden:NO];
    }
    [self.tableView reloadData];
}

-(void)getSocialContactApi{
    [APP_DELEGATE showActivityIndicator];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyoffset] = [NSNumber numberWithInteger:serendiptyContact.count];;
    params[kKeyNumRecords] = @999;//[NSNumber numberWithInteger:serendiptyContact.count+1];
    params[kKeyContactType] = kKeyContactValue;
    params[@"is_registered"] = @1;
    
    [server makeAsychronousRequest:kKeyClassUserContactsGet inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}
#pragma mark
// --------------------------------------------------------------------------------
// getDeletedUsersSucceed:

- (void)getSocialConatctUsersSucceed:(NSNotification *)inNotify {
    [(APP_DELEGATE) hideActivityIndicator];
    NSString* strType = [NSString stringWithFormat:@"%@",[[inNotify.userInfo valueForKey:@"requestedParams"] valueForKey:@"is_registered"]];
    
    
    if ([strType  isEqual: @"1"]){
        NSDictionary *contactSuccess = [inNotify object];
        self->serendiptyContact = contactSuccess[@"list"];
//         if (serendiptyContact.count == 0){
//           self->serendiptyContact = contactSuccess[@"list"];
//         }else{
//             NSArray * tempArray = contactSuccess[@"list"];
//             [serendiptyContact addObjectsFromArray: tempArray];
//         }
         
        // self->totalRecord = contactSuccess[@"total"];
         for (int i = 0; i < [serendiptyContact count]; i++)
         {
            // NSLog(@"%d",i);
             NSMutableDictionary  *contactDict = [serendiptyContact objectAtIndex:i];
             contactDict[kKeyIsSelected] = @1;
             [_btnSelectAll setSelected:true];
             [serendiptyContact replaceObjectAtIndex:i withObject:contactDict];
             _btnSendConnectionRequest.enabled =  YES;
             _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:1];
         }
        
        if (serendiptyContact.count){
            
        }
        if (serendiptyContact.count == 0){
            _btnSendConnectionRequest.enabled =  NO;
            _btnSendConnectionRequest.backgroundColor = [Rgb2UIColor(255, 149, 0) colorWithAlphaComponent:0.6];
            UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width, _tableView.bounds.size.height)];
            noDataLabel.text             = @"Contacts not found";
            noDataLabel.textColor        = [UIColor whiteColor];
            noDataLabel.textAlignment    = NSTextAlignmentCenter;
            _tableView.backgroundView = noDataLabel;
            _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [_viewSelectAll setHidden:YES];
            //self.tableView no
        }else{
            _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            _tableView.backgroundView = nil;
            [_viewSelectAll setHidden:NO];
        }
        
         // Reload table
         [self.tableView reloadData];
    }
}

//// --------------------------------------------------------------------------------
// getDeleteUsersFailed:

- (void)getSocialConatctUsersFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}
- (void)getConatctConnectionRequestUserSuccess:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    [SRModalClass showAlertWithTitle:@"Your connection requests have been sent successfully" alertTitle:@"Success"];
    
    PhoneConatactInviteStep2VC *objPhoneConatactInviteStep2View = [[PhoneConatactInviteStep2VC alloc] initWithNibName:nil bundle:nil inDict:nil server:server];
       self.hidesBottomBarWhenPushed = YES;
       [self.navigationController pushViewController:objPhoneConatactInviteStep2View animated:YES];
       self.hidesBottomBarWhenPushed = YES;
   // NSString* message = [inNotify object];
    //[self showAlertController:message alertTitle:@"Serrendipity"];
}
- (void)getConatctConnectionRequestUsersFailed:(NSNotification *)inNotify {

}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
