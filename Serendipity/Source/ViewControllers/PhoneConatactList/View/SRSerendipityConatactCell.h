//
//  SRSerendipityConatactCell.h
//  Serendipity
//
//  Created by Hitesh Surani on 15/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRSerendipityConatactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMark;
@property (weak, nonatomic) IBOutlet UIImageView *imgSerendipityUser;
@end

NS_ASSUME_NONNULL_END
