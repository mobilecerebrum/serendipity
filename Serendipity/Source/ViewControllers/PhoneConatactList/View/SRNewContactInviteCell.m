//
//  SRNewContactInviteCell.m
//  Serendipity
//
//  Created by Hitesh Surani on 16/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "SRNewContactInviteCell.h"

@implementation SRNewContactInviteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btnInvite.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
