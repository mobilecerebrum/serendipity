//
//  SRNewContactInviteCell.h
//  Serendipity
//
//  Created by Hitesh Surani on 16/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRNewContactInviteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContactName;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;

@end

NS_ASSUME_NONNULL_END
