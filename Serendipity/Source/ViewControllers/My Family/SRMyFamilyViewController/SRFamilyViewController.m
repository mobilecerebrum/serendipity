#import "SRModalClass.h"
#import "SRFamilyViewCell.h"
#import "SRCreateEditFamilyViewController.h"
#import "SRConnectionNotificationCell.h"
#import "SRFamilyViewController.h"
#import "SRMapViewController.h"
#import "SRDistanceClass.h"
#import "SRNotifyMeViewController.h"
#import "NotifyMeVC.h"

#define DISTANCEFILTER 50


@interface familyDistanceClass : NSObject

@property (nonatomic,strong) NSString *lat;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *dataId;
@property (strong, nonatomic) CLLocation *myLocation;
@property (nonatomic,strong) NSDictionary *userDict;

+(familyDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation;

@end

@implementation familyDistanceClass

+(familyDistanceClass *)getObjectFromDict:(NSDictionary *)userDict myLocation:(CLLocation *)myLocation{
    
    familyDistanceClass *obj = [[familyDistanceClass alloc] init];
    obj.dataId = userDict[kKeyId];
    NSDictionary *locationDict = userDict[kKeyLocation];
    obj.lat = locationDict[kKeyLattitude];
    obj.longitude = locationDict[kKeyRadarLong];
    obj.distance = userDict[kKeyDistance];
    obj.myLocation = myLocation;
    obj.userDict = userDict;
    
    return obj;
}

@end


@interface SRFamilyViewController ()
//@property (nonatomic,strong) NSOperationQueue *familyDistanceQueue;
@end

@implementation SRFamilyViewController

#pragma mark private methods
-(void)callDistanceAPI:(NSDictionary *)userDict{
    
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[[[userDict valueForKey:kKeyRelative] objectForKey:kKeyLocation] valueForKey:kKeyLattitude]floatValue] longitude:[[[[userDict valueForKey:kKeyRelative] objectForKey:kKeyLocation] valueForKey:kKeyRadarLong]floatValue]];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@",fromLoc.coordinate.latitude,fromLoc.coordinate.longitude,toLoc.coordinate.latitude,toLoc.coordinate.longitude,kKeyGoogleMap];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    request.timeoutInterval = 60;
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               
                               if (!error) {
                                   
                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                   
                                   if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"] ) {
                                       //parse data
                                       NSArray *rows = json[@"rows"];
                                       NSDictionary *rowsDict = nil;
                                       if (rows && rows.count) {
                                           
                                           rowsDict = rows[0];

                                       }
                                       
                                       NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                                       NSDictionary *dict = dataArray[0];
                                       NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                                       NSString *distance = [distanceDict valueForKey:@"text"];
                                       
                                       NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                                       double convertDist = [distanceWithoutCommas doubleValue];
                                       if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                                           if ([distance containsString:@"km"]) {
                                               
                                           }
                                           else
                                           {
                                               //meters to killometer
                                               convertDist = convertDist / 1000;
                                           }
                                       }
                                       else
                                       {
                                           if ([distance containsString:@"km"]) {
                                               //killometer to miles
                                               convertDist = convertDist * 0.621371192;
                                           }
                                           else
                                           {
                                               //meters to miles
                                               convertDist = (convertDist * 0.000621371192);
                                           }
                                       }
                                       
                                      // NSLog(@"Distance: %@",[NSString stringWithFormat:@"%.1f %@", convertDist, distanceUnit]);
                                       
                                       SRDistanceClass *obj = [[SRDistanceClass alloc] init];
                                       obj.dataId = userDict[kKeyId];
                                       NSDictionary *locationDict = [userDict[kKeyRelative] objectForKey:kKeyLocation];
                                       obj.lat = locationDict[kKeyLattitude];
                                       obj.longitude = locationDict[kKeyRadarLong];
                                       obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit];
                                       obj.myLocation = server.myLocation;
                                       //Live Address
                                       NSArray *destinationArray = json[@"destination_addresses"];
                                       
                                      // NSLog(@"Address : %@", [NSString stringWithFormat:@"in %@", destinationArray[0]]);
                                       
                                       NSUInteger indexOfUserObject = [familyListArr indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                           
                                           
                                           if([[obj objectForKey:kKeyId] isEqualToString:userDict[kKeyId]])
                                           {
                                               return YES;
                                           }
                                           else{
                                               
                                               return NO;
                                           }
                                           
                                       }];
                                       
                                      // NSLog(@"%lu",(unsigned long)indexOfUserObject);
                                       
                                       
                                     //  NSLog(@"IndexPath : %lu",(unsigned long)indexOfUserObject);
                                       
                                       
                                       NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                                       NSArray *tempArr = [(APP_DELEGATE).oldFamilyDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                                       obj.address = [NSString stringWithFormat:@"in %@", destinationArray[0]];
                                       if (convertDist <= 0)
                                       {
                                           obj.address = @"";
                                       }
                                       
                                       if (tempArr && tempArr.count) {
                                           
                                           NSUInteger indexOfObject = [(APP_DELEGATE).oldFamilyDataArray indexOfObjectPassingTest:^BOOL(SRDistanceClass* anotherObj, NSUInteger idx, BOOL * _Nonnull stop) {
                                               
                                               
                                               if([anotherObj.dataId isEqualToString:userDict[kKeyId]])
                                               {
                                                   return YES;
                                               }
                                               else{
                                                   
                                                   return NO;
                                               }
                                               
                                           }];
                                           
                                           
                                           (APP_DELEGATE).oldFamilyDataArray[indexOfObject] = obj;
                                           NSLog(@"oldFamilyDataArray:%@",(APP_DELEGATE).oldFamilyDataArray);
                                       }
                                       else{
                                           
                                           [(APP_DELEGATE).oldFamilyDataArray addObject:obj];
                                           //NSLog(@"oldFamilyDataArray:%@",(APP_DELEGATE).oldFamilyDataArray);
                                           
                                       }
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           
                                           if(indexOfUserObject!=NSNotFound && indexOfUserObject<familyListArr.count)
                                           {
                                               [_objtableView reloadSections:[NSIndexSet indexSetWithIndex:indexOfUserObject] withRowAnimation:UITableViewRowAnimationNone];
                                           }
                                       });
                                   }
                               }
                           }];
    
}


-(void)getDistance{
    
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    if(!(APP_DELEGATE).oldFamilyDataArray.count)
    {
        
        [arr addObjectsFromArray:familyListArr];
        
    }
    else{
        
        for (NSDictionary *userDict in familyListArr) {
            
            NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
            
            NSArray *tempArr = [(APP_DELEGATE).oldFamilyDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
            
            if (tempArr && tempArr.count) {
                
                SRDistanceClass *distanceObj = [tempArr firstObject];
                
                NSDictionary *locationDict = [userDict[kKeyRelative] objectForKey:kKeyLocation];
                
                
                BOOL isBLocationChanged = NO;
                BOOL isALocationChanged = NO;
                
                if (![distanceObj.lat isEqualToString:locationDict[kKeyLattitude]] || ![distanceObj.longitude isEqualToString:locationDict[kKeyRadarLong]]) {
                    
                    isBLocationChanged = YES;
                }
                
                CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];
                
                if (distance >= 160) {
                    
                    isALocationChanged = YES;
                    
                }
                
                if(isBLocationChanged || isALocationChanged)
                {
                    [arr addObject:userDict];
                }

            }
            else{
                
                [arr addObject:userDict];
                
            }
        }
    }
    
    
    for(int i=0;i<arr.count;i++)
    {
        NSDictionary *userDict = arr[i];
        userDict = [SRModalClass removeNullValuesFromDict:userDict];
        
        if (server.myLocation != nil && [[userDict[kKeyRelative] objectForKey:kKeyLocation] isKindOfClass:[NSDictionary class]])
        {
            NSBlockOperation * op = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation * weakOp = op; // Use a weak reference to avoid a retain cycle
            [op addExecutionBlock:^{
                // Put this code between whenever you want to allow an operation to cancel
                // For example: Inside a loop, before a large calculation, before saving/updating data or UI, etc.
                if (!weakOp.isCancelled){
                    
                    [self callDistanceAPI:userDict];
                }
                else{
                    
                  //  NSLog(@"Operation Not canceled");
                }
            }];
            
        }
        
    }
    
}


#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
	//Call Super
	self = [super initWithNibName:@"SRFamilyViewController" bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		server = inServer;
		familyListArr = [[NSMutableArray alloc]init];
		cacheImgArr = [[NSMutableArray alloc]init];
		expandedList = [[NSMutableArray alloc]init];
		storedLocation = [[NSMutableArray alloc]init];
		updatedNotiList = [[NSMutableArray alloc]init];
        
        _matrixRequestFamilyArray = [[NSMutableArray alloc] init];
        _matrixResponseFamilyArray = [[NSMutableArray alloc] init];

		// Register Notifications
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self
		                  selector:@selector(familyActionSucceed:)
		                      name:kKeyNotificationFamilyActionSucceed object:nil];
		[defaultCenter addObserver:self
		                  selector:@selector(familyActionFailed:)
		                      name:kKeyNotificationFamilyActionFailed object:nil];
		[defaultCenter addObserver:self
		                  selector:@selector(getNotificationSucceed:)
		                      name:kKeyNotificationGetNotificationSucceed object:nil];
		[defaultCenter addObserver:self
		                  selector:@selector(getNotificationFailed:)
		                      name:kKeyNotificationGetNotificationFailed object:nil];

		[defaultCenter addObserver:self
		                  selector:@selector(updateNotificationSucceed:)
		                      name:kKeyNotificationUpdateNotificationSucceed object:nil];
	}

	//return
	return self;
}

//---------------------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
	//Call Super
	[super viewDidLoad];
    // Display no data
    //Empty Data Label
    self.lblNoData =[[UILabel alloc]init];
    self.lblNoData.textAlignment=NSTextAlignmentCenter;
    self.lblNoData.textColor=[UIColor orangeColor];
    self.lblNoData.text=NSLocalizedString(@"lbl.nodata.txt", @"");
    [self.objtableView reloadData];
    
//    _familyDistanceQueue = [[NSOperationQueue alloc] init];
    //Creating and Adding a blurring overlay view
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.imgScreenBackGroundImage.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark
                                    ];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.tintColor=[UIColor clearColor];
        blurEffectView.frame = self.imgScreenBackGroundImage.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.imgScreenBackGroundImage addSubview:blurEffectView];
    }


	// Set navigation bar
	self.navigationController.navigationBar.hidden = NO;
	self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
	self.navigationController.navigationBar.translucent = NO;

	// Title
	[SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.MyFamily.view", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

	UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
	[leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
	rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"Edit-50.png"] forViewNavCon:self offset:-23];
	[rightButton addTarget:self action:@selector(pencilBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

// ----------------------------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    (APP_DELEGATE).topBarView.hidden = YES;
    //Get and Set distance measurement unit
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    }
    else
        distanceUnit = kkeyUnitKilometers;
    
   
        // Call api
        [APP_DELEGATE showActivityIndicator];
        [server makeAsychronousRequest:kKeyClassFamily inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    
        notificationsList = server.notificationArr;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"2"];
        notificationsList = [notificationsList filteredArrayUsingPredicate:predicate];
    
        if ([notificationsList count] == 0) {
            self.btnNotifications.hidden = YES;
            self.imgNatificationsBG.hidden = YES;
            self.imgDropDown.hidden = YES;
            self.notificationsTable.hidden = YES;
            
            
            self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.imgBottomBarBGImage.frame.origin.y);
        }
        else {
            self.notificationsTable.frame = CGRectMake(0, 40, self.view.frame.size.width, 0);
            self.notificationsTable.hidden = YES;
            
            // Set Title to Notification Button
            NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long)[notificationsList count]]];
            [self.btnNotifications setTitle:str forState:UIControlStateNormal];
        }
}

#pragma mark
#pragma mark Action Methods
#pragma mark
// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
-(void)compassTappedCaptured:(UIButton *)sender
{
    NSMutableDictionary *userDict = [familyListArr[sender.tag] valueForKey:kKeyRelative];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:@"FromFamilyView" bundle:nil inDict:userDict server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}
// ---------------------------------------------------------------------------------------------------------------------------------
// acceptFriendRequest:

- (void)acceptFriendRequest:(UIButton *)sender {
	NSInteger i = [sender tag];
	NSDictionary *notiDict = notificationsList[i];
	// Call notification API
	NSDictionary *familyDict = notiDict[kKeyReference];
	NSMutableDictionary *paramsDict = [NSMutableDictionary dictionary];
	paramsDict[kKeyRelativeId] = familyDict[kKeyRelativeId];
	paramsDict[kKeyRelation] = familyDict[kKeyRelation];
	paramsDict[kKeyAllowPushNotificationTrack] = familyDict[kKeyAllowPushNotificationTrack];

	NSMutableDictionary *mutatedLocationsDict = [NSMutableDictionary dictionary];
	NSArray *locationArr = familyDict[kKeyFamilyLocationArr];
	for (NSDictionary *locationDict in locationArr) {
		NSString *latitude = locationDict[kKeyLattitude];
		NSString *longitude = locationDict[kKeyRadarLong];

		NSDictionary *locationParamDict = @{kKeyAddress: locationDict[kKeyLocation], kKeyLattitude: latitude, kKeyRadarLong: longitude};
		mutatedLocationsDict[locationDict[kKeyType]] = locationParamDict;
		if ([locationDict[kKeyMediaIsDefault] isEqualToString:@"1"]) {
			paramsDict[kKeyDefault] = locationDict[kKeyType];
		}
	}
	paramsDict[kKeyFamilyLocation] = mutatedLocationsDict;
	paramsDict[@"_method"] = kPUT;
    paramsDict[kKeyInvitationStatus] = @"2";
    if ([notiDict[kKeyReferenceStatus] isEqualToString:@"1"]) {
		paramsDict[kKeyAllowTrackStatus] = @"4";
	}
    
	NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassFamily, familyDict[kKeyId]];
	[server makeAsychronousRequest:urlStr inParams:paramsDict isIndicatorRequired:YES inMethodType:kPUT];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// rejectFriendRequest:

- (void)rejectFriendRequest:(UIButton *)sender {
	NSInteger i = [sender tag];
	NSDictionary *notiDict = notificationsList[i];

	// Call notification API
	NSDictionary *familyDict = notiDict[kKeyReference];
	NSMutableDictionary *paramsDict = [NSMutableDictionary dictionary];
	paramsDict[kKeyRelativeId] = familyDict[kKeyRelativeId];
	paramsDict[kKeyRelation] = familyDict[kKeyRelation];
	paramsDict[kKeyAllowPushNotificationTrack] = familyDict[kKeyAllowPushNotificationTrack];

	NSArray *locationArr = familyDict[kKeyFamilyLocationArr];
	NSMutableDictionary *mutatedLocationsDict = [NSMutableDictionary dictionary];
	for (NSDictionary *locationDict in locationArr) {
		NSString *latitude = locationDict[kKeyLattitude];
		NSString *longitude = locationDict[kKeyRadarLong];

		NSDictionary *locationParamDict = @{kKeyAddress: locationDict[kKeyLocation], kKeyLattitude: latitude, kKeyRadarLong: longitude};
		mutatedLocationsDict[locationDict[kKeyType]] = locationParamDict;

		if ([locationDict[kKeyMediaIsDefault] isEqualToString:@"1"]) {
			paramsDict[kKeyDefault] = locationDict[kKeyType];
		}
	}
	paramsDict[kKeyFamilyLocation] = mutatedLocationsDict;
	paramsDict[@"_method"] = kPUT;
	if (sender.superview.tag == 100) {
		paramsDict[kKeyInvitationStatus] = @"3";
	}
	else {
		paramsDict[kKeyAllowTrackStatus] = @"5";
		paramsDict[kKeyAllowPushNotificationTrack] = @"0";
	}
	NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassFamily, familyDict[kKeyId]];
	[server makeAsychronousRequest:urlStr inParams:paramsDict isIndicatorRequired:YES inMethodType:kPUT];
}

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
     [APP_DELEGATE hideActivityIndicator];
	[self.navigationController popToViewController:self.delegate animated:NO];
}

// ----------------------------------------------------------------------------------------------------------
// editBtnAction:

- (void)pencilBtnAction:(id)sender {
     [APP_DELEGATE hideActivityIndicator];
    SRCreateEditFamilyViewController *createEditCon = [[SRCreateEditFamilyViewController alloc]initWithNibName:nil bundle:nil server:server familyMembers:familyListArr isAddFamilyMembers:NO];
	createEditCon.delegate = self;
	self.hidesBottomBarWhenPushed = YES;
	[self.navigationController pushViewController:createEditCon animated:YES];
	self.hidesBottomBarWhenPushed = YES;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnSwitch:

- (void)actionOnSwitch:(UISwitch *)sender {
    
	NSDictionary *familyDict = familyListArr[sender.tag];
    NSArray *locationArr = familyDict[kKeyFamilyLocationArr];
    NSDictionary *userDict;
    userDict= familyDict[kKeyRelative];
    NSString *strValue = @"0";


    if ([familyDict[kKeyAllowPushNotificationTrack] isEqualToString:@"1"])
    {
        if (familyDict[kKeyRelative] != nil && [familyDict[kKeyRelative] isKindOfClass:[NSDictionary class]])
        {
            if ([sender isOn])
            {
                strValue = @"1";
                NSNumber *number = @(sender.tag);
                [expandedList addObject:number];
                
                if (locationArr.count > 0)
                {
                    // Update the location track
                    NSMutableDictionary *paramsDict = [NSMutableDictionary dictionary];
                    paramsDict[kKeyRelativeId] = userDict[kKeyId];
                    paramsDict[kKeyRelation] = familyDict[kKeyRelation];
                    paramsDict[kKeyAllowPushNotificationTrack] = strValue;
                    paramsDict[kKeyInvitationStatus] = familyDict[kKeyInvitationStatus];
                    
                    
                    BOOL isAdded = NO;
                    NSMutableDictionary *mutatedLocationsDict = [NSMutableDictionary dictionary];
                    for (NSDictionary *locationDict in locationArr) {
                        NSString *latitude = locationDict[kKeyLattitude];
                        NSString *longitude = locationDict[kKeyRadarLong];
                        
                        NSDictionary *locationParamDict = @{kKeyAddress: locationDict[kKeyLocation], kKeyLattitude: latitude, kKeyRadarLong: longitude};
                        mutatedLocationsDict[locationDict[kKeyType]] = locationParamDict;
                        if ([locationDict[kKeyMediaIsDefault] isEqualToString:@"1"] && !isAdded)
                        {
                            isAdded = YES;
                            paramsDict[kKeyDefault] = locationDict[kKeyType];
                        }
                    }
                    paramsDict[kKeyFamilyLocation] = mutatedLocationsDict;
                    
                    NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassFamily, familyDict[kKeyId]];
                    [server makeAsychronousRequest:urlStr inParams:paramsDict isIndicatorRequired:YES inMethodType:kPUT];
                    
                    // Reload table view
                    [self.objtableView reloadData];
//                    [_familyDistanceQueue cancelAllOperations];
//                    [self getDistance];
                }
                else
                {
                    [SRModalClass showAlert:@"Go to edit family and add location to track."];
                }
            }
            else{
                NSNumber *number = @(sender.tag);
                [expandedList removeObject:number];
            }
        }
        else
        {
            NSNumber *number = @(sender.tag);
            [expandedList removeObject:number];
        }
    }
    else
    {
        [SRModalClass showAlert:@"Sorry, but this user has disabled the ability for anyone to track them."];
        NSNumber *number = @(sender.tag);
        [expandedList removeObject:number];
        [sender setOn:NO];
    }
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnRadioButton:

- (void)actionOnRadioButton:(UIButton *)inSender {
	[inSender setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];

	// Get the super view and tag to get family dict
	UIView *superView = inSender.superview;
	NSDictionary *familyDict = familyListArr[superView.tag];
	    NSDictionary *familyUserDict = [familyDict valueForKey:kKeyRelative];
    
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"TrackingRequest" bundle:nil];
                         NotifyMeVC *controller = [s instantiateViewControllerWithIdentifier:@"NotifyMeVC"];
                          controller.server = server;
                          controller.profileDict = familyUserDict;
                     
                         [self.navigationController pushViewController:controller animated:true];
    
//    SRNotifyMeViewController *notifyMe = [[SRNotifyMeViewController alloc]initWithNibName:@"SRNotifyMeViewController" bundle:nil profileData:familyUserDict server:server];
//    [self.navigationController pushViewController:notifyMe animated:YES];
}

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnButtons:

- (IBAction)actionOnButtons:(id)sender {
	if (sender == self.btnAddFamilyMember) {
        [APP_DELEGATE hideActivityIndicator];
		SRCreateEditFamilyViewController *createEditCon = [[SRCreateEditFamilyViewController alloc]initWithNibName:nil bundle:nil server:server familyMembers:nil isAddFamilyMembers:YES];
		createEditCon.delegate = self;
		self.hidesBottomBarWhenPushed = YES;
		[self.navigationController pushViewController:createEditCon animated:YES];
		self.hidesBottomBarWhenPushed = YES;
	}
	else if (sender == self.btnNotifications) {
		if (!isClicked) {
			//[self.view addSubview:self.notificationsTable];
			self.notificationsTable.hidden = NO;
			[UIView animateWithDuration:0.5
			                 animations: ^{
			    CGFloat height = 50 * notificationsList.count + 2;
			    if (height > 200) {
			        height = 200;
				}
			    self.notificationsTable.frame = CGRectMake(0, 40, self.view.bounds.size.width, height);
			    self.objtableView.frame = CGRectMake(0, self.notificationsTable.frame.size.height + self.notificationsTable.frame.origin.y - 4, self.objtableView.frame.size.width, self.objtableView.frame.size.height);
			}];
			isClicked = YES;
			self.imgDropDown.image = [UIImage imageNamed:@"down.png"];

			// Reload data
			[self.notificationsTable reloadData];

			// Update notifications status
			for (NSUInteger i = 0; i < [notificationsList count]; i++) {
				NSDictionary *notifyDict = notificationsList[i];
				if ([notifyDict[kKeyReferenceStatus] isEqualToString:@"3"] || [notifyDict[kKeyReferenceStatus] isEqualToString:@"4"] || [notifyDict[kKeyReferenceStatus] isEqualToString:@"5"]) {
					NSDictionary *param = @{kKeyStatus: @1};
					NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
					[server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
				}
			}
            
		} else {
			[UIView animateWithDuration:0.5
			                 animations: ^{
			    self.notificationsTable.frame = CGRectMake(0, 40, self.view.bounds.size.width, 0);
			    self.objtableView.frame = CGRectMake(0, self.btnNotifications.frame.origin.y + self.btnNotifications.frame.size.height, self.objtableView.frame.size.width, self.objtableView.frame.size.height);
			} completion: ^(BOOL finished) {
			    //[self.notificationsTable removeFromSuperview];
			    self.notificationsTable.hidden = YES;
			}];

			isClicked = NO;
			self.imgDropDown.image = [UIImage imageNamed:@"caret-down.png"];

			NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
			for (NSString *notiId in updatedNotiList) {
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", kKeyId, notiId];
				NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
                
                if(filterArr && filterArr.count)
                {
                    [array removeObject:filterArr[0]];

                }
			}

			// Remove all objects
			[updatedNotiList removeAllObjects];
			notificationsList = array;
            
			if ([notificationsList count] == 0) {
				self.btnNotifications.hidden = YES;
				self.imgNatificationsBG.hidden = YES;
				self.imgDropDown.hidden = YES;
				self.notificationsTable.hidden = YES;
				self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.imgBottomBarBGImage.frame.origin.y);
			}
            
            
            // Update notifications status
            for (NSUInteger i = 0; i < [notificationsList count]; i++) {
                NSDictionary *notifyDict = notificationsList[i];
                if ([notifyDict[kKeyReferenceStatus] isEqualToString:@"0"] || [notifyDict[kKeyReferenceStatus] isEqualToString:@"2"]) {
                    NSDictionary *param = @{kKeyStatus: @1};
                    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                    [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
                }
            }
		}
	}
}

- (IBAction)addFamilyMemberTapGesture:(id)sender {
    [APP_DELEGATE hideActivityIndicator];
    SRCreateEditFamilyViewController *createEditCon = [[SRCreateEditFamilyViewController alloc]initWithNibName:nil bundle:nil server:server familyMembers:nil isAddFamilyMembers:YES];
    createEditCon.delegate = self;
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:createEditCon animated:YES];
    self.hidesBottomBarWhenPushed = YES;

}

#pragma mark - TableView Data source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	NSInteger count = 0;
	if (tableView == self.notificationsTable) {
		count = notificationsList.count;
        return count;
	} else {
        if (familyListArr.count == 0) {
            self.lblNoData.frame = tableView.frame;
            tableView.backgroundView=self.lblNoData;
            return 0;
            
        } else {
            tableView.backgroundView=nil;
            count = familyListArr.count;
            return count;
        }
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

// --------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
	if (tableView == self.notificationsTable)
    {
        SRConnectionNotificationCell *customCell = (SRConnectionNotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionNotificationCell" owner:self options:nil];
//		if (customCell == nil)
//        {
			if ([nibObjects count] > 0) {
				customCell = (SRConnectionNotificationCell *) nibObjects[0];
			}
//		}
		// Assign values
		NSDictionary *notiDict = notificationsList[indexPath.section];
        NSDictionary *userDetails;
        if (notiDict[kKeySender]) {
            userDetails = notiDict[kKeySender];
            //remove null values
            userDetails = [SRModalClass removeNullValuesFromDict:userDetails];
            notiDict = [SRModalClass removeNullValuesFromDict:notiDict];
        }
		NSString *fullName = [NSString stringWithFormat:@"%@ %@", userDetails[kKeyFirstName], userDetails[kKeyLastName]];

        if ([notiDict[kKeyReferenceStatus] isEqualToString:@"0"] || [notiDict[kKeyReferenceStatus] isEqualToString:@"2"]) {
            customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has added you in his family", fullName];
            CGRect lblFrame = ((SRConnectionNotificationCell *)customCell).lblNotification.frame;
            lblFrame.size.width = SCREEN_WIDTH;
            customCell.lblNotification.textAlignment = NSTextAlignmentCenter;
            customCell.lblNotification.frame = lblFrame;
            customCell.btnYesNotification.hidden = YES;
            customCell.btnNoNotification.hidden = YES;
            customCell.btnNoNotification.superview.tag = 100;
        }
        else if ([notiDict[kKeyReferenceStatus] isEqualToString:@"1"]) {
			customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has requested to track you.", fullName];
			customCell.btnYesNotification.hidden = NO;
			customCell.btnNoNotification.hidden = NO;
			customCell.btnNoNotification.superview.tag = 101;
		}
		else if ([notiDict[kKeyReferenceStatus] isEqualToString:@"4"]) {
			customCell.lblNotification.text = [NSString stringWithFormat:@"%@ is now tracking you!", fullName];
			customCell.btnYesNotification.hidden = YES;
			customCell.btnNoNotification.hidden = YES;
			customCell.btnNoNotification.superview.tag = 101;
		}
		else if ([notiDict[kKeyReferenceStatus] isEqualToString:@"5"]) {
			customCell.lblNotification.text = [NSString stringWithFormat:@"%@ has rejected your track request", fullName];
			customCell.btnYesNotification.hidden = YES;
			customCell.btnNoNotification.hidden = YES;
		}
        [customCell.lblNotification sizeToFit];
		customCell.btnYesNotification.tag = indexPath.section;
		[customCell.btnYesNotification addTarget:self action:@selector(acceptFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
		customCell.btnNoNotification.tag = indexPath.section;
		[customCell.btnNoNotification addTarget:self action:@selector(rejectFriendRequest:) forControlEvents:UIControlEventTouchUpInside];
		customCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return customCell;
	}
	else
    {
		NSDictionary *familyDict = familyListArr[indexPath.section];
		familyDict = [SRModalClass removeNullValuesFromDict:familyDict];
        SRFamilyViewCell *customCell =(SRFamilyViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
		NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRFamilyViewCell" owner:self options:nil];
        
		if ([nibObjects count] > 0)
        {
			customCell = (SRFamilyViewCell *) nibObjects[1];
            NSDictionary *userDetailDict;
            if (familyDict[kKeyRelative] !=nil && [familyDict[kKeyRelative] isKindOfClass:[NSDictionary class]])
            {
                // User dict
                userDetailDict = familyDict[kKeyRelative];
                // if expanded or allow track is enable
                if ([userDetailDict[kKeySetting] isKindOfClass:[NSDictionary class]] && ([[userDetailDict[kKeySetting] objectForKey:kKeyFamilyTrack] isEqualToString:@"1"] || [expandedList containsObject:@(indexPath.section)]))
                {
                    customCell = (SRFamilyViewCell *) nibObjects[0];

                    NSArray *locationsArr = familyDict[kKeyFamilyLocationArr];
                    CGRect latestFrame;
                    for (NSUInteger i = 0; i < [locationsArr count]; i++) {
					NSDictionary *locationDict = locationsArr[i];
					locationDict = [SRModalClass removeNullValuesFromDict:locationDict];
                    
                    if (i == 0) {
                        latestFrame = CGRectMake(0, 0, customCell.frame.size.width - customCell.lblLocation.frame.size.width + 10, customCell.scrollView.frame.size.height);
                    }
                    else
                        latestFrame = CGRectMake(latestFrame.origin.x + latestFrame.size.width + 2, 0, customCell.frame.size.width - customCell.lblLocation.frame.size.width + 10, customCell.scrollView.frame.size.height);
                    
                    // Add views
                    UIScrollView *locatnScrollView = [[UIScrollView alloc]init];
                    locatnScrollView.tag = indexPath.section;
                    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10]};
                    CGFloat textWidth =  [locationDict[kKeyLocation] sizeWithAttributes:attributes].width;
                    UILabel *lblType = [[UILabel alloc]initWithFrame:CGRectMake(0, 7, textWidth, 18)];
					lblType.text = locationDict[kKeyLocation];
//                    lblType.numberOfLines = 0;
//					[lblType sizeToFit];
					lblType.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
					lblType.textColor = [UIColor whiteColor];
					lblType.backgroundColor = [UIColor clearColor];

					UIButton *btnView = [[UIButton alloc]initWithFrame:CGRectMake(lblType.frame.origin.x + lblType.frame.size.width, 7, 25, 25)];
					btnView.backgroundColor = [UIColor clearColor];
					latestFrame.size.width = btnView.frame.origin.x + 25;

                    [btnView setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
					// Add target
					[btnView addTarget:self action:@selector(actionOnRadioButton:) forControlEvents:UIControlEventTouchUpInside];
					btnView.tag = i;

                    locatnScrollView.frame = latestFrame;
                    [locatnScrollView addSubview:lblType];
                    [locatnScrollView addSubview:btnView];

					// Scroll content size
					[customCell.scrollView addSubview:locatnScrollView];
					customCell.scrollView.contentSize = CGSizeMake(latestFrame.origin.x + latestFrame.size.width + 2, 0);
				}
			}

            // Name
            customCell.lblProfileNameLabel.text = [NSString stringWithFormat:@"%@ %@", userDetailDict[kKeyFirstName], [userDetailDict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)]];
                
            // User image
            if ([userDetailDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]])
            {
                if ([userDetailDict[kKeyProfileImage] objectForKey:kKeyImageName] != nil)
                {
                    NSString *imageName = [userDetailDict[kKeyProfileImage] objectForKey:kKeyImageName];
                    if ([imageName length] > 0)
                    {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                        [customCell.imgProfileImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                    }
                    else
                        customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
                else
                    customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            }
            else
                customCell.imgProfileImage.image = [UIImage imageNamed:@"deault_profile_bck.png"];

            // Relation
            customCell.lblDetailsLabel.text = familyDict[kKeyRelation];
                
            // User Direction:
            CLLocationCoordinate2D myLoc = { server.myLocation.coordinate.latitude,
                    server.myLocation.coordinate.longitude };
                
               
            if ([userDetailDict[kKeyLocation] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *userLocDict = userDetailDict[kKeyLocation];
                CLLocationCoordinate2D userLoc = { [[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue] };
                NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];
                [customCell.imgDirectionBtn addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
                customCell.imgDirectionBtn.tag = indexPath.section;
                
            
                if (directionValue == kKeyDirectionNorth) {
                    customCell.lblCompassDirection.text = @"N";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionEast) {
                    customCell.lblCompassDirection.text = @"E";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionSouth) {
                    customCell.lblCompassDirection.text = @"S";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionWest) {
                    customCell.lblCompassDirection.text = @"W";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionNorthEast) {
                    customCell.lblCompassDirection.text = @"NE";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionNorthWest) {
                    customCell.lblCompassDirection.text = @"NW";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionSouthEast) {
                    customCell.lblCompassDirection.text = @"SE";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionSoutnWest) {
                    customCell.lblCompassDirection.text = @"SW";
                    [customCell.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
                }
            }
                
                // Location
                customCell.lblDistanceLabel.textAlignment = NSTextAlignmentRight;
                if (!isNullObject(userDetailDict[kKeyLocation])) {
                    
                    if ([userDetailDict[kKeyLocation] objectForKey:kKeyLiveAddress] && [[userDetailDict[kKeyLocation] objectForKey:kKeyLiveAddress] length] > 1) {
                        customCell.lblAddressLabel.text = [userDetailDict[kKeyLocation] objectForKey:kKeyLiveAddress];
                        
                        if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                            if ([userDetailDict[kKeyDistance] floatValue] > 32) {
                                customCell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.2f"@" %@ %@", [userDetailDict[kKeyDistance] floatValue ], distanceUnit, @"approx"];
                            }
                            else
                                customCell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.2f"@" %@", [userDetailDict[kKeyDistance] floatValue ], distanceUnit];
                        }
                        else{
                            if ([userDetailDict[kKeyDistance] floatValue] > 20) {
                                customCell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.2f"@" %@ %@", [userDetailDict[kKeyDistance] floatValue], distanceUnit, @"approx"];
                            }
                            else
                                customCell.lblDistanceLabel.text = [NSString stringWithFormat:@"%.2f"@" %@", [userDetailDict[kKeyDistance] floatValue], distanceUnit];
                        }

                        
                    }
                    else
                    {
                        customCell.lblAddressLabel.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                        customCell.lblDistanceLabel.textAlignment = NSTextAlignmentCenter;
                        customCell.lblDistanceLabel.text = @"        N/A";
                    }
                    
                    
                    
                }
                else
                {
                    customCell.lblAddressLabel.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                    customCell.lblDistanceLabel.textAlignment = NSTextAlignmentCenter;
                    customCell.lblDistanceLabel.text = @"        N/A";
                }
                
                if ([customCell.lblAddressLabel.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {
                    customCell.lblDistanceLabel.textAlignment = NSTextAlignmentCenter;
                    customCell.lblDistanceLabel.text = @"        N/A";
                    
                }
                
            // Switch action
            customCell.notificationSwitch.layer.cornerRadius = 16.0;
            if ([userDetailDict[kKeySetting] isKindOfClass:[NSDictionary class]] && ([expandedList containsObject:@(indexPath.section)] || [[userDetailDict[kKeySetting] objectForKey:kKeyFamilyTrack] isEqualToString:@"1"]))
            {
                [customCell.notificationSwitch setOn:YES];
            }
            else
            {
                [customCell.notificationSwitch setOn:NO];
            }
            customCell.notificationSwitch.userInteractionEnabled = FALSE;
            customCell.notificationSwitch.tag = indexPath.section;
            [customCell.notificationSwitch addTarget:self action:@selector(actionOnSwitch:) forControlEvents:UIControlEventValueChanged];
                
//                // Adjust green dot label appearence
//                customCell.lblOnline.hidden = YES;
//                CGRect frame = customCell.lblOnline.frame;
//
//                // Profile online status
//                NSXMLElement *presence = [NSXMLElement elementWithName:@"presence"];
//                NSString *toObjectId = [NSString stringWithFormat:@"%@@%@", [userDetailDict objectForKey:kKeyId], kServerAddress];
//                XMPPJID *xmppJid = [XMPPJID jidWithString:toObjectId];
//
//                [presence addAttributeWithName:@"to" stringValue:[xmppJid bare]];
//                [presence addAttributeWithName:@"type" stringValue:@"subscribe"];
//
//                [(APP_DELEGATE).xmppStream sendElement:presence];
//
//                if ([server.onlineUserList containsObject:[NSString stringWithFormat:@"%@", [userDetailDict objectForKey:kKeyId]]]) {
//                    customCell.lblOnline.hidden = NO;
//                    // Adjust green dot label appearence
//                    customCell.lblOnline.layer.cornerRadius = customCell.lblOnline.frame.size.width / 2;
//                    customCell.lblOnline.layer.masksToBounds = YES;
//                    customCell.lblOnline.clipsToBounds = YES;
//
//                    customCell.lblProfileNameLabel.frame = CGRectMake(frame.origin.x + 15, customCell.lblProfileNameLabel.frame.origin.y, customCell.lblProfileNameLabel.frame.size.width + 10, customCell.lblProfileNameLabel.frame.size.height);
//
//                }else
//                {
//                    customCell.lblOnline.hidden = YES;
//                    customCell.lblProfileNameLabel.frame = CGRectMake(frame.origin.x, customCell.lblProfileNameLabel.frame.origin.y, customCell.lblProfileNameLabel.frame.size.width + 10, customCell.lblProfileNameLabel.frame.size.height);
//                }
            }
        }
        customCell.selectionStyle = UITableViewCellSelectionStyleNone;
         return customCell;
	}

	

	// Return
	return nil;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// heightForRowAtIndexPath:
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (tableView == self.notificationsTable) {
		return 35;
	}
	else {
		NSDictionary *familyDict = familyListArr[indexPath.section];
		// if expanded or allow track is enable
        if ([[familyDict[kKeyRelative] objectForKey:kKeySetting] isKindOfClass:[NSDictionary class]]) {
            if ([[[familyDict[kKeyRelative] objectForKey:kKeySetting] objectForKey:kKeyFamilyTrack] isEqualToString:@"1"] || [expandedList containsObject:@(indexPath.section)]) {
                return 152;
            }
            else
                return 120;
        }
        else
        {
            return 120;
        }
		
	}
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	CGFloat sectionHeight = 2.0;
	if (section == 0) {
		sectionHeight = 0.0;
	}

	// Return
	return sectionHeight;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *headerView = nil;
	if (section > 0) {
		headerView = [[UIView alloc] init];
		headerView.backgroundColor = [UIColor clearColor];
	}

	// Return
	return headerView;
}


#pragma mark
#pragma mark Private Methods
#pragma mark

- (NSDictionary *)updateDistance:(NSDictionary *)dict
{
    __block NSDictionary *updatedDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
    float distance = [[updatedDict[kKeyRelative] valueForKey:kKeyDistance]floatValue ];
    
    BOOL shouldCallMatrixAPI = NO;
    
    if ([distanceUnit isEqualToString:kkeyUnitMiles])
    {
        distance = distance * 1.1508;
        [updatedDict[kKeyRelative] setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 20) {
            
            shouldCallMatrixAPI = YES;
            
        }
    }
    else
    {
        distance = distance * 1.852;
        [updatedDict[kKeyRelative] setValue:[NSString stringWithFormat:@"%.2f", distance] forKey:kKeyDistance];
        if (distance <= 32) {
            
            shouldCallMatrixAPI = YES;
        }
    }
    
    if(shouldCallMatrixAPI)
    {
        
        [_matrixRequestFamilyArray addObject:updatedDict];
        
        
        
        
    }
    
    return updatedDict;
    
}

#pragma mark GoogleMap Distance Matrics Methods
-(void)callDistanceAPI:(NSDictionary *)userDict completionBlock:(void(^)(NSDictionary *dict,NSError *error))completionBlock
{
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude]floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong]floatValue]];
    
    
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=driving&language=en-EN&key=%@",fromLoc.coordinate.latitude,fromLoc.coordinate.longitude,toLoc.coordinate.latitude,toLoc.coordinate.longitude,kKeyGoogleMap];
    //        NSLog(@"urlStr::%@",urlStr);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    request.timeoutInterval = 180;
    [NSURLConnection sendAsynchronousRequest:request  queue:[NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (!error)
         {
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
             
           //  NSLog(@"\n\nMATRIX API RESPONSE:%@ \n\n",json);
             
             if ([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"] )
             {
                 //parse data
                 NSArray *rows = json[@"rows"];
                 NSDictionary *rowsDict = rows[0];
                 NSArray *dataArray = [rowsDict valueForKey:@"elements"];
                 NSDictionary *dict = dataArray[0];
                 NSDictionary *distanceDict = [dict valueForKey:@"distance"];
                 NSString *distance = [distanceDict valueForKey:@"text"];
                 
                 NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                 double convertDist = [distanceWithoutCommas doubleValue];
                 if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                     if ([distance containsString:@"km"]) {
                         
                     }
                     else
                     {
                         //meters to killometer
                         convertDist = convertDist / 1000;
                     }
                 }
                 else
                 {
                     if ([distance containsString:@"km"]) {
                         //killometer to miles
                         convertDist = convertDist * 0.621371192;
                     }
                     else
                     {
                         //meters to miles
                         convertDist = (convertDist * 0.000621371192);
                     }
                 }
                 
                 [userDict setValue:[NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit] forKey:kKeyDistance];
                 
                 
                 
                 __block NSDictionary *sourceUserDict = [SRModalClass addLocationOffset:userDict];
                 
                 NSUInteger indexOfUserObject = [familyListArr indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                     
                     if([obj[kKeyId] isEqualToString:sourceUserDict[kKeyId]])
                     {
                         return YES;
                         *stop= YES;
                         
                     }
                     else{
                         
                         return NO;
                         
                     }
                     
                 }];
                 
                 if (indexOfUserObject!=NSNotFound && indexOfUserObject<familyListArr.count) {
                     
                     familyListArr[indexOfUserObject] = sourceUserDict;
                     
                 }
                 
                 
                 NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldFamilyDataArray indexOfObjectPassingTest:^BOOL(familyDistanceClass* anotherObj, NSUInteger idx, BOOL * _Nonnull stop) {
                     
                     
                     if([anotherObj.dataId isEqualToString:userDict[kKeyId]])
                     {
                         return YES;
                     }
                     else{
                         
                         return NO;
                     }
                     
                 }];
                 
                 if(indexOfOldDataObject!=NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldFamilyDataArray.count)
                 {
                     familyDistanceClass *obj = [[familyDistanceClass alloc] init];
                     obj.dataId = userDict[kKeyId];
                     NSDictionary *locationDict = userDict[kKeyLocation];
                     obj.lat = locationDict[kKeyLattitude];
                     obj.longitude = locationDict[kKeyRadarLong];
                     obj.distance = [NSString stringWithFormat:@"%.2f %@", convertDist, distanceUnit];
                     obj.myLocation = server.myLocation;
                     obj.userDict = userDict;
                     
                     (APP_DELEGATE).oldFamilyDataArray[indexOfOldDataObject] = obj;
                     
                 }
                 
                 dispatch_async(dispatch_get_main_queue(), ^{

                     
                     [self.objtableView reloadData];
                     
                 });
                 
             }
             else{
                 
                 completionBlock(userDict,error);
             }
         }
         else{
             
             completionBlock(userDict,error);
         }
     }];
    
}


-(void)callDistanceAPIForMatrixRequestArr{
    
    NSMutableArray *requestsArray = [[NSMutableArray alloc] init];
    CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
   __block NSString *checkURL=@"";
    NSMutableArray *arrLocation=[[NSMutableArray alloc]init];
    int i=0;
    for (NSDictionary *userDict in _matrixRequestFamilyArray) {
        
        if (!isNullObject([userDict[kKeyRelative] valueForKey:kKeyLocation]))
        {
            arrLocation[i]=userDict;
            CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[userDict[kKeyLocation] valueForKey:kKeyLattitude]floatValue] longitude:[[userDict[kKeyLocation] valueForKey:kKeyRadarLong]floatValue]];
            
            NSString *urlStr =[NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true",kOSMServer,kOSMroutingAPi,fromLoc.coordinate.latitude,fromLoc.coordinate.longitude,toLoc.coordinate.latitude,toLoc.coordinate.longitude];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
            request.timeoutInterval = 180;
            
            [requestsArray addObject:request];
            i++;
        }
    }
    
    if(requestsArray.count)
    {
        __block NSInteger currentRequestIndex = 0;
        
        __weak typeof(self) weakSelf = self;
        
        _handler = ^void(NSURLResponse *response, NSData *data, NSError *error) {
            
            if (!error) {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

                if ([weakSelf IsValidResponse:data])
                {

                    dict[@"data"] = data; //add graphhopper response
                    
                    if(currentRequestIndex < weakSelf.matrixRequestFamilyArray.count)
                    {
                        dict[@"userDict"] = weakSelf.matrixRequestFamilyArray[currentRequestIndex];
                        [weakSelf.matrixResponseFamilyArray addObject:dict];
                    }
                    currentRequestIndex++;
                    if (currentRequestIndex < [requestsArray count])
                    {
                        NSURLRequest *requ= requestsArray[currentRequestIndex];
                        NSString *requestPath = [[requ URL] absoluteString];
                        checkURL=requestPath;
                        [NSURLConnection sendAsynchronousRequest:requestsArray[currentRequestIndex]
                                                           queue:[NSOperationQueue mainQueue]
                                               completionHandler:weakSelf.handler];
                    }
                    else
                    {
                        [weakSelf MatrixAPICallCompleted];
                    }
                }
            }
        };
        NSURLRequest *requ= requestsArray[0];
        NSString *requestPath = [[requ URL] absoluteString];
        checkURL=requestPath;
        [NSURLConnection sendAsynchronousRequest:requestsArray[0]
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:weakSelf.handler];
    }
}

-(Boolean)IsValidResponse:(NSData *)data
{
    Boolean isOSMValid=false;
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    for (NSString *keyStr in json)
    {
        if ([keyStr isEqualToString:@"paths"])
        {
            isOSMValid=true;
        }
    }
    if (isOSMValid ||([json[@"rows"] count] && [json[@"status"] isEqualToString:@"OK"] )) {
        return true;
    }
    else
    {
        return false;
    }
}

-(void)MatrixAPICallCompleted{
    
    [_matrixRequestFamilyArray removeAllObjects];
    if(_matrixResponseFamilyArray && _matrixResponseFamilyArray.count)
    {
        BOOL shouldReloadData = NO;
        
        for (NSDictionary *dict in _matrixResponseFamilyArray) {
            
            NSData *data = dict[@"data"];
            NSDictionary *userDict = dict[@"userDict"];
            Boolean isjsonValid=false;
            double convertDist = 0.0;
            NSError *error;
            if(data.length>0)
            {
                 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                if ([json[@"paths"] count])
                {
                    isjsonValid=true;
                    //parse data
                    NSDictionary *dictOSM=[json valueForKey:@"paths"];
                    NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                    NSString *distance = arrdistance[0];
                    convertDist = [distance doubleValue];
                    
                    NSString *distanceWithoutCommas = [distance stringByReplacingOccurrencesOfString:@"," withString:@""];
                    convertDist = [distanceWithoutCommas doubleValue];
                    if ([distanceUnit isEqualToString:kkeyUnitKilometers])
                    {
                            //meters to killometer
                            convertDist = convertDist / 1000;
                    }
                    else
                    {
                        if ([distance containsString:@"km"]) {
                            //killometer to miles
                            convertDist = convertDist * 0.621371192;
                        }
                        else
                        {
                            //meters to miles
                            convertDist = (convertDist * 0.000621371192);
                        }
                    }
                }
                else
                {
                   // NSLog(@"Matrix JSON not ok");
                    
                    NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldFamilyDataArray indexOfObjectPassingTest:^BOOL(familyDistanceClass* anotherObj, NSUInteger idx, BOOL * _Nonnull stop) {
                        
                        
                        if([anotherObj.dataId isEqualToString:userDict[kKeyId]])
                        {
                            return YES;
                        }
                        else{
                            
                            return NO;
                        }
                        
                    }];
                    
                    if(indexOfOldDataObject!=NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldFamilyDataArray.count)
                    {
                        [(APP_DELEGATE).oldFamilyDataArray removeObjectAtIndex:indexOfOldDataObject];
                    }
                }
            }
            else
            {
                isjsonValid=true;
                convertDist = [[userDict valueForKey:kKeyDistance] doubleValue];
            }
            
            if (isjsonValid)
            {
                [userDict setValue:[NSString stringWithFormat:@"%.2f",convertDist] forKey:kKeyDistance];
                
                NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldFamilyDataArray indexOfObjectPassingTest:^BOOL(familyDistanceClass* anotherObj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    
                    if([anotherObj.dataId isEqualToString:userDict[kKeyId]])
                    {
                        return YES;
                    }
                    else{
                        
                        return NO;
                    }
                    
                }];
                
                if(indexOfOldDataObject!=NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldFamilyDataArray.count)
                {
                    familyDistanceClass *obj = [[familyDistanceClass alloc] init];
                    obj.dataId = userDict[kKeyId];
                    NSDictionary *locationDict = userDict[kKeyLocation];
                    obj.lat = locationDict[kKeyLattitude];
                    obj.longitude = locationDict[kKeyRadarLong];
                    obj.distance = [NSString stringWithFormat:@"%.2f",convertDist];
                    obj.myLocation = server.myLocation;
                    obj.userDict = userDict;
                    
                    (APP_DELEGATE).oldFamilyDataArray[indexOfOldDataObject] = obj;
                    
                }
                
                __block NSDictionary *sourceUserDict = [SRModalClass addLocationOffset:userDict];
                
                NSUInteger indexOfUserObject = [familyListArr indexOfObjectPassingTest:^BOOL(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    if([obj[kKeyId] isEqualToString:sourceUserDict[kKeyId]])
                    {
                        return YES;
                        *stop= YES;
                        
                    }
                    else{
                        
                        return NO;
                        
                    }
                    
                }];
                
                if (indexOfUserObject!=NSNotFound && indexOfUserObject<familyListArr.count) {
                    
                    familyListArr[indexOfUserObject] = sourceUserDict;
                }
            }
        }
        
        if(shouldReloadData)
        {
            dispatch_async(dispatch_get_main_queue(), ^{

                [self.objtableView reloadData];
            });
        }
        else{
            
            NSLog(@"All Matrix API failed");
        }
        [_matrixResponseFamilyArray removeAllObjects];
    }
}


#pragma mark
#pragma mark Notification Method
#pragma mark


// --------------------------------------------------------------------------------
// familyActionSucceed:

- (void)familyActionSucceed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
	id object = [inNotify object];
	if ([object isKindOfClass:[NSArray class]]) {
        
        
        //
        
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        
        if((APP_DELEGATE).oldFamilyDataArray && (APP_DELEGATE).oldFamilyDataArray.count && familyListArr.count)
        {
            
            for (NSDictionary *userDict in [inNotify object]) {
                
                NSPredicate *predicateForDistanceObj = [NSPredicate predicateWithFormat:@"dataId=%@", userDict[kKeyId]];
                
                NSArray *tempArr = [(APP_DELEGATE).oldFamilyDataArray filteredArrayUsingPredicate:predicateForDistanceObj];
                
                if (tempArr && tempArr.count) {
                    
                    familyDistanceClass *distanceObj = [tempArr firstObject];
                    
                    NSDictionary *currentLocationDict = userDict[kKeyLocation];
                    NSDictionary *previousLocationDict = distanceObj.userDict[kKeyLocation];
                    
                    BOOL isBLocationChanged = NO;
                    BOOL isALocationChanged = NO;
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSDate *previousUpdateDate = [dateFormatter dateFromString:previousLocationDict[kKeyLastSeenDate]];
                    NSDate *currentDate = [dateFormatter dateFromString:currentLocationDict[kKeyLastSeenDate]];
                    
                    if([previousUpdateDate compare:currentDate]!=NSOrderedSame)
                    {
                        
                        
                        CLLocation *fromLoc = [[CLLocation alloc] initWithLatitude:[[currentLocationDict valueForKey:kKeyLattitude]floatValue] longitude:[[currentLocationDict valueForKey:kKeyRadarLong]floatValue]];
                        
                        CLLocation *toLoc = [[CLLocation alloc] initWithLatitude:[[previousLocationDict valueForKey:kKeyLattitude]floatValue] longitude:[[previousLocationDict valueForKey:kKeyRadarLong]floatValue]];
                        
                        CLLocationDistance userBdistance = [toLoc distanceFromLocation:fromLoc];
                        
                        if (userBdistance >= DISTANCEFILTER) {
                            
                            isBLocationChanged = YES;
                            
//                            NSLog(@"id: %@ location changed", userDict[kKeyId]);
//                            NSLog(@"Previous Lat: %@ Current Lat:%@",distanceObj.lat, currentLocationDict[kKeyLattitude]);
//                            NSLog(@"Previous Long: %@ Current Long:%@",distanceObj.longitude, currentLocationDict[kKeyRadarLong]);
                        }
                    }
                    
                    CLLocationDistance distance = [distanceObj.myLocation distanceFromLocation:(APP_DELEGATE).lastLocation];
                    
                    if (distance >= DISTANCEFILTER) {
                        
                        isALocationChanged = YES;
                        
                    }
                    
                    if(isBLocationChanged || isALocationChanged)
                    {
                        [arr addObject:userDict];
                    }
                    
                }
                else{
                    
                    
                    
                    [arr addObject:userDict];
                    
                    if ([userDict[kKeyRelative] valueForKey:kKeyLocation] != (id)[NSNull null]) {
                        [(APP_DELEGATE).oldFamilyDataArray addObject:[familyDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
                    }
                    
                }
            }
            
            
        }
        else{
            
            [(APP_DELEGATE).oldFamilyDataArray removeAllObjects];
            
            [arr addObjectsFromArray:[inNotify object]];
            
            for (NSDictionary *userDict in [inNotify object]) {
                
                if ([userDict[kKeyRelative] valueForKey:kKeyLocation] != (id)[NSNull null])
                {
                    [(APP_DELEGATE).oldFamilyDataArray addObject:[familyDistanceClass getObjectFromDict:userDict myLocation:server.myLocation]];
                }
            }
            
        }
        
        if (arr && arr.count) {
            
            NSLog(@"arr count: %lu",(unsigned long)arr.count);
            
            for (NSDictionary *dict in arr)
            {
                
                __block NSDictionary *updatedDict = [SRModalClass removeNullValuesFromDict:dict];
                NSDictionary *distanceDict = [self updateDistance:updatedDict];
                
              //  NSLog(@"id:%d",[distanceDict[kKeyId] intValue]);
                
                updatedDict = distanceDict;
                
                NSUInteger indexOfOldDataObject = [(APP_DELEGATE).oldFamilyDataArray indexOfObjectPassingTest:^BOOL(familyDistanceClass* anotherObj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    
                    if([anotherObj.dataId isEqualToString:updatedDict[kKeyId]])
                    {
                        return YES;
                    }
                    else{
                        
                        return NO;
                    }
                    
                }];
                
                if(indexOfOldDataObject!=NSNotFound && indexOfOldDataObject < (APP_DELEGATE).oldFamilyDataArray.count)
                {
                    
                    if ([updatedDict[kKeyRelative] valueForKey:kKeyLocation] != (id)[NSNull null])
                    {
                        familyDistanceClass *obj = [familyDistanceClass getObjectFromDict:updatedDict myLocation:server.myLocation];
                        
                        (APP_DELEGATE).oldFamilyDataArray[indexOfOldDataObject] = obj;
                    }
                    
                }
                
                updatedDict = [SRModalClass addLocationOffset:updatedDict];
      
                NSUInteger indexOfObject = [familyListArr indexOfObjectPassingTest:^BOOL(NSDictionary* anotherObj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    if([anotherObj[kKeyId] isEqualToString:updatedDict[kKeyId]])
                    {
                        return YES;
                    }
                    else{
                        
                        return NO;
                    }
                    
                }];
                
                if (indexOfObject!=NSNotFound && familyListArr && familyListArr.count && indexOfObject < familyListArr.count) {
                    
                    familyListArr[indexOfObject] = updatedDict;
                }
                else{
                    
                    [familyListArr addObject:updatedDict];
                }
                
            }
            
        }
        
        
        
        
    
        
        
        //
        
		// Remove all objects add new
//		[familyListArr removeAllObjects];
//		[familyListArr addObjectsFromArray:object];
        if (familyListArr.count == 0) {
            self.lblNoData.text=NSLocalizedString(@"lbl.nodata.txt", @"");
            rightButton.userInteractionEnabled = NO;
        }
        else
            rightButton.userInteractionEnabled = YES;
        
		[self.objtableView reloadData];
//        [_familyDistanceQueue cancelAllOperations];
//        [self getDistance];
        
        //By Madhura
        //[self calculateDistance];
         [self callDistanceAPIForMatrixRequestArr];
        
        
        
	}
	else if ([object isKindOfClass:[NSDictionary class]]) {
		// Search for dict if present then update or add
		NSDictionary *postInfo = [inNotify userInfo];
		NSDictionary *requestedParam = postInfo[kKeyRequestedParams];
		if (requestedParam[@"_method"] != nil) {
			// Call api to read status
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyReferenceID, [object objectForKey:kKeyId]];
			NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];
            if(filterArr && filterArr.count>0)
            {
                NSDictionary *notifyDict = filterArr[0];
                NSDictionary *param = @{kKeyStatus: @1};
                NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, notifyDict[kKeyId]];
                [server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
                [server makeAsychronousRequest:kKeyClassFamily inParams:nil isIndicatorRequired:NO inMethodType:kGET];
            }else
            {
                
            }

			// Acceptance alert
			if ([requestedParam[kKeyInvitationStatus] isEqualToString:@"2"]) {
                [SRModalClass showAlert: NSLocalizedString(@"alert.addFamily_member.txt", @"")];
			}
		}
		else {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [object objectForKey:kKeyId]];
			NSArray *filterArr = [familyListArr filteredArrayUsingPredicate:predicate];
			if (filterArr && [filterArr count] > 0) {
				NSDictionary *dict = filterArr[0];
				NSInteger index = [familyListArr indexOfObject:dict];
				familyListArr[index] = object;
			}
			else {
				//[familyListArr addObject:object];
			}
            
            if (familyListArr.count == 0) {
                self.lblNoData.text = NSLocalizedString(@"lbl.nodata.txt", @"");
                rightButton.userInteractionEnabled = NO;
            }
            else
                rightButton.userInteractionEnabled = YES;
            
			// Update table
			[self.objtableView reloadData];

		}
	}
}





// --------------------------------------------------------------------------------
// familyActionFailed:

- (void)familyActionFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    self.lblNoData.text=NSLocalizedString(@"lbl.nodata.txt", @"");
    [self.objtableView reloadData];
}

// --------------------------------------------------------------------------------
// getNotificationSucceed:

- (void)getNotificationSucceed:(NSNotification *)inNotify {
	if (!isClicked) {
		notificationsList = server.notificationArr;
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND (reference_type_id == %@)", @"0", @"2"];
		notificationsList = [notificationsList filteredArrayUsingPredicate:predicate];

		if ([notificationsList count] == 0) {
			self.btnNotifications.hidden = YES;
			self.imgNatificationsBG.hidden = YES;
			self.imgDropDown.hidden = YES;

			self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.imgBottomBarBGImage.frame.origin.y);
			[self.notificationsTable reloadData];
			self.notificationsTable.hidden = YES;
		}
		else {
			self.btnNotifications.hidden = NO;
			self.imgNatificationsBG.hidden = NO;
			self.imgDropDown.hidden = NO;

			self.objtableView.frame = CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height - 40);

			// Set Title to Notification Button
			NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long)[notificationsList count]]];
			[self.btnNotifications setTitle:str forState:UIControlStateNormal];
		}
	}
}

// --------------------------------------------------------------------------------
// getNotificationFailed:

- (void)getNotificationFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// updateNotificationSucceed:

- (void)updateNotificationSucceed:(NSNotification *)inNotify
{
	NSDictionary *userInfo = [inNotify userInfo];
	NSString *inObjectUrl = userInfo[kKeyObjectUrl];
	NSArray *componentArr  = [inObjectUrl componentsSeparatedByString:@"/"];
	
    if (componentArr && componentArr.count>1) {
        [updatedNotiList addObject:componentArr[1]];
    }

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, componentArr[1]];
	NSArray *filterArr = [notificationsList filteredArrayUsingPredicate:predicate];

	NSMutableArray *array = [NSMutableArray arrayWithArray:notificationsList];
    if (array.count > 0) {
        if (filterArr && filterArr.count > 0) {
            [array removeObject:filterArr[0]];
        }
        
    }
    if(array.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long)[array count]]];
        [self.btnNotifications setTitle:str forState:UIControlStateNormal];
        notificationsList = array;
        [self.notificationsTable reloadData];
    }
    else
    {
        self.btnNotifications.hidden = YES;
        self.imgNatificationsBG.hidden = YES;
        self.imgDropDown.hidden = YES;
        self.notificationsTable.hidden = YES;
        self.objtableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }

    // Update notification count in menu view
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdatedNotificationCount object:componentArr[1] userInfo:nil];
}

@end
