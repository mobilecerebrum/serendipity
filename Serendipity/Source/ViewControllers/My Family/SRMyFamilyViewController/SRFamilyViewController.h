#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface SRFamilyViewController : ParentViewController <UITableViewDataSource, UITableViewDelegate>
{
	// Instance variable
    NSMutableArray *familyListArr;
    NSMutableArray *cacheImgArr;
    NSMutableArray *expandedList;
    NSMutableArray *storedLocation;

	NSArray *notificationsList;
	NSMutableArray *updatedNotiList;
    NSString *distanceUnit;
    
	BOOL isClicked;
    BOOL isOnce;
	BOOL isNotificationUpdated;

    UIButton *rightButton;
    
	//Server
	SRServerConnection *server;
}

// Properties
@property (weak, nonatomic) IBOutlet SRProfileImageView *imgScreenBackGroundImage;

@property (weak, nonatomic) IBOutlet UIImageView *imgNatificationsBG;
@property (weak, nonatomic) IBOutlet UIButton *btnNotifications;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropDown;

@property (strong, nonatomic) IBOutlet UITableView *objtableView;

@property (weak, nonatomic) IBOutlet UIImageView *imgBottomBarBGImage;
@property (weak, nonatomic) IBOutlet UILabel *lblAddfamilyMember;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFamilyMember;

@property (strong, nonatomic) IBOutlet UITableView *notificationsTable;

// Other properties
@property (weak) id delegate;
@property (strong, nonatomic) UILabel *lblNoData;

//For Google Matrix API call and response
@property (nonatomic,strong) __block NSMutableArray *matrixRequestFamilyArray;
@property (nonatomic,strong) __block NSMutableArray *matrixResponseFamilyArray;
@property (nonatomic,copy) void (^handler)(NSURLResponse *response, NSData *data, NSError *error);

#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;

#pragma mark
#pragma mark IBAction Method
#pragma mark

- (IBAction)actionOnButtons:(id)sender;
- (IBAction)addFamilyMemberTapGesture:(id)sender;

@end
