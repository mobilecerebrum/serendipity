#import "SRModalClass.h"
#import "SRCreateEditFamilyView.h"
#import "SRLocationSetViewController.h"
#import "SRCreateEditFamilyViewController.h"
#import "SRMapViewController.h"

#define kKeyRelationFieldTag 300

@implementation SRCreateEditFamilyViewController

#pragma mark
#pragma mark Init Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer
        familyMembers:(NSArray *)inFamilyArr
   isAddFamilyMembers:(BOOL)isAddMembers {
    // Call Super
    self = [super initWithNibName:@"SRCreateEditFamilyViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        server = inServer;
        familyListArr = inFamilyArr;
        isAddFamilyMembers = isAddMembers;
        modifiedArrList = [[NSMutableArray alloc] init];

        // Register notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(getConnectionSucceed:)
                              name:kKeyNotificationGetConnectionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(getConnectionFailed:)
                              name:kKeyNotificationGetConnectionFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(familyActionSucceed:)
                              name:kKeyNotificationFamilyActionSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(familyActionFailed:)
                              name:kKeyNotificationFamilyActionFailed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateSucceed:)
                              name:kKeyNotificationSettingUpdateSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(settingUpdateFailed:)
                              name:kKeyNotificationSettingUpdateFailed object:nil];
    }

    //return
    return self;
}

//---------------------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Remove observers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Overrides
#pragma mark

//-----------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    // Call super
    [super viewDidLoad];

    // Title
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;

    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];

    // Call api to get connections
    if (isAddFamilyMembers) {
        [SRModalClass setNavTitle:NSLocalizedString(@"Add Family", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

        UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"plus-orange.png"] forViewNavCon:self offset:-23];
        [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];

        // Server call Get Connection
        [server makeAsychronousRequest:kKeyClassGetConnection inParams:nil isIndicatorRequired:NO
                          inMethodType:kGET];
    } else {
        [SRModalClass setNavTitle:NSLocalizedString(@"Edit Family", "") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];

        UIButton *rightButton = [SRModalClass setRightNavBarItem:nil barImage:[UIImage imageNamed:@"Edit-50.png"] forViewNavCon:self offset:-23];
        [rightButton addTarget:self action:@selector(plusBtnAction:) forControlEvents:UIControlEventTouchUpInside];

        [self loadScrollViewWithMembers];
    }

    // Set profile photo
    [SRModalClass setUserImage:server.loggedInUserInfo inSize:nil applyToView:self.profileImg];

    // Set the track value
    self.switchTrack.layer.cornerRadius = 16.0;
    NSDictionary *settings = server.loggedInUserInfo[kKeySetting];
    if ([settings isKindOfClass:[NSDictionary class]] && [[settings valueForKey:kKeyTrackFamily] isEqualToString:@"1"]) {
        [self.switchTrack setOn:YES];
    } else {
        [self.switchTrack setOn:NO];
    }
    // Set gesture to dismiss keyboard
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:tapGesture];
    [tapGesture setCancelsTouchesInView:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

#pragma mark
#pragma mark Private Method
#pragma mark

// --------------------------------------------------------------------------------
// hideKeyboard

- (void)hideKeyboard {
    // End editing
    [self.view endEditing:YES];
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}

#pragma mark
#pragma mark Load Scroll View Method
#pragma mark

//---------------------------------------------------------------------------------------------------------------------------------
// loadScrollViewWithMembers;

- (void)loadScrollViewWithMembers {

    [self.lblNoData removeFromSuperview];
    //Remove all subview
    for (UIView *subview in[self.scrollView subviews]) {
        [subview removeFromSuperview];
    }

    NSArray *usersListArr;
    if (isAddFamilyMembers) {
        usersListArr = connectionListArr;
    } else {
        usersListArr = familyListArr;
    }

    if (usersListArr.count > 0) {


        // Display the views
        CGRect latestFrame = CGRectZero;

        for (NSUInteger i = 0; i < [usersListArr count]; i++) {
            NSDictionary *detailDict = [SRModalClass removeNullValuesFromDict:usersListArr[i]];
            NSDictionary *dict;

            if (isAddFamilyMembers) {
                dict = [[NSDictionary alloc] initWithDictionary:detailDict];
            } else {
                if ([detailDict[kKeyRelative] isKindOfClass:[NSDictionary class]] && detailDict[kKeyRelative] != nil) {
                    dict = detailDict[kKeyRelative];
                }

            }
            if (CGRectIsEmpty(latestFrame)) {
                latestFrame = CGRectMake(0, 2, self.scrollView.frame.size.width, 139);
            } else {
                latestFrame = CGRectMake(0, latestFrame.origin.y + latestFrame.size.height, self.scrollView.frame.size.width, 139);
            }
            SRCreateEditFamilyView *view = [[SRCreateEditFamilyView alloc] initWithFrame:latestFrame];
            latestFrame = view.frame;
            view.userInfoDict = dict;
            view.txtFldRltn.delegate = self;
            view.txtFldRltn.tag = 300 + i;
            view.familyUserInfo = detailDict;

            // Display info in views
            // Name
            view.lblName.text = [NSString stringWithFormat:@"%@ %@", dict[kKeyFirstName], [dict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)]];

            // User image
            if ([dict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *profileDict = dict[kKeyProfileImage];
                if (profileDict[kKeyImageName] != nil) {
                    NSString *imageName = profileDict[kKeyImageName];
                    if ([imageName length] > 0) {
                        NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];

                        [view.imgProfileView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                    } else
                        view.imgProfileView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                } else
                    view.imgProfileView.image = [UIImage imageNamed:@"deault_profile_bck.png"];
            } else
                view.imgProfileView.image = [UIImage imageNamed:@"deault_profile_bck.png"];

            // User Direction:
            CLLocationCoordinate2D myLoc = {server.myLocation.coordinate.latitude, server.myLocation.coordinate.longitude};

            //Get and Set distance measurement unit
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                distanceUnit = kkeyUnitMiles;
            } else
                distanceUnit = kkeyUnitKilometers;

            if ([dict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *userLocDict = dict[kKeyLocation];
                CLLocationCoordinate2D userLoc = {[[userLocDict valueForKey:kKeyLattitude] floatValue], [[userLocDict valueForKey:kKeyRadarLong] floatValue]};
                NSInteger directionValue = [SRModalClass getHeadingForDirectionFromCoordinate:myLoc toCoordinate:userLoc];

                [view.imgDirectionBtn addTarget:self action:@selector(compassTappedCaptured:) forControlEvents:UIControlEventTouchUpInside];
                view.imgDirectionBtn.tag = i;
                if (directionValue == kKeyDirectionNorth) {
                    view.lblCompassDirection.text = @"N";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_north"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionEast) {
                    view.lblCompassDirection.text = @"E";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_east"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionSouth) {
                    view.lblCompassDirection.text = @"S";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_south"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionWest) {
                    view.lblCompassDirection.text = @"W";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_west"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionNorthEast) {
                    view.lblCompassDirection.text = @"NE";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_northeast"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionNorthWest) {
                    view.lblCompassDirection.text = @"NW";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_northwest"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionSouthEast) {
                    view.lblCompassDirection.text = @"SE";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_southeast"] forState:UIControlStateNormal];
                } else if (directionValue == kKeyDirectionSoutnWest) {
                    view.lblCompassDirection.text = @"SW";
                    [view.imgDirectionBtn setImage:[UIImage imageNamed:@"compass_southwest"] forState:UIControlStateNormal];
                }
                
                //Live Address
                //Address and Distance
                if ([dict[kKeyLocation] isKindOfClass:[NSDictionary class]]) {
                    if (![[dict[kKeyLocation] valueForKey:kKeyLiveAddress] isKindOfClass:[NSNull class]] && [[dict[kKeyLocation] valueForKey:kKeyLiveAddress] length] > 0) {
                        view.lblLocation.text = [NSString stringWithFormat:@"in %@", [dict[kKeyLocation] objectForKey:kKeyLiveAddress]];
                        //Distance
                        double distance = [dict[kKeyDistance] doubleValue];
                        view.lblDistance.text = [NSString stringWithFormat:@"%.1f %@", distance, distanceUnit];
                    } else {
                        view.lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                        view.lblDistance.textAlignment = NSTextAlignmentRight;
                    }
                } else {
                    view.lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                    view.lblDistance.textAlignment = NSTextAlignmentRight;
                }


                if ([view.lblLocation.text isEqualToString:NSLocalizedString(@"lbl.location_not_available.txt", @"")]) {
                    view.lblDistance.textAlignment = NSTextAlignmentCenter;
                    view.lblDistance.text = @"     N/A";

                }

            } else {
                view.lblLocation.text = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                view.lblDistance.textAlignment = NSTextAlignmentCenter;
                view.lblDistance.text = [NSString stringWithFormat:@"     N/A"];
            }
            // For Edit apply the values
            if (!isAddFamilyMembers) {
                // Relation
                view.txtFldRltn.text = detailDict[kKeyRelation];

                // Added Locations
                if ([detailDict[kKeyFamilyLocationArr] isKindOfClass:[NSArray class]] && [detailDict[kKeyFamilyLocationArr] count] > 0) {
                    NSArray *familyLocationArr = detailDict[kKeyFamilyLocationArr];
                    CGRect updatedFrame = CGRectZero;

                    // Get btn add parent view subviews
                    for (NSUInteger i = 0; i < [familyLocationArr count]; i++) {
                        NSDictionary *locationDict = familyLocationArr[i];

                        if (i == 0) {
                            updatedFrame = CGRectMake(8, 9, view.btnAddNewLocation.frame.size.width, 35);
                        } else
                            updatedFrame = CGRectMake(8, updatedFrame.size.height + updatedFrame.origin.y + 9, view.btnAddNewLocation.frame.size.width, 35);

                        SRFamilyAddressView *addressView = [[SRFamilyAddressView alloc] initWithFrame:updatedFrame];
                        updatedFrame = addressView.frame;

                        // Add values
                        // Type
                        addressView.txtFldAddType.text = locationDict[kKeyType];

                        // Address info
                        addressView.txtFldAddInfo.text = locationDict[kKeyLocation];
                        CLLocationCoordinate2D location;
                        location.latitude = [locationDict[kKeyLattitude] floatValue];
                        location.longitude = [locationDict[kKeyRadarLong] floatValue];

                        addressView.location = location;

                        addressView.txtFldAddType.delegate = self;
                        [addressView.btnClose addTarget:self action:@selector(actionOnDeleteAddView:) forControlEvents:UIControlEventTouchUpInside];
                        [addressView.btnLocation addTarget:self action:@selector(actionOnGetLocation:) forControlEvents:UIControlEventTouchUpInside];

                        addressView.addresDict = locationDict;

                        // Add to conatiner view
                        [view.containerView addSubview:addressView];
                    }
                    // Update button frame
                    view.btnAddNewLocation.frame = CGRectMake(8, updatedFrame.size.height + updatedFrame.origin.y + 9, view.btnAddNewLocation.frame.size.width, 35);
                    updatedFrame = view.btnAddNewLocation.frame;

                    // Update container frame
                    view.containerView.frame = CGRectMake(view.containerView.frame.origin.x, view.containerView.frame.origin.y, view.containerView.frame.size.width, updatedFrame.origin.y + updatedFrame.size.height + 6);

                    // Update the latest frame for view
                    latestFrame.size.height = view.containerView.frame.size.height + view.containerView.frame.origin.y + 5;

                    CGRect frame = view.frame;
                    frame.size.height = latestFrame.size.height;
                    view.frame = frame;
                }
            }

            // Add traget to button
            [view.btnAddNewLocation addTarget:self action:@selector(actionOnBtnAddAddress:) forControlEvents:UIControlEventTouchUpInside];

            // Add to scroll view
            view.tag = i;
            [self.scrollView addSubview:view];
            self.scrollView.contentSize = CGSizeMake(0, latestFrame.size.height + latestFrame.origin.y + 30);
        }
    } else {
        [self.lblNoData removeFromSuperview];
        self.lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH, 20)];
        self.lblNoData.text = NSLocalizedString(@"lbl.nodata.txt", @"");
        self.lblNoData.textAlignment = NSTextAlignmentCenter;
        self.lblNoData.textColor = [UIColor orangeColor];
        self.lblNoData.font = [UIFont boldSystemFontOfSize:16];
        [self.scrollView addSubview:self.lblNoData];
    }
}

//---------------------------------------------------------------------------------------------------------------------------------
// actionOnBtnAddAddress:

- (void)actionOnBtnAddAddress:(UIButton *)btnSender {
    // Add the view and update the view also scroll view
    UIView *viewOfBtn = btnSender.superview;
    SRCreateEditFamilyView *superView = (SRCreateEditFamilyView *) viewOfBtn.superview;

    if ([superView.txtFldRltn.text length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.add_relation.txt", @"")];
    } else {
        CGRect frame = btnSender.frame;

        // Add the location view
        SRFamilyAddressView *addressView = [[SRFamilyAddressView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 35)];
        [viewOfBtn addSubview:addressView];
        addressView.txtFldAddType.delegate = self;
        [addressView.btnClose addTarget:self action:@selector(actionOnDeleteAddView:) forControlEvents:UIControlEventTouchUpInside];
        [addressView.btnLocation addTarget:self action:@selector(actionOnGetLocation:) forControlEvents:UIControlEventTouchUpInside];

        // Update Button location
        btnSender.frame = CGRectMake(frame.origin.x, addressView.frame.origin.y + 39, frame.size.width, frame.size.height);

        // Update the height of container
        viewOfBtn.frame = CGRectMake(viewOfBtn.frame.origin.x, viewOfBtn.frame.origin.y, viewOfBtn.frame.size.width, btnSender.frame.size.height + btnSender.frame.origin.y + 6);

        // Update superview height
        CGRect superViewFrame = superView.frame;
        superViewFrame.size.height = viewOfBtn.frame.origin.y + viewOfBtn.frame.size.height + 8;
        superView.frame = superViewFrame;

        NSArray *subViewsArr = self.scrollView.subviews;

        // Get the view and update the rest views frame orgin in scroll view
        CGRect latestFrame = superViewFrame;
        for (NSUInteger i = 0; i < [subViewsArr count]; i++) {
            SRCreateEditFamilyView *subView = (SRCreateEditFamilyView *) subViewsArr[i];
            if (subView.tag > superView.tag) {
                CGRect subViewFrame = subView.frame;
                subViewFrame.origin.y = latestFrame.origin.y + latestFrame.size.height + 3;
                subView.frame = subViewFrame;
                latestFrame = subView.frame;
            }
        }

        self.scrollView.contentSize = CGSizeMake(0, latestFrame.size.height + latestFrame.origin.y + 30);
    }
//	}
}

//---------------------------------------------------------------------------------------------------------------------------------
// actionOnDeleteAddView:

- (void)actionOnDeleteAddView:(UIButton *)btnSender {
    SRFamilyAddressView *familyAddressView = (SRFamilyAddressView *) btnSender.superview;

    // Remove view from its superview
    UIView *familyAddSuperView = familyAddressView.superview;
    [familyAddressView removeFromSuperview];

    // Update remaining sub views frame
    NSArray *subViewsArr = familyAddSuperView.subviews;
    CGRect latestFrame;
    NSMutableArray *viewsArr = [NSMutableArray arrayWithArray:subViewsArr];
    UIButton *btnView = viewsArr[0];
    [viewsArr removeObjectAtIndex:0];

    // Add again
    [viewsArr addObject:btnView];

    for (NSUInteger i = 0; i < [viewsArr count]; i++) {
        UIView *subView = viewsArr[i];
        if (i == 0) {
            latestFrame = CGRectMake(8, 9, subView.frame.size.width, subView.frame.size.height);
        } else
            latestFrame = CGRectMake(8, latestFrame.size.height + latestFrame.origin.y + 9, subView.frame.size.width, subView.frame.size.height);
        subView.frame = latestFrame;
        latestFrame = subView.frame;

        // Update container frame
        familyAddSuperView.frame = CGRectMake(familyAddSuperView.frame.origin.x, familyAddSuperView.frame.origin.y, familyAddSuperView.frame.size.width, latestFrame.origin.y + latestFrame.size.height + 6);
    }



    // Update superview height
    SRCreateEditFamilyView *mostSuperView = (SRCreateEditFamilyView *) familyAddSuperView.superview;
    if (!isAddFamilyMembers) {
        mostSuperView.isEdit = YES;
    }
    CGRect superViewFrame = mostSuperView.frame;
    superViewFrame.size.height = familyAddSuperView.frame.origin.y + familyAddSuperView.frame.size.height + 8;
    mostSuperView.frame = superViewFrame;

    // Get the view and update the rest views frame orgin in scroll view
    latestFrame = superViewFrame;
    for (NSUInteger i = 0; i < [self.scrollView.subviews count]; i++) {
        SRCreateEditFamilyView *subView = (SRCreateEditFamilyView *) self.scrollView.subviews[i];
        if (subView.tag > mostSuperView.tag) {
            CGRect subViewFrame = subView.frame;
            subViewFrame.origin.y = latestFrame.origin.y + latestFrame.size.height + 3;
            subView.frame = subViewFrame;
            latestFrame = subView.frame;

            self.scrollView.contentSize = CGSizeMake(0, subView.frame.size.height + subView.frame.origin.y + 30);
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------------
// actionOnGetLocation:

- (void)actionOnGetLocation:(UIButton *)btnSender {
    selectedLocationView = (SRFamilyAddressView *) btnSender.superview;
    SRCreateEditFamilyView *superView = (SRCreateEditFamilyView *) selectedLocationView.superview.superview;
    if ([superView.txtFldRltn.text length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.add_relation.txt", @"")];
    } else if ([selectedLocationView.txtFldAddType.text length] == 0) {
        [SRModalClass showAlert:NSLocalizedString(@"alert.add_locationDetails.txt", @"")];
    } else {
        // Location from map
        SRLocationSetViewController *objLocationView = [[SRLocationSetViewController alloc] initWithNibName:nil bundle:nil server:server];
        objLocationView.delegate = self;
        objLocationView.getLocation = YES;
        self.navigationController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:objLocationView animated:YES];
        self.navigationController.hidesBottomBarWhenPushed = YES;
    }
}


#pragma mark
#pragma mark NavBar Action Methods
#pragma mark

// ----------------------------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    [self.navigationController popToViewController:self.delegate animated:NO];
}

// ----------------------------------------------------------------------------------------------------------
// plusBtnAction:

- (void)plusBtnAction:(id)sender {
    // End editing
    [self.view endEditing:YES];

    NSArray *subViewsArr = self.scrollView.subviews;
    if (subViewsArr.count > 0) {
        for (NSUInteger i = 0; i < [subViewsArr count]; i++) {
            UIView *view = subViewsArr[i];
            if ([view isKindOfClass:[SRCreateEditFamilyView class]] && ((SRCreateEditFamilyView *) view).isEdit) {
                SRCreateEditFamilyView *createEditView = (SRCreateEditFamilyView *) view;
                NSDictionary *userDict = createEditView.userInfoDict;
                NSDictionary *familyDict = createEditView.familyUserInfo;

                if ([createEditView.txtFldRltn.text length] == 0) {
                    NSString *string = [NSString stringWithFormat:@"Can not edit entry for %@ %@, relation hasn't metioned by you please add relation", userDict[kKeyFirstName], [userDict[kKeyLastName] substringWithRange:NSMakeRange(0, 1)]];
                    [SRModalClass showAlert:string];
                } else {
                    NSMutableDictionary *paramsDict = [NSMutableDictionary dictionary];
                    paramsDict[kKeyRelativeId] = userDict[kKeyId];
                    paramsDict[kKeyRelation] = createEditView.txtFldRltn.text;
                    if (isAddFamilyMembers) {
                        paramsDict[kKeyAllowPushNotificationTrack] = @"0";
                    } else {
                        paramsDict[kKeyAllowPushNotificationTrack] = familyDict[kKeyAllowPushNotificationTrack];
                        paramsDict[kKeyInvitationStatus] = @"2";
                    }

                    // Locations array
                    NSArray *containerSubViewsArr = createEditView.containerView.subviews;
                    NSMutableDictionary *locationArr = [NSMutableDictionary dictionary];
                    BOOL isAdded = NO;
                    NSString *defaultType = @"";

                    for (UIView *containerSubView in containerSubViewsArr) {
                        if ([containerSubView isKindOfClass:[SRFamilyAddressView class]]) {
                            SRFamilyAddressView *familyAddView = (SRFamilyAddressView *) containerSubView;
                            if ([familyAddView.txtFldAddInfo.text length] > 0 && [familyAddView.txtFldAddType.text length] > 0) {
                                NSString *latitude = [NSString localizedStringWithFormat:@"%f", familyAddView.location.latitude];
                                NSString *longitude = [NSString localizedStringWithFormat:@"%f", familyAddView.location.longitude];
                                NSDictionary *locationDict = @{kKeyAddress: familyAddView.txtFldAddInfo.text, kKeyLattitude: latitude, kKeyRadarLong: longitude};
                                locationArr[familyAddView.txtFldAddType.text] = locationDict;

                                if (isAddFamilyMembers && !isAdded) {
                                    isAdded = YES;
                                    paramsDict[kKeyDefault] = familyAddView.txtFldAddType.text;
                                } else {
                                    NSArray *serverLocationArr = familyDict[kKeyFamilyLocationArr];
                                    if ([serverLocationArr count] == 0 && !isAdded) {
                                        isAdded = YES;
                                        paramsDict[kKeyDefault] = familyAddView.txtFldAddType.text;
                                    } else {
                                        if (defaultType == nil) {
                                            defaultType = familyAddView.txtFldAddType.text;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // if location array
                    if ([locationArr count] > 0) {
                        paramsDict[kKeyFamilyLocation] = locationArr;

                        if (!isAddFamilyMembers && !isAdded) {
                            NSArray *serverLocationArr = familyDict[kKeyFamilyLocationArr];
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyMediaIsDefault, @"1"];
                            NSArray *filterArr = [serverLocationArr filteredArrayUsingPredicate:predicate];
                            if ([filterArr count] > 0) {
                                NSDictionary *dict = filterArr[0];
                                NSString *str = dict[kKeyType];

                                if (locationArr[str] == nil) {
                                    paramsDict[kKeyDefault] = defaultType;
                                }
                            }
                        }
                    } else {
                        paramsDict[kKeyFamilyLocation] = locationArr;
                        if ([familyDict[kKeyAllowPushNotificationTrack] isEqualToString:@"1"]) {
                            paramsDict[kKeyAllowPushNotificationTrack] = @"0";
                        }
                    }

                    // Add to modified array
                    [modifiedArrList addObject:paramsDict];
                    [(APP_DELEGATE) showActivityIndicator];

                    if (isAddFamilyMembers) {
                        [server makeAsychronousRequest:kKeyClassFamily inParams:paramsDict isIndicatorRequired:YES inMethodType:kPOST];
                    } else {
                        NSString *urlStr = [NSString stringWithFormat:@"%@/%@", kKeyClassFamily, familyDict[kKeyId]];
                        [server makeAsychronousRequest:urlStr inParams:paramsDict isIndicatorRequired:YES inMethodType:kPUT];
                    }
                }
            }
        }
    }
}

#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// compassTappedCaptured:
- (void)compassTappedCaptured:(UIButton *)sender {
    NSMutableDictionary *userDict;
    if (isAddFamilyMembers) {
        userDict = connectionListArr[sender.tag];
    } else
        userDict = [familyListArr[sender.tag] valueForKey:kKeyRelative];
    SRMapViewController *dropPin = [[SRMapViewController alloc] initWithNibName:@"FromFamilyView" bundle:nil inDict:userDict server:server];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:dropPin animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

#pragma mark
#pragma mark IBAction Method
#pragma mark

// --------------------------------------------------------------------------------
// switchAction:

- (IBAction)switchAction:(id)sender {
    NSString *strValue;
    if ([sender isOn]) {
        strValue = @"1";
    } else {
        strValue = @"0";
    }

    // Call api to update settings value
    NSDictionary *params = @{kKeyFieldName: kKeyTrackFamily, kKeyFieldValue: strValue};
    [server makeAsychronousRequest:kKeyClassSetting inParams:params isIndicatorRequired:NO inMethodType:kPOST];
}

#pragma mark
#pragma mark TextField Delegate Method
#pragma mark

// --------------------------------------------------------------------------------
// textFieldDidEndEditing:

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *txtStr = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    SRCreateEditFamilyView *createEditView;

    if (textField.tag >= 300) {
        createEditView = (SRCreateEditFamilyView *) textField.superview;
    } else {
        createEditView = (SRCreateEditFamilyView *) textField.superview.superview.superview;
    }

    if (isAddFamilyMembers) {
        if ([txtStr length] > 0 && textField.tag >= 300) {
            createEditView.isEdit = YES;
        } else if ([txtStr length] == 0 && textField.tag >= 300) {
            createEditView.isEdit = NO;
        }
    } else {
        NSDictionary *familyDict = createEditView.familyUserInfo;
        NSDictionary *locationDict;
        if (!isAddFamilyMembers && textField.tag == 0) {
            SRFamilyAddressView *addView = (SRFamilyAddressView *) textField.superview;
            locationDict = addView.addresDict;
        }
        if (textField.tag >= 300 && ![txtStr isEqualToString:familyDict[kKeyRelation]]) {
            createEditView.isEdit = YES;
        } else if (textField.tag == 0 && ![txtStr isEqualToString:locationDict[kKeyType]]) {
            createEditView.isEdit = YES;
        }
    }
//    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}

// --------------------------------------------------------------------------------
// textFieldDidBeginEditing:

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    SRCreateEditFamilyView *createEditView;

    if (sender.tag >= 300) {
        createEditView = (SRCreateEditFamilyView *) sender.superview;
    } else {
        createEditView = (SRCreateEditFamilyView *) sender.superview.superview.superview;
    }
    [self.scrollView setContentOffset:CGPointMake(0, createEditView.frame.origin.y)];
}

// --------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];

    // Return
    return YES;
}

#pragma mark
#pragma mark Location Controller Deelegate
#pragma mark

// -----------------------------------------------------------------------------------------------------
// locationAddressWithLatLong:

- (void)locationAddressWithLatLong:(NSString *)data centerCoordinate:(CLLocationCoordinate2D)inCenter {
    selectedLocationView.location = inCenter;
    selectedLocationView.txtFldAddInfo.text = data;

    // Edit info
    if (!isAddFamilyMembers) {
        SRCreateEditFamilyView *createEditView = (SRCreateEditFamilyView *) selectedLocationView.superview.superview;
        createEditView.isEdit = YES;
    }
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getConnectionSucceed:

- (void)getConnectionSucceed:(NSNotification *)inNotify {

    connectionListArr = [inNotify object];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyFirstName
                                                                   ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    connectionListArr = [connectionListArr sortedArrayUsingDescriptors:sortDescriptors];

    NSMutableArray *filteredArr = [NSMutableArray array];
    for (NSUInteger i = 0; i < [connectionListArr count]; i++) {
        NSDictionary *dict = connectionListArr[i];
        if ([dict[kKeyFamily] isKindOfClass:[NSNull class]]) {
            [filteredArr addObject:dict];
        } else if ([dict[kKeyFamily] isKindOfClass:[NSDictionary class]] && [[dict[kKeyFamily] objectForKey:kKeyInvitationStatus] isEqualToString:@"3"]) {
            [filteredArr addObject:dict];
        }
    }
    connectionListArr = filteredArr;

    // Load Scroll view with data
    [self loadScrollViewWithMembers];
}

// --------------------------------------------------------------------------------
// getConnectionFailed:

- (void)getConnectionFailed:(NSNotification *)inNotify {
    [self.lblNoData removeFromSuperview];
    self.lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH, 20)];
    self.lblNoData.text = NSLocalizedString(@"lbl.nodata.txt", @"");
    self.lblNoData.textAlignment = NSTextAlignmentCenter;
    self.lblNoData.textColor = [UIColor orangeColor];
    self.lblNoData.font = [UIFont boldSystemFontOfSize:16];
    [self.scrollView addSubview:self.lblNoData];
}

// --------------------------------------------------------------------------------
// familyActionSucceed:

- (void)familyActionSucceed:(NSNotification *)inNotify {
    id object = [inNotify object];
    if ([object isKindOfClass:[NSDictionary class]]) {
        // Remove objects from modified array and update the view
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyRelativeId, [object objectForKey:kKeyRelativeId]];
        NSArray *filterObject = [modifiedArrList filteredArrayUsingPredicate:predicate];
        if ([filterObject count] > 0) {
            NSDictionary *dict = filterObject[0];
            // Remove
            [modifiedArrList removeObject:dict];
        }

        // if modified array count is zero
        if ([modifiedArrList count] == 0) {
            [(APP_DELEGATE) hideActivityIndicator];
            [self actionOnBack:nil];
        }
    }
}

// --------------------------------------------------------------------------------
// familyActionFailed:

- (void)familyActionFailed:(NSNotification *)inNotify {
}
// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateSucceed:


- (void)settingUpdateSucceed:(NSNotification *)inNotify {
    NSDictionary *response = [[inNotify userInfo] valueForKey:kKeyResponse];
    NSString *trackFamily = [response valueForKey:kKeyTrackFamily];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setValue:trackFamily forKey:kKeyTrackFamily];
    [[server.loggedInUserInfo valueForKey:kKeySetting] setValue:trackFamily forKey:kKeyTrackFamily];
    [userDef synchronize];
}

// ----------------------------------------------------------------------------------------------------------------------
// settingUpdateFailed:

- (void)settingUpdateFailed:(NSNotification *)inNotify {

}

@end
