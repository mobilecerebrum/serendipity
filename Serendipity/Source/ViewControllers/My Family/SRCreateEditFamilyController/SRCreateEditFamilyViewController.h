#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"
#import "SRFamilyAddressView.h"
#import "SRServerConnection.h"
#import "SRCreateEditFamilyView.h"

@interface SRCreateEditFamilyViewController : ParentViewController <UITextFieldDelegate>

{
	// Instance Variable
	NSArray *familyListArr;
	NSArray *connectionListArr;
    NSMutableArray *modifiedArrList;

	SRFamilyAddressView *selectedLocationView;

	// Server
	SRServerConnection *server;

	// Flag
	BOOL isAddFamilyMembers;
    NSString *distanceUnit;
}

// Property
@property (weak, nonatomic) IBOutlet SRProfileImageView *profileImg;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISwitch *switchTrack;

//Other Properties
@property (weak) id delegate;
@property (strong, nonatomic) UILabel *lblNoData;

#pragma mark
#pragma mark Init Method
#pragma mark

- (id) initWithNibName:(NSString *)nibNameOrNil
                bundle:(NSBundle *)nibBundleOrNil
                server:(SRServerConnection *)inServer
         familyMembers:(NSArray *)inFamilyArr
    isAddFamilyMembers:(BOOL)isAddMembers;

#pragma mark
#pragma mark Action Method
#pragma mark

- (IBAction)switchAction:(id)sender;

@end
