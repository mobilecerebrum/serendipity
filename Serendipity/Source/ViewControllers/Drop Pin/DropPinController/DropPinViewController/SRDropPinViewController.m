#import "SRModalClass.h"
#import "SRMapProfileView.h"
#import "SRPinDetailView.h"
#import "SwipeableTableViewCell.h"
#import "SRDropPinWindowView.h"
#import "SRDropPinViewController.h"
#import "SRPanoramaViewController.h"

@implementation SRDropPinViewController
#pragma mark
#pragma mark Init Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// initWithNibName:
// Load the xib from this methood

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer {
    // Call super
    self = [super initWithNibName:@"SRDropPinViewController" bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
        server = inServer;
        droppedPinArr = [[NSMutableArray alloc] init];
        myPinsArr = [[NSMutableArray alloc] init];
        allMarkers = [[NSMutableArray alloc] init];
        allMyPinArray = [[NSMutableArray alloc] init];
        
        // Register the notifications
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(createUpdatePinSucceed:)
                              name:kKeyNotificationCreatePinSucceed object:nil];
        [defaultCenter addObserver:self
                          selector:@selector(createUpdatePinFailed:)
                              name:kKeyNotificationDeletePinFailed object:nil];
        
        // register for keyboard notifications
        [defaultCenter addObserver:self selector:@selector(keyboardWillShow:)
                              name:UIKeyboardWillShowNotification
                            object:self.view.window];
        // register for keyboard notifications
        [defaultCenter addObserver:self selector:@selector(keyboardWillHide:)
                              name:UIKeyboardWillHideNotification
                            object:self.view.window];
        
    }
    return self;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Remove all observers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark Standard Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// viewDidLoad

- (void)viewDidLoad {
    //Call Super
    [super viewDidLoad];
    
    // Set navigation bar
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // NavBar Title
    [SRModalClass setNavTitle:NSLocalizedString(@"navigation.title.DropAPin.view", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    UIButton *leftButton = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:25];
    [leftButton addTarget:self action:@selector(actionOnBack:) forControlEvents:UIControlEventTouchUpInside];
    [streetViewBtn addTarget:self action:@selector(touchEnded:withEvent:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    
    UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [customButton setImage:[UIImage imageNamed:@"tabbar-my-location.png"] forState:UIControlStateNormal];
    [customButton setTitle:@"Current Location" forState:UIControlStateNormal];
    [customButton.titleLabel setFont:[UIFont fontWithName:kFontHelveticaRegular size:10]];
    customButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, -25, 0);
    customButton.imageEdgeInsets = UIEdgeInsetsMake(-15, 0, 0, -100);
    [customButton addTarget:self action:@selector(addPinOnCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
    [customButton sizeToFit];
    UIBarButtonItem *customBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customButton];
    self.navigationItem.rightBarButtonItem = customBarButtonItem;
    
    center.latitude = server.myLocation.coordinate.latitude;
    center.longitude = server.myLocation.coordinate.longitude;
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 50, SCREEN_WIDTH, 44)];
    searchBar.userInteractionEnabled = YES;
    searchBar.searchBarStyle = UISearchBarIconClear;
    searchBar.translucent = YES;
    searchBar.placeholder = NSLocalizedString(@"Search or enter an address", "");
    [searchBar setBarTintColor:[UIColor blackColor]];
    searchBar.delegate = self;
    // set bar style
    searchBar.barStyle = UIBarStyleBlack;
    // set bar transparancy
    searchBar.translucent = NO;
    // set bar color
    searchBar.barTintColor = [UIColor blackColor];
    // set bar button color
    //  searchBar.tintColor = [UIColor whiteColor];
    // set bar background color
    searchBar.backgroundColor = [UIColor blackColor];
    if (@available(iOS 13.0, *)) {
        searchBar.searchTextField.backgroundColor = [UIColor whiteColor];
    } else {
        // Fallback on earlier versions
    }
    
    //find the UITextField view within searchBar (outlet to UISearchBar)
    //and assign self as delegate
    for (UIView *view in searchBar.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *tf = (UITextField *) view;
            tf.delegate = self;
            break;
        }
    }
    
    [self.view addSubview:searchBar];
    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
        distanceUnit = kkeyUnitMiles;
    } else
        distanceUnit = kkeyUnitKilometers;
    
    // Bring views to front
    lblNavigateInfo = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    lblNavigateInfo.font = [UIFont fontWithName:kFontHelveticaMedium size:12.0];
    lblNavigateInfo.numberOfLines = 3;
    lblNavigateInfo.textAlignment = NSTextAlignmentCenter;
    lblNavigateInfo.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.9];
    lblNavigateInfo.backgroundColor = [UIColor orangeColor];
    
    //For make half text bold
    NSString *infoStr = NSLocalizedString(@"drop.pin.navigate.title", "");
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:infoStr];
    
    [attrString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFontHelveticaBold size:12] range:NSMakeRange(0, [[infoStr componentsSeparatedByString:@"!"] firstObject].length + 1)];
    
    lblNavigateInfo.attributedText = attrString;
    [self.view addSubview:lblNavigateInfo];
    lblNavigateInfo.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnMapHideTableKeyboard)];
    tapGesture1.delegate = self;
    tapGesture1.numberOfTapsRequired = 1;
    tapGesture1.cancelsTouchesInView = NO;
    [lblNavigateInfo addGestureRecognizer:tapGesture1];
    
    
    oldLocation = server.myLocation.coordinate;
    
    // Set map
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude
                                                                 zoom:18];
    [self.mapView animateToCameraPosition:camera];
    self.mapView.mapType = kGMSTypeNormal;
    //self.mapView.myLocationEnabled = YES;
    self.mapView.delegate = self;
    
    // Creates a marker in the center of the map.
    [self showMapProfileView:(APP_DELEGATE).server.myLocation.coordinate.latitude andLongitude:(APP_DELEGATE).server.myLocation.coordinate.longitude];
    
    // Set Tab bar apperance
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBar.translucent = YES;
    self.tabBar.tintColor = [UIColor whiteColor];
    
    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBar.backgroundImage = image;
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10.0f], NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaRegular size:10.0f], NSForegroundColorAttributeName: appTintColor} forState:UIControlStateNormal];
    //  Set 1st tab
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-current-pin-orange.png"];
    [self.currentPinTab setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // Set 2nd tab
    tabImage = [UIImage imageNamed:@"tabbar-my-location.png"];
    [self.myLocationTab setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // Set 3rd tab
    tabImage = [UIImage imageNamed:@"tabbar-my-pins.png"];
    [self.myPinsTab setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    selectedTab = 1;
    self.tabBar.selectedItem = self.myLocationTab;
    
    // Create table view and add
    tblView = [[UITableView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 310, SCREEN_WIDTH, 200) style:UITableViewStylePlain];
    tblView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    tblView.delegate = self;
    tblView.dataSource = self;
    [self.view addSubview:tblView];
    tblView.hidden = YES;
    [tblView setClipsToBounds:YES];
    
    //Add searchbar for tableview
    searchBar1 = [[UISearchBar alloc] initWithFrame:CGRectMake(0, tblView.frame.origin.y - 44, SCREEN_WIDTH-100, 44)];
    searchBar1.userInteractionEnabled = YES;
    searchBar1.searchBarStyle = UISearchBarIconClear;
    searchBar1.translucent = YES;
    searchBar1.placeholder = NSLocalizedString(@"Search or enter an address", "");
    [searchBar1 setBarTintColor:[UIColor blackColor]];
    searchBar1.delegate = self;
    // set bar style
    searchBar1.barStyle = UIBarStyleBlack;
    // set bar transparancy
    searchBar1.translucent = NO;
    // set bar color
    searchBar1.barTintColor = [UIColor blackColor];
    // set bar background color
    searchBar1.backgroundColor = [UIColor blackColor];
    //    searchBar1.showsCancelButton = YES;
    //find the UITextField view within searchBar (outlet to UISearchBar)
    //and assign self as delegate
    for (UIView *view in searchBar1.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            UITextField *tf = (UITextField *) view;
            tf.delegate = self;
            break;
        }
    }
    
    [[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    
    searchBar1.hidden = YES;
    
    [self.view addSubview:searchBar1];
    
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setFrame:CGRectMake(SCREEN_WIDTH-100, tblView.frame.origin.y - 44, 100, 44)];
    [cancelButton setBackgroundColor:[UIColor blackColor]];
    cancelButton.hidden = YES;
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.view addSubview:cancelButton];
    
    [cancelButton addTarget:self action:@selector(btnCancelClick) forControlEvents:UIControlEventTouchUpInside];
    
    // Auto complete
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = _tableDataSource;
    _resultsController.tableView.dataSource = _tableDataSource;
    
    // Getsure
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnMapHideTableKeyboard)];
    tapGesture2.delegate = self;
    tapGesture2.numberOfTapsRequired = 1;
    tapGesture2.cancelsTouchesInView = NO;
    [self.mapView addGestureRecognizer:tapGesture2];
    
  //  NSLog(@"%@", [self deviceDetect]);
    if ([[self deviceDetect] isEqualToString:@"iPhone6Plus/6splus/7plus"]) {
        lblElevation.frame = CGRectMake(lblElevation.frame.origin.x + 30, lblElevation.frame.origin.y + 170, lblElevation.frame.size.width, lblElevation.frame.size.height - 20);
    } else if ([[self deviceDetect] isEqualToString:@"iPhone6/6s/7"]) {
        lblElevation.frame = CGRectMake(lblElevation.frame.origin.x + 10, lblElevation.frame.origin.y + 165, lblElevation.frame.size.width, lblElevation.frame.size.height - 20);
    } else if ([[self deviceDetect] isEqualToString:@"iPhone4"]) {
        lblElevation.frame = CGRectMake(lblElevation.frame.origin.x - 23, lblElevation.frame.origin.y + 137, lblElevation.frame.size.width - 1, lblElevation.frame.size.height - 22);
    } else if ([[self deviceDetect] isEqualToString:@"iPhone5/5s"]) {
        lblElevation.frame = CGRectMake(lblElevation.frame.origin.x - 17, lblElevation.frame.origin.y + 152, lblElevation.frame.size.width - 10, lblElevation.frame.size.height - 21);
    } else if ([[self deviceDetect] isEqualToString:@"iPhoneX"]) {
        lblElevation.frame = CGRectMake(lblElevation.frame.origin.x - 5, lblElevation.frame.origin.y + 152, lblElevation.frame.size.width - 10, lblElevation.frame.size.height - 21);
    }
    if ((APP_DELEGATE).isFromNotification) {
        //        (APP_DELEGATE).topBarView.hidden = YES;
        (APP_DELEGATE).topBarView.dropDownListView.hidden = YES;
        [(APP_DELEGATE).topBarView.dropDownListView removeFromSuperview];
        (APP_DELEGATE).topBarView.lblCommunity.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewCommunityDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblConnections.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewConnectionsDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblGroup.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewGroupDropDown.hidden = NO;
        (APP_DELEGATE).topBarView.lblTrack.hidden = NO;
        (APP_DELEGATE).topBarView.imgViewTrackDropDown.hidden = NO;
    }
}

- (NSString *)deviceDetect {
    NSString *device;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    if (screenHeight < screenWidth) {
        screenHeight = screenWidth;
    }
    
    if (screenHeight > 480 && screenHeight < 667) {
        device = @"iPhone5/5s";
        
    } else if (screenHeight > 480 && screenHeight < 736) {
        
        device = @"iPhone6/6s/7";
    } else if (screenHeight > 480) {
        
        device = @"iPhone6Plus/6splus/7plus";
    } else {
        
        device = @"iPhone4";
    }
    return device;
}


// ---------------------------------------------------------------------------------------
// viewWillAppear:

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Call the api to get all pins
    NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassGetPins, [server.loggedInUserInfo objectForKey:kKeyId]];
    [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
    
    
    //If from street view
    if (isPanoramaLoaded) {
        isPanoramaLoaded = NO;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:panoramaLastLoc.latitude longitude:panoramaLastLoc.longitude zoom:5];
        [self.mapView animateToCameraPosition:camera];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


// --------------------------------------------------------------------------------
- (void)updateMarkersForSelectedItem {
    // Find the selected item on tab
    [self.mapView clear];
    [allMarkers removeAllObjects];
    // Creates a marker in the center of the map
    [self showMapProfileView:(APP_DELEGATE).server.myLocation.coordinate.latitude andLongitude:(APP_DELEGATE).server.myLocation.coordinate.longitude];
    
    NSArray *pinsArr;
    
    CGRect frame;
    frame = btnSatelite.frame;
    frame.origin.y = self.view.frame.size.height - 85 - [self getBottomSafeArea];
    btnSatelite.frame = frame;
    
    frame = btnStreet.frame;
    frame.origin.y = self.view.frame.size.height - 85 - [self getBottomSafeArea];
    
    btnStreet.frame = frame;
    
    
    if (selectedTab == 0) {
        if (selectedMarker != nil) {
            dropPinWindow.hidden = NO;
        }
        tblView.hidden = YES;
        searchBar1.hidden = YES;
        cancelButton.hidden = YES;
        pinsArr = droppedPinArr;
        
        if (isMyPinSearch) {
            [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                
                CGRect frame;
                frame = self.view.frame;
                frame.origin.y = 64;
                self.view.frame = frame;
                [searchBar1 resignFirstResponder];
                
            }                completion:nil];
        }
        
    } else if (selectedTab == 1) {
        if (selectedMarker != nil) {
            dropPinWindow.hidden = NO;
        }
        tblView.hidden = YES;
        searchBar1.hidden = YES;
        cancelButton.hidden = YES;
        pinsArr = myPinsArr;
        
        if (isMyPinSearch) {
            [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                
                CGRect frame;
                frame = self.view.frame;
                frame.origin.y = 64;
                self.view.frame = frame;
                [searchBar1 resignFirstResponder];
                
            }                completion:nil];
        }
    } else if (selectedTab == 2) {
        if (selectedMarker != nil) {
            dropPinWindow.hidden = NO;
        }
        myPinsArr = allMyPinArray;
        if ([myPinsArr count] > 0) {
            // selectedTab = 2;
            tblView.hidden = NO;
            searchBar1.hidden = NO;
            cancelButton.hidden = NO;
            searchBar1.text = nil;
            
            CGRect frame;
            frame = btnSatelite.frame;
            frame.origin.y = tblView.frame.origin.y - 44 - [self getBottomSafeArea];
            btnSatelite.frame = frame;
            
            frame = btnStreet.frame;
            frame.origin.y = tblView.frame.origin.y - 44 - [self getBottomSafeArea];
            btnStreet.frame = frame;
                        
            pinsArr = myPinsArr;
            [tblView reloadData];
        } else {
            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", @"") message:NSLocalizedString(@"alert.no_pins_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
            [panoAlert show];
            selectedTab = 1;
            tblView.hidden = YES;
            searchBar1.hidden = YES;
            cancelButton.hidden = YES;
            searchBar1.text = nil;
        }
    }
    
    // Display the pins
    for (NSUInteger i = 0; i < [pinsArr count]; i++) {
        NSDictionary *locationDict = [pinsArr objectAtIndex:i];
        CLLocationCoordinate2D coordinate = {[[locationDict valueForKey:kKeyLattitude] floatValue], [[locationDict valueForKey:kKeyRadarLong] floatValue]};
        
        GMSMarker *dropMarker = [[GMSMarker alloc] init];
        dropMarker.position = coordinate;
        dropMarker.title = [locationDict objectForKey:kKeyName];
        dropMarker.snippet = [locationDict objectForKey:kKeyAddress];
        dropMarker.userData = locationDict;
        dropMarker.icon = [UIImage imageNamed:@"map-pin.png"];
        if (selectedMarker) {
            if ([[NSString stringWithFormat:@"%@", [locationDict objectForKey:kKeyId]] isEqualToString:[selectedMarker.userData objectForKey:kKeyId]]) {
                dropMarker.icon = [UIImage imageNamed:@"radar-pin-white-20px.png"];
            }
        }
        dropMarker.map = self.mapView;
        // Add to markers
        [allMarkers addObject:dropMarker];
    }
}
// --------------------------------------------------------------------------------
// tapOnProfilePicView

- (IBAction)addPinOnCurrentLocation {
    [searchBar resignFirstResponder];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake((APP_DELEGATE).server.myLocation.coordinate.latitude, (APP_DELEGATE).server.myLocation.coordinate.longitude);
    
    [self mapView:self.mapView didLongPressAtCoordinate:coordinate];
}
// --------------------------------------------------------------------------------
// tapOnMapHideTableKeyboard

- (void)tapOnMapHideTableKeyboard {
    // Resign keyboard
    [self.view endEditing:YES];
    //[searchBar resignFirstResponder];
    tblView.hidden = YES;
    searchBar1.hidden = YES;
    cancelButton.hidden = YES;
    searchBar1.text = nil;
    
    CGRect frame;
    frame = btnSatelite.frame;
    frame.origin.y = self.view.frame.size.height - 85 - [self getBottomSafeArea];
    btnSatelite.frame = frame;

    frame = btnStreet.frame;
    frame.origin.y = self.view.frame.size.height - 85 - [self getBottomSafeArea];
    btnStreet.frame = frame;
}

// --------------------------------------------------------------------------------
// imageFromView:

- (UIImage *)imageFromView:(UIView *)view {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// setMyLocationInView:

- (void)setMyLocationInView:(CLLocationCoordinate2D)inCenter {
    
    _mapView.layer.cameraLatitude = inCenter.latitude;
    _mapView.layer.cameraLongitude = inCenter.longitude;
    _mapView.layer.cameraBearing = 0.0;
    
    // Access the GMSMapLayer directly to modify the following properties with a
    // specified timing function and duration.
    
    CAMediaTimingFunction *curve =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    CABasicAnimation *animation;
    
    animation = [CABasicAnimation animationWithKeyPath:kGMSLayerCameraLatitudeKey];
    animation.duration = 2.0f;
    animation.timingFunction = curve;
    animation.toValue = @(inCenter.latitude);
    [_mapView.layer addAnimation:animation forKey:kGMSLayerCameraLatitudeKey];
    
    animation = [CABasicAnimation animationWithKeyPath:kGMSLayerCameraLongitudeKey];
    animation.duration = 2.0f;
    animation.timingFunction = curve;
    animation.toValue = @(inCenter.longitude);
    [_mapView.layer addAnimation:animation forKey:kGMSLayerCameraLongitudeKey];
    
    animation = [CABasicAnimation animationWithKeyPath:kGMSLayerCameraBearingKey];
    animation.duration = 2.0f;
    animation.timingFunction = curve;
    animation.toValue = @0.0;
    [_mapView.layer addAnimation:animation forKey:kGMSLayerCameraBearingKey];
    
    // Fly out to the minimum zoom and then zoom back to the current zoom!
    CGFloat zoom = _mapView.camera.zoom;
    CGFloat minZoom = 4;
    NSArray *keyValues = @[@(zoom), @(minZoom), @(zoom)];
    CAKeyframeAnimation *keyFrameAnimation =
    [CAKeyframeAnimation animationWithKeyPath:kGMSLayerCameraZoomLevelKey];
    keyFrameAnimation.duration = 2.0f;
    keyFrameAnimation.values = keyValues;
    [_mapView.layer addAnimation:keyFrameAnimation forKey:kGMSLayerCameraZoomLevelKey];
    
    // Creates a marker in the center of the map.
    [self showMapProfileView:inCenter.latitude andLongitude:inCenter.longitude];
}

#pragma mark
#pragma mark Stret view Action Method
#pragma mark

//
//---------------------------------------------------
// Show street view methods
- (IBAction)wasDragged:(id)sender withEvent:(UIEvent *)event {
    UIButton *selected = (UIButton *) sender;
    selected.center = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchEnded:(UIButton *)addOnButton withEvent:event {
    // [self.mapView removeGestureRecognizer:tapRecognizer];
   // NSLog(@"touchEnded called......");
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
 //   NSLog(@" In Touch Ended : touchpoint.x and y is %f,%f", touchPoint.x, touchPoint.y);
    CLLocationCoordinate2D tapPoint = [_mapView.projection coordinateForPoint:touchPoint];
    
    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_main_queue(), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    // Zoom in one zoom level
                    [_mapView animateToZoom:12];
                    [_mapView animateToBearing:90];
                    [_mapView animateToViewingAngle:45];
                    
                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    
                    if (!(CGRectContainsPoint(streetViewBtn.frame, touchPoint))) {
                        if (!isPanoramaLoaded) {
                            UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                            panoAlert.tag = 3;
                            [panoAlert show];
                        }
                    }
                }
            }];
        });
    });
}

- (IBAction)ShowStreetView:(id)sender {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.cancelsTouchesInView = YES;
    [self.mapView addGestureRecognizer:tapRecognizer];
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapPoint = [_mapView.projection coordinateForPoint:point];
    // Show panorama view if available
    [UIView animateWithDuration:0.5f animations:^{
        streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [googleStreetViewService requestPanoramaNearCoordinate:tapPoint radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    panoramaLastLoc = tapPoint;
                    // Zoom in one zoom level
                    [_mapView animateToZoom:12];
                    [_mapView animateToBearing:90];
                    [_mapView animateToViewingAngle:45];
                    
                    // Delay execution of my block for 3 seconds.
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        // Go to panorama view
                        isPanoramaLoaded = YES;
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = tapPoint.latitude;
                        panoramaView.requestedLong = tapPoint.longitude;
                        self.hidesBottomBarWhenPushed = YES;
                        [self.navigationController pushViewController:panoramaView animated:YES];
                        self.hidesBottomBarWhenPushed = NO;
                    });
                } else {
                    if (!isPanoramaLoaded) {
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        panoAlert.tag = 3;
                        [panoAlert show];
                    }
                }
            }];
        });
    });
    [recognizer removeTarget:self action:nil];
}


#pragma mark
#pragma mark Action Methods
#pragma mark

// ---------------------------------------------------------------------------------------
// actionOnBack:

- (void)actionOnBack:(id)sender {
    if ((APP_DELEGATE).isFromNotification) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
        (APP_DELEGATE).isFromNotification = NO;
        (APP_DELEGATE).isNotificationFromClosed = FALSE;
        
    } else {
        [(APP_DELEGATE) hideActivityIndicator];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

// ---------------------------------------------------------------------------------------
// ShowPanoramaView:
- (void)ShowPanoramaView {
    isOnce = NO;
    // Show panorama view if available
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[dropPinWindow.infoDict valueForKey:kKeyLattitude] floatValue], [[dropPinWindow.infoDict valueForKey:kKeyRadarLong] floatValue]);
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [googleStreetViewService requestPanoramaNearCoordinate:coordinate radius:50.0 callback:^(GMSPanorama *panorama, NSError *error) {
                if (!error) {
                    if (!isOnce) {
                        isOnce = YES;
                        // Go to panorama view
                        SRPanoramaViewController *panoramaView = [[SRPanoramaViewController alloc] initWithNibName:@"SRPanoramaViewController" bundle:nil];
                        panoramaView.requestedLat = [[dropPinWindow.infoDict valueForKey:kKeyLattitude] floatValue];
                        panoramaView.requestedLong = [[dropPinWindow.infoDict valueForKey:kKeyRadarLong] floatValue];
                        [self.navigationController pushViewController:panoramaView animated:YES];
                    }
                    
                } else {
                    if (!isOnce) {
                        isOnce = YES;
                        UIAlertView *panoAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text", @"") otherButtonTitles:nil, nil];
                        [panoAlert show];
                    }
                }
            }];
        });
    });
}
// --------------------------------------------------------------------------------
// deletePinFromList:

- (void)deletePinFromList:(UIButton *)inSender {
    
    // Delete the pin
    pinTag = inSender.tag;
    
    UIAlertView *deleteAlert = [[UIAlertView alloc]
                                initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:NSLocalizedString(@"delete.pin.alert.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.no.text", @"") otherButtonTitles:NSLocalizedString(@"alert.button.yes.text", @""), nil];
    deleteAlert.tag = 1;
    [deleteAlert show];
    
}

// ---------------------------------------------------------------------------------------
// alertView Delegate:
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // tag 1 for delete pin from list alert
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [(APP_DELEGATE) showActivityIndicator];
            // Remove polyline and pinview from map
            [dropPinWindow removeFromSuperview];
            if (lblElevation.hidden == NO) {
                lblElevation.hidden = YES;
                [self.updateElevation invalidate];
                self.updateElevation = nil;
            }
            polyline.map = nil;
            polyline = nil;
            // Delete the pin
            NSDictionary *pinDict = [myPinsArr objectAtIndex:pinTag];
            NSString *strUrl = [NSString stringWithFormat:@"%@/%@", kKeyClassCreatePins, [pinDict objectForKey:kKeyId]];
            [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
        }
    } else if (alertView.tag == 2) {
        if (buttonIndex == 1) {
            [(APP_DELEGATE) showActivityIndicator];
            
            //delete the pin
            SRDropPinWindowView *superView = (SRDropPinWindowView *) pinSuperView;
            NSDictionary *pinDict = superView.infoDict;
            
            // Remove polyline and pinview from map
            [dropPinWindow removeFromSuperview];
            if (lblElevation.hidden == NO) {
                lblElevation.hidden = YES;
                [self.updateElevation invalidate];
                self.updateElevation = nil;
            }
            polyline.map = nil;
            polyline = nil;
            
            NSString *strUrl = [NSString stringWithFormat:@"%@/%@", kKeyClassCreatePins, [pinDict objectForKey:kKeyId]];
            [server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kDELETE];
        }
    } else if (alertView.tag == 3) {
        [UIView animateWithDuration:0.5f animations:^{
            streetViewBtn.frame = CGRectOffset(self.pegmanImg.frame, 0, 0);
        }];
    }
    
}
// ---------------------------------------------------------------------------------------
// actionOnDeletePin:

- (void)actionOnDeletePin:(UIButton *)inSender {
    // Delete the pin
    pinSuperView = inSender.superview;
    
    UIAlertView *deleteAlert = [[UIAlertView alloc]
                                initWithTitle:NSLocalizedString(@"alert.title.text", @"") message:NSLocalizedString(@"delete.pin.alert.txt", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.no.text", @"") otherButtonTitles:NSLocalizedString(@"alert.button.yes.text", @""), nil];
    deleteAlert.tag = 2;
    [deleteAlert show];
    
}

// --------------------------------------------------------------------------------
// placeLocationInMapApp:

- (void)placeLocationInMapApp:(UIButton *)inSender {
    SRDropPinWindowView *superView = (SRDropPinWindowView *) inSender.superview;
    selecetdMarkerDict = superView.infoDict;
    
    // give the user a choice of Apple or Google Maps
//    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Open in Maps" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Apple Maps", @"Google Maps", nil];
//    [sheet showInView:self.view];
    
    CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake([[selecetdMarkerDict objectForKey:kKeyLattitude] floatValue], [[selecetdMarkerDict objectForKey:kKeyRadarLong] floatValue]);

    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Open in Maps" message:@"alert controller" preferredStyle:UIAlertControllerStyleActionSheet];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Apple Maps" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {

            // Apple Maps, using the MKMapItem class
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
            MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
            item.name = [selecetdMarkerDict objectForKey:kKeyName];
            [item openInMapsWithLaunchOptions:nil];
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Google Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            //Google Maps
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemapsurl://maps.google.com/?q=%f,%f", rdOfficeLocation.latitude, rdOfficeLocation.longitude]];
            
            if (![[UIApplication sharedApplication] canOpenURL:url]) {
                url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.google.com/?q=%f,%f", rdOfficeLocation.latitude, rdOfficeLocation.longitude]];
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            }

            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void)showPlaceDirectionInMapApp:(UIButton *)inSender {
    SRDropPinWindowView *superView = (SRDropPinWindowView *) inSender.superview;
    selecetdMarkerDict = superView.infoDict;
    CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake([[selecetdMarkerDict objectForKey:kKeyLattitude] floatValue], [[selecetdMarkerDict objectForKey:kKeyRadarLong] floatValue]);
    
    NSString *urlString = [NSString stringWithFormat:
                           @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@&mode=walking",
                           @"https://maps.googleapis.com/maps/api/directions/json",
                           server.myLocation.coordinate.latitude,
                           server.myLocation.coordinate.longitude,
                           rdOfficeLocation.latitude,
                           rdOfficeLocation.longitude, kKeyServerGoogleMap];
    NSURL *directionsURL = [NSURL URLWithString:urlString];
    
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:directionsURL];
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            GMSPath *path = [GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
            polyline = [GMSPolyline polylineWithPath:path];
            polyline.strokeWidth = 7;
            polyline.strokeColor = [UIColor greenColor];
            polyline.map = self.mapView;
        }
    }];
    
}
// ---------------------------------------------------------------------------------------
// clickedButtonAtIndex:

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // coordinates for the place we want to display
    CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake([[selecetdMarkerDict objectForKey:kKeyLattitude] floatValue], [[selecetdMarkerDict objectForKey:kKeyRadarLong] floatValue]);
    if (buttonIndex == 0) {
        // Apple Maps, using the MKMapItem class
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
        item.name = [selecetdMarkerDict objectForKey:kKeyName];
        [item openInMapsWithLaunchOptions:nil];
    } else if (buttonIndex == 1) {
        //Google Maps
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemapsurl://maps.google.com/?q=%f,%f", rdOfficeLocation.latitude, rdOfficeLocation.longitude]];
        
        if (![[UIApplication sharedApplication] canOpenURL:url]) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.google.com/?q=%f,%f", rdOfficeLocation.latitude, rdOfficeLocation.longitude]];
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }
    }
}

// ---------------------------------------------------------------------------------------
// singleTapGestureCaptured:

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)sender {
    SwipeableTableViewCell *cell = (SwipeableTableViewCell *) sender.view;
    if (CGPointEqualToPoint(cell.scrollView.contentOffset, CGPointZero)) {
        // Get pins detail
        NSDictionary *pinDict = [myPinsArr objectAtIndex:cell.tag];
        
        // Animate map to that position
        CLLocationCoordinate2D coordinate = {[[pinDict valueForKey:kKeyLattitude] floatValue], [[pinDict valueForKey:kKeyRadarLong] floatValue]};
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:18];
        [self.mapView animateToCameraPosition:camera];
        
        // Create current selected marker
        GMSMarker *dropMarker = [[GMSMarker alloc] init];
        dropMarker.position = coordinate;
        dropMarker.title = [pinDict objectForKey:kKeyName];
        dropMarker.snippet = [pinDict objectForKey:kKeyAddress];
        dropMarker.userData = pinDict;
        dropMarker.icon = [UIImage imageNamed:@"map-pin.png"];
        selectedMarker = dropMarker;
        [self showPinWindow:pinDict];
        
        
    } else
        [cell.scrollView setContentOffset:CGPointZero animated:YES];
}

// --------------------------------------------------------------------------------
// showPinWindow:
- (void)showPinWindow:(NSDictionary *)pinDict {
    // Open pinWindow to show details
    [dropPinWindow removeFromSuperview];
    
    
    CLLocationCoordinate2D coordinate = {[[pinDict valueForKey:kKeyLattitude] floatValue], [[pinDict valueForKey:kKeyRadarLong] floatValue]};
    
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:coordinate];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    CGRect frame = CGRectMake(pointInViewCoords.x - 95, pointInViewCoords.y - 300, 190, 203);
    
    if (dropPinWindow == nil) {
        dropPinWindow = [[SRDropPinWindowView alloc] initWithFrame:frame];
    } else
        dropPinWindow.frame = frame;
    
    dropPinWindow.infoDict = pinDict;
    
    NSString *imageUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/streetview?size=190x92&location=%@,%@&heading=151.78&pitch=-0.76&key=%@", pinDict[kKeyLattitude], pinDict[kKeyRadarLong], kKeyGoogleMap];
    NSURL *imageUrl = [NSURL URLWithString:imageUrlStr];
    [dropPinWindow.BGImage sd_setImageWithURL:imageUrl];
    // Add tap gesture
    UITapGestureRecognizer *tapBGimage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ShowPanoramaView)];
    tapBGimage.delegate = self;
    [dropPinWindow.BGImage addGestureRecognizer:tapBGimage];
    
    dropPinWindow.BGImage.backgroundColor = [UIColor blackColor];
    dropPinWindow.trashBtn.hidden = NO;
    dropPinWindow.openInMapBtn.hidden = NO;
    dropPinWindow.mapImage.hidden = NO;
    dropPinWindow.openInMapLbl.hidden = NO;
    dropPinWindow.titleTxtFld.enabled = NO;
    dropPinWindow.titleTxtFld.textColor = [UIColor whiteColor];
    dropPinWindow.lblMiles.textColor = [UIColor whiteColor];
    dropPinWindow.addressLbl.textColor = [UIColor whiteColor];
    dropPinWindow.editImage.image = [UIImage imageNamed:@"pencil.png"];
    dropPinWindow.isEdit = NO;
    
    
    // Name
    dropPinWindow.titleTxtFld.text = [pinDict objectForKey:kKeyName];
    dropPinWindow.titleTxtFld.delegate = self;
    
    // Address
    dropPinWindow.addressLbl.text = [pinDict objectForKey:kKeyAddress];
    
    // Miles
    CLLocationCoordinate2D userCoordinate = {[[pinDict valueForKey:kKeyLattitude] floatValue], [[pinDict valueForKey:kKeyRadarLong] floatValue]};
    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:userCoordinate.latitude longitude:userCoordinate.longitude];
    CLLocation *myLocation = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
    CLLocationDistance distance = [myLocation distanceFromLocation:userLocation] * 0.000621371;
    dropPinWindow.lblMiles.text = [NSString stringWithFormat:@"%.1f miles away", distance];
    
    [dropPinWindow.editBtn addTarget:self action:@selector(actionOnEditButton:) forControlEvents:UIControlEventTouchUpInside];
    dropPinWindow.deleteBtn.hidden = YES;
    [dropPinWindow.trashBtn addTarget:self action:@selector(actionOnDeletePin:) forControlEvents:UIControlEventTouchUpInside];
    [dropPinWindow.openInMapBtn addTarget:self action:@selector(showPlaceDirectionInMapApp:) forControlEvents:UIControlEventTouchUpInside];
    //dropPinWindow.openInMapBtn.hidden = YES;
    [dropPinWindow.deleteBtn addTarget:self action:@selector(actionOnDeletePin:) forControlEvents:UIControlEventTouchUpInside];
    
    // Add subview
    [_mapView addSubview:dropPinWindow];
}
// --------------------------------------------------------------------------------
// actionOnEditButton:

- (void)actionOnEditButton:(UIButton *)inSender {
    SRDropPinWindowView *superView = (SRDropPinWindowView *) inSender.superview;
    if (!superView.isEdit) {
        superView.BGImage.backgroundColor = [UIColor whiteColor];
        superView.trashBtn.hidden = YES;
        superView.openInMapBtn.hidden = YES;
        superView.mapImage.hidden = YES;
        superView.openInMapLbl.hidden = YES;
        superView.deleteBtn.hidden = NO;
        superView.titleTxtFld.enabled = YES;
        superView.titleTxtFld.textColor = [UIColor whiteColor];
        superView.lblMiles.textColor = [UIColor whiteColor];
        superView.addressLbl.textColor = [UIColor whiteColor];
        superView.isEdit = YES;
        [superView.titleTxtFld becomeFirstResponder];
        superView.editImage.image = [UIImage imageNamed:@"edit_fill.png"];
    } else {
        superView.isEdit = NO;
        
        NSString *nameStr = [superView.titleTxtFld.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        // Call Create Pin API here
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[[server loggedInUserInfo] valueForKey:kKeyId] forKey:kKeyUserID];
        if ([nameStr length] == 0) {
            [params setObject:@"Untitled" forKey:kKeyName];
        } else {
            [params setObject:superView.titleTxtFld.text forKey:kKeyName];
        }
        [params setObject:[superView.infoDict objectForKey:kKeyAddress] forKey:kKeyAddress];
        [params setObject:[superView.infoDict objectForKey:kKeyLattitude] forKey:kKeyLattitude];
        [params setObject:[superView.infoDict objectForKey:kKeyRadarLong] forKey:kKeyRadarLong];
        [params setObject:[superView.infoDict objectForKey:kKeyElevation] forKey:kKeyElevation];
        [params setObject:[superView.infoDict objectForKey:kKeyDistance] forKey:kKeyDistance];
        [params setObject:kPUT forKey:@"_method"];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@/%@", kKeyClassCreatePins, [superView.infoDict objectForKey:kKeyId]];
        [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:NO inMethodType:kPUT];
    }
}

// --------------------------------------------------------------------------------
// actionOnBtnClick:

- (IBAction)actionOnBtnClick:(UIButton *)inSender {
    // Set map
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:center.latitude longitude:center.longitude zoom:18];
    [self.mapView animateToCameraPosition:camera];
    
    if (inSender.tag == 0) {
        self.mapView.mapType = kGMSTypeSatellite;
        [btnStreet setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    } else {
        self.mapView.mapType = kGMSTypeNormal;
        [btnStreet setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [btnSatelite setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    if (searchBar.text.length == 0)
        [searchBar resignFirstResponder];
}

// --------------------------------------------------------------------------------
// // Creates a marker in the center of the map
- (void)showMapProfileView:(CGFloat)Latitude andLongitude:(CGFloat)Longitude {
    // Set profile pic
    currentMarker = [[GMSMarker alloc] init];
    currentMarker.position = CLLocationCoordinate2DMake(Latitude, Longitude);
    if (!isDropaPin) {
        [self.mapView animateToLocation:currentMarker.position];
    }
    SRMapProfileView *myProfileView = [[SRMapProfileView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    UIImage *markerIcon = [self imageFromView:myProfileView];
    currentMarker.icon = markerIcon;
    currentMarker.map = self.mapView;
}

#pragma mark
#pragma mark Search Bar Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// textDidChange:

- (void)searchBar:(UISearchBar *)inSearchBar textDidChange:(NSString *)searchText {
    if (inSearchBar == searchBar) {
        [_tableDataSource sourceTextHasChanged:searchText];
    } else if (inSearchBar == searchBar1) {
        if (searchBar1.text.length > 0) {
            isMyPinSearch = YES;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"address BEGINSWITH[c] %@ OR (name BEGINSWITH[c] %@)", searchBar1.text, searchBar1.text];
            NSArray *filteredArr = [myPinsArr filteredArrayUsingPredicate:predicate];
            if (filteredArr) {
                myPinsSearchArr = (NSMutableArray *) filteredArr;
            }
            [tblView reloadData];
        } else {
            isMyPinSearch = NO;
            [tblView reloadData];
        }
    }
    
}

// --------------------------------------------------------------------------------
// searchBarShouldBeginEditing:

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)inSearchBar {
    if (inSearchBar == searchBar) {
        isMyPinSearch = NO;
    }
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidBeginEditing:

- (void)searchBarTextDidBeginEditing:(UISearchBar *)inSearchBar {
    if (inSearchBar == searchBar) {
        // Add overlay view
        [self addChildViewController:_resultsController];
        _resultsController.view.frame = CGRectMake(0, 94, SCREEN_WIDTH, 200);
        _resultsController.view.alpha = 0.0f;
        [self.view addSubview:_resultsController.view];
        [UIView animateWithDuration:0.5
                         animations:^{
            _resultsController.view.alpha = 1.0f;
        }];
        [_resultsController didMoveToParentViewController:self];
    }
}

// --------------------------------------------------------------------------------
// searchBarShouldEndEditing:

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)inSearchBar {
    return YES;
}

// --------------------------------------------------------------------------------
// searchBarTextDidEndEditing:

- (void)searchBarTextDidEndEditing:(UISearchBar *)srchBar {
    if (srchBar == searchBar) {
        // Search
        [_tableDataSource sourceTextHasChanged:nil];
        // Remove  overlay view from view
        [_resultsController willMoveToParentViewController:nil];
        [UIView animateWithDuration:0.5
                         animations:^{
            _resultsController.view.alpha = 0.0f;
        }
                         completion:^(BOOL finished) {
            [_resultsController.view removeFromSuperview];
            [_resultsController removeFromParentViewController];
        }];
    } else if (srchBar == searchBar1) {
        
        if (isMyPinSearch) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"address BEGINSWITH[c] %@ OR (name BEGINSWITH[c] %@)", srchBar.text, srchBar.text];
            NSArray *filteredArr = [myPinsArr filteredArrayUsingPredicate:predicate];
            if (filteredArr) {
                myPinsSearchArr = (NSMutableArray *) filteredArr;
                [tblView reloadData];
            }
        } else {
            [tblView reloadData];
        }
    }
}

// --------------------- -----------------------------------------------------------
// searchBarSearchButtonClicked:

- (void)searchBarSearchButtonClicked:(UISearchBar *)inSearchBar {
    if (inSearchBar == searchBar) {
        // Search
        [_tableDataSource sourceTextHasChanged:inSearchBar.text];
    } else if (inSearchBar == searchBar1) {
        if (searchBar1.text.length > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"address BEGINSWITH[c] %@ OR (name BEGINSWITH[c] %@)", inSearchBar.text, inSearchBar.text];
            NSArray *filteredArr = [myPinsArr filteredArrayUsingPredicate:predicate];
            if (filteredArr) {
                myPinsSearchArr = (NSMutableArray *) filteredArr;
                [tblView reloadData];
            }
        }
    }
}

// --------------------- -----------------------------------------------------------
// searchBarCancelButtonClicked:
-(void) btnCancelClick{
    [self searchBarCancelButtonClicked:searchBar1];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)inSearchBar {
    
    isMyPinSearch = NO;
    searchBar1.text = @"";
    
    [self.view endEditing:YES];
    if (inSearchBar == searchBar1) {
        if(searchBar1.text.length == 0 ) {
            tblView.hidden = YES;
            searchBar1.hidden = YES;
            cancelButton.hidden = YES;
            searchBar1.text = nil;
            
            CGRect frame;
            frame = btnSatelite.frame;
            frame.origin.y = self.view.frame.size.height - 85 - [self getBottomSafeArea];
            btnSatelite.frame = frame;
            
            btnStreet.hidden = NO;
            frame = btnStreet.frame;
            frame.origin.y = self.view.frame.size.height - 85 - [self getBottomSafeArea];
            
            btnStreet.frame = frame;
            isMyPinSearch = NO;
        }
    }
}

#pragma mark
#pragma mark TextField Delegate Method
#pragma mark
//
// --------------------------------------------------------------------------------
// textFieldShouldReturn:

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // Return
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    textField.text = @"";
    isMyPinSearch = NO;
    [tblView reloadData];
    return YES;
}

#pragma mark
#pragma mark GMSAutocompleteTableDataSource Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// didAutocompleteWithPlace:

- (void) tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    [searchBar resignFirstResponder];
    searchBar.text = place.name;
    
    // Call APi to create the pin and drop it
    [(APP_DELEGATE) showActivityIndicator];
    NSString *addressStr = searchBar.text;
    
    // Call Create Pin API here
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[[server loggedInUserInfo] valueForKey:kKeyId] forKey:kKeyUserID];
    [params setObject:@"Untitled" forKey:kKeyName];
    [params setObject:addressStr forKey:kKeyAddress];
    [params setObject:[NSString stringWithFormat:@"%f", place.coordinate.latitude] forKey:kKeyLattitude];
    [params setObject:[NSString stringWithFormat:@"%f", place.coordinate.longitude] forKey:kKeyRadarLong];
    
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/elevation/json?locations=%f,%f&key=%@", place.coordinate.latitude, place.coordinate.longitude, kKeyServerGoogleMap];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            if ([json[@"status"] isEqualToString:@"OK"]) {
                //parse data
                NSArray *rows = json[@"results"];
                NSDictionary *rowsDict = [rows objectAtIndex:0];
                NSString *strElevation = [rowsDict valueForKey:@"elevation"];
                [params setObject:strElevation forKey:kKeyElevation];
                CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
                CLLocation *myLocation = [[CLLocation alloc] initWithLatitude:center.latitude longitude:center.longitude];
                CLLocationDistance distance = [myLocation distanceFromLocation:userLocation];
                [params setObject:[NSString stringWithFormat:@"%f", distance] forKey:kKeyDistance];
                NSString *strUrl = [NSString stringWithFormat:@"%@", kKeyClassCreatePins];
                [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:NO inMethodType:kPOST];
                tappedPinElevation = [[params objectForKey:kKeyElevation] intValue];
                NSString *urlString = [NSString stringWithFormat:
                                       @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@&mode=walking",
                                       @"https://maps.googleapis.com/maps/api/directions/json",
                                       server.myLocation.coordinate.latitude,
                                       server.myLocation.coordinate.longitude,
                                       place.coordinate.latitude,
                                       place.coordinate.longitude,
                                       kKeyServerGoogleMap];
                NSURL *directionsURL = [NSURL URLWithString:urlString];
                
                
                NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:directionsURL];
                [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    if (!error) {
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                        GMSPath *path = [GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                        polyline = [GMSPolyline polylineWithPath:path];
                        polyline.strokeWidth = 7;
                        polyline.strokeColor = [UIColor greenColor];
                        polyline.map = self.mapView;
                    }
                }];
                pinLat = place.coordinate.latitude;
                pinLon = place.coordinate.longitude;
                [self getElevationDifference:tappedPinElevation latitude:pinLat longitude:pinLon];
                if (self.updateElevation != nil) {
                    [self.updateElevation invalidate];
                    self.updateElevation = nil;
                }
                self.updateElevation = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                                        target:self
                                                                      selector:@selector(updateElevationCall:)
                                                                      userInfo:nil
                                                                       repeats:YES];
            }
        }
    }];
}

// --------------------------------------------------------------------------------
// didFailAutocompleteWithError:

- (void)     tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [SRModalClass showAlert:@"Sorry, we are not able to properly map user locations at the moment. Please close and reopen the app."];
}

// --------------------------------------------------------------------------------
// didUpdateAutocompletePredictionsForTableDataSource:

- (void)didUpdateAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

// --------------------------------------------------------------------------------
// didRequestAutocompletePredictionsForTableDataSource:

- (void)didRequestAutocompletePredictionsForTableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource {
    [_resultsController.tableView reloadData];
}

#pragma mark
#pragma mark GMS MapView Delegates
#pragma mark

// -----------------------------------------------------------------------------------------------------------------
// didLongPressAtCoordinate:

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [(APP_DELEGATE) showActivityIndicator];
    
    CLGeocoder *ceo = [[CLGeocoder alloc] init];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude]; //insert your coordinates
    isDropaPin = true;
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSString *addressStr = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@","];
        
        // Call Create Pin API here
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[[server loggedInUserInfo] valueForKey:kKeyId] forKey:kKeyUserID];
        [params setObject:@"Untitled" forKey:kKeyName];
        if (addressStr && ![addressStr isKindOfClass:[NSNull class]]) {
            [params setObject:addressStr forKey:kKeyAddress];
        } else {
            [params setObject:@"N/A" forKey:kKeyAddress];
        }
        [params setObject:[NSString stringWithFormat:@"%f", coordinate.latitude] forKey:kKeyLattitude];
        [params setObject:[NSString stringWithFormat:@"%f", coordinate.longitude] forKey:kKeyRadarLong];
        NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/elevation/json?locations=%f,%f&key=%@", coordinate.latitude, coordinate.longitude, kKeyServerGoogleMap];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            
            if (!error) {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                if ([json[@"status"] isEqualToString:@"OK"]) {
                    //parse data
                    NSArray *rows = json[@"results"];
                    NSDictionary *rowsDict = [rows objectAtIndex:0];
                    NSString *strElevation = [rowsDict valueForKey:@"elevation"];
                    [params setObject:strElevation forKey:kKeyElevation];
                    
                }
                NSString *urlStrDist = [NSString stringWithFormat:@"%@%@=%f,%f&point=%f,%f&locale=en&debug=true", kOSMServer, kOSMroutingAPi, coordinate.latitude, coordinate.longitude, server.myLocation.coordinate.latitude, server.myLocation.coordinate.longitude];
         //       NSLog(@"%@", urlStrDist);
                NSMutableURLRequest *requestdist = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStrDist]];
                
                //                                                 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                //                                                 NSDictionary *dictOSM = [json valueForKey:@"paths"];
                //                                                 NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                NSString *distance = @"100";//[arrdistance objectAtIndex:0];
                double convertDist = [distance doubleValue];
                NSString *distanceUnit;
                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                    distanceUnit = kkeyUnitMiles;
                } else {
                    distanceUnit = kkeyUnitKilometers;
                }
                if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                    //meters to killometer
                    convertDist = convertDist / 1000;
                } else {
                    //meters to miles
                    convertDist = (convertDist * 0.000621371192);
                }
                [params setObject:[NSString stringWithFormat:@"%f", convertDist] forKey:kKeyDistance];
                NSString *strUrl = [NSString stringWithFormat:@"%@", kKeyClassCreatePins];
                
                [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:NO inMethodType:kPOST];
                tappedPinElevation = [[params objectForKey:kKeyElevation] intValue];
                pinLat = coordinate.latitude;
                pinLon = coordinate.longitude;
                [self getElevationDifference:tappedPinElevation latitude:pinLat longitude:pinLon];
                NSString *urlString = [NSString stringWithFormat:
                                       @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@&mode=walking",
                                       @"https://maps.googleapis.com/maps/api/directions/json",
                                       
                                       coordinate.latitude,
                                       coordinate.longitude,
                                       server.myLocation.coordinate.latitude,
                                       server.myLocation.coordinate.longitude,
                                       kKeyServerGoogleMap];
                NSURL *directionsURL = [NSURL URLWithString:urlString];
                
                
                NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:directionsURL];
                [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    
                    if (!error) {
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                        GMSPath *path = [GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                        polyline = [GMSPolyline polylineWithPath:path];
                        polyline.strokeWidth = 7;
                        polyline.strokeColor = [UIColor greenColor];
                        polyline.map = self.mapView;
                    }
                    
                }];
                if (self.updateElevation != nil) {
                    [self.updateElevation invalidate];
                    self.updateElevation = nil;
                }
                
                self.updateElevation = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                                        target:self
                                                                      selector:@selector(updateElevationCall:)
                                                                      userInfo:nil
                                                                       repeats:YES];
                
                //                                                 GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:loc.coordinate.latitude longitude:loc.coordinate.longitude zoom:18];
                //                                                 [self.mapView animateToCameraPosition:camera];
                
                //                                                 [NSURLConnection sendAsynchronousRequest:requestdist queue:[NSOperationQueue mainQueue]
                //                                                                        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                //                                                                            if (!error) {
                //                                                                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                //                                                                                NSDictionary *dictOSM = [json valueForKey:@"paths"];
                //                                                                                NSArray *arrdistance = [dictOSM valueForKey:@"distance"];
                //                                                                                NSString *distance = [arrdistance objectAtIndex:0];
                //                                                                                double convertDist = [distance doubleValue];
                //                                                                                NSString *distanceUnit;
                //                                                                                if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyMeasurementUnit] boolValue]) {
                //                                                                                    distanceUnit = kkeyUnitMiles;
                //                                                                                } else {
                //                                                                                    distanceUnit = kkeyUnitKilometers;
                //                                                                                }
                //                                                                                if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                //                                                                                    //meters to killometer
                //                                                                                    convertDist = convertDist / 1000;
                //                                                                                } else {
                //                                                                                    //meters to miles
                //                                                                                    convertDist = (convertDist * 0.000621371192);
                //                                                                                }
                //                                                                                [params setObject:[NSString stringWithFormat:@"%f", convertDist] forKey:kKeyDistance];
                //                                                                                NSString *strUrl = [NSString stringWithFormat:@"%@", kKeyClassCreatePins];
                //
                //                                                                                [server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:NO inMethodType:kPOST];
                //                                                                                tappedPinElevation = [[params objectForKey:kKeyElevation] intValue];
                //                                                                                pinLat = coordinate.latitude;
                //                                                                                pinLon = coordinate.longitude;
                //                                                                                [self getElevationDifference:tappedPinElevation latitude:pinLat longitude:pinLon];
                //                                                                                NSString *urlString = [NSString stringWithFormat:
                //                                                                                        @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@&mode=walking",
                //                                                                                        @"https://maps.googleapis.com/maps/api/directions/json",
                //                                                                                        server.myLocation.coordinate.latitude,
                //                                                                                        server.myLocation.coordinate.longitude,
                //                                                                                        coordinate.latitude,
                //                                                                                        coordinate.longitude,
                //                                                                                                kKeyServerGoogleMap];
                //                                                                                NSURL *directionsURL = [NSURL URLWithString:urlString];
                //
                //
                //                                                                                NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:directionsURL];
                //                                                                                [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue]
                //                                                                                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                //                                                                                                           if (!error) {
                //                                                                                                               NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                //                                                                                                               GMSPath *path = [GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                //                                                                                                               polyline = [GMSPolyline polylineWithPath:path];
                //                                                                                                               polyline.strokeWidth = 7;
                //                                                                                                               polyline.strokeColor = [UIColor greenColor];
                //                                                                                                               polyline.map = self.mapView;
                //                                                                                                           }
                //                                                                                                       }];
                //                                                                                if (self.updateElevation != nil) {
                //                                                                                    [self.updateElevation invalidate];
                //                                                                                    self.updateElevation = nil;
                //                                                                                }
                //                                                                                self.updateElevation = [NSTimer scheduledTimerWithTimeInterval:5.0
                //                                                                                                                                        target:self
                //                                                                                                                                      selector:@selector(updateElevationCall:)
                //                                                                                                                                      userInfo:nil
                //                                                                                                                                       repeats:YES];
                //                                                                            } else {
                //                                                                                [(APP_DELEGATE) hideActivityIndicator];
                //                                                                            }
                //                                                                        }];
            }
        }];
    }];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:loc.coordinate.latitude longitude:loc.coordinate.longitude zoom:18];
    [self.mapView animateToCameraPosition:camera];
}

- (void) getElevationDifference:(int)pinElevation latitude:(CGFloat)Lat longitude:(CGFloat)lon {
    
    if (oldLocation.latitude != server.myLocation.coordinate.latitude && oldLocation.longitude != server.myLocation.coordinate.longitude) {
        currentMarker.map = nil;
        [self showMapProfileView:(APP_DELEGATE).server.myLocation.coordinate.latitude andLongitude:(APP_DELEGATE).server.myLocation.coordinate.longitude];
        oldLocation = server.myLocation.coordinate;
        polyline.map = nil;
        polyline = nil;
        CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:pinLat longitude:pinLon];
        CLLocation *myLocation = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
        CLLocationDistance distance = [myLocation distanceFromLocation:userLocation] * 0.000621371;
        dropPinWindow.lblMiles.text = [NSString stringWithFormat:@"%.1f miles away", distance];
        NSString *urlString = [NSString stringWithFormat:
                               @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@&mode=walking",
                               @"https://maps.googleapis.com/maps/api/directions/json",
                               server.myLocation.coordinate.latitude,
                               server.myLocation.coordinate.longitude,
                               pinLat,
                               pinLon, kKeyServerGoogleMap];
        NSURL *directionsURL = [NSURL URLWithString:urlString];
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:directionsURL];
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            if (!error) {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                GMSPath *path = [GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth = 7;
                polyline.strokeColor = [UIColor greenColor];
                polyline.map = self.mapView;
            }
        }];
    }
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/elevation/json?locations=%f,%f&key=%@", server.myLocation.coordinate.latitude, server.myLocation.coordinate.longitude, kKeyServerGoogleMap];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            if ([json[@"status"] isEqualToString:@"OK"]) {
                //parse data
                NSArray *rows = json[@"results"];
                NSDictionary *rowsDict = [rows objectAtIndex:0];
                int myElevation = [[rowsDict valueForKey:@"elevation"] intValue];
                //int elevationDiff=myElevation-pinElevation;
                NSString *strElevationDiff;
                if ([distanceUnit isEqualToString:kkeyUnitKilometers]) {
                    strElevationDiff = [NSString stringWithFormat:@"%d m current elevation & %d m pin elevation", myElevation, pinElevation];
                } else {
                    myElevation = myElevation * 3.28084;
                    int pinElev = pinElevation;
                    pinElev = pinElev * 3.28084;
                    strElevationDiff = [NSString stringWithFormat:@"%d ft current elevation & %d ft pin elevation", myElevation, pinElev];
                }
                lblElevation.text = [NSString stringWithFormat:@"%@", strElevationDiff];
                lblElevation.hidden = NO;
            }
        }
    }];
}

// --------------------------------------------------------------------------------
// didTapMarker:

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    searchBar.text = nil;
    [searchBar resignFirstResponder];
    // Check for marker in the list if same marker got tapped then remove the select object
    if (marker.userData != nil) {
        for (GMSMarker *savedMarker in allMarkers) {
            NSMutableDictionary *infoDict = [NSMutableDictionary dictionaryWithDictionary:savedMarker.userData];
            if ([[marker.userData objectForKey:kKeyId] isEqualToString:[infoDict objectForKey:kKeyId]]) {
                [dropPinWindow removeFromSuperview];
                polyline.map = nil;

                if ([infoDict objectForKey:@"selected"] != nil) {
                    [infoDict removeObjectForKey:@"selected"];
                    //selectedMarker = nil;
                    savedMarker.icon = [UIImage imageNamed:@"map-pin.png"];
                } else {
                    [infoDict setObject:@"select" forKey:@"selected"];
                    selectedMarker = marker;

                    marker.icon = [UIImage imageNamed:@"radar-pin-white-20px.png"];

                    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:marker.position];
                    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
                    CGRect frame = CGRectMake(pointInViewCoords.x - 95, pointInViewCoords.y - 300, 190, 203);

                    if (dropPinWindow == nil) {
                        dropPinWindow = [[SRDropPinWindowView alloc] initWithFrame:frame];
                    } else
                        dropPinWindow.frame = frame;

                    dropPinWindow.infoDict = marker.userData;

                    NSString *imageUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/streetview?size=190x92&location=%f,%f&heading=151.78&pitch=-0.76&key=%@", marker.position.latitude, marker.position.longitude, kKeyGoogleMap];
                    NSURL *imageUrl = [NSURL URLWithString:imageUrlStr];
                    [dropPinWindow.BGImage sd_setImageWithURL:imageUrl];

                    // Add tap gesture
                    UITapGestureRecognizer *tapBGimage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ShowPanoramaView)];
                    tapBGimage.delegate = self;
                    [dropPinWindow.BGImage addGestureRecognizer:tapBGimage];

                    dropPinWindow.BGImage.backgroundColor = [UIColor blackColor];
                    dropPinWindow.trashBtn.hidden = NO;
                    dropPinWindow.openInMapBtn.hidden = NO;
                    dropPinWindow.mapImage.hidden = NO;
                    dropPinWindow.openInMapLbl.hidden = NO;
                    dropPinWindow.titleTxtFld.enabled = NO;
                    dropPinWindow.titleTxtFld.textColor = [UIColor whiteColor];
                    dropPinWindow.lblMiles.textColor = [UIColor whiteColor];
                    dropPinWindow.addressLbl.textColor = [UIColor whiteColor];
                    dropPinWindow.editImage.image = [UIImage imageNamed:@"pencil.png"];
                    dropPinWindow.isEdit = NO;

                    // Name
                    dropPinWindow.titleTxtFld.text = [marker.userData objectForKey:kKeyName];
                    dropPinWindow.titleTxtFld.delegate = self;

                    // Address
                    dropPinWindow.addressLbl.text = [marker.userData objectForKey:kKeyAddress];

                    // Miles
                    CLLocationCoordinate2D userCoordinate = {[[marker.userData valueForKey:kKeyLattitude] floatValue], [[marker.userData valueForKey:kKeyRadarLong] floatValue]};
                    CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:userCoordinate.latitude longitude:userCoordinate.longitude];
                    CLLocation *myLocation = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
                    CLLocationDistance distance = [myLocation distanceFromLocation:userLocation] * 0.000621371;
                    dropPinWindow.lblMiles.text = [NSString stringWithFormat:@"%.1f miles away", distance];
                    tappedPinElevation = [[marker.userData valueForKey:kKeyElevation] intValue];
                    pinLat = marker.position.latitude;
                    pinLon = marker.position.longitude;
                    [self getElevationDifference:tappedPinElevation latitude:pinLat longitude:pinLon];
                    if (self.updateElevation != nil) {
                        [self.updateElevation invalidate];
                        self.updateElevation = nil;
                    }
                    self.updateElevation = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                                            target:self
                                                                          selector:@selector(updateElevationCall:)
                                                                          userInfo:nil
                                                                           repeats:YES];
                    //_lblElevation.hidden=NO;

                    [dropPinWindow.editBtn addTarget:self action:@selector(actionOnEditButton:) forControlEvents:UIControlEventTouchUpInside];
                    dropPinWindow.deleteBtn.hidden = YES;
                    [dropPinWindow.trashBtn addTarget:self action:@selector(actionOnDeletePin:) forControlEvents:UIControlEventTouchUpInside];
                    [dropPinWindow.openInMapBtn addTarget:self action:@selector(showPlaceDirectionInMapApp:) forControlEvents:UIControlEventTouchUpInside];
                    [dropPinWindow.deleteBtn addTarget:self action:@selector(actionOnDeletePin:) forControlEvents:UIControlEventTouchUpInside];
                    /*
                     NSString *urlString = [NSString stringWithFormat:
                     @"%@?origin=%f,%f&destination=%f,%f&sensor=true&key=%@&mode=walking",
                     @"https://maps.googleapis.com/maps/api/directions/json",
                     server.myLocation.coordinate.latitude,
                     server.myLocation.coordinate.longitude,
                     marker.position.latitude,
                     marker.position.longitude, kKeyServerGoogleMap];
                     NSURL *directionsURL = [NSURL URLWithString:urlString];


                     NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:directionsURL];
                     [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue]
                     completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                     if (!error) {
                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                     GMSPath *path = [GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                     polyline = [GMSPolyline polylineWithPath:path];
                     polyline.strokeWidth = 7;
                     polyline.strokeColor = [UIColor greenColor];
                     polyline.map = self.mapView;
                     }
                     }];
                     */

                    // Add subview
                    [_mapView addSubview:dropPinWindow];
                    //show map select option:
                    [self placeLocationInMapApp:dropPinWindow.openInMapBtn];
                }
            } else {
                if ([infoDict objectForKey:@"selected"] != nil) {
                    [infoDict removeObjectForKey:@"selected"];
                    savedMarker.icon = [UIImage imageNamed:@"map-pin.png"];
                }

            }
            savedMarker.userData = infoDict;
        }
    }
    // Return
    return YES;
}

- (void)updateElevationCall:(NSTimer *)timer {
    [self getElevationDifference:tappedPinElevation latitude:pinLat longitude:pinLon];
    isDropaPin = false ;
}
// --------------------------------------------------------------------------------
// didChangeCameraPosition:

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    CGRect frame = CGRectMake(pointInViewCoords.x - 95, pointInViewCoords.y - 300, 190, 203);
    
    if (dropPinWindow == nil) {
        dropPinWindow = [[SRDropPinWindowView alloc] initWithFrame:frame];
    } else
        dropPinWindow.frame = frame;
}

// --------------------------------------------------------------------------------
// idleAtCameraPosition:

// This method gets called whenever the map was moving but has now stopped

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    CGPoint markerPoint = [self.mapView.projection pointForCoordinate:selectedMarker.position];
    CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
    CGRect frame = CGRectMake(pointInViewCoords.x - 95, pointInViewCoords.y - 300, 190, 203);
    
    if (dropPinWindow == nil) {
        dropPinWindow = [[SRDropPinWindowView alloc] initWithFrame:frame];
    } else
        dropPinWindow.frame = frame;
}

// --------------------------------------------------------------------------------
// didTapAtCoordinate:

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    if (selectedMarker) {
        [dropPinWindow removeFromSuperview];
        //selectedMarker = nil;
        for (GMSMarker *savedMarker in allMarkers) {
            if ([savedMarker.userData objectForKey:@"selected"] != nil) {
                [savedMarker.userData removeObjectForKey:@"selected"];
            }
        }
        lblElevation.hidden = YES;
        
        polyline.map = nil;
        [self.updateElevation invalidate];
        self.updateElevation = nil;
    } else {
        if (searchBar.text.length == 0) {
            [searchBar resignFirstResponder];
        }
    }
}

#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (myPinsArr.count == 0) {
        UILabel *defaultLbl = [[UILabel alloc] initWithFrame:tableView.frame];
        defaultLbl.textAlignment = NSTextAlignmentCenter;
        defaultLbl.textColor = [UIColor orangeColor];
        defaultLbl.text = NSLocalizedString(@"lbl.nodata.txt", @"");
        tableView.backgroundView = defaultLbl;
        return 0;
    } else if (isMyPinSearch) {
        return myPinsSearchArr.count;
    } else
        tableView.backgroundView = nil;
    return myPinsArr.count;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// --------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"DROPPINCELL";
    NSDictionary *pinDict;
    if (isMyPinSearch) {
        pinDict = [myPinsSearchArr objectAtIndex:indexPath.section];
    } else
        pinDict = [myPinsArr objectAtIndex:indexPath.section];
    
    SwipeableTableViewCell *cell;
    if (!cell) {
        cell = [[SwipeableTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.backgroundColor = [UIColor clearColor];
        
        // Tap Gesture
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
        singleTap.delegate = self;
        cell.tag = indexPath.section;
        [cell addGestureRecognizer:singleTap];
        singleTap.cancelsTouchesInView = NO;
        
        UIButton *blockButton = [cell createButtonWithWidth:40 onSide:SwipeableTableViewCellSideRight];
        blockButton.backgroundColor = [UIColor redColor];
        [blockButton setImage:[UIImage imageNamed:@"trash-white.png"] forState:UIControlStateNormal];
        blockButton.tag = indexPath.section;
        [blockButton addTarget:self action:@selector(deletePinFromList:) forControlEvents:UIControlEventTouchUpInside];
        
        // Assign values
        SRPinDetailView *contentView = (SRPinDetailView *) cell.scrollViewContentView;
        contentView.lblName.text = [pinDict objectForKey:kKeyName];
        contentView.lblAddress.text = [pinDict objectForKey:kKeyAddress];
    }
    
    // Return
    return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat headerHeight = 1.0;
    if (section == 0) {
        headerHeight = 0.0;
    }
    
    // Return
    return headerHeight;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = nil;
    if (section > 0) {
        headerView = [[UIView alloc] init];
        headerView.backgroundColor = [UIColor clearColor];
    }
    
    // Return
    return headerView;
}

// --------------------------------------------------------------------------------
// willDisplayCell:

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove insets and margins from cells.
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

// ----------------------------------------------------------------------------------------------------------------------
// didSelectRowAtIndexPath:

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark
#pragma mark TabBar Delegate method
#pragma mark

// --------------------------------------------------------------------------------
// didSelectItem:

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    if (item == self.currentPinTab) {
        selectedTab = 0;
    } else if (item == self.myLocationTab) {
        [self setMyLocationInView:center];
        selectedTab = 1;
    } else if (item == self.myPinsTab) {
        selectedTab = 2;
    }
    // update the markers
    [self updateMarkersForSelectedItem];
}

#pragma mark
#pragma mark Notification method
#pragma mark

// --------------------------------------------------------------------------------
// createUpdatePinSucceed:

- (void)createUpdatePinSucceed:(NSNotification *)inNotify {
    // Add pin to dropped list and update the my pins array
    NSDictionary *object = [inNotify object];
    [(APP_DELEGATE) hideActivityIndicator];
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *postInfo = [inNotify userInfo];
        NSDictionary *requestedParam = [postInfo objectForKey:kKeyRequestedParams];
        
        if ([requestedParam objectForKey:@"_method"] != nil) {
            // Edited the marker
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [object objectForKey:kKeyId]];
            NSArray *filterArr = [myPinsArr filteredArrayUsingPredicate:predicate];
            
            // Add to my pins
            if ([filterArr count] > 0) {
                NSInteger index = [myPinsArr indexOfObject:[filterArr objectAtIndex:0]];
                [myPinsArr replaceObjectAtIndex:index withObject:object];
                [allMyPinArray replaceObjectAtIndex:index withObject:object];
            }
            filterArr = [droppedPinArr filteredArrayUsingPredicate:predicate];
            if ([filterArr count] > 0) {
                NSInteger index = [droppedPinArr indexOfObject:[filterArr objectAtIndex:0]];
                [droppedPinArr replaceObjectAtIndex:index withObject:object];
            }
            // Update the markers in map
            [dropPinWindow removeFromSuperview];
            [self updateMarkersForSelectedItem];
        } else {
            // Deleted the marker from the list
            NSString *inObjectUrl = [postInfo objectForKey:kKeyObjectUrl];
            NSArray *componentArr = [inObjectUrl componentsSeparatedByString:@"/"];
            
            
            if ([componentArr count] > 1) {
                NSString *strId = [componentArr objectAtIndex:1];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, strId];
                NSArray *filterArr = [myPinsArr filteredArrayUsingPredicate:predicate];
                
                // Add to my pins
                if ([filterArr count] > 0) {
                    [myPinsArr removeObject:[filterArr objectAtIndex:0]];
                    [allMyPinArray removeObject:[filterArr objectAtIndex:0]];
                }
                filterArr = [droppedPinArr filteredArrayUsingPredicate:predicate];
                if ([filterArr count] > 0) {
                    [droppedPinArr removeObject:[filterArr objectAtIndex:0]];
                }
                [SRModalClass showAlertWithTitle:@"Pin deleted successfully" alertTitle:@"Success"];
                // Update markers
                [self updateMarkersForSelectedItem];
            } else {
                // Marker got created
                CLLocationCoordinate2D coordinate = {[[requestedParam valueForKey:kKeyLattitude] floatValue], [[requestedParam valueForKey:kKeyRadarLong] floatValue]};
                
                CGPoint markerPoint = [self.mapView.projection pointForCoordinate:coordinate];
                CGPoint pointInViewCoords = [self.view convertPoint:markerPoint fromView:self.mapView];
                CGRect frame = CGRectMake(pointInViewCoords.x - 95, pointInViewCoords.y - 300, 190, 203);
                if (dropPinWindow == nil) {
                    dropPinWindow = [[SRDropPinWindowView alloc] initWithFrame:frame];
                } else
                    dropPinWindow.frame = frame;
                
                NSString *imageUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/streetview?size=190x92&location=%@,%@&heading=151.78&pitch=-0.76&key=%@", requestedParam[@"lat"], requestedParam[@"lon"], kKeyGoogleMap];
                NSURL *imageUrl = [NSURL URLWithString:imageUrlStr];
                [dropPinWindow.BGImage sd_setImageWithURL:imageUrl];
                [dropPinWindow.BGImage setUserInteractionEnabled:YES];
                UITapGestureRecognizer *tapBGimage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ShowPanoramaView)];
                tapBGimage.delegate = self;
                [dropPinWindow.BGImage addGestureRecognizer:tapBGimage];
                
                CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
                CLLocation *myLocation = [[CLLocation alloc] initWithLatitude:server.myLocation.coordinate.latitude longitude:server.myLocation.coordinate.longitude];
                CLLocationDistance distance = [myLocation distanceFromLocation:userLocation] * 0.000621371;
                dropPinWindow.lblMiles.text = [NSString stringWithFormat:@"%.1f miles away", distance];
                dropPinWindow.BGImage.backgroundColor = [UIColor whiteColor];
                dropPinWindow.trashBtn.hidden = YES;
                dropPinWindow.openInMapBtn.hidden = YES;
                dropPinWindow.mapImage.hidden = YES;
                dropPinWindow.openInMapLbl.hidden = YES;
                dropPinWindow.deleteBtn.hidden = NO;
                dropPinWindow.titleTxtFld.enabled = YES;
                [dropPinWindow.titleTxtFld setText:@"Untitled"];
                dropPinWindow.titleTxtFld.delegate = self;
                dropPinWindow.titleTxtFld.textColor = [UIColor whiteColor];
                dropPinWindow.lblMiles.textColor = [UIColor whiteColor];
                dropPinWindow.addressLbl.textColor = [UIColor whiteColor];
                [dropPinWindow.addressLbl setText:[requestedParam objectForKey:kKeyAddress]];
                dropPinWindow.isEdit = NO;
                dropPinWindow.editImage.image = [UIImage imageNamed:@"edit_fill.png"];
                [dropPinWindow.editBtn addTarget:self action:@selector(actionOnEditButton:) forControlEvents:UIControlEventTouchUpInside];
                [dropPinWindow.deleteBtn addTarget:self action:@selector(actionOnDeletePin:) forControlEvents:UIControlEventTouchUpInside];
                [_mapView addSubview:dropPinWindow];
                
                // Add to my pins
                NSDictionary *dictUser = [object objectForKey:kKeyUser];
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[object objectForKey:kKeyUser]];
                //[[NSMutableDictionary alloc]init];
                // [dict setObject:inStr forKey:kKeyId];
                NSInteger pinId = [[dict objectForKey:kKeyId] integerValue];
                NSString *inStr = [NSString stringWithFormat:@"%ld", (long) pinId];
                [dict setObject:inStr forKey:kKeyId];
                
                dropPinWindow.infoDict = dict;
                GMSMarker *dropMarker = [[GMSMarker alloc] init];
                dropMarker.position = coordinate;
                dropMarker.title = [requestedParam objectForKey:kKeyName];
                dropMarker.snippet = [requestedParam objectForKey:kKeyAddress];
                dropMarker.userData = dict;
                dropMarker.icon = [UIImage imageNamed:@"map-pin.png"];
                dropMarker.map = self.mapView;
                selectedMarker = dropMarker;
                
                [myPinsArr addObject:dict];
                //[allMyPinArray addObject:dict];
                allMyPinArray = myPinsArr;
                [tblView reloadData];
                
                // Also create current pins objects
                [droppedPinArr addObject:dict];
                
                // Add to markers list
                [allMarkers addObject:dropMarker];
                
                
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:18];
                [self.mapView animateToCameraPosition:camera];
                
                // Update the markers in map
                //                [dropPinWindow removeFromSuperview];
                [self updateMarkersForSelectedItem];
            }
        }
    } else if ([object isKindOfClass:[NSArray class]]) {
        // Remove all objects and update new
        [myPinsArr removeAllObjects];
        [myPinsArr addObjectsFromArray:[inNotify object]];
        [allMyPinArray removeAllObjects];
        [allMyPinArray addObjectsFromArray:[inNotify object]];
        
        // Update the marker
        [self updateMarkersForSelectedItem];
    }
}

// --------------------------------------------------------------------------------
// createUpdatePinFailed:

- (void)createUpdatePinFailed:(NSNotification *)inNotify {
    [(APP_DELEGATE) hideActivityIndicator];
    NSDictionary *object = [inNotify object];
    if ([object isKindOfClass:[NSDictionary classForCoder]]) {
        [SRModalClass showAlert:[object valueForKey:@"response"]];
    } else if ([object isKindOfClass:[NSString classForCoder]])  {
        [SRModalClass showAlert:(NSString*)object];
    }
    
}

#pragma mark- keyborad show/hide Notification Action

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *value = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration = 0;
    [value getValue:&duration];
    
    [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        CGRect frame;
        frame = self.view.frame;
        frame.origin.y = [self getTopSafeArea]+44;
        self.view.frame = frame;
        [searchBar1 resignFirstResponder];
        
    }                completion:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    if (isMyPinSearch) {
        NSDictionary *userInfo = [notification userInfo];
        
        // get the size of the keyboard
        CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        if (keyboardSize.height == 0)
            return;
        
        NSValue *value = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        NSTimeInterval duration = 0;
        [value getValue:&duration];
        
        
        [UIView animateWithDuration:duration delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            CGRect frame;
            frame = self.view.frame;
            frame.origin.y = self.view.frame.origin.y - 219;
            self.view.frame = frame;
            
        }                completion:nil];
        
    }
    
}

@end
