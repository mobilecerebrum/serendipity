#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CLGeocoder.h>
#import "SRDropPinWindowView.h"
#import "SRServerConnection.h"

@interface SRDropPinViewController : ParentViewController <UISearchBarDelegate, GMSMapViewDelegate, GMSAutocompleteTableDataSourceDelegate, UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate, UIActionSheetDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    // Instance Variables
    // Modals
    NSMutableArray *droppedPinArr;
    NSMutableArray *myPinsArr,*myPinsSearchArr;
    NSMutableArray *allMarkers;
    NSDictionary *selecetdMarkerDict;

    NSInteger selectedTab;
    CLLocationCoordinate2D center;
    
    // Views
    UILabel *lblNavigateInfo;
    UISearchBar *searchBar;
    IBOutlet UIButton *btnSatelite,*btnStreet;
    IBOutlet UILabel *lblElevation;
    UITableView *tblView;
    UISearchBar *searchBar1;
    UIButton* cancelButton;
    BOOL isMyPinSearch,isCancelClicked;
    
    // Controllers
    UITableViewController *_resultsController;
    GMSAutocompleteTableDataSource *_tableDataSource;
    
    SRDropPinWindowView *dropPinWindow ;
    GMSMarker *selectedMarker;
    GMSPolyline *polyline;
    NSMutableArray *allMyPinArray;
    
    // save pin tag
    NSInteger pinTag;
    int tappedPinElevation;
    UIView *pinSuperView;
    
    // Server
    SRServerConnection *server;
    BOOL isOnce;
    BOOL isPanoramaLoaded;
    IBOutlet UIButton *streetViewBtn;
    CLLocationCoordinate2D panoramaLastLoc,oldLocation;
    CGFloat pinLat, pinLon;
    UIView *bacTableView;
    GMSMarker *currentMarker;
    NSString *distanceUnit;
    BOOL isDropaPin;

}

// Property
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *pegmanImg;
@property (nonatomic, weak) IBOutlet UITabBar *tabBar;
@property (nonatomic, weak) IBOutlet UITabBarItem *currentPinTab;
@property (nonatomic, weak) IBOutlet UITabBarItem *myLocationTab;
@property (nonatomic, weak) IBOutlet UITabBarItem *myPinsTab;
@property (strong, nonatomic) NSTimer *updateElevation;

//@property(nonatomic,weak)IBOutlet UILabel *lblElevation;
#pragma mark
#pragma mark Init Method
#pragma mark

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
               server:(SRServerConnection *)inServer;


@end
