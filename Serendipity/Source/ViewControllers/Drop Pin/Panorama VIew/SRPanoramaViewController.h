//
//  SRPanoramaViewController.h
//  Serendipity
//
//  Created by Leo on 18/07/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface SRPanoramaViewController : ParentViewController <UIAlertViewDelegate>
//Property
@property (nonatomic)CGFloat requestedLat,requestedLong;
@property (weak) IBOutlet GMSPanoramaView *panoramaView;


@end
