//
//  SRPanoramaViewController.m
//  Serendipity
//
//  Created by Leo on 18/07/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRPanoramaViewController.h"
#import "SRModalClass.h"

@interface SRPanoramaViewController ()

@end

@implementation SRPanoramaViewController
//
//--------------------------------------------------------------------
// viewDidLoad
- (void)viewDidLoad {
    [super viewDidLoad];
//    (APP_DELEGATE).topBarView.hidden = YES;
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = NO;
    
    // Set title
    [SRModalClass setNavTitle:NSLocalizedString(@"Street View", @"") forViewNavCon:self.navigationItem forFontSize:kNavBarFontSize];
    
    // Set button
    UIButton *backBtn = [SRModalClass setLeftNavBarItem:nil barImage:[UIImage imageNamed:@"menubar-back.png"] forViewNavCon:self offset:23];
    [backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];

    // Show panorama view if available
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        GMSPanoramaService *googleStreetViewService = [[GMSPanoramaService alloc] init];
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.requestedLat,self.requestedLong);
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [googleStreetViewService requestPanoramaNearCoordinate:coordinate  radius:50.0  callback:^(GMSPanorama *panorama, NSError *error)
             {
                 if (!error)
                 {
                    self.panoramaView.camera = [GMSPanoramaCamera cameraWithHeading:180 pitch:-10 zoom:1];
                    // [self.panoramaView moveNearCoordinate:coordinate radius:100];
                    [self.panoramaView moveNearCoordinate: coordinate];
                 }
                 else
                 {
                     UIAlertView *panoAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"alert.title.Street_View.txt", @"") message:NSLocalizedString(@"alert.show_panorama_view.txt", @"")delegate:self cancelButtonTitle:NSLocalizedString(@"alert.button.ok.text",@"") otherButtonTitles:nil, nil];
                     panoAlert.tag = 1;
                     [panoAlert show];
                 }
             }];
        });
    });
}
# pragma
# pragma Action method
# pragma
//
//--------------------------------------------------------------------
// backAction
-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
# pragma
# pragma AlertViewDelegate
# pragma
//
//----------------------------------------------------------------
//alertview:clickedButtonAtIndex

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
