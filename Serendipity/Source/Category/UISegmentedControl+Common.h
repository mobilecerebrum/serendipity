//
//  UISegmentedControl+Common.h
//  Serendipity
//
//  Created by Hitesh Surani on 08/04/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UISegmentedControl (Common)
- (void)ensureiOS12Style;
@end
