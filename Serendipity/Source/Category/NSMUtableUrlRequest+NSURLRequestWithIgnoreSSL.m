//
//  NSMUtableUrlRequest+NSURLRequestWithIgnoreSSL.m
//  Serendipity
//
//  Created by Mcuser on 1/4/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "NSMUtableUrlRequest+NSURLRequestWithIgnoreSSL.h"

@implementation NSMutableURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}

@end
