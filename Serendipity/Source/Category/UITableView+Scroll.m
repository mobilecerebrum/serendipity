//
//  UITableView+Scroll.m
//  Serendipity
//
//  Created by Mcuser on 3/8/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "UITableView+Scroll.h"

@implementation UITableView (Scroll)

// Returns true if the table is currently scrolled to the bottom
- (bool) scrolledToBottom {
    return self.contentOffset.y >= (self.contentSize.height - self.bounds.size.height);
}


// Scrolls the UITableView to the bottom of the last row
- (void)scrollToBottom:(BOOL)animated {
//    NSInteger lastSection = [self.dataSource numberOfSectionsInTableView:self] - 1;
//    NSInteger rowIndex = [self.dataSource tableView:self numberOfRowsInSection:lastSection] - 1;
//
//    if(rowIndex >= 0) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowIndex inSection:lastSection];
//        [self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:animated];
//    }
    
    NSInteger numberOfRows = [self numberOfRowsInSection:0];
    if (numberOfRows) {
        [self scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}

@end
