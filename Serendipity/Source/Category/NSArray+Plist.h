//
//  NSArray+Plist.h
//  Serendipity
//
//  Created by Mcuser on 1/18/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray(Plist)
-(BOOL)writeToPlistFile:(NSString*)filename;
+(NSArray*)readFromPlistFile:(NSString*)filename;
@end

NS_ASSUME_NONNULL_END
