//
//  NSMUtableUrlRequest+NSURLRequestWithIgnoreSSL.h
//  Serendipity
//
//  Created by Mcuser on 1/4/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableURLRequest (NSURLRequestWithIgnoreSSL)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host;
@end

NS_ASSUME_NONNULL_END
