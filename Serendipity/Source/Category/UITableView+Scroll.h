//
//  UITableView+Scroll.h
//  Serendipity
//
//  Created by Mcuser on 3/8/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (Scroll)

- (bool) scrolledToBottom;
- (void) scrollToBottom:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
