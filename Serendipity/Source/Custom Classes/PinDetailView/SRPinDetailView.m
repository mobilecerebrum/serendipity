#import "SRPinDetailView.h"

@implementation SRPinDetailView

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRPinDetailView" owner:self options:nil];
        self = nibArray[0];
        self.frame = frame;
    }
    
    // Return
    return self;
}

@end
