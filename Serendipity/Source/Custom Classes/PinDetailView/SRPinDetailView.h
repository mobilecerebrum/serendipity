#import <UIKit/UIKit.h>

@interface SRPinDetailView : UIView

// Properties
@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblAddress;

@end
