//
//  noInternetView.m
//  Serendipity
//
//  Created by Sunil Dhokare on 05/12/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRNoInternetView.h"

@implementation SRNoInternetView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

// -----------------------------------------------------------------------------------------------
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
// Call super
self = [super initWithFrame:frame];

if (self) {
    // Initialization code
    NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRNoInternetView" owner:self options:nil];
    self = [nibArray objectAtIndex:0];
    self.frame = frame;
    
    
}
    
    // Return
    return self;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Unregister the notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


@end
