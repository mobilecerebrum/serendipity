//
//  SwipeCellButton.h
//  Serendipity
//
//  Created by OKharchenko on 6/17/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface SwipeCellButton : UIView
@property (strong, nonatomic) IBOutlet MGSwipeButton *mgButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
