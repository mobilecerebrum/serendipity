#import <UIKit/UIKit.h>

@interface SRCustomImageView : UIImageView

@property (strong, nonatomic) NSDictionary *object;
@property (assign) BOOL isExtended;
@property (assign) BOOL isContainDegree;
@property (assign) long int repeatUserId;

- (void)setDictionaryValue:(NSDictionary *)inDict;

@end
