#import "SRCreateEditFamilyView.h"

@implementation SRCreateEditFamilyView

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRCreateEditFamilyView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
        self.frame = frame;
    }

    [self setRelationPicker];
    return self;
}

#pragma mark
#pragma mark - Set picker to textfield method
#pragma mark

- (void)setRelationPicker {
    //Relation array
    relationArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"lbl.relation_father.txt", @""), NSLocalizedString(@"lbl.relation_mother.txt", @""), NSLocalizedString(@"lbl.relation_son.txt", @""), NSLocalizedString(@"lbl.relation_daughter.txt", @""), NSLocalizedString(@"lbl.relation_bro.txt", @""), NSLocalizedString(@"lbl.relation_sis.txt", @""), NSLocalizedString(@"lbl.relation_grnadMaa.txt", @""), NSLocalizedString(@"lbl.relation_grandPaa.txt", @""), NSLocalizedString(@"lbl.relation_husband.txt", @""),
                                                     NSLocalizedString(@"lbl.relation_wife.txt", @""),
                                                     NSLocalizedString(@"lbl.relation_boyFrnd.txt", @""),
                                                     NSLocalizedString(@"lbl.relation_girlFrnd.txt", @""),
                                                     NSLocalizedString(@"lbl.relation_friend.txt", @""),
                                                     NSLocalizedString(@"lbl.relation_favFrnd.txt", @""),
                                                     NSLocalizedString(@"lbl.relation_Other.txt", @""), nil];

    // Make view round corner
    selectedRelation = _txtFldRltn.text;
    self.txtFldRltn.layer.cornerRadius = 5.0;
    relationPicker = [[UIPickerView alloc] init];
    relationPicker.dataSource = self;
    relationPicker.delegate = self;

    relationPicker.showsSelectionIndicator = YES;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
            initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                   target:self action:@selector(pickRelation:)];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:
            CGRectMake(0, SCREEN_HEIGHT -
                    relationPicker.frame.size.height - 40, SCREEN_WIDTH, 40)];
    toolBar.barTintColor = [UIColor orangeColor];
    toolBar.tintColor = [UIColor whiteColor];
    toolBar.translucent = NO;
    NSArray *toolbarItems = [NSArray arrayWithObjects:
            doneButton, nil];
    [toolBar setItems:toolbarItems];
    _txtFldRltn.inputView = relationPicker;
    _txtFldRltn.inputAccessoryView = toolBar;


    self.btnAddNewLocation.layer.cornerRadius = 5.0;

}

#pragma mark
#pragma mark - Picker button action method
#pragma mark

- (void)pickRelation:(id)sender {
    if ([selectedRelation isEqualToString:NSLocalizedString(@"lbl.relation_Other.txt", @"")]) {

        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Relation" message:@"Please mention relation" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        av.alertViewStyle = UIAlertViewStylePlainTextInput;
        [av textFieldAtIndex:0].delegate = self;
        [av show];
        [_txtFldRltn setText:selectedRelation];
        [_txtFldRltn resignFirstResponder];
    } else {
        if (![selectedRelation isEqualToString:@""]) {
            [_txtFldRltn setText:selectedRelation];
        } else {
            [_txtFldRltn setText:NSLocalizedString(@"lbl.relation_father.txt", @"")];
        }
        [_txtFldRltn resignFirstResponder];
    }
}

#pragma mark
#pragma mark - Picker View Data source
#pragma mark

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [relationArray count];
}


#pragma mark
#pragma mark- Picker View Delegate
#pragma mark

- (void)                pickerView:(UIPickerView *)pickerView didSelectRow:
        (NSInteger)row inComponent:(NSInteger)component {
    selectedRelation = [relationArray objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *lbl = (UILabel *) view;
    // Customise Font
    if (lbl == nil) {
        //label size
        CGRect frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, 30);

        lbl = [[UILabel alloc] initWithFrame:frame];
        [lbl setTextAlignment:NSTextAlignmentCenter];
        [lbl setBackgroundColor:[UIColor clearColor]];
        //here you can play with fonts
        [lbl setFont:[UIFont fontWithName:kFontHelveticaRegular size:15.0]];

    }
    //picker view array is the datasource
    [lbl setText:[relationArray objectAtIndex:row]];

    return lbl;
}

#pragma mark
#pragma mark- Alert View Delegate
#pragma mark

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        selectedRelation = [alertView textFieldAtIndex:0].text;
        [_txtFldRltn setText:selectedRelation];
        [_txtFldRltn resignFirstResponder];
    } else {
        _txtFldRltn.text = @"";
        [_txtFldRltn resignFirstResponder];
    }
}
@end
