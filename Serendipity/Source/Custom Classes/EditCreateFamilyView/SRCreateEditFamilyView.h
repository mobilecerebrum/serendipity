#import <UIKit/UIKit.h>

@interface SRCreateEditFamilyView : UIView <UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate,UITextFieldDelegate>
{
    //Picker
    UIPickerView *relationPicker;

    //Array
    NSArray *relationArray;
    
    // STring
    NSString *selectedRelation;
}
// Properties
@property (weak, nonatomic) IBOutlet UIImageView *imgFamilyBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextField *txtFldRltn;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UIButton *imgDirectionBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewLocation;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (assign, nonatomic) BOOL isEdit;
@property (strong, nonatomic) NSDictionary *userInfoDict;
@property (strong, nonatomic) NSDictionary *familyUserInfo;

@end
