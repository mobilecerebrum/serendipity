#import "MyAnnotation.h"

@implementation MyAnnotation

- (id)initWithTitle:(NSString *)inTitle location:(CLLocationCoordinate2D)inLocation
{
    self = [super init];
    if(self != nil)
    {
        _coordinate = inLocation;
        _title= inTitle;
    }
    
    // Return
    return (self);
}

- (MKAnnotationView *)annotationView
{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"MyCustomAnnotation"];
    annotationView.enabled = YES;
    
    // Return
    return annotationView;
}

@end
