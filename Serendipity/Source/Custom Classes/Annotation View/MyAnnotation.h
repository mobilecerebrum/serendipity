#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define kAnnotationProfileType @"profileType"
#define kAnnotationOldType @"oldType"
#define kAnnotationCurrentPin @"currentPin"

@interface MyAnnotation : NSObject<MKAnnotation>

// Properties
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSString *type;

- (id)initWithTitle:(NSString *)inTitle location:(CLLocationCoordinate2D)inLocation;
- (MKAnnotationView *)annotationView;

@end
