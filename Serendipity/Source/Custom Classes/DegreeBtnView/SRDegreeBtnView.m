#import "SRDegreeBtnView.h"

@implementation SRDegreeBtnView

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRDegreeBtnView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }

    // Return
    return self;
}

- (void)setUpView {
    [self setUpButton:self.degreeBtn1];
    [self setUpButton:self.degreeBtn2];
    [self setUpButton:self.degreeBtn3];
    [self setUpButton:self.degreeBtn4];
    [self setUpButton:self.degreeBtn5];

    [self.degreeBtn1 setTitle:[NSString stringWithFormat:@"1%@", @"\u00B0"] forState:UIControlStateNormal];
    [self.degreeBtn2 setTitle:[NSString stringWithFormat:@"2%@", @"\u00B0"] forState:UIControlStateNormal];
    [self.degreeBtn3 setTitle:[NSString stringWithFormat:@"3%@", @"\u00B0"] forState:UIControlStateNormal];
    [self.degreeBtn4 setTitle:[NSString stringWithFormat:@"4%@", @"\u00B0"] forState:UIControlStateNormal];
    [self.degreeBtn5 setTitle:[NSString stringWithFormat:@"5%@", @"\u00B0"] forState:UIControlStateNormal];
    [self.degreeBtn6 setTitle:[NSString stringWithFormat:@"6%@", @"\u00B0"] forState:UIControlStateNormal];
}


- (void)setUpButton:(UIButton *)btn {
    btn.backgroundColor = [UIColor lightGrayColor];

    btn.layer.borderColor = [[UIColor blackColor] CGColor];
    btn.layer.borderWidth = 1;
}

#pragma mark- degree button select

- (IBAction)degreeBtnSelected:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedDegreeButton:withDegree:)]) {

        UIButton *dBtn = (UIButton *) sender;
        NSUInteger degree = 0;

        if (dBtn == self.degreeBtn1) {

            degree = 1;

        } else if (dBtn == self.degreeBtn2) {

            degree = 2;
        } else if (dBtn == self.degreeBtn3) {

            degree = 3;
        } else if (dBtn == self.degreeBtn4) {

            degree = 4;
        } else if (dBtn == self.degreeBtn5) {

            degree = 5;
        } else if (dBtn == self.degreeBtn6) {

            degree = 6;
        }
        [self.delegate selectedDegreeButton:dBtn withDegree:degree];
    }
}

@end
