


#import <UIKit/UIKit.h>

@protocol SRDegreeChooseDelegate <NSObject>

@optional

-(void)selectedDegreeButton:(UIButton *)degreeBtn withDegree:(NSUInteger)degree;


@end

@interface SRDegreeBtnView : UIView
{
    
}
//Properties
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn1;
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn2;
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn3;
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn4;
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn5;
@property (strong, nonatomic) IBOutlet UIButton *degreeBtn6;

@property (weak) id<SRDegreeChooseDelegate> delegate;

-(void)setUpView;

- (IBAction)degreeBtnSelected:(id)sender;

@end
