
#import "ListCellView.h"

@implementation ListCellView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ListCellView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }
    
    // Return
    return self;
}

@end
