//
//  FrequencyUpdateView.m
//  Serendipity
//
//  Created by Long Lee on 9/22/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "FrequencyUpdateView.h"

@interface FrequencyUpdateView()

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, strong) IBOutlet UIImageView *statusBackground;
@property (nonatomic, strong) IBOutlet UIImageView *statusIndicator;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) NSDictionary *userInfo;

@end

@implementation FrequencyUpdateView

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"FrequencyUpdateView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
        self.frame = frame;
    }
    
    // Return
    return self;
}

- (void)updateFrequencyWithUserDict:(NSDictionary *)dict {
    self.userInfo = dict;
    NSString *batteryUsage = [self getBatterySettings];
    
    NSString *lastUpdateTime = [self.userInfo[@"location"] valueForKey:@"location_captured_time"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSDate *dateNow = [NSDate date];
    
    NSDate *updateTime = [dateFormat dateFromString:lastUpdateTime];
    NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:updateTime];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    int daysOffline = (int) (hoursBetweenDates / 24);

    
    if (batteryUsage) {
        batteryUsage = [batteryUsage lowercaseString];
        [self updateBackgroundImageWithColor:[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage]];
        [self.statusIndicator setImage:[UIImage imageNamed:@"icons-8-available-updates-filled-100"]];
        if ([batteryUsage isEqualToString:@"realtime"] || [batteryUsage isEqualToString:@"recommended"]) {
            [self.statusBackground setImage:[UIImage imageNamed:@"oval-3-copy"]];
//            [self updateBackgroundImageWithColor:[[CommonFunction shared] colorFromHexString:@"f5a623"]];
        } else if ([batteryUsage isEqualToString:@"very low"]) {
            [self.statusBackground setImage:[UIImage imageNamed:@"oval-2"]];
            [self updateBackgroundImageWithColor:[[CommonFunction shared] colorFromHexString:@"4a90e2"]];
        } else if ([batteryUsage isEqualToString:@"low"]) {
            [self.statusBackground setImage:[UIImage imageNamed:@"medium"]];
//            [self updateBackgroundImageWithColor:[[CommonFunction shared] colorFromHexString:@"f8e71c"]];
        } else if ([batteryUsage containsString:@"medium"]) {
            [self.statusBackground setImage:[UIImage imageNamed:@"recommended"]];
//            [self updateBackgroundImageWithColor:[[CommonFunction shared] colorFromHexString:@"33a532"]];
        } else if ([batteryUsage containsString:@"ground travel"] || [batteryUsage containsString:@"ground mode"]) {
            [self.statusIndicator setImage:[UIImage imageNamed:@"ground_mode"]];
            [self.statusBackground setImage:[UIImage imageNamed:@"travel_mode"]];
//            [self updateBackgroundImageWithColor:[UIColor blackColor]];
        } else if ([batteryUsage containsString:@"air travel"] || [batteryUsage containsString:@"air mode"]) {
            [self.statusIndicator setImage:[UIImage imageNamed:@"airplane"]];
            [self.statusBackground setImage:[UIImage imageNamed:@"travel_mode"]];
//            [self updateBackgroundImageWithColor:[UIColor blackColor]];
        }

        NSString *__text = [[CommonFunction shared] displayFromDayofline:daysOffline hoursBetweenDates: (int)(hoursBetweenDates)];
        if (__text.length > 0) {
            [self updateOfflineLabel:__text];
        }
    } else {

    }
    
}

- (UIImage *)toImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0f);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return snapshotImage;

}

//MARK: - Private Method
- (NSString *)getBatterySettings {
    if (self.userInfo[@"setting"]) {
        if ([self.userInfo[@"setting"] valueForKey:@"battery_usage"]) {
            return [self.userInfo[@"setting"] valueForKey:@"battery_usage"];
        }
    }
    return nil;
}

- (void)updateBackgroundImageWithColor:(UIColor *)color {
//    [self.bgImg setImage:[UIImage imageNamed:@"marker-yellow"]];
//    [self.bgImg setTintColor:[UIColor whiteColor]];
//    self.bgImg.image = [self.bgImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [self.bgImg setTintColor:color];
}

- (void)updateOfflineLabel:(NSString *)status {
    self.statusLabel.hidden = NO;
    self.statusIndicator.hidden = YES;
    self.statusLabel.text = status;
    [self.statusBackground setImage:[UIImage imageNamed:@"group-2"]];
    [self updateBackgroundImageWithColor:[UIColor grayColor]];
}

@end
