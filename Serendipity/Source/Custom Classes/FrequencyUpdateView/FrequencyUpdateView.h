//
//  FrequencyUpdateView.h
//  Serendipity
//
//  Created by Long Lee on 9/22/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FrequencyUpdateView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imvIndicatorWidth;
- (void)updateFrequencyWithUserDict:(NSDictionary *)dict;
- (UIImage *)toImage;
@end

