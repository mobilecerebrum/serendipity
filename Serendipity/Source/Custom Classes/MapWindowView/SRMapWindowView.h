#import <UIKit/UIKit.h>
#import "SRCustomImageView.h"

@interface SRMapWindowView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet SRCustomImageView *userProfileImage;
@property (strong, nonatomic) IBOutlet UIButton *pingButton;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupation;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblDegree;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UIImageView *datingImgView;

@property (strong, nonatomic) NSDictionary *object;

// Delegate
@property (weak) id delegate;

@end
