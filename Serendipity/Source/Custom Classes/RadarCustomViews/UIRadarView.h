//
//  UIRadarView.h
//  Serendipity
//
//  Created by Mcuser on 2/14/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN


@interface UIRadarView : UIView

@property (nonatomic, strong) CMMotionManager* motionManager;
@property (nonatomic, strong) CLLocationManager* locationManager;

@end

NS_ASSUME_NONNULL_END
