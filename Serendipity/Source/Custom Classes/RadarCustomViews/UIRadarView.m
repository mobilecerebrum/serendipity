//
//  UIRadarView.m
//  Serendipity
//
//  Created by Mcuser on 2/14/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "UIRadarView.h"

@interface UIRadarView()<CLLocationManagerDelegate>
@end
@implementation UIRadarView

#pragma mark - CLLocation Manager delegates

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
   // NSLog(@"moved from %@ to %@",oldLocation, newLocation);
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    NSLog(@"received Core Location Error %@",error);
    [self.locationManager stopUpdatingLocation];
    
}

#pragma mark - CMMotion and CLLocation shared managers

- (CMMotionManager*) motionManager {
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    return _motionManager;
}

- (CLLocationManager*) locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}

#pragma mark - CLLocation start/stop

#pragma mark - sharedManager creation

- (id) init {
    if (self = [super init]) {
        //[self  copyDatabaseFromBundle];
        [self motionManager];
        [self locationManager];
//        [_locationManager startUpdatingHeading];
//        [_motionManager startGyroUpdates];
//        [_motionManager startDeviceMotionUpdates];
//        [_motionManager startAccelerometerUpdates];
    }
    return self;
}



@end
