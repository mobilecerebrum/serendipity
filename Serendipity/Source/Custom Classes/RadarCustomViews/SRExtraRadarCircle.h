#import <UIKit/UIKit.h>

#define DEGREES_TO_RADIANS(degrees) (M_PI * degrees / 180)

@interface SRExtraRadarCircle : UIView

@end
