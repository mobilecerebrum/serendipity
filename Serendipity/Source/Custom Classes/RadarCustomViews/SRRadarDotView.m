#import "SRRadarDotView.h"

@implementation SRRadarDotView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
	// Call super
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRRadarDotView" owner:self options:nil];
		self = [nibArray objectAtIndex:0];

		// Register the notification
		/*NSNotificationCenter *defultCenter = [NSNotificationCenter defaultCenter];
		[defultCenter addObserver:self
		                 selector:@selector(getUserProfileImageSucceed:)
		                     name:kKeyNotificationRadarUserImageDownloaded
		                   object:nil];*/
	}

	// Return
	return self;
}

#pragma mark-
#pragma mark- Notification Methods
#pragma mark-

//______________________________________________________________________________
// getUserProfileImageSucceed:

- (void)getUserProfileImageSucceed:(NSNotification *)inNotify {
	NSDictionary *dict = [inNotify object];
	if ([[dict objectForKey:kKeyImageName]isEqualToString:[self.userProfile objectForKey:kKeyImageName]]) {
		UIImage *img = [dict objectForKey:kKeyImageObject];
		
        if (img) {
			self.profileImg = img;
		}
	}
}

@end
