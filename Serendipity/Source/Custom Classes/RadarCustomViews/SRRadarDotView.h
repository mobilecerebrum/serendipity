#import <UIKit/UIKit.h>

@interface SRRadarDotView : UIView

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) UIImage *profileImg;

// Other Properties
@property (nonatomic, strong) NSNumber *bearing;
@property (nonatomic, strong) NSNumber *userDistance;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSDictionary *userProfile;
@property (nonatomic, assign) BOOL isImageApplied;

@property (nonatomic, strong) NSNumber *initialDistance;
@property (nonatomic, readwrite) BOOL zoomEnabled;
@property (nonatomic, assign) CGRect initialFrame;

@end
