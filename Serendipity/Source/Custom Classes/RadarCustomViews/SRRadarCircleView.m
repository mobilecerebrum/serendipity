#import "SRRadarCircleView.h"

@implementation SRRadarCircleView

#pragma mark
#pragma mark Init Method
#pragma mark

// ------------------------------------------------------------------------------------
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
	// Call Super
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		[self setOpaque:NO];
	}

	// Return
	return self;
}

#pragma mark
#pragma mark Standard Overrides Method
#pragma mark

// ------------------------------------------------------------------------------------
// drawRect:

- (void)drawRect:(CGRect)rect {
	// The colors of the arcs
	NSArray *colors = @[
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8],
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5],
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4],
	];

	// initial radius & increment in radius
	NSInteger radius = 40;

	// angles - some random start & end angles
	NSArray *angles = @[
	    @{ @"start":@0, @"end":@360 },
	    @{ @"start":@0, @"end":@360 },
	    @{ @"start":@0, @"end":@360 },
	];
	for (int i = 0; i < colors.count; i++) {
		// set stroke color
		[colors[i] setStroke];

		// create a bezier path arc
		UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(150, 150)
		                                                    radius:radius
		                                                startAngle:DEGREES_TO_RADIANS([[angles[i] valueForKey:@"start"] integerValue])
		                                                  endAngle:DEGREES_TO_RADIANS([[angles[i] valueForKey:@"end"] integerValue])
		                                                 clockwise:NO];

		// Adjust the drawing options as needed
		// The line width of the path
		if (i == 0) {
			path.lineWidth = 30;
			radius += 34;
		}
		else {
			path.lineWidth = 38;
			radius += 38;
		}
		// draw the arc
		[path stroke];

		// increase the radius for next arc
		//radius += increment + 30;
	}

	// Create the white path around the profile image
	CGRect frame = CGRectMake(125, 125, 50, 50);
	[[UIColor whiteColor] setStroke];
	[[UIColor clearColor] setFill];
	UIBezierPath *circlePath = [UIBezierPath bezierPathWithOvalInRect:
	                            frame];
	circlePath.lineWidth = 2;
	[circlePath fill];
	[circlePath stroke];
}

@end
