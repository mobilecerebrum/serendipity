#import "SRExtraRadarCircle.h"

@implementation SRExtraRadarCircle

#pragma mark
#pragma mark Standard Overrides Method
#pragma mark

// ------------------------------------------------------------------------------------
// drawRect:

- (void)drawRect:(CGRect)rect {
	// The colors of the arcs
	NSArray *colors = @[
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3],
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.25],
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2],
	    [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1],
	];

	// initial radius & increment in radius
	NSInteger radius = 150;//150;

	// angles - some random start & end angles
	NSArray *angles = @[
	    @{ @"start":@0, @"end":@360 },
	    @{ @"start":@0, @"end":@360 },
	    @{ @"start":@0, @"end":@360 },
	    @{ @"start":@0, @"end":@360 },
	    @{ @"start":@0, @"end":@360 },
	];
	for (int i = 0; i < colors.count; i++) {
		// set stroke color
		[colors[i] setStroke];

		// create a bezier path arc
		UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)
		                                                    radius:radius
		                                                startAngle:DEGREES_TO_RADIANS([[angles[i] valueForKey:@"start"] integerValue])
		                                                  endAngle:DEGREES_TO_RADIANS([[angles[i] valueForKey:@"end"] integerValue])
		                                                 clockwise:NO];

		// Adjust the drawing options as needed
		// The line width of the path
		path.lineWidth = 38;
		radius += 38;
		[path stroke];
	}
}

@end
