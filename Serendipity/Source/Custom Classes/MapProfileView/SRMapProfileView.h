#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

@interface SRMapProfileView : UIView {
    
}

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *bgImg,*backImg;
@property (nonatomic, strong) IBOutlet SRProfileImageView *profileImg;
@property (nonatomic, strong) IBOutlet UIImageView *userProfileImg;
@property (nonatomic, strong) IBOutlet UILabel *lblMemberCnt;
@property (nonatomic, strong) IBOutlet UILabel *lblSpeed;
@property (nonatomic, strong) IBOutlet UIImageView *imgDirection;
@property (nonatomic, strong) IBOutlet UIImageView *imgEast;
@property (nonatomic, strong) IBOutlet UIImageView *imgWest;
@property (nonatomic, strong) IBOutlet UIImageView *imgNorth;
@property (nonatomic, strong) IBOutlet UIImageView *imgSouth;
@property (nonatomic, strong) IBOutlet UIImageView *imgNW;
@property (nonatomic, strong) IBOutlet UIImageView *imgSW;
@property (nonatomic, strong) IBOutlet UIImageView *imgNE;
@property (nonatomic, strong) IBOutlet UIImageView *imgSE;
@property (nonatomic, strong) IBOutlet UIView *collectionView;
@property (nonatomic, strong) IBOutlet UIImageView *img1;
@property (nonatomic, strong) IBOutlet UIImageView *img2;
@property (nonatomic, strong) IBOutlet UIImageView *img3;
@property (nonatomic, strong) IBOutlet UIImageView *img4;

@property (nonatomic, strong) IBOutlet UIView *statusViewContainer;
@property (nonatomic, strong) IBOutlet UIImageView *statusBackground;
@property (nonatomic, strong) IBOutlet UIImageView *statusIndicator;
@property (nonatomic, strong) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) NSDictionary *userDict;
@property (nonatomic, strong) NSArray *combinationsArr;
@property (nonatomic, strong) NSDictionary *userInfo;


#pragma mark-
#pragma mark- Download Methods
#pragma mark-

- (void)downloadImage:(NSDictionary *) dataDict;
- (BOOL)isMyFriend;
- (void)updateStatus;

@end
