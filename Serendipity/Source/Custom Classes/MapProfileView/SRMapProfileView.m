#import "SRMapProfileView.h"


@implementation SRMapProfileView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRMapProfileView" owner:self options:nil];
        self = nibArray[0];

        // Provide round rect
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2.0;
        self.profileImg.layer.masksToBounds = YES;
        self.profileImg.clipsToBounds = YES;

    }
    return self;
}

#pragma mark- Download Methods

- (UIImage *)makeRoundedImage:(UIImage *)image radius:(float)radius {
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, radius, radius);
    imageLayer.contents = (id) image.CGImage;

    imageLayer.masksToBounds = YES;
    radius = radius / 2;
    imageLayer.cornerRadius = radius;

    UIGraphicsBeginImageContext(CGSizeMake(radius, radius));
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return roundedImage;
}

- (void)downloadImage:(NSDictionary *)dataDict {
    if ([dataDict[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *profileDict = dataDict[kKeyProfileImage];
        NSString *imageName = profileDict[kKeyImageName];
        if ([imageName length] > 0) {
            NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
            [self.userProfileImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        } else {
            self.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
        }
    } else {
        self.userProfileImg.contentMode = UIViewContentModeScaleAspectFill;
        self.userProfileImg.layer.cornerRadius = self.userProfileImg.frame.size.width / 2.0;
        self.userProfileImg.clipsToBounds = YES;
        self.userProfileImg.layer.masksToBounds = YES;
        self.userProfileImg.image = [UIImage imageNamed:@"profile_menu.png"];
    }
}

- (BOOL)isMyFriend {
    if (self.userInfo[@"connection"] != nil) {
        if ([self.userInfo[@"connection"] isKindOfClass:[NSDictionary class]]) {
            if ([self.userInfo[@"connection"] valueForKey:@"user_id"] != nil) {
                if ([[self.userInfo[@"connection"] valueForKey:@"user_id"] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"id"]] || [[self.userInfo[@"connection"] valueForKey:@"connection_id"] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"id"]]) {
                    self.statusViewContainer.hidden = NO;
                    [self bringSubviewToFront:self.statusViewContainer];
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void)updateStatus {
    if ([self isMyFriend]) {
        // location > location_captured_time
        // geohash

        // setting > battery_usage
        // setting > broadcast_location
        // update icon for battery settings
        NSString *batteryUsage = [self getBatterySettings];

        NSString *lastUpdateTime = [self.userInfo[@"location"] valueForKey:@"location_captured_time"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];

        NSDate *dateNow = [NSDate date];
        
        
        NSDate *updateTime = [dateFormat dateFromString:lastUpdateTime];//[NSDate dateWithTimeIntervalSinceNow: longLongValue]];
        NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:updateTime];
        double secondsInAnHour = 3600;
        NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
        int daysOffline = (int) (hoursBetweenDates / 24);

        BOOL broadcastLocation = [[self.userInfo[@"setting"] valueForKey:@"broadcast_location"] boolValue];
        [self.bgImg setImage:[UIImage imageNamed:@"marker-yellow"]];
        self.bgImg.image = [self.bgImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.bgImg setTintColor:[self colorFromHexString:@"f5a623"]];
        if (!broadcastLocation) {
            self.statusLabel.hidden = NO;
            self.userProfileImg.alpha = 0.5f;
            self.bgImg.alpha = 0.5f;
            self.statusLabel.text = @"OFF";
           // [self.statusBackground setImage:[UIImage imageNamed:@"oval-2-copy"]];
          //  [self updateBackgroundImageWithColor:[UIColor whiteColor]];
        } else if (batteryUsage) {
            batteryUsage = [batteryUsage lowercaseString];
            [self.statusIndicator setImage:[UIImage imageNamed:@"icons-8-available-updates-filled-100"]];
            [self updateBackgroundImageWithColor:[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage]];
            if (([batteryUsage isEqualToString:@"realtime"] || [batteryUsage isEqualToString:@"recommended"])) {
                [self.statusBackground setImage:[UIImage imageNamed:@"oval-3-copy"]];
                
            } else if ([batteryUsage isEqualToString:@"very low"]) {
                [self.statusBackground setImage:[UIImage imageNamed:@"oval-2"]];
                //[self updateBackgroundImageWithColor:[self colorFromHexString:@"4a90e2"]];
            } else if ([batteryUsage isEqualToString:@"low"]) {
                [self.statusBackground setImage:[UIImage imageNamed:@"medium"]];
                //[self updateBackgroundImageWithColor:[self colorFromHexString:@"f8e71c"]];
            } else if ([batteryUsage containsString:@"medium"]) {
                [self.statusBackground setImage:[UIImage imageNamed:@"recommended"]];
                //[self updateBackgroundImageWithColor:[self colorFromHexString:@"33a532"]];
            } else if ([batteryUsage containsString:@"ground travel"] || [batteryUsage containsString:@"ground mode"]) {
                [self.statusIndicator setImage:[UIImage imageNamed:@"ground_mode"]];
                [self.statusBackground setImage:[UIImage imageNamed:@"travel_mode"]];
                //[self updateBackgroundImageWithColor:[UIColor blackColor]];
            } else if ([batteryUsage containsString:@"air travel"] || [batteryUsage containsString:@"air mode"]) {
                [self.statusIndicator setImage:[UIImage imageNamed:@"airplane"]];
                [self.statusBackground setImage:[UIImage imageNamed:@"travel_mode"]];
                //[self updateBackgroundImageWithColor:[UIColor blackColor]];
            }
            int years = daysOffline / 365;
            int months = daysOffline / 31;
            
            if (years > 0) {
                [self updateOfflineLabel:[NSString stringWithFormat:@"%dy", years]];
            } else if (months > 0) {
                [self updateOfflineLabel:[NSString stringWithFormat:@"%dm", months]];
            } else if (daysOffline >= 21 && daysOffline < 30) {
                [self updateOfflineLabel:@"3w"];
            } else if (daysOffline >= 14 && daysOffline < 21) {
                [self updateOfflineLabel:@"2w"];
            } else if (daysOffline >= 14 && daysOffline < 21) {
                [self updateOfflineLabel:@"2w"];
            } else if (daysOffline >= 7 && daysOffline < 14) {
                [self updateOfflineLabel:@"1w"];
            } else if (daysOffline == 6) {
                [self updateOfflineLabel:@"6d"];
            } else if (daysOffline == 5) {
                [self updateOfflineLabel:@"5d"];
            } else if (daysOffline == 4) {
                [self updateOfflineLabel:@"4d"];
            } else if (daysOffline == 3) {
                [self updateOfflineLabel:@"96h"];
            } else if (daysOffline == 2) {
                [self updateOfflineLabel:@"48h"];
            } else if (hoursBetweenDates >= 24 && daysOffline < 2) {
                [self updateOfflineLabel:@"24h"];
            }
            
//            if (years >= 1) {
//                [self updateOfflineLabel:@"1y"];
//            } else if (years >= 2) {
//                [self updateOfflineLabel:@"2y"];
//            } else if (daysOffline >= 30) {
//                [self updateOfflineLabel:@"1m"];
//            }
            
        }
    }
//    self.statusLabel.hidden = NO;
//    self.statusIndicator.hidden = YES;
//    [self updateOfflineLabel:@"24h+"];
}

- (void)updateBackgroundImageWithColor:(UIColor *)color {
    
    [self.bgImg setImage:[UIImage imageNamed:@"marker-yellow"]];
    [self.bgImg setTintColor:[UIColor whiteColor]];
    self.bgImg.image = [self.bgImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.bgImg setTintColor:color];
}

- (void)updateOfflineLabel:(NSString *)status {
    self.statusLabel.hidden = NO;
    self.statusIndicator.hidden = YES;
    self.statusLabel.text = status;
    [self.statusBackground setImage:[UIImage imageNamed:@"group-2"]];
    [self updateBackgroundImageWithColor:[UIColor grayColor]];
}

- (NSString *)getBatterySettings {
    if (self.userInfo[@"setting"]) {
        if ([self.userInfo[@"setting"] valueForKey:@"battery_usage"]) {
            return [self.userInfo[@"setting"] valueForKey:@"battery_usage"];
        }
    }
    return nil;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

-(CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
