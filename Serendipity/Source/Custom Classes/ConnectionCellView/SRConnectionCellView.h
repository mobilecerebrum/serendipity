#import <UIKit/UIKit.h>

@protocol SRConnectionCellViewProtocol;

@interface SRConnectionCellView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgCellBg;
@property (weak, nonatomic) IBOutlet UILabel *lblOnline;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblProfession;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgDating;
@property (weak, nonatomic) IBOutlet UILabel *lblDating;
@property (weak, nonatomic) IBOutlet UILabel *lblFamily;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;

@property (weak, nonatomic) IBOutlet UIImageView *favImg,*blackOverlay;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
// (weak, nonatomic) IBOutlet UILabel *lblDegree;
@property (weak, nonatomic) IBOutlet UIButton *btnProfileView;
@property (weak, nonatomic) IBOutlet UIButton *btnCompass, *btnChat,*btnGesture;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnDegree;
@property (weak, nonatomic) IBOutlet UIButton *btnInvisible;
@property (weak, nonatomic) IBOutlet UIView *baterryWrapper;
@property (weak, nonatomic) IBOutlet UIImageView *batteryStatusIcon;
@property (weak, nonatomic) IBOutlet UIImageView *batteryStateBg;
@property (weak, nonatomic) IBOutlet UILabel *batterStateLbl;
@property (nonatomic, strong) NSDictionary *userInfo;

@property (weak) id<SRConnectionCellViewProtocol> delegate;

- (void)updateStatus;

@end


@protocol SRConnectionCellViewProtocol <NSObject>
-(void)viewDidClickedCheckBox:(SRConnectionCellView *)view;
@end
