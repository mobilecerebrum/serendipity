#import "SRConnectionCellView.h"

@implementation SRConnectionCellView

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionCellView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }
    [self registerNotification];
    [self rotationCompass:-((APP_DELEGATE).compassHeading * M_PI / 180)];
    // Return
    return self;
}

- (BOOL)isMyFriend {
    if (self.userInfo[@"connection"] != nil) {
        if ([self.userInfo[@"connection"] isKindOfClass:[NSDictionary class]]) {
            if ([self.userInfo[@"connection"] valueForKey:@"user_id"] != nil) {
                if ([[self.userInfo[@"connection"] valueForKey:@"user_id"] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"id"]] || [[self.userInfo[@"connection"] valueForKey:@"connection_id"] isEqualToString:[(APP_DELEGATE).server.loggedInUserInfo valueForKey:@"id"]]) {
                    self.baterryWrapper.hidden = NO;
                    [self bringSubviewToFront:self.baterryWrapper];
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (void)updateStatus {
    if ([self isMyFriend]) {
        // location > location_captured_time
        // geohash
        
        // setting > battery_usage
        // setting > broadcast_location
        // update icon for battery settings
        NSString *batteryUsage = [self getBatterySettings];
        
        NSString *lastUpdateTime = [self.userInfo[@"location"] valueForKey:@"location_captured_time"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        
        NSDate *dateNow = [NSDate date];
        NSDate *updateTime = [dateFormat dateFromString:lastUpdateTime];//[NSDate dateWithTimeIntervalSinceNow: longLongValue]];
        NSTimeInterval distanceBetweenDates = [dateNow timeIntervalSinceDate:updateTime];
        double secondsInAnHour = 3600;
        NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
        int daysOffline = (int) (hoursBetweenDates / 24);
        
        BOOL broadcastLocation = [[self.userInfo[@"setting"] valueForKey:@"broadcast_location"] boolValue];
      //  [self.batteryStateBg setImage:[UIImage imageNamed:@"marker-yellow"]];
      //  self.batteryStateBg.image = [self.batteryStateBg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
      //  [self.batteryStateBg setTintColor:[self colorFromHexString:@"f5a623"]];
        if (!broadcastLocation) {
            self.batterStateLbl.hidden = NO;
            self.imgProfile.alpha = 0.5f;
            self.imgProfile.alpha = 0.5f;
            self.batterStateLbl.text = @"OFF";
            // [self.statusBackground setImage:[UIImage imageNamed:@"oval-2-copy"]];
            //  [self updateBackgroundImageWithColor:[UIColor whiteColor]];
        } else if (batteryUsage) {
            batteryUsage = [batteryUsage lowercaseString];
            [self updateBackgroundImageWithColor:[APP_DELEGATE getColorBaseOnBatteryUsage:batteryUsage]];
            [self.batteryStatusIcon setImage:[UIImage imageNamed:@"icons-8-available-updates-filled-100"]];
            if (([batteryUsage isEqualToString:@"realtime"] || [batteryUsage isEqualToString:@"recommended"])) {
                [self.batteryStateBg setImage:[UIImage imageNamed:@"oval-3-copy"]];
               // [self updateBackgroundImageWithColor:[self colorFromHexString:@"f5a623"]];
            } else if ([batteryUsage isEqualToString:@"very low"]) {
                [self.batteryStateBg setImage:[UIImage imageNamed:@"oval-2"]];
                //[self updateBackgroundImageWithColor:[self colorFromHexString:@"4a90e2"]];
            } else if ([batteryUsage isEqualToString:@"low"]) {
                [self.batteryStateBg setImage:[UIImage imageNamed:@"medium"]];
                //[self updateBackgroundImageWithColor:[self colorFromHexString:@"f8e71c"]];
            } else if ([batteryUsage containsString:@"medium"]) {
                [self.batteryStateBg setImage:[UIImage imageNamed:@"recommended"]];
                //[self updateBackgroundImageWithColor:[self colorFromHexString:@"33a532"]];
            } else if ([batteryUsage containsString:@"ground travel"] || [batteryUsage containsString:@"ground mode"]) {
                [self.batteryStatusIcon setImage:[UIImage imageNamed:@"ground_mode"]];
                [self.batteryStateBg setImage:[UIImage imageNamed:@"travel_mode"]];
              //  [self updateBackgroundImageWithColor:[UIColor blackColor]];
            } else if ([batteryUsage containsString:@"air travel"] || [batteryUsage containsString:@"air mode"]) {
                [self.batteryStatusIcon setImage:[UIImage imageNamed:@"airplane"]];
                [self.batteryStateBg setImage:[UIImage imageNamed:@"travel_mode"]];
                //[self updateBackgroundImageWithColor:[UIColor blackColor]];
            }
            
            int years = daysOffline / 365;
            int months = daysOffline / 31;
            
            if (years > 0) {
                [self updateOfflineLabel:[NSString stringWithFormat:@"%dy", years]];
            } else if (months > 0) {
                [self updateOfflineLabel:[NSString stringWithFormat:@"%dm", months]];
            } else if (daysOffline >= 21 && daysOffline < 30) {
                [self updateOfflineLabel:@"3w"];
            } else if (daysOffline >= 14 && daysOffline < 21) {
                [self updateOfflineLabel:@"2w"];
            } else if (daysOffline >= 14 && daysOffline < 21) {
                [self updateOfflineLabel:@"2w"];
            } else if (daysOffline >= 7 && daysOffline < 14) {
                [self updateOfflineLabel:@"1w"];
            } else if (daysOffline == 6) {
                [self updateOfflineLabel:@"6d"];
            } else if (daysOffline == 5) {
                [self updateOfflineLabel:@"5d"];
            } else if (daysOffline == 4) {
                [self updateOfflineLabel:@"4d"];
            } else if (daysOffline == 3) {
                [self updateOfflineLabel:@"96h"];
            } else if (daysOffline == 2) {
                [self updateOfflineLabel:@"48h"];
            } else if (hoursBetweenDates >= 24 && daysOffline < 2) {
                [self updateOfflineLabel:@"24h"];
            }
        }
    }
    //    self.statusLabel.hidden = NO;
    //    self.statusIndicator.hidden = YES;
    //    [self updateOfflineLabel:@"24h+"];
}

- (void)updateBackgroundImageWithColor:(UIColor *)color {
    
   // [self.batteryStateBg setImage:[UIImage imageNamed:@"marker-yellow"]];
   // [self.batteryStateBg setTintColor:[UIColor whiteColor]];
  //  self.batteryStateBg.image = [self.batteryStateBg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
  //  [self.batteryStateBg setTintColor:color];
}

- (void)updateOfflineLabel:(NSString *)status {
    self.batterStateLbl.hidden = NO;
    self.batteryStatusIcon.hidden = YES;
    self.batterStateLbl.text = status;
    [self.batteryStateBg setImage:[UIImage imageNamed:@"group-2"]];
    [self updateBackgroundImageWithColor:[UIColor grayColor]];
}

- (NSString *)getBatterySettings {
    if (self.userInfo[@"setting"]) {
        if ([self.userInfo[@"setting"] valueForKey:@"battery_usage"]) {
            return [self.userInfo[@"setting"] valueForKey:@"battery_usage"];
        }
    }
    return nil;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

-(CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}


// Compass rotation
- (void)registerNotification {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(updateHeadingAngle:)
                          name:kKeyNotificationUpdateHeadingValue
                        object:nil];
}

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    float headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    [self rotationCompass:headingAngle];
}

- (void)rotationCompass:(CGFloat)headingAngle {
    // rotate the compass to heading degree
    self.btnCompass.transform = CGAffineTransformMakeRotation(headingAngle);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)btnCheckBoxClicked:(UIButton *)sender {
    if (sender.isSelected) {
        [sender setSelected:false];
    } else {
        [sender setSelected:true];
    }
    
    if ([self.delegate respondsToSelector:@selector(viewDidClickedCheckBox:)]) {
        [self.delegate viewDidClickedCheckBox:self];
    }
}

@end
