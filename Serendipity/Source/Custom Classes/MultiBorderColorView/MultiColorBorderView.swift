//
//  MultiColorBorderView.swift
//  Serendipity
//
//  Created by Rahul Patel on 24/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

import UIKit
import CoreGraphics

@objcMembers public class Paint: NSObject {
    public var color: UIColor?
}

@objcMembers public class ColorData: NSObject {
    public var percentage: CGFloat = 0.0
    public var color: UIColor?
}

@objcMembers class MultiBorderColorView: UIView {
    
    private var totalLength: CGFloat = 0.0;
    private var fillLength: CGFloat = 0.0;
    private var topLength: CGFloat = 0.0;
    private var topRightLength: CGFloat = 0.0;
    private var topRightBottomLength: CGFloat = 0.0;
    private var measuredWidth: CGFloat = 0.0, measuredHeight: CGFloat = 0.0;
    private var strokeWidth: CGFloat = 10.0;
    private var firstHalfLength: CGFloat = 0.0
    var colorList = [ColorData]() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        measuredWidth = frame.width
        measuredHeight = frame.height;
        //For Mid,Left
        firstHalfLength = measuredHeight / 2;
        topLength = firstHalfLength + measuredWidth;
        topRightLength = topLength + measuredHeight;
        topRightBottomLength = topRightLength + measuredWidth;
        //For Top,Left
//        topLength = measuredWidth;
//        topRightLength = topLength + measuredHeight;
//        topRightBottomLength = topRightLength + measuredWidth;
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = 6
        clipsToBounds = true
        for colorData in colorList {
            colorPortion(contex: UIGraphicsGetCurrentContext()!, colorData: colorData)
        }
    }
    
    func colorPortion(contex: CGContext, colorData: ColorData ) {
        let total = measuredWidth*2 + measuredHeight*2
        let currentLength = total * colorData.percentage
        let paint  = Paint()
        paint.color = colorData.color
        startDraw(context: contex, drawLength: currentLength, paint: paint)
    }
    
    
    
    func drawLine(start point:CGPoint, length: CGFloat, color: UIColor) -> CGPoint? {
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        var nextPoint: CGPoint! = CGPoint(x: length, y: 0.0)
        
        if length > frame.width  {
            context.strokeLineSegments(between: [point])
            nextPoint.x = length
        }
        if point.x >= frame.width {
            nextPoint.x = frame.width
            nextPoint.y = length
        }
        if point.y >= frame.height {
            nextPoint.y = frame.height
        }
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(10)
        context.strokeLineSegments(between: [point, nextPoint])
        
        return nextPoint
    }
    
    func startDraw(context: CGContext, drawLength: CGFloat, paint: Paint) {
        
        if (fillLength >= 0 && fillLength < firstHalfLength) {
            if (fillLength + drawLength <= measuredHeight / 2) {
                let rect = CGRect(x:0,y: (measuredHeight / 2) - (fillLength + drawLength),width:  strokeWidth,height:  (measuredHeight / 2) - fillLength)
                paint.color!.setFill()
                context.fill(rect)
                fillLength = fillLength + drawLength;
            } else {
                let rect = CGRect(x:0,y:  0,width:  strokeWidth,height:  (measuredHeight / 2) - fillLength)
                paint.color!.setFill()
                context.fill(rect)
                let lastfill = fillLength;
                fillLength = fillLength + (measuredHeight / 2) - fillLength;
                startDraw(context: context, drawLength: drawLength - ((measuredHeight / 2) - lastfill), paint: paint)
            }
        } else if (fillLength >= firstHalfLength && fillLength < topLength) {
            if (fillLength - firstHalfLength + drawLength <= measuredWidth) {
                let rect = CGRect(x: fillLength - firstHalfLength,y:  0,width:  fillLength - firstHalfLength + drawLength,height:  strokeWidth)
                paint.color!.setFill()
                context.fill(rect)
                fillLength = fillLength + drawLength;
            } else {
                let rect = CGRect(x: fillLength - firstHalfLength,y:  0,width:  measuredWidth,height:  strokeWidth)
                paint.color!.setFill()
                context.fill(rect)
                let lastfill  = fillLength;
                fillLength = fillLength + measuredWidth - (fillLength - firstHalfLength);
                startDraw(context: context, drawLength: drawLength - (measuredWidth - (lastfill - firstHalfLength)), paint: paint)
            }
        } else if (fillLength >= topLength && fillLength < topRightLength) {
            if (fillLength - topLength + drawLength <= measuredHeight) {
                let rect = CGRect(x: measuredWidth - strokeWidth, y: fillLength - topLength, width: measuredWidth, height: fillLength - topLength + drawLength)
                paint.color!.setFill()
                context.fill(rect)
                fillLength = fillLength + drawLength;
            } else {
                let rect = CGRect(x: measuredWidth - strokeWidth, y: fillLength - topLength, width: measuredWidth, height: measuredHeight)
                paint.color!.setFill()
                context.fill(rect)
                let lastfill = fillLength;
                fillLength = fillLength + ((measuredHeight - (fillLength - topLength)));
                startDraw(context: context, drawLength: drawLength - (measuredHeight - (lastfill - topLength)), paint: paint)
            }
        } else if (fillLength >= topRightLength && fillLength < topRightBottomLength) {
            if (fillLength - topRightLength + drawLength <= measuredWidth) {
                let rect = CGRect(x: measuredWidth - (fillLength - topRightLength + drawLength), y: measuredHeight - strokeWidth, width: measuredWidth - (fillLength - topRightLength ), height: measuredHeight)
                paint.color!.setFill()
                context.fill(rect)
                fillLength = fillLength + drawLength;
                
            } else {
                let rect = CGRect(x: 0, y: measuredHeight - strokeWidth, width: measuredWidth - (fillLength - topRightLength), height: measuredHeight)
                paint.color!.setFill()
                context.fill(rect)
                let lastfill = fillLength;
                fillLength = fillLength + measuredWidth - (fillLength - topRightLength);
                startDraw(context: context, drawLength: drawLength - (measuredWidth - (lastfill - topRightLength)), paint: paint)
            }
        } else {
            let rect = CGRect(x: 0,y: ((measuredHeight ) - (fillLength - topRightBottomLength)) - drawLength,width:  strokeWidth, height: (measuredHeight ) - (fillLength - topRightBottomLength))
            paint.color!.setFill()
            context.fill(rect)
            fillLength = fillLength + drawLength;
        }
    }
}
