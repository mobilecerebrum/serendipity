#import <UIKit/UIKit.h>

@interface SRActivityIndicator : UIView

// Properties
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
