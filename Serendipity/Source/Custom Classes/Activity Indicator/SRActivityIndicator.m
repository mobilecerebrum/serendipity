#import "SRActivityIndicator.h"

@implementation SRActivityIndicator

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
	// Call super
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRActivityIndicator" owner:self options:nil];
		self = [nibArray objectAtIndex:0];
		self.layer.cornerRadius = 5.0;
	}
	// Return
	return self;
}

@end
