
//  DDPageControlViewController.m
//  DDPageControl
//
//  Created by Damien DeVille on 1/14/11.
//  Copyright 2011 Snappy Code. All rights reserved.
//

#import "SRPageControlViewController.h"
#import "SRPageControl.h"
#import "UIImage+animatedGIF.h"
#import "UIImage+GIF.h"
#import "SRModalClass.h"
#import "FLAnimatedImage/FLAnimatedImage.h"


@implementation SRPageControlViewController

@synthesize scrollView ;

- (void)viewDidLoad {
    [super viewDidLoad] ;
        
    NSInteger numberOfPages = self.titleArr.count;
    
    
    // define the scroll view content size and enable paging
    [scrollView setPagingEnabled: YES] ;
    [scrollView setContentSize: CGSizeMake(SCREEN_WIDTH * numberOfPages, SCREEN_HEIGHT)] ;
    
    // programmatically add the page control
    pageControl = [[SRPageControl alloc] init] ;
    [pageControl setCenter: CGPointMake(SCREEN_WIDTH / 2, 125)] ;
    [pageControl setNumberOfPages: numberOfPages] ;
    [pageControl setCurrentPage: 0] ;
    [pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged] ;
    [pageControl setDefersCurrentPageDisplay: YES] ;
    [pageControl setType: DDPageControlTypeOnFullOffEmpty] ;
    [pageControl setOnColor: [UIColor whiteColor]] ;
    [pageControl setOffColor: [UIColor grayColor]] ;
    [pageControl setIndicatorDiameter: 10.0f] ;
    [pageControl setIndicatorSpace: 10.0f] ;
    [self.view addSubview: pageControl] ;
    
    //Add blink effect on BottomBarTab View
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(onTimerEvent:) userInfo:nil repeats:YES];
    
    //         Description label
    descLbl = [[UILabel alloc]initWithFrame:CGRectMake(20,10, SCREEN_WIDTH - 40, 140)];
    descLbl.font = [UIFont fontWithName:kFontHelveticaMedium size:16];
    descLbl.textColor = [UIColor whiteColor];
    descLbl.numberOfLines = 5;
    descLbl.lineBreakMode = NSLineBreakByWordWrapping;
    descLbl.textAlignment = NSTextAlignmentCenter;
    [descLbl setText:[self.titleArr objectAtIndex:0]];
    [self.view addSubview:descLbl];
    
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:200 / 255.0 blue:150/255.0 alpha:1.0];
    self.viewBottomTab.hidden = YES;
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaMedium size:10.0f],NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaRegular size:10.0f],NSForegroundColorAttributeName : appTintColor } forState:UIControlStateNormal];
    
    UIImage *deselectedImage = [[UIImage imageNamed:@"tabbar-radar.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImage = [[UIImage imageNamed:@"tabbar-radar-active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _radarTab.image =deselectedImage;
    _radarTab.selectedImage = selectedImage;
    
    UIImage *deselectedImage1 = [[UIImage imageNamed:@"tabbar-map.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImage1 = [[UIImage imageNamed:@"tabbar-map-active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _mapTab.image =deselectedImage1;
    _mapTab.selectedImage = selectedImage1;
    
    UIImage *deselectedImage2 = [[UIImage imageNamed:@"tabbar-list.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *selectedImage2 = [[UIImage imageNamed:@"tabbar-list-active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    _listTab.image = deselectedImage2;
    _listTab.selectedImage = selectedImage2;
    
    UIImageView *toolTipImg ;
    CGRect pageFrame ;
    for (int i = 0 ; i < numberOfPages ; i++) {
        // determine the frame of the current page
        pageFrame = CGRectMake(i * SCREEN_WIDTH, 12.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ;
        
        
        // create a page as a simple UILabel
        toolTipImg = [[UIImageView alloc] initWithFrame: pageFrame];
        [toolTipImg setUserInteractionEnabled:YES];
        [toolTipImg setBackgroundColor:[UIColor clearColor]];
        [toolTipImg setContentMode:UIViewContentModeScaleAspectFit];
        
        // add it to the scroll view
        [scrollView addSubview: toolTipImg];
        
        // Skip button
        skipBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, SCREEN_HEIGHT - 60, SCREEN_WIDTH - 40, 46)];
        skipBtn.titleLabel.font = [UIFont fontWithName:kFontHelveticaMedium size:24];
        [skipBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [skipBtn setBackgroundColor:[UIColor clearColor]];
        skipBtn.layer.borderColor = [[UIColor orangeColor]CGColor];
        skipBtn.layer.borderWidth = 1.0;
        skipBtn.layer.cornerRadius = 5;
        [skipBtn addTarget:self action:@selector(hideToolTipView) forControlEvents:UIControlEventTouchUpInside];
        if (i == numberOfPages - 1) {
            [skipBtn setTitle:@"Get Started" forState:UIControlStateNormal];
            [toolTipImg setContentMode:UIViewContentModeScaleAspectFill];
        } else {
            [skipBtn setTitle:@"Skip" forState:UIControlStateNormal];
            [toolTipImg setContentMode:UIViewContentModeScaleAspectFit];
        }
        [toolTipImg addSubview:skipBtn];
        
        if ((self.imageArr.count - 1) != 0 && (self.imageArr.count - 1) == i) {
            skipBtn.hidden = NO;
            [pageControl setCenter: CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT - 60.0f)] ;
        } else {
            skipBtn.hidden = YES;
        }
        
        [toolTipImg bringSubviewToFront:_viewBottomTab];
        if ([self.showTooltipOnView isEqualToString: @"SRDiscoveryRadarViewController"]) {
            
            if (i == 0) {
                if (SCREEN_WIDTH <= 320) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -3.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 375)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -3.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 414)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                
            } else if (i == 1) {
                [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH) - 2, -5.0f, SCREEN_WIDTH, 310) ] ;
            } else if (i == 2) {
                if (SCREEN_WIDTH <= 375) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 10, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                } else if (SCREEN_WIDTH <= 414) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 20, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
            } else if (i == 3) {
                if (SCREEN_WIDTH <= 320) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 100, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                } else if (SCREEN_WIDTH <= 375) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 150, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                } else if (SCREEN_WIDTH <= 414) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 150, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
            } else if (i == 4) {
                [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -5.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
            } else if (i == 5) {
                if (SCREEN_WIDTH <= 320) {
                   [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 5, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 375)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 0, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 414)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -5, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
            }
        }
        else if ([self.showTooltipOnView isEqualToString:@"SRDiscoveryListViewController"])
        {
            //[_tooltipTabBar setSelectedItem: [_tooltipTabBar.items objectAtIndex:2]];
            if (i == 0) {
                
                if (SCREEN_WIDTH <= 320) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 70.0f, SCREEN_WIDTH, SCREEN_HEIGHT - 27) ] ;
                }
                else if (SCREEN_WIDTH <= 375)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 70.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 414)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 80.0f, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                
                NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:[self.titleArr objectAtIndex:i]];
                NSString *boldString = @"compass";
                NSRange boldRange = [[self.titleArr objectAtIndex:i] rangeOfString:boldString];
                [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:20] range:boldRange];
                [descLbl setAttributedText: yourAttributedString];
            }
            else if (i == 2)
            {
                if (SCREEN_WIDTH <= 320) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 5, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 375)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 0, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 414)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -5, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
            }

        }
        else if ([self.showTooltipOnView isEqualToString:@"SRDiscoveryMapViewController"])
        {
            //[_tooltipTabBar setSelectedItem: [_tooltipTabBar.items objectAtIndex:0]];
            if (i == 0) {
                if (SCREEN_WIDTH <= 320) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -3.0f, SCREEN_WIDTH, SCREEN_HEIGHT)] ;
                }
                else if (SCREEN_WIDTH <= 375)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -3.0f, SCREEN_WIDTH, SCREEN_HEIGHT)] ;
                }
                else if (SCREEN_WIDTH <= 414)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT)] ;
                }
            }
            else if (i == 1)
            {
                if (SCREEN_WIDTH <= 320) {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 5, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 375)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), 0, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
                else if (SCREEN_WIDTH <= 414)
                {
                    [toolTipImg setFrame:CGRectMake((i * SCREEN_WIDTH), -5, SCREEN_WIDTH, SCREEN_HEIGHT) ] ;
                }
            }
        }
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:[self.imageArr objectAtIndex:i] withExtension:@"gif"]]];
        FLAnimatedImageView *imageView = [[FLAnimatedImageView alloc] init];
        imageView.animatedImage = image;
        imageView.frame = toolTipImg.frame;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.animationDuration = 1;
        //imageView.animationRepeatCount = 1;
        [scrollView addSubview:imageView];
    }
}

-(void)hideToolTipView {
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;

    [self dismissViewControllerAnimated:NO completion:^{
        [self hsHideShowTopBar];
    }];

    [[NSNotificationCenter defaultCenter]postNotificationName:kKeyNotificationLocationInit object:nil];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark DDPageControl triggered actions

- (void)pageControlClicked:(id)sender {
    SRPageControl *thePageControl = (SRPageControl *)sender ;
    
    // we need to scroll to the new index
    [scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
}


#pragma mark -
#pragma mark UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
    NSInteger nearestNumber = lround(fractionalPage) ;
    
    if (pageControl.currentPage != nearestNumber)
    {
        pageControl.currentPage = nearestNumber ;
        if ([self.showTooltipOnView isEqualToString: @"SRDiscoveryRadarViewController"]) {
            if (nearestNumber == 1) {
                [pageControl setCenter: CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT - 70.0f)] ;
                [descLbl setFrame:CGRectMake(20, SCREEN_HEIGHT - 180.0f, SCREEN_WIDTH - 40, 140)] ;
                [descLbl setText:[self.titleArr objectAtIndex:nearestNumber]];
            } else {
                [pageControl setCenter: CGPointMake(SCREEN_WIDTH / 2, 120)] ;
                [descLbl setFrame:CGRectMake(20,10, SCREEN_WIDTH - 40, 140)] ;
                if (nearestNumber == 4) {
                    NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:[self.titleArr objectAtIndex:nearestNumber]];
                    NSString *boldString = @"Update Button";
                    NSRange boldRange = [[self.titleArr objectAtIndex:nearestNumber] rangeOfString:boldString];
                    [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:20] range:boldRange];
                    [descLbl setAttributedText: yourAttributedString];
                } else {
                    if (nearestNumber < [self.titleArr count])
                        [descLbl setText:[self.titleArr objectAtIndex:nearestNumber]];
                }
            }
        }
        else if ([self.showTooltipOnView isEqualToString: @"SRDiscoveryListViewController"])
        {
            if (nearestNumber == 0) {
                NSMutableAttributedString *yourAttributedString = [[NSMutableAttributedString alloc] initWithString:[self.titleArr objectAtIndex:nearestNumber]];
                NSString *boldString = @"compass";
                NSRange boldRange = [[self.titleArr objectAtIndex:nearestNumber] rangeOfString:boldString];
                [yourAttributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:20] range:boldRange];
                [descLbl setAttributedText: yourAttributedString];
            } else {
                if (nearestNumber<[self.titleArr count])
                    [descLbl setText:[self.titleArr objectAtIndex:nearestNumber]];
            }
            
            if (nearestNumber == 1) {
                
                [pageControl setCenter: CGPointMake(SCREEN_WIDTH/2, SCREEN_HEIGHT - 70.0f)] ;
                [descLbl setFrame:CGRectMake(20, SCREEN_HEIGHT-180.0f, SCREEN_WIDTH - 40, 140)] ;
            } else {
                [pageControl setCenter: CGPointMake(SCREEN_WIDTH/2, 120)] ;
                [descLbl setFrame:CGRectMake(20,20, SCREEN_WIDTH - 40, 140)] ;                
            }
        }
        else if ([self.showTooltipOnView isEqualToString: @"SRDiscoveryMapViewController"])
        {
            if (nearestNumber<[self.titleArr count])
                [descLbl setText:[self.titleArr objectAtIndex:nearestNumber]];
        }
        
        if ((self.imageArr.count - 1) != 0 && (self.imageArr.count - 1) == nearestNumber) {
            skipBtn.hidden = NO;
            //[pageControl setCenter: CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT + 100.0f)] ;
            skipBtn.frame = CGRectMake(20, SCREEN_HEIGHT - 70, SCREEN_WIDTH - 40, 46);
            //pageControl.backgroundColor = [UIColor redColor];
            [pageControl setCenter: CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT-90.0f)] ;
        } else {
            skipBtn.hidden = YES;
        }
        
        // if we are dragging, we want to update the page control directly during the drag
        if (scrollView.dragging)
            [pageControl updateCurrentPageDisplay] ;
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)aScrollView
{
    // if we are animating (triggered by clicking on the page control), we update the page control
    [pageControl updateCurrentPageDisplay] ;
}


- (void)onTimerEvent:(NSTimer*)timer {
    //self.viewBottomTab.hidden = !self.viewBottomTab.hidden;
}




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait) ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning] ;
}

@end
