//
//  DDPageControlViewController.h
//  DDPageControl
//
//  Created by Damien DeVille on 1/14/11.
//  Copyright 2011 Snappy Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SRPageControl ;

@interface SRPageControlViewController : ParentViewController <UIScrollViewDelegate>
{
	IBOutlet UIScrollView *scrollView ;
	
	SRPageControl *pageControl ;
    UILabel *descLbl;
    UIButton *skipBtn;
    NSTimer *timer;
}

@property (nonatomic,retain) IBOutlet UIScrollView *scrollView ;
@property (strong,nonatomic) NSArray *titleArr,*imageArr;
@property (strong,nonatomic) NSString *showTooltipOnView;
@property (strong,nonatomic) IBOutlet UITabBarItem *radarTab,*mapTab,*listTab;
@property (strong,nonatomic) IBOutlet UITabBar *tooltipTabBar;

@property (nonatomic,retain) IBOutlet UIView *viewBottomTab ;

@end

