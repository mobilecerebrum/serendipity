#import <UIKit/UIKit.h>
#import "SRServerConnection.h"

@interface SRNotificationView : UIView
{
	// Instance variable
	//BOOL isOpen;
	NSMutableArray *updatedNotiList;
}

// Properties
@property (nonatomic) BOOL isOpen;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewCaret;
@property (nonatomic, strong) IBOutlet UIButton *btnNotify;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIImageView *imgNatificationsBG;
@property (nonatomic, strong) IBOutlet UILabel *clearBtn;

@property(nonatomic,strong) NSString    *notifyViewFor;         //For diffrentiate group/Event

@property (nonatomic, strong) NSMutableArray *notificationArr;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnButtonClick:(id)sender;

@end
