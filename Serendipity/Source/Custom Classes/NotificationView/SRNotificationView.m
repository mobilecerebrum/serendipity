#import "SRConnectionNotificationCell.h"
#import "SRNotificationView.h"

@implementation SRNotificationView

@synthesize notifyViewFor;

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
	// Call super
	self = [super initWithFrame:frame];
    updatedNotiList = [[NSMutableArray alloc]init];

	if (self) {
		// Initialization code
		NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRNotificationView" owner:self options:nil];
		self = [nibArray objectAtIndex:0];
		self.frame = frame;
		

		// Register Notifications
		NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
		[defaultCenter addObserver:self
		                  selector:@selector(getNotificationSucceed:)
		                      name:kKeyNotificationGetNotificationSucceed object:nil];
		[defaultCenter addObserver:self
		                  selector:@selector(getNotificationFailed:)
		                      name:kKeyNotificationGetNotificationFailed object:nil];
		[defaultCenter addObserver:self
		                  selector:@selector(updateNotificationSucceed:)
		                      name:kKeyNotificationUpdateNotificationSucceed object:nil];
		[defaultCenter addObserver:self
		                  selector:@selector(updateNotificationFailed:)
		                      name:kKeyNotificationUpdateNotificationFailed object:nil];
        
        self.clearBtn.hidden = YES;
	}

	// Return
	return self;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// dealloc

- (void)dealloc {
    // Unregister the notifications
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnButtonClick:

- (IBAction)actionOnButtonClick:(id)sender {
	if (!_isOpen) {
		self.hidden = NO;
		[UIView animateWithDuration:0.5
		                 animations: ^{
		    CGFloat height = 50 * self.notificationArr.count + 2;
		    if (height > 200) {
		        height = 200;
			}
		    self.frame = CGRectMake(0, 0, self.bounds.size.width, height);
		    self.tblView.frame = CGRectMake(0, 40, self.bounds.size.width, height);
		}];
		_isOpen = YES;
        self.clearBtn.hidden = NO;
		self.imgViewCaret.image = [UIImage imageNamed:@"down.png"];

		// Reload data
        if (self.notificationArr.count > 0) {
            [self.tblView reloadData];
        }
	}
	else {
		[UIView animateWithDuration:0.5
		                 animations: ^{
		    self.frame = CGRectMake(0, 0, self.bounds.size.width, 40);
		    self.tblView.frame = CGRectMake(0, self.btnNotify.frame.origin.y + self.btnNotify.frame.size.height, self.bounds.size.width, 0);
		} completion: ^(BOOL finished) {
		    if ([self.notificationArr count] == 0) {
		        self.hidden = YES;
                self.imgNatificationsBG.hidden = YES;
			}
		    else {
		        NSString *str = [NSString stringWithFormat:@"You have %ld notifications", (unsigned long)self.notificationArr.count];
		        [self.btnNotify setTitle:str forState:UIControlStateNormal];
			}
		}];

		_isOpen = NO;
        self.clearBtn.hidden = YES;
		self.imgViewCaret.image = [UIImage imageNamed:@"caret-down.png"];

//		NSMutableArray *array = [NSMutableArray arrayWithArray:self.notificationArr];
//		for (NSString *notiId in updatedNotiList) {
//			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", kKeyId, notiId];
//			NSArray *filterArr = [self.notificationArr filteredArrayUsingPredicate:predicate];
//			[array removeObject:[filterArr objectAtIndex:0]];
//		}
//
//		// Remove all objects
//		[updatedNotiList removeAllObjects];
//		//self.notificationArr = array;
        
    }
}
-(IBAction)clearBtnSelected:(UIButton *)sender
{
    // Update notifications status
    for (NSUInteger i = 0; i < [self.notificationArr count] && _isOpen; i++) {
        NSDictionary *notifyDict = [self.notificationArr objectAtIndex:i];
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], kKeyStatus, nil];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@", kKeyClassUpdateNotification, [notifyDict objectForKey:kKeyId]];
        [(APP_DELEGATE).server makeAsychronousRequest:strUrl inParams:param isIndicatorRequired:NO inMethodType:kPUT];
    }

}
#pragma mark
#pragma mark TableView Data source Methods
#pragma mark

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfSectionsInTableView:

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return
	return 1;
}

// ---------------------------------------------------------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.notificationArr count];
}

// --------------------------------------------------------------------------------------------------------------------
// cellForRowAtIndexPath:

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell;
	if (cell == nil) {
		NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRConnectionNotificationCell" owner:self options:nil];
		if ([nibObjects count] > 0) {
			SRConnectionNotificationCell *customCell = (SRConnectionNotificationCell *)[nibObjects objectAtIndex:0];
			cell = customCell;
		}
	}
	// Assign values
	NSDictionary *notiDict = [self.notificationArr objectAtIndex:indexPath.row];
	NSDictionary *userDetails = [notiDict objectForKey:kKeySender];
	NSString *fullName = [NSString stringWithFormat:@"%@ %@", [userDetails objectForKey:kKeyFirstName], [userDetails objectForKey:kKeyLastName]];
	NSString *txtStr;
	if ([[notiDict objectForKey:kKeyReference]isKindOfClass:[NSDictionary class]])
    {
		NSDictionary *referenceDict = [notiDict objectForKey:kKeyReference];
        if ([[notiDict valueForKey:kKeyReferenceStatus] isEqualToString:@"0"])
        {
            if ([self.notifyViewFor isEqualToString:kKeyEvent])
            {
                if ([[NSString stringWithFormat:@"%@", [referenceDict valueForKey:kKeyIs_Public]] isEqualToString:@"1"])
                {
                    txtStr = [NSString stringWithFormat:@"%@ has invited you to the public event, %@!" , fullName, [referenceDict objectForKey:kKeyName]];
                }
                else
                {
                    txtStr = [NSString stringWithFormat:@"%@ has invited you to the private event, %@!" , fullName, [referenceDict objectForKey:kKeyName]];
                }
            }
            else
            {
                if ([[NSString stringWithFormat:@"%@", [referenceDict valueForKey:kKeyGroupType]] isEqualToString:@"1"]) {
                    txtStr = [NSString stringWithFormat:@"%@ has invited you to the public group, %@!" , fullName, [referenceDict objectForKey:kKeyName]];
                }
                else
                {
                    txtStr = [NSString stringWithFormat:@"%@ has invited you to the private group, %@!" , fullName, [referenceDict objectForKey:kKeyName]];
                }
            }
        }
        else if ([[NSString stringWithFormat:@"%@", [notiDict valueForKey:kKeyReferenceStatus]] isEqualToString:@"1"])
        {
            txtStr = [self.notifyViewFor isEqualToString:kKeyEvent]?[NSString stringWithFormat:@"%@ has accepted your invitation to join %@", fullName, [referenceDict objectForKey:kKeyName]]:[NSString stringWithFormat:@"%@ has accepted your invitation to join %@", fullName, [referenceDict objectForKey:kKeyName]];
        }
        else
        {
            txtStr = [self.notifyViewFor isEqualToString:kKeyEvent]?[NSString stringWithFormat:@"%@ has declined your invitation to join %@", fullName, [referenceDict objectForKey:kKeyName]]:[NSString stringWithFormat:@"%@ has declined your invitation to join %@", fullName, [referenceDict objectForKey:kKeyName]];
        }
	}
	else {
        txtStr = [self.notifyViewFor isEqualToString:kKeyEvent]?[NSString stringWithFormat:@"%@ added you in event", fullName]:[NSString stringWithFormat:@"%@ added you in group", fullName];
	}

	// text message
	((SRConnectionNotificationCell *)cell).btnNoNotification.hidden = YES;
	((SRConnectionNotificationCell *)cell).btnYesNotification.hidden = YES;

	CGRect frame = ((SRConnectionNotificationCell *)cell).lblNotification.frame;
	frame.size.width = cell.frame.size.width-10;
	((SRConnectionNotificationCell *)cell).lblNotification.frame = frame;
	((SRConnectionNotificationCell *)cell).lblNotification.text = txtStr;
	((SRConnectionNotificationCell *)cell).selectionStyle = UITableViewCellSelectionStyleNone;

	// Return
	return cell;
}

#pragma mark
#pragma mark TableView Delegates Methods
#pragma mark

// ----------------------------------------------------------------------------------
// heightForRowAtIndexPath:

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 50;
}

// --------------------------------------------------------------------------------
// heightForHeaderInSection:

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	CGFloat headerHeight = 1.0;
	if (section == 0) {
		headerHeight = 0.0;
	}

	// Return
	return headerHeight;
}

// --------------------------------------------------------------------------------
// viewForHeaderInSection:

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *headerView = nil;
	if (section > 0) {
		headerView = [[UIView alloc] init];
		headerView.backgroundColor = [UIColor clearColor];
	}

	// Return
	return headerView;
}

// --------------------------------------------------------------------------------
// willDisplayCell:

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	// Remove insets and margins from cells.
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}

	if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
		[cell setPreservesSuperviewLayoutMargins:NO];
	}

	if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
}

#pragma mark
#pragma mark Notification Method
#pragma mark

// --------------------------------------------------------------------------------
// getNotificationSucceed: group as well as event view handled here

- (void)getNotificationSucceed:(NSNotification *)inNotify {
	/*NSArray *array = (APP_DELEGATE).server.notificationArr;

	__block NSArray *filterArr;
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND(reference_type_id == %@)",@"0", [self.notifyViewFor isEqualToString:kKeyEvent]?@"4": @"3"];
		filterArr = [array filteredArrayUsingPredicate:predicate];
		dispatch_async(dispatch_get_main_queue(), ^{
			// Set Title to Notification Button
			if ([filterArr count] == 0) {
			    if (!isOpen) {
			        self.hidden = YES;
				}
			}
			else {
			    if (!isOpen) {
			        self.hidden = NO;
			        self.notificationArr = filterArr;
                    if (self.notificationArr.count > 0)
                    {
                        [self.tblView reloadData];
                    }
                

			        NSString *str = [NSString stringWithFormat:@"You have %ld new notifications", [filterArr count]];
			        [self.btnNotify setTitle:str forState:UIControlStateNormal];
				}
			}
		});
    });*/
    
    if (!_isOpen)
    {
        self.notificationArr = (APP_DELEGATE).server.notificationArr;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@) AND(reference_type_id == %@)",@"0", [self.notifyViewFor isEqualToString:kKeyEvent]?@"4": @"3"];
        self.notificationArr = [self.notificationArr filteredArrayUsingPredicate:predicate];
        
        if ([self.notificationArr count] == 0)
        {
            self.btnNotify.hidden = YES;
            self.imgViewCaret.hidden = YES;
            self.imgNatificationsBG.hidden = YES;
            self.tblView.frame = CGRectMake(0, self.btnNotify.frame.origin.y + self.btnNotify.frame.size.height, self.bounds.size.width, 0);
            self.tblView.hidden = YES;
        }
        else {
            self.hidden=NO;
            self.btnNotify.hidden = NO;
            self.imgNatificationsBG.hidden = NO;
            self.imgViewCaret.hidden = NO;
            [self.tblView reloadData];
            CGFloat height = 50 * self.notificationArr.count + 2;
            if (height > 200) {
                height = 200;
            }
            self.frame = CGRectMake(0, 0, self.bounds.size.width, height);
            self.tblView.frame = CGRectMake(0, 40, self.bounds.size.width, height);
            
            // Set Title to Notification Button
            NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long)[self.notificationArr count]]];
            [self.btnNotify setTitle:str forState:UIControlStateNormal];
        }
    }
}

// --------------------------------------------------------------------------------
// getNotificationFailed:

- (void)getNotificationFailed:(NSNotification *)inNotify {
}

// --------------------------------------------------------------------------------
// updateNotificationSucceed:

- (void)updateNotificationSucceed:(NSNotification *)inNotify {
	NSDictionary *userInfo = [inNotify userInfo];
	NSString *inObjectUrl = [userInfo objectForKey:kKeyObjectUrl];
	NSArray *componentArr  = [inObjectUrl componentsSeparatedByString:@"/"];
    if (componentArr.count>=1)
        [updatedNotiList addObject:[componentArr objectAtIndex:1]];

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [componentArr objectAtIndex:1]];
	NSArray *filterArr = [self.notificationArr filteredArrayUsingPredicate:predicate];

	NSMutableArray *array = [NSMutableArray arrayWithArray:self.notificationArr];
    if (array.count > 0) {
        if (filterArr && filterArr.count > 0) {
            [array removeObject:[filterArr objectAtIndex:0]];
        }
    }
    
    if(array.count>0)
    {
        NSString *str = [NSString stringWithFormat:@"You have %@ notifications", [NSString stringWithFormat:@"%lu", (unsigned long)[array count]]];
        [self.btnNotify setTitle:str forState:UIControlStateNormal];
        self.notificationArr = array;
        [self.tblView reloadData];
    }
    else
    {
        self.btnNotify.hidden = YES;
        self.imgViewCaret.hidden = YES;
        self.imgNatificationsBG.hidden = YES;
        self.tblView.frame = CGRectMake(0, 0, self.bounds.size.width, 0);
        self.tblView.hidden = YES;
        [[NSNotificationCenter defaultCenter]postNotificationName:kkeynotificationHideNotificationView object:nil];
    }

    // Update notification count in menu view
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdatedNotificationCount object:[componentArr objectAtIndex:1] userInfo:nil];

}

// --------------------------------------------------------------------------------
// updateNotificationFailed:

- (void)updateNotificationFailed:(NSNotification *)inNotify {
}

@end
