//
//  HSOutlookSignIn.swift
//  MSALiOS
//
//  Created by Hitesh Surani on 30/06/20.
//  Copyright © 2020 Microsoft. All rights reserved.
//

import UIKit
import MSAL


@objcMembers public class HSOutlookSignIn: NSObject {
    typealias ContactBlock = (Bool,[[String:String]]?,String) -> Void
    
    let kClientID = "b280da64-2406-4aae-b690-247b726e62ef"
    let kRedirectUri = "msauth.com.bv.serendipityApp://auth"
    let kAuthority = "https://login.microsoftonline.com/common"
    let kGraphEndpoint = "https://graph.microsoft.com/"
    let kScopes: [String] = ["user.read","contacts.read"]
    
    var contactBlock:ContactBlock!
    
    var accessToken = String()
    var applicationContext : MSALPublicClientApplication?
    var webViewParamaters : MSALWebviewParameters?
    var currentAccount: MSALAccount?
    var aryAllContact = [[String:Any]]()
    var graphAPIInitURL = "https://graph.microsoft.com/v1.0/me/contacts?$top=100&$skip=0"
    static let shared = HSOutlookSignIn()
    
    private override init() {
        super.init()
        do {
            try self.initMSAL()
        } catch let error {
            self.updateLogging(text: "Unable to create Application Context \(error)")
        }
        
        //        NotificationCenter.default.addObserver(self,
        //                                               selector: #selector(appCameToForeGround(notification:)),
        //                                               name: UIApplication.willEnterForegroundNotification,
        //                                               object: nil)
        
    }
    
    private func resetData(){
        graphAPIInitURL = "https://graph.microsoft.com/v1.0/me/contacts?$top=100&$skip=0"
        aryAllContact.removeAll()
    }
    
    @objc func appCameToForeGround(notification: Notification) {
        self.loadCurrentAccount()
    }
    
    
    private func initMSAL() throws {
        
        guard let authorityURL = URL(string: kAuthority) else {
            self.updateLogging(text: "Unable to create authority URL")
            return
        }
        
        let authority = try MSALAADAuthority(url: authorityURL)
        
        let msalConfiguration = MSALPublicClientApplicationConfig(clientId: kClientID,
                                                                  redirectUri: kRedirectUri,
                                                                  authority: authority)
        self.applicationContext = try MSALPublicClientApplication(configuration: msalConfiguration)
    }
    
    func initWebViewParams(viewController:UIViewController) {
        self.webViewParamaters = MSALWebviewParameters(authPresentationViewController: viewController)
    }
    
    func updateLogging(text : String) {
        print("========== Microsoft Log ==========\n\n\n\n\(text)")
    }
    
    func updateCurrentAccount(account: MSALAccount?) {
        self.currentAccount = account
    }
    
    
    func signOut() {
        self.resetData()
        guard let applicationContext = self.applicationContext else { return }
        
        guard let account = self.currentAccount else { return }
        
        do {
            
            /**
             Removes all tokens from the cache for this application for the provided account
             
             - account:    The account to remove from the cache
             */
            
            let signoutParameters = MSALSignoutParameters(webviewParameters: self.webViewParamaters!)
            signoutParameters.signoutFromBrowser = false
            
            applicationContext.signout(with: account, signoutParameters: signoutParameters, completionBlock: {(success, error) in
                
                if let error = error {
                    self.updateLogging(text: "Couldn't sign out account with error: \(error)")
                    return
                }
                
                self.updateLogging(text: "Sign out completed successfully")
                self.accessToken = ""
                self.updateCurrentAccount(account: nil)
            })
            
        }
    }
}


extension HSOutlookSignIn {
    
    typealias AccountCompletion = (MSALAccount?) -> Void
    
    func loadCurrentAccount(completion: AccountCompletion? = nil) {
        
        guard let applicationContext = self.applicationContext else { return }
        
        let msalParameters = MSALParameters()
        msalParameters.completionBlockQueue = DispatchQueue.main
        
        // Note that this sample showcases an app that signs in a single account at a time
        // If you're building a more complex app that signs in multiple accounts at the same time, you'll need to use a different account retrieval API that specifies account identifier
        // For example, see "accountsFromDeviceForParameters:completionBlock:" - https://azuread.github.io/microsoft-authentication-library-for-objc/Classes/MSALPublicClientApplication.html#/c:objc(cs)MSALPublicClientApplication(im)accountsFromDeviceForParameters:completionBlock:
        
        applicationContext.getCurrentAccount(with: msalParameters, completionBlock: { (currentAccount, previousAccount, error) in
            
            if let error = error {
                self.updateLogging(text: "Couldn't query current account with error: \(error)")
                return
            }
            
            if let currentAccount = currentAccount {
                
                self.updateLogging(text: "Found a signed in account \(String(describing: currentAccount.username)). Updating data for that account...")
                
                self.updateCurrentAccount(account: currentAccount)
                
                
                if let completion = completion {
                    completion(self.currentAccount)
                }
                
                return
            }
            
            self.updateLogging(text: "Account signed out. Updating UX")
            self.accessToken = ""
            self.updateCurrentAccount(account: nil)
            
            if let completion = completion {
                completion(nil)
            }
        })
    }
}


extension HSOutlookSignIn {
    
    /**
     This will invoke the authorization flow.
     */
    
    
    @objc func callGraphAPI(vc:UIViewController,block:@escaping ContactBlock) {
        
        self.contactBlock = block;
        self.aryAllContact.removeAll()
        resetData()
        
        self.initWebViewParams(viewController: vc)
        
        self.loadCurrentAccount { (account) in
            
            guard let currentAccount = account else {
                
                // We check to see if we have a current logged in account.
                // If we don't, then we need to sign someone in.
                self.acquireTokenInteractively()
                return
            }
            
            self.acquireTokenSilently(currentAccount)
        }
    }
    
    func acquireTokenInteractively() {
        
        guard let applicationContext = self.applicationContext else { return }
        guard let webViewParameters = self.webViewParamaters else { return }
        
        let parameters = MSALInteractiveTokenParameters(scopes: kScopes, webviewParameters: webViewParameters)
        parameters.promptType = .selectAccount
        
        applicationContext.acquireToken(with: parameters) { (result, error) in
            
            if let error = error {
                if (error as NSError).code == -50005{
                    self.contactBlock(false,nil,"Cancel")
                }else{
                    self.contactBlock(false,nil,error.localizedDescription)
                }
                return
            }
            
            guard let result = result else {
                self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken
            self.updateLogging(text: "Access token is \(self.accessToken)")
            self.updateCurrentAccount(account: result.account)
            self.getAllContact()
        }
    }
    
    func acquireTokenSilently(_ account : MSALAccount!) {
        
        guard let applicationContext = self.applicationContext else { return }
        
        /**
         
         Acquire a token for an existing account silently
         
         - forScopes:           Permissions you want included in the access token received
         in the result in the completionBlock. Not all scopes are
         guaranteed to be included in the access token returned.
         - account:             An account object that we retrieved from the application object before that the
         authentication flow will be locked down to.
         - completionBlock:     The completion block that will be called when the authentication
         flow completes, or encounters an error.
         */
        
        let parameters = MSALSilentTokenParameters(scopes: kScopes, account: account)
        
        applicationContext.acquireTokenSilent(with: parameters) { (result, error) in
            
            if let error = error {
                
                let nsError = error as NSError
                
                // interactionRequired means we need to ask the user to sign-in. This usually happens
                // when the user's Refresh Token is expired or if the user has changed their password
                // among other possible reasons.
                
                if (nsError.domain == MSALErrorDomain) {
                    
                    if (nsError.code == MSALError.interactionRequired.rawValue) {
                        
                        DispatchQueue.main.async {
                            self.acquireTokenInteractively()
                        }
                        return
                    }
                }
                self.contactBlock(false,nil,error.localizedDescription)
                return
            }
            
            guard let result = result else {
                self.contactBlock(false,nil,"Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken
            self.updateLogging(text: "Refreshed Access token is \(self.accessToken)")
            self.getAllContact()
        }
    }
    
    func getGraphEndpoint() -> String {
        
        return graphAPIInitURL
        
        //        return kGraphEndpoint.hasSuffix("/") ? (kGraphEndpoint + "v1.0/me/contacts") : (kGraphEndpoint + "/v1.0/me/");
    }
    
    /**
     This will invoke the call to the Microsoft Graph API. It uses the
     built in URLSession to create a connection.
     */
    
    func getAllContact() {
        
        // Specify the Graph API endpoint
        let graphURI = getGraphEndpoint()
        let url = URL(string: graphURI)
        var request = URLRequest(url: url!)
        
        // Set the Authorization header for the request. We use Bearer tokens, so we specify Bearer + the token we got from the result
        request.setValue("Bearer \(self.accessToken)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                if let error = error {
                    self.contactBlock(false,nil,error.localizedDescription)
                    return
                }
                
                guard let result = try? JSONSerialization.jsonObject(with: data!, options: []) else {
                    
                    self.contactBlock(false,nil,"Couldn't deserialize result JSON")
                    return
                }
                
                if let dictData = result as? NSDictionary, let aryContact = dictData["value"] as? [[String:Any]]{
                    
                    self.aryAllContact.append(contentsOf: aryContact)
                    if let nextPageUrl = dictData["@odata.nextLink"] as? String, nextPageUrl.count > 0{
                        self.graphAPIInitURL = nextPageUrl
                        self.getAllContact()
                    }else{
                        print("Conntact Count: \(self.aryAllContact.count)")
                        print(self.aryAllContact)
                        
                        var aryFilterContact = [[String:String]]()
                        for contact in self.aryAllContact{
                            let firstName = contact["givenName"] as? String ?? ""
                            let lastName = contact["surname"] as? String ?? ""
                            var mobile =  contact["mobilePhone"] as? String ?? ""
                            mobile = mobile.replacingOccurrences(of: "-", with: "")
                            mobile = mobile.replacingOccurrences(of: "tel:", with: "")
                            
                            if mobile.count > 0{
                                let dict = ["first_name":firstName,
                                            "last_name":lastName,
                                            "mobile_number":mobile
                                ]
                                aryFilterContact.append(dict)
                            }
                        }
                        self.contactBlock(true,aryFilterContact,"You don't have any contacts with mobile number in outlook.")
                    }
                }else{
                    self.contactBlock(true,[],"You don't have any contacts in outlook.")
                }
            }
        }.resume()
    }
}
