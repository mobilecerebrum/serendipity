//
//  HSProfileImageView.h
//  Serendipity
//
//  Created by Hitesh Surani on 22/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRBluredImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface HSProfileImageView : SRBluredImage

@end

NS_ASSUME_NONNULL_END
