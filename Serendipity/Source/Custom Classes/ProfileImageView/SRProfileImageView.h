#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface SRProfileImageView : UIImageView
@property (nonatomic) IBInspectable BOOL useBlur;
-(void)blurImage;
@end
