//
//  HSProfileImageView.m
//  Serendipity
//
//  Created by Hitesh Surani on 22/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "HSProfileImageView.h"

@implementation HSProfileImageView

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        if ([[(APP_DELEGATE).server.loggedInUserInfo objectForKey:kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *imageDict = [(APP_DELEGATE).server.loggedInUserInfo objectForKey:kKeyProfileImage];
            if ([imageDict isKindOfClass:[NSDictionary class]] && [imageDict objectForKey:kKeyImageName] != nil) {
                
                NSString *imageName = [imageDict objectForKey:kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityStorageServer, kKeyUserProfileImage, imageName];
                    
                    [self sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"deault_profile_bck.png"]];
                } else {
                    self.image = [UIImage imageNamed:@"deault_profile_bck.png"];
                }
                
                self.image = [self blurImage:self.image];
            }
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateProfileImage:)
                                                     name:kKeyNotificationProfileImageCached
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
