#import <UIKit/UIKit.h>

@interface SRFamilyAddressView : UIView

// Properties
@property (weak, nonatomic) IBOutlet UITextField *txtFldAddType;
@property (weak, nonatomic) IBOutlet UITextField *txtFldAddInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgClose;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (strong, nonatomic) NSDictionary *addresDict;
@property (assign, nonatomic) CLLocationCoordinate2D location;

@end
