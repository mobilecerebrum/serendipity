#import "SRFamilyAddressView.h"

@implementation SRFamilyAddressView

#pragma mark
#pragma mark - Init Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
	// Call super
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRFamilyAddressView" owner:self options:nil];
		self = [nibArray objectAtIndex:0];
		self.frame = frame;

		// Make view round corner
		self.txtFldAddType.layer.cornerRadius = 5.0;
		self.txtFldAddInfo.layer.cornerRadius = 5.0;
        self.imgClose.layer.cornerRadius = self.imgClose.frame.size.width / 2.0;
        self.imgClose.layer.masksToBounds = YES;
	}

	// Return
	return self;
}

@end
