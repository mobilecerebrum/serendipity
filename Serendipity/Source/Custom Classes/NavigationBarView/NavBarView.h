//
//  NavBarView.h
//  Serendipity
//
//  Created by Rahul Patel on 12/06/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NavBarView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@end

NS_ASSUME_NONNULL_END
