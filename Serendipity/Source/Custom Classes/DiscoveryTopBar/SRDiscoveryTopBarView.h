#import <UIKit/UIKit.h>
#import "SRDropDownListView.h"
#import "SRHomeParentViewController.h"

@interface SRDiscoveryTopBarView : UIView<UITableViewDataSource, UITableViewDelegate>
{
    BOOL isGroupType,isConnection,isTrackUsers;
    SRServerConnection *server;
    NSArray *nearbyUsers;
    
    NSMutableArray *tempArray;
    NSMutableArray *sourceArr;
    NSMutableArray *userArr;

}

// Properties
@property (nonatomic, strong) IBOutlet UIImageView *bckBarImgView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewConnections;
@property (weak, nonatomic) IBOutlet UIButton *btnConnIcon;

@property (strong, nonatomic) IBOutlet UILabel *lblConnections;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewConnectionsDropDown;
@property (strong, nonatomic) IBOutlet UIButton *btnConnection;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewCommunity;
@property (weak, nonatomic) IBOutlet UIButton *btnCommIcon;

@property (strong, nonatomic) IBOutlet UILabel *lblCommunity;
@property (strong, nonatomic) IBOutlet UIButton *btnCommunity;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCommunityDropDown;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewTrack;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewTrackDropDown;
@property (weak, nonatomic) IBOutlet UIButton *btnTrackIcon;

@property (strong, nonatomic) IBOutlet UILabel *lblTrack;
@property (strong, nonatomic) IBOutlet UIButton *btnTrack;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewGroup;
@property (weak, nonatomic) IBOutlet UIButton *btnGrpIcon;

@property (strong, nonatomic) IBOutlet UILabel *lblGroup;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewGroupDropDown;
@property (strong, nonatomic) IBOutlet UIButton *btnGroup;

@property (strong, nonatomic) SRDropDownListView *dropDownListView;
@property (assign, nonatomic) NSInteger selectedDegree;
@property (retain, nonatomic) SRHomeParentViewController *parent;
@property (assign, nonatomic) NSInteger selectedConnection;
@property (assign, nonatomic) NSInteger selectedTrackUser;

@property(nonatomic, strong) NSMutableArray *groupUsers;

#pragma mark
#pragma mark - IBAction Method
#pragma mark

- (IBAction)actionOnTopBarBtnClick:(UIButton *)sender;
- (void)actionOnCommunityViewBtnClick:(UIButton *)sender;

- (void) removeAllDropDownFromSuperview;
-(void) refershGroups;

@end
