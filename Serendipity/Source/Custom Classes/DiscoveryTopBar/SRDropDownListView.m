#import "SRDropDownListView.h"

@implementation SRDropDownListView

#pragma mark - Init Method

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRDropDownListView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
        self.frame = frame;
    }
    return self;
}

@end
