#import <UIKit/UIKit.h>

@interface SRDropDownListView : UIView

@property (strong, nonatomic) IBOutlet UIButton *CommunityViewBtn;
@property (strong, nonatomic) IBOutlet UILabel *CommunityViewLbl;
@property (strong, nonatomic) IBOutlet UIImageView *CommunityViewDropDwnImgView;
@property (strong, nonatomic) IBOutlet UIImageView *CommunityViewImgView;
@property (strong, nonatomic) IBOutlet UITableView *dropDownTbl;

@end
