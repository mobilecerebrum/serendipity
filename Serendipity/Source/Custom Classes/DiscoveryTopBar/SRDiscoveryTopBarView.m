#import "SRDiscoveryCustomCell.h"
#import "SRDiscoveryTopBarView.h"
#import "SRModalClass.h"

@implementation SRDiscoveryTopBarView
@synthesize groupUsers;

#pragma mark
#pragma mark - Init Method
#pragma mark

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:[APP_DELEGATE isUserBelow18] ? @"SRDiscoveryTopBarViewBelow18" : @"SRDiscoveryTopBarView" owner:self options:nil];
        self = nibArray[0];
        self.frame = frame;
    }
    [self setTopBarView];
    
    return self;
}

- (void)setTopBarView {
    groupUsers = [[NSMutableArray alloc] init];
    //jonish sprint 2
    _selectedDegree = 1;
    //jonish sprint 2
    // Initialization code
    self.dropDownListView = [[SRDropDownListView alloc] initWithFrame:CGRectMake(self.btnCommunity.frame.origin.x, 65, self.btnCommunity.frame.size.width, 50)];
    [self.dropDownListView.CommunityViewBtn addTarget:self action:@selector(actionOnCommunityViewBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    self.dropDownListView.hidden = YES;
    
    // Customize look and fill
    self.imgViewConnections.layer.cornerRadius = self.imgViewConnections.frame.size.width / 2;
    self.imgViewCommunity.layer.cornerRadius = self.imgViewCommunity.frame.size.width / 2.0;
    self.imgViewGroup.layer.cornerRadius = self.imgViewGroup.frame.size.width / 2;
    self.imgViewTrack.layer.cornerRadius = self.imgViewTrack.frame.size.width / 2;
    
    // Set selected defualt values
    [self.btnConnection setSelected:YES];
    //[self.btnConnection setSelected:[APP_DELEGATE isUserBelow18]?YES:NO];
    // [self.btnCommunity setSelected:[APP_DELEGATE isUserBelow18]?NO:YES];
    [self.btnCommunity setSelected:NO];
    [self.btnTrack setSelected:NO];
    [self.btnGroup setSelected:NO];
    
    //Set Selected BG image
    //    self.imgViewConnections.backgroundColor=[APP_DELEGATE isUserBelow18]?[UIColor colorWithRed:0 green:0 blue:0 alpha:0.66]:[UIColor clearColor];
    //    self.imgViewCommunity.backgroundColor=[APP_DELEGATE isUserBelow18]?[UIColor clearColor]:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.66];
    self.imgViewConnections.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.66];
    self.imgViewCommunity.backgroundColor = [UIColor clearColor];
    self.imgViewTrack.backgroundColor = [UIColor clearColor];
    self.imgViewGroup.backgroundColor = [UIColor clearColor];
    
    
    //Set images for selection btn
    [self.btnConnIcon setImage:[UIImage imageNamed:@"navibar-connection"] forState:UIControlStateNormal];
    [self.btnConnIcon setImage:[UIImage imageNamed:@"navibar-connection-active"] forState:UIControlStateSelected];
    
    [self.btnCommIcon setImage:[UIImage imageNamed:@"navibar-community"] forState:UIControlStateNormal];
    [self.btnCommIcon setImage:[UIImage imageNamed:@"navibar-community-active"] forState:UIControlStateSelected];
    
    [self.btnTrackIcon setImage:[UIImage imageNamed:@"mainmenu-tracking"] forState:UIControlStateNormal];
    [self.btnTrackIcon setImage:[UIImage imageNamed:@"tracking-active"] forState:UIControlStateSelected];
    
    [self.btnGrpIcon setImage:[UIImage imageNamed:@"navibar-group"] forState:UIControlStateNormal];
    [self.btnGrpIcon setImage:[UIImage imageNamed:@"navibar-group-active"] forState:UIControlStateSelected];
    
    //    if([APP_DELEGATE isUserBelow18])
    //    {
    self.btnConnIcon.selected = YES;
    (APP_DELEGATE).topBarView.btnCommunity.selected = NO;
    (APP_DELEGATE).topBarView.btnConnection.selected = YES;
    (APP_DELEGATE).topBarView.btnTrack.selected = NO;
    (APP_DELEGATE).topBarView.btnGroup.selected = NO;
    
    //First show active users only.
    NSDictionary *params = @{kKeyConnection: @4, kKeyStatus: @((APP_DELEGATE).topBarView.btnConnection.selected)};
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
    //   }
    //    else{
    //
    //        self.btnCommIcon.selected=YES;
    //        (APP_DELEGATE).topBarView.btnCommunity.selected = YES;
    //        (APP_DELEGATE).topBarView.btnConnection.selected = NO;
    //        (APP_DELEGATE).topBarView.btnTrack.selected = NO;
    //        (APP_DELEGATE).topBarView.btnGroup.selected = NO;
    //    }
    
    
    // Register notification
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(ShowGroupSucceed:)
                          name:kKeyNotificationCreateGroupSucceed object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(ShowGroupFailed:)
                          name:kKeyNotificationCreateGroupFailed object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(hideTopBarDropMenu)
                          name:kKeyNotificationHideTopDropDown object:nil];
    self.dropDownListView.dropDownTbl.contentInset = UIEdgeInsetsMake(0, 0, 80, 0);
    
    _selectedConnection = 1;
}

- (void)dealloc {
    // Dealloc all register notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark
#pragma mark - IBAction Method
#pragma mark

// -----------------------------------------------------------------------------------------------
// actionOnTopBarBtnClick:

- (IBAction)actionOnTopBarBtnClick:(UIButton *)sender {
    (APP_DELEGATE).isTrackUserSelected = NO;
    // Create table
    if (sender.tag == 1) {
        self.dropDownListView.dropDownTbl.delegate = self;
        self.dropDownListView.dropDownTbl.dataSource = self;
        
        (APP_DELEGATE).topBarView.btnCommunity.selected = NO;
        (APP_DELEGATE).topBarView.btnConnection.selected = YES;
        (APP_DELEGATE).topBarView.btnTrack.selected = NO;
        (APP_DELEGATE).topBarView.btnGroup.selected = NO;
        
        //Set Selected BG image
        self.btnConnIcon.selected = YES;
        self.btnCommIcon.selected = NO;
        self.btnTrackIcon.selected = NO;
        self.btnGrpIcon.selected = NO;
        
        isConnection = YES;
        isGroupType = NO;
        isTrackUsers = NO;
        
        self.imgViewConnections.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.66];
        self.imgViewCommunity.backgroundColor = [UIColor clearColor];
        self.imgViewTrack.backgroundColor = [UIColor clearColor];
        self.imgViewGroup.backgroundColor = [UIColor clearColor];
        
        
        self.lblCommunity.hidden = NO;
        self.imgViewCommunityDropDown.hidden = NO;
        self.lblGroup.hidden = NO;
        self.imgViewGroupDropDown.hidden = NO;
        self.lblConnections.hidden = YES;
        self.imgViewConnectionsDropDown.hidden = YES;
        self.lblTrack.hidden = NO;
        self.imgViewTrackDropDown.hidden = NO;
        
        self.dropDownListView.CommunityViewImgView.image = [UIImage imageNamed:@"navibar-connection-active.png"];
        self.dropDownListView.CommunityViewLbl.text = NSLocalizedString(@"lbl.Connections.txt", @"");
        self.dropDownListView.CommunityViewLbl.adjustsFontSizeToFitWidth = YES;
        self.dropDownListView.CommunityViewDropDwnImgView.image = [UIImage imageNamed:@"ic_caret_up.png"];
        [self.dropDownListView removeFromSuperview];
        self.dropDownListView.frame = CGRectZero;
        
        int x;
        if ([APP_DELEGATE isUserBelow18]) {
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnConnection.frame.origin.x - 9);
            } else {
                //                x = (int) (self.btnConnection.frame.origin.x - 7);
                x = (int) (self.btnConnection.frame.origin.x - 6);
            }
            x = (int) (self.btnConnection.frame.origin.x);

        } else {
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnConnection.frame.origin.x + 0);
            } else {
                x = (int) (self.btnConnection.frame.origin.x + 1);
            }
        }
        
        
        self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
        self.dropDownListView.hidden = NO;
        [(APP_DELEGATE).tabBarController.view addSubview:self.dropDownListView];
        [(APP_DELEGATE).tabBarController.view bringSubviewToFront:self.dropDownListView];
//        int buttonWidth = self.btnConnection.frame.size.width;
        int numberOfItemsInDropdown = 4;
        [UIView animateWithDuration:0.5
                         animations:^{
            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 78 * numberOfItemsInDropdown );
        }];
        [self.dropDownListView.dropDownTbl reloadData];
        
        //        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"prevConnectionSelection"] boolValue]) {
        //            NSDictionary *params = @{kKeyConnection: @3, kKeyStatus: @1};
        //            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
        //            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
        //        } else {
        //            NSDictionary *params = @{kKeyConnection: @2, kKeyStatus: @1};
        //            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
        //            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
        //        }
//        NSDictionary *params = @{kKeyConnection: @(_selectedConnection + 3), kKeyStatus: @(_selectedConnection + 1)};
//        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
        // To reset filter values save current filter value as previousConnection value
        [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"prevConnectionSelection"] forKey:@"previousConnection"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if (sender.tag == 2) {
        self.dropDownListView.dropDownTbl.delegate = self;
        self.dropDownListView.dropDownTbl.dataSource = self;
        
        (APP_DELEGATE).topBarView.btnGroup.selected = NO;
        (APP_DELEGATE).topBarView.btnCommunity.selected = YES;
        (APP_DELEGATE).topBarView.btnConnection.selected = NO;
        (APP_DELEGATE).topBarView.btnTrack.selected = NO;
        
        //Set Selected BG image
        self.btnConnIcon.selected = NO;
        self.btnCommIcon.selected = YES;
        self.btnTrackIcon.selected = NO;
        self.btnGrpIcon.selected = NO;
        
        isConnection = NO;
        isGroupType = NO;
        isTrackUsers = NO;
        
        self.imgViewConnections.backgroundColor = [UIColor clearColor];
        self.imgViewCommunity.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.66];
        self.imgViewTrack.backgroundColor = [UIColor clearColor];
        self.imgViewGroup.backgroundColor = [UIColor clearColor];
        self.imgViewConnections.image = nil;
        
        self.lblCommunity.hidden = YES;
        self.imgViewCommunityDropDown.hidden = YES;
        self.lblGroup.hidden = NO;
        self.imgViewGroupDropDown.hidden = NO;
        self.lblConnections.hidden = NO;
        self.imgViewConnectionsDropDown.hidden = NO;
        self.lblTrack.hidden = NO;
        self.imgViewTrackDropDown.hidden = NO;
        
        
        self.dropDownListView.CommunityViewImgView.image = [UIImage imageNamed:@"navibar-community-active.png"];
        self.dropDownListView.CommunityViewLbl.text = NSLocalizedString(@"lbl.community.txt", @"");
        self.dropDownListView.CommunityViewDropDwnImgView.image = [UIImage imageNamed:@"ic_caret_up.png"];
        
        // To reset filter values save current filter value as previousDegree value
        [[NSUserDefaults standardUserDefaults] setValue:@((APP_DELEGATE).topBarView.selectedDegree) forKey:@"previousDegree"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        int x;
        if ([APP_DELEGATE isUserBelow18]){
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnCommunity.frame.origin.x);
            } else {
                x = (int) (self.btnCommunity.frame.origin.x);
            }
            x = sender.tag*(SCREEN_WIDTH/3) + x;
        }else{
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnCommunity.frame.origin.x + 0);
            } else {
                x = (int) (self.btnCommunity.frame.origin.x + 1);
            }}
        self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
        self.dropDownListView.hidden = NO;
        [(APP_DELEGATE).tabBarController.view addSubview:self.dropDownListView];
        [(APP_DELEGATE).tabBarController.view bringSubviewToFront:self.dropDownListView];
        
        int buttonWidth = self.btnCommunity.frame.size.width;
        int numberOfItemsInDropdown = 3;
        
        [UIView animateWithDuration:0.5
                         animations:^{
            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], buttonWidth, buttonWidth * numberOfItemsInDropdown - 10);
        }];
        
        [self.dropDownListView.dropDownTbl reloadData];
    } else if (sender.tag == 3) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [defaults objectForKey:@"trackUserArray"];
        NSMutableArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [defaults synchronize];
        (APP_DELEGATE).trackUserArray = myArray;
        (APP_DELEGATE).isTrackUserSelected = YES;
        self.dropDownListView.dropDownTbl.delegate = self;
        self.dropDownListView.dropDownTbl.dataSource = self;
        
        //Set filter state
        (APP_DELEGATE).topBarView.btnGroup.selected = NO;
        (APP_DELEGATE).topBarView.btnCommunity.selected = NO;
        (APP_DELEGATE).topBarView.btnConnection.selected = NO;
        (APP_DELEGATE).topBarView.btnTrack.selected = YES;
        
        //Set Selected BG image
        self.btnConnIcon.selected = NO;
        self.btnCommIcon.selected = NO;
        self.btnTrackIcon.selected = YES;
        self.btnGrpIcon.selected = NO;
        
        self.imgViewConnections.backgroundColor = [UIColor clearColor];
        self.imgViewCommunity.backgroundColor = [UIColor clearColor];
        self.imgViewTrack.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.66];
        self.imgViewGroup.backgroundColor = [UIColor clearColor];
        
        self.lblConnections.hidden = NO;
        self.imgViewConnectionsDropDown.hidden = NO;
        self.lblCommunity.hidden = NO;
        self.imgViewCommunityDropDown.hidden = NO;
        self.lblGroup.hidden = NO;
        self.imgViewGroupDropDown.hidden = NO;
        self.lblTrack.hidden = YES;
        self.imgViewTrackDropDown.hidden = YES;
        
        isGroupType = NO;
        isConnection = NO;
        isTrackUsers = YES;
        
        self.dropDownListView.CommunityViewImgView.image = [UIImage imageNamed:@"tracking-active.png"];
        self.dropDownListView.CommunityViewLbl.text = NSLocalizedString(@"lbl.tracUsers.txt", @"");
        self.dropDownListView.CommunityViewDropDwnImgView.image = [UIImage imageNamed:@"ic_caret_up.png"];
        self.dropDownListView.CommunityViewLbl.adjustsFontSizeToFitWidth = YES;
        
        //        int x;
        //        if (SCREEN_WIDTH == 320) {
        //            x = (int) (self.btnTrack.frame.origin.x + 0);
        //        } else
        //            x = (int) (self.btnTrack.frame.origin.x + 1);
        //
        
        
        int x;
        if ([APP_DELEGATE isUserBelow18]) {
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnTrack.frame.origin.x);
            } else {
                x = (int) (self.btnTrack.frame.origin.x);
            }
            x = (SCREEN_WIDTH/3) + x;
        } else {
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnTrack.frame.origin.x + 0);
            } else {
                x = (int) (self.btnTrack.frame.origin.x + 1);
            }
        }
        
        
        NSMutableArray *filteredTrackUsers = [[NSMutableArray alloc] init];
        [sourceArr enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
            if ( [[obj objectForKey:@"enable_tracking"] boolValue]) {
                [filteredTrackUsers addObject:obj];
            }
        }];
        
        userArr = (APP_DELEGATE).trackUserArray;
        //        if ((APP_DELEGATE).trackUserArray.count == 0) {
        //            exit(0);
        //        }
        self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
        self.dropDownListView.hidden = NO;
        int height = 160;
        int buttonWidth = self.btnTrack.frame.size.width;
        
        if ([userArr count] > 0  && [userArr count] < 4 ){
            height = (int) (height + buttonWidth * [userArr count]);
        }
        if ([userArr count] >= 4){
            height = height + buttonWidth * 5;
        }
        [(APP_DELEGATE).tabBarController.view addSubview:self.dropDownListView];
        [(APP_DELEGATE).tabBarController.view bringSubviewToFront:self.dropDownListView];
        [UIView animateWithDuration:0.5
                         animations:^{
            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, height);
        }];
        
        [self.dropDownListView.dropDownTbl reloadData];
        
//        if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"] boolValue] && [[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"]) {
//            NSDictionary *params = @{kKeyGroupTag: @"0", kKeyGroupType: @"All Connections"};
//            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
//        }
//        //        else if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"prevGrpTrackSelection"] boolValue])
//        //        {
//        //            [[NSUserDefaults standardUserDefaults]synchronize];
//        //            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys: @"1", kKeyGroupTag, @"Family", kKeyGroupType, nil];
//        //            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
//        //            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
//        //        }
//        else {
//            NSDictionary *params = @{kKeyGroupTag: @"2", kKeyGroupType: @"All Connections"};
//            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
//            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
//        }
//        NSDictionary *params = @{kKeyGroupTag: @"2", kKeyGroupType: @"All Connections"};
//        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
//        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRotateMap object:nil];
        // To reset filter values save current filter value as previousDegree value
        [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"] forKey:@"previousTracking"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    } else if (sender.tag == 4) {
        
        //Set filter state
        (APP_DELEGATE).topBarView.btnGroup.selected = YES;
        (APP_DELEGATE).topBarView.btnCommunity.selected = NO;
        (APP_DELEGATE).topBarView.btnConnection.selected = NO;
        (APP_DELEGATE).topBarView.btnTrack.selected = NO;
        
        //Set Selected BG image
        self.btnConnIcon.selected = NO;
        self.btnCommIcon.selected = NO;
        self.btnTrackIcon.selected = NO;
        self.btnGrpIcon.selected = YES;
        
        self.imgViewConnections.backgroundColor = [UIColor clearColor];
        self.imgViewCommunity.backgroundColor = [UIColor clearColor];
        self.imgViewTrack.backgroundColor = [UIColor clearColor];
        self.imgViewGroup.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.66];
        
        self.lblConnections.hidden = NO;
        self.imgViewConnectionsDropDown.hidden = NO;
        self.lblCommunity.hidden = NO;
        self.imgViewCommunityDropDown.hidden = NO;
        self.lblGroup.hidden = NO;
        self.imgViewGroupDropDown.hidden = NO;
        self.lblTrack.hidden = NO;
        self.imgViewTrackDropDown.hidden = NO;
        
        isGroupType = YES;
        isConnection = NO;
        isTrackUsers = NO;
        
        self.dropDownListView.CommunityViewImgView.image = [UIImage imageNamed:@"navibar-group-active.png"];
        self.dropDownListView.CommunityViewLbl.text = NSLocalizedString(@"lbl.groups.txt", @"");
        self.dropDownListView.CommunityViewDropDwnImgView.image = [UIImage imageNamed:@"ic_caret_up.png"];
        [self.dropDownListView removeFromSuperview];
        self.dropDownListView.frame = CGRectZero;
        
        if (groupUsers.count > 0 && sourceArr.count > 0) {
            self.btnGroup.userInteractionEnabled = YES;
            // Display the dots
            self.lblGroup.hidden = YES;
            self.imgViewGroupDropDown.hidden = YES;
            
            int x;
            if ([APP_DELEGATE isUserBelow18]) {
                if (SCREEN_WIDTH == 320) {
                    x = (int) (self.btnGroup.frame.origin.x - 9);
                } else {
                    x = (int) (self.btnGroup.frame.origin.x-8);
                }
                x = (int) self.btnGroup.frame.origin.x;
                x = 2*(SCREEN_WIDTH/3) + x;
            }else{
                if (SCREEN_WIDTH == 320) {
                    x = (int) (sender.frame.origin.x - 0);
                } else
                    x = (int) (sender.frame.origin.x + 0);
            }
            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
            [(APP_DELEGATE).tabBarController.view addSubview:self.dropDownListView];
            
            NSInteger rowHeight = 80;
            if(sourceArr.count == 0){
                rowHeight = 0;
            }

            [UIView animateWithDuration:0.5 animations:^{
                NSInteger height = (sourceArr.count + 1) * rowHeight;
                if (height > 400){
                    height = 400;
                }
                self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, height);
            }];
            self.dropDownListView.hidden = NO;
            self.dropDownListView.dropDownTbl.delegate = self;
            self.dropDownListView.dropDownTbl.dataSource = self;
            [self.dropDownListView.dropDownTbl reloadData];
        } else {
            [self makeServerCallToGetGroupList];
        }
    }
    //jonish sprint 2
    //if (sender.tag != 1) {
       // self.selectedDegree = 1;
    //}
    //jonish sprint 2
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateTimer object:nil];
}

- (void) removeAllDropDownFromSuperview {
    self.dropDownListView.frame = CGRectMake(-80, self.dropDownListView.frame.origin.y, 80, 50);
    self.dropDownListView.dropDownTbl.removeFromSuperview;
}

- (void)refershGroups {
    (APP_DELEGATE).topBarView.btnGroup.selected = YES;
    (APP_DELEGATE).topBarView.btnCommunity.selected = NO;
    (APP_DELEGATE).topBarView.btnConnection.selected = NO;
    (APP_DELEGATE).topBarView.btnTrack.selected = NO;
    [groupUsers removeAllObjects];
    [sourceArr removeAllObjects];
    [self makeServerCallToGetGroupList];
}

- (void)makeServerCallToGetGroupList {
    [APP_DELEGATE showActivityIndicator];
    NSString *latitude = [NSString localizedStringWithFormat:@"%f", (APP_DELEGATE).server.myLocation.coordinate.latitude];
    NSString *longitude = [NSString localizedStringWithFormat:@"%f", (APP_DELEGATE).server.myLocation.coordinate.longitude];
    NSInteger distance = 12500;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@?distance=%@&lat=%@&lon=%@&only_user_groups=1", kKeyClassCreateGroup, [NSString stringWithFormat:@"%ldmi", (long) distance], latitude, longitude];
    [(APP_DELEGATE).server makeAsychronousRequest:urlStr inParams:nil isIndicatorRequired:NO inMethodType:kGET];
}

- (void)actionOnCommunityViewBtnClick:(UIButton *)sender {
    //   sender.backgroundColor = [UIColor clearColor];
    [UIView animateWithDuration:0.5 animations:^{
        // To reset filter values save current filter value
        [[NSUserDefaults standardUserDefaults] setValue:@((APP_DELEGATE).topBarView.selectedDegree) forKey:@"recentDegree"];
        [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"prevConnectionSelection"] forKey:@"recentConnection"];
        [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"] forKey:@"recentTracking"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.dropDownListView removeFromSuperview];
        self.dropDownListView.frame = CGRectZero;
        int x;
        if (isConnection) {
            if ([APP_DELEGATE isUserBelow18]) {
                if (SCREEN_WIDTH == 320) {
                    x = (int) (self.btnConnection.frame.origin.x - 9);
                } else{
                    x = (int) (self.btnConnection.frame.origin.x - 7);
                }
                x = (int) (self.btnConnection.frame.origin.x);
            } else {
                if (SCREEN_WIDTH == 320) {
                    x = (int) (self.btnConnection.frame.origin.x - 0);
                } else
                    x = (int) (self.btnConnection.frame.origin.x + 1);
            }
            self.dropDownListView.frame = CGRectMake(x, self.dropDownListView.frame.origin.y, sender.frame.size.width, 50);
            
            // if recentDegree and previousDegree is same then reset degree filter and show all user on radar
            //            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"previousConnection"] isEqual:[[NSUserDefaults standardUserDefaults] valueForKey:@"recentConnection"]]) {
            //                NSDictionary *params = @{kKeyConnection: @(_selectedConnection + 3), kKeyStatus: @(_selectedConnection + 1)};
            //                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
            //                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
            //                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"prevConnectionSelection"];
            //                [[NSUserDefaults standardUserDefaults] synchronize];
            //            }
        } else if (isTrackUsers) {
            if (SCREEN_WIDTH == 320) {
                x = (int) (self.btnTrack.frame.origin.x - 0);
            } else
                x = self.btnTrack.frame.origin.x + 1;
            
            //            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
            
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"previousTracking"] isEqual:[[NSUserDefaults standardUserDefaults] valueForKey:@"recentTracking"]]) {
                NSDictionary *params = @{kKeyGroupTag: @"2", kKeyGroupType: @"All Connections"};
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"prevGrpTrackSelection"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        } else if (!isGroupType) {
            if (SCREEN_WIDTH == 320) {
                x = self.btnCommunity.frame.origin.x + 0;
            } else
                x = self.btnCommunity.frame.origin.x + 1;
            
            //            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
            
            // if recentDegree and previousDegree is same then reset degree filter and show all user on radar
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"recentDegree"] isEqual:[[NSUserDefaults standardUserDefaults] valueForKey:@"previousDegree"]]) {
                (APP_DELEGATE).topBarView.selectedDegree = -1;
                NSDictionary *dict = @{kKeyDegree: @((APP_DELEGATE).topBarView.selectedDegree)};
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:dict];
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:dict];
            }
        } else {
            if (SCREEN_WIDTH == 320) {
                x = self.btnGroup.frame.origin.x - 0;
            } else
                x = self.btnGroup.frame.origin.x - 0;
            
            self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], sender.frame.size.width, 50);
        }
        
    }                completion:^(BOOL finished) {
        [self.dropDownListView removeFromSuperview];
        self.dropDownListView.frame = CGRectZero;
        if (isConnection) {
            self.lblConnections.hidden = NO;
            self.imgViewConnectionsDropDown.hidden = NO;
        } else if (isTrackUsers) {
            _selectedConnection = 1;
            self.lblTrack.hidden = NO;
            self.imgViewTrackDropDown.hidden = NO;
        } else if (!isGroupType) {
            _selectedConnection = 1;
            self.lblCommunity.hidden = NO;
            self.imgViewCommunityDropDown.hidden = NO;
        } else {
            _selectedConnection = 1;
            self.lblGroup.hidden = NO;
            self.imgViewGroupDropDown.hidden = NO;
        }
    }];
}

- (void)communityBtnActions:(UIButton *)sender {
    //jonish sprint 2
    (APP_DELEGATE).topBarView.selectedDegree = sender.tag;
    self.selectedDegree = sender.tag;
    //jonish sprint 2
    //Hide grop dowin if present
    [self actionOnCommunityViewBtnClick:nil];
    
    NSInteger tag = sender.tag;
    NSDictionary *dict = @{kKeyDegree: @(tag)};
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:dict];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:dict];
    
    // Reload table
    [self.dropDownListView.dropDownTbl reloadData];
}

#pragma mark - GroupTable Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// --------------------------------------------------------------------------------
// numberOfRowsInSection:

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count;
    if (isConnection)
        count = 3;
    else if (isTrackUsers){
        count = 1;
        if ([userArr count] > 0) {
            count = count + [userArr count];
        }
        
    } else if (!isGroupType)
        count = 2;
    else
        count = sourceArr.count;
    //[sourceArr count]>4?4:sourceArr.count;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell1";
    SRDiscoveryCustomCell *cell = (SRDiscoveryCustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SRDiscoveryCustomCell" owner:self options:nil];
    if ([nibObjects count] > 0) {
        cell = (SRDiscoveryCustomCell *) nibObjects[0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // Check If its community/connection or group
    if (isConnection) {
        cell.communityBtn.hidden = NO;
        cell.GroupImage.hidden = YES;
        cell.GroupLbl.hidden = NO;
        
        [cell.communityBtn setBackgroundColor:[UIColor clearColor]];
        //
        CGRect frame = cell.communityBtn.frame;
        CGFloat marginTop = 15;
        if (indexPath.row == 0) {
            frame.origin.y = 0;
            marginTop = 15;
        } else if (indexPath.row == 1){
            frame.origin.y = - 10 ;
            marginTop = 25;
        } else {
            frame.origin.y = - 15 ;
            marginTop = 40;
        }
        //            frame.origin.y = frame.origin.y - 15;
        cell.communityBtn.frame = frame;
        //        cell.communityBtn.frame = cell.GroupImage.frame;
        
        
        CGRect frame1 = cell.GroupLbl.frame;
        //        frame1.origin.y = -40;
        
        //        if (indexPath.row == 0) {
        //            frame1.origin.y = 45;
        //        }
        /*
         else
         frame1.origin.y = 60;
         cell.GroupLbl.frame = frame1;
         
         */
        //        cell.communityBtn.frame = cell.GroupImage.frame;
        
        cell.GroupLbl.frame = CGRectMake(cell.GroupLbl.frame.origin.x + 38 , cell.GroupLbl.frame.origin.y - marginTop, _dropDownListView.frame.size.width, cell.GroupLbl.frame.size.height);
        cell.communityBtn.layer.masksToBounds = YES;
        cell.communityBtn.contentMode = UIViewContentModeScaleAspectFit;
        [cell.communityBtn setBackgroundColor:[UIColor clearColor]];
        cell.GroupLbl.numberOfLines = 1;
        cell.GroupLbl.textAlignment = UITextAlignmentCenter;
        if (self.selectedConnection == indexPath.row){
            [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            
        }
        if (indexPath.row == 2)
        {
            cell.GroupLbl.text = NSLocalizedString(@"lbl.Favourites.txt", @"");
            [cell.communityBtn setImage:[UIImage imageNamed:@"favorite-white.png"] forState:UIControlStateNormal];
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"prevConnectionSelection"] boolValue]) {
                //            [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            }
        } else if (indexPath.row == 0) {
            cell.GroupLbl.numberOfLines = 2;
            cell.GroupLbl.text = NSLocalizedString(@"lbl.AllConnections.txt", @"");
            [cell.communityBtn setImage:[UIImage imageNamed:@"navibar-group-active.png"] forState:UIControlStateNormal];
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"prevConnectionSelection"] boolValue]) {
                //                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            }
        } else {
            cell.GroupLbl.numberOfLines = 2;
            cell.GroupLbl.text = NSLocalizedString(@"lbl.ActiveConnections.txt", @"");
            [cell.communityBtn setImage:[UIImage imageNamed:@"topbar-connection.png"] forState:UIControlStateNormal];
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"prevConnectionSelection"] boolValue]) {
                //                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            }
        }
        
        //            cell.GroupLbl.text = NSLocalizedString(@"lbl.family.txt" , @"");
        //            [cell.communityBtn setImage:[UIImage imageNamed:@"topbar-family.png"] forState:UIControlStateNormal];
        //            if (![[[NSUserDefaults standardUserDefaults]valueForKey:@"prevConnectionSelection"] boolValue] && [[NSUserDefaults standardUserDefaults]valueForKey:@"prevConnectionSelection"])
        //            {
        //                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
        //            }
        //       }
        //        else
        //        {
        //            cell.GroupLbl.text = NSLocalizedString(@"lbl.Favourites.txt" , @"");
        //            [cell.communityBtn setImage:[UIImage imageNamed:@"topbar-connection.png"] forState:UIControlStateNormal];
        //            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"prevConnectionSelection"] boolValue])
        //            {
        //                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
        //            }
        //        }
        
        [cell.communityBtn setUserInteractionEnabled:NO];
        [cell.communityBtn setTitle:@"" forState:UIControlStateNormal];
        [cell.communityBtn setTintColor:[UIColor whiteColor]];
    } else if (isTrackUsers) {
        
        cell.communityBtn.contentMode = UIViewContentModeScaleAspectFit;
        
        [cell.communityBtn setUserInteractionEnabled:NO];
        [cell.communityBtn setTitle:@"" forState:UIControlStateNormal];
        [cell.communityBtn setBackgroundColor:[UIColor clearColor]];
        [cell.communityBtn setTintColor:[UIColor whiteColor]];
        cell.GroupLbl.numberOfLines = 2;
        cell.GroupLbl.textAlignment = NSTextAlignmentCenter;
        CGRect frame = cell.communityBtn.frame;
        
        if (indexPath.row == 0) {
            frame.origin.y = 0;
            
            //            frame.origin.y = frame.origin.y - 15;
            cell.communityBtn.frame = frame;
            cell.GroupLbl.frame = CGRectMake(cell.GroupLbl.frame.origin.x + 38 , cell.GroupLbl.frame.origin.y - 15, _dropDownListView.frame.size.width, cell.GroupLbl.frame.size.height);
            //        if (indexPath.row == 0) {
            cell.GroupLbl.text = @"Active\nConnections";
            [cell.communityBtn setImage:[UIImage imageNamed:@"topbar-connection.png"] forState:UIControlStateNormal];
            if (self.selectedTrackUser == indexPath.row){
                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            }
            if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"] boolValue] && [[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"]) {
                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            }
        } else {
            CGFloat marginTop = 15;
            if (indexPath.row == 0) {
                frame.origin.y = 0;
                marginTop = 15;
            } else if (indexPath.row == 1){
                frame.origin.y = -5 ;
                marginTop = 25;
            } else {
                frame.origin.y = -1*( indexPath.row *5 ) ;
                marginTop = 40;
            }
            //            frame.origin.y = frame.origin.y - 15;
            cell.communityBtn.frame = frame;
            cell.GroupLbl.frame = CGRectMake(cell.GroupLbl.frame.origin.x + 38 , cell.GroupLbl.frame.origin.y , _dropDownListView.frame.size.width, cell.GroupLbl.frame.size.height);
            //        if (indexPath.row == 0) {
            NSDictionary *dataDictionary = userArr[indexPath.row - 1];
            
            cell.GroupLbl.text = [dataDictionary valueForKey:@"name"];
            //            NSString *imageName;
            //            if (dataDictionary[kKeyProfileImage] != [NSNull null]) {
            //                imageName = dataDictionary[kKeyProfileImage];
            //            }
            //
            //            if (![imageName isKindOfClass:[NSNull class]]) {
            //                if ([imageName length] > 0) {
            //                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
            //                    [cell.communityBtn.imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
            //                } else {
            //                    cell.communityBtn.imageView.image = [UIImage imageNamed:@"profile_menu.png"];
            //                }
            //            }
            [cell.GroupImage setImage:[UIImage imageNamed:@"profile_menu.png"]];
            if (self.selectedTrackUser == indexPath.row){
                [cell.GroupImage.layer setBorderColor: [[UIColor orangeColor] CGColor]];
                [cell.GroupImage.layer setBorderWidth: 2.0];
            }
            if ([dataDictionary[kKeyProfileImage] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *profileDict = dataDictionary[kKeyProfileImage];
                NSString *imageName = profileDict[kKeyImageName];
                if ([imageName length] > 0) {
                    NSString *imageUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imageName];
                    [cell.GroupImage sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
                } else {
                    cell.communityBtn.imageView.image = [UIImage imageNamed:@"profile_menu.png"];
                }
            }
            
            cell.communityBtn.hidden = YES;
            cell.GroupImage.hidden = NO;
            
            //            [cell.communityBtn setImage:[UIImage imageNamed:@"topbar-connection.png"] forState:UIControlStateNormal];
            if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"] boolValue] && [[NSUserDefaults standardUserDefaults] valueForKey:@"prevGrpTrackSelection"]) {
                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
            }
            
        }
        //        }
        //        else if (indexPath.row == 1)
        //        {
        //            cell.GroupLbl.text = @"Family";
        //            [cell.communityBtn setImage:[UIImage imageNamed:@"topbar-family.png"] forState:UIControlStateNormal];
        //            if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"prevGrpTrackSelection"] boolValue])
        //            {
        //                [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
        //            }
        //        }
    } else if (!isGroupType) {
        cell.communityBtn.hidden = NO;
        cell.GroupImage.hidden = YES;
        cell.GroupLbl.hidden = YES;
        //jonish sprint 2
        cell.communityBtn.tag = indexPath.row + 2;
       // NSLog(@"%ld", (long)(APP_DELEGATE).topBarView.selectedDegree);
        if (self.selectedDegree == indexPath.row + 2) {
            self.selectedDegree = indexPath.row + 2;
            cell.communityBtn.backgroundColor = [UIColor orangeColor];
            [cell.communityBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            cell.communityBtn.layer.borderWidth = 1.0;
            
        } else {
            
            cell.communityBtn.backgroundColor = [UIColor whiteColor];
            cell.communityBtn.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.communityBtn.layer.borderWidth = 1.0;
        }
        //jonish sprint 2
        NSString *degreeString;
        degreeString = [NSString stringWithFormat:@"%ld%@", indexPath.row + 2, @"\u00B0"];
        [cell.communityBtn setTitle:degreeString forState:UIControlStateNormal];
        [cell.communityBtn addTarget:self action:@selector(communityBtnActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.GroupImage.hidden = YES;
        cell.GroupLbl.hidden = YES;
    } else if (isGroupType) {
        NSDictionary *dataDictionary = sourceArr[indexPath.row];
        dataDictionary = [SRModalClass removeNullValuesFromDict:dataDictionary];
        
        if ([dataDictionary[@"image"] isKindOfClass:[NSDictionary class]] && dataDictionary[@"image"] != nil) {
            NSDictionary *imageDict = [dataDictionary valueForKey:@"image"];
            NSString *imagename = [imageDict valueForKey:@"file"];
            if ([imagename length] > 1) {
                NSString *stringUrl = [NSString stringWithFormat:@"%@%@%@/%@", kSerendipityStorageServer, kKeyUserProfileImage, @"120", imagename];
                [cell.GroupImage sd_setImageWithURL:[NSURL URLWithString:stringUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (image == nil) {
                        cell.GroupImage.image = [UIImage imageNamed:@"navibar-group-active.png"];
                    }
                }];
            } else {
                cell.GroupImage.image = [UIImage imageNamed:@"navibar-group-active.png"];
            }
        } else {
            cell.GroupImage.image = [UIImage imageNamed:@"navibar-group-active.png"];
        }
        
        cell.GroupImage.hidden = NO;
        cell.GroupLbl.hidden = NO;
        cell.communityBtn.hidden = YES;
        cell.GroupLbl.frame = CGRectMake(cell.GroupLbl.frame.origin.x + 43, cell.GroupLbl.frame.origin.y, _dropDownListView.frame.size.width, cell.GroupLbl.frame.size.height);
        if (![[dataDictionary valueForKey:kKeyName] isKindOfClass:[NSNull class]] && [dataDictionary valueForKey:kKeyName] != nil) {
            cell.GroupLbl.text = [NSString stringWithFormat:@"%@", [dataDictionary valueForKey:kKeyName]];
        } else {
            cell.GroupLbl.text = @"";
        }
        
        cell.communityBtn.frame = cell.GroupImage.frame;
        cell.communityBtn.tag = indexPath.row;
        
    }
    return cell;
}

#pragma mark - GroupTable Delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SRDiscoveryCustomCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (isConnection) {
        [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
        NSDictionary *params = @{kKeyConnection: @(indexPath.row + 3), kKeyStatus: @(indexPath.row + 1)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
        self.selectedConnection = indexPath.row;
        //Hide grop dowin if present
        [self actionOnCommunityViewBtnClick:nil];
        //        }
    } else if (isGroupType) {
        if (indexPath.row <= sourceArr.count) {
            // If the file doesn’t exist, create an empty array
            tempArray = [[NSMutableArray alloc] init];
            NSDictionary *groupdict = sourceArr[indexPath.row];
            NSArray *array = [groupdict valueForKey:@"group_members"];
            for (NSDictionary *dictt in array) {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, [dictt valueForKey:kKeyUserID]];
                NSArray *filterArr = [groupUsers filteredArrayUsingPredicate:predicate];
                if (filterArr.count > 0) {
                    [tempArray addObject:filterArr[0]];
                }
            }
            //NSDictionary *usersListDict = [groupUsers objectAtIndex:indexPath.row];
            NSDictionary *params = @{@"group_notification": tempArray};
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
            
            self.selectedConnection = indexPath.row;
            //Hide grop dowin if present
            [self actionOnCommunityViewBtnClick:nil];
        }
        
    } else if (isTrackUsers) {
        [cell.communityBtn setBackgroundColor:[UIColor orangeColor]];
        //        [[NSUserDefaults standardUserDefaults] setValue:@0 forKey:@"prevGrpTrackSelection"];
        //        [[NSUserDefaults standardUserDefaults] synchronize];
        if (indexPath.row == 0)
        {
            
            NSDictionary *params = @{kKeyGroupTag: @"2", kKeyGroupType: @"All Connections"};
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
        } else {
            NSDictionary *userdict = userArr[indexPath.row - 1 ];
            if (userdict[kKeyId] != nil ) {
                NSString *userId = userdict[kKeyId];
                NSDictionary *params = @{kKeyGroupTag: userId, kKeyGroupType: @"Selected Connections"};
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationFilterApplied object:params];
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMapFilterApplied object:params];
                
            }
        }
        
        self.selectedTrackUser = indexPath.row;
        //Hide grop dowin if present
        [self actionOnCommunityViewBtnClick:nil];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (isTrackUsers) {
        return 80;
    } else if (isConnection) {
        return 80;
    } else if (!isGroupType) {
        return 60;
    } else {
        return 80;
    }
}

#pragma mark-
#pragma mark- Get Group notification
#pragma mark-

// --------------------------------------------------------------------------------
//
- (void)hideTopBarDropMenu {
    [self actionOnCommunityViewBtnClick:nil];
}

- (void)ShowGroupSucceed:(NSNotification *)inNotify {
    
    NSDictionary *dictionary = [inNotify object];
    self.btnGroup.userInteractionEnabled = YES;
    if (groupUsers.count == 0 && sourceArr.count == 0) {
        // Display the dots
        if ([(APP_DELEGATE).topBarView.btnGroup isSelected]) {
            if ([dictionary isKindOfClass:[NSDictionary class]] && dictionary[kKeyGroupList] != nil) {
                NSArray *groupListArr = dictionary[kKeyGroupList];
                
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kKeyDistance ascending:YES];
                NSArray *sortDescriptors = @[sortDescriptor];
                groupListArr = [groupListArr sortedArrayUsingDescriptors:sortDescriptors];
                
                groupUsers = dictionary[kKeyGroupUsers];
                server.groupUsersArr = groupUsers;
                
                if ([groupListArr count] > 0) {
                    if ([APP_DELEGATE isUserBelow18]){
                        NSMutableArray *newGroupListArr = [NSMutableArray new];
//                        [sourceArr removeAllObjects];
                        
                        for (NSDictionary* dict in groupListArr){
                            BOOL isExistId = NO;
                            
                            NSMutableArray *groupMemberArray = [[NSMutableArray alloc] initWithArray:[dict valueForKey:kKeyGroupMembers]];
                            for (NSDictionary *dict in groupMemberArray) {
                                if ([[dict valueForKey:kKeyUserID] isEqualToString:[[APP_DELEGATE server].loggedInUserInfo valueForKey:kKeyId]]) {
                                    isExistId = YES;
                                }
                            }
//                            if (isExistId && [[NSString stringWithFormat:@"%@",[dict valueForKey:@"group_type"]] isEqualToString:@"0"]){
                            if (isExistId){

                                [newGroupListArr addObject:dict];
                            }
                        }
                        sourceArr = [NSMutableArray arrayWithArray:newGroupListArr];
                        if(sourceArr.count == 0){
                            [SRModalClass showAlert:@"Groups not found"];
                            return;
                        }
                    }else{
                        sourceArr = [NSMutableArray arrayWithArray:groupListArr];
                    }
                    
                    self.lblGroup.hidden = YES;
                    self.imgViewGroupDropDown.hidden = YES;
                    isGroupType = YES;
                    isConnection = NO;
                    isTrackUsers = NO;
                    int x;
                    
                    if ([APP_DELEGATE isUserBelow18]) {
                        if (SCREEN_WIDTH == 320) {
                            x = (int) (self.btnGroup.frame.origin.x - 9);
                        } else {
                            x = (int) (self.btnGroup.frame.origin.x-8);
                        }
                        x = (int) (self.btnGroup.frame.origin.x);

                        x = 2*(SCREEN_WIDTH/3) + x;
                    }else{
                        if (SCREEN_WIDTH == 320) {
                            x = self.btnGroup.frame.origin.x - 0;
                        } else
                            x = self.btnGroup.frame.origin.x - 0;
                        
                    }
                   
                    self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], self.btnGroup.frame.size.width, 50);
                    [(APP_DELEGATE).tabBarController.view addSubview:self.dropDownListView];
                    NSInteger rowHeight = 80;
                    [UIView animateWithDuration:0.5 animations:^{
                        NSInteger height = (sourceArr.count + 1) * 80;
                        if (height > 400){
                            height = 400;
                        }
                        self.dropDownListView.frame = CGRectMake(x, [self getDropDownY:nil], self.btnGroup.frame.size.width, height);
                    }];
                    self.dropDownListView.hidden = NO;
                    self.dropDownListView.dropDownTbl.delegate = self;
                    self.dropDownListView.dropDownTbl.dataSource = self;
                    [self.dropDownListView.dropDownTbl reloadData];
                } else {
                    [self.dropDownListView removeFromSuperview];
                    self.dropDownListView.frame = CGRectZero;
                    self.lblGroup.hidden = NO;
                    self.imgViewGroupDropDown.hidden = YES;
                }
            } else {
                [self.dropDownListView removeFromSuperview];
                self.dropDownListView.frame = CGRectZero;
                self.lblGroup.hidden = NO;
                self.imgViewGroupDropDown.hidden = YES;
            }
        }
    }
}
// --------------------------------------------------------------------------------
// ShowGroupFailed:

- (void)ShowGroupFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
    self.btnGroup.userInteractionEnabled = YES;
    [self.dropDownListView removeFromSuperview];
    self.dropDownListView.frame = CGRectZero;
    self.lblGroup.hidden = NO;
    self.imgViewGroupDropDown.hidden = YES;
}

- (float)getDropDownY:(UIImage *)buttonBackground {
    return self.frame.origin.y + self.imgViewConnections.frame.origin.y;
}

@end
