#import <UIKit/UIKit.h>

@interface SRDropPinWindowView : UIView <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *BGImage;
@property (strong, nonatomic) IBOutlet UITextField *titleTxtFld;
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *lblMiles;

@property (weak, nonatomic) IBOutlet UIButton *openInMapBtn;

@property (weak, nonatomic) IBOutlet UILabel *openInMapLbl;
@property (weak, nonatomic) IBOutlet UIButton *trashBtn;
@property (weak, nonatomic) IBOutlet UIImageView *editImage;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (strong, nonatomic) NSDictionary *infoDict;
@property (assign, nonatomic) BOOL isEdit;

@end
