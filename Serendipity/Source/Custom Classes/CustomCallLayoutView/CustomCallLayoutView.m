
#import "CustomCallLayoutView.h"

@implementation CustomCallLayoutView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCallLayoutView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }
    self.deleteBtn.hidden = YES;
    // Return
    return self;
}

//- (IBAction)editBtnAction:(id)sender {
//
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Serendipity" message:@"Clicked" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//    self.BGImage.backgroundColor = [UIColor grayColor];
//    self.openInMapLbl.hidden = YES;
//    self.openInMapBtn.hidden = YES;
//    self.deleteBtn.hidden = NO;
//}

- (IBAction)openInBtnAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Serendipity" message:@"Clicked" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}

- (IBAction)trashBtnAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Serendipity" message:@"Clicked" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}
@end
