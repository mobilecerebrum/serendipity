
#import <UIKit/UIKit.h>

@interface CustomCallLayoutView : UIView


@property (weak, nonatomic) IBOutlet UIImageView *BGImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *openInMapBtn;
@property (weak, nonatomic) IBOutlet UILabel *openInMapLbl;
@property (weak, nonatomic) IBOutlet UIButton *trashBtn;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

- (IBAction)openInBtnAction:(id)sender;
- (IBAction)trashBtnAction:(id)sender;


@end
