#import "SRDropPinWindowView.h"

@implementation SRDropPinWindowView

#pragma mark-
#pragma mark- Standard Methods
#pragma mark-

//______________________________________________________________________________
// initWithFrame:

- (id)initWithFrame:(CGRect)frame {
    // Call super
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SRDropPinWindowView" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
        self.frame = frame;
    }
    // Return
    return self;
}

@end
