//
//  ComposeView.h
//  Serendipity
//
//  Created by Mcuser on 1/24/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ComposeViewDelegate
-(void)sendButtonClicked:(UIButton *)button;
- (void)shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text andTextView:(UITextView *)textView;
@end

@interface ComposeView : UIView<UITextViewDelegate, UIImagePickerControllerDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIView *textViewContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraints;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *attachmentButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong,nonatomic) NSMutableArray *filesArray;
@property (assign,nonatomic) CGFloat keyBoardHeight;
@property (strong,nonatomic) id delegate;
@property (strong,nonatomic) NSString *placeHolder;
@property (nonatomic) CGFloat progress;

- (void) setHeightOfComposeViewForText:(NSString *)text andAttachment:(NSInteger)numberOfAttachment forKeyBoardHeight:(CGFloat) keyboardHeight;
- (void) setHeightOfComposeViewForText1:(NSString *)text andAttachment:(NSInteger)numberOfAttachment forKeyBoardHeight:(CGFloat) keyboardHeight;
- (void) setFilesArray;
- (void) setProgressOfFileForRow:(NSInteger)row andProgress:(CGFloat)progress;

@end

NS_ASSUME_NONNULL_END
