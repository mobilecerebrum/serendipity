//
//  ComposeView.m
//  Serendipity
//
//  Created by Mcuser on 1/24/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "ComposeView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "SRModalClass.h"

@implementation ComposeView

-(instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _progress = 0.0;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView reloadData];
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"ComposeView" owner:self options:nil];
        self = [nibs objectAtIndex:0];
        _placeHolder = @"";
        self.textView.delegate = self;
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textViewContainer.layer.cornerRadius = 4.0;
        self.textViewContainer.clipsToBounds = YES;
        self.textView.text = _placeHolder;
        self.textView.textColor = [UIColor lightGrayColor];
        self.tableView.tableFooterView = [UIView new];
        self.textView.textContainerInset = UIEdgeInsetsMake(0, -4, 0, 0);
        self.textView.layoutManager.allowsNonContiguousLayout = NO;
        self.translatesAutoresizingMaskIntoConstraints = YES;
        [self.attachmentButton addTarget:self action:@selector(attachmentButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.sendButton addTarget:self action:@selector(sendButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.frame = frame;
        _filesArray = [NSMutableArray new];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        self.sendButton.enabled = NO;
        _progress = 0;
        
       [self setHeightOfComposeViewForText:@"" andAttachment:self.filesArray.count forKeyBoardHeight:self.keyBoardHeight];
    }
    return self;
}

-(void) sendButtonClicked:(UIButton *)button {
    if (![self.textView.text isEqualToString:_placeHolder]) {
        if ([_delegate respondsToSelector:@selector(sendButtonClicked:)])
            [_delegate performSelector:@selector(sendButtonClicked:) withObject:button];
    }
}

-(void) attachmentButtonClicked {
    UIViewController *viewController = _delegate;
    [self.textView endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Choose option to upload files."
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"iCloud"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              UIViewController *viewController = _delegate;
                                                              UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.data"]
                                                                                                                                                                      inMode:UIDocumentPickerModeImport];
                                                              documentPicker.delegate = self;
                                                              
                                                              documentPicker.modalPresentationStyle = UIModalPresentationFullScreen;
                                                              [viewController presentViewController:documentPicker animated:YES completion:nil];
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Photo library"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               UIViewController *viewController = _delegate;
                                                               PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                                                               
                                                               if (status == PHAuthorizationStatusAuthorized) {
                                                                   // Access has been granted.
                                                                   if ([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                                                       
                                                                       UIImagePickerController *picker = [UIImagePickerController new];
                                                                       picker.delegate = self;
                                                                       //                                                                       picker.allowsEditing = YES;
                                                                       [picker setModalPresentationStyle:UIModalPresentationFullScreen];
                                                                       picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                                       [viewController presentViewController:picker animated:YES completion:NULL];
                                                                       
                                                                   } else {
                                                                       [SRModalClass showAlert:@"Sorry, your device is not supported."];
                                                                   }
                                                               }
                                                               
                                                               else if (status == PHAuthorizationStatusDenied) {
                                                                   // Access has been denied.
                                                                   [self showGalleryAcessAlert];
                                                               }
                                                               else if (status == PHAuthorizationStatusNotDetermined)
                                                               {
                                                                   UIViewController *viewController = _delegate;
                                                                   if ([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                                                       
                                                                       UIImagePickerController *picker = [UIImagePickerController new];
                                                                       picker.delegate = self;
                                                                       //                                                                       picker.allowsEditing = YES;
                                                                       [picker setModalPresentationStyle:UIModalPresentationFullScreen];
                                                                       picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                                       [viewController presentViewController:picker animated:YES completion:NULL];
                                                                       
                                                                   } else {
                                                                       
                                                                       [SRModalClass showAlert:@"Sorry, your device is not supported."];
                                                                       
                                                                   }
                                                               }
                                                               
                                                               else if (status == PHAuthorizationStatusRestricted) {
                                                                   // Restricted access - normally won't happen.
                                                                   [self showGalleryAcessAlert];
                                                               }
                                                               
                                                           }];
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Camera"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              UIViewController *viewController = _delegate;
                                                              PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                                                              
                                                              if (status == PHAuthorizationStatusAuthorized) {
                                                                  // Access has been granted.
                                                                  if ([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                                                      
                                                                      UIImagePickerController *picker = [UIImagePickerController new];
                                                                      picker.delegate = self;
                                                                      //                                                                      picker.allowsEditing = YES;
                                                                      picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                      [picker setModalPresentationStyle:UIModalPresentationFullScreen];
                                                                      [viewController presentViewController:picker animated:YES completion:NULL];
                                                                      
                                                                  }
                                                                  else
                                                                  {
                                                                      [SRModalClass showAlert:@"Sorry, your device is not supported."];
                                                                  }
                                                              }
                                                              
                                                              else if (status == PHAuthorizationStatusDenied) {
                                                                  // Access has been denied.
                                                                  [self showGalleryAcessAlert];
                                                              }
                                                              else if (status == PHAuthorizationStatusNotDetermined)
                                                              {
                                                                  UIViewController *viewController = _delegate;
                                                                  if ([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                                                                      
                                                                      UIImagePickerController *picker = [UIImagePickerController new];
                                                                      picker.delegate = self;
                                                                      //                                                                      picker.allowsEditing = YES;
                                                                      [picker setModalPresentationStyle:UIModalPresentationFullScreen];
                                                                      picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                      [viewController presentViewController:picker animated:YES completion:NULL];
                                                                      
                                                                  } else {
                                                                      
                                                                      [SRModalClass showAlert:@"Sorry, your device is not supported."];
                                                                  }
                                                              }
                                                              
                                                              else if (status == PHAuthorizationStatusRestricted) {
                                                                  // Restricted access - normally won't happen.
                                                                  [self showGalleryAcessAlert];
                                                              }
                                                              
                                                          }];
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:thirdAction];
    [alert addAction:cancel];
    alert.popoverPresentationController.sourceView = viewController.view;
    alert.popoverPresentationController.sourceRect = CGRectMake(viewController.view.bounds.size.width / 2.0 - 105, viewController.view.bounds.size.height / 2.0 + 70, 1.0, 1.0);;
    
    [viewController presentViewController:alert animated:YES completion:nil];
}

- (void)photoBtnAction {
    UIViewController *viewController = _delegate;
    
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    switch (authStatus) {
        case AVAuthorizationStatusAuthorized:
        {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                UIImagePickerController *picker = [UIImagePickerController new];
                picker.delegate = self;
                //                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [picker setModalPresentationStyle:UIModalPresentationFullScreen];
                [viewController presentViewController:picker animated:YES completion:NULL];
                
            }
            else
            {
                [SRModalClass showAlert:@"Sorry, your device is not supported."];
            }
            break;
        }
        case AVAuthorizationStatusDenied:
        {
            [self showCameraAcessAlert];
            break;
        }
        case AVAuthorizationStatusRestricted:
        {
            [self showCameraAcessAlert];
            break;
        }
        case AVAuthorizationStatusNotDetermined:
        {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                UIImagePickerController *picker = [UIImagePickerController new];
                picker.delegate = self;
                //                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [picker setModalPresentationStyle:UIModalPresentationFullScreen];
                [viewController presentViewController:picker animated:YES completion:NULL];
                
            }else{
                
                [SRModalClass showAlert:@"Sorry, your device is not supported."];
                
            }
            break;
        }
        default:
        {
            [self showCameraAcessAlert];
            break;
        }
    }
}

- (void)showCameraAcessAlert{
    UIViewController *viewController = _delegate;
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Serendipity" message:@"Please allow camera access" preferredStyle:(UIAlertControllerStyleAlert)];
    
    
    [alertViewController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];

        }
        
    }]];
    [viewController presentViewController:alertViewController animated:YES completion:nil];
    
}
- (void)showGalleryAcessAlert
{
    UIViewController *viewController = _delegate;
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Serendipity" message:@"Please allow camera access" preferredStyle:(UIAlertControllerStyleAlert)];
    
    
    [alertViewController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }
        
    }]];
    [viewController presentViewController:alertViewController animated:YES completion:nil];
    
}

-(void)setHeightOfComposeViewForText:(NSString *)text andAttachment:(NSInteger)numberOfAttachment forKeyBoardHeight:(CGFloat) keyboardHeight {
    
}

-(void)updateOnScreenPosition {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    self.frame = CGRectMake(0, screenSize.height - self.frame.size.height - _keyBoardHeight, screenSize.width, self.frame.size.height);
}

- (CGFloat)heightForString:(NSString *)text font:(UIFont *)font maxWidth:(CGFloat)maxWidth {
    if (![text isKindOfClass:[NSString class]] || !text.length) {
        // no text means no height
        return 0;
    }
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    CGSize size = [text boundingRectWithSize:CGSizeMake(maxWidth, CGFLOAT_MAX) options:options attributes:attributes context:nil].size;
    CGFloat height = ceilf(size.height) + 1; // add 1 point as padding
    
    return height;
}

- (void)setHeightOfComposeViewForText1:(NSString *)text andAttachment:(NSInteger)numberOfAttachment forKeyBoardHeight:(CGFloat) keyboardHeight
{
    
    UIFont *robotoFont = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    CGSize size = [[_textView.text stringByAppendingString:text] boundingRectWithSize:CGSizeMake(self.textView.frame.size.width - 5, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : robotoFont} context:nil].size;
    
    
    size.height = ceilf(size.height);
    if (numberOfAttachment<5)
        self.tableViewHeightConstraints.constant = numberOfAttachment *30;
    else
        self.tableViewHeightConstraints.constant = 150;
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (size.height < 17)
        size = CGSizeMake(self.textView.frame.size.width, 17);
    else if(size.height > 115)
        size = CGSizeMake(self.textView.frame.size.width, 115);
    
    
    CGFloat height = self.tableViewHeightConstraints.constant + 16 + size.height+16;
    self.frame = CGRectMake(0, screenSize.height - height - keyboardHeight, screenSize.width, height);
    
    if ([text isEqualToString:@"\n"] && size.height < 115)
        [self.textView setContentOffset:CGPointZero animated:NO];
    
    if (size.height<115) {
        [self.textView scrollRangeToVisible:NSMakeRange(text.length, 0)];
        
    }
    
}

-(void)setFilesArray
{
    [self setHeightOfComposeViewForText:@"" andAttachment:_filesArray.count forKeyBoardHeight:_keyBoardHeight];
    [self.tableView reloadData];
}

-(void)setPlaceHolder:(NSString *)placeHolder
{
    _textView.text = placeHolder;
    _placeHolder = placeHolder;
    [self.textView setTextColor:[UIColor lightGrayColor]];
    [self.sendButton setEnabled:NO];
    
}

#pragma  mark textView delegates

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if ([textView.text isEqualToString:_placeHolder] && [textView.textColor isEqual:[UIColor lightGrayColor]])
        [textView setSelectedRange:NSMakeRange(0, 0)];
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:_placeHolder])
    {
        [textView setSelectedRange:NSMakeRange(0, 0)];
    }
    else
    {
        self.sendButton.enabled = YES;
    }
    [self setHeightOfComposeViewForText:@"" andAttachment:_filesArray.count forKeyBoardHeight:_keyBoardHeight];
    if ([_delegate respondsToSelector:@selector(textViewDidBeginEditing:)])
        [_delegate performSelector:@selector(textViewDidBeginEditing:) withObject:textView];
}

- (void)textViewDidChange:(UITextView *)textView
{
    
    if (textView.text.length != 0 && [[textView.text substringFromIndex:1] isEqualToString:_placeHolder] && [textView.textColor isEqual:[UIColor lightGrayColor]]){
        textView.text = [textView.text substringToIndex:1];
        textView.textColor = [UIColor blackColor]; ///optional
        self.sendButton.enabled = YES;
    }
    else if(textView.text.length == 0){
        textView.text = _placeHolder;
        self.sendButton.enabled = NO;
        textView.textColor = [UIColor lightGrayColor];
        [textView setSelectedRange:NSMakeRange(0, 0)];
        [self setHeightOfComposeViewForText:@"" andAttachment:_filesArray.count forKeyBoardHeight:_keyBoardHeight];
        
    }
    if (![textView.text isEqualToString:_placeHolder])
    {
        // textView.textColor = [UIColor blackColor];
    }
    
    if ([_delegate respondsToSelector:@selector(textViewDidChange:)])
        [_delegate performSelector:@selector(textViewDidChange:) withObject:textView];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([_delegate respondsToSelector:@selector(textViewDidEndEditing:)])
        [_delegate performSelector:@selector(textViewDidEndEditing:) withObject:textView];
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = _placeHolder;
        textView.textColor = [UIColor lightGrayColor];
        self.sendButton.enabled = NO;
    }
    [self setHeightOfComposeViewForText:@"" andAttachment:_filesArray.count forKeyBoardHeight:_keyBoardHeight];
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    [_delegate shouldChangeTextInRange:range replacementText:text andTextView:textView];
    
    if ([text isEqualToString:@""] && [textView.text isEqualToString:_placeHolder] && [textView.textColor isEqual:[UIColor lightGrayColor]])
    {
        return NO;
    }
    if (textView.text.length > 0 && [textView.text isEqualToString:_placeHolder] && [textView.textColor isEqual:[UIColor lightGrayColor]]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        self.sendButton.enabled = YES;
    }
    if ([text length] == 1)
    {
        if(textView.selectedTextRange.empty && ![text isEqualToString:@""])
        {
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithAttributedString:self.textView.attributedText];
            NSAttributedString *str = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:15]}];
            UITextRange *selRange = textView.selectedTextRange;
            NSInteger pos = [textView offsetFromPosition:textView.beginningOfDocument
                                              toPosition:selRange.start];
            
            [string insertAttributedString:str atIndex:pos];
            self.textView.attributedText = [string mutableCopy];
            selRange = textView.selectedTextRange;
            
            if (![text isEqualToString:@"\n"]) {
                self.textView.selectedRange = NSMakeRange(pos+1, 0);
                
            }
            [self setHeightOfComposeViewForText1:text andAttachment:_filesArray.count forKeyBoardHeight:_keyBoardHeight];
            return NO;
            
        }
    }
    [self setHeightOfComposeViewForText1:text andAttachment:_filesArray.count forKeyBoardHeight:_keyBoardHeight];
    return YES;
}

#pragma mark -
#pragma mark - iCloud files

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        NSFileCoordinator *coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
        NSError *error = nil;
        [coordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL *newURL) {
            
            NSData *fileData = [NSData dataWithContentsOfURL:newURL];
            NSString *theFileName = [newURL lastPathComponent];
            [_filesArray addObject:@{@"fileData":fileData,@"fileName":theFileName}];
            [self setFilesArray];
            _progress = 0.0;
            
            // Do something
        }];
        if (error) {
            // Do something else
        }
    }
}


#pragma mark keyboard data

- (void)keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    UIViewController *viewController = _delegate;
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [viewController.view convertRect:keyboardFrameEnd fromView:nil];
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        //  [self setHeightOfComposeViewForText:self.textView.text andAttachment:self.filesArray.count forKeyBoardHeight:keyboardFrameEnd.size.height];
        self.keyBoardHeight = keyboardFrameEnd.size.height;
        [self updateOnScreenPosition];
        
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        //  [self setHeightOfComposeViewForText:self.textView.text andAttachment:self.filesArray.count forKeyBoardHeight:0];
        self.keyBoardHeight = 0;
        [self updateOnScreenPosition];
    } completion:nil];
}
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    _progress = 0.0;
    
    UIImage *chosenImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    __block NSData *fileData = UIImagePNGRepresentation(chosenImage);
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        [_filesArray addObject:@{@"fileData":fileData,@"fileName":@"image.jpeg"}];
        [self setFilesArray];
    }
    else{
        
        
        NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[imageURL] options:nil];
        NSString *fileName = [[result firstObject] filename];
        
        
        
        PHAsset *asset=[PHAsset fetchAssetsWithALAssetURLs:@[imageURL] options:nil].firstObject;
        if (asset) {
            // get photo info from this asset
            PHImageRequestOptions * imageRequestOptions = [PHImageRequestOptions new];
            imageRequestOptions.synchronous = YES;
            [[PHImageManager defaultManager]
             requestImageDataForAsset:asset
             options:imageRequestOptions
             resultHandler:^(NSData *imageData, NSString *dataUTI,
                             UIImageOrientation orientation,
                             NSDictionary *info)
             {
                 NSDictionary *dict = [self metadataFromImageData:imageData];// as this imageData is in NSData format so we need a method to convert this NSData into NSDictionary to display metadata
                 
                 NSString *mimeType = @"image/jpeg";
                 fileData = [self dataFromImage:chosenImage metadata:dict mimetype:mimeType];
                 if ([self isNullString:fileName]) {
                     [_filesArray addObject:@{@"fileData":fileData,@"fileName":@"image.jpeg"}];
                 }
                 else{
                     [_filesArray addObject:@{@"fileData":fileData,@"fileName":fileName}];
                 }
                 
                 //[[APIManager sharedManagerWithDelegate:self] uploadImage:fileData toPath:methodName workspace:_workspaceId folder:_folderId];
                 [self filesArray];
             }];
        }
    }
    [self.tableView reloadData];
    [self setHeightOfComposeViewForText:@"" andAttachment:self.filesArray.count forKeyBoardHeight:self.keyBoardHeight];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (NSData *)dataFromImage:(UIImage *)image metadata:(NSDictionary *)metadata mimetype:(NSString *)mimetype
{
    
    NSMutableData *imageData = [NSMutableData data];
    CFStringRef uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, (__bridge CFStringRef)mimetype, NULL);
    CGImageDestinationRef imageDestination = CGImageDestinationCreateWithData((__bridge CFMutableDataRef)imageData, uti, 1, NULL);
    
    if (imageDestination == NULL)
    {
      //  NSLog(@"Failed to create image destination");
        imageData = nil;
    } else {
        
        CGImageDestinationAddImage(imageDestination, image.CGImage, (__bridge CFDictionaryRef)metadata);
        
        if (CGImageDestinationFinalize(imageDestination) == NO)
        {
            NSLog(@"Failed to finalise");
            imageData = nil;
        }
        CFRelease(imageDestination);
    }
    
    CFRelease(uti);
    
    return imageData;
}

-(NSDictionary*)metadataFromImageData:(NSData*)imageData{
    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)(imageData), NULL);
    if (imageSource) {
        NSDictionary *options = @{(NSString *)kCGImageSourceShouldCache : [NSNumber numberWithBool:NO]};
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, (__bridge CFDictionaryRef)options);
        if (imageProperties) {
            NSDictionary *metadata = (__bridge NSDictionary *)imageProperties;
            CFRelease(imageProperties);
            CFRelease(imageSource);
           // NSLog(@"Metadata of selected image%@",metadata);// It will display the metadata of image after converting NSData into NSDictionary
            return metadata;
            
        }
        CFRelease(imageSource);
    }
    
    NSLog(@"Can't read metadata");
    return nil;
}

-(void)setProgressOfFileForRow:(NSInteger)row andProgress:(CGFloat)progress
{
    _progress = progress;
    if ([_filesArray count]>0)
    {
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    _progress = 0.0;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark tableview datasource and delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_filesArray count];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *MyIdentifier = @"ComposeViewAttachmentCellTableViewCell";
//    ComposeViewAttachmentCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//
//    if (cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:MyIdentifier owner:self options:nil];
//        cell = (ComposeViewAttachmentCellTableViewCell *)[nib objectAtIndex:0];
//    }
//    NSDictionary *file = [_filesArray objectAtIndex:indexPath.row];
//    cell.attachmentLabel.text = [file objectForKey:@"fileName"];
//    cell.progressBar.progress = _progress;
//    [cell.attachmentClearButton addTarget:self action:@selector(attachmentClearButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    cell.attachmentClearButton.tag = indexPath.row;
    return nil;
}

-(void)attachmentClearButtonClicked:(UIButton *)button
{
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    [_filesArray removeObjectAtIndex:indexpath.row];
    [self setFilesArray];
    
}

- (BOOL)isNullString:(NSString*)_inputString {
    NSString *InputString ;
    
    if ([_inputString isKindOfClass:[NSString class]])
    {
        InputString = [NSString stringWithFormat:@"%@",_inputString];
    }
    else if ([_inputString isKindOfClass:[NSNumber class]])
    {
        
        InputString = [NSString stringWithFormat:@"%d",(int)_inputString];
    }
    else
    {
        InputString = @"";
    }
    if( (InputString == nil) ||(InputString ==(NSString *)[NSNull null])||([InputString isEqual:nil])||([InputString length] == 0)||([InputString isEqualToString:@""])||([InputString isEqualToString:@"(NULL)"])||([InputString isEqualToString:@"<NULL>"])||([InputString isEqualToString:@"<null>"]||([InputString isEqualToString:@"(null)"])||([InputString isEqualToString:@""]))
       
       )
        return YES;
    else
        return NO ;
}

@end
