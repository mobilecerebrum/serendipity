//
//  SRBluredImage.h
//  Serendipity
//
//  Created by Mcuser on 12/25/18.
//  Copyright © 2018 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRProfileImageView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRBluredImage : SRProfileImageView
- (UIImage*)blurImage:(UIImage*)image;
@end

NS_ASSUME_NONNULL_END
