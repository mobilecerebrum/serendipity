//
//  MYViewController.m
//  MYBlurIntroductionView-Example
//
//  Created by Matthew York on 10/16/13.
//  Copyright (c) 2013 Matthew York. All rights reserved.
//

#import "SRToolTipViewController.h"
#import "MYIntroductionPanel.h"

@interface SRToolTipViewController ()

@end

@implementation SRToolTipViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    //Calling this methods builds the intro and adds it to the screen. See below.
    [self buildIntro];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Build MYBlurIntroductionView

-(void)buildIntro{

//    MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_01-Radar.jpg"]];
    
    MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"SD_Tooltips_01-Radar.jpg" ];
    
//    MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_02-Map.jpg"]];
    
     MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"SD_Tooltips_02-Map.jpg" ];
    
    MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_03-List.jpg"]];
    
     MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_04-Menu.jpg"]];
    
     MYIntroductionPanel *panel5 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_05-Inner-Change-Radar.jpg"]];
    
     MYIntroductionPanel *panel6 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_06-Inner-Change-Filters.jpg"]];
    
     MYIntroductionPanel *panel7 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_07-Inner-Update-Radar.jpg"]];
    
     MYIntroductionPanel *panel8 = [[MYIntroductionPanel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) title:@"" description:@"" image:[UIImage imageNamed:@"SD_Tooltips_08-Inner-Swipe-Left-Right.jpg"]];
    
    //Add panels to an array
    NSArray *panels = @[panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8];
    
    //Create the introduction view and set its delegate
    MYBlurIntroductionView *introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    introductionView.delegate = self;
    introductionView.BackgroundImageView.image = [UIImage imageNamed:@"Toronto, ON.jpg"];
    [introductionView setBackgroundColor:[UIColor colorWithRed:90.0f/255.0f green:175.0f/255.0f blue:113.0f/255.0f alpha:0.65]];
    //introductionView.LanguageDirection = MYLanguageDirectionRightToLeft;
    
    //Build the introduction with desired panels
    [introductionView buildIntroductionWithPanels:panels];
    
    //Add the introduction to your view
    [self.view addSubview:introductionView];
}

#pragma mark - MYIntroduction Delegate 

-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex{
    NSLog(@"Introduction did change to panel %ld", (long)panelIndex);
    
    //You can edit introduction view properties right from the delegate method!
    //If it is the first panel, change the color to green!
    if (panelIndex == 0) {
        [introductionView setBackgroundColor:[UIColor colorWithRed:90.0f/255.0f green:175.0f/255.0f blue:113.0f/255.0f alpha:0.65]];
    }
    //If it is the second panel, change the color to blue!
    else if (panelIndex == 1){
        [introductionView setBackgroundColor:[UIColor colorWithRed:50.0f/255.0f green:79.0f/255.0f blue:133.0f/255.0f alpha:0.65]];
    }
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType {
    NSLog(@"Introduction did finish");
}

@end
