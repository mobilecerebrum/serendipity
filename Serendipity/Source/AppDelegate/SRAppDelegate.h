#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

#import "Constant.h"
#import "SlideNavigationController.h"
#import "SRPreLoginViewController.h"
#import "SRServerConnection.h"
#import "SRSplashViewController.h"
#import "SRActivityIndicator.h"
#import "SRDiscoveryTopBarView.h"
#import "Reachability.h"
#import "SRNoInternetView.h"
#import <UserNotifications/UserNotifications.h>
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import "SREventTabViewController.h"

@interface SRAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate,UNUserNotificationCenterDelegate, FIRMessagingDelegate>

{

	// Instance variable
	// Modals
	NSMutableArray *tabsConArr;

	// Controllers
	SRPreLoginViewController *preLoginViewCon;
	UINavigationController *preNavCon;
    SRActivityIndicator *activityIndicator;
    SRActivityIndicator *hsactivityIndicator;

	// Flag
    // ПИЗДЕЦЬ. УБРАТЬ НАХУЙ ЗВІДСИ І РУКИ ПООДРУБУВАТИ ХТО ТАК ЗРОБИТЬ
	BOOL isAfterLogout,isOnChat,isOnChatList,isNotificationFromClosed,isPreviewOpen,IsDownloadStarted;
    BOOL isAfterTrackNoti;
    BOOL wasInBackground;

    // Chat modal
	NSMutableArray *chatFriendListArr;
	NSString *chatListPath;
	NSMutableArray *chatGroupListArr;
	NSString *groupChatListPath;
    NSMutableDictionary *notificationDict;
    NSNumber * loggedinUserID,*chatUserID,*msgType;
    SRNoInternetView *objInternetView;
    NSDictionary *dictUserInfo, *onlineUserDict, *groupDict,*dictUserData, *remoteNotification;
}
@property (nonatomic,strong) NSDictionary *dictUserInfo,*onlineUserDict,*groupDict,*dictUserData;
@property (nonatomic,strong)NSNumber * loggedinUserID, *chatUserID,*msgType;
@property(nonatomic,assign)NSInteger notificationType;
@property(nonatomic,strong)NSString *selectedGroupId,*lblLocationText,*imageSentStatus;
// Properties
@property (nonatomic, strong) SRDiscoveryTopBarView *topBarView;
@property (nonatomic, strong) NSMutableDictionary  *userPhoneNoDetails;
@property (nonatomic,strong) NSMutableDictionary *selectedConnection;
@property (strong, nonatomic) SRServerConnection *server;
@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, assign) BOOL isZero;
//@property(nonatomic,assign)  BOOL isAppLaunched;
@property (nonatomic, assign) BOOL isEventFilterApplied;
@property (nonatomic, strong) NSMutableArray *dataDict  ;
@property BOOL accountSettingVCFlag;
@property (nonatomic,strong) NSMutableDictionary *settingData;

// Chat Modal properties
@property (nonatomic, strong) NSMutableArray *chatFriendListArr,*arrChatMsg;
@property (nonatomic, strong) NSMutableArray *chatGroupListArr;

//Neha
@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
//Neha

//Tooltip properties
@property (nonatomic, assign) BOOL tooltipRadarViewFlag,isPresentDiscoveryMapView;
@property (nonatomic, assign) BOOL tooltipListViewFlag;
@property (nonatomic, assign) BOOL tooltipMapViewFlag;

//Is user below 18
@property (nonatomic, assign) BOOL isUserBelow18,isRetry;

//Is SignUp Success
@property (nonatomic, assign) BOOL isSignUpSuccess;

@property (nonatomic,strong) CLLocation *lastLocation;
@property BOOL isMeasurementChangesForMap;
@property BOOL isMeasurementChangesForRadar;

//
@property (nonatomic, strong) NSMutableArray *oldDataArray;
@property (nonatomic, strong) NSMutableArray *oldSourceArray;
@property (nonatomic, strong) NSMutableArray *oldConnDataArray;
@property (nonatomic, strong) NSMutableArray *oldFamilyDataArray;
@property (nonatomic, strong) NSMutableArray *oldConnTrackMeArray;
@property (nonatomic, strong) NSMutableArray *oldConnTrackAmArray;
@property (nonatomic, strong) NSMutableArray *oldEventUserArray;
@property (nonatomic,assign) NSMutableDictionary *myLocationDictInPlist;
@property (nonatomic, strong) NSMutableArray *trackUserArray;


@property (nonatomic, strong)NSMutableArray *groupTrackingArray;
@property (nonatomic, assign) BOOL isGroupTrack,isLogoutCall;
@property (retain, nonatomic)  Reachability* reach;
@property (nonatomic, assign) BOOL isSwipedCell, isFromNotification, isOnChat,isOnChatList,isNotificationFromClosed,isPreviewOpen,isAcceptFromDetail,isMobileVerifcationPending,IsDownloadStarted;

@property (nonatomic, strong) NSIndexPath *swipedCellIndexPath;
@property (nonatomic, strong) NSString *swipedCellUserId;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property BOOL slideToLeft;
@property BOOL slideToRight;

// For RadarUserGet
@property BOOL timer, isBackMenu, isOnMap,isTrackUserSelected,isReqInProcess,isTabClicked,isMapDisappear;
@property float compassHeading;
@property double rotation;
@property CGPoint anchor;
#pragma mark
#pragma mark Chat Methods
#pragma mark

- (BOOL)isLocationTutorialDone;
- (void)showTutorial:(UIViewController*)container;
- (void)saveSendMessage:(NSDictionary *)inMessageDict;
- (NSMutableArray *)getUserMessage:(NSString *)userId;
- (NSString *)getAdditionalInfo;
- (void)saveChatMessagesForUser:(NSString *)userId inChatArr:(NSArray *)inArr;
- (void)deleteFriendFromList:(NSDictionary *)inUserDict;
- (NSInteger)getUnreadMessageCount;
- (NSInteger)getUnreadTrackingLocationCount;

- (void)callNotificationsApi;
-(BOOL)isRestrictedAge:(int)age forCountry:(NSString*)countryCode;

#pragma mark
#pragma mark Group Chat Methods
#pragma mark

- (void)createOrJoinChatRoom:(NSDictionary *)inEventDict
                 withInvitee:(NSMutableArray *)inInviteeList
                   editOwner:(NSString *)inUserId;

// --------------------------------------------------------------------------------
// fetchAndSaveEventGroupChatList: forEventDict:
- (NSMutableArray *)fetchAndSaveEventGroupChatList:(BOOL)isFetch forEventDict:(NSDictionary *)inDict;
- (NSMutableDictionary *)localSavingEventChat:(NSDictionary *)inEventDetails
                                  withInvitee:(NSMutableArray *)inInviteeList
                                      isFetch:(BOOL)isFetch;

- (void)saveGroupChatMessage:(NSMutableDictionary *)inMessageDict
                    forEvent:(NSString *)inEventId;
- (void)updateReadCount:(NSMutableArray *)inMessageArr
               forEvent:(NSString *)inEventId;
-(void)setInitialController;
-(UIColor*)getColorBaseOnBatteryUsage:(NSString*)cuurentBatteryOption;
#pragma mark Indicator Method

- (void)showActivityIndicator;
- (void)hideActivityIndicator;
- (void)hsshowActivityIndicator;
- (void)hshideActivityIndicator;


- (void)setRootView;
- (void)toolTipFlagSaveData;
- (void)logoutIfNotVerified;
-(void)ClearDataOnLogout;
- (BOOL)isLoggedIn;
- (void)showContactTutorial;
- (NSString *)getDeviceId ;
@property BOOL isLoggedInSuccess;

@end
