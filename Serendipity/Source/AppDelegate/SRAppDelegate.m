#import <GoogleMaps/GoogleMaps.h>
#import "SRModalClass.h"
#import "SRDiscoveryListViewController.h"
#import "SRDiscoveryMapViewController.h"
#import "SRDiscoveryRadarViewController.h"
#import "SRLoginViewController.h"
#import "SRMyConnectionsViewController.h"
#import "SRTrackOnMapViewController.h"
#import "SRAppDelegate.h"
#import "SRMyConnectionsViewController.h"
#import "SRTrackingViewController.h"
#import "SRGroupsListController.h"
#import "SREventsViewController.h"
#import "SRDropPinViewController.h"
#import "SRPageControlViewController.h"
#import "SRGroupTabViewController.h"
#import "SRDropPinViewController.h"
#import "SRDiscoveryRadarViewController.h"
#import "CommonFunction.h"
#import "SRPingsViewController.h"
#import "SRManagePersistentConnectionsViewController.h"
#import "DBManager.h"
#import "HSLocationManager.h"
#import "LocationAuthorizeViewController.h"
#import <Firebase.h>
#import <FirebaseCrashlytics/FirebaseCrashlytics.h>
#import "Serendipity-Bridging-Header.h"
#import "SRCodeViewController.h"
#import "HSLocalNotificationManger.h"
#import "KFKeychain.h"
#import "LocationFirstResponseViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@import MSAL;
@import TSLocationManager;
@import TSBackgroundFetch;
@import FirebaseMessaging;
@import CocoaAsyncSocket;
@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;
@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;
@import IQKeyboardManager;
#import "DDLog.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#if DEBUG
static const int ddLogLevel = DDLogLevelVerbose;
#else
static const int ddLogLevel = DDLogLevelInfo;
#endif

@implementation SRAppDelegate

@synthesize chatFriendListArr;
@synthesize chatGroupListArr;
@synthesize oldSourceArray;
@synthesize oldDataArray;
@synthesize lastLocation;
@synthesize oldConnDataArray;
@synthesize oldFamilyDataArray;
@synthesize oldConnTrackMeArray;
@synthesize oldConnTrackAmArray;
@synthesize groupTrackingArray;
@synthesize oldEventUserArray;
@synthesize reach;
@synthesize isSignUpSuccess;
@synthesize isSwipedCell;

#pragma mark - Group Chat Methods


- (NSMutableDictionary *)localSavingEventChat:(NSDictionary *)inEventDict withInvitee:(NSMutableArray *)inInviteeList isFetch:(BOOL)isFetch {
    NSString *fullPath = [self createGroupChatList:inEventDict[kKeyId]];
    
    NSMutableDictionary *eventDict;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:fullPath]) {
        eventDict = [[NSMutableDictionary alloc] initWithContentsOfFile:fullPath];
    } else {
        eventDict = [[NSMutableDictionary alloc] init];
    }
    
    if (!isFetch) {
        NSMutableArray *messageArr = [NSMutableArray array];
        
        [eventDict addEntriesFromDictionary:inEventDict];
        eventDict[kKeyGroupMembers] = inInviteeList;
        
        eventDict[kKeyMessage] = messageArr;
        
        [eventDict writeToFile:fullPath atomically:YES];
    }
    
    return eventDict;
}

- (NSMutableArray *)fetchAndSaveEventGroupChatList:(BOOL)isFetch forEventDict:(NSDictionary *)inDict {
    if ([[NSFileManager defaultManager] fileExistsAtPath:groupChatListPath]) {
        chatGroupListArr = [[NSMutableArray alloc] initWithContentsOfFile:groupChatListPath];
    } else {
        chatGroupListArr = [[NSMutableArray alloc] init];
    }
    
    if (!isFetch) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, inDict[kKeyId]];
        NSArray *filterArr = [chatGroupListArr filteredArrayUsingPredicate:predicate];
        
        if ([filterArr count] == 0) {
            [chatGroupListArr addObject:inDict];
            
            [self saveGroupChat];
        } else {
            NSInteger index = [chatGroupListArr indexOfObject:filterArr[0]];
            
            chatGroupListArr[index] = inDict;
            
            [self saveGroupChat];
        }
    }
    
    return chatGroupListArr;
}

#pragma mark - Chat Methods

- (NSString *)directoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *loggedInUser = [NSString stringWithFormat:@"%@", self.server.loggedInUserInfo[kKeyId]];
  //  NSLog(@"%@",loggedInUser);
    
    if ( [loggedInUser isEqualToString:@"(null)"]) {
        return @"";
    } else {
        NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:loggedInUser];
        NSError *error;
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];
        }
        
        return dirPath;
    }
}

- (NSString *)groupDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *loggedInUser = self.server.loggedInUserInfo[kKeyId];
    NSString *dirPath=@"";
    
    dirPath= [[documentsDirectory stringByAppendingPathComponent:loggedInUser]stringByAppendingPathComponent:@"Group_Folder"];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return dirPath;
}

- (NSString *)createUserChatList:(NSString *)userID {
    NSString *plistSubPath = [NSString stringWithFormat:@"%@.%@", userID, @"plist"];
    
    NSString *fullPath = [self directoryPath];
    fullPath = [fullPath stringByAppendingPathComponent:plistSubPath];
    
    return fullPath;
}

- (NSString *)createGroupChatList:(NSString *)groupId {
    NSString *plistSubPath = [NSString stringWithFormat:@"%@.%@", groupId, @"plist"];
    
    NSString *fullPath = [self groupDirectoryPath];
    fullPath = [fullPath stringByAppendingPathComponent:plistSubPath];
    
    return fullPath;
}

- (void)saveMessagesOfUser:(NSDictionary *)messageDict {
    NSMutableDictionary *userDict = [NSMutableDictionary dictionaryWithDictionary:messageDict[kKeyUser]];
    NSString *userPlistPath = [self createUserChatList:userDict[kKeyId]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *userMsgArr;
    
    if ([fileManager fileExistsAtPath:userPlistPath]) {
        userMsgArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
    } else {
        userMsgArr = [[NSMutableArray alloc] init];
    }
    
    [userMsgArr addObject:messageDict];
    
    [userMsgArr writeToFile:userPlistPath atomically:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyChatMessageRecieved object:userMsgArr];
    
    [self getUnreadMessageCount];
}

- (void)saveTypingStateOfUser:(NSDictionary *)messageDict {
    NSString *userPlistPath = [self createUserChatList:messageDict[kKeyId]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *userMsgArr;
    
    if ([fileManager fileExistsAtPath:userPlistPath]) {
        userMsgArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
    } else {
        userMsgArr = [[NSMutableArray alloc] init];
    }
    
    if (messageDict[kKeyCheckInMessage] == nil) {
        [userMsgArr addObject:messageDict];
    } else {
        NSMutableDictionary *composedDict = [NSMutableDictionary dictionary];
        
        composedDict[kKeyId] = messageDict[kKeyId];
        composedDict[kKeyCheckInMessage] = @"composing";
        composedDict[kKeyMessageStatus] = @"1";
        composedDict[kKeyOutgoingFlag] = @"0";
        
        if ([messageDict[kKeyCheckInMessage] isEqualToString:@"composing"]) {
            if (![userMsgArr containsObject:composedDict]) {
                [userMsgArr addObject:composedDict];
            }
        } else {
            if ([userMsgArr containsObject:composedDict]) {
                [userMsgArr removeObject:composedDict];
            }
        }
    }
    [userMsgArr writeToFile:userPlistPath atomically:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyChatTypingStateNotify object:userMsgArr];
}

- (NSMutableArray *)getUserMessage:(NSString *)userId {
    NSString *userPlistPath = [self createUserChatList:userId];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *userMsgArr;
    
    if ([fileManager fileExistsAtPath:userPlistPath]) {
        userMsgArr = [[NSMutableArray alloc] initWithContentsOfFile:userPlistPath];
    } else {
        userMsgArr = [NSMutableArray array];
    }
    
    return userMsgArr;
}

- (void)saveChatMessagesForUser:(NSString *)userId inChatArr:(NSArray *)inArr {
    NSString *userPlistPath = [self createUserChatList:userId];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:userPlistPath]) {
        [inArr writeToFile:userPlistPath atomically:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationMessageStateUpdated object:nil];
        
        [self getUnreadMessageCount];
    }
}

- (void)PathForPlistFile {
    chatListPath = [self directoryPath];
    chatListPath = [chatListPath stringByAppendingPathComponent:@"chat_list.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:chatListPath]) {
        chatFriendListArr = [[NSMutableArray alloc] initWithContentsOfFile:chatListPath];
    } else {
        chatFriendListArr = [[NSMutableArray alloc] init];
    }
    
    [chatFriendListArr writeToFile:chatListPath atomically:YES];
}

- (void)saveChatList {
    [chatFriendListArr writeToFile:chatListPath atomically:YES];
}

- (void)pathForGroupChatList {
    groupChatListPath = [self directoryPath];
    groupChatListPath = [groupChatListPath stringByAppendingPathComponent:@"groupchat_list.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:groupChatListPath]) {
        chatGroupListArr = [[NSMutableArray alloc] initWithContentsOfFile:groupChatListPath];
    } else {
        chatGroupListArr = [[NSMutableArray alloc] init];
    }
    
    [chatGroupListArr writeToFile:groupChatListPath atomically:YES];
}

- (void)saveGroupChat {
    [chatGroupListArr writeToFile:groupChatListPath atomically:YES];
}

- (void)deleteFriendFromList:(NSDictionary *)inUserDict {
    //NSInteger count = [self getUnreadMessageCount];
    
    if ([chatFriendListArr containsObject:inUserDict]) {
        [chatFriendListArr removeObject:inUserDict];
        
        if (inUserDict[kKeyFriendId] != nil) {
            NSString *userPlistPath = [self createUserChatList:inUserDict[kKeyFriendId]];
            NSError *error;
            
            if (![[NSFileManager defaultManager] removeItemAtPath:userPlistPath error:&error]) {
                //TODO: Handle/Log error
            }
        } else {
            NSString *fullPath = [self createUserChatList:inUserDict[kKeyId]];
            NSError *error;
            
            if (![[NSFileManager defaultManager] removeItemAtPath:fullPath error:&error]) {
                //TODO: Handle/Log error
            }
            
        }
    }
    
    [self saveChatList];
}

- (void)saveSendMessage:(NSDictionary *)inMessageDict {
    NSMutableDictionary *userDict = [NSMutableDictionary dictionaryWithDictionary:inMessageDict[kKeyUser]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyId, userDict[kKeyId]];
    NSArray *filterArr = [chatFriendListArr filteredArrayUsingPredicate:predicate];
    
    if ([filterArr count] == 0) {
        [chatFriendListArr addObject:userDict];
        
        [self saveChatList];
    } else {
        NSInteger index = [chatFriendListArr indexOfObject:filterArr[0]];
        
        chatFriendListArr[index] = userDict;
        
        [self saveChatList];
    }
    
    [self saveMessagesOfUser:inMessageDict];
}

- (NSInteger)getUnreadMessageCount {
    NSInteger count = 0;
    
    for (NSDictionary *friendIdDict in chatFriendListArr) {
        if (friendIdDict[kKeyFriendId] != nil) {
            NSString *friendId = friendIdDict[kKeyFriendId];
            NSMutableArray *chatArr = [self getUserMessage:friendId];
            if ([chatArr count] > 0) {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyMessageStatus, @"0"];
                
                [chatArr filterUsingPredicate:predicate];
                
                NSInteger unreadCount = [chatArr count];
                
                count = count + unreadCount;
            }
        }
    }
    
    for (NSDictionary *inGroupDict in chatGroupListArr) {
        NSString *eventId = inGroupDict[kKeyId];
        NSDictionary *params = @{kKeyId: eventId};
        NSMutableDictionary *eventDict = [self localSavingEventChat:params withInvitee:nil isFetch:YES];
        NSMutableArray *chatArr = eventDict[kKeyMessage];
        
        if ([chatArr count] > 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", kKeyMessageStatus, @"0"];
            
            [chatArr filterUsingPredicate:predicate];
            
            NSInteger unreadCount = [chatArr count];
            
            count = count + unreadCount;
        }
    }
    
    return count;
}

#pragma mark - XMMPP Manage Methods

#pragma mark Notifications API Methods

-(void)checkNotifications:(NSTimer*)timer {
    //  [self callNotificationsApi];
}

-(BOOL)isRestrictedAge:(int)age forCountry:(NSString*)countryCode {
    NSMutableDictionary *restriction = [NSMutableDictionary new];
    //    restriction[@"40"] =  @"14";
    //    restriction[@"56"] =  @"13";
    //    restriction[@"100"] =  @"14";
    //    restriction[@"191"] =  @"16";
    //    restriction[@"196"] =  @"14";
    //    restriction[@"203"] =  @"15";
    //    restriction[@"208"] =  @"13";
    //    restriction[@"233"] =  @"13";
    //    restriction[@"246"] =  @"13";
    //    restriction[@"250"] =  @"15";
    //    restriction[@"276"] =  @"16";
    //    restriction[@"300"] =  @"15";
    //    restriction[@"348"] =  @"16";
    //    restriction[@"352"] =  @"13";
    //    restriction[@"372"] =  @"16";
    //    restriction[@"380"] =  @"14";
    //    restriction[@"428"] =  @"13";
    //    restriction[@"438"] =  @"16";
    //    restriction[@"440"] =  @"16";
    //    restriction[@"442"] =  @"16";
    //    restriction[@"470"] =  @"16";
    //    restriction[@"578"] =  @"13";
    //    restriction[@"616"] =  @"13";
    //    restriction[@"620"] =  @"13";
    //    restriction[@"642"] =  @"16";
    //    restriction[@"703"] =  @"16";
    //    restriction[@"705"] =  @"15";
    //    restriction[@"724"] =  @"13";
    //    restriction[@"752"] =  @"13";
    //    restriction[@"756"] =  @"16";
    //    restriction[@"528"] =  @"16";
    //    restriction[@"826"] =  @"13";
    restriction[@"14"] = @"14"; // Austria
    restriction[@"21"] = @"13"; // Belgium
    restriction[@"33"] = @"14"; // Bulgaria
    restriction[@"54"] = @"16"; // Croatia
    restriction[@"56"] = @"14"; // Cyprus
    restriction[@"57"] = @"15"; // Czech
    restriction[@"58"] = @"13"; // Denmark
    restriction[@"67"] = @"13"; // Estonia
    restriction[@"72"] = @"13"; // Finland
    restriction[@"73"] = @"15"; // France
    restriction[@"80"] = @"16"; // Germany
    
    restriction[@"83"] = @"15"; // Greece
    restriction[@"97"] = @"16"; // Hungary
    restriction[@"98"] = @"13"; // Iceland
    restriction[@"103"] = @"16"; // Ireland
    restriction[@"105"] = @"14"; // Italy
    restriction[@"117"] = @"13"; // Latvia
    restriction[@"122"] = @"16"; // Liechtenstein
    restriction[@"123"] = @"16"; // Lithuania
    restriction[@"124"] = @"16"; // Luxembourg
    restriction[@"132"] = @"16"; // Malta
    
    restriction[@"160"] = @"13"; // Norway
    restriction[@"171"] = @"13"; // Poland
    restriction[@"172"] = @"13"; // Portugal
    restriction[@"176"] = @"16"; // Romania
    restriction[@"193"] = @"16"; // Slovakia
    restriction[@"194"] = @"15"; // Slovenia
    restriction[@"199"] = @"13"; // Spain
    restriction[@"205"] = @"13"; // Sweden
    restriction[@"206"] = @"16"; // Switzerland
    restriction[@"150"] = @"16"; // The Netherlands
    restriction[@"151"] = @"16"; // The Netherlands ANTILLES
    restriction[@"225"] = @"13"; // United Kingdom
    
    NSString *requiredAge = restriction[countryCode];
    if (requiredAge != nil) {
        if ([requiredAge intValue] > age) {
            return YES;
        }
    } else {
        if (age < 13) {
            return YES;
        }
    }
    return NO;
}

- (void)callNotificationsApi {
    if (self.server.loggedInUserInfo[kKeyId] != nil || (!self.isLogoutCall)) {
        NSString *strUrl = [NSString stringWithFormat:@"%@%@?token=%@", kKeyClassGetNotification, self.server.loggedInUserInfo[kKeyId], [_server.token stringByReplacingOccurrencesOfString:@"Bearer " withString:@""]];
        [self.server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
        strUrl = [NSString stringWithFormat:@"%@?token=%@", kKeyClassGetTrackingNotification, [_server.token stringByReplacingOccurrencesOfString:@"Bearer " withString:@""]];
        
        [_server makeAsychronousRequest:strUrl inParams:nil isIndicatorRequired:NO inMethodType:kGET];
        
        [[SRAPIManager sharedInstance] getNotificationsForUser:self.server.loggedInUserInfo[kKeyId] completion:^(id  _Nonnull result, NSError * _Nonnull error) {
            if (!error) {
                
            }
        }];
    }
}

#pragma mark Activity indicator Methods

- (void)showActivityIndicator {
    // [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    activityIndicator.center = self.window.center;
    
    CGRect frame = activityIndicator.frame;
    
    activityIndicator.frame = frame;
    
    [self.window addSubview:activityIndicator];
    
    activityIndicator.hidden = NO;
    
    [activityIndicator.activityIndicator startAnimating];
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
}

- (void)hideActivityIndicator {
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [activityIndicator removeFromSuperview];
    
    activityIndicator.hidden = YES;
    
    [activityIndicator.activityIndicator stopAnimating];
}

- (void)hsshowActivityIndicator {
    // [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    hsactivityIndicator.center = self.window.center;
    
    CGRect frame = hsactivityIndicator.frame;
    
    hsactivityIndicator.frame = frame;
    
    [self.window addSubview:hsactivityIndicator];
    
    hsactivityIndicator.hidden = NO;
    
    [hsactivityIndicator.activityIndicator startAnimating];
}

- (void)hshideActivityIndicator {
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    [hsactivityIndicator removeFromSuperview];
    
    hsactivityIndicator.hidden = YES;
    
    [hsactivityIndicator.activityIndicator stopAnimating];
}


#pragma mark - Private Method

- (void)setUpTabBar {
    if (isAfterLogout) {
        self.tabBarController = [[UITabBarController alloc] init];
    }
    UIColor *appTintColor = [UIColor colorWithRed:255 / 255.0 green:149 / 255.0 blue:0 alpha:1.0];
    self.tabBarController.tabBar.translucent = YES;
    self.tabBarController.tabBar.tintColor = [UIColor whiteColor];
    [self.tabBarController.tabBar setShadowImage:nil];
    [self.tabBarController.tabBar invalidateIntrinsicContentSize];
    
    tabsConArr = [NSMutableArray new];
    
    UIImage *image = [SRModalClass createImageWith:[UIColor blackColor]];
    self.tabBarController.tabBar.backgroundImage = image;
    // Set Discovery Map tab
    SRDiscoveryMapViewController *discoveryMapCon = [[SRDiscoveryMapViewController alloc]initWithNibName:nil bundle:nil server:self.server];
    UINavigationController *discoveryMapNavBarCon = [[UINavigationController alloc]initWithRootViewController:discoveryMapCon];
    UIImage *tabImage = [UIImage imageNamed:@"tabbar-map.png"];
    
    UITabBarItem *discoveryMapTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"tab.map.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-map-active.png"]];
    [discoveryMapTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    discoveryMapNavBarCon.tabBarItem = discoveryMapTabItem;
    [tabsConArr addObject:discoveryMapNavBarCon];
    
    //  Radar Tab
    SRDiscoveryRadarViewController *discoveryRadarCon = [[SRDiscoveryRadarViewController alloc] initWithNibName:nil bundle:nil server:self.server];
    UINavigationController *discoveryRadarNav = [[UINavigationController alloc]initWithRootViewController:discoveryRadarCon];
    tabImage = [UIImage imageNamed:@"tabbar-radar.png"];
    UITabBarItem *discoveryRadarTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"tab.radar.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-radar-active.png"]];
    [discoveryRadarTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    discoveryRadarNav.tabBarItem = discoveryRadarTabItem;
    [tabsConArr addObject:discoveryRadarNav];
    
    // Set list tab
    SRDiscoveryListViewController *discoveryListCon = [[SRDiscoveryListViewController alloc]initWithNibName:nil bundle:nil server:self.server];
    UINavigationController *discoveryListNavBarCon = [[UINavigationController alloc]initWithRootViewController:discoveryListCon];
    tabImage = [UIImage imageNamed:@"tabbar-list.png"];
    UITabBarItem *discoveryListTabItem = [[UITabBarItem alloc]initWithTitle:NSLocalizedString(@"tab.list.title", @"") image:tabImage selectedImage:[UIImage imageNamed:@"tabbar-list-active.png"]];
    [discoveryListTabItem setImage:[tabImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    discoveryListNavBarCon.tabBarItem = discoveryListTabItem;
    [tabsConArr addObject:discoveryListNavBarCon];
    
    // Add controllers to tab bar
    self.tabBarController.viewControllers = [NSArray arrayWithArray:tabsConArr];
    
    // Set default selected index and image
    self.tabBarController.tabBar.unselectedItemTintColor = appTintColor;
    self.tabBarController.selectedIndex = 1;
    
    
    NSInteger topSafeArea = 0;
    
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topSafeArea = window.safeAreaInsets.top;
    }
    
    [self.topBarView removeFromSuperview];
    self.topBarView = nil;
    
    self.topBarView = [[SRDiscoveryTopBarView alloc] initWithFrame:CGRectMake(0, 44 + topSafeArea, SCREEN_WIDTH, 75)];
    [self.tabBarController.view addSubview:self.topBarView];
    
    // self.window.rootViewController = self.tabBarController;
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaMedium size:10.0f],NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName : [UIFont fontWithName:kFontHelveticaRegular size:10.0f],NSForegroundColorAttributeName : appTintColor } forState:UIControlStateNormal];
    
    [self setTabBarActions];
}

-(void)setTabBarActions {
    //
    UILongPressGestureRecognizer *tabsLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressOnTabs:)];
    tabsLongPress.minimumPressDuration = 1.0f;
    [self.tabBarController.tabBar addGestureRecognizer:tabsLongPress];
}

- (void)longPressOnTabs:(UILongPressGestureRecognizer *)recognizer {
    UIViewController *parent = self.tabBarController.viewControllers[self.tabBarController.selectedIndex];
    if (parent != nil) {
        UINavigationController *nav = (UINavigationController*)parent;
        [(SRHomeParentViewController*)[nav viewControllers][0] refreshControllerData];
    }
}

- (void)setBadgeCount {
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@)", @"0"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(status == %@ AND reference_type_id != %@)", @"0", @"14"];

    NSArray *filterArray = [_server.notificationArr filteredArrayUsingPredicate:predicate];
    
    NSInteger count = [[DBManager getSharedInstance] getUnreadMsgCount];
    count = count + [(APP_DELEGATE) getUnreadTrackingLocationCount];
    count = count + [filterArray count];
    
    if ([_server.loggedInUserInfo valueForKey:kKeyId]) {
        if (count == 0) {
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        } else {
            [UIApplication sharedApplication].applicationIconBadgeNumber = count;
        }
    }
}

#pragma mark - Application Life Cycle Method

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
//    IQKeyboardManager.sharedManager.enable = false;
//    [FIRMessaging messaging].autoInitEnabled = YES;
    [FIRMessaging messaging].delegate = self;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *contactimportStatus = [userDefaults valueForKey:kKeyIsImportPhoneContact];
    if (contactimportStatus == (id)[NSNull null] || contactimportStatus.length == 0 || contactimportStatus == nil){
        [userDefaults setValue:@"yes" forKey:kKeyIsImportPhoneContact];
        [userDefaults synchronize];
    }
    if (![self getDeviceId]){
        NSString* deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [self saveDeviceID:deviceId];
    }
    
    NSLog(@"%@",[self getDeviceId]);
    
    
//    if (launchOptions[UIApplicationLaunchOptionsLocationKey]){
//    [[TSLocationManager sharedInstance] onProviderChange:^(TSProviderChangeEvent *event) {
//        CLAuthorizationStatus status = event.status;
//        NSLog(@"[providerchange] enabled: %d",status);
//        if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied || ([APP_DELEGATE isLocationTutorialDone] && status == kCLAuthorizationStatusNotDetermined)){
//            [[HSLocalNotificationManger sharedManager] triggerLocationNotification:@"" :kKeyLocationPermission];
//        }else  if(status == kCLAuthorizationStatusAuthorizedAlways){
//            [[HSLocalNotificationManger sharedManager] removeLocationNotification];
//        }
//    }];

    isNotificationFromClosed = NO;
    [MSAppCenter start:@"4eafed82-442f-43b0-99bf-c444b4c9c645" withServices:@[
        [MSAnalytics class],
        [MSCrashes class]
    ]];
    
    if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) {
        addTextInSignificantTxt(@"UIApplicationLaunchOptionsRemoteNotificationKey");
        addTextInSignificantTxt(isNotificationFromClosed ? @"Yes" : @"No");
        isNotificationFromClosed=TRUE;
        addTextInSignificantTxt(isNotificationFromClosed ? @"Yes" : @"No");
    }
    
    //    [Instabug startWithToken:@"2b007fb364f50e5aa38c7a04e53d1aa0" invocationEvents: IBGInvocationEventShake | IBGInvocationEventScreenshot];
    
    
    
    
    self.isFromNotification = NO;
    self.timer = YES;
    self.isBackMenu = NO;
    self.isOnMap = NO;
    
    self.isTrackUserSelected = NO;
    self.lblLocationText = @"Updating Location...";
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:@"Significant.txt"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
//     [defaultCenter addObserver:self
//                         selector:@selector(EmailSendSucceded:)
//                             name:kKeyNotificationSendEmailSuccess
//                           object:nil];
//
//    [defaultCenter addObserver:self
//                         selector:@selector(EmailSendFailed:)
//                             name:kKeyNotificationSendEmailFail
//                           object:nil];
    
    
    [defaultCenter addObserver:self
                      selector:@selector(loginSucceded:)
                          name:kKeyNotificationLoginSuccess
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(logoutSucceed:)
                          name:kKeyNotificationLogoutSucceed
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(updateActiveStatusSucceed:)
                          name:kKeyNotificationUpdateActiveStatusSucceed
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(updateActiveStatusFailed:)
                          name:kKeyNotificationUpdateActiveStatusFailed
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(deleteAccountSucceed:)
                          name:kKeyNotificationDeleteAccountSucceed
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(getTrackingNotificationSucceed:)
                          name:kKeyNotificationGetTrackingNotificationSucceed
                        object:nil];
    
    [defaultCenter addObserver:self
                      selector:@selector(getTrackingNotificationFailed:)
                          name:kKeyNotificationGetTrackingNotificationFailed
                        object:nil];
    
    self.server = [[SRServerConnection alloc] initWithURl:kSerendipityServer];
    
    oldSourceArray = [[NSMutableArray alloc]init];
    tabsConArr = [[NSMutableArray alloc]init];
    oldDataArray = [[NSMutableArray alloc]init];
    oldConnDataArray = [[NSMutableArray alloc] init];
    oldFamilyDataArray = [[NSMutableArray alloc] init];
    oldConnTrackMeArray = [[NSMutableArray alloc] init];
    oldConnTrackAmArray = [[NSMutableArray alloc] init];
    oldEventUserArray = [[NSMutableArray alloc] init];
    isSignUpSuccess = NO;
    isSwipedCell = NO;
    _isLogoutCall = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    self.reach = [Reachability reachabilityForInternetConnection];
    
    [reach startNotifier];
    
    NetworkStatus remoteHostStatus = [self.reach currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        [self hideActivityIndicator];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        dispatch_async(dispatch_get_main_queue(),^{
            objInternetView = [[SRNoInternetView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 20)];
            
            [[[UIApplication sharedApplication] keyWindow] addSubview:objInternetView];
        });
    }
    
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:(0)];
    
    [self setRootView];
    
    activityIndicator = [[SRActivityIndicator alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    
    hsactivityIndicator = [[SRActivityIndicator alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];

    
    UNUserNotificationCenter * notificationCenter = [UNUserNotificationCenter currentNotificationCenter];
    
    notificationCenter.delegate = self;
    
    [notificationCenter requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge)
                                      completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
        }
    }];
    
    [GMSServices provideAPIKey:kKeyGoogleMap];
    [GMSPlacesClient provideAPIKey:kKeyGoogleMap];
    
    [GIDSignIn sharedInstance].clientID = kKeyClientID;
    
    //[DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self showLocationAuthorizationFailedViewController];
    }
    
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                 message:@"The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh"
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
            //
        }]];
        
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    } else if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""
                                                                                 message:@"The functions of this app are limited because the Background App Refresh is disable."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
            //
        }]];
        
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    } else {
        if (launchOptions[UIApplicationLaunchOptionsLocationKey]) {
            addTextInSignificantTxt([NSString stringWithFormat:@"Relaunched"]);
            addTextInSignificantTxt([NSString stringWithFormat:@"Posting kKeyNotificationUpdateLocationInBackground notification from DidFinishLaunching"]);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateLocationInBackground object:nil userInfo:nil];
        } else {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            [userDefault setObject:@NO forKey:SIGNIFICANT_LOCATION_CHANGE];
            [userDefault synchronize];
            
            addTextInSignificantTxt([NSString stringWithFormat:@"UIApplicationLaunchOptionsLocationKey : %@", launchOptions[UIApplicationLaunchOptionsLocationKey]]);
            addTextInSignificantTxt([NSString stringWithFormat:@"Posting kKeyNotificationStopUpdateLocationInBackground notification from DidFinishLaunching"]);
            
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationStopUpdateLocationInBackground object:nil userInfo:nil];
        }
    }
    
    
    
    NSString *loginDataPath = paths[0];
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    
    if ([fileManager fileExistsAtPath:loginDataPath]) {
        
    } else {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    
    [self copyDatabaseIfNeeded];
    
    [NSTimer scheduledTimerWithTimeInterval:30.0f target:self selector:@selector(checkNotifications:) userInfo:nil repeats:YES];
    
    [[FIRCrashlytics crashlytics] sendUnsentReports];
    
    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    TSBackgroundFetch *fetchManager = [TSBackgroundFetch sharedInstance];
    
    [fetchManager performFetchWithCompletionHandler:completionHandler applicationState:application.applicationState];
}

- (void)showLocationEnableViewController:(UIViewController*)container {
    UIStoryboard *locationStoryBoard = [UIStoryboard storyboardWithName:@"LocationEnable" bundle:nil];
    UIViewController *locationViewController = [locationStoryBoard instantiateInitialViewController];
    [locationViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    if (container) {
        [container presentViewController:locationViewController animated:YES completion:nil];
    } else {
        [self.window.rootViewController presentViewController:locationViewController animated:YES completion:nil];
    }
}

- (void)showLocationFirstResponeViewController:(UIViewController*)container {
    UIStoryboard *locationStoryBoard = [UIStoryboard storyboardWithName:@"LocationFirstResponse" bundle:nil];
    UIViewController *locationViewController = [locationStoryBoard instantiateInitialViewController];
    [locationViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    if (container) {
        [container presentViewController:locationViewController animated:YES completion:nil];
    } else {
        [self.window.rootViewController presentViewController:locationViewController animated:YES completion:nil];
    }
}

- (void)showLocationAuthorizationFailedViewController {
    //    UIStoryboard *locationStoryBoard = [UIStoryboard storyboardWithName:@"LocationFirstResponse" bundle:nil];
    //    UIViewController *locationViewController = [locationStoryBoard instantiateViewControllerWithIdentifier:@"LocationAuthorizationCheck"];
    //    [locationViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    //    [self.window.rootViewController presentViewController:locationViewController animated:YES completion:nil];
}

-(NSString *)getAdditionalInfo {
    NSString *strInfo,*strLocationSetting = @"";
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *build = infoDictionary[(NSString*)kCFBundleVersionKey];
    NSString *iOSVersion=[UIDevice currentDevice].systemVersion;
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways) {
        strLocationSetting = @"Always";
    } else if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse) {
        strLocationSetting = @"WhileinUse";
    } else if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
        strLocationSetting = @"Denied";
    } else if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted) {
        strLocationSetting = @"Restricted";
    } else if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined) {
        strLocationSetting = @"NotDetermined";
    }
    
    strInfo=[NSString stringWithFormat:@"%@|%@|%@",build,iOSVersion,strLocationSetting];
    
    return strInfo;
}

- (void)copyDatabaseIfNeeded {
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:getDBPath()];
    
    if (!success) {
        NSString *databasePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"trackingDB.sqlite"];
        [fileManager copyItemAtPath:databasePath toPath:getDBPath() error:nil];
    }
}

- (void)setRootView {
    preLoginViewCon = [[SRPreLoginViewController alloc] initWithNibName:nil bundle:nil server:self.server];
    
    preNavCon = [[UINavigationController alloc] initWithRootViewController:preLoginViewCon];
    
    [self.window setRootViewController:preNavCon];
    [self.window makeKeyAndVisible];
}

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

    return [[GIDSignIn sharedInstance] handleURL:url] || [MSALPublicClientApplication handleMSALResponse:url sourceApplication:sourceApplication];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateLocationInBackground object:nil userInfo:nil];
    
    if (![[HSLocationManager sharedManager]isLocationManagerRunning] && self.isLoggedInSuccess){
        [[HSLocationManager sharedManager] startLocationManger:^(CLAuthorizationStatus status) {
            NSLog(@"Meet Location Status:%d",status);
        }];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self setBadgeCount];
    
    //Neha
       self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
               [[UIApplication sharedApplication] endBackgroundTask: self.backgroundTask];
               self.backgroundTask = UIBackgroundTaskInvalid;
           //    NSLog(@"background task ended");
           }];
       //Neha
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setValue:self.server.loggedInUserInfo[kKeyId] forKey:kKeyUserID];
    
    [self.server makeAsychronousRequest:kKeyClassUpdateUserActiveStatus inParams:dict isIndicatorRequired:NO inMethodType:kPOST];
    wasInBackground = true;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationBadgeCountUpdated object:nil];
    
    BOOL locationAllowed = [CLLocationManager locationServicesEnabled];
    
    if (!locationAllowed) {
        //  [self showLocationEnableViewController];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationServicesClose" object:nil];
    }else{
        [self showLocationAuthorizationFailedViewController];
    }

    UIViewController* currentVC = [[HSLocationManager sharedManager] topViewController];

    
    //CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    CLAuthorizationStatus status = [[HSLocationManager sharedManager] authStatus];
    
    if (self.isLoggedInSuccess && status != kCLAuthorizationStatusAuthorizedAlways && (![currentVC isKindOfClass:LocationAuthorizeViewController.self])) {
        [self showTutorial:currentVC];
    } else if (status == kCLAuthorizationStatusAuthorizedAlways && ([currentVC isKindOfClass:LocationAuthorizeViewController.self] || [currentVC isKindOfClass:LocationFirstResponseViewController.self])) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"locationTutorialDone"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [currentVC dismissViewControllerAnimated:NO completion:^{
                [self showContactTutorial];
        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationTutorialDone" object:nil];
    }
    
    //UIViewController* currentVC = [[HSLocationManager sharedManager] topViewController];
    
    if (![[HSLocationManager sharedManager]isLocationManagerRunning] && [[NSUserDefaults standardUserDefaults] boolForKey:@"locationTutorialDone"]){
        [[HSLocationManager sharedManager] startLocationManger:^(CLAuthorizationStatus status) {
            if (status == kCLAuthorizationStatusAuthorizedWhenInUse && (![currentVC isKindOfClass:LocationAuthorizeViewController.self]) && self.isLoggedInSuccess){
                [self showTutorial:currentVC];
            }
        }];
    }else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse && (![currentVC isKindOfClass:LocationAuthorizeViewController.self]) && self.isLoggedInSuccess) {
        [self showTutorial:currentVC];
    }
}
- (BOOL)isLoggedIn {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:loginDataPath];
}

- (BOOL)isLocationTutorialDone {
    BOOL result = [[NSUserDefaults standardUserDefaults] boolForKey:@"locationTutorialDone"];
    return result;
}

- (void)showTutorial:(UIViewController*)container {
    self.isLoggedInSuccess = YES;
        if (! [CLLocationManager locationServicesEnabled] ) {
            [self showLocationEnableViewController:container];
        }else{
            CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
            switch (status) {
                case kCLAuthorizationStatusDenied:
                case kCLAuthorizationStatusRestricted:
                case kCLAuthorizationStatusNotDetermined:{
                    [self showLocationFirstResponeViewController:container];
                }break;
                case kCLAuthorizationStatusAuthorizedWhenInUse: {
                    [self showLocationFirstResponeViewController:container];
                } break;
                    
                case kCLAuthorizationStatusAuthorizedAlways: {
                    [self showContactTutorial];
                   // NSLog(@"HS:Alway permission");
                } break;
                    
                default:
                    
                    break;
                    
            }
        }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    addTextInSignificantTxt(@"did Become Active called");
    //    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self setBadgeCount];
    
    addTextInSignificantTxt([NSString stringWithFormat:@"Posting kKeyNotificationUpdateLocationInBackground notification from applicationWillTerminate"]);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationUpdateLocationInBackground object:nil userInfo:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - App Push Delegate

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSString *deviceKey = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    deviceKey = [deviceKey stringByReplacingOccurrencesOfString:@" " withString:@""];
//    deviceKey = [deviceKey stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//    self.server.deviceToken = deviceKey;

//    [FIRMessaging messaging].APNSToken = deviceToken;

//    const unsigned char *buffer = deviceToken.bytes;
//    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(deviceToken.length * 2)];
//    for (int i = 0; i < deviceToken.length; ++i) {
//        [hexString appendFormat:@"%02x", buffer[i]];
//    }
//
//    self.server.deviceToken = [hexString copy];
//
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//
//    [userDefaults setValue:self.server.deviceToken forKey:@"deviceToken"];
//    [userDefaults synchronize];

    [FIRMessaging messaging].APNSToken = deviceToken;

}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
  //  NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FCMToken" object:nil userInfo:dataDict];

    self.server.deviceToken = fcmToken;

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    [userDefaults setValue:fcmToken forKey:@"deviceToken"];
    [userDefaults synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    //HS:Patch for simulator
    NSString* deviceToken = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    self.server.deviceToken = deviceToken;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:deviceToken forKey:@"deviceToken"];
    [userDefaults synchronize];

}

- (void)handleNotificationBackground:(NSDictionary *)userInfo {
    if ([userInfo valueForKey:kKeyType]) {
        dictUserInfo = userInfo;
        _notificationType = [[userInfo valueForKey:kKeyType]integerValue];
//        _isFromNotification = YES;
        isAfterTrackNoti = YES;
        
//        [self callNotificationsApi];
        
//        if (isNotificationFromClosed == FALSE) {
            if ([[userInfo valueForKey:kKeyType] integerValue] == 1 || [[userInfo valueForKey:kKeyType] integerValue] == 2) {
                
                SRMyConnectionsViewController *objConection = [[SRMyConnectionsViewController alloc]initWithNibName:nil bundle:nil server:_server];
                [self.window setRootViewController:self.tabBarController];
                [self.window makeKeyAndVisible];
                objConection.hidesBottomBarWhenPushed = YES;
                [((UINavigationController*)self.tabBarController.selectedViewController) pushViewController:objConection animated:true];
                ((UINavigationController*)self.tabBarController.selectedViewController).viewControllers[0].hidesBottomBarWhenPushed = NO;
            } else if ([[userInfo valueForKey:kKeyType] integerValue] == 5 || [[userInfo valueForKey:kKeyType] integerValue] == 6 || [[userInfo valueForKey:kKeyType] integerValue] == 8 || [[userInfo valueForKey:kKeyType] integerValue] == 11 || [[userInfo valueForKey:kKeyType] integerValue] == 12) {
                //Open tracking screen
                SRTrackingViewController *objMap = [[SRTrackingViewController alloc] initWithNibName:nil bundle:nil profileData:userInfo server:_server];
                [self.window setRootViewController:self.tabBarController];
                [self.window makeKeyAndVisible];
                objMap.hidesBottomBarWhenPushed = YES;
                [((UINavigationController*)self.tabBarController.selectedViewController) pushViewController:objMap animated:true];
                ((UINavigationController*)self.tabBarController.selectedViewController).viewControllers[0].hidesBottomBarWhenPushed = NO;
                
            } else if ([[userInfo valueForKey:kKeyType] integerValue] == 9) {
                SRDropPinViewController *objDropPin = [[SRDropPinViewController alloc]initWithNibName:nil bundle:nil server:_server];
                
                [self.window setRootViewController:self.tabBarController];
                [self.window makeKeyAndVisible];
                objDropPin.hidesBottomBarWhenPushed = YES;
                [((UINavigationController*)self.tabBarController.selectedViewController) pushViewController:objDropPin animated:true];
                ((UINavigationController*)self.tabBarController.selectedViewController).viewControllers[0].hidesBottomBarWhenPushed = NO;
                [self callNotificationsApi];
            } else if ([[userInfo valueForKey:kKeyType] integerValue] == 13) {
                //Open chat screen
                SRPingsViewController *objChatView = [[SRPingsViewController alloc] initWithNibName:nil bundle:nil server:_server];
                [self.window setRootViewController:self.tabBarController];
                [self.window makeKeyAndVisible];
                objChatView.hidesBottomBarWhenPushed = YES;
                [((UINavigationController*)self.tabBarController.selectedViewController) pushViewController:objChatView animated:true];
                ((UINavigationController*)self.tabBarController.selectedViewController).viewControllers[0].hidesBottomBarWhenPushed = NO;
            } else if ([[userInfo valueForKey:kKeyType] integerValue] == 3) {
                //Open group screen
                [self.window setRootViewController:self.tabBarController];
                [self.window makeKeyAndVisible];
                SRGroupTabViewController *objGroupTab = [[SRGroupTabViewController alloc] initWithNibName:nil bundle:nil];
                objGroupTab.hidesBottomBarWhenPushed = true;
                [((UINavigationController*)self.tabBarController.selectedViewController) pushViewController:objGroupTab animated:YES];
            } else if ([[userInfo valueForKey:kKeyType] integerValue] == 4 || [[userInfo valueForKey:kKeyType] integerValue] == 10) {
                //Open Event screen
                [self.window setRootViewController:self.tabBarController];
                [self.window makeKeyAndVisible];
                SREventTabViewController *eventTab = [[SREventTabViewController alloc] initWithNibName:nil bundle:nil server:_server className:nil];
                eventTab.hidesBottomBarWhenPushed = true;
                [((UINavigationController*)self.tabBarController.selectedViewController) pushViewController:eventTab animated:YES];
            } else {
//                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLoginSuccess object:nil];
            }
        remoteNotification = nil;
        }
//    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSDictionary *dictData = [[NSDictionary alloc]init];
    
    dictData = notification.request.content.userInfo;
    
    //write condition for logged in user
    if (_server.loggedInUserInfo != nil) {
        [self callNotificationsApi];
    }
    
    
    if ([dictData valueForKey:kKeyType]) {
        completionHandler(UNNotificationPresentationOptionAlert);
    }
    
    if ([[dictData valueForKey:@"isFirebaseNotification"] isEqualToString:@"1"]) {
        completionHandler(UNNotificationPresentationOptionAlert);
    }
    
    if ([[notification request].identifier containsString:@"HS_Location"]){
//         completionHandler(UNNotificationPresentationOptionAlert);
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler {
    [self setBadgeCount];
    
    NSDictionary *dictData=[[NSDictionary alloc]init];
    dictData=response.notification.request.content.userInfo;
    
    if ([dictData valueForKey:kKeyType]) {
        if ([[dictData valueForKey:kKeyType] integerValue] == 0) {
            completionHandler();
        } else {
            if (!wasInBackground && !([UIApplication.sharedApplication applicationState] == UIApplicationStateActive)) {
                remoteNotification = response.notification.request.content.userInfo;
                [self setRootView];
            } else {
                [self callNotificationsApi];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self handleNotificationBackground:response.notification.request.content.userInfo];
                });
                
            }
        }
    }
    
    if ([[dictData valueForKey:@"isFirebaseNotification"] isEqualToString:@"1"] && isNotificationFromClosed==FALSE) {
        isAfterTrackNoti = YES;
        _isFromNotification=YES;
        
        SRPingsViewController *objChat = [[SRPingsViewController alloc]initWithNibName:nil bundle:nil server:_server];
        
        preNavCon = [[UINavigationController alloc]initWithRootViewController:objChat];
        
        [self.window setRootViewController:preNavCon];
        [self.window makeKeyAndVisible];
    }
    
    if ([[dictData valueForKey:@"isFirebaseNotification"] isEqualToString:@"1"] && isNotificationFromClosed==TRUE) {
        _isFromNotification=YES;
        isAfterTrackNoti = YES;
        _notificationType=10;
        
        SRChatViewController *objChatView = [[SRChatViewController alloc] initWithNibName:nil bundle:nil server:_server inObjectInfo:dictData];
        preNavCon = [[UINavigationController alloc]initWithRootViewController:objChatView];
        
        [self.window setRootViewController:preNavCon];
        [self.window makeKeyAndVisible];
    }
    
    if ([response.notification.request.identifier containsString:@"HS_Location"]){
//        NSURL* url = [[NSURL alloc]initWithString:UIApplicationOpenSettingsURLString];
//
//        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

#pragma mark - TabBarController Delegate

- (void)tabBarController:(UITabBarController *)inTabBarController didSelectViewController:(UIViewController *)viewController {
    //
}

#pragma mark - XMPP Methods


#pragma mark XMPP Connect Discoonect


#pragma mark XMPP Delegate

// -----------------------------------------------------------------------------
// didSendMessage:

#pragma mark XMPPStream Register user

#pragma mark - Roster delegate Delegate

#pragma mark - Group Chat XMPP Delegate

// ----------------------------------------------------------------------------
// xmppRoomDidCreate:

// ----------------------------------------------------------------------------
// xmppRoomDidJoin:

// ----------------------------------------------------------------------------
// didFetchConfigurationForm:

// ----------------------------------------------------------------------------
// didConfigure:

#pragma mark - Notifications Method

- (void)signUpCompleted:(NSNotification *)inNotify {
    [self setUpTabBar];
    
    [self.window setRootViewController:self.tabBarController];
}

-(void)toolTipFlagSaveData {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:kKeyRadarToolTipFlag]boolValue ]) {
        self.tooltipRadarViewFlag = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyRadarToolTipFlag];
    }
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:kKeyMapToolTipFlag]boolValue ]) {
        self.tooltipMapViewFlag = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyMapToolTipFlag];
    }
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:kKeyListToolTipFlag] boolValue]) {
        self.tooltipListViewFlag = [[NSUserDefaults standardUserDefaults] valueForKey:kKeyListToolTipFlag];
    }
    
    [userdef setBool:self.tooltipRadarViewFlag forKey:kKeyRadarToolTipFlag];
    [userdef setBool:self.tooltipMapViewFlag forKey:kKeyMapToolTipFlag];
    [userdef setBool:self.tooltipListViewFlag forKey:kKeyListToolTipFlag];
    
    [userdef synchronize];
}

-(void)setInitialController {
    NSUserDefaults *userdef = [NSUserDefaults standardUserDefaults];
    
    [self hideActivityIndicator];
    
    if (isAfterTrackNoti) {
        isAfterLogout = YES;
        
        [tabsConArr removeAllObjects];
        
        self.tabBarController = nil;
        
        [self hideActivityIndicator];
    }
    
    [self setUpTabBar];
    [self.window setRootViewController:self.tabBarController];
    [self callNotificationsApi];
    
    [self PathForPlistFile];
    [self pathForGroupChatList];
    
    [[DBManager getSharedInstance] getUnreadMsgCount];
    
    if ([[self.server.loggedInUserInfo valueForKey:kKeySetting] isKindOfClass:[NSDictionary class]] && ![[[self.server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kkeyDistanceAccuracy]  isEqualToString:@""]) {
        [userdef setValue:[[[self.server.loggedInUserInfo valueForKey:kKeySetting]valueForKey:kkeyDistanceAccuracy]stringByAppendingString:@" M"] forKey:kKeyMeters_Data];
    } else {
        [userdef setValue:@"0 M" forKey:kKeyMeters_Data];
    }
    
    [userdef synchronize];
}

- (void)showContactTutorial{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![[userDefaults valueForKey:kKeyIsImportPhoneContact] isEqual:@"yes"]){
        UIStoryboard *locationStoryBoard = [UIStoryboard storyboardWithName:@"LocationFirstResponse" bundle:nil];
        UIViewController *contactPermissionVC = [locationStoryBoard instantiateViewControllerWithIdentifier:@"NavSRContactPermissionVC"];
        [contactPermissionVC setModalPresentationStyle:UIModalPresentationFullScreen];
        
        [self.window.rootViewController presentViewController:contactPermissionVC animated:YES completion:nil];
        // locationViewController.delegate = self;
    }else{
        
    }
}
- (void)loginSucceded:(NSNotification *)inNotify {
    addTextInSignificantTxt(@"loginSucceded");
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    NSMutableDictionary *loggedInDict = [[NSMutableDictionary alloc] init];
    
    NSString *currentCountryId = @"";
    int currentAge = 0;
    
//    [SRModalClass showAlert:inNotify.description];
    
    if ([[self.server.loggedInFbUserInfo valueForKey:kKeyIsFromFacebook] isEqualToString:@"1"]) {
        [loggedInDict setValue:self.server.loggedInFbUserInfo[kKeyEmail] forKey:kKeyEmail];
        [loggedInDict setValue:[self.server.loggedInFbUserInfo valueForKey:kKeyId] forKey:kKeyId];
        [loggedInDict setValue:@"1" forKey:kKeyIsFromFacebook];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
        [userDefaults synchronize];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *loginDataPath = paths[0];
        
        loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateOfBirth = [dateFormatter dateFromString:[self.server.loggedInUserInfo objectForKey:@"dob"]];
        currentCountryId = [[self.server.loggedInUserInfo valueForKey:@"country"] valueForKey:@"numcode"];
        NSDate *now = [NSDate date];
        NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                           components:NSCalendarUnitYear
                                           fromDate:dateOfBirth
                                           toDate:now
                                           options:0];
        currentAge = [ageComponents year];
        
        if ([fileManager fileExistsAtPath:loginDataPath]) {
            NSError *error;
            [fileManager removeItemAtPath:loginDataPath error:&error];
        }
        if (![self isRestrictedAge:currentAge forCountry:currentCountryId]) {
            BOOL success= [loggedInDict writeToFile:loginDataPath atomically:YES];
            
            if (success) {
                addTextInSignificantTxt(@"Login Credential written successfully");
            } else {
                addTextInSignificantTxt(@"Login Credential written Unsuccessfully");
            }
        }
    } else {
        NSDictionary *dictUserInfo = [inNotify userInfo];
        NSDictionary *dictParam = [dictUserInfo valueForKey:@"requestedParams"];
        NSDictionary *dictResponse = [dictUserInfo valueForKey:@"response"];
        NSDictionary *dictUserData = [dictResponse valueForKey:@"user"];
        
        loggedInDict= [[NSMutableDictionary alloc]init];
        
        [loggedInDict setValue:[dictUserData valueForKey:kKeyMobileNumber] forKey:kKeyMobileNumber];
        [loggedInDict setValue:[dictParam valueForKey:kKeyPassword] forKey:kKeyPassword];
        [loggedInDict setValue:[dictUserData valueForKey:kKeyCountryMasterId] forKey:kKeyCountryMasterId];
        [loggedInDict setValue:@"0" forKey:kKeyIsFromFacebook];
        
        currentCountryId = [[dictUserData valueForKey:@"country"] valueForKey:@"numcode"];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setValue:loggedInDict forKey:@"loggedInUser"];
        [userDefaults synchronize];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *loginDataPath = paths[0];
        
        loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if ([fileManager fileExistsAtPath:loginDataPath]) {
            NSError *error;
            [fileManager removeItemAtPath:loginDataPath error:&error];
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateOfBirth = [dateFormatter dateFromString:[dictUserData objectForKey:@"dob"]];
        NSDate *now = [NSDate date];
        NSDateComponents *ageComponents = [[NSCalendar currentCalendar]
                                           components:NSCalendarUnitYear
                                           fromDate:dateOfBirth
                                           toDate:now
                                           options:0];
        currentAge = [ageComponents year];
        
        if (![self isRestrictedAge:currentAge forCountry:currentCountryId]) {
            
            BOOL success= [loggedInDict writeToFile:loginDataPath atomically:YES];
            
            if (success) {
                addTextInSignificantTxt(@"Login Credential written successfully");
            } else {
                addTextInSignificantTxt(@"Login Credential written Unsuccessfully");
            }
            
        }
    }
    
    if (![self isRestrictedAge:currentAge forCountry:currentCountryId]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *loginDataPath = paths[0];
        
        loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if ([fileManager fileExistsAtPath:loginDataPath]) {
            // [self cometChatLogin];
        }
        
        [[SRAPIManager sharedInstance] updateToken:self.server.token];
        
      NSDictionary *dictUserInfo = [inNotify userInfo];
       NSDictionary *dictResponse = [dictUserInfo valueForKey:@"response"];
//       NSDictionary *dictUserData = [dictResponse valueForKey:@"user"];
           
     if ([[dictResponse valueForKey:@"is_device_verified"]  isEqual: @0]){
         if ([[[HSLocationManager sharedManager] topViewController] isKindOfClass:SRLoginViewController.self]){
             [self ValidateChangeUserDevice];
         }else{
             [SRModalClass showAlertWithTitle:@"We have terminated you're session for Account Security" alertTitle:@"Account Verification"];
             [self logoutIfNotVerified];
         }
     } else {
         [self setInitialController];
         //After successfull login and initializing controller's check for push notifications
//         [SRModalClass showAlert:[remoteNotification d]];
         if (remoteNotification != nil) {
            [self handleNotificationBackground:remoteNotification];
         }
     }
        
    } else {
        [SRModalClass showAlert:@"Sorry, this app is unavailable to you based upon your age and country."];
    }
}

- (void)ValidateChangeUserDevice{

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Account Verification"
                                 message:@"We currently have you logged into your account on another device.  To ensure your account security, we have sent an authentication code to the email address you used when you signed up.  Please check your email and enter that code on the next screen."
                                 preferredStyle:UIAlertControllerStyleAlert];

    //Add Buttons

    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self CallAPiForSendOTPOnEmail];
                                }];

    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];

    //Add your buttons to alert controller

    [alert addAction:yesButton];
    [alert addAction:noButton];


    [[[HSLocationManager sharedManager] topViewController] presentViewController:alert animated:YES completion:nil];
}

- (void)CallAPiForSendOTPOnEmail{

   [APP_DELEGATE showActivityIndicator];
    NSString *deviceToken = _server.deviceToken ? _server.deviceToken: @"";
    NSDictionary *params = @{kKeyDeviceToken: deviceToken,
                             kDeviceId : [APP_DELEGATE getDeviceId],
                             kKeyDeviceType: kKeyDeviceName};

    [self.server makeSynchronousRequest:kKeyUserDeviceVerifications inParams:params inMethodType:kPOST isIndicatorRequired:YES];
}


- (void)logoutIfNotVerified{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *folderPath = [self directoryPath];
       
    if ([fileManager fileExistsAtPath:folderPath]) {
        [fileManager removeItemAtPath:folderPath error:&error];
    }
       
       [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedInUser"];
       [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedInUesrInfo"];
       [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
       [[NSUserDefaults standardUserDefaults] synchronize];
       
       [SRModalClass removeImage:kKeyFolderProfileImg inUser:self.server.loggedInUserInfo[kKeyId]];
       
       self.server.token = nil;
       
       [self.server.loggedInUserInfo removeAllObjects];
       [self.server.loggedInFbUserInfo removeAllObjects];
       
       [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
       
       NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
       NSString *loginDataPath = paths[0];
       
       loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
       
       if ([fileManager fileExistsAtPath:loginDataPath]) {
           [fileManager removeItemAtPath:loginDataPath error:&error];
       }
       
       [DBManager clearInstance];
       isAfterLogout = YES;
       [tabsConArr removeAllObjects];
       self.tabBarController = nil;
       [self hideActivityIndicator];

    
       SRPreLoginViewController *preViewcon = [[SRPreLoginViewController alloc] initWithNibName:nil bundle:nil server:_server];
       UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:preViewcon];
       [self.window setRootViewController:navCon];
       [[SRAPIManager sharedInstance] updateToken:self.server.token];
}

- (void)logoutSucceed:(NSNotification *)iNotify {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSString *folderPath = [self directoryPath];
    
    if ([fileManager fileExistsAtPath:folderPath]) {
        [fileManager removeItemAtPath:folderPath error:&error];
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"kTrackingSettings"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedInUser"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loggedInUesrInfo"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [SRModalClass removeImage:kKeyFolderProfileImg inUser:self.server.loggedInUserInfo[kKeyId]];
    
    self.server.token = nil;
    
    [self.server.loggedInUserInfo removeAllObjects];
    [self.server.loggedInFbUserInfo removeAllObjects];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *loginDataPath = paths[0];
    
    loginDataPath = [loginDataPath stringByAppendingPathComponent:@"loginData.plist"];
    
    if ([fileManager fileExistsAtPath:loginDataPath]) {
        [fileManager removeItemAtPath:loginDataPath error:&error];
    }
    
    [DBManager clearInstance];
    
    isAfterLogout = YES;
    
    [tabsConArr removeAllObjects];
    
    self.tabBarController = nil;
    
    [self hideActivityIndicator];
    
    SRLoginViewController *objLogin = [[SRLoginViewController alloc] initWithNibName:nil bundle:nil server:self.server];
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:objLogin];
    
    [self.window setRootViewController:navCon];
    
    [[SRAPIManager sharedInstance] updateToken:self.server.token];
}

- (void)deleteAccountSucceed:(NSNotification *)inNotify {
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"loggedInUser"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"kTrackingSettings"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [SRModalClass removeImage:kKeyFolderProfileImg inUser:self.server.loggedInUserInfo[kKeyId]];
    
    self.server.token = nil;
    self.server.contactList = nil;
    
    [self.server.loggedInUserInfo removeAllObjects];
    [self.server.loggedInFbUserInfo removeAllObjects];
    
    isAfterLogout = YES;
    [tabsConArr removeAllObjects];
    self.tabBarController = nil;
    [self hideActivityIndicator];
    [self setRootView];
}

- (NSInteger)getUnreadTrackingLocationCount {
    NSInteger count = 0;
    NSMutableArray *notificationsList;
    
    if (notificationDict.count > 0) {
        notificationsList = [[NSMutableArray alloc] initWithArray:[notificationDict allKeys]];
        NSInteger unreadCount = [notificationsList count];
        count = count + unreadCount;
    }
    return count;
}

- (void)getTrackingNotificationSucceed:(NSNotification *)inNotify {
    [notificationDict removeAllObjects];
    
    NSDictionary *dict = [inNotify object];
    
    if (dict.count >0) {
        notificationDict = [[NSMutableDictionary alloc]initWithDictionary:[inNotify object]];
    }
}

- (void)getTrackingNotificationFailed:(NSNotification *)inNotify {
    [APP_DELEGATE hideActivityIndicator];
}

- (void)updateActiveStatusSucceed:(NSNotification *)iNotify {
    //
}

- (void)updateActiveStatusFailed:(NSNotification *)iNotify {
    //
}

- (void) reachabilityChanged:(NSNotification *)notice {
    NetworkStatus remoteHostStatus = [reach currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        [self hideActivityIndicator];
        
        dispatch_async(dispatch_get_main_queue(),^{
            objInternetView = [[SRNoInternetView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 20)];
            
            [[[UIApplication sharedApplication] keyWindow] addSubview:objInternetView];
        });
    } else if (remoteHostStatus == ReachableViaWiFi || remoteHostStatus == ReachableViaWWAN) {
        [self.server makeSynchronousRequest:kKeyClassGetCountryList inParams:nil inMethodType:kGET isIndicatorRequired:NO];
        
        [self PathForPlistFile];
        [self pathForGroupChatList];
        
        [[DBManager getSharedInstance] getUnreadMsgCount];
        
        [objInternetView removeFromSuperview];
    }
}

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host {
    return YES;
}

- (void)saveDeviceID:(NSString *)deviceId {
    [KFKeychain saveObject:deviceId forKey:kDeviceId];
}

- (NSString *)getDeviceId {
    return [KFKeychain loadObjectForKey:kDeviceId];
}

- (void)removeAuthToken {
    [KFKeychain deleteObjectForKey:kDeviceId];
}

-(UIColor*)getColorBaseOnBatteryUsage:(NSString*)cuurentBatteryOption{

    NSString *batteryUsage = [cuurentBatteryOption lowercaseString];
    NSString* hexValue = @"";
    //FCE91D
    if ([batteryUsage isEqualToString:@"realtime"] || [batteryUsage isEqualToString:@"recommended"]) {
        hexValue = @"f5a623";
    }else if ([batteryUsage isEqualToString:@"very low"]) {
        hexValue = @"4a90e2";
    }else if ([batteryUsage isEqualToString:@"low"]) {
         hexValue = @"f8e71c";
    }else if ([batteryUsage containsString:@"medium"]) {
        hexValue = @"33a532";
    }else if ([batteryUsage containsString:@"ground travel"] || [batteryUsage containsString:@"ground mode"]) {
         hexValue = @"000000";//black
    }else if ([batteryUsage containsString:@"air travel"] || [batteryUsage containsString:@"air mode"]) {
         hexValue = @"000000";//black
    }else{
       hexValue = @"000000";//black
    }
    
    return [self colorFromHexString:hexValue];
}
- (UIColor *)colorFromHexString:(NSString *)hexString {
        NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
        CGFloat alpha, red, blue, green;
        switch ([colorString length]) {
            case 3: // #RGB
                alpha = 1.0f;
                red   = [self colorComponentFrom: colorString start: 0 length: 1];
                green = [self colorComponentFrom: colorString start: 1 length: 1];
                blue  = [self colorComponentFrom: colorString start: 2 length: 1];
                break;
            case 4: // #ARGB
                alpha = [self colorComponentFrom: colorString start: 0 length: 1];
                red   = [self colorComponentFrom: colorString start: 1 length: 1];
                green = [self colorComponentFrom: colorString start: 2 length: 1];
                blue  = [self colorComponentFrom: colorString start: 3 length: 1];
                break;
            case 6: // #RRGGBB
                alpha = 1.0f;
                red   = [self colorComponentFrom: colorString start: 0 length: 2];
                green = [self colorComponentFrom: colorString start: 2 length: 2];
                blue  = [self colorComponentFrom: colorString start: 4 length: 2];
                break;
            case 8: // #AARRGGBB
                alpha = [self colorComponentFrom: colorString start: 0 length: 2];
                red   = [self colorComponentFrom: colorString start: 2 length: 2];
                green = [self colorComponentFrom: colorString start: 4 length: 2];
                blue  = [self colorComponentFrom: colorString start: 6 length: 2];
                break;
            default:
                [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
                break;
        }
        return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
    }

    -(CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
        NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
        NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
        unsigned hexComponent;
        [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
        return hexComponent / 255.0;
    }


@end


