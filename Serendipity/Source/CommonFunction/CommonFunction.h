//
//  CommonFunction.h
//  Serendipity
//
//  Created by Madhura on 21/04/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonFunction : NSObject
{
    
}
+ (id)shared;
- (void)DownloadMedia:(NSDictionary *)response;
- (UIColor *)colorFromHexString:(NSString *)hexString;
- (NSString *)displayFromDayofline:(int)daysOffline hoursBetweenDates: (int)hoursBetweenDates;
@end
