//
//  DBManager.h
//  Serendipity
//
//  Created by Madhura on 21/04/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject
+ (DBManager *)getSharedInstance;
- (void)insertMessages:(NSDictionary *)messageData;
-(NSMutableArray *)getdata:(NSNumber *)sender receiver:(NSNumber *)receiver;
- (void)insertGroupMessages:(NSDictionary *)messageData ;
-(NSMutableArray *)getGroupChatData:(NSNumber *)chatroomid;
-(NSMutableArray *)getRecentList;
-(void)insertRecentList:(NSDictionary *)userData;
-(NSInteger)getUnreadMsgCount;
+ (void)clearInstance;
@end
