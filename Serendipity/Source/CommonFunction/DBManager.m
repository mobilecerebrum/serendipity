                          //
//  DBManager.m
//  Serendipity
//
//  Created by Madhura on 21/04/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import "DBManager.h"
#import "SRServerConnection.h"

#import <sqlite3.h>
static DBManager *sharedInstance = nil;
static sqlite3 *chatDB = nil;
static sqlite3_stmt *statement = nil;
NSObject <OS_dispatch_queue> *queue_database_operations;
NSObject <OS_dispatch_queue> *queue_callback_operations;

@implementation DBManager {
    NSString *databasePath;
    SRServerConnection *server;

}
+ (DBManager *)getSharedInstance {
    
    if(sharedInstance == nil) {
        
        sharedInstance = [[DBManager allocWithZone:NULL] init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

+ (void)clearInstance
{
    //  [[NSNotificationCenter defaultCenter] removeObserver:self];
    //instance = nil;
    sharedInstance = nil;
}

- (void)createDB {
    
    NSString *directoryPath;
    NSArray *directoryArray;
    directoryArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    directoryPath = directoryArray[0];
    BOOL success = NO;
    databasePath = [[NSString alloc] initWithString:[directoryPath stringByAppendingPathComponent:@"chat_app.db"]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    const char *charPath = [databasePath UTF8String];
    
    if([fileManager fileExistsAtPath:databasePath] == NO) {
        
        if(sqlite3_open(charPath, &chatDB) == SQLITE_OK)
        {
           // NSLog(@"DBMANAGER : CometChat App Database created! %s",charPath);
            success = YES;
        }
        else
        {
            success = NO;
          //  NSLog(@"DBMANAGER : Failed to create database -> %s",sqlite3_errmsg(chatDB));
        }
    }
    else
    {
        success = YES;
        sqlite3_open(charPath, &chatDB);
    }
   // NSLog(@"DBMANAGER : DATABASE PATH -> %@",databasePath);
    if (success)
    {
        char *errorMsg;
        /* Creating buddy_messages table */
        const char *sql_stmt;
        sql_stmt ="create table if not exists chat_messages (id integer primary key autoincrement, message_id integer not null unique, sender integer ,receiver integer , message text, timestamp long, messageTypeFlag integer,downloadStatusFlag integer, messagestatus text , messagetosend text , offlinemessage text, status text, loggedUserId integer )";  //dynasty added one new column => messagetosend
        
        if(sqlite3_exec(chatDB, sql_stmt, NULL, NULL, NULL) != SQLITE_OK){
            
            NSLog(@"DBMANAGER : Failed to create buddy_messages table -> %s",sqlite3_errmsg(chatDB));
            sqlite3_close(chatDB);
            
        } else{
            NSLog(@"DBMANAGER : chat_messages table created");
        }
        
        /* Creating chatroom_messages table */
        sql_stmt ="create table if not exists chatroom_messages (id integer primary key autoincrement, chatroom_id text, sender_name text, sender_id integer, message_id long not null unique, message text, timestamp long, messageTypeFlag integer, downloadStatusFlag integer,messagestatus text , messagetosend text, offlinemessage text,groupname text, grouppic text, status text, loggedUserId integer)";  //dynastyrooms added one new column => messagetosend
        
        if(sqlite3_exec(chatDB, sql_stmt, NULL, NULL, &errorMsg) != SQLITE_OK){
            
            NSLog(@"DBMANAGER : Failed to create chatroom_messages table -> %s",sqlite3_errmsg(chatDB));
            
            sqlite3_close(chatDB);
            
        } else {
            NSLog(@"DBMANAGER : chatroom_messages table created");
        }
        
        /* Creating recent_messages table */
        sql_stmt ="create table if not exists recent_messages (conversation_id integer primary key autoincrement, buddy_id integer not null, name text , last_message text,timestamp long, avtar_url text, isGroup text, loggedUserId integer)";
        
        if(sqlite3_exec(chatDB, sql_stmt, NULL, NULL, NULL) != SQLITE_OK){
            
            NSLog(@"DBMANAGER : Failed to create recent_messages table -> %s",sqlite3_errmsg(chatDB));
            sqlite3_close(chatDB);
            
        } else {
            NSLog(@"DBMANAGER : recent_messages table created");
            
        }
    }
}

- (void)insertMessages:(NSDictionary *)messageData {
   
    NSDictionary *messages = [messageData objectForKey:@"messages"];
    [self processContactMessages:@{@"message":messages}];
}

-(void) processContactMessages:(NSDictionary *)messageData {
   
        NSDictionary *messageDictionary = [messageData objectForKey:@"message"];
        /* Retrieve all fields from message */
        NSString *message = [messageDictionary objectForKey:MESSAGE];
       NSNumber *isSelf = @([[messageDictionary objectForKey:SELF] intValue]);
        NSNumber *message_id = [messageDictionary objectForKey:kKeyId];
        NSString *timestamp,*name,*imgUrl;
        NSNumber *sender;
        NSNumber *receiver;
        NSString *messageStatus;
        NSNumber *downloadStatusFlag = @0;
        NSNumber *messageTypeFlag = @10;
              if([messageDictionary objectForKey:MESSAGE_TYPE]){
            messageTypeFlag = @([[messageDictionary objectForKey:MESSAGE_TYPE] intValue]);
        }
        NSString *isOfflineMessage = @"0";
        if([[messageDictionary objectForKey:IS_OFFLINE_MESSAGE] isEqualToString:@"1"]) {
            isOfflineMessage = @"1";
        } else if([[messageDictionary objectForKey:IS_OFFLINE_MESSAGE] isEqualToString:@"2"]) {
            isOfflineMessage = @"2";
        }
        /* Check for downloadStatus flag */
        if([isSelf isEqual:@1] && [messageTypeFlag isEqual:@14]) {
            downloadStatusFlag = @1;
        }
        /* Set flag for various message type and whether to save message in database or not */
        BOOL success = NO;
        if ([[NSString stringWithFormat:@"%@",[messageDictionary objectForKey:SENT]] length] > 10) {
            timestamp = [NSString stringWithFormat:@"%@",[messageDictionary objectForKey:SENT]];
        } else {
            timestamp = [NSString stringWithFormat:@"%@000",[messageDictionary objectForKey:SENT]];
        }
        timestamp = [NSString stringWithFormat:@"%lld",(long long)[timestamp longLongValue]];
    sender = [messageDictionary objectForKey:@"from"];
    receiver =[messageDictionary objectForKey:@"To"];
    NSString *msgToSend,*buddy_id;
        /* Check for self message */
        if ([isSelf isEqual:@0]) {
            messageStatus = @"1";
            name=@"";
            imgUrl=@"";
           receiver =(APP_DELEGATE).loggedinUserID;
            buddy_id=[messageDictionary objectForKey:@"from"];
           msgToSend =[messageDictionary objectForKey:MESSAGE];
        }
        else if ([isSelf isEqual:@1])
        {
            buddy_id=[messageDictionary objectForKey:@"To"];
            msgToSend =[messageDictionary objectForKey:MESSAGE_TO_SEND];
            name=[messageDictionary objectForKey:@"name"];
            if (name==nil)
            {
                name=@"";
            }
            imgUrl=[messageDictionary objectForKey:@"url"];
            if (imgUrl==nil)
            {
                imgUrl=@"";
            }
            if([[messageDictionary objectForKey:IS_OFFLINE_MESSAGE] isEqualToString:@"1"])
                messageStatus = @"0";
             else
                messageStatus = @"1";
        }
    NSString *status=@"0";//Unread
    if ((APP_DELEGATE).isOnChat  && sender==(APP_DELEGATE).chatUserID)
    {
        status=@"1";
    }
        const char *charPath = [databasePath UTF8String];
        const char *error;
        if(sqlite3_open(charPath, &chatDB) == SQLITE_OK)
        {
            message = [message stringByReplacingOccurrencesOfString:@"\"" withString:@"U+0022"];
            NSString *insertQuery;
            if([isOfflineMessage isEqualToString:@"1"] && [message_id isEqual:@0]) {
                insertQuery = [NSString stringWithFormat:@"insert into chat_messages (message_id ,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messagestatus,messagetosend , offlinemessage,status,loggedUserId )values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",timestamp,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messageStatus, msgToSend , isOfflineMessage,status,(APP_DELEGATE).loggedinUserID];
            }
            else if([isOfflineMessage isEqualToString:@"2"] || ([isSelf isEqual:@1] && [[messageDictionary objectForKey:LOCALMESSAGE_ID] intValue] < 1000000000))
            {
                if([messageDictionary objectForKey:LOCALMESSAGE_ID] && ![messageTypeFlag isEqual:@13])
                {
                    insertQuery = [NSString stringWithFormat:@"UPDATE chat_messages SET message_id=\"%@\" , offlinemessage = 0, messagestatus = 1  WHERE id = %@ and loggedUserId= %@ ",message_id,[messageDictionary objectForKey:LOCALMESSAGE_ID],(APP_DELEGATE).loggedinUserID];
                }
                else
                {
                    insertQuery = [NSString stringWithFormat:@"insert into chat_messages (message_id ,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messagestatus,offlinemessage,status,loggedUserID )values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",message_id,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messageStatus, isOfflineMessage,status,(APP_DELEGATE).loggedinUserID];
                }
            }
            else if([isSelf isEqual:@0] && (![messageDictionary objectForKey:LOCALMESSAGE_ID] || [[messageDictionary objectForKey:LOCALMESSAGE_ID] isEqual:@0] || [[messageDictionary objectForKey:LOCALMESSAGE_ID] isEqualToString:@""]))
            {
                    insertQuery = [NSString stringWithFormat:@"insert into chat_messages (message_id ,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messagestatus,offlinemessage,status,loggedUserID )values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%@\",\"%@\")",message_id,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messageStatus,isOfflineMessage,status,(APP_DELEGATE).loggedinUserID];
            }
            else
            {
                if([isSelf isEqual:@1])
                {
                    NSString *messageExists = [[NSString alloc] init];
                    messageExists = @"0";
                    const char *error;
                    NSString *selectQuery = [NSString stringWithFormat: @"SELECT * FROM chat_messages where message_id  = \"%@\" and loggedUserId=%@",message_id,(APP_DELEGATE).loggedinUserID];
                    const char *query = [selectQuery UTF8String];
                    sqlite3_stmt *compiledStmnt;
                    if(sqlite3_prepare_v2(chatDB, query, -1, &compiledStmnt, &error) == SQLITE_OK)
                    {
                        while(sqlite3_step(compiledStmnt) == SQLITE_ROW)
                        {
                            messageExists = @"1";
                        }
                        sqlite3_reset(compiledStmnt);
                        sqlite3_finalize(compiledStmnt);
                    }
                    if([messageExists isEqualToString:@"1"])
                    {
                        insertQuery = [NSString stringWithFormat:@"UPDATE chat_messages SET message_id=\"%@\" , offlinemessage = 0  WHERE id = %@ and loggedUserId=%@",message_id,[messageDictionary objectForKey:LOCALMESSAGE_ID],(APP_DELEGATE).loggedinUserID];
                    }
                    else
                    {
                        insertQuery = [NSString stringWithFormat:@"insert into chat_messages (message_id ,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messagestatus, offlinemessage,status,loggedUserID )values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%@\", \"%@\")",message_id,sender,receiver,message,timestamp,messageTypeFlag,downloadStatusFlag,messageStatus, isOfflineMessage,status,(APP_DELEGATE).loggedinUserID];
                    }
                }
            }
            const char *query = [insertQuery UTF8String];
             sqlite3_stmt *compiledStmnt1;
            sqlite3_prepare_v2(chatDB, query, -1, &compiledStmnt1,&error);
            NSInteger result;
            if ((APP_DELEGATE).isRetry)
            {
                result =SQLITE_DONE;
                sender = [messageDictionary objectForKey:@"sender"];
                receiver =[messageDictionary objectForKey:@"receiver"];
            }
            else
                 result = sqlite3_step(compiledStmnt1);
            sqlite3_reset(compiledStmnt1);
            sqlite3_finalize(compiledStmnt1);
            if (result == SQLITE_DONE)
            {
                [self getdata:sender receiver:receiver];
                success = YES;
                long long lastRowId = 0;
                if ((APP_DELEGATE).isRetry) {
                    NSString *selectQuery = [NSString stringWithFormat: @"SELECT id FROM chat_messages where message_id = %@ and loggedUserId=%@",[messageDictionary objectForKey:@"message_id"],(APP_DELEGATE).loggedinUserID];
                    const char *sqlStatement = [selectQuery UTF8String] ;
                    sqlite3_stmt *compiledStatement;
                    
                    if(sqlite3_prepare_v2(chatDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                             lastRowId =[[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 0)] longLongValue];
                        }
                    }
                    sqlite3_reset(compiledStatement);
                    sqlite3_finalize(compiledStatement);
                } else {
                     lastRowId = sqlite3_last_insert_rowid(chatDB);
                }
                
                if ([isOfflineMessage isEqualToString:@"1"]) {
                    if([messageTypeFlag isEqual:@10]) {
                    } else if([messageTypeFlag isEqual:@12] ) {
                    } else if([messageTypeFlag isEqual:@14]) {
                    }
                }
                if (buddy_id != nil && ![buddy_id isEqualToString:@""]) {
                    NSDictionary*  dict = [[NSDictionary alloc] initWithObjectsAndKeys:buddy_id, @"buddyID", msgToSend, @"lastMessage", timestamp, @"timestamp", name, @"name", imgUrl, @"imgUrl", @"0", @"isGroup",nil];
                    [[DBManager getSharedInstance] insertRecentList:dict];
                }
            }
            else if(result == SQLITE_BUSY)
            {
                NSLog(@"message not inserted db busy = %s\n", sqlite3_errmsg(chatDB));
                success = NO;
            }
            else
            {
                NSLog(@"message not inserted error 5555 = %s\n", sqlite3_errmsg(chatDB));
                success = NO;
            }
        }
    sqlite3_close(chatDB);
    if (((APP_DELEGATE).isOnChat && [isSelf isEqual:@0] && sender==(APP_DELEGATE).chatUserID) || ((APP_DELEGATE).isOnChat && ([messageTypeFlag isEqual:@12] || [messageTypeFlag isEqual:@14]) ))
    {
        (APP_DELEGATE).msgType=messageTypeFlag;
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshList
                                                            object:nil];
    }
}

- (void)insertGroupMessages:(NSDictionary *)messageData  {
        NSDictionary *messageDictionary = [messageData objectForKey:@"messages"];
        /* Retrieve all fields from message */
        NSString *message = [messageDictionary objectForKey:MESSAGE];
        NSString *messageToSend = [messageDictionary objectForKey:MESSAGE_TO_SEND];
        NSString *message_id = [NSString stringWithFormat:@"%@",[messageDictionary objectForKey:MESSAGE_ID]];
        NSNumber *isSelf = @([[messageDictionary objectForKey:SELF] intValue]);
        NSString *sender_name,*msg,*grouppic=@"",*groupname=@"";
        NSString *sender_id;
        if ([isSelf isEqual:@1]) {
            sender_name = [messageDictionary objectForKey:MSG_FROM];
            sender_id = [messageDictionary objectForKey:FROM_ID];
            msg=messageToSend;
        }
        else
        {
            sender_name=[messageDictionary objectForKey:@"from"];
            sender_id=[messageDictionary objectForKey:@"fromid"];
            msg=message;
        }
        NSString *roomID = [NSString stringWithFormat:@"%@",[messageDictionary objectForKey:ROOM_ID]];
        NSString *timestamp;
    
    if ([messageDictionary objectForKey:@"grouppic"]!=nil) {
        grouppic=[messageDictionary objectForKey:@"grouppic"];
    }
    if ([messageDictionary objectForKey:@"groupname"]!=nil)
    {
        groupname=[messageDictionary objectForKey:@"groupname"];
    }
        NSNumber *messageTypeFlag = @([[messageDictionary objectForKey:MESSAGE_TYPE] intValue]);
        NSNumber *msgreadstatus = @-1;
        NSNumber *downloadStatusFlag = @0; //Getbacktothis
        /* For Message from self = 0 'isOfflineMessage' should always be inserted as '0'
         and for self = 1 'isOfflineMessage' should be inserted as '1' when message is send to server and once server responds then it will be changes to '0' */
        NSString *isOfflineMessage = @"0";
        if([[messageDictionary objectForKey:IS_OFFLINE_MESSAGE] isEqualToString:@"1"]) {
            isOfflineMessage = @"1";
        } else if([[messageDictionary objectForKey:IS_OFFLINE_MESSAGE] isEqualToString:@"2"]) {
            isOfflineMessage = @"2";
        }
    
    if ([isSelf isEqual:@0]) {
               msgreadstatus = @1;
    }
    else if ([isSelf isEqual:@1]){
        if([[messageDictionary objectForKey:IS_OFFLINE_MESSAGE] isEqualToString:@"1"]) {
            msgreadstatus = @0;
        } else {
            msgreadstatus = @1;
        }
    }
    NSString *status=@"0";
    if ((APP_DELEGATE).isOnChat && [messageDictionary objectForKey:ROOM_ID]==(APP_DELEGATE).chatUserID)
    {
        status=@"1";
    }
        /* Check for downloadStatus flag */
         if([isSelf isEqual:@1] && [messageTypeFlag isEqual:@14]) {
            downloadStatusFlag = @1;
        }
        if ([[NSString stringWithFormat:@"%@",[messageDictionary objectForKey:SENT]] length] > 10) {
            timestamp = [NSString stringWithFormat:@"%@",[messageDictionary objectForKey:SENT]];
        } else {
            timestamp = [NSString stringWithFormat:@"%@000",[messageDictionary objectForKey:SENT]];
        }
        timestamp = [NSString stringWithFormat:@"%lld",(long long)[timestamp longLongValue]];
        /* Set flag for various message type and whether to save message in database or not */
        BOOL success = NO;
        const char *charPath = [databasePath UTF8String];
        const char *error;
        if(sqlite3_open(charPath, &chatDB) == SQLITE_OK){
            NSString *insertQuery;
            /* Store message in database */
            message = [message stringByReplacingOccurrencesOfString:@"\"" withString:@"U+0022"];
            
            if(![isSelf isEqual:@1] || ([isSelf isEqual:@1] && ![messageDictionary objectForKey:LOCALMESSAGE_ID] )) {
                if([message_id isEqualToString:@"0"]){
                    insertQuery = [NSString stringWithFormat:@"insert into chatroom_messages (chatroom_id,sender_name ,sender_id, message_id, message,timestamp, messageTypeFlag,downloadStatusFlag,messagestatus,messagetosend, offlinemessage,grouppic,groupname,status,loggedUserId) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",roomID,sender_name,sender_id,timestamp,message,timestamp,messageTypeFlag,downloadStatusFlag,msgreadstatus,messageToSend,isOfflineMessage,grouppic,groupname,status,(APP_DELEGATE).loggedinUserID];
                }else {
                    insertQuery = [NSString stringWithFormat:@"insert into chatroom_messages (chatroom_id,sender_name ,sender_id, message_id, message,timestamp, messageTypeFlag,downloadStatusFlag,messagestatus,messagetosend, offlinemessage,grouppic,groupname,status,loggedUserId) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",roomID,sender_name,sender_id,timestamp,message,timestamp,messageTypeFlag,downloadStatusFlag,msgreadstatus,messageToSend,isOfflineMessage,grouppic,groupname,status,(APP_DELEGATE).loggedinUserID];
                }
            }
            else if([isOfflineMessage isEqualToString:@"1"] && [message_id isEqual:@0]) {
                insertQuery = [NSString stringWithFormat:@"insert into chatroom_messages (chatroom_id,sender_name ,sender_id, message_id, message,timestamp, messageTypeFlag,downloadStatusFlag,messagestatus,messagetosend, offlinemessage,grouppic,groupname,status,loggedUserId) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",roomID,sender_name,sender_id,timestamp,message,timestamp,messageTypeFlag,downloadStatusFlag,msgreadstatus,messageToSend,isOfflineMessage,grouppic,groupname,status,(APP_DELEGATE).loggedinUserID];
                
            } else if([isOfflineMessage isEqualToString:@"2"]){                
                insertQuery = [NSString stringWithFormat:@"UPDATE chatroom_messages SET message_id=\"%@\" , offlinemessage = 0, messagestatus = 1  WHERE id = %@ and loggedUserId=%@ ",message_id,[messageDictionary objectForKey:LOCALMESSAGE_ID],(APP_DELEGATE).loggedinUserID];
            } else if ([messageTypeFlag isEqual:@13] && [isSelf isEqual:@1]){
                isOfflineMessage = @"2";
                insertQuery = [NSString stringWithFormat:@"insert into chatroom_messages (chatroom_id,sender_name ,sender_id, message_id, message,timestamp, messageTypeFlag,downloadStatusFlag,messagestatus,messagetosend, offlinemessage,grouppic,groupname,status,loggedUserId) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",roomID,sender_name,sender_id,timestamp,message,timestamp,messageTypeFlag,downloadStatusFlag,msgreadstatus,messageToSend,isOfflineMessage,grouppic,groupname,status,(APP_DELEGATE).loggedinUserID];
            }
            else {
                if([isSelf isEqual:@1]) {
                    NSString *messageExists = [[NSString alloc] init];
                    messageExists = @"0";
                    const char *charPath = [databasePath UTF8String];
                    const char *error;
                    if(sqlite3_open(charPath, &chatDB) == SQLITE_OK)
                    {
                        NSString *selectQuery = [NSString stringWithFormat: @"SELECT * FROM chatroom_messages where message_id  = \"%@\" and loggedUserId=%@ ",message_id,(APP_DELEGATE).loggedinUserID];
                        const char *query = [selectQuery UTF8String];
                        if(sqlite3_prepare_v2(chatDB, query, -1, &statement, &error) == SQLITE_OK)
                        {
                            while(sqlite3_step(statement) == SQLITE_ROW)
                            {
                                messageExists = @"1";
                            }
                            sqlite3_reset(statement);
                        }
                        if([messageExists isEqualToString:@"0"]) {
                            insertQuery = [NSString stringWithFormat:@"UPDATE chatroom_messages SET message_id=\"%@\" , offlinemessage = 0  WHERE id = %@ and loggedUserId=%@ ",message_id,[messageDictionary objectForKey:LOCALMESSAGE_ID],(APP_DELEGATE).loggedinUserID];
                        } else {
                            insertQuery = [NSString stringWithFormat:@"insert into chatroom_messages (chatroom_id,sender_name ,sender_id, message_id, message,timestamp, messageTypeFlag,downloadStatusFlag,messagestatus,messagetosend, offlinemessage,grouppic,groupname,status,loggedUserId) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",roomID,sender_name,sender_id,timestamp,message,timestamp,messageTypeFlag,downloadStatusFlag,msgreadstatus,messageToSend,isOfflineMessage,grouppic,groupname,status,(APP_DELEGATE).loggedinUserID];
                        }
                    }
                    messageExists = nil;
                }
            }
            NSLog(@"Sql Query : insertQuery %@", insertQuery);
            const char *query = [insertQuery UTF8String];
            sqlite3_prepare_v2(chatDB, query, -1, &statement,&error);
            NSInteger result;
            if ((APP_DELEGATE).isRetry)
            {
                result =SQLITE_DONE;
            }
            else
                result = sqlite3_step(statement);
            if (result == SQLITE_DONE) {
                [self getGroupChatData:roomID];
                success = YES;
                NSLog(@"Sql Query : insert Success ");
                long long lastRowId = 0;
                if ((APP_DELEGATE).isRetry)
                {
                    NSString *selectQuery = [NSString stringWithFormat: @"SELECT id FROM chatroom_messages where message_id = %@ and loggedUserId=%@",[messageDictionary objectForKey:@"message_id"],(APP_DELEGATE).loggedinUserID];
                    const char *sqlStatement = [selectQuery UTF8String] ;
                    sqlite3_stmt *compiledStatement;
                    if(sqlite3_prepare_v2(chatDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
                    {
                        while(sqlite3_step(compiledStatement) == SQLITE_ROW)
                        {
                            NSLog(@"%@",[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 0)]);
                            lastRowId =[[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 0)] longLongValue];
                        }
                    }
                }
                else
                 lastRowId = sqlite3_last_insert_rowid(chatDB);
                if([isOfflineMessage isEqualToString:@"1"]) {
                    if([messageTypeFlag isEqual:@10]) {
                    }
                    if([messageTypeFlag isEqual:@12]) {
                    }
                    if([messageTypeFlag isEqual:@14]) {
                    }
                }
                if (msg!=nil && ![msg isEqualToString:@""] && ![roomID isEqualToString:@""]) {
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    [dict setObject:roomID forKey:@"buddyID"];
                    [dict setObject:msg forKey:@"lastMessage"];
                    [dict setObject:timestamp forKey:@"timestamp"];
                    [dict setObject:groupname forKey:@"name"];
                    [dict setObject:grouppic forKey:@"imgUrl"];
                    [dict setObject:@"1" forKey:@"isGroup"];
                    [[DBManager getSharedInstance] insertRecentList:dict];
                }
            }
            else if(result == SQLITE_BUSY)
            {
                NSLog(@"message not inserted in chatroom db = %s\n", sqlite3_errmsg(chatDB));
                success = NO;
            }
            else
            {
                NSLog(@"message not inserted in chatroom db error = %s\n", sqlite3_errmsg(chatDB));
                success = NO;
            }
            sqlite3_reset(statement);
        }
    NSLog(@"%@",(APP_DELEGATE).chatUserID);

    if (((APP_DELEGATE).isOnChat && [isSelf isEqual:@0] && [messageDictionary objectForKey:ROOM_ID]==(APP_DELEGATE).chatUserID) || ((APP_DELEGATE).isOnChat && ([messageTypeFlag isEqual:@12] || [messageTypeFlag isEqual:@14])))
    {
        (APP_DELEGATE).msgType=messageTypeFlag;
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshList
                                                            object:nil];
    }
}


-(void)insertRecentList:(NSDictionary *)userData
{
    NSLog(@"Insert Message : %@",userData);
    NSString *buddyName=[NSString stringWithFormat:@"%@",[userData objectForKey:@"name"]];
    NSString *avatarURL=[NSString stringWithFormat:@"%@",[userData objectForKey:@"imgUrl"]];;
    NSString *selectQuery;
    NSString *buddyID = [NSString stringWithFormat:@"%@",[userData objectForKey:@"buddyID"]];
    NSString *lastMessage = [userData objectForKey:@"lastMessage"];
    NSString *timestamp = [NSString stringWithFormat:@"%@",[userData objectForKey:@"timestamp"]];
    NSString *isGroup=[NSString stringWithFormat:@"%@",[userData objectForKey:@"isGroup"]];
    selectQuery  = [NSString stringWithFormat:@"select * from recent_messages where buddy_id = '%@'and loggedUserId=%@",buddyID,(APP_DELEGATE).loggedinUserID];
    NSLog(@"RECENT SELECT QUERY : %@",selectQuery);
    const char *charPath = [databasePath UTF8String];
    BOOL recordExists = NO;
    const char *error;
    sqlite3_stmt *compiledStmnt1;
    if(sqlite3_open(charPath, &chatDB) == SQLITE_OK){
        const char *query = [selectQuery UTF8String];
        if(sqlite3_prepare_v2(chatDB, query, -1, &compiledStmnt1, NULL) == SQLITE_OK){
            while (sqlite3_step(compiledStmnt1) == SQLITE_ROW) {
                NSLog(@"Record exists");
                recordExists = YES;
            }
        }
        selectQuery = nil;
        query = nil;
        sqlite3_reset(compiledStmnt1);
        sqlite3_finalize(compiledStmnt1);
        if(recordExists){
            NSString *updateQuery = [NSString stringWithFormat:@"UPDATE recent_messages SET timestamp = %@, last_message = '%@'",timestamp,lastMessage];
            if (![buddyName isEqualToString:@""] && buddyName!=nil && ![buddyName isEqualToString:@"(null)"])
            {
                updateQuery=[updateQuery stringByAppendingString:[NSString stringWithFormat:@",name ='%@'",buddyName]];
            }
            if (![avatarURL isEqualToString:@""]&& avatarURL!=nil && ![avatarURL isEqualToString:@"(null)"])
            {
                updateQuery=[updateQuery stringByAppendingString:[NSString stringWithFormat:@",avtar_url ='%@'",avatarURL]];
            }
            updateQuery=[updateQuery stringByAppendingString:[NSString stringWithFormat:@" WHERE buddy_id = '%@' and loggedUserId=%@",buddyID,(APP_DELEGATE).loggedinUserID]];
            NSLog(@"UPDATE query recent_messages= %@",updateQuery);
            const char *query = [updateQuery UTF8String];
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(chatDB, query, -1, &compiledStatement, &error) == SQLITE_OK)
            {
                while(sqlite3_step(compiledStatement) == SQLITE_DONE)
                {
                    NSLog(@"UPDATED for buddy  %@",buddyName);
//                    if ((APP_DELEGATE).isOnChatList)
//                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshList
                                                                            object:nil];
                  //  }
                }
            }
            else
            {
                NSLog(@"Update not executed db busy = %s\n", sqlite3_errmsg(chatDB));
            }
            query = nil;
            updateQuery = nil;
            sqlite3_reset(compiledStatement);
            sqlite3_finalize(compiledStatement);
        }
        else
        {
            if (([buddyName isEqualToString:@""] || buddyName== (id)[NSNull null] || [buddyName length]==0) || ([avatarURL isEqualToString:@""] || avatarURL== (id)[NSNull null] || [avatarURL length]==0))
            {
                if ([[userData objectForKey:@"isGroup"]isEqualToString:@"0"])
                {
                    if ([(APP_DELEGATE).oldSourceArray count]>0)
                    {
                        for (int i=0;i<[(APP_DELEGATE).oldSourceArray count];i++)
                        {
                            NSDictionary *dict=[(APP_DELEGATE).oldSourceArray objectAtIndex:i];
                            if ([[dict objectForKey:kKeyId] isEqualToString:buddyID])
                            {
                                buddyName=[dict objectForKey:kKeyFirstName];
                                NSDictionary*dictImage=[dict objectForKey:kKeyProfileImage];
                                avatarURL=[dictImage objectForKey:@"file"];
                                break;
                            }
                        }
                    }
                    else
                    {
                        if ((APP_DELEGATE).onlineUserDict!=nil)
                        {
                            NSDictionary *dictUser=[(APP_DELEGATE).onlineUserDict valueForKey:[NSString stringWithFormat:@"_%@",buddyID]];
                            if (dictUser!=nil)
                            {
                                buddyName=[dictUser valueForKey:@"n"];
                                NSString *imgPath=[dictUser valueForKey:@"a"];
                                avatarURL= [imgPath lastPathComponent];
                            }
                        }
                    }
                }
                else
                {
                    if ((APP_DELEGATE).groupDict!=nil)
                    {
                        NSDictionary *dictGroup=[(APP_DELEGATE).groupDict valueForKey:[NSString stringWithFormat:@"_%@",buddyID]];
                        if (dictGroup!=nil)
                            buddyName=[dictGroup valueForKey:@"name"];
                    }
                }
            }
            NSString *insertSQL = [NSString stringWithFormat:@"insert into recent_messages (buddy_id,name,last_message,timestamp,avtar_url,isGroup,loggedUserId) values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",buddyID,buddyName,lastMessage,timestamp,avatarURL,isGroup,(APP_DELEGATE).loggedinUserID];
            const char *query = [insertSQL UTF8String];
            NSLog(@"insert Recent chat query = %@",insertSQL);
            sqlite3_stmt *compiledStmnt;
            sqlite3_prepare_v2(chatDB, query,-1, &compiledStmnt, NULL);
            if (sqlite3_step(compiledStmnt) == SQLITE_DONE)
            {
                NSLog(@"Recent chat inserted");
                if ((APP_DELEGATE).isOnChatList)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshList
                                                                        object:nil];
                }
            }
            else if(sqlite3_step(compiledStmnt) == SQLITE_BUSY)
            {
                NSLog(@"Recent chat not inserted db busy = %s\n", sqlite3_errmsg(chatDB));
            }
            else
            {
                NSLog(@"Recent chat not inserted error = %s\n", sqlite3_errmsg(chatDB));
            }
            insertSQL = nil;
            query = nil;
            sqlite3_reset(compiledStmnt);
            sqlite3_finalize(compiledStmnt);
        }
    }
    sqlite3_close(chatDB);
}

-(NSMutableArray *)getdata:(NSNumber *)sender receiver:(NSNumber *)receiver
{
    sqlite3 * database;
    NSString *databasename=@"chat_app.db";
    NSArray * documentpath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSAllDomainsMask, YES);
    NSString * DocDir=[documentpath objectAtIndex:0];
    NSString * databasepath=[DocDir stringByAppendingPathComponent:databasename];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];;
    NSMutableDictionary *dictData;
    if(sqlite3_open([databasepath UTF8String], &database) == SQLITE_OK)
    {
         NSString *selectQuery = [NSString stringWithFormat: @"SELECT * FROM chat_messages where (sender  = \"%@\" and receiver = \"%@\") OR (sender = \"%@\" and receiver = \"%@\") and loggedUserId=%@",sender,receiver,receiver,sender,(APP_DELEGATE).loggedinUserID];
        const char *sqlStatement = [selectQuery UTF8String] ;
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            [arrData removeAllObjects];
          
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
               dictData=[[NSMutableDictionary alloc]init];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)] forKey:@"message_id"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] forKey:@"sender"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 3)] forKey:@"receiver"];
                if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 4)] isEqualToString:@"(null)"])
                {
                    NSString *message = [[NSString alloc]initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledStatement, 4)];
                    message = [message stringByReplacingOccurrencesOfString:@"U+0022" withString:@"\""];
                    [dictData setObject:message forKey:@"message"];
                }
                else
                    [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 4)] forKey:@"message"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] forKey:@"timestamp"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 6)] forKey:@"message_type"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 7)] forKey:@"downloadStatus"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 8)] forKey:@"messageStatus"];
                NSLog(@"%@",[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 9)]);
                if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 9)] isEqualToString:@"(null)"])
                {
                    NSString *message = [[NSString alloc]initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledStatement, 9)];
                    message = [message stringByReplacingOccurrencesOfString:@"U+0022" withString:@"\""];
                    [dictData setObject:message forKey:@"messagetosend"];
                }
               else
                    [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 9)] forKey:@"messagetosend"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 10)] forKey:IS_OFFLINE_MESSAGE];
                [arrData addObject:dictData];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    if ((APP_DELEGATE).isOnChat  && (sender==(APP_DELEGATE).chatUserID || receiver==(APP_DELEGATE).chatUserID))
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success=[fileManager fileExistsAtPath:databasePath];
        if (!success) {
            NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databasename];
            // Copy the database from the package to the users filesystem
            [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
        }
        const char *dbPath=[databasePath UTF8String];
        if (sqlite3_open(dbPath, &database)==SQLITE_OK) {
            NSLog(@"database Opened");
            NSString *  selectQuery = [NSString stringWithFormat: @"Update chat_messages set status = 1 where (sender  = \"%@\" and receiver = \"%@\") OR (sender = \"%@\" and receiver = \"%@\") and loggedUserId=%@",sender,receiver,receiver,sender,(APP_DELEGATE).loggedinUserID];
            const char * sqlStatement = [selectQuery UTF8String] ;
            sqlite3_stmt *stmt;
            if (sqlite3_prepare_v2(database, sqlStatement, -1, &stmt, NULL)==SQLITE_OK)
            {
                NSInteger result;
                result = sqlite3_step(stmt);
                 sqlite3_finalize(stmt);
            }
        }
        sqlite3_close(database);
    }
    return arrData;
}

-(NSMutableArray *)getGroupChatData:(NSNumber *)chatroomid
{
    sqlite3 * database;
    NSString *databasename=@"chat_app.db";
    NSArray * documentpath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSAllDomainsMask, YES);
    NSString * DocDir=[documentpath objectAtIndex:0];
    NSString * databasepath=[DocDir stringByAppendingPathComponent:databasename];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];;
    NSMutableDictionary *dictData;
    if(sqlite3_open([databasepath UTF8String], &database) == SQLITE_OK)
    {
        NSString *selectQuery = [NSString stringWithFormat: @"SELECT * FROM chatroom_messages where chatroom_id  = \"%@\" and loggedUserId=%@ ",chatroomid, (APP_DELEGATE).loggedinUserID];
        const char *sqlStatement = [selectQuery UTF8String] ;
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            [arrData removeAllObjects];
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                dictData=[[NSMutableDictionary alloc]init];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)] forKey:ROOM_ID];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] forKey:@"senderName"];
                 [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 3)] forKey:@"sender"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 4)] forKey:@"message_id"];
                if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] isEqualToString:@"(null)"])
                {
                    NSString *message = [[NSString alloc]initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledStatement, 5)];
                    message = [message stringByReplacingOccurrencesOfString:@"U+0022" withString:@"\""];
                    [dictData setObject:message forKey:@"message"];
                }
                else
                    [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] forKey:@"message"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 6)] forKey:@"timestamp"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 7)] forKey:@"message_type"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 8)] forKey:@"downloadStatus"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 9)] forKey:@"messageStatus"];
                if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 10)] isEqualToString:@"(null)"])
                {
                    NSString *message = [[NSString alloc]initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledStatement, 10)];
                    message = [message stringByReplacingOccurrencesOfString:@"U+0022" withString:@"\""];
                    [dictData setObject:message forKey:@"messagetosend"];
                }
                else
                   [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 10)] forKey:@"messagetosend"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 11)] forKey:IS_OFFLINE_MESSAGE];
                [arrData addObject:dictData];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    if ((APP_DELEGATE).isOnChat  &&  chatroomid==((APP_DELEGATE).chatUserID))
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success=[fileManager fileExistsAtPath:databasePath];
        if (!success) {
            NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databasename];
            // Copy the database from the package to the users filesystem
            [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
        }
        const char *dbPath=[databasePath UTF8String];
        if (sqlite3_open(dbPath, &database)==SQLITE_OK) {
            NSLog(@"database Opened");
           NSString * selectQuery = [NSString stringWithFormat: @"Update chatroom_messages set status = 1 where chatroom_id  = \"%@\" and loggedUserId=%@",chatroomid,(APP_DELEGATE).loggedinUserID];
           const char * sqlStatement = [selectQuery UTF8String] ;
            sqlite3_stmt *compiledStmnt1;
            if (sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStmnt1, NULL)==SQLITE_OK)
            {
                NSInteger result;
                result = sqlite3_step(compiledStmnt1);
                sqlite3_finalize(compiledStmnt1);
            }
        }
        sqlite3_close(database);
    }
    return arrData;
}

-(NSMutableArray *)getRecentList
{
    sqlite3 * database;
    NSString *databasename=@"chat_app.db";
    NSArray * documentpath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSAllDomainsMask, YES);
    NSString * DocDir=[documentpath objectAtIndex:0];
    NSString * databasepath=[DocDir stringByAppendingPathComponent:databasename];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];;
    NSMutableDictionary *dictData;
    if(sqlite3_open([databasepath UTF8String], &database) == SQLITE_OK)
    {
        NSString *selectQuery = [NSString stringWithFormat: @"SELECT * from recent_messages where last_message != '' and loggedUserID=%@",(APP_DELEGATE).loggedinUserID];
        const char *sqlStatement = [selectQuery UTF8String] ;
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            [arrData removeAllObjects];
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                dictData=[[NSMutableDictionary alloc]init];
                NSString * avatarURL=@"", *name=@"";
                if ([[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 6)]isEqualToString:@"0"] && (([[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] isEqualToString:@"(null)"] ||[[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] isEqualToString:@"(null)"]) || ([[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] isEqualToString:@""] ||[[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] isEqualToString:@""])))
                {
                    NSString *buddyID=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)];
                    if ([(APP_DELEGATE).oldSourceArray count]>0)
                    {
                        for (int i=0;i<[(APP_DELEGATE).oldSourceArray count];i++)
                        {
                            NSDictionary *dict=[(APP_DELEGATE).oldSourceArray objectAtIndex:i];
                            if ([[dict objectForKey:kKeyId] isEqualToString:buddyID])
                            {
                                NSDictionary*dictImage=[dict objectForKey:kKeyProfileImage];
                                avatarURL=[dictImage objectForKey:@"file"];
                                [dictData setObject:avatarURL forKey:@"imageName"];
                                name=[dict objectForKey:kKeyFirstName];
                                [dictData setObject:name forKey:kKeyFirstName];
                                break;
                            }
                        }
                    }
                   else
                   {
                       if ((APP_DELEGATE).onlineUserDict!=nil)
                       {
                           NSDictionary *dictUser=[(APP_DELEGATE).onlineUserDict valueForKey:[NSString stringWithFormat:@"_%@",buddyID]];
                           if (dictUser!=nil)
                           {
                               name=[dictUser valueForKey:@"n"];
                               NSString *imgPath=[dictUser valueForKey:@"a"];
                               avatarURL= [imgPath lastPathComponent];
                               [dictData setObject:name forKey:kKeyFirstName];
                               [dictData setObject:avatarURL forKey:@"imageName"];
                           }
                       }
                   }
                    NSString *updateQuery = @"UPDATE recent_messages SET ";
                    if (![avatarURL isEqualToString:@""] && [[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] isEqualToString:@""])
                    {
                        updateQuery=[updateQuery stringByAppendingString:[NSString stringWithFormat:@"avtar_url = '%@'",avatarURL]];
                    }
                    if (![name isEqualToString:@""] && [[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] isEqualToString:@""])
                    {
                        updateQuery=[updateQuery stringByAppendingString:[NSString stringWithFormat:@",name = '%@'",name]];
                    }
                    updateQuery=[updateQuery stringByAppendingString:[NSString stringWithFormat:@" WHERE buddy_id = '%@'and loggedUserId=%@",buddyID,(APP_DELEGATE).loggedinUserID]];
                    const char *error;
                    const char *query = [updateQuery UTF8String];
                    if(sqlite3_prepare_v2(chatDB, query, -1, &statement, &error) == SQLITE_OK)
                        NSLog(@"Success");
                    else
                        NSLog(@"Update not executed db busy = %s\n", sqlite3_errmsg(chatDB));
                    query = nil;
                    updateQuery = nil;
                    sqlite3_finalize(statement);
                    sqlite3_close(database);
                }
                else
                {
                    if ([[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 6)]isEqualToString:@"1"])
                    {
                        NSString *buddyID=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)];
                        if([[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] isEqualToString:@"(null)"]||[[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] isEqualToString:@""])
                        {
                            if ((APP_DELEGATE).groupDict!=nil)
                            {
                                NSDictionary *dictGroup=[(APP_DELEGATE).groupDict valueForKey:[NSString stringWithFormat:@"_%@",buddyID]];
                                if (dictGroup!=nil)
                                    [dictData setObject:[dictGroup valueForKey:@"name"]forKey:kKeyFirstName];
                                else
                                    [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] forKey:kKeyFirstName];
                            }
                            else
                                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] forKey:kKeyFirstName];
                        }
                        else
                            [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] forKey:kKeyFirstName];
                        [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] forKey:@"imageName"];
                    }
                    else
                    {
                        if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] isEqualToString:@""])
                        {
                            [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 5)] forKey:@"imageName"];
                        }
                        if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] isEqualToString:@""])
                        {
                            [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 2)] forKey:kKeyFirstName];
                        }
                    }
                }
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)] forKey:kKeyId];
                
                if (![[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 3)] isEqualToString:@"(null)"])
                {
                    NSString *message = [[NSString alloc]initWithUTF8String:
                                         (const char *) sqlite3_column_text(compiledStatement, 3)];
                    message = [message stringByReplacingOccurrencesOfString:@"U+0022" withString:@"\""];
                    [dictData setObject:message forKey:@"message"];
                }
                else
                    [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 3)] forKey:@"message"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 4)] forKey:@"timestamp"];
                [dictData setObject:[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 6)] forKey:@"isGroup"];
                
                if ([[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 6)]isEqualToString:@"0"])
                {
                    NSString *buddyID=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)];
                    NSString *selectQuery = [NSString stringWithFormat: @"SELECT count(*) from chat_messages where status = 0 and (sender  = \"%@\" OR receiver = \"%@\") and loggedUserId=%@",buddyID,buddyID,(APP_DELEGATE).loggedinUserID];
                    const char *sqlStatement = [selectQuery UTF8String] ;
                    sqlite3_stmt *compiledStment;
                    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStment, NULL) == SQLITE_OK)
                    {
                        while(sqlite3_step(compiledStment) == SQLITE_ROW)
                        {
                            NSInteger count = sqlite3_column_int(compiledStment, 0);
                            [dictData setObject:[NSString stringWithFormat:@"%ld",count] forKey:@"notificationCount"];
                        }
                    }
                     sqlite3_finalize(compiledStment);
                }
                else
                {
                    NSString *buddyID=[NSString stringWithFormat:@"%s",(char *) sqlite3_column_text(compiledStatement, 1)];
                    NSString *selectQuery = [NSString stringWithFormat: @"SELECT count(*) from chatroom_messages where status = 0 and chatroom_id = \"%@\" and loggedUserID=%@",buddyID,(APP_DELEGATE).loggedinUserID];
                    const char *sqlStatement = [selectQuery UTF8String] ;
                    sqlite3_stmt *compiledStment;
                    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStment, NULL) == SQLITE_OK)
                    {
                        while(sqlite3_step(compiledStment) == SQLITE_ROW)
                        {
                            NSInteger count = sqlite3_column_int(compiledStment, 0);
                            [dictData setObject:[NSString stringWithFormat:@"%ld",count] forKey:@"notificationCount"];
                        }
                    }
                    sqlite3_finalize(compiledStment);
                }
                [arrData addObject:dictData];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    return arrData;
}

-(NSInteger)getUnreadMsgCount;
{
    sqlite3 * database;
    NSString *databasename=@"chat_app.db";
    NSArray * documentpath=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSAllDomainsMask, YES);
    NSString * DocDir=[documentpath objectAtIndex:0];
    NSString * databasepath=[DocDir stringByAppendingPathComponent:databasename];
    NSInteger count =0,countgrpmsg=0;
    if(sqlite3_open([databasepath UTF8String], &database) == SQLITE_OK)
    {
        NSString *selectQuery =[NSString stringWithFormat:@"SELECT count(*) FROM chat_messages where status = 0 and loggedUserId=%@",(APP_DELEGATE).loggedinUserID] ;
        const char *sqlStatement = [selectQuery UTF8String] ;
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                count = sqlite3_column_int(compiledStatement, 0);
            }
        }
        sqlite3_finalize(compiledStatement);
        
        selectQuery =[NSString stringWithFormat:@"SELECT count(*) FROM chatroom_messages where status = 0 and loggedUserId=%@",(APP_DELEGATE).loggedinUserID] ;
        sqlStatement = [selectQuery UTF8String] ;
        sqlite3_stmt *compiledStment;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStment, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStment) == SQLITE_ROW)
            {
                countgrpmsg = sqlite3_column_int(compiledStment, 0);
            }
        }
        sqlite3_finalize(compiledStment);
        if (countgrpmsg>0)
            count=count+countgrpmsg;
    }
    sqlite3_close(database);
    return  count;
}
@end
