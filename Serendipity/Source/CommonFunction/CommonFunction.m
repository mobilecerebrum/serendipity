//
//  CommonFunction.m
//  Serendipity
//
//  Created by Madhura on 21/04/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import "CommonFunction.h"
#import "DBManager.h"

@import FirebaseMessaging;
@implementation CommonFunction
{
    NSString *userID;

}
static CommonFunction *shared = nil;

-(id)init
{
    shared = [super init];
    if (shared)
    {
        return shared;
    }
    return shared;
}
+(id)shared
{
    if (!shared)
    {
        shared = [[self alloc] init];
    }
    return shared;
}


- (void)DownloadMedia:(NSDictionary *)response
{
    if ((APP_DELEGATE).isOnChat && ([response valueForKey:@"from"] == (APP_DELEGATE).chatUserID ||[response objectForKey:ROOM_ID] == (APP_DELEGATE).chatUserID))
      {
          dispatch_async(dispatch_get_main_queue(), ^{
              (APP_DELEGATE).IsDownloadStarted=TRUE;
              [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshMedia
                                                                  object:nil];
          });
      }
    NSError *error;
    NSString *strWhole=[response valueForKey:kKeyMessage];
    NSString *strSearch=@"unencryptedfilename=";
    NSArray *arrFileData = [strWhole componentsSeparatedByString:strSearch];
    NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *documentTXTPath = [documentsDirectoryPath stringByAppendingPathComponent:@"SDKCometChatMediaFiles"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:documentTXTPath withIntermediateDirectories:NO attributes:nil error:&error];
    if ([[response valueForKey:kKeyMessageType] isEqualToString:@"14"]) {
        NSError *error;
        documentTXTPath=[documentTXTPath stringByAppendingPathComponent:@"Videos"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:documentTXTPath withIntermediateDirectories:NO attributes:nil error:&error];
        }
        
        documentTXTPath = [documentTXTPath stringByAppendingPathComponent:@"Recieve"];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:documentTXTPath withIntermediateDirectories:NO attributes:nil error:&error];
        }
        
        NSURL *outputURL = [NSURL fileURLWithPath:[documentTXTPath stringByAppendingPathComponent:[arrFileData objectAtIndex:1]]];
        [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
        NSURL *actualUrl = [NSURL URLWithString:strWhole];
        NSData  *data1 = [NSData dataWithContentsOfURL:actualUrl];
        NSString *vidoeUrl = [outputURL path];
        if (data1) {
            BOOL success= [data1 writeToFile:vidoeUrl options:NSAtomicWrite error:&error];
           // NSLog(success ? @"Yes" : @"No");
        }
        documentTXTPath =@"";
        documentTXTPath = [documentsDirectoryPath stringByAppendingPathComponent:@"SDKCometChatMediaFiles"];
        NSString *dirPath = [documentTXTPath stringByAppendingPathComponent:@"Thumbnail"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];
        dirPath =[dirPath stringByAppendingPathComponent:@"Recieve"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:&error];
        NSURL *url=[NSURL fileURLWithPath:vidoeUrl];
        MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:url];
        UIImage  *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
        [player stop];
        player = nil;
        NSData * binaryImageData = UIImagePNGRepresentation(thumbnail);
        NSString * imgName = [[arrFileData objectAtIndex:1] stringByReplacingOccurrencesOfString:@".mp4" withString:@".png"];
        [binaryImageData writeToFile:[dirPath stringByAppendingPathComponent:imgName] options:NSAtomicWrite error:&error];
          if ((APP_DELEGATE).isOnChat && ([response valueForKey:@"from"]==(APP_DELEGATE).chatUserID ||[response objectForKey:ROOM_ID]==(APP_DELEGATE).chatUserID))
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                (APP_DELEGATE).IsDownloadStarted=FALSE;
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshMedia
                                                                    object:nil];
            });
        }
    }
    else
    {
        documentTXTPath=[documentTXTPath stringByAppendingPathComponent:@"Images"];
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:documentTXTPath withIntermediateDirectories:NO attributes:nil error:&error];
        documentTXTPath=[documentTXTPath stringByAppendingPathComponent:@"Recieve"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:documentTXTPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:documentTXTPath withIntermediateDirectories:NO attributes:nil error:&error];
        NSString *imgName =[arrFileData objectAtIndex:1];
        NSString *imgURL = [response valueForKey:kKeyMessage];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *writablePath = [documentTXTPath stringByAppendingPathComponent:imgName];

        if(![fileManager fileExistsAtPath:writablePath]){
           // NSLog(@"file doesn't exist");
            NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgURL]];
            NSError *error = nil;
            [data writeToFile:writablePath options:NSAtomicWrite error:&error];
            if (error)
            {
               // NSLog(@"Error Writing File : %@",error);
            }
            else
            {
               // NSLog(@"Image %@ Saved SuccessFully",imgName);
                  if ((APP_DELEGATE).isOnChat && ([response valueForKey:@"from"]==(APP_DELEGATE).chatUserID ||[response objectForKey:ROOM_ID]==(APP_DELEGATE).chatUserID))
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        (APP_DELEGATE).IsDownloadStarted=FALSE;
                        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRefreshMedia
                                                                            object:nil];
                    });
                }
            }
        }
        else
        {
           // NSLog(@"file exist");
        }
    }
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

-(CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (NSString *)displayFromDayofline:(int)daysOffline hoursBetweenDates:(int)hoursBetweenDates {
    int years = daysOffline / 365;
    int months = daysOffline / 31;
    
    if (years > 0) {
        return [NSString stringWithFormat:@"%dy", years];
    } else if (months > 0) {
        return [NSString stringWithFormat:@"%dm", months];
    } else if (daysOffline >= 21 && daysOffline < 30) {
        return @"3w";
    } else if (daysOffline >= 14 && daysOffline < 21) {
        return @"2w";
    } else if (daysOffline >= 14 && daysOffline < 21) {
        return @"2w";
    } else if (daysOffline >= 7 && daysOffline < 14) {
        return @"1w";
    } else if (daysOffline == 6) {
        return @"6d";
    } else if (daysOffline == 5) {
        return @"5d";
    } else if (daysOffline == 4) {
        return @"4d";
    } else if (daysOffline == 3) {
        return @"96h";
    } else if (daysOffline == 2) {
        return @"48h";
    } else if (hoursBetweenDates >= 24 && daysOffline < 2) {
        return @"24h";
    }
    return @"";
}

@end
