import UIKit

protocol ButtonPickerDelegate: class {
    
    func buttonDidPress()
    func buttonDidLongPress()
    func buttonDidLongPressEnd()
    
}

class ButtonPicker: UIButton {
    
    struct Dimensions {
        static let borderWidth: CGFloat = 2
        static let buttonSize: CGFloat = 48
        static let buttonBorderSize: CGFloat = 58
    }
    
    var videoTimer : Timer!
    var videoDuration = 0
    var configuration = Configuration()
    
    lazy var numberLabel: UILabel = { [unowned self] in
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = self.configuration.numberLabelFont
        
        return label
        }()
    
    weak var delegate: ButtonPickerDelegate?
    
    // MARK: - Initializers
    
    public init(configuration: Configuration? = nil) {
        if let configuration = configuration {
            self.configuration = configuration
        }
        super.init(frame: .zero)
        configure()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    func configure() {
        addSubview(numberLabel)
        
        subscribe()
        setupButton()
        setupConstraints()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func subscribe() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(recalculatePhotosCount(_:)),
                                               name: NSNotification.Name(rawValue: ImageStack.Notifications.imageDidPush),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(recalculatePhotosCount(_:)),
                                               name: NSNotification.Name(rawValue: ImageStack.Notifications.imageDidDrop),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(recalculatePhotosCount(_:)),
                                               name: NSNotification.Name(rawValue: ImageStack.Notifications.stackDidReload),
                                               object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    
    func setupButton() {
        backgroundColor = UIColor.white
        layer.cornerRadius = Dimensions.buttonSize / 2
        accessibilityLabel = "Take photo"
        addTarget(self, action: #selector(pickerButtonDidPress(_:)), for: .touchUpInside)
        addTarget(self, action: #selector(pickerButtonDidHighlight(_:)), for: .touchDown)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(pickerButtonDidPress(_:)))
        self.addGestureRecognizer(tapGestureRecognizer)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(pickerButtonDidLongPress(_:)))
        self.addGestureRecognizer(longPressRecognizer)
    }
    
    // MARK: - Actions
    
    @objc func recalculatePhotosCount(_ notification: Notification) {
        guard let sender = notification.object as? ImageStack else { return }
        numberLabel.text = sender.assets.isEmpty ? "" : String(sender.assets.count)
    }
    
    @objc func pickerButtonDidPress(_ button: UIButton) {
        backgroundColor = UIColor.white
        numberLabel.textColor = UIColor.black
        numberLabel.sizeToFit()
        delegate?.buttonDidPress()
    }
    
    
    @objc func pickerButtonDidLongPress(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began{
            backgroundColor = UIColor(red: 1.0, green: 0.58, blue: 0, alpha: 1)
            videoTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
                self.videoDuration += 1
                self.changeVideoDuration()
            })
            delegate?.buttonDidLongPress()
        }else if gesture.state == .ended{
            backgroundColor = UIColor.white
            videoDuration = 0
            self.changeVideoDuration()
            delegate?.buttonDidLongPressEnd()
        }
    }
    
    fileprivate func changeVideoDuration(){
        if videoDuration > 0{
            lblTimer.text = self.timeFormatted(videoDuration)
        }else{
            lblTimer.text = ""
            videoTimer.invalidate()
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = minutes % 60
        
        if hours > 0{
            return String(format: "%02d:%02d:%02d", hours,minutes, seconds)
        }
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    
    @objc func pickerButtonDidHighlight(_ button: UIButton) {
        numberLabel.textColor = UIColor.white
        backgroundColor = configuration.AppTint
    }
}
