//
//  SLocationService.m
//  Serendipity
//
//  Created by Mcuser on 12/18/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import "SLocationService.h"
#import "SRModalClass.h"
#import "SRAppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "HSLocalNotificationManger.h"
#import "HSLocationManager.h"
#import "LocationFirstResponseViewController.h"

@implementation SLocationService
@synthesize server;
#define SEND_MAIL NO
#define DEBUG_LOCATION NO

+ (instancetype)sharedInstance {
    static SLocationService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SLocationService alloc] init];
    });
    return sharedInstance;
}

- (TSLocationManager *)getLocationManager {
    if (!bgGeo) {
        assert("Location manager not initialized");
    }
    return bgGeo;
}

- (void)initLocationTracker:(SRServerConnection *)serverData {

    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"locationTutorialDone"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    if (![APP_DELEGATE isLocationTutorialDone] && !TARGET_IPHONE_SIMULATOR) {
        return;
    }
    server = serverData;
    urlString = [NSString stringWithFormat:@"%@%@%@", kSerendipityServer, kKeyClassUpdateLocation, server.loggedInUserInfo[kKeyId]];

    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];
    TSConfig *config = [TSConfig sharedInstance];
    [config reset];
    
    UIViewController *root = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    // For #emailLog method.  While debugging, the plugin can optionally email you its log-file.  Providing a reference to viewController allows the plugin to launch email client.
    bgGeo.viewController = root;

    // Add "location" event listener
    void (^success)(TSLocation *) = ^void(TSLocation *tsLocation) {
        //[[HSLocalNotificationManger sharedManager] triggerLocationNotification:@"NEW" :@"NEW FLUCK YOU**"];

        addTextInSignificantTxt([NSString stringWithFormat:@"Location call back method executed successfully"]);
        (APP_DELEGATE).lastLocation = tsLocation.location;
        server.myLocation = (APP_DELEGATE).lastLocation;
        // Notify all app that location updated
        [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationLocationChanged object:nil];
    };
    void (^failure)(NSError *) = ^void(NSError *error) {
        switch ([error code]) {
            case kCLErrorNetwork: // general, network-related error
            {
                addTextInSignificantTxt([NSString stringWithFormat:@"Network Connection Problem : %@", error]);
            }
                break;
            case kCLErrorDenied: {
                addTextInSignificantTxt([NSString stringWithFormat:@"user has denied to use current Location : %@", error]);
            }
                break;
            default: {
                addTextInSignificantTxt([NSString stringWithFormat:@"unknown network error: %@", error]);
            }
                break;
        }
      //  NSLog(@"*** event location error: %@", error);
    };
    [bgGeo onLocation:success failure:failure];
    
    [[TSLocationManager sharedInstance] onProviderChange:^(TSProviderChangeEvent *event) {
        CLAuthorizationStatus status = event.status;
       // NSLog(@"[providerchange] enabled: %d",status);
        if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusRestricted || status == kCLAuthorizationStatusDenied || ([APP_DELEGATE isLocationTutorialDone] && status == kCLAuthorizationStatusNotDetermined)){

            [[HSLocalNotificationManger sharedManager] triggerLocationNotification:@"" :kKeyLocationPermission];
        }else  if(status == kCLAuthorizationStatusAuthorizedAlways){
            
            UIViewController* currentVC = [[HSLocationManager sharedManager] topViewController];
            
            if ([currentVC isKindOfClass:LocationFirstResponseViewController.self]){
                [currentVC dismissViewControllerAnimated:YES completion:nil];
            }
            [[HSLocalNotificationManger sharedManager] removeLocationNotification];
        }
    }];
    
    // Add motionchange event listener
    void (^callback)(TSLocation *) = ^void(TSLocation *tsLocation) {
        NSDictionary *params = @{
                @"isMoving": @(tsLocation.isMoving),
                @"location": [tsLocation toDictionary]
        };
       // NSLog(@"*** event motionchange: %@, %@", tsLocation, params);
        [bgGeo sync:^(NSArray *locations) {
            //
        } failure:^(NSError *error) {
            //
        }];
    };
    [bgGeo onMotionChange:callback];

//    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"kTrackingSettings"] == nil) {
        [config updateWithBlock:^(TSConfigBuilder *builder) {
            builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
            builder.logLevel = tsLogLevelOff;
            builder.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            builder.distanceFilter = 3; // filter for mooving across area. Currently changed to 3 meters
            builder.stopOnTerminate = NO;
            builder.stationaryRadius = 1;
            builder.stopOnStationary = NO;
            builder.startOnBoot = YES;
            builder.disableElasticity = YES;
            builder.autoSync = YES;
            builder.url = urlString;
            builder.method = @"PUT";
            builder.heartbeatInterval = 900;
            builder.stopTimeout = 15;
            builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                    @"Authorization": server.token
            };
        }];
//    }


    // Configuration object.
    //    NSDictionary *config = @{
    //                             @"desiredAccuracy": @(0),
    //                             @"distanceFilter": @(10),
    //                             @"debug": @(false),
    //                             @"desiredOdometerAccuracy":@(5),
    //                             @"stopOnTerminate":@(false),
    //                             @"heartbeatInterval":@(900),
    //                             @"stopTimeout":@(15),
    //                             @"logLevel": @(5),
    //                             @"url":urlString,
    //                             @"preventSuspend":@(true),
    //                             @"method":@"PUT",
    //                             @"headers":@{
    //                                     @"Content-Type":@"application/json; charset=utf-8",
    //                                     @"Authorization":server.token
    //                                     },
    //                             @"autoSync":@(true)
    //                             };
    // Configure the plugin
    //@"schedule":@[@"1-7 00:00-06:00"],
    [bgGeo ready];

    [self initCompass:bgGeo];


    void (^onHeartbeat)(TSHeartbeatEvent *) = ^void(TSHeartbeatEvent *event) {
       // NSLog(@"- onHeartbeat: %@", event);
    };
    if ([MFMailComposeViewController canSendMail] && SEND_MAIL) {
        [bgGeo emailLog:@"serendipityawaits@googlegroups.com" success:^{
           // NSLog(@" - Successfully sent device log to sunil1@gmail.com");
        }       failure:^(NSString *error) {
           // NSLog(@" - Email log failure: %@", error);
        }];
    }
    void (^onHttp)(TSHttpEvent *) = ^void(TSHttpEvent *event) {
        
      //  NSLog(@"\nHS Status: %ld",event.statusCode);
        
        NSData *data = [event.responseText dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSLog(@"%@", [json objectForKey:@"success"]);
        if ([[json objectForKey:@"success"] boolValue] == 0 && [json objectForKey:@"success"] != nil) {
            (APP_DELEGATE).lblLocationText = NSLocalizedString(@"lbl.location_not_available.txt", @"");
            [APP_DELEGATE hideActivityIndicator];
        } else {
            NSDictionary *dictData = [json objectForKey:@"response"];
            
            if ([[json objectForKey:@"response"] isKindOfClass:[NSString classForCoder]] && [[[NSString stringWithFormat:@"%@", dictData] lowercaseString] isEqualToString:@"unauthenticated"]) {
                return;
            }
            
            NSDictionary *dictLoc = dictData[@"location"];
            if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeyBroadcastLocation] boolValue] == YES) {
                NSDate *now = [NSDate date];
                NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
                NSInteger hour = [dateComponents hour];
                NSInteger minutes = [dateComponents minute];
                if ([[server.loggedInUserInfo valueForKey:kKeySetting] objectForKey:kkeySmartBatteryUsage]) {
                    if ([[[server.loggedInUserInfo valueForKey:kKeySetting] valueForKey:kkeySmartBatteryUsage] boolValue] == YES && hour < 6) {
                        int differenceInHour = 6 - (int) hour;
                        differenceInHour = (3600 * differenceInHour) - (60 * (int) minutes);
                        [(APP_DELEGATE).locationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:differenceInHour];
                    }
                }
            }
            if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
                //App is in foreground. Act on it.
                updatedLocation = dictLoc[kKeyLiveAddress];
                if (updatedLocation == nil) {
                    updatedLocation = @"";
                }
                NSDictionary *locDict = @{kKeyLiveAddress: updatedLocation};
                [server.loggedInUserInfo setValue:locDict forKey:kKeyLocation];
                if ([@"" isEqualToString:updatedLocation]) {
                    (APP_DELEGATE).lblLocationText = NSLocalizedString(@"lbl.location_not_available.txt", @"");
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:kKeyNotificationRadarLocationUpdated object:nil];
            }
        }
       // NSLog(@"- onHttp: %@", event);
    };
    if (!config.enabled) {
        // Start tracking immediately (if not already).
        [bgGeo start];
    }
    [bgGeo onHttp:onHttp];
    //[bgGeo startSchedule];
    [bgGeo onHeartbeat:onHeartbeat];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Start tracking.Calling #start here would normally be executed from a UI action rather than automatically being called here.
        [bgGeo start];
        [bgGeo locationManager].pausesLocationUpdatesAutomatically = false;
//        [bgGeo locationManager].showsBackgroundLocationIndicator = true;
        [bgGeo locationManager].allowsBackgroundLocationUpdates = true;
    });
}

- (void)setRealtimeSettings {

    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];
    TSConfig *config = [TSConfig sharedInstance];
    [config reset];
    [config updateWithBlock:^(TSConfigBuilder *builder) {
        builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
        builder.logLevel = tsLogLevelVerbose;
        builder.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        builder.distanceFilter = 3; // filter for mooving across area. Currently changed to 3 meters
        builder.stopOnTerminate = NO;
        builder.stationaryRadius = 1;
        builder.stopOnStationary = NO;
        builder.preventSuspend = YES;
        builder.startOnBoot = YES;
        builder.disableElasticity = YES;
        builder.autoSync = YES;
        builder.url = urlString;
        builder.method = @"PUT";
        builder.heartbeatInterval = 900;
        builder.stopTimeout = 15;
        builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                @"Authorization": server.token
        };
    }];
    [bgGeo ready];
    if (!config.enabled) {
        // Start tracking immediately (if not already).
       // NSLog(@" ℹ️ Changed Plugin settings to Realtime ℹ️");
        [bgGeo start];
    }

}

- (void)setMediumSettings {

    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];
    TSConfig *config = [TSConfig sharedInstance];
    [config reset];
    [config updateWithBlock:^(TSConfigBuilder *builder) {
        builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
        builder.logLevel = tsLogLevelVerbose;
        builder.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        builder.distanceFilter = 45; // filter for mooving across area. Currently changed to 3 meters
        builder.stopOnTerminate = NO;
        builder.stationaryRadius = 1;
        builder.stopOnStationary = NO;
        builder.preventSuspend = YES;
        builder.startOnBoot = YES;
        builder.disableElasticity = YES;
        builder.autoSync = YES;
        builder.url = urlString;
        builder.method = @"PUT";
        builder.heartbeatInterval = 900;
        builder.stopTimeout = 15;
        builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                @"Authorization": server.token
        };
    }];
    [bgGeo ready];
    if (!config.enabled) {
        // Start tracking immediately (if not already).
       // NSLog(@" ℹ️ Changed Plugin settings to Medium ℹ️");
        [bgGeo start];
    }
}

- (void)setOptimalSettings {

    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];
    TSConfig *config = [TSConfig sharedInstance];
    [config updateWithBlock:^(TSConfigBuilder *builder) {
        builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
        builder.logLevel = tsLogLevelVerbose;
        builder.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        builder.distanceFilter = 8; // filter for mooving across area. Currently changed to 3 meters
        builder.stopOnTerminate = NO;
        builder.stationaryRadius = 1;
        builder.stopOnStationary = NO;
        builder.preventSuspend = YES;
        builder.startOnBoot = YES;
        builder.disableElasticity = YES;
        builder.autoSync = YES;
        builder.url = urlString;
        builder.method = @"PUT";
        builder.heartbeatInterval = 900;
        builder.stopTimeout = 15;
        builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                @"Authorization": server.token
        };
    }];
    [bgGeo ready];
    if (!config.enabled) {
        // Start tracking immediately (if not already).
        NSLog(@" ℹ️ Changed Plugin settings to Optimal ℹ️");
        [bgGeo start];
    }
}

- (void)setLowSettings {

    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];
    TSConfig *config = [TSConfig sharedInstance];
    [config reset];
    [config updateWithBlock:^(TSConfigBuilder *builder) {
        builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
        builder.logLevel = tsLogLevelVerbose;
        builder.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        builder.distanceFilter = 150; // filter for mooving across area. Currently changed to 3 meters
        builder.stopOnTerminate = NO;
        builder.stationaryRadius = 1;
        builder.stopOnStationary = NO;
        builder.preventSuspend = NO;
        builder.startOnBoot = YES;
        builder.disableElasticity = YES;
        builder.autoSync = YES;
        builder.url = urlString;
        builder.method = @"PUT";
        builder.heartbeatInterval = 900;
        builder.stopTimeout = 15;
        builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                @"Authorization": server.token
        };
    }];
    [bgGeo ready];
    if (!config.enabled) {
        // Start tracking immediately (if not already).
        NSLog(@" ℹ️ Changed Plugin settings to Low ℹ️");
        [bgGeo start];
    }
}

//Ground mode
- (void)setGroundSettings {
    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];
    TSConfig *config = [TSConfig sharedInstance];
    [config reset];
    [config updateWithBlock:^(TSConfigBuilder *builder) {
        builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
        builder.logLevel = tsLogLevelVerbose;
        builder.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        builder.distanceFilter = 150; // filter for mooving across area. Currently changed to 3 meters
        builder.elasticityMultiplier = 5;
        builder.stopOnTerminate = NO;
        builder.stationaryRadius = 1;
        builder.stopOnStationary = NO;
        builder.preventSuspend = YES;
        builder.startOnBoot = YES;
        builder.disableElasticity = NO;
        builder.autoSync = YES;
        builder.url = urlString;
        builder.method = @"PUT";
        builder.heartbeatInterval = 900;
        builder.stopTimeout = 15;
        builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                @"Authorization": server.token
        };
    }];
    [bgGeo ready];
    if (!config.enabled) {
        // Start tracking immediately (if not already).
     //   NSLog(@" ℹ️ Changed Plugin settings to Ground Travel ℹ️");
        [bgGeo start];
    }
}

//Air mode
- (void)setAirSettings {
    TSLocationManager *bgGeo = [TSLocationManager sharedInstance];
    [bgGeo stop];
    [bgGeo stopSchedule];

    TSConfig *config = [TSConfig sharedInstance];
    [config reset];
    [config updateWithBlock:^(TSConfigBuilder *builder) {
        builder.debug = DEBUG_LOCATION;   // Enable debug mode for having notifications while moving in location!!!!
        builder.logLevel = tsLogLevelVerbose;

        // for desired accuracy value
        // https://transistorsoft.github.io/react-native-background-geolocation/modules/_react_native_background_geolocation_.html#locationaccuracy
        // builder.desiredAccuracy = 100;

        builder.desiredAccuracy = 0;
        builder.distanceFilter = 3000; // track location each 10km
        builder.elasticityMultiplier = 10;
        builder.stopOnTerminate = NO;
        builder.stationaryRadius = 1;
        builder.stopOnStationary = NO;
        builder.preventSuspend = YES;
        builder.startOnBoot = YES;
        builder.disableElasticity = NO;
        builder.autoSync = YES;
        builder.url = urlString;
        builder.method = @"PUT";
        builder.schedule = [self getScheduledTime];
        builder.heartbeatInterval = 900;
        builder.stopTimeout = 15;
        builder.headers = @{@"Content-Type": @"application/json; charset=utf-8",
                @"Authorization": server.token
        };
    }];
    [bgGeo ready];
    if (!config.enabled) {
        // Start tracking immediately (if not already).
        [bgGeo startSchedule];
        //  [bgGeo start];
        NSLog(@" ℹ️ Changed Plugin settings to Air Travel ℹ️");
    }
}

- (BOOL)isLocationEnabled {
    return [CLLocationManager locationServicesEnabled];
}

- (NSArray *)getScheduledTime {
    return @[
            @"1-7 0:00-0:00",
            @"1-7 0:14-0:15",
            @"1-7 0:29-0:30",
            @"1-7 0:44-0:45",
            @"1-7 1:00-1:00",
            @"1-7 1:14-1:15",
            @"1-7 1:29-1:30",
            @"1-7 1:44-1:45",
            @"1-7 2:00-2:00",
            @"1-7 2:14-2:15",
            @"1-7 2:29-2:30",
            @"1-7 2:44-2:45",
            @"1-7 3:00-3:00",
            @"1-7 3:14-3:15",
            @"1-7 3:29-3:30",
            @"1-7 3:44-3:45",
            @"1-7 4:00-4:00",
            @"1-7 4:14-4:15",
            @"1-7 4:29-4:30",
            @"1-7 4:44-4:45",
            @"1-7 5:00-5:00",
            @"1-7 5:14-5:15",
            @"1-7 5:29-5:30",
            @"1-7 5:44-5:45",
            @"1-7 6:00-6:00",
            @"1-7 6:14-6:15",
            @"1-7 6:29-6:30",
            @"1-7 6:44-6:45",
            @"1-7 7:00-7:00",
            @"1-7 7:14-7:15",
            @"1-7 7:29-7:30",
            @"1-7 7:44-7:45",
            @"1-7 8:00-8:00",
            @"1-7 8:14-8:15",
            @"1-7 8:29-8:30",
            @"1-7 8:44-8:45",
            @"1-7 9:00-9:00",
            @"1-7 9:14-9:15",
            @"1-7 9:29-9:30",
            @"1-7 9:44-9:45",
            @"1-7 10:00-10:00",
            @"1-7 10:14-10:15",
            @"1-7 10:29-10:30",
            @"1-7 10:44-10:45",
            @"1-7 11:00-11:00",
            @"1-7 11:14-11:15",
            @"1-7 11:29-11:30",
            @"1-7 11:44-11:45",
            @"1-7 12:00-12:00",
            @"1-7 12:14-12:15",
            @"1-7 12:29-12:30",
            @"1-7 12:44-12:45",
            @"1-7 13:00-13:00",
            @"1-7 13:14-13:15",
            @"1-7 13:29-13:30",
            @"1-7 13:44-13:45",
            @"1-7 14:00-14:00",
            @"1-7 14:14-14:15",
            @"1-7 14:29-14:30",
            @"1-7 14:44-14:45",
            @"1-7 15:00-15:00",
            @"1-7 15:14-15:15",
            @"1-7 15:29-15:30",
            @"1-7 15:44-15:45",
            @"1-7 16:00-16:00",
            @"1-7 16:14-16:15",
            @"1-7 16:29-16:30",
            @"1-7 16:44-16:45",
            @"1-7 17:00-17:00",
            @"1-7 17:14-17:15",
            @"1-7 17:29-17:30",
            @"1-7 17:44-17:45",
            @"1-7 18:00-18:00",
            @"1-7 18:14-18:15",
            @"1-7 18:29-18:30",
            @"1-7 18:44-18:45",
            @"1-7 19:00-19:00",
            @"1-7 19:14-19:15",
            @"1-7 19:29-19:30",
            @"1-7 19:44-19:45",
            @"1-7 20:00-20:00",
            @"1-7 20:14-20:15",
            @"1-7 20:29-20:30",
            @"1-7 20:44-20:45",
            @"1-7 21:00-21:00",
            @"1-7 21:14-21:15",
            @"1-7 21:29-21:30",
            @"1-7 21:44-21:45",
            @"1-7 22:00-22:00",
            @"1-7 22:14-22:15",
            @"1-7 22:29-22:30",
            @"1-7 22:44-22:45",
            @"1-7 23:00-23:00",
            @"1-7 23:14-23:15",
            @"1-7 23:29-23:30",
            @"1-7 23:44-23:45"
    ];
}

- (void)initCompass:(TSLocationManager *)bgGeo {

    /*
    -(BOOL) isMotionHardwareAvailable;
    -(BOOL) isDeviceMotionAvailable;
    -(BOOL) isAccelerometerAvailable;
    -(BOOL) isGyroAvailable;
    -(BOOL) isMagnetometerAvailable;
     */

    if (bgGeo.isMagnetometerAvailable) {
      //  NSLog(@"MagnetometerAvailable");
    }
    if (bgGeo.isDeviceMotionAvailable) {
      //  NSLog(@"DeviceMotionAvailable");
    }
    if (bgGeo.isAccelerometerAvailable) {
      //  NSLog(@"AccelerometerAvailable");
    }
    if (bgGeo.isGyroAvailable) {
      //  NSLog(@"GyroAvailable");
    }
    if (bgGeo.isMotionHardwareAvailable) {
     //   NSLog(@"MotionHardwareAvailable");
    }

}

- (void)emailLocationLogs {
    dispatch_async(dispatch_get_main_queue(), ^{
#warning pass an email, where you want to have user's location logs. I have set default to sunil's mail.
        [[TSLocationManager sharedInstance] emailLog:@"sunil1@gmail.com" success:^{
            
        } failure:^(NSString *error) {
            printf(@"%@", error.localizedLowercaseString);
        }];
    });
}

@end
