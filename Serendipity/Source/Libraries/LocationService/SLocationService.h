//
//  SLocationService.h
//  Serendipity
//
//  Created by Mcuser on 12/18/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>
@import TSLocationManager;
@import TSBackgroundFetch;

NS_ASSUME_NONNULL_BEGIN

@interface SLocationService : NSObject {
    TSLocationManager *bgGeo;
    NSString *updatedLocation;
    NSString *urlString;
}

+ (instancetype) sharedInstance;
+ (void)reset;
- (void)emailLocationLogs;

-(void)initLocationTracker:(SRServerConnection*)server;
-(TSLocationManager*)getLocationManager;
-(BOOL)isLocationEnabled;
-(void)setRealtimeSettings;
-(void)setMediumSettings;
-(void)setOptimalSettings;
-(void)setLowSettings;
-(void)setGroundSettings;
-(void)setAirSettings;
@property SRServerConnection *server;
@end

NS_ASSUME_NONNULL_END
