//
//  CBZRasterSplashView.m
//  Telly
//
//  Created by Mazyad Alabduljaleel on 8/7/14.
//  Copyright (c) 2014 Telly, Inc. All rights reserved.
//
#import "Constant.h"
#import "CBZRasterSplashView.h"
#import "SRSplashViewController.h"
@interface CBZRasterSplashView ()

@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, strong) UIImageView *iconImageView;

@end

@implementation CBZRasterSplashView

- (instancetype)initWithIconImage:(UIImage *)icon backgroundColor:(UIColor *)color andManageVC:(UIViewController *)vc {
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self) {
        self.backgroundColor = color;
        
        UIImageView *iconImageView = [UIImageView new];
        iconImageView.image = [icon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        iconImageView.tintColor = self.iconColor;
        iconImageView.frame = CGRectMake(0, 0, 50, 60);
        iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        //   [iconImageView layer].anchorPoint = CGPointMake(0.0f, 0.0f);
        iconImageView.center = CGPointMake(self.center.x, self.center.y - 3);
        [self addSubview:iconImageView];
        
        _iconImageView = iconImageView;
        _manageVC = vc;
    }
    return self;
}

- (void)startAnimationWithCompletionHandler:(void (^)())completionHandler {
    __block __weak typeof(self) weakSelf = self;
    
    if (!self.animationDuration) {
        return;
    }
    
    CGFloat shrinkDuration = self.animationDuration * 0.3;
    CGFloat growDuration = self.animationDuration * 1.0;
    
    [UIView animateWithDuration:shrinkDuration delay:0 usingSpringWithDamping:0.7f initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations: ^{
        CGAffineTransform scaleTransform = CGAffineTransformMakeScale(0.75, 0.75);
        weakSelf.iconImageView.transform = scaleTransform;
    } completion: ^(BOOL finished) {
        // ((SRSplashViewController *)self.manageVC).imgViewLargeIcon.alpha = 0.4;
        
        [UIView animateWithDuration:growDuration animations: ^{
            CGAffineTransform scaleTransform = CGAffineTransformMakeScale(20, 20);
            weakSelf.iconImageView.transform = scaleTransform;
            weakSelf.alpha = 0;
            ((SRSplashViewController *)self.manageVC).lblQuote.hidden = YES;
            [((SRSplashViewController *)self.manageVC).progressView setProgress:0.99 animated:NO];
            ((SRSplashViewController *)self.manageVC).lblProgressCount.text = @"99%";
            // ((SRSplashViewController *)self.manageVC).imgViewLargeIcon.hidden = NO;
            // ((SRSplashViewController *)self.manageVC).imgViewLargeIcon.alpha = 1.0;
        } completion: ^(BOOL finished) {
            [self pushToNextView];
            
            [weakSelf removeFromSuperview];
            if (completionHandler) {
                completionHandler();
            }
            
            /* ((SRSplashViewController *)self.manageVC).imgViewLargeIcon.alpha = 1.0;
             [((SRSplashViewController *)self.manageVC).progressView setProgress:1.0 animated:NO];
	            ((SRSplashViewController *)self.manageVC).lblProgressCount.text = @"100%";
             [self performSelector:@selector(pushToNextView) withObject:nil afterDelay:0.8];*/
            // [self pushToNextView];
        }];
    }];
    
    //    [UIView animateWithDuration:growDuration animations:^{
    //        CGAffineTransform scaleTransform = CGAffineTransformMakeScale(20, 20);
    //        weakSelf.iconImageView.transform = scaleTransform;
    //        weakSelf.alpha = 0;
    //        weakSelf.manageVC.view.alpha = 0;
    //    } completion:^(BOOL finished) {
    //        [weakSelf removeFromSuperview];
    //        if (completionHandler) {
    //            completionHandler();
    //        }
    //    }];
}

- (void)pushToNextView {
    [((SRSplashViewController *)self.manageVC) pushToPreLoginView];
}

@end
