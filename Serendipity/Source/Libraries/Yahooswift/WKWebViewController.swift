//
//  WKWebViewController.swift
//  Serendipity
//
//  Created by OKharchenko on 5/18/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

import Foundation
import WebKit
import OAuthSwift

typealias CompletionHandler = (_ success:Bool) -> Void

class WKWebViewController: OAuthWebViewController {
    var webView: WKWebView!
    var targetURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func handle(_ url: URL) {
        targetURL = url
        super.handle(url)
        loadAddressURL()
    }
    
    func loadAddressURL() {
        guard let url = targetURL else { return }
        let req = URLRequest(url: url)
        
        self.webView?.load(req)
    }
}

extension WKWebViewController: WKUIDelegate, WKNavigationDelegate {
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.allowsBackForwardNavigationGestures = true
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("loaded")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        // Check for OAuth Callback
        if let url = navigationAction.request.url, url.absoluteString.contains("serendipityapp://oauth-swift/oauth-callback?code=") {
           // UIApplication.shared.open(url, options: [:], completionHandler: nil)
            let code = navigationAction.request.url?.valueOf("code")
            UserDefaults.standard.setValue(code, forKey: "YAHOO_AUTHORIZATION_CODE")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "YAHOO_GET_CODE"), object: code)
            self.dismiss(animated: true, completion: nil)
            decisionHandler(.cancel)
            
            return
        }
        
//        // Restrict URL's a user can access
//        if let host = navigationAction.request.url?.host {
//            if host.contains("spotify") {
//                decisionHandler(.allow)
//                return
//            } else {
//                // open link outside of our app
//                UIApplication.shared.open(navigationAction.request.url!)
//                decisionHandler(.cancel)
//                return
//            }
//        }
        
        decisionHandler(.allow)
        
    }
}

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
