//
//  YahooSwift.swift
//  Serendipity
//
//  Created by OKharchenko on 5/17/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

import Foundation
import OAuthSwift
import p2_OAuth2
import SwiftyXMLParser


@objc
class YahooSwift : NSObject {
    
    var consumer: String?
    var secret: String?
    let nc = NotificationCenter.default
    var oauthswift: OAuth2Swift?

     func sayHello() {
        print("Hello swift")
    }
    
    @objc open func createSession(consumerKey: String, secretKey: String, callbackUrl: String, authorizeUrl: String, viewController: UIViewController) {
        self.consumer = consumerKey
        self.secret = secretKey
        
        let internalWebViewController: WKWebViewController = {
            let controller = WKWebViewController()
            controller.view = UIView(frame: UIScreen.main.bounds)
            controller.loadView()
            controller.viewDidLoad()
            return controller
        }()
        
        oauthswift =
       OAuth2Swift(consumerKey: consumerKey, consumerSecret: consumerKey, authorizeUrl: authorizeUrl, accessTokenUrl: "https://api.login.yahoo.com/oauth2/get_token", responseType: "code")
        
        
        
        
        oauthswift?.authorizeURLHandler = internalWebViewController
        
        // Register to receive notification in your class
        nc.addObserver(self,
                                                     selector: #selector(self.gotCode(notification:)),
                                                     name: NSNotification.Name(rawValue: "YAHOO_GET_CODE"),
                                                     object: nil)

//
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(self.gotCode(notification:)),
//            name: Notification.Name("YAHOO_GET_CODE"),
//            object: nil)

        _ = oauthswift?.authorize(withCallbackURL: callbackUrl, scope: "", state: "", success: { (credentials, response, params) in
            print(credentials.oauthToken)
        }) { (error) in
            print(error.description)
        }
        
        
    }
    
    @objc func gotCode(notification: NSNotification){
        let authorizedCode = UserDefaults.standard.object(forKey: "YAHOO_AUTHORIZATION_CODE") as! String
        
        // UTF 8 str from original
        // NSData! type returned (optional)
        let str = (self.consumer != nil ? self.consumer! : "") + ":" + (self.secret != nil ? self.secret! : "")
        let utf8str = str.data(using: String.Encoding.utf8)
        
        // Base64 encode UTF 8 string
        // fromRaw(0) is equivalent to objc 'base64EncodedStringWithOptions:0'
        // Notice the unwrapping given the NSData! optional
        // NSString! returned (optional)
        let base64Encoded = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))//base64EncodedStringWithOptions(NSData.Base64EncodingOptions.fromRaw(0)!)
        print("Encoded:  \(String(describing: base64Encoded))")
        print("Generating token")
        
        oauthswift?.client.post("https://api.login.yahoo.com/oauth2/get_token", parameters: ["code" : authorizedCode, "grant_type" : "authorization_code", "redirect_uri" : "oob"], headers: [ "Authorization" : "Basic " + base64Encoded!], body: nil, success: { (response) in
            let json = self.dataToJSON(data: response.data) as! [String:AnyObject]
            UserDefaults.standard.set(json["refresh_token"], forKey: "YAHOO_REFRESH_TOKEN")
            UserDefaults.standard.set(json["access_token"], forKey: "YAHOO_ACCESS_TOKEN")
            UserDefaults.standard.set(json["expires_in"], forKey: "YAHOO_EXPIRES_IN")
            UserDefaults.standard.set(json["xoauth_yahoo_guid"], forKey: "YAHOO_GUID")
            
            print(json["xoauth_yahoo_guid"] as Any)
            print(json["expires_in"] as Any)
            print(json["access_token"] as Any)
            print(json["refresh_token"] as Any)
            print("success")
            
            self.oauthswift?.client.get("https://social.yahooapis.com/v1/me/guid", parameters: ["access_token" : json["access_token"] as! String], headers: [:], success: { (response) in
                let myProfile = self.dataToJSON(data: response.data)
                
                
                if let profileDict = myProfile as? NSMutableDictionary,let guidDict = profileDict["guid"] as? NSMutableDictionary,let value = guidDict["value"] as? String{
                    print(value)
              //  }
                
                
               
                var xmlString = NSString(data: response.data, encoding: String.Encoding.utf8.rawValue)
                var _: NSError?
                    _ = try! XML.parse(xmlString! as String)
              //  if let value = xml.guid.value.text {
                    print(value)
                    UserDefaults.standard.set(value, forKey: "YAHOO_USER_GUID")
                    let queryContacts = "https://social.yahooapis.com/v1/user/" + value + "/contacts"
                    
                    self.oauthswift?.client.get(queryContacts, parameters: ["access_token" : json["access_token"] as! String], headers:[:], success: { (response) in
                        xmlString = NSString(data: response.data, encoding: String.Encoding.utf8.rawValue);
                        let xml = try! XML.parse(xmlString! as String)
                        let contacts = xml["contacts"]
                        let importedContacts = NSMutableArray()
                        for hit in contacts {
                            for fields in hit.contact {
                                let userDict = NSMutableDictionary()
                                var userEmail = ""
                                for field in fields.fields {
                                    if let type = field.type.text {
                                        
                                        if type == "name" {
                                            if let userName = field.value.givenName.text {
                                                userDict["first_name"] = userName as String
                                            }
                                            if let lastName = field.value.familyName.text {
                                                userDict["last_name"] = lastName as String
                                            }
                                            
                                        }
                                        
                                        if type == "email" {
                                            userEmail = field.value.text! as String
                                            userDict["email"] = userEmail
                                        }
                                        if type == "phone" {
                                            print (field.value.text! as String)
                                            userDict["mobile_number"] = field.value.text! as String
                                            importedContacts.add(userDict)
                                            continue
                                            print("User found")
                                        }
                                    }
                                }
                            }
                        }
                        print("Contacts loaded " + (NSString(format: "%d", importedContacts.count ) as String))
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "YAHOO_CONTACTS_LOADED"), object: importedContacts)
                        print("Import done")
                    }, failure: { (error) in
                        print("Failed loading contacts")
                    })
                    
                }
                print("success locading profile")
            }, failure: { (error) in
                print("Failed loading profile")
            })
        }, failure: { (error) in
            print("failed")
        })
    }
    
    func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
}
