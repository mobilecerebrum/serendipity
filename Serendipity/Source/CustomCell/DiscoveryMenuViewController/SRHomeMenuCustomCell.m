#import "SRHomeMenuCustomCell.h"

@implementation SRHomeMenuCustomCell

@synthesize MenuIconImage,MenuItemLabel,MenuItemCountLabel;

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
