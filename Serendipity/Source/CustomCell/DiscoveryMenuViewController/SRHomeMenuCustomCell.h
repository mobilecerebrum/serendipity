#import <UIKit/UIKit.h>

@interface SRHomeMenuCustomCell : UITableViewCell

@property(strong,nonatomic)IBOutlet UIImageView *MenuIconImage;
@property(strong,nonatomic)IBOutlet UILabel *MenuItemLabel;
@property(strong,nonatomic)IBOutlet UILabel *MenuItemCountLabel;
@property(strong,nonatomic)IBOutlet UIButton *MenuItemFacebookBTN;
@property(strong,nonatomic)IBOutlet UIButton *MenuItemGoogleBTN;
@property(strong,nonatomic)IBOutlet UIButton *MenuItemLinkedInBTN;
@property(strong,nonatomic)IBOutlet UIButton *MenuItemTwitterBTN;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuItemLableWidth;
@end
