//
//  SRAddressCell.m
//  Serendipity
//
//  Created by Abbas Mulani on 04/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRAddressCell.h"

@implementation SRAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_typeLabel setAdjustsFontSizeToFitWidth:YES];
    [_distanceLabel setAdjustsFontSizeToFitWidth:YES];
    _addCloseButton = (SRButton *)[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, _addressTextField.frame.size.height)];
    [_addCloseButton setImageEdgeInsets:UIEdgeInsetsMake(0, -8, 0, 0)];
    [_addCloseButton setImage:[UIImage imageNamed:@"add_address"] forState:UIControlStateNormal];
    [_addressTextField setRightView:_addCloseButton];
    _addressTextField.rightViewMode = UITextFieldViewModeAlways;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
