//
//  NotificationAddressCell.m
//  Serendipity
//
//  Created by Abbas Mulani on 08/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "NotificationAddressCell.h"
#import "TrackingLocationClass.h"
#import "SRTrackingLocationView.h"

@implementation NotificationAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(TrackingUserDetails *)obj indexPath:(NSIndexPath *)indexPath{
    
    _selectedLocationsArr =[[NSMutableArray alloc]init];
    trackingUserdetails = obj;
    //jonish CR
    _departureViewHeight.constant = 0;
    _departureLblHeight.constant = 0;

    _arrivalViewHeight.constant = 0;
    _arrivalLblHeight.constant = 0;

    _arivalDepartureViewHeight.constant = 0;
    _allNotificationLblHeight.constant = 0;

    _oneTimeViewHeight.constant = 0;
    _oneTimeLblHeight.constant = 0;
    
    _oneTimeDepartureHeight.constant = 0;
    _oneTimeDepartureLblHeight.constant = 0;
        
    if (obj.userImage) {
        
        _profileImage.image = obj.userImage;
    }
    NSMutableAttributedString* str = [self getAttributedString:[NSString stringWithFormat:@"%@",obj.username] andSecond:@"has requested to track you and receive the following notifications from the addresses below:" andThird:@""];
    
     NSMutableAttributedString* strWhenNoAddress = [self getAttributedString:[NSString stringWithFormat:@"%@",obj.username] andSecond:@"has requested to track you." andThird:@""];
    
    if ([obj.locationObjects objectAtIndex:0] != nil){
        TrackingLocationClass *locationObj =  [obj.locationObjects objectAtIndex: 0];
        if(locationObj.notify == nil || [locationObj.notify  isEqual: @0]){
            _nameLabel.attributedText = strWhenNoAddress;
        }else{
             _nameLabel.attributedText = str;
        }
    }else{
        _nameLabel.attributedText = strWhenNoAddress;
    }
    
   

    _allowButton.indexPath = indexPath;
    _closeButton.indexPath = indexPath;
    _scrollView.scrollEnabled = YES;
    
    NSMutableArray *allNotificationArray = [[NSMutableArray alloc] init];
    NSMutableArray *arrivalArray = [[NSMutableArray alloc] init];
    NSMutableArray *departureArray = [[NSMutableArray alloc] init];
    NSMutableArray *OneTimeArrivalArray = [[NSMutableArray alloc] init];
    NSMutableArray *OneTimeDepartureArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < obj.locationObjects.count; i++)
    {
        TrackingLocationClass *locationObj = [obj.locationObjects objectAtIndex:i];
        if (locationObj.isSelected) {
            [_selectedLocationsArr addObject:locationObj];
        }
        //Manage locations height
        int heightView = 49;
        int heightLbl = 35;
        SRTrackingLocationView *locationView = [[[NSBundle mainBundle] loadNibNamed:@"SRTrackingLocationView" owner:self options:nil] objectAtIndex:0];
        //self test
        if(locationObj.notify != nil){
            locationView.locationDetail.text = locationObj.address;
            locationView.name.text = locationObj.locationName;
            heightView = heightView + [self getLabelHeight:locationView.locationDetail];
            if((locationView != nil) && ([locationObj.notify_on isEqualToNumber:[NSNumber numberWithInt:3]]) && ([locationObj.notify isEqualToNumber:[NSNumber numberWithInt:2]])){
                [allNotificationArray addObject:locationObj];
                for (int j = 0; j <allNotificationArray.count; j++){
                    locationView.frame = CGRectMake(0, ((j * heightView) + heightLbl), (APP_DELEGATE).window.frame.size.width, heightView);
                    _arivalDepartureViewHeight.constant = heightLbl + (heightView * (j+1));
                    _allNotificationLblHeight.constant = heightLbl;
                    NSString *distance = [self convertKMtoFeet:locationObj.radius];
                    _arrivalDepartureLblOutlet.attributedText = [self getAttributedCustomString:@"Arrival/Departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distance] andThird:@"of:"];
                    [_locationView addSubview:locationView];
                }
            }else if((locationView != nil) && ([locationObj.notify_on  isEqualToNumber:[NSNumber numberWithInt:1]]) && ([locationObj.notify  isEqualToNumber:[NSNumber numberWithInt:2]])){
                [arrivalArray addObject:locationObj];
                for (int j = 0; j <arrivalArray.count; j++){
                    locationView.frame = CGRectMake(0, ((j * heightView) + heightLbl), (APP_DELEGATE).window.frame.size.width, heightView);
                    _arrivalViewHeight.constant = heightLbl + (heightView * (j+1));
                    _arrivalLblHeight.constant = heightLbl;
                    NSString *distance = [self convertKMtoFeet:locationObj.radius];
                    _arrivalLblOutlet.attributedText = [self getAttributedCustomString:@"Arrival Notifications within" andSecond:[NSString stringWithFormat:@"%@",distance] andThird:@"of:"];
                    [_arrivalViewOutlet addSubview:locationView];
                }
            }else if((locationView != nil) && ([locationObj.notify_on  isEqualToNumber:[NSNumber numberWithInt:2]]) && ([locationObj.notify  isEqualToNumber:[NSNumber numberWithInt:2]])){
                [departureArray addObject:locationObj];
                for (int j = 0; j <departureArray.count; j++){
                    locationView.frame = CGRectMake(0, ((j * heightView) + heightLbl), (APP_DELEGATE).window.frame.size.width, heightView);
                    _departureViewHeight.constant = heightLbl + (heightView * (j+1));
                    _departureLblHeight.constant = heightLbl;
                    NSString *distance = [self convertKMtoFeet:locationObj.radius];
                    _departureLblOutlet.attributedText = [self getAttributedCustomString:@"Departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distance] andThird:@"of:"];
                    [_departureViewOutlet addSubview:locationView];
                }
            }else if((locationView != nil) && ([locationObj.notify_on  isEqualToNumber:[NSNumber numberWithInt:1]]) && ([locationObj.notify  isEqualToNumber:[NSNumber numberWithInt:1]])){
                [OneTimeArrivalArray addObject:locationObj];
                for (int j = 0; j <OneTimeArrivalArray.count; j++){
                    locationView.frame = CGRectMake(0, ((j * heightView) + heightLbl), (APP_DELEGATE).window.frame.size.width, heightView);
                    _oneTimeViewHeight.constant = heightLbl + (heightView * (j+1));
                    _oneTimeLblHeight.constant = heightLbl;
                    NSString *distance = [self convertKMtoFeet:locationObj.radius];
                    _oneTimeArrivalOutlet.attributedText = [self getAttributedCustomString:@"One time arrival Notifications within" andSecond:[NSString stringWithFormat:@"%@",distance] andThird:@"of:"];
                    [_locationViewForOneTime addSubview:locationView];
                }
            }else if((locationView != nil) && ([locationObj.notify_on  isEqualToNumber:[NSNumber numberWithInt:2]]) && ([locationObj.notify  isEqualToNumber:[NSNumber numberWithInt:1]])){
                [OneTimeDepartureArray addObject:locationObj];
                for (int j = 0; j <OneTimeDepartureArray.count; j++){
                    locationView.frame = CGRectMake(0, ((j * heightView) + heightLbl), (APP_DELEGATE).window.frame.size.width, heightView);
                    _oneTimeDepartureHeight.constant = heightLbl + (heightView * (j+1));
                    _oneTimeDepartureLblHeight.constant = heightLbl;
                    NSString *distance = [self convertKMtoFeet:locationObj.radius];
                    _oneTimeDepatureOutlet.attributedText = [self getAttributedCustomString:@"One time departure Notifications within" andSecond:[NSString stringWithFormat:@"%@",distance] andThird:@"of:"];
                    [_oneTimeDepartureView addSubview:locationView];
                }
            }
            locationView.switchBtn.indexPath = indexPath;
            locationView.switchBtn.on = obj.pushNotificationStatus;
            locationView.switchBtn.transform = CGAffineTransformMakeScale(0.85, 0.85);
            locationView.switchBtn.layer.cornerRadius = 16.0;
            [locationView.switchBtn addTarget:self action:@selector(oneOffSwitchAction:) forControlEvents:UIControlEventValueChanged];
            
        }
        //change .xib file also
        //Manage locations height
    }
    
    // If notification switch is on
    if (obj.pushNotificationStatus) {
    
        _bottomViewHeightConstraint.constant = 91;
        [self setNeedsUpdateConstraints];
        
        if (obj.locationObjects && obj.locationObjects.count) {
            
            //Remove all objects from scrollview
            for (UIView *subview in[_scrollContentView subviews]) {
                if ([subview isKindOfClass:[UIView class]]) {
                    [subview removeFromSuperview];
                }
            }

            UIView *subView = [[UIView alloc]init];
            int x = 10;
            //Remove empty Address locations
            for (int i = 0; i < obj.locationObjects.count; i++)
            {
                TrackingLocationClass *locationObj = [obj.locationObjects objectAtIndex:i];
                if ([locationObj.locationName isEqual:[NSNull null]]) {
                    [obj.locationObjects removeObjectAtIndex:i];
                }
            }
            for (int i = 0; i < obj.locationObjects.count; i++) {
                TrackingLocationClass *locationObj = [obj.locationObjects objectAtIndex:i];
                subView.tag = indexPath.section;
                // Location name lbl
                UILabel *lblType = [[UILabel alloc]init];
                NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:kFontHelveticaMedium size:10]};
                CGFloat newWidth;
                if (locationObj.locationName!=(NSString *)[NSNull null])
                {
                    newWidth=  [locationObj.locationName sizeWithAttributes:attributes].width;
                    lblType.font = [UIFont fontWithName:kFontHelveticaMedium size:10.0];
                    lblType.frame = CGRectMake(x, 7, newWidth, 18);
                    lblType.text = locationObj.locationName;
                    lblType.textColor = [UIColor whiteColor];
                    //lblType.backgroundColor = [UIColor clearColor];
                    [subView addSubview:lblType];
                     x = x + lblType.frame.size.width +5;
                }
                UIButton *btnView = [[UIButton alloc]initWithFrame:CGRectMake(x, 7, 25, 25)];
                //btnView.backgroundColor = [UIColor clearColor];
                if (locationObj.isSelected) {
                    [btnView setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
                }
                else {
                    [btnView setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
                }
                
                // Add target
                btnView.tag = i;
                btnView.userInteractionEnabled = YES;
                subView.userInteractionEnabled = YES;
                [btnView addTarget:self action:@selector(actionOnRadioButton:) forControlEvents:UIControlEventTouchUpInside];
                [subView addSubview:btnView];
                x = x + btnView.frame.size.width +10;
            }
            
            subView.frame = CGRectMake(0, 0, x, 30);
            [_scrollContentView addSubview:subView];
            // Set constraints to content view within scrollview
            _contentViewWidthConstraint.constant = x+40;
            _contentViewWidthConstraint.priority = UILayoutPriorityRequired;
            [self setNeedsUpdateConstraints];

        }
        else{
            _bottomViewHeightConstraint.constant = 0;
        }
        
    }
    else{
    
        _bottomViewHeightConstraint.constant = 0;
    }
    
    NSArray *arr = [obj.dataDict valueForKey:kKeyLocation];
    NSNumber *notifyOn = arr[0][kKeyTrackNotify_On];
    if ([notifyOn intValue] == 1) {
        _lblArrivalORDeparture.text = @"Allow notifications of your arrival?";
    } else {
        _lblArrivalORDeparture.text = @"Allow notifications of your departure?";
    }
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setNeedsUpdateConstraints];
    //jonish CR
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}

//self test
-(NSMutableAttributedString *)getAttributedString:(NSString *)first andSecond:(NSString *)second andThird:(NSString *)third{
    NSString *strFirst = first;
    NSString *strSecond = second;
    NSString *strThird = third;
    UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];

   
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@ %@",strFirst,strSecond, strThird];

    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:appOrangeColor
                  range:[strComplete rangeOfString:strFirst]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strSecond]];
        
    [attributedString addAttribute:NSFontAttributeName
                   value: [UIFont boldSystemFontOfSize:14]
                   range:[strComplete rangeOfString:strFirst]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strThird]];

     
    return attributedString;
}

-(NSMutableAttributedString *)getAttributedCustomString:(NSString *)first andSecond:(NSString *)second andThird:(NSString *)third{
    NSString *strFirst = first;
    NSString *strSecond = second;
    NSString *strThird = third;
    UIColor *appOrangeColor = [UIColor colorWithRed:237.0f/255.0f green:152.0f/255.0f blue:33.0f/255.0f alpha:1.0f];

   
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@ %@",strFirst,strSecond, strThird];

    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strFirst]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:appOrangeColor
                  range:[strComplete rangeOfString:strSecond]];
        
    [attributedString addAttribute:NSFontAttributeName
                   value: [UIFont boldSystemFontOfSize:15]
                   range:[strComplete rangeOfString:strSecond]];

    [attributedString addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:[strComplete rangeOfString:strThird]];

     
    return attributedString;
}

-(NSString *)convertKMtoFeet:(NSString *)valueInKm{
     id unitt = (APP_DELEGATE).server.loggedInUserInfo[@"setting"][@"measurement_unit"];
     int measurementUnit = [unitt intValue];
    //1 = imperial -> Feet ,//0 = metrics ->meters
    //1km = 3280.84 feet
    //1km = 1000 meters
    //1km = 0.62 mile
    //radius is coming in kms from backend
    
    float value = [valueInKm floatValue];
    
    if (measurementUnit == 0){
        //metrics is selected ->show in meters
        float val =  value * 1000;
        int valueInFeet = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInFeet];
        return [NSString stringWithFormat:@"%d meters",roundOffValue];
        
    }else{
        //imperial -> Feet
        float val =  value * 3280.84;
        int valueInMeters = [[NSString stringWithFormat:@"%f", val] intValue];
        int roundOffValue = [self roundOFFvalueBy:valueInMeters];
        return [NSString stringWithFormat:@"%d feet",roundOffValue];
    }
}

-(int)roundOFFvalueBy:(int)value{
    double val = 50.0 * floor((value /50.0)+ 0.5);
    int valueRoundedOff = [[NSString stringWithFormat:@"%f", val] intValue];
    return valueRoundedOff;
}

//
//-(NSString *)convertKMtoFeet:(NSString *)valueInKm{
//
//    float value = [valueInKm floatValue];
//    float val =  value * 1000;
//     int valueInFeet = [[NSString stringWithFormat:@"%f", val] intValue];
//
//      return [NSString stringWithFormat:@"%d meter",valueInFeet];
//
//}
//self test
// ---------------------------------------------------------------------------------------------------------------------------------
// actionOnRadioButton:

- (void)actionOnRadioButton:(UIButton *)inSender {
    //NSInteger tag = inSender.tag;
    TrackingLocationClass *locationObj = [trackingUserdetails.locationObjects objectAtIndex:inSender.tag];
    if ([[inSender imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"radio_unchecked.png"]])
    {
        [inSender setImage:[UIImage imageNamed:@"radio_checked.png"] forState:UIControlStateNormal];
        [_selectedLocationsArr addObject:locationObj];
    }
    else
    {
        [inSender setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:UIControlStateNormal];
        [_selectedLocationsArr removeObject:locationObj];
    }
}

- (void)oneOffSwitchAction:(id)sender {
    SRSwitch *onOffSwitch = (SRSwitch *) sender;
    TrackingLocationClass *locationObj = [trackingUserdetails.locationObjects objectAtIndex:onOffSwitch.indexPath.row];
    if (onOffSwitch.on)
    {
        [_selectedLocationsArr addObject:locationObj];
    }
    else
    {
        [_selectedLocationsArr removeObject:locationObj];
    }
}

@end
