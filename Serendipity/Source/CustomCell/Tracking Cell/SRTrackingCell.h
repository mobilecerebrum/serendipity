//
//  SRTrackingCell.h
//  Serendipity
//
//  Created by Abbas Mulani on 05/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRButton.h"

@interface SRTrackingCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) IBOutlet SRButton *allowButton;
@property (strong, nonatomic) IBOutlet SRButton *closeButton;


@end
