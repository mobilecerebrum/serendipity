#import <UIKit/UIKit.h>

@interface SRTrackingViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCellBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileNameLabel;
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UIImageView *notificationBGImg;
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *allowBtn;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end
