//
//  TrackingSwitchCell.m
//  Serendipity
//
//  Created by Abbas Mulani on 04/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "TrackingSwitchCell.h"

@implementation TrackingSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _onOffSwitch.transform = CGAffineTransformMakeScale(0.85, 0.85);
    _onOffSwitch.layer.cornerRadius = 16.0;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
