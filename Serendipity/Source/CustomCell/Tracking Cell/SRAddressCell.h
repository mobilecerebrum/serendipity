//
//  SRAddressCell.h
//  Serendipity
//
//  Created by Abbas Mulani on 04/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRTextField.h"
#import "SRButton.h"
#import "SRLabel.h"

@interface SRAddressCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SRLabel *typeLabel;
@property (strong, nonatomic) IBOutlet SRTextField *addressTextField;
@property (strong, nonatomic) IBOutlet UIImageView *locationImageView;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel,*addrLabel;
@property (strong, nonatomic) IBOutlet SRButton *distanceButton;
@property (strong, nonatomic) SRButton *addCloseButton;

@end
