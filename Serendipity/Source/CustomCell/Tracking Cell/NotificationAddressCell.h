//
//  NotificationAddressCell.h
//  Serendipity
//
//  Created by Abbas Mulani on 08/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackingUserDetails.h"
#import "SRButton.h"
#import "SRSwitch.h"
#import "SRTrackingLocationView.h"

@protocol NotificationAddressCellDelegate <NSObject, UITableViewDataSource,UITableViewDelegate>

-(void)didSelectAddressAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface NotificationAddressCell : UITableViewCell
{
    UIView *trackLoc;
    TrackingUserDetails *trackingUserdetails;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (strong, nonatomic) IBOutlet SRSwitch *onOffSwitch;
@property (nonatomic,strong) id <NotificationAddressCellDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet SRButton *allowButton;
@property (strong, nonatomic) IBOutlet SRButton *closeButton;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *scrollContentView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidthConstraint;
@property (strong, nonatomic) NSMutableArray *selectedLocationsArr;
@property (weak, nonatomic) IBOutlet UILabel *lblArrivalORDeparture;
@property (weak, nonatomic) IBOutlet UITableView *locationTblView;
@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UIView *locationViewForOneTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arivalDepartureViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneTimeViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *departureViewHeight;
@property (weak, nonatomic) IBOutlet UIView *departureViewOutlet;
@property (weak, nonatomic) IBOutlet UIView *arrivalViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrivalViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrivalLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *departureLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneTimeLblHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allNotificationLblHeight;
@property (weak, nonatomic) IBOutlet UIView *oneTimeDepartureView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneTimeDepartureHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oneTimeDepartureLblHeight;

@property (weak, nonatomic) IBOutlet UILabel *arrivalDepartureLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *departureLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *arrivalLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *oneTimeDepatureOutlet;
@property (weak, nonatomic) IBOutlet UILabel *oneTimeArrivalOutlet;


-(void)configureCell:(TrackingUserDetails *)obj indexPath:(NSIndexPath *)indexPath;

@end
