//
//  TextMiddleAlignCell.h
//  Serendipity
//
//  Created by Abbas Mulani on 04/08/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextMiddleAlignCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
