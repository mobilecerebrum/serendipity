
#import <UIKit/UIKit.h>

@interface SRDiscoveryCustomCell : UITableViewCell

//Properties
@property (weak, nonatomic) IBOutlet UIButton *communityBtn;
@property (weak, nonatomic) IBOutlet UIImageView *GroupImage;
@property (weak, nonatomic) IBOutlet UILabel *GroupLbl;
@end
