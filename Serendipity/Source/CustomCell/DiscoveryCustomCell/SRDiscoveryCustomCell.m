

#import "SRDiscoveryCustomCell.h"

@implementation SRDiscoveryCustomCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

}


- (void)layoutSubviews {
    self.communityBtn.layer.cornerRadius = 20.0;
    self.GroupImage.layer.cornerRadius = 20.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
