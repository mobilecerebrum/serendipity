#import <UIKit/UIKit.h>

@interface SRPrivacySettingTableViewCell : UITableViewCell

// Properties
@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblOption;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;

@end
