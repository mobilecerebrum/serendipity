
#import <UIKit/UIKit.h>

@interface SREditFamilyCell : UITableViewCell
{
   //Instance Variables
    
}

//Properties
@property (weak, nonatomic) IBOutlet UIImageView *imgCellBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgFamilyTag;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgDirectionImage;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceLabel;
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UIImageView *notificationBGImg;
@property (weak, nonatomic) IBOutlet UITextField *txtRelationTextField;
@property (weak, nonatomic) IBOutlet UITextField *txtAddNewConnection;
@property (weak, nonatomic) IBOutlet UITextField *txtHome;
@property (weak, nonatomic) IBOutlet UITextField *txtWork;
@property (weak, nonatomic) IBOutlet UITextField *txtGame;
@property (weak, nonatomic) IBOutlet UITextField *txtHomeAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtWorkAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtGameAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnHomeClose;
@property (weak, nonatomic) IBOutlet UIButton *btnWorkClose;
@property (weak, nonatomic) IBOutlet UIButton *btnGameClose;

@property (weak, nonatomic) IBOutlet UIView *viewAddNewConnection;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewNewConnection;


@end
