//
//  SRConnectionTrackingCell.h
//  Serendipity
//
//  Created by Leo on 01/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeableTableViewCell.h"
#import "SWTableViewCell.h"

@interface SRConnectionTrackingCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCellBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgFamilyTag;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblOnline;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileNameLabel;
/*
 Commented by neha
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsLabel,*lblfamily;
 */
@property (weak, nonatomic) IBOutlet UILabel *lblfamily;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationLabel,*lblLocation;
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UIImageView *notificationBGImg;
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *lblPendingRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIImageView *imgBlackOverlay;
@property (weak, nonatomic) IBOutlet UIView *locationView;

@end
