//
//  SRConnectionTrackingCell.m
//  Serendipity
//
//  Created by Leo on 01/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRConnectionTrackingCell.h"

@implementation SRConnectionTrackingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _btnClose.layer.cornerRadius = _btnClose.frame.size.width/2;
    _btnClose.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
