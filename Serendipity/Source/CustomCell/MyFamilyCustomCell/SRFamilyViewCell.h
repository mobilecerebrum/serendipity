#import <UIKit/UIKit.h>

@interface SRFamilyViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCellBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgFamilyTag;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (weak, nonatomic) IBOutlet UIButton *imgDirectionBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblOnline;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceLabel;
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UIImageView *notificationBGImg;
@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end
