//
//  SRFamilyViewCell.m
//  Serendipity
//
//  Created by Sunil Dhokare on 22/05/1937 SAKA.
//  Copyright (c) 1937 SAKA Pragati Dubey. All rights reserved.
//

#import "SRFamilyViewCell.h"

@implementation SRFamilyViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
