//
//  SRNotifyMeTableCell.h
//  Serendipity
//
//  Created by Leo on 02/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRNotifyMeTableCell : UITableViewCell
@property (strong,nonatomic) IBOutlet UIImageView *pinImage;
@property (strong,nonatomic) IBOutlet UIButton *checkBtn;
@property (strong,nonatomic) IBOutlet UILabel *userName;
@property (strong,nonatomic) IBOutlet UILabel *placeAddr;
@property (strong,nonatomic) IBOutlet UILabel *lblNotifyOff;
@property (strong,nonatomic) IBOutlet UILabel *lblNotifyOneTime;
@property (strong,nonatomic) IBOutlet UILabel *lblNotifyAlways;
@property (strong,nonatomic) IBOutlet UIButton *btnNotifyOff;
@property (strong,nonatomic) IBOutlet UIButton *btnNotifyOneTime;
@property (strong,nonatomic) IBOutlet UIButton *btnNotifyAlways;

@end
