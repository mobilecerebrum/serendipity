//
//  SRNotifyMeTableCell.m
//  Serendipity
//
//  Created by Leo on 02/09/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRNotifyMeTableCell.h"

@implementation SRNotifyMeTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
