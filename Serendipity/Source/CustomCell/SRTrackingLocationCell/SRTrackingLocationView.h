//
//  SRTrackingLocationView.h
//  Serendipity
//
//  Created by Jonish Sangwan on 28/09/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//
//jonish CR
#import <UIKit/UIKit.h>
#import "SRSwitch.h"

@interface SRTrackingLocationView : UIView

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *locationDetail;
@property (weak, nonatomic) IBOutlet SRSwitch *switchBtn;

@end

