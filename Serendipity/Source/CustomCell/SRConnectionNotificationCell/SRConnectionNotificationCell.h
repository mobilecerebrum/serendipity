
#import <UIKit/UIKit.h>

@interface SRConnectionNotificationCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblNotification;
@property (weak, nonatomic) IBOutlet UIButton *btnYesNotification;
@property (weak, nonatomic) IBOutlet UIButton *btnNoNotification;


@end
