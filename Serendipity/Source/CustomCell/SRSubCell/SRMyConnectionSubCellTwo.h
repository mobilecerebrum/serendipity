//
//  SRMyConnectionSubCellTwo.h
//  Serendipity
//
//  Created by Jonish Sangwan on 05/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//
//jonish CR
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRMyConnectionSubCellTwo : UIView
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UIButton *turnOnNotificationBtn;
@property (weak, nonatomic) IBOutlet UIButton *arrivalDepartureRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *oneTimeOnlyRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *arrivalRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *departureRadioBtn;
@property (weak, nonatomic) IBOutlet UILabel *cellDynmicTitleLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *offLblHeight;

@end

NS_ASSUME_NONNULL_END
