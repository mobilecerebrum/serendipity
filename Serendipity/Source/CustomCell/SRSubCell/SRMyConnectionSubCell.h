//
//  SRMyConnectionSubCell.h
//  Serendipity
//
//  Created by Jonish Sangwan on 01/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//
//jonish CR
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRMyConnectionSubCell : UIView
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UILabel *trackingTypeLbl;
@property (weak, nonatomic) IBOutlet UIButton *indexOfLocPin;

@end

NS_ASSUME_NONNULL_END
