//
//  SRTrackBtnCellTableViewCell.h
//  Serendipity
//
//  Created by Jonish Sangwan on 28/10/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SRTrackBtnCellTableViewCell : UIView
//jonish history
@property (weak, nonatomic) IBOutlet UIButton *trackBtnOutlet;

@end

NS_ASSUME_NONNULL_END
