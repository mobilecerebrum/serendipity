//
//  SRBatterySettingsTableViewCell.m
//  Serendipity
//
//  Created by Mcuser on 12/21/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import "SRBatterySettingsTableViewCell.h"

@implementation SRBatterySettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    realtimeLbl.text  = @"Real Time \n(Recommended)";
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setupSliderUI];
    int sliderValue = 3;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"kTrackingSettings"] != nil) {
        sliderValue = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"kTrackingSettings"];
    } else {
        sliderValue = [self getBatterySettings:(APP_DELEGATE).server.loggedInUserInfo];
    }
    slider.value = sliderValue;
    [self updateRadioButtons:sliderValue];
    [self updateHint:sliderValue];
    [self updateLabelsForValue:sliderValue];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self updateLabelsPosition];
    });
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTouchUp:)];
    [slider addGestureRecognizer:tap];
    
}

-(int)getBatterySettings:(NSDictionary*)userInfo {
    NSString *batteryUsage = [userInfo[kKeySetting][kkeyBatteryUsage] lowercaseString];
    int batterySettings = 3;
    
    if ([batteryUsage isEqualToString:@"realtime"] || [batteryUsage isEqualToString:@"recommended"]) {
        batterySettings = 3;
    }else if ([batteryUsage isEqualToString:@"very low"]) {
        batterySettings = 0;
    }else if ([batteryUsage isEqualToString:@"low"]) {
        batterySettings = 1;
    }else if ([batteryUsage containsString:@"medium"]) {
        batterySettings = 2;
    }else if ([batteryUsage containsString:@"ground travel"] || [batteryUsage containsString:@"ground mode"]) {
         batterySettings = 4;
    }else if ([batteryUsage containsString:@"air travel"] || [batteryUsage containsString:@"air mode"]) {
         batterySettings = 5;
    }
    return batterySettings;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)updateUI {
    [self updateLabelsPosition];
}

-(void)updateTagsForLabels {
    lowLbl.tag = 1;
    mediumLbl.tag = 2;
    optimalLbl.tag = 3;
    realtimeLbl.tag = 4;
}

- (void)setupSliderUI {
    slider.minimumTrackTintColor = [UIColor blackColor];
    slider.maximumTrackTintColor = [UIColor blackColor];
    slider.backgroundColor = [UIColor clearColor];
    [slider setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
}

// positioning for slider labels
-(void)updateLabelsPosition {
    int sliderStepSize = slider.frame.size.width / 3;
    float labelsY = slider.frame.origin.y + slider.frame.size.height + 10;
    CGFloat sliderXPosition = slider.frame.origin.x;
    
    for (int i = 0; i <= (int)slider.maximumValue; i++) {
        UILabel *lbl = (UILabel*)[self.contentView viewWithTag:i + 1];
        if (lbl) {
            CGRect lblFrame = lbl.frame;
            lblFrame.size.width = [self widthOfString:lbl.text withFont:lbl.font];
            lbl.frame = lblFrame;
            int labelOffset = 0;
            if (i == 0) {
                labelOffset = 10;
            } else if (i == (int)slider.maximumValue) {
                labelOffset = -10;
            }
            lbl.center = CGPointMake(sliderXPosition + (i * sliderStepSize) + labelOffset, labelsY);
            lbl.backgroundColor = [UIColor clearColor];
        }
    }
}

- (IBAction)onSliderChanger:(id)sender {
    [airCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
    [groundCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
    UISlider *slider = (UISlider*)sender;
    [slider setValue:round(slider.value) animated:NO];
    [self updateHint:(int)slider.value];
    [self updateLabelsForValue:(int)slider.value];
}

-(void)sliderTouchUp:(UITapGestureRecognizer*)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSLog(@"%s Slider touch ended", __PRETTY_FUNCTION__);
    }
}

- (IBAction)onTouchUp:(id)sender {
    UISlider *slider = (UISlider*)sender;
    [self updateRadioButtons:0];
    [self updateTrackerSettingsForValue:(int)slider.value];
    [[NSUserDefaults standardUserDefaults] setInteger:(int)slider.value forKey:@"kTrackingSettings"];
    [[NSUserDefaults standardUserDefaults] setInteger:(int)slider.value forKey:@"kTrackingSlider"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self updateServerSettings:(int)slider.value];
}
    
- (IBAction)onSetAirMode:(id)sender {
    [self updateRadioButtons:5];
    [self updateTrackerSettingsForValue:5];
    [self updateServerSettings:5];
    [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"kTrackingSettings"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
    
- (IBAction)onSetGroundMode:(id)sender {
    [self updateRadioButtons:4];
    [self updateTrackerSettingsForValue:4];
    [self updateServerSettings:4];
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"kTrackingSettings"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
    
-(void)updateRadioButtons:(int)value {
    // if we are going to use travel mode then update UI
    if (value == 4 || value == 5) {
        [self updateTravelMode:YES];
    }
    [self updateUIForRadio:value];
}

-(void)updateUIForRadio:(int)value {
    if (value == 4) { // ground
        [airCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
        [groundCheckbox setImage:[UIImage imageNamed:@"radio_checked.png"] forState:(UIControlStateNormal)];
    } else if (value == 5){ // air
        [airCheckbox setImage:[UIImage imageNamed:@"radio_checked.png"] forState:(UIControlStateNormal)];
        [groundCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
    } else {
        [airCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
        [groundCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
    }
}

- (void)updateHint:(int)value {
    switch (value) {
        case 0: // low
            userHintLbl.text = NSLocalizedString(@"lbl.bhint_low", @"");
            break;
        case 1: // medium
            userHintLbl.text = NSLocalizedString(@"lbl.bhint_medium", @"");
            break;
        case 2: // Recommended (Optimal)
            userHintLbl.text = NSLocalizedString(@"lbl.bhint_optimal", @"");
            break;
        case 3: // Realtime
            userHintLbl.text = NSLocalizedString(@"lbl.bhint_realtime", @"");
            break;
        case 4: // Ground Travel
            userHintLbl.text = @"";//NSLocalizedString(@"lbl.bhint_ground", @"");
            break;
        case 5: // Air Travel
            userHintLbl.text = @"";//NSLocalizedString(@"lbl.bhint_air", @"");
            break;
        default:
            break;
    }
}

- (void)updateTrackerSettingsForValue:(int)value {
    switch (value) {
        case 0: // low
            [[SLocationService sharedInstance] setLowSettings];
            break;
        case 1: // Medium
            [[SLocationService sharedInstance] setMediumSettings];
            break;
        case 2: // Recommended (Optimal)
            [[SLocationService sharedInstance] setOptimalSettings];
            break;
        case 3: // Realtime
            [[SLocationService sharedInstance] setRealtimeSettings];
            break;
        case 4: // Ground Travel
            [[SLocationService sharedInstance] setGroundSettings];
            break;
        case 5: // Air Travel
            [[SLocationService sharedInstance] setAirSettings];
            break;
        default:
            break;
    }
}

- (IBAction)onTravelMode:(id)sender {
    UISwitch *switcher = (UISwitch*)sender;
    [self updateTravelMode:switcher.isOn];
    if (switcher.isOn) {
        [self updateTrackerSettingsForValue:4];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"kTrackingSettings"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self updateUIForRadio:4];
        [self updateServerSettings:4];
    } else {
        int sliderValue = 2;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"kTrackingSlider"] != nil) {
            sliderValue = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"kTrackingSlider"];
            [[NSUserDefaults standardUserDefaults] setInteger:(int)slider.value forKey:@"kTrackingSettings"];
            [[NSUserDefaults standardUserDefaults] setInteger:(int)slider.value forKey:@"kTrackingSlider"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"kTrackingSettings"] != nil) {
            [[NSUserDefaults standardUserDefaults] setInteger:sliderValue forKey:@"kTrackingSettings"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        slider.value = sliderValue;
        [self updateRadioButtons:sliderValue];
        [self updateHint:sliderValue];
        [self updateLabelsForValue:sliderValue];
        [self updateTrackerSettingsForValue:sliderValue];
        [self updateServerSettings:sliderValue];
        [airCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
        [groundCheckbox setImage:[UIImage imageNamed:@"radio_unchecked.png"] forState:(UIControlStateNormal)];
    }
}

-(void)updateTravelMode:(BOOL)isTravelMode {
    airCheckbox.enabled = isTravelMode;
    groundCheckbox.enabled = isTravelMode;
    slider.enabled = !isTravelMode;
    [modeSwitch setOn:isTravelMode];
    // userHintLbl.textColor = switcher.isOn ? [UIColor grayColor] : [UIColor whiteColor];
    if (isTravelMode) {
        [self updateLabelsAlpha:0.5];
        userHintLbl.textColor = [UIColor blackColor];
    } else {
        [self updateLabelsAlpha:1];
        userHintLbl.textColor = [UIColor whiteColor];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"kTrackingSlider"] != nil) {
            int sliderValue = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"kTrackingSlider"];
            [self updateHint:sliderValue];
            [self updateLabelsForValue:sliderValue];
            slider.value = sliderValue;
        }
    }
}

-(void)updateServerSettings:(int)value {
    switch (value) {
        case 0: // low
            [[SRAPIManager sharedInstance] saveBatterySettings:NSLocalizedString(@"lbl.very_low.txt", @"")];
            break;
        case 1: // medium
            [[SRAPIManager sharedInstance] saveBatterySettings:NSLocalizedString(@"lbl.low.txt", @"")];
            break;
        case 2: // Recommended (Optimal)
            [[SRAPIManager sharedInstance] saveBatterySettings:NSLocalizedString(@"lbl.medium.txt", @"")];
            break;
        case 3: // Realtime
            [[SRAPIManager sharedInstance] saveBatterySettings:@"realtime"];
            break;
        case 4: // Ground Travel
            [[SRAPIManager sharedInstance] saveBatterySettings:@"Ground mode"];
            break;
        case 5: // Air Travel
            [[SRAPIManager sharedInstance] saveBatterySettings:@"Air mode"];
            break;
        default:
            break;
    }
}

// Swap color for slider step values
- (void)updateLabelsForValue:(int)value {
    NSArray *subViews = self.contentView.subviews;
    for (id v in subViews) {
        if ([v isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel*)v;
            if (lbl.tag == 100) {
                continue;
            } else {
                if (lbl.tag - 1 != value) {
                    lbl.textColor = [UIColor blackColor];
                } else {
                    lbl.textColor = [UIColor whiteColor];
                }
            }
        }
    }
}

-(void)updateLabelsAlpha:(float)alpha {
    lowLbl.alpha = mediumLbl.alpha = optimalLbl.alpha = realtimeLbl.alpha = alpha;
    userHintLbl.alpha = alpha;
}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    return [string sizeWithAttributes:@{NSFontAttributeName : font}].width;
}

@end
