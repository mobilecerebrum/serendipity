//
//  SRBatterySettingsTableViewCell.h
//  Serendipity
//
//  Created by Mcuser on 12/21/18.
//  Copyright © 2018 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRHomeMenuCustomCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRBatterySettingsTableViewCell : SRHomeMenuCustomCell {
    IBOutlet UILabel *userHintLbl;
    IBOutlet UILabel *realtimeLbl;
    IBOutlet UILabel *optimalLbl;
    IBOutlet UILabel *mediumLbl;
    IBOutlet UILabel *lowLbl;
    IBOutlet UILabel *groundTravelLbl;
    IBOutlet UILabel *airTravelLbl;
    IBOutlet UISlider *slider;
    IBOutlet UIButton *airCheckbox;
    IBOutlet UIButton *groundCheckbox;
    IBOutlet UISwitch *modeSwitch;
    
}

-(void)updateUI;

@end

NS_ASSUME_NONNULL_END
