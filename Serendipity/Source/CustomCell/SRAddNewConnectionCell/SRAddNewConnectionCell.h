#import <UIKit/UIKit.h>

@interface SRAddNewConnectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupation;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UIImageView *datingImage;
@property (weak, nonatomic) IBOutlet UILabel *datingLbl;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblFamily;
@property (weak, nonatomic) IBOutlet UILabel *lblReqSent;

@property (strong, nonatomic) UIImage *storedImg;

@end
