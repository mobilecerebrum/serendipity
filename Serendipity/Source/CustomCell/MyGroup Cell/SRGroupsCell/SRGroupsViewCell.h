#import <UIKit/UIKit.h>

@interface SRGroupsViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgBGCell;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblMembers;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAccess;
@property (weak, nonatomic) IBOutlet UILabel *lblCompassDirection;
@property (weak, nonatomic) IBOutlet UIImageView *imgGroups;
@property (weak, nonatomic) IBOutlet UIButton *btnDirection;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnDecline;
@property (weak, nonatomic) IBOutlet UIImageView *declineGroupShadow;

@end
