#import "SRGroupsViewCell.h"

@implementation SRGroupsViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self registerNotification];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// Compass rotation
- (void)registerNotification {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(updateHeadingAngle:)
                          name:kKeyNotificationUpdateHeadingValue
                        object:nil];
}

- (void)updateHeadingAngle:(NSNotification *)inNotify {
    float heading = [[inNotify object] floatValue];
    float headingAngle = -(heading * M_PI / 180); //assuming needle points to top of iphone. convert to radians
    [self rotationCompass:headingAngle];
}

- (void)rotationCompass:(CGFloat)headingAngle {
    // rotate the compass to heading degree
    self.btnDirection.transform = CGAffineTransformMakeRotation(headingAngle);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
