#import <UIKit/UIKit.h>

@interface SRMemberViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgCellBg;
@property (weak, nonatomic) IBOutlet UILabel *lblOnline;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblProfession;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgDating;
@property (weak, nonatomic) IBOutlet UILabel *lblDating;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;

@end
