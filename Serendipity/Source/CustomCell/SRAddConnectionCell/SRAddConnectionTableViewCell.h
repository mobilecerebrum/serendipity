//
//  AddConnectionTableViewCell.h
//  Serendipity
//
//  Created by Leo on 11/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "MGSwipeTableCell.h"

@interface SRAddConnectionTableViewCell : MGSwipeTableCell
@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) NSDictionary *user;
@property (weak, nonatomic) IBOutlet UIImageView *SDIconImage;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *invitelbl;

@property (strong, nonatomic) SRServerConnection *server;
@property (weak, nonatomic) IBOutlet UIImageView *imgContactType;

-(void)configureForRightSwipe;

@end
