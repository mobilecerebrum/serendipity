//
//  AddConnectionTableViewCell.m
//  Serendipity
//
//  Created by Leo on 11/04/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import "SRAddConnectionTableViewCell.h"
#import "SwipeableTableViewCell.h"
#import "SwipeCellButton.h"

@implementation SRAddConnectionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureForRightSwipe {

    //configure left buttons
    SwipeCellButton *blockBtn = [[[NSBundle mainBundle] loadNibNamed:@"SwipeCellButton" owner:self options:nil] firstObject];
    blockBtn.mgButton.buttonWidth = 50;
    blockBtn.titleLabel.text = [NSString stringWithFormat:@"Block %@", self.user[kKeyFirstName]];
    [blockBtn.mgButton setImage:[UIImage imageNamed:@"ic_cancel"] forState:(UIControlStateNormal)];
    blockBtn.backgroundColor = [UIColor orangeColor];
    blockBtn.titleLabel.textColor = [UIColor whiteColor];
    
    SwipeCellButton *deleteButton = [[[NSBundle mainBundle] loadNibNamed:@"SwipeCellButton" owner:self options:nil] firstObject];
    [deleteButton.mgButton setImage:[UIImage imageNamed:@"close-icon.png"] forState:(UIControlStateNormal)];
    deleteButton.mgButton.buttonWidth = 50;
    deleteButton.mgButton.tintColor = [UIColor clearColor];
    deleteButton.backgroundColor = [UIColor redColor];
    deleteButton.titleLabel.text = [NSString stringWithFormat:@"Delete %@", self.user[kKeyFirstName]];
    deleteButton.titleLabel.textColor = [UIColor whiteColor];
     [blockBtn.mgButton addTarget:self action:@selector(blockUserAction:) forControlEvents:UIControlEventTouchUpInside];
    [deleteButton.mgButton addTarget:self action:@selector(deleteUserAction:) forControlEvents:UIControlEventTouchUpInside];
       self.leftButtons = @[deleteButton, blockBtn];
    
    self.leftSwipeSettings.transition = MGSwipeTransitionStatic;
    self.leftSwipeSettings.threshold = 0.2;

}

- (UIView *)viewWithImageName:(NSString *)imageName {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, self.frame.size.height)];
    view.backgroundColor = [UIColor redColor];
    return view;
}

- (void)deleteUserAction:(UIButton *)sender {

    NSDictionary *connectionDict = self.user[kKeyConnection];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = self.user[@"contact_user_id"];//connectionParams[kKeyConnectionID];
    params[kKeyType] = @"delete";
    params[kKeyIsContact] = @1;
    params[kKeyStatus] = @1;
    NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
     [APP_DELEGATE showActivityIndicator];
     [self.server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];

}
- (void)blockUserAction:(UIButton *)sender {

    NSInteger i = [sender tag];

    // Call notification API
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[kKeyUserID] = self.user[@"contact_user_id"];//connectionParams[kKeyConnectionID];
    params[kKeyType] = @"block";
    params[kKeyIsContact] = @1;
    params[kKeyStatus] = @1;
    NSString *strUrl = kKeyClassBlockdeleteConnectionuser;
    [APP_DELEGATE showActivityIndicator];
    [self.server makeAsychronousRequest:strUrl inParams:params isIndicatorRequired:YES inMethodType:kPOST];
}
@end
