//
//  SRAPIManager+Notifications.m
//  Serendipity
//
//  Created by Mcuser on 2/14/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager+Notifications.h"

@implementation SRAPIManager (Notifications)

-(void)getNotificationsForUser:(NSString *)userId completion:(APIManagerBlock)completion {
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%@", kSerendipityServer, kKeyClassGetNotification, userId];
    [self.managerJson GET:strUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

-(void)getNotificationByType:(int)type completion:(APIManagerBlock)completion {
    NSString *strUrl = [NSString stringWithFormat:@"%@%@%d", kSerendipityServer, kKeyClassUpdateNotification, type];
    [self.managerJson GET:strUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

@end
