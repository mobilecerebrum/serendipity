//
//  SRAPIManager+Preloading.m
//  Serendipity
//
//  Created by Mcuser on 3/21/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager+Preloading.h"

@implementation SRAPIManager (Preloading)

-(void)getCountriesWithCompletion:(APIManagerBlock)completion {
    NSString *url = [NSString stringWithFormat:@"%@/%@", kSerendipityServer, kKeyClassGetCountryList];
    [self.managerJson GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

@end
