//
//  SRAPIManager.m
//  Serendipity
//
//  Created by Mcuser on 2/12/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager.h"

@interface SRAPIManager()
    @property NSString *token;
@end

@implementation SRAPIManager

+ (instancetype)sharedInstance {
    static SRAPIManager *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[self class] new];
    });
    return service;
}

- (void)updateToken:(NSString *)newToken {
    if ([newToken containsString:@"Bearer "]) {
        newToken = [newToken stringByReplacingOccurrencesOfString:@"Bearer " withString:@""];
    }
    self.token = newToken;
    [self updateTokenHeader];
}

-(NSString *)getToken {
    return _token;
}

- (id)init {
    if(self = [super init]) {
        [self updateTokenHeader];
    }
    return self;
}
   
- (void) updateTokenHeader{
    [self setTokenToManager:self.managerJson];
    [self setTokenToManager:self.managerData];
}

- (void) setTokenToManager:(AFHTTPSessionManager*)manager {
    if (self.token) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", self.token]
                         forHTTPHeaderField:@"Authorization"];
    } else {
        [manager.requestSerializer setValue:@"Bearer" forHTTPHeaderField:@"Authorization"];
    }
}

- (void) setTokenAuthorization:(AFHTTPSessionManager*)manager {
    if (self.token) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", self.token]
                         forHTTPHeaderField:@"Authorization"];
    } else {
        [manager.requestSerializer setValue:@"Bearer" forHTTPHeaderField:@"Authorization"];
    }
}

- (AFHTTPSessionManager*)managerJson {
    if (!_managerJson) {
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        responseSerializer.removesKeysWithNullValues = YES;
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //_managerJson = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:ApplicationAPI]];
        _managerJson = [[AFHTTPSessionManager alloc] init];
        _managerJson.requestSerializer = requestSerializer;
        _managerJson.responseSerializer = responseSerializer;
        _managerJson.requestSerializer.timeoutInterval = 10.0f;
        [self setTokenToManager:_managerJson];
    }
    return _managerJson;
}

- (AFHTTPSessionManager*)managerAuthorization {
    if (!_managerAuthorization) {
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        responseSerializer.removesKeysWithNullValues = YES;
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        //_managerJson = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:ApplicationAPI]];
        _managerAuthorization = [[AFHTTPSessionManager alloc] init];
        _managerAuthorization.requestSerializer = requestSerializer;
        _managerAuthorization.responseSerializer = responseSerializer;
        _managerAuthorization.requestSerializer.timeoutInterval = 10.0f;
    }
    return _managerAuthorization;
}



- (AFHTTPSessionManager*)managerData {
    if (!_managerData) {
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        responseSerializer.removesKeysWithNullValues = YES;
        responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"image/png", @"image/jpeg", @"image/jpg", @"text/html"]];
        
        //_managerData = [[AFHTTPSessionManager alloc]initWithBaseURL:[NSURL URLWithString:ApplicationAPI]];
        _managerData = [[AFHTTPSessionManager alloc] init];
        _managerData.requestSerializer = [AFHTTPRequestSerializer serializer];
        _managerData.responseSerializer = responseSerializer;
        _managerJson.requestSerializer.timeoutInterval = 10.0f;
        [self setTokenToManager:_managerData];
    }
    
    return _managerData;
}

- (void) succsessfulRequest:(id)result completion:(APIManagerBlock)completion{
    if (completion) {
        completion(result, nil);
    }
}

- (void)failedRequest:(id)result error:(NSError*)error completion:(APIManagerBlock)completion{
    
    if ([result isKindOfClass:[NSData class]]) {
        NSString *str = [NSString stringWithUTF8String:((NSData*)result).bytes];
        
        if (str.length) {
            if (completion) {
                completion(str, error);
            }
        }
    }
    
    if (completion) {
        completion(result, error);
    }
}


@end
