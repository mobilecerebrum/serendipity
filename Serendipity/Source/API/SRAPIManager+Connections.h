//
//  Connections.h
//  Serendipity
//
//  Created by Mcuser on 3/13/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRAPIManager (Connections)

-(void)getConnectionsWithStatus:(int)status completion:(APIManagerBlock)completion;
-(void)getConnectionsWithStatuses:(NSString*)statuses completion:(APIManagerBlock)completion;
-(void)getUserWithId:(NSString*)userId completion:(APIManagerBlock)completion;
@end

NS_ASSUME_NONNULL_END
