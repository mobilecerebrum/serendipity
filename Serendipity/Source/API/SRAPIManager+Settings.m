//
//  SRAPIManager+Settings.m
//  Serendipity
//
//  Created by Mcuser on 3/13/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager+Settings.h"

@implementation SRAPIManager (Settings)

-(void)saveBatterySettings:(NSString*)settings {

    NSString *endPoint = [NSString stringWithFormat:@"%@setting", kSerendipityServer];
    
    NSDictionary *params = @{
                             @"field_name" : @"battery_usage",
                             @"field_value" : settings
                             };
  //  NSLog(@"Battery Setting Update Param %@",params);
    [self.managerJson POST:endPoint parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
      //  NSLog(@"Battery settings saved");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       // NSLog(@"Battery settings not saved");
    }];
}

-(void)updateSettings:(NSString*)key withValue:(NSString*)value {
    NSString *endPoint = [NSString stringWithFormat:@"%@setting", kSerendipityServer];
    
    NSDictionary *params = @{
                             @"field_name" : key,
                             @"field_value" : value
                             };
    
    [self.managerJson POST:endPoint parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
      //  NSLog(@"User's settings saved");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
      //  NSLog(@"User's settings not saved");
    }];
}

@end
