//
//  SRAPIManager+Notifications.h
//  Serendipity
//
//  Created by Mcuser on 2/14/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRAPIManager (Notifications)

-(void)getNotificationsForUser:(NSString*)userId completion:(APIManagerBlock)completion;
-(void)getNotificationByType:(int)type completion:(APIManagerBlock)completion;
@end

NS_ASSUME_NONNULL_END
