//
//  SRAPIManager+Chat.h
//  Serendipity
//
//  Created by Mcuser on 2/12/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRAPIManager (Chat)

#define NUMBER_OF_RECORDS @"10"

-(void)getMessages:(NSString *)senderId receiverId:(NSString *)receiverId lastMessageId:(NSString * __nullable)lastMessageId completion:(APIManagerBlock)completion;
-(void)sendMessageSender:(NSString *)senderId receiverId:(NSString *)receiverId message:(NSString *)message forGorup:(BOOL)isGroup completion:(APIManagerBlock)completion;
-(void) getChatsHistoryWithCompletion:(APIManagerBlock)completion;
-(void)sendImage:(UIImage *)image forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion;
-(void)sendImage:(UIImage *)image withText:(NSString*)str forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion;
-(void)sendVideo:(NSURL *)videoPath withText:(NSString*)str forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion;
-(void)sendVideo:(NSURL *)videoPath forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion;
-(void)getMessageForGroup:(NSString*)groupId andSender:(NSString *)senderId lastMessageId:(NSString * __nullable)lastMessageId completion:(APIManagerBlock)completion;
@end

NS_ASSUME_NONNULL_END
