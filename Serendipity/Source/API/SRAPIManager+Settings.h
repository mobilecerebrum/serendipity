//
//  SRAPIManager+Settings.h
//  Serendipity
//
//  Created by Mcuser on 3/13/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRAPIManager (Settings)

-(void)saveBatterySettings:(NSString*)settings;
-(void)updateSettings:(NSString*)key withValue:(NSString*)value;
@end

NS_ASSUME_NONNULL_END
