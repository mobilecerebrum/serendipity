//
//  SRAPIManager.h
//  Serendipity
//
//  Created by Mcuser on 2/12/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^APIManagerBlock)(id __nullable result, NSError* __nullable error );
typedef void(^APIManagerProgressBlock)(CGFloat progress);

@interface SRAPIManager : NSObject

/**
 @return singleton instance of SRAPIManager
 */
+ (instancetype)sharedInstance;

/**
 @return manager for sending/recieving throught json format
 */
@property (nonatomic, strong) AFHTTPSessionManager *managerJson;


@property (nonatomic, strong) AFHTTPSessionManager *managerAuthorization;

/**
 @return manager for sending/recieving throught data format(images,data,http params,etc)
 */
@property (nonatomic, strong) AFHTTPSessionManager *managerData;

/**
 @return manager for subscription service
 */
@property (nonatomic, strong) AFHTTPSessionManager *managerSubscriptions;

/**
 Should be called from instance
 */
- (void)updateToken:(NSString*)newToken;

- (void) succsessfulRequest:(id)result completion:(APIManagerBlock)completion;
- (void) failedRequest:(id)result error:(NSError*)error completion:(APIManagerBlock)completion;
- (void) updateTokenHeader;
- (NSString*) getToken;

@end

NS_ASSUME_NONNULL_END

#import "SRAPIManager+Chat.h"
#import "SRAPIManager+Notifications.h"
#import "SRAPIManager+Connections.h"
#import "SRAPIManager+Settings.h"
#import "SRAPIManager+Preloading.h"
