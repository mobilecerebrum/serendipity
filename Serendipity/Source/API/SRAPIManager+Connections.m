//
//  Connections.m
//  Serendipity
//
//  Created by Mcuser on 3/13/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager+Connections.h"

@implementation SRAPIManager (Connections)

-(void)getConnectionsWithStatus:(int)status completion:(APIManagerBlock)completion {
    NSString *connectionsUrl = [NSString stringWithFormat:@"%@%@", kSerendipityServer, kKeyClassAddConnection];
    
    NSDictionary *params = @{@"connection_status" : [NSNumber numberWithInt:status]};
    
    [self.managerJson GET:connectionsUrl parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

-(void)getConnectionsWithStatuses:(NSString*)statuses completion:(APIManagerBlock)completion {
    NSString *connectionsUrl = [NSString stringWithFormat:@"%@%@", kSerendipityServer, kKeyClassAddConnection];
    
    NSDictionary *params = @{@"connection_status" : statuses};
    
    [self.managerJson GET:connectionsUrl parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

-(void)getUserWithId:(NSString*)userId completion:(APIManagerBlock)completion {
    NSString *connectionsUrl = [NSString stringWithFormat:@"%@%@/%@", kSerendipityServer, kKeyClassUpdateUserPhoneNo, userId];
    
    [self.managerJson GET:connectionsUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

@end
