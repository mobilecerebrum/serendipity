//
//  SRAPIManager+Chat.m
//  Serendipity
//
//  Created by Mcuser on 2/12/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager+Chat.h"

@implementation SRAPIManager (Chat)

-(void)sendVideo:(NSURL *)videoPath withText:(NSString*)str forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion {
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages", kSerendipityChatServer];
    NSData *videoData = [NSData dataWithContentsOfURL:videoPath];
    NSDictionary *params = @{
                             @"user_id" : receiverId,
                             @"type" : @"video",
                             @"text" : str
                             };
    if (isGroup) {
        endPoint = [NSString stringWithFormat:@"%@/api/group/%@/messages", kSerendipityChatServer, receiverId];
        params = @{
                   @"group_id" : receiverId,
                   @"type" : @"video",
                   @"text" : str
                   };
    }
    NSString *fileName = [[videoPath path] lastPathComponent];
    [self.managerData POST:endPoint parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:videoData name:@"video" fileName:fileName mimeType:@"video/quicktime"];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

-(void)sendVideo:(NSURL *)videoPath forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion {
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages", kSerendipityChatServer];
    NSData *videoData = [NSData dataWithContentsOfURL:videoPath];
    NSDictionary *params = @{
                             @"user_id" : receiverId,
                             @"type" : @"video"
                             };
    if (isGroup) {
        endPoint = [NSString stringWithFormat:@"%@/api/group/%@/messages", kSerendipityChatServer, receiverId];
        params = @{
                   @"group_id" : receiverId,
                   @"type" : @"video"
                   };
    }
    NSString *fileName = [[videoPath path] lastPathComponent];
    [self.managerData POST:endPoint parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:videoData name:@"video" fileName:fileName mimeType:@"video/quicktime"];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

-(void)sendImage:(UIImage *)image forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion {
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages", kSerendipityChatServer];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSDictionary *params = @{
                             @"user_id" : receiverId,
                             @"type" : @"image"
                             };
    if (isGroup) {
        endPoint = [NSString stringWithFormat:@"%@/api/group/%@/messages", kSerendipityChatServer, receiverId];
        params = @{
                   @"group_id" : receiverId,
                   @"type" : @"image"
                   };
    }
    [self.managerData POST:endPoint parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
//    [self.managerData POST:endPoint parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        completion(responseObject, nil);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        completion(nil, error);
//    }];
    
}

-(void)sendImage:(UIImage *)image withText:(NSString*)str forSender:(NSString *)senderId receiver:(NSString *)receiverId forGroup:(BOOL)isGroup completion:(APIManagerBlock)completion {
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages", kSerendipityChatServer];
        NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
        NSDictionary *params = @{
                                 @"user_id" : receiverId,
                                 @"type" : @"image",
                                 @"text" : str
                                 };
        if (isGroup) {
            endPoint = [NSString stringWithFormat:@"%@/api/group/%@/messages", kSerendipityChatServer, receiverId];
            params = @{
                       @"group_id" : receiverId,
                       @"type" : @"image",
                       @"text" : str
                       };
        }
        [self.managerData POST:endPoint parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            completion(responseObject, nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            completion(nil, error);
        }];
    //    [self.managerData POST:endPoint parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    //        completion(responseObject, nil);
    //    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    //        completion(nil, error);
    //    }];
}

-(void)sendMessageSender:(NSString *)senderId receiverId:(NSString *)receiverId message:(NSString *)message forGorup:(BOOL)isGroup completion:(APIManagerBlock)completion {
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages", kSerendipityChatServer];
    
    NSDictionary *params = @{
                             @"user_id" : receiverId,
                             @"sender_id" : senderId,
                             @"type" : @"text",
                             @"text" : message
                             };
    
    if (isGroup) {
        endPoint = [NSString stringWithFormat:@"%@/api/group/%@/messages", kSerendipityChatServer, receiverId];
        params = @{
                   @"group_id" : receiverId,
                   @"sender_id" : senderId,
                   @"type" : @"text",
                   @"text" : message
                   };
    }
    
   [self.managerJson POST:endPoint parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
     //   NSLog(@"Complete");
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
      //  NSLog(@"Failed to send chat message");
        completion(nil, error);
    }];
}

-(void)getChatsHistoryWithCompletion:(APIManagerBlock)completion {
    
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages/history", kSerendipityChatServer];
    [self.managerJson GET:endPoint parameters:nil
                 progress:^(NSProgress * _Nonnull downloadProgress) {
                     
                 } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     completion(responseObject, nil);
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     completion(nil, error);
                 }];
}

-(void)getMessageForGroup:(NSString*)groupId andSender:(NSString *)senderId lastMessageId:(NSString * __nullable)lastMessageId completion:(APIManagerBlock)completion {
    
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/group/%@/messages?group_id=%@", kSerendipityChatServer, groupId, groupId];
    NSMutableDictionary *params = [@{
                             @"sender_id" : senderId,
                             @"token" : [self getToken],
                             @"num_records": NUMBER_OF_RECORDS,
                             @"scrolling": @"0"
                             } mutableCopy];
    
    if (lastMessageId.length > 0) {
        params[@"last_message_id"] = lastMessageId;
    }
    
//    NSLog(@"API End point: %@",endPoint);
//    NSLog(@"Chat API Request: %@",params);
    
    [self.managerJson GET:endPoint parameters:params
                 progress:^(NSProgress * _Nonnull downloadProgress) {
                     
                 } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                   //  NSLog(@"API Response: %@",responseObject);
                     completion(responseObject, nil);
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     completion(nil, error);
                 }];
}

-(void)getMessages:(NSString *)senderId receiverId:(NSString *)receiverId lastMessageId:(NSString * __nullable)lastMessageId completion:(APIManagerBlock)completion {
    //messages?sender_id=1&receiver_id=2&token=token
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:receiverId forKey:@"other_user_id"];
    [params setValue:senderId forKey:@"sender_id"];
    [params setValue:[self getToken] forKey:@"token"];
    [params setValue:NUMBER_OF_RECORDS forKey:@"num_records"];
    [params setValue:@"0" forKey:@"scrolling"];
    
    if (lastMessageId.length > 0) {
        [params setValue:lastMessageId forKey:@"last_message_id"];
    }
    
    NSString *endPoint = [NSString stringWithFormat:@"%@/api/messages", kSerendipityChatServer];
    
    [self.managerJson GET:endPoint parameters:params
                 progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}


-(NSString*)urlEscape:(NSString *)unencodedString {
    NSString *s = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                        (CFStringRef)unencodedString,
                                                                                        NULL,
                                                                                        (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                        kCFStringEncodingUTF8));
    return s; // Due to the 'create rule' we own the above and must autorelease it
}

// Put a query string onto the end of a url
-(NSString*)addQueryStringToUrl:(NSString *)url params:(NSDictionary *)params {
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:url];
    // Convert the params into a query string
    if (params) {
        for(id key in params) {
            NSString *sKey = [key description];
            NSString *sVal = [[params objectForKey:key] description];
            // Do we need to add ?k=v or &k=v ?
            if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
                [urlWithQuerystring appendFormat:@"?%@=%@", [self urlEscape:sKey], [self urlEscape:sVal]];
            } else {
                [urlWithQuerystring appendFormat:@"&%@=%@", [self urlEscape:sKey], [self urlEscape:sVal]];
            }
        }
    }
    return urlWithQuerystring;
}

@end
