//
//  SRAPIManager+Preloading.h
//  Serendipity
//
//  Created by Mcuser on 3/21/19.
//  Copyright © 2019 Serendipity. All rights reserved.
//

#import "SRAPIManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SRAPIManager (Preloading)

-(void)getCountriesWithCompletion:(APIManagerBlock)completion;

@end

NS_ASSUME_NONNULL_END
