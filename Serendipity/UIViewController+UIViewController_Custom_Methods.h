//
//  UIViewController+UIViewController_Custom_Methods.m
//  Serendipity
//
//  Copyright © 2020 Neha Dubey. All rights reserved.


#import <UIKit/UIKit.h>

@interface UIViewController (UIViewController_Custom_Methods)

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message withCompletionBlok:(void (^)(void))block;
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonTitle:(NSString *)buttonTitle cancelButtonTitle:(NSString *)cancelButtonTitle withCompletionBlok:(void (^)(BOOL isCancelAction))block;


@end
