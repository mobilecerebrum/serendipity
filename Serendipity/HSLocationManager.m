//
//  HSLocationManager.m
//  Serendipity
//
//  Created by Hitesh Surani on 04/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import "HSLocationManager.h"

@implementation HSLocationManager

+ (id)sharedManager {
    static HSLocationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void) startLocationManger:(LocationStatusChanged)block{
    self.authBlock = block;
    self.isLocationManagerRunning = YES;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)viewController {
    if ([viewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)viewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([viewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navContObj = (UINavigationController*)viewController;
        return [self topViewControllerWithRootViewController:navContObj.visibleViewController];
    } else if (viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed) {
        UIViewController* presentedViewController = viewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    }
    else {
        for (UIView *view in [viewController.view subviews])
        {
            id subViewController = [view nextResponder];
            if ( subViewController && [subViewController isKindOfClass:[UIViewController class]])
            {
                if ([(UIViewController *)subViewController presentedViewController]  && ![subViewController presentedViewController].isBeingDismissed) {
                    return [self topViewControllerWithRootViewController:[(UIViewController *)subViewController presentedViewController]];
                }
            }
        }
        return viewController;
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
        self.authBlock(status);
        self.authStatus = status;
    
//        switch (status) {
//            case kCLAuthorizationStatusDenied:
//            case kCLAuthorizationStatusRestricted:
//            case kCLAuthorizationStatusNotDetermined:
//            case kCLAuthorizationStatusAuthorizedWhenInUse: {
//                NSLog(@"While in use");
//            } break;
//
//            case kCLAuthorizationStatusAuthorizedAlways: {
//                NSLog(@"Always permission");
//            } break;
//
//            default:
//                //
//                break;
//        }
}

@end
