//
//  HSLocationManager.h
//  Serendipity
//
//  Created by Hitesh Surani on 04/05/20.
//  Copyright © 2020 Serendipity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


typedef void (^LocationStatusChanged)(CLAuthorizationStatus status);

NS_ASSUME_NONNULL_BEGIN

@interface HSLocationManager : NSObject<CLLocationManagerDelegate>
+ (id)sharedManager;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property BOOL isLocationManagerRunning;
@property (copy, nonatomic) LocationStatusChanged authBlock;
@property CLAuthorizationStatus authStatus;
-(void) startLocationManger:(LocationStatusChanged)block;

@end

NS_ASSUME_NONNULL_END
