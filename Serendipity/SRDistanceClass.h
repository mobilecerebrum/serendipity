//
//  SRDistanceClass.h
//  Serendipity
//
//  Created by Sunil Dhokare on 06/10/16.
//  Copyright © 2016 Pragati Dubey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRDistanceClass : NSObject

@property (nonatomic,strong) NSString *lat;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *eventAddress;
@property (nonatomic,strong) NSString *eventDistance;
@property (nonatomic,strong) NSString *dataId;

@end
